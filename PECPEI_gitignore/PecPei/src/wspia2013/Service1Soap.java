/**
 * Service1Soap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public interface Service1Soap extends java.rmi.Remote {

    /**
     * Metodo pubblico per la protocollazione semplice di un documento.
     */
    public java.lang.String protocolla(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per la protocollazione con immagine.
     */
    public java.lang.String protocollaConImmagine(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per la protocollazione con immagine.
     */
    public java.lang.String protocollaConImmagineBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per la protocollazione semplice di un documento.
     */
    public java.lang.String protocollaCICS(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per la protocollazione interoperabilità in
     * entrata.
     */
    public java.lang.String interopEntrata(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per la protocollazione interoperabilità in
     * uscita
     */
    public java.lang.String interopUscita(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String indTelRisposta, java.lang.String xml) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per l'associazione di una immagine a un documento
     * protocollato.
     */
    public java.lang.String associaImmagine(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per associa immagine Blob64.
     */
    public java.lang.String associaImmagineBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml, java.lang.String blob64String) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per l'associazione di un allegato contestuale
     * a un documento protocollato.
     */
    public java.lang.String associaImmagineAllegatoContestuale(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per associare un allegato.
     */
    public java.lang.String associaAllegato(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per associare un allegato.
     */
    public java.lang.String associaAllegatoBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per l'annullamento di un documento protocollato
     */
    public java.lang.String annullaProtocollo(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String motivoAnnullamento) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per la modifica di un documento.
     */
    public java.lang.String modificaDocumento(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il retrieval dell'immagine associata ad
     * un documento.
     */
    public java.lang.String getImmagine(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il retrieval dell'immagine associata ad
     * un documento in formato Blob64.
     */
    public wspia2013.XmlGetImmagineBlob64Response getImmagineBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il retrieval dell'immagine associata ad
     * un documento in formato Blob64.
     */
    public wspia2013.XmlGetImmagineBlob64ResponseExt getImmagineBlob64Ext(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per la consultazione dei documenti allegati.
     */
    public java.lang.String getAllegati(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero del Processo Destinatatio associato
     * alle singole sedi.
     */
    public java.lang.String getProcessoDestinatario(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero degli allegati contestuali
     * a un documento protocollato.
     */
    public java.lang.String getAllegatiContestuali(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero in formato Blob64 degli allegati
     * contestuali a un documento protocollato.
     */
    public wspia2013.XmlGetAllegatiContestualiBlob64Response getAllegatiContestualiBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero del titolario associato alle
     * sedi.
     */
    public java.lang.String getTitolario(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String PSTipoAOO) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero del titolario PEI-PEC associato
     * alle sedi.
     */
    public java.lang.String getTitolarioPEI_PEC(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String PSTipoAOO) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero della ricevuta di protocollo(pdf,
     * foglio di stile - dati xml - barcode in formato tif).
     */
    public java.lang.String getRicevutaProtocollo(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String bilinguismo, java.lang.String outputMode) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il trasferimento dei protocolli ad altra
     * sede.
     */
    public java.lang.String trasferisci(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, wspia2013.Trasferimento[] datiTrasferimento) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero del registro di protocollo
     * in un dato periodo.
     */
    public java.lang.String getRegistroProtocollo(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String dataDa, java.lang.String dataA, java.lang.String progressivo, int numeroMax) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero del registro di protocollo.
     */
    public java.lang.String ricercaDocumentiProtocollati(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml, java.lang.String progressivo, int numeroMax) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per recuperare l'elenco dei poli specializzati.
     */
    public java.lang.String getPoliSpecializzati(java.lang.String codApp, java.lang.String codAmm, java.lang.String codAoo, java.lang.String codUtente) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero dei metadati documenti informatici
     */
    public wspia2013.MetadatiDocumentiInformaticiResponse getMetadatiDocumentiInformatici(java.lang.String codApp, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero di un documento informatico
     */
    public wspia2013.DocumentoInformaticoResponse getDocumentoInformatico(java.lang.String codApp, java.lang.String codAOO, java.lang.String codUtente, java.lang.String documentID) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero della ricevuta di protocollo
     * formato PDF con codifica base64.
     */
    public wspia2013.RicevutaBase64Response getRicevutaProtocolloPdfBase64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero della ricevuta di protocollo
     * formato Barcode con codifica base64.
     */
    public wspia2013.RicevutaBase64Response getRicevutaProtocolloBarcodeBase64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero della ricevuta di protocollo
     * formato Html con codifica base64.
     */
    public wspia2013.RicevutaHTMLBase64Response getRicevutaProtocolloHtmlBase64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String bilinguismo) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero della ricevuta di protocollo
     * formato Xml-Fop con codifica base64.
     */
    public wspia2013.RicevutaXmlBase64Response getRicevutaProtocolloXmlFopBase64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero della ricevuta di protocollo
     * formato Xml-Html con codifica base64.
     */
    public wspia2013.RicevutaXmlBase64Response getRicevutaProtocolloXmlHtmlBase64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il settaggio dello stato in GDP2.
     */
    public java.lang.String settaStatoGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String stato, int GDPoEXT) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il settaggio dello stato in GDP.
     */
    public java.lang.String settaStatoGDP(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String matricola, java.lang.String segnatura, java.lang.String stato, java.lang.String codSedeTrasf) throws java.rmi.RemoteException;

    /**
     * Imposta una tabella di corrispondenza tra gli stati di una
     * applicazione esterna ed gli stati del GDP2
     */
    public java.lang.String settaMapGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String statoExt, java.lang.String descStatoExt, java.lang.String statoGDP) throws java.rmi.RemoteException;

    /**
     * Recupera lo stato di corrispondenza tra un'applicazione esterna
     * ed uno stato GDP2
     */
    public java.lang.String getMapGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String statoExt) throws java.rmi.RemoteException;

    /**
     * Recupera la lista di stati di corrispondenza tra un'applicazione
     * esterna ed uno stato GDP2.
     */
    public java.lang.String getMappingGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per il recupero della versione del client di
     * acquisizione massiva.
     */
    public java.lang.String getVersioneBC() throws java.rmi.RemoteException;

    /**
     * Il metodo restituisce, se l’operazione è andata a buon fine,
     * un Arraylist che descrive lo stato del documento in GDP2, nel formato
     * idsStato-DescStato.
     */
    public java.lang.String getStatoGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per la protocollazione interoperabilità in
     * entrata, gestione files in Blob64.
     */
    public java.lang.String interopEntrataBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException;

    /**
     * Metodo pubblico per la protocollazione interoperabilità in
     * uscita, gestione files in Blob64.
     */
    public java.lang.String interopUscitaBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String indTelRisposta, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException;

    /**
     * Interoperabilità in Entrata conforme alla circolare 2013, con
     * invio di files in formato string base 64
     */
    public wspia2013.InteropEntrataResponse2013 interopEntrata2013(java.lang.String codAMM, java.lang.String codAOO, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException;

    /**
     * Interoperabilità in Uscita conforme alla circolare 2013, con
     * invio di files in formato string base 64
     */
    public wspia2013.InteropUscitaResponse2013 interopUscita2013(java.lang.String codAMM, java.lang.String codAOO, java.lang.String indTelRisposta, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException;
}
