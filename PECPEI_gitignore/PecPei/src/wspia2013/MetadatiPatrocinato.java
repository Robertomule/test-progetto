/**
 * MetadatiPatrocinato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class MetadatiPatrocinato  extends wspia2013.DatiProtocollo  implements java.io.Serializable {
    private wspia2013.Classifica[] classificazioni;

    private wspia2013.Soggetto mittente;

    private wspia2013.Soggetto[] soggettiAfferenti;

    public MetadatiPatrocinato() {
    }

    public MetadatiPatrocinato(
           java.lang.String categoriaProtocollo,
           java.lang.String oggetto,
           java.util.Calendar dataProtocollazione,
           java.util.Calendar dataAnnullamento,
           java.lang.String segnatura,
           java.lang.String segnaturaIniziale,
           wspia2013.Classifica[] classificazioni,
           wspia2013.Soggetto mittente,
           wspia2013.Soggetto[] soggettiAfferenti) {
        super(
            categoriaProtocollo,
            oggetto,
            dataProtocollazione,
            dataAnnullamento,
            segnatura,
            segnaturaIniziale);
        this.classificazioni = classificazioni;
        this.mittente = mittente;
        this.soggettiAfferenti = soggettiAfferenti;
    }


    /**
     * Gets the classificazioni value for this MetadatiPatrocinato.
     * 
     * @return classificazioni
     */
    public wspia2013.Classifica[] getClassificazioni() {
        return classificazioni;
    }


    /**
     * Sets the classificazioni value for this MetadatiPatrocinato.
     * 
     * @param classificazioni
     */
    public void setClassificazioni(wspia2013.Classifica[] classificazioni) {
        this.classificazioni = classificazioni;
    }


    /**
     * Gets the mittente value for this MetadatiPatrocinato.
     * 
     * @return mittente
     */
    public wspia2013.Soggetto getMittente() {
        return mittente;
    }


    /**
     * Sets the mittente value for this MetadatiPatrocinato.
     * 
     * @param mittente
     */
    public void setMittente(wspia2013.Soggetto mittente) {
        this.mittente = mittente;
    }


    /**
     * Gets the soggettiAfferenti value for this MetadatiPatrocinato.
     * 
     * @return soggettiAfferenti
     */
    public wspia2013.Soggetto[] getSoggettiAfferenti() {
        return soggettiAfferenti;
    }


    /**
     * Sets the soggettiAfferenti value for this MetadatiPatrocinato.
     * 
     * @param soggettiAfferenti
     */
    public void setSoggettiAfferenti(wspia2013.Soggetto[] soggettiAfferenti) {
        this.soggettiAfferenti = soggettiAfferenti;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MetadatiPatrocinato)) return false;
        MetadatiPatrocinato other = (MetadatiPatrocinato) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.classificazioni==null && other.getClassificazioni()==null) || 
             (this.classificazioni!=null &&
              java.util.Arrays.equals(this.classificazioni, other.getClassificazioni()))) &&
            ((this.mittente==null && other.getMittente()==null) || 
             (this.mittente!=null &&
              this.mittente.equals(other.getMittente()))) &&
            ((this.soggettiAfferenti==null && other.getSoggettiAfferenti()==null) || 
             (this.soggettiAfferenti!=null &&
              java.util.Arrays.equals(this.soggettiAfferenti, other.getSoggettiAfferenti())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getClassificazioni() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClassificazioni());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClassificazioni(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMittente() != null) {
            _hashCode += getMittente().hashCode();
        }
        if (getSoggettiAfferenti() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSoggettiAfferenti());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSoggettiAfferenti(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MetadatiPatrocinato.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "MetadatiPatrocinato"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classificazioni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Classificazioni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "Classifica"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mittente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Mittente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "Soggetto"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soggettiAfferenti");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SoggettiAfferenti"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "Soggetto"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
