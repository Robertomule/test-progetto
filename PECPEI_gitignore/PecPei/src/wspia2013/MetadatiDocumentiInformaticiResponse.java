/**
 * MetadatiDocumentiInformaticiResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class MetadatiDocumentiInformaticiResponse  extends wspia2013.Response  implements java.io.Serializable {
    private wspia2013.MetadatiDocumentoInformatico[] metadatiDocumentiInformatici;

    public MetadatiDocumentiInformaticiResponse() {
    }

    public MetadatiDocumentiInformaticiResponse(
           wspia2013.Esito codice,
           java.lang.String descrizione,
           wspia2013.MetadatiDocumentoInformatico[] metadatiDocumentiInformatici) {
        super(
            codice,
            descrizione);
        this.metadatiDocumentiInformatici = metadatiDocumentiInformatici;
    }


    /**
     * Gets the metadatiDocumentiInformatici value for this MetadatiDocumentiInformaticiResponse.
     * 
     * @return metadatiDocumentiInformatici
     */
    public wspia2013.MetadatiDocumentoInformatico[] getMetadatiDocumentiInformatici() {
        return metadatiDocumentiInformatici;
    }


    /**
     * Sets the metadatiDocumentiInformatici value for this MetadatiDocumentiInformaticiResponse.
     * 
     * @param metadatiDocumentiInformatici
     */
    public void setMetadatiDocumentiInformatici(wspia2013.MetadatiDocumentoInformatico[] metadatiDocumentiInformatici) {
        this.metadatiDocumentiInformatici = metadatiDocumentiInformatici;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MetadatiDocumentiInformaticiResponse)) return false;
        MetadatiDocumentiInformaticiResponse other = (MetadatiDocumentiInformaticiResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.metadatiDocumentiInformatici==null && other.getMetadatiDocumentiInformatici()==null) || 
             (this.metadatiDocumentiInformatici!=null &&
              java.util.Arrays.equals(this.metadatiDocumentiInformatici, other.getMetadatiDocumentiInformatici())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMetadatiDocumentiInformatici() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMetadatiDocumentiInformatici());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMetadatiDocumentiInformatici(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MetadatiDocumentiInformaticiResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "MetadatiDocumentiInformaticiResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metadatiDocumentiInformatici");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MetadatiDocumentiInformatici"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "MetadatiDocumentoInformatico"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
