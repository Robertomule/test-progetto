/**
 * RicevutaHTMLBase64Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class RicevutaHTMLBase64Response  extends wspia2013.Response  implements java.io.Serializable {
    private wspia2013.BinaryBase64 ricevutaProtocolloHTML;

    private wspia2013.BinaryBase64 logo;

    private wspia2013.BinaryBase64 barcode;

    public RicevutaHTMLBase64Response() {
    }

    public RicevutaHTMLBase64Response(
           wspia2013.Esito codice,
           java.lang.String descrizione,
           wspia2013.BinaryBase64 ricevutaProtocolloHTML,
           wspia2013.BinaryBase64 logo,
           wspia2013.BinaryBase64 barcode) {
        super(
            codice,
            descrizione);
        this.ricevutaProtocolloHTML = ricevutaProtocolloHTML;
        this.logo = logo;
        this.barcode = barcode;
    }


    /**
     * Gets the ricevutaProtocolloHTML value for this RicevutaHTMLBase64Response.
     * 
     * @return ricevutaProtocolloHTML
     */
    public wspia2013.BinaryBase64 getRicevutaProtocolloHTML() {
        return ricevutaProtocolloHTML;
    }


    /**
     * Sets the ricevutaProtocolloHTML value for this RicevutaHTMLBase64Response.
     * 
     * @param ricevutaProtocolloHTML
     */
    public void setRicevutaProtocolloHTML(wspia2013.BinaryBase64 ricevutaProtocolloHTML) {
        this.ricevutaProtocolloHTML = ricevutaProtocolloHTML;
    }


    /**
     * Gets the logo value for this RicevutaHTMLBase64Response.
     * 
     * @return logo
     */
    public wspia2013.BinaryBase64 getLogo() {
        return logo;
    }


    /**
     * Sets the logo value for this RicevutaHTMLBase64Response.
     * 
     * @param logo
     */
    public void setLogo(wspia2013.BinaryBase64 logo) {
        this.logo = logo;
    }


    /**
     * Gets the barcode value for this RicevutaHTMLBase64Response.
     * 
     * @return barcode
     */
    public wspia2013.BinaryBase64 getBarcode() {
        return barcode;
    }


    /**
     * Sets the barcode value for this RicevutaHTMLBase64Response.
     * 
     * @param barcode
     */
    public void setBarcode(wspia2013.BinaryBase64 barcode) {
        this.barcode = barcode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RicevutaHTMLBase64Response)) return false;
        RicevutaHTMLBase64Response other = (RicevutaHTMLBase64Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ricevutaProtocolloHTML==null && other.getRicevutaProtocolloHTML()==null) || 
             (this.ricevutaProtocolloHTML!=null &&
              this.ricevutaProtocolloHTML.equals(other.getRicevutaProtocolloHTML()))) &&
            ((this.logo==null && other.getLogo()==null) || 
             (this.logo!=null &&
              this.logo.equals(other.getLogo()))) &&
            ((this.barcode==null && other.getBarcode()==null) || 
             (this.barcode!=null &&
              this.barcode.equals(other.getBarcode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRicevutaProtocolloHTML() != null) {
            _hashCode += getRicevutaProtocolloHTML().hashCode();
        }
        if (getLogo() != null) {
            _hashCode += getLogo().hashCode();
        }
        if (getBarcode() != null) {
            _hashCode += getBarcode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RicevutaHTMLBase64Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "RicevutaHTMLBase64Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ricevutaProtocolloHTML");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RicevutaProtocolloHTML"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "BinaryBase64"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Logo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "BinaryBase64"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("barcode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Barcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "BinaryBase64"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
