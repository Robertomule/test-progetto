/**
 * XmlGetImmagineBlob64Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class XmlGetImmagineBlob64Response  extends wspia2013.XmlResponse  implements java.io.Serializable {
    private int codiceEsito;

    private java.lang.String descrizioneEsito;

    private java.lang.String blob64Binary;

    public XmlGetImmagineBlob64Response() {
    }

    public XmlGetImmagineBlob64Response(
           int codiceEsito,
           java.lang.String descrizioneEsito,
           java.lang.String blob64Binary) {
        this.codiceEsito = codiceEsito;
        this.descrizioneEsito = descrizioneEsito;
        this.blob64Binary = blob64Binary;
    }


    /**
     * Gets the codiceEsito value for this XmlGetImmagineBlob64Response.
     * 
     * @return codiceEsito
     */
    public int getCodiceEsito() {
        return codiceEsito;
    }


    /**
     * Sets the codiceEsito value for this XmlGetImmagineBlob64Response.
     * 
     * @param codiceEsito
     */
    public void setCodiceEsito(int codiceEsito) {
        this.codiceEsito = codiceEsito;
    }


    /**
     * Gets the descrizioneEsito value for this XmlGetImmagineBlob64Response.
     * 
     * @return descrizioneEsito
     */
    public java.lang.String getDescrizioneEsito() {
        return descrizioneEsito;
    }


    /**
     * Sets the descrizioneEsito value for this XmlGetImmagineBlob64Response.
     * 
     * @param descrizioneEsito
     */
    public void setDescrizioneEsito(java.lang.String descrizioneEsito) {
        this.descrizioneEsito = descrizioneEsito;
    }


    /**
     * Gets the blob64Binary value for this XmlGetImmagineBlob64Response.
     * 
     * @return blob64Binary
     */
    public java.lang.String getBlob64Binary() {
        return blob64Binary;
    }


    /**
     * Sets the blob64Binary value for this XmlGetImmagineBlob64Response.
     * 
     * @param blob64Binary
     */
    public void setBlob64Binary(java.lang.String blob64Binary) {
        this.blob64Binary = blob64Binary;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof XmlGetImmagineBlob64Response)) return false;
        XmlGetImmagineBlob64Response other = (XmlGetImmagineBlob64Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.codiceEsito == other.getCodiceEsito() &&
            ((this.descrizioneEsito==null && other.getDescrizioneEsito()==null) || 
             (this.descrizioneEsito!=null &&
              this.descrizioneEsito.equals(other.getDescrizioneEsito()))) &&
            ((this.blob64Binary==null && other.getBlob64Binary()==null) || 
             (this.blob64Binary!=null &&
              this.blob64Binary.equals(other.getBlob64Binary())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getCodiceEsito();
        if (getDescrizioneEsito() != null) {
            _hashCode += getDescrizioneEsito().hashCode();
        }
        if (getBlob64Binary() != null) {
            _hashCode += getBlob64Binary().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(XmlGetImmagineBlob64Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "XmlGetImmagineBlob64Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceEsito");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceEsito"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizioneEsito");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DescrizioneEsito"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("blob64Binary");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Blob64Binary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
