/**
 * DocumentoInformaticoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class DocumentoInformaticoResponse  extends wspia2013.Response  implements java.io.Serializable {
    private wspia2013.MetadatiDocumentoInformatico metadatiDocumentoInformatico;

    private byte[] body;

    public DocumentoInformaticoResponse() {
    }

    public DocumentoInformaticoResponse(
           wspia2013.Esito codice,
           java.lang.String descrizione,
           wspia2013.MetadatiDocumentoInformatico metadatiDocumentoInformatico,
           byte[] body) {
        super(
            codice,
            descrizione);
        this.metadatiDocumentoInformatico = metadatiDocumentoInformatico;
        this.body = body;
    }


    /**
     * Gets the metadatiDocumentoInformatico value for this DocumentoInformaticoResponse.
     * 
     * @return metadatiDocumentoInformatico
     */
    public wspia2013.MetadatiDocumentoInformatico getMetadatiDocumentoInformatico() {
        return metadatiDocumentoInformatico;
    }


    /**
     * Sets the metadatiDocumentoInformatico value for this DocumentoInformaticoResponse.
     * 
     * @param metadatiDocumentoInformatico
     */
    public void setMetadatiDocumentoInformatico(wspia2013.MetadatiDocumentoInformatico metadatiDocumentoInformatico) {
        this.metadatiDocumentoInformatico = metadatiDocumentoInformatico;
    }


    /**
     * Gets the body value for this DocumentoInformaticoResponse.
     * 
     * @return body
     */
    public byte[] getBody() {
        return body;
    }


    /**
     * Sets the body value for this DocumentoInformaticoResponse.
     * 
     * @param body
     */
    public void setBody(byte[] body) {
        this.body = body;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentoInformaticoResponse)) return false;
        DocumentoInformaticoResponse other = (DocumentoInformaticoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.metadatiDocumentoInformatico==null && other.getMetadatiDocumentoInformatico()==null) || 
             (this.metadatiDocumentoInformatico!=null &&
              this.metadatiDocumentoInformatico.equals(other.getMetadatiDocumentoInformatico()))) &&
            ((this.body==null && other.getBody()==null) || 
             (this.body!=null &&
              java.util.Arrays.equals(this.body, other.getBody())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMetadatiDocumentoInformatico() != null) {
            _hashCode += getMetadatiDocumentoInformatico().hashCode();
        }
        if (getBody() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBody());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBody(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentoInformaticoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "DocumentoInformaticoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metadatiDocumentoInformatico");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MetadatiDocumentoInformatico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "MetadatiDocumentoInformatico"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("body");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Body"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
