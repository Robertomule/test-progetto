/**
 * InteropUscitaResponse2013.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class InteropUscitaResponse2013  implements java.io.Serializable {
    private wspia2013.EnResult codice;

    private java.lang.String descrizione;

    private java.lang.String segnaturaXML;

    private java.lang.String notificaEccezione;

    public InteropUscitaResponse2013() {
    }

    public InteropUscitaResponse2013(
           wspia2013.EnResult codice,
           java.lang.String descrizione,
           java.lang.String segnaturaXML,
           java.lang.String notificaEccezione) {
           this.codice = codice;
           this.descrizione = descrizione;
           this.segnaturaXML = segnaturaXML;
           this.notificaEccezione = notificaEccezione;
    }


    /**
     * Gets the codice value for this InteropUscitaResponse2013.
     * 
     * @return codice
     */
    public wspia2013.EnResult getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this InteropUscitaResponse2013.
     * 
     * @param codice
     */
    public void setCodice(wspia2013.EnResult codice) {
        this.codice = codice;
    }


    /**
     * Gets the descrizione value for this InteropUscitaResponse2013.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this InteropUscitaResponse2013.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the segnaturaXML value for this InteropUscitaResponse2013.
     * 
     * @return segnaturaXML
     */
    public java.lang.String getSegnaturaXML() {
        return segnaturaXML;
    }


    /**
     * Sets the segnaturaXML value for this InteropUscitaResponse2013.
     * 
     * @param segnaturaXML
     */
    public void setSegnaturaXML(java.lang.String segnaturaXML) {
        this.segnaturaXML = segnaturaXML;
    }


    /**
     * Gets the notificaEccezione value for this InteropUscitaResponse2013.
     * 
     * @return notificaEccezione
     */
    public java.lang.String getNotificaEccezione() {
        return notificaEccezione;
    }


    /**
     * Sets the notificaEccezione value for this InteropUscitaResponse2013.
     * 
     * @param notificaEccezione
     */
    public void setNotificaEccezione(java.lang.String notificaEccezione) {
        this.notificaEccezione = notificaEccezione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InteropUscitaResponse2013)) return false;
        InteropUscitaResponse2013 other = (InteropUscitaResponse2013) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.segnaturaXML==null && other.getSegnaturaXML()==null) || 
             (this.segnaturaXML!=null &&
              this.segnaturaXML.equals(other.getSegnaturaXML()))) &&
            ((this.notificaEccezione==null && other.getNotificaEccezione()==null) || 
             (this.notificaEccezione!=null &&
              this.notificaEccezione.equals(other.getNotificaEccezione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getSegnaturaXML() != null) {
            _hashCode += getSegnaturaXML().hashCode();
        }
        if (getNotificaEccezione() != null) {
            _hashCode += getNotificaEccezione().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InteropUscitaResponse2013.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it", "InteropUscitaResponse2013"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.inps.it", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it", "EnResult"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.inps.it", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnaturaXML");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.inps.it", "SegnaturaXML"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notificaEccezione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.inps.it", "NotificaEccezione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
