/**
 * RicevutaXmlBase64Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class RicevutaXmlBase64Response  extends wspia2013.Response  implements java.io.Serializable {
    private wspia2013.BinaryBase64 ricevutaProtocolloXml;

    private wspia2013.BinaryBase64 logo;

    private wspia2013.BinaryBase64 barcode;

    private wspia2013.BinaryBase64 xsl;

    public RicevutaXmlBase64Response() {
    }

    public RicevutaXmlBase64Response(
           wspia2013.Esito codice,
           java.lang.String descrizione,
           wspia2013.BinaryBase64 ricevutaProtocolloXml,
           wspia2013.BinaryBase64 logo,
           wspia2013.BinaryBase64 barcode,
           wspia2013.BinaryBase64 xsl) {
        super(
            codice,
            descrizione);
        this.ricevutaProtocolloXml = ricevutaProtocolloXml;
        this.logo = logo;
        this.barcode = barcode;
        this.xsl = xsl;
    }


    /**
     * Gets the ricevutaProtocolloXml value for this RicevutaXmlBase64Response.
     * 
     * @return ricevutaProtocolloXml
     */
    public wspia2013.BinaryBase64 getRicevutaProtocolloXml() {
        return ricevutaProtocolloXml;
    }


    /**
     * Sets the ricevutaProtocolloXml value for this RicevutaXmlBase64Response.
     * 
     * @param ricevutaProtocolloXml
     */
    public void setRicevutaProtocolloXml(wspia2013.BinaryBase64 ricevutaProtocolloXml) {
        this.ricevutaProtocolloXml = ricevutaProtocolloXml;
    }


    /**
     * Gets the logo value for this RicevutaXmlBase64Response.
     * 
     * @return logo
     */
    public wspia2013.BinaryBase64 getLogo() {
        return logo;
    }


    /**
     * Sets the logo value for this RicevutaXmlBase64Response.
     * 
     * @param logo
     */
    public void setLogo(wspia2013.BinaryBase64 logo) {
        this.logo = logo;
    }


    /**
     * Gets the barcode value for this RicevutaXmlBase64Response.
     * 
     * @return barcode
     */
    public wspia2013.BinaryBase64 getBarcode() {
        return barcode;
    }


    /**
     * Sets the barcode value for this RicevutaXmlBase64Response.
     * 
     * @param barcode
     */
    public void setBarcode(wspia2013.BinaryBase64 barcode) {
        this.barcode = barcode;
    }


    /**
     * Gets the xsl value for this RicevutaXmlBase64Response.
     * 
     * @return xsl
     */
    public wspia2013.BinaryBase64 getXsl() {
        return xsl;
    }


    /**
     * Sets the xsl value for this RicevutaXmlBase64Response.
     * 
     * @param xsl
     */
    public void setXsl(wspia2013.BinaryBase64 xsl) {
        this.xsl = xsl;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RicevutaXmlBase64Response)) return false;
        RicevutaXmlBase64Response other = (RicevutaXmlBase64Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ricevutaProtocolloXml==null && other.getRicevutaProtocolloXml()==null) || 
             (this.ricevutaProtocolloXml!=null &&
              this.ricevutaProtocolloXml.equals(other.getRicevutaProtocolloXml()))) &&
            ((this.logo==null && other.getLogo()==null) || 
             (this.logo!=null &&
              this.logo.equals(other.getLogo()))) &&
            ((this.barcode==null && other.getBarcode()==null) || 
             (this.barcode!=null &&
              this.barcode.equals(other.getBarcode()))) &&
            ((this.xsl==null && other.getXsl()==null) || 
             (this.xsl!=null &&
              this.xsl.equals(other.getXsl())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRicevutaProtocolloXml() != null) {
            _hashCode += getRicevutaProtocolloXml().hashCode();
        }
        if (getLogo() != null) {
            _hashCode += getLogo().hashCode();
        }
        if (getBarcode() != null) {
            _hashCode += getBarcode().hashCode();
        }
        if (getXsl() != null) {
            _hashCode += getXsl().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RicevutaXmlBase64Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "RicevutaXmlBase64Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ricevutaProtocolloXml");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RicevutaProtocolloXml"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "BinaryBase64"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Logo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "BinaryBase64"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("barcode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Barcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "BinaryBase64"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xsl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Xsl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "BinaryBase64"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
