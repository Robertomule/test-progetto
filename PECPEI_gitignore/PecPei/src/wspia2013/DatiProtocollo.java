/**
 * DatiProtocollo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class DatiProtocollo  implements java.io.Serializable {
    private java.lang.String categoriaProtocollo;

    private java.lang.String oggetto;

    private java.util.Calendar dataProtocollazione;

    private java.util.Calendar dataAnnullamento;

    private java.lang.String segnatura;

    private java.lang.String segnaturaIniziale;

    public DatiProtocollo() {
    }

    public DatiProtocollo(
           java.lang.String categoriaProtocollo,
           java.lang.String oggetto,
           java.util.Calendar dataProtocollazione,
           java.util.Calendar dataAnnullamento,
           java.lang.String segnatura,
           java.lang.String segnaturaIniziale) {
           this.categoriaProtocollo = categoriaProtocollo;
           this.oggetto = oggetto;
           this.dataProtocollazione = dataProtocollazione;
           this.dataAnnullamento = dataAnnullamento;
           this.segnatura = segnatura;
           this.segnaturaIniziale = segnaturaIniziale;
    }


    /**
     * Gets the categoriaProtocollo value for this DatiProtocollo.
     * 
     * @return categoriaProtocollo
     */
    public java.lang.String getCategoriaProtocollo() {
        return categoriaProtocollo;
    }


    /**
     * Sets the categoriaProtocollo value for this DatiProtocollo.
     * 
     * @param categoriaProtocollo
     */
    public void setCategoriaProtocollo(java.lang.String categoriaProtocollo) {
        this.categoriaProtocollo = categoriaProtocollo;
    }


    /**
     * Gets the oggetto value for this DatiProtocollo.
     * 
     * @return oggetto
     */
    public java.lang.String getOggetto() {
        return oggetto;
    }


    /**
     * Sets the oggetto value for this DatiProtocollo.
     * 
     * @param oggetto
     */
    public void setOggetto(java.lang.String oggetto) {
        this.oggetto = oggetto;
    }


    /**
     * Gets the dataProtocollazione value for this DatiProtocollo.
     * 
     * @return dataProtocollazione
     */
    public java.util.Calendar getDataProtocollazione() {
        return dataProtocollazione;
    }


    /**
     * Sets the dataProtocollazione value for this DatiProtocollo.
     * 
     * @param dataProtocollazione
     */
    public void setDataProtocollazione(java.util.Calendar dataProtocollazione) {
        this.dataProtocollazione = dataProtocollazione;
    }


    /**
     * Gets the dataAnnullamento value for this DatiProtocollo.
     * 
     * @return dataAnnullamento
     */
    public java.util.Calendar getDataAnnullamento() {
        return dataAnnullamento;
    }


    /**
     * Sets the dataAnnullamento value for this DatiProtocollo.
     * 
     * @param dataAnnullamento
     */
    public void setDataAnnullamento(java.util.Calendar dataAnnullamento) {
        this.dataAnnullamento = dataAnnullamento;
    }


    /**
     * Gets the segnatura value for this DatiProtocollo.
     * 
     * @return segnatura
     */
    public java.lang.String getSegnatura() {
        return segnatura;
    }


    /**
     * Sets the segnatura value for this DatiProtocollo.
     * 
     * @param segnatura
     */
    public void setSegnatura(java.lang.String segnatura) {
        this.segnatura = segnatura;
    }


    /**
     * Gets the segnaturaIniziale value for this DatiProtocollo.
     * 
     * @return segnaturaIniziale
     */
    public java.lang.String getSegnaturaIniziale() {
        return segnaturaIniziale;
    }


    /**
     * Sets the segnaturaIniziale value for this DatiProtocollo.
     * 
     * @param segnaturaIniziale
     */
    public void setSegnaturaIniziale(java.lang.String segnaturaIniziale) {
        this.segnaturaIniziale = segnaturaIniziale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DatiProtocollo)) return false;
        DatiProtocollo other = (DatiProtocollo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.categoriaProtocollo==null && other.getCategoriaProtocollo()==null) || 
             (this.categoriaProtocollo!=null &&
              this.categoriaProtocollo.equals(other.getCategoriaProtocollo()))) &&
            ((this.oggetto==null && other.getOggetto()==null) || 
             (this.oggetto!=null &&
              this.oggetto.equals(other.getOggetto()))) &&
            ((this.dataProtocollazione==null && other.getDataProtocollazione()==null) || 
             (this.dataProtocollazione!=null &&
              this.dataProtocollazione.equals(other.getDataProtocollazione()))) &&
            ((this.dataAnnullamento==null && other.getDataAnnullamento()==null) || 
             (this.dataAnnullamento!=null &&
              this.dataAnnullamento.equals(other.getDataAnnullamento()))) &&
            ((this.segnatura==null && other.getSegnatura()==null) || 
             (this.segnatura!=null &&
              this.segnatura.equals(other.getSegnatura()))) &&
            ((this.segnaturaIniziale==null && other.getSegnaturaIniziale()==null) || 
             (this.segnaturaIniziale!=null &&
              this.segnaturaIniziale.equals(other.getSegnaturaIniziale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCategoriaProtocollo() != null) {
            _hashCode += getCategoriaProtocollo().hashCode();
        }
        if (getOggetto() != null) {
            _hashCode += getOggetto().hashCode();
        }
        if (getDataProtocollazione() != null) {
            _hashCode += getDataProtocollazione().hashCode();
        }
        if (getDataAnnullamento() != null) {
            _hashCode += getDataAnnullamento().hashCode();
        }
        if (getSegnatura() != null) {
            _hashCode += getSegnatura().hashCode();
        }
        if (getSegnaturaIniziale() != null) {
            _hashCode += getSegnaturaIniziale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DatiProtocollo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "DatiProtocollo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoriaProtocollo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CategoriaProtocollo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oggetto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Oggetto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataProtocollazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataProtocollazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataAnnullamento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataAnnullamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnatura");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Segnatura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnaturaIniziale");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SegnaturaIniziale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
