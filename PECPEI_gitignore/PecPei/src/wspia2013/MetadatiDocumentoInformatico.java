/**
 * MetadatiDocumentoInformatico.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class MetadatiDocumentoInformatico  implements java.io.Serializable {
    private java.lang.String documentID;

    private java.lang.String contentType;

    private int contentLength;

    private java.lang.String originalFileName;

    private java.util.Calendar dataCreazione;

    private int versione;

    private wspia2013.DocumentType documentType;

    public MetadatiDocumentoInformatico() {
    }

    public MetadatiDocumentoInformatico(
           java.lang.String documentID,
           java.lang.String contentType,
           int contentLength,
           java.lang.String originalFileName,
           java.util.Calendar dataCreazione,
           int versione,
           wspia2013.DocumentType documentType) {
           this.documentID = documentID;
           this.contentType = contentType;
           this.contentLength = contentLength;
           this.originalFileName = originalFileName;
           this.dataCreazione = dataCreazione;
           this.versione = versione;
           this.documentType = documentType;
    }


    /**
     * Gets the documentID value for this MetadatiDocumentoInformatico.
     * 
     * @return documentID
     */
    public java.lang.String getDocumentID() {
        return documentID;
    }


    /**
     * Sets the documentID value for this MetadatiDocumentoInformatico.
     * 
     * @param documentID
     */
    public void setDocumentID(java.lang.String documentID) {
        this.documentID = documentID;
    }


    /**
     * Gets the contentType value for this MetadatiDocumentoInformatico.
     * 
     * @return contentType
     */
    public java.lang.String getContentType() {
        return contentType;
    }


    /**
     * Sets the contentType value for this MetadatiDocumentoInformatico.
     * 
     * @param contentType
     */
    public void setContentType(java.lang.String contentType) {
        this.contentType = contentType;
    }


    /**
     * Gets the contentLength value for this MetadatiDocumentoInformatico.
     * 
     * @return contentLength
     */
    public int getContentLength() {
        return contentLength;
    }


    /**
     * Sets the contentLength value for this MetadatiDocumentoInformatico.
     * 
     * @param contentLength
     */
    public void setContentLength(int contentLength) {
        this.contentLength = contentLength;
    }


    /**
     * Gets the originalFileName value for this MetadatiDocumentoInformatico.
     * 
     * @return originalFileName
     */
    public java.lang.String getOriginalFileName() {
        return originalFileName;
    }


    /**
     * Sets the originalFileName value for this MetadatiDocumentoInformatico.
     * 
     * @param originalFileName
     */
    public void setOriginalFileName(java.lang.String originalFileName) {
        this.originalFileName = originalFileName;
    }


    /**
     * Gets the dataCreazione value for this MetadatiDocumentoInformatico.
     * 
     * @return dataCreazione
     */
    public java.util.Calendar getDataCreazione() {
        return dataCreazione;
    }


    /**
     * Sets the dataCreazione value for this MetadatiDocumentoInformatico.
     * 
     * @param dataCreazione
     */
    public void setDataCreazione(java.util.Calendar dataCreazione) {
        this.dataCreazione = dataCreazione;
    }


    /**
     * Gets the versione value for this MetadatiDocumentoInformatico.
     * 
     * @return versione
     */
    public int getVersione() {
        return versione;
    }


    /**
     * Sets the versione value for this MetadatiDocumentoInformatico.
     * 
     * @param versione
     */
    public void setVersione(int versione) {
        this.versione = versione;
    }


    /**
     * Gets the documentType value for this MetadatiDocumentoInformatico.
     * 
     * @return documentType
     */
    public wspia2013.DocumentType getDocumentType() {
        return documentType;
    }


    /**
     * Sets the documentType value for this MetadatiDocumentoInformatico.
     * 
     * @param documentType
     */
    public void setDocumentType(wspia2013.DocumentType documentType) {
        this.documentType = documentType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MetadatiDocumentoInformatico)) return false;
        MetadatiDocumentoInformatico other = (MetadatiDocumentoInformatico) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.documentID==null && other.getDocumentID()==null) || 
             (this.documentID!=null &&
              this.documentID.equals(other.getDocumentID()))) &&
            ((this.contentType==null && other.getContentType()==null) || 
             (this.contentType!=null &&
              this.contentType.equals(other.getContentType()))) &&
            this.contentLength == other.getContentLength() &&
            ((this.originalFileName==null && other.getOriginalFileName()==null) || 
             (this.originalFileName!=null &&
              this.originalFileName.equals(other.getOriginalFileName()))) &&
            ((this.dataCreazione==null && other.getDataCreazione()==null) || 
             (this.dataCreazione!=null &&
              this.dataCreazione.equals(other.getDataCreazione()))) &&
            this.versione == other.getVersione() &&
            ((this.documentType==null && other.getDocumentType()==null) || 
             (this.documentType!=null &&
              this.documentType.equals(other.getDocumentType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDocumentID() != null) {
            _hashCode += getDocumentID().hashCode();
        }
        if (getContentType() != null) {
            _hashCode += getContentType().hashCode();
        }
        _hashCode += getContentLength();
        if (getOriginalFileName() != null) {
            _hashCode += getOriginalFileName().hashCode();
        }
        if (getDataCreazione() != null) {
            _hashCode += getDataCreazione().hashCode();
        }
        _hashCode += getVersione();
        if (getDocumentType() != null) {
            _hashCode += getDocumentType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MetadatiDocumentoInformatico.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "MetadatiDocumentoInformatico"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DocumentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ContentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentLength");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ContentLength"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalFileName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OriginalFileName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataCreazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataCreazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Versione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DocumentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "DocumentType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
