/**
 * Trasferimento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public class Trasferimento  implements java.io.Serializable {
    private java.lang.String codAmm;

    private java.lang.String codAoo;

    private java.lang.String classifica;

    private java.lang.String processoDestinatario;

    private java.lang.String automatizzaTrasferimento;

    public Trasferimento() {
    }

    public Trasferimento(
           java.lang.String codAmm,
           java.lang.String codAoo,
           java.lang.String classifica,
           java.lang.String processoDestinatario,
           java.lang.String automatizzaTrasferimento) {
           this.codAmm = codAmm;
           this.codAoo = codAoo;
           this.classifica = classifica;
           this.processoDestinatario = processoDestinatario;
           this.automatizzaTrasferimento = automatizzaTrasferimento;
    }


    /**
     * Gets the codAmm value for this Trasferimento.
     * 
     * @return codAmm
     */
    public java.lang.String getCodAmm() {
        return codAmm;
    }


    /**
     * Sets the codAmm value for this Trasferimento.
     * 
     * @param codAmm
     */
    public void setCodAmm(java.lang.String codAmm) {
        this.codAmm = codAmm;
    }


    /**
     * Gets the codAoo value for this Trasferimento.
     * 
     * @return codAoo
     */
    public java.lang.String getCodAoo() {
        return codAoo;
    }


    /**
     * Sets the codAoo value for this Trasferimento.
     * 
     * @param codAoo
     */
    public void setCodAoo(java.lang.String codAoo) {
        this.codAoo = codAoo;
    }


    /**
     * Gets the classifica value for this Trasferimento.
     * 
     * @return classifica
     */
    public java.lang.String getClassifica() {
        return classifica;
    }


    /**
     * Sets the classifica value for this Trasferimento.
     * 
     * @param classifica
     */
    public void setClassifica(java.lang.String classifica) {
        this.classifica = classifica;
    }


    /**
     * Gets the processoDestinatario value for this Trasferimento.
     * 
     * @return processoDestinatario
     */
    public java.lang.String getProcessoDestinatario() {
        return processoDestinatario;
    }


    /**
     * Sets the processoDestinatario value for this Trasferimento.
     * 
     * @param processoDestinatario
     */
    public void setProcessoDestinatario(java.lang.String processoDestinatario) {
        this.processoDestinatario = processoDestinatario;
    }


    /**
     * Gets the automatizzaTrasferimento value for this Trasferimento.
     * 
     * @return automatizzaTrasferimento
     */
    public java.lang.String getAutomatizzaTrasferimento() {
        return automatizzaTrasferimento;
    }


    /**
     * Sets the automatizzaTrasferimento value for this Trasferimento.
     * 
     * @param automatizzaTrasferimento
     */
    public void setAutomatizzaTrasferimento(java.lang.String automatizzaTrasferimento) {
        this.automatizzaTrasferimento = automatizzaTrasferimento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Trasferimento)) return false;
        Trasferimento other = (Trasferimento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codAmm==null && other.getCodAmm()==null) || 
             (this.codAmm!=null &&
              this.codAmm.equals(other.getCodAmm()))) &&
            ((this.codAoo==null && other.getCodAoo()==null) || 
             (this.codAoo!=null &&
              this.codAoo.equals(other.getCodAoo()))) &&
            ((this.classifica==null && other.getClassifica()==null) || 
             (this.classifica!=null &&
              this.classifica.equals(other.getClassifica()))) &&
            ((this.processoDestinatario==null && other.getProcessoDestinatario()==null) || 
             (this.processoDestinatario!=null &&
              this.processoDestinatario.equals(other.getProcessoDestinatario()))) &&
            ((this.automatizzaTrasferimento==null && other.getAutomatizzaTrasferimento()==null) || 
             (this.automatizzaTrasferimento!=null &&
              this.automatizzaTrasferimento.equals(other.getAutomatizzaTrasferimento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodAmm() != null) {
            _hashCode += getCodAmm().hashCode();
        }
        if (getCodAoo() != null) {
            _hashCode += getCodAoo().hashCode();
        }
        if (getClassifica() != null) {
            _hashCode += getClassifica().hashCode();
        }
        if (getProcessoDestinatario() != null) {
            _hashCode += getProcessoDestinatario().hashCode();
        }
        if (getAutomatizzaTrasferimento() != null) {
            _hashCode += getAutomatizzaTrasferimento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Trasferimento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.inps.it/encodedTypes", "Trasferimento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAmm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodAmm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAoo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodAoo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classifica");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Classifica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processoDestinatario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProcessoDestinatario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("automatizzaTrasferimento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AutomatizzaTrasferimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
