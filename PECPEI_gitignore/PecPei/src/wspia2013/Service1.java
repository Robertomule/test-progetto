/**
 * Service1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia2013;

public interface Service1 extends javax.xml.rpc.Service {
    public java.lang.String getService1SoapAddress();

    public wspia2013.Service1Soap getService1Soap() throws javax.xml.rpc.ServiceException;

    public wspia2013.Service1Soap getService1Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
