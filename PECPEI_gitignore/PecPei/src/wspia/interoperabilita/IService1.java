/**
 * IService1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wspia.interoperabilita;

public interface IService1 extends java.rmi.Remote {
    public java.lang.String interopEntrata(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException;
    public java.lang.String interopUscita(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String indTelRisposta, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException;
}
