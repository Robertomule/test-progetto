package wspia.interoperabilita;

public class IService1Proxy implements wspia.interoperabilita.IService1 {
  private String _endpoint = null;
  private wspia.interoperabilita.IService1 iService1 = null;
  
  public IService1Proxy() {
    _initIService1Proxy();
  }
  
  public IService1Proxy(String endpoint) {
    _endpoint = endpoint;
    _initIService1Proxy();
  }
  
  private void _initIService1Proxy() {
    try {
      iService1 = (new wspia.interoperabilita.Service1Locator()).getBasicHttpBinding_IService1();
      if (iService1 != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iService1)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iService1)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iService1 != null)
      ((javax.xml.rpc.Stub)iService1)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public wspia.interoperabilita.IService1 getIService1() {
    if (iService1 == null)
      _initIService1Proxy();
    return iService1;
  }
  
  public java.lang.String interopEntrata(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException{
    if (iService1 == null)
      _initIService1Proxy();
    return iService1.interopEntrata(codApp, codAMM, codAOO, codUtente, xml, att);
  }
  
  public java.lang.String interopUscita(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String indTelRisposta, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException{
    if (iService1 == null)
      _initIService1Proxy();
    return iService1.interopUscita(codApp, codAMM, codAOO, codUtente, indTelRisposta, xml, att);
  }
  
  
}