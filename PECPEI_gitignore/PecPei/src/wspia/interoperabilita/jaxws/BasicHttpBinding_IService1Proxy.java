package wspia.interoperabilita.jaxws;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;

public class BasicHttpBinding_IService1Proxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private wspia.interoperabilita.jaxws.Service1 _service = null;
        private wspia.interoperabilita.jaxws.IService1 _proxy = null;
        private Dispatch<Source> _dispatch = null;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new wspia.interoperabilita.jaxws.Service1(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            _service = new wspia.interoperabilita.jaxws.Service1();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getBasicHttpBindingIService1();
        }

        public wspia.interoperabilita.jaxws.IService1 getProxy() {
            return _proxy;
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("http://tempuri.org/", "BasicHttpBinding_IService1");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

    }

    public BasicHttpBinding_IService1Proxy() {
        _descriptor = new Descriptor();
    }

    public BasicHttpBinding_IService1Proxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public String interopEntrata(String codApp, String codAMM, String codAOO, String codUtente, String xml, ArrayOfstring att) {
        return _getDescriptor().getProxy().interopEntrata(codApp,codAMM,codAOO,codUtente,xml,att);
    }

    public String interopUscita(String codApp, String codAMM, String codAOO, String codUtente, String indTelRisposta, String xml, ArrayOfstring att) {
        return _getDescriptor().getProxy().interopUscita(codApp,codAMM,codAOO,codUtente,indTelRisposta,xml,att);
    }

}