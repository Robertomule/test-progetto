package wspia;

public class Service1SoapProxy implements wspia.Service1Soap {
  private String _endpoint = null;
  private wspia.Service1Soap service1Soap = null;
  
  public Service1SoapProxy() {
    _initService1SoapProxy();
  }
  
  public Service1SoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initService1SoapProxy();
  }
  
  private void _initService1SoapProxy() {
    try {
      service1Soap = (new wspia.Service1Locator()).getService1Soap();
      if (service1Soap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)service1Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)service1Soap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (service1Soap != null)
      ((javax.xml.rpc.Stub)service1Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public wspia.Service1Soap getService1Soap() {
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap;
  }
  
  public java.lang.String protocollaConImmagineBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.protocollaConImmagineBlob64(codApp, codAMM, codAOO, codUtente, xml, att);
  }
  
  public java.lang.String getRegistroProtocollo(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String dataDa, java.lang.String dataA, java.lang.String progressivo, int numeroMax) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getRegistroProtocollo(codApp, codAMM, codAOO, codUtente, dataDa, dataA, progressivo, numeroMax);
  }
  
  public java.lang.String ricercaDocumentiProtocollati(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml, java.lang.String progressivo, int numeroMax) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.ricercaDocumentiProtocollati(codApp, codAMM, codAOO, codUtente, xml, progressivo, numeroMax);
  }
  
  public java.lang.String getRicevutaProtocollo(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String bilinguismo, java.lang.String outputMode) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getRicevutaProtocollo(codApp, codAMM, codAOO, codUtente, segnatura, bilinguismo, outputMode);
  }
  
  public java.lang.String getMapGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String statoExt) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getMapGDP2(codApp, codAMM, codAOO, codUtente, statoExt);
  }
  
  public java.lang.String getMappingGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getMappingGDP2(codApp, codAMM, codAOO, codUtente);
  }
  
  public java.lang.String settaMapGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String statoExt, java.lang.String descStatoExt, java.lang.String statoGDP) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.settaMapGDP2(codApp, codAMM, codAOO, codUtente, statoExt, descStatoExt, statoGDP);
  }
  
  public java.lang.String getStatoGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getStatoGDP2(codApp, codAMM, codAOO, codUtente, segnatura);
  }
  
  public java.lang.String settaStatoGDP2(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String stato, int GDPoEXT) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.settaStatoGDP2(codApp, codAMM, codAOO, codUtente, segnatura, stato, GDPoEXT);
  }
  
  public java.lang.String settaStatoGDP(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String matricola, java.lang.String segnatura, java.lang.String stato, java.lang.String codSedeTrasf) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.settaStatoGDP(codApp, codAMM, codAOO, matricola, segnatura, stato, codSedeTrasf);
  }
  
  public java.lang.String getAllegatiContestuali(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getAllegatiContestuali(codApp, codAMM, codAOO, codUtente, segnatura);
  }
  
  public java.lang.String getImmagine(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getImmagine(codApp, codAMM, codAOO, codUtente, segnatura);
  }
  
  public wspia.XmlGetImmagineBlob64Response getImmagineBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getImmagineBlob64(codApp, codAMM, codAOO, codUtente, segnatura);
  }
  
  public java.lang.String getAllegati(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getAllegati(codApp, codAMM, codAOO, codUtente, segnatura);
  }
  
  public java.lang.String modificaDocumento(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.modificaDocumento(codApp, codAMM, codAOO, codUtente, segnatura, xml);
  }
  
  public java.lang.String getDocumento(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getDocumento(codApp, codAMM, codAOO, codUtente, segnatura);
  }
  
  public java.lang.String interopEntrata(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.interopEntrata(codApp, codAMM, codAOO, codUtente, xml);
  }
  
  public java.lang.String interopEntrataBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.interopEntrataBlob64(codApp, codAMM, codAOO, codUtente, xml, att);
  }
  
  public java.lang.String interopUscita(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String indTelRisposta, java.lang.String xml) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.interopUscita(codApp, codAMM, codAOO, codUtente, indTelRisposta, xml);
  }
  
  public java.lang.String interopUscitaBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String indTelRisposta, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.interopUscitaBlob64(codApp, codAMM, codAOO, codUtente, indTelRisposta, xml, att);
  }
  
  public java.lang.String getProcessoDestinatario(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getProcessoDestinatario(codApp, codAMM, codAOO);
  }
  
  public java.lang.String getTitolario(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String PSTipoAOO) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getTitolario(codApp, codAMM, codAOO, PSTipoAOO);
  }
  
  public java.lang.String getTitolarioPEI_PEC(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String PSTipoAOO) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getTitolarioPEI_PEC(codApp, codAMM, codAOO, PSTipoAOO);
  }
  
  public java.lang.String associaImmagine(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.associaImmagine(codApp, codAMM, codAOO, codUtente, segnatura, xml);
  }
  
  public java.lang.String protocollaConImmagine(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.protocollaConImmagine(codApp, codAMM, codAOO, codUtente, xml);
  }
  
  public java.lang.String protocolla(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.protocolla(codApp, codAMM, codAOO, codUtente, xml);
  }
  
  public java.lang.String protocollaCICS(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String xml) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.protocollaCICS(codApp, codAMM, codAOO, codUtente, xml);
  }
  
  public java.lang.String associaAllegato(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.associaAllegato(codApp, codAMM, codAOO, codUtente, segnatura, xml);
  }
  
  public java.lang.String associaAllegatoBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml, java.lang.String[] att) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.associaAllegatoBlob64(codApp, codAMM, codAOO, codUtente, segnatura, xml, att);
  }
  
  public java.lang.String getVersioneBC() throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getVersioneBC();
  }
  
  public java.lang.String annullaProtocollo(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String motivoAnnullamento) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.annullaProtocollo(codApp, codAMM, codAOO, codUtente, segnatura, motivoAnnullamento);
  }
  
  public java.lang.String trasferisci(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, wspia.Trasferimento[] datiTrasferimento) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.trasferisci(codApp, codAMM, codAOO, codUtente, segnatura, datiTrasferimento);
  }
  
  public java.lang.String getPoliSpecializzati(java.lang.String codApp, java.lang.String codAmm, java.lang.String codAoo, java.lang.String codUtente) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.getPoliSpecializzati(codApp, codAmm, codAoo, codUtente);
  }
  
  public java.lang.String associaImmagineBlob64(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml, java.lang.String blob64String) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.associaImmagineBlob64(codApp, codAMM, codAOO, codUtente, segnatura, xml, blob64String);
  }
  
  public java.lang.String associaImmagineAllegatoContestuale(java.lang.String codApp, java.lang.String codAMM, java.lang.String codAOO, java.lang.String codUtente, java.lang.String segnatura, java.lang.String xml) throws java.rmi.RemoteException{
    if (service1Soap == null)
      _initService1SoapProxy();
    return service1Soap.associaImmagineAllegatoContestuale(codApp, codAMM, codAOO, codUtente, segnatura, xml);
  }
  
  
}