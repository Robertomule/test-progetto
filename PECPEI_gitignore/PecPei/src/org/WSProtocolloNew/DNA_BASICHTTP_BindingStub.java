/**
 * DNA_BASICHTTP_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.WSProtocolloNew;

import it.inps.soa.WS00732.data.AssociaContainerAFascicoloRequest;
import it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse;
import it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse;
import it.inps.soa.WS00732.data.CatenaFascicolazioneResponse;
import it.inps.soa.WS00732.data.ContainerRequest;
import it.inps.soa.WS00732.data.ContainerResponse;
import it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse;
import it.inps.soa.WS00732.data.CreaCatenaFascicolazioneRequest;
import it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse;
import it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleRequest;
import it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse;
import it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleRequest;
import it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse;
import it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse;
import it.inps.soa.WS00732.data.DocumentiFascicoloResponse;
import it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoRequest;
import it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse;
import it.inps.soa.WS00732.data.ListaAllegatiResponse;
import it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse;
import it.inps.soa.WS00732.data.ProtocollaAllegaResponse;
import it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse;
import it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse;
import it.inps.soa.WS00732.data.ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest;
import it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest;
import it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaRequest;
import it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazioneProcedimentaleRequest;
import it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse;
import it.inps.soa.WS00732.data.ProtocollaInEntrataNAIRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazioneProcedimentaleRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse;
import it.inps.soa.WS00732.data.ProtocollaNAIResponse;
import it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse;
import it.inps.soa.WS00732.data.ProtocolloEntrataESpedisciRequest;
import it.inps.soa.WS00732.data.ProtocolloEntrataRequest;
import it.inps.soa.WS00732.data.ProtocolloUscitaESpedisciRequest;
import it.inps.soa.WS00732.data.ProtocolloUscitaRequest;
import it.inps.soa.WS00732.data.RegistraStatoConsegnaPECInUscitaRequest;
import it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaRequest;
import it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse;
import it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse;
import it.inps.soa.WS00732.data.RichiestaCRMRequest;
import it.inps.soa.WS00732.data.RichiestaCRMResponse;
import it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse;
import it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest;
import it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse;
import it.inps.soa.WS00732.data.VariaEsitoProcedimentoRequest;
import it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse;

import java.rmi.RemoteException;

import org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse;

public class DNA_BASICHTTP_BindingStub extends org.apache.axis.client.Stub implements it.inps.soa.WS00732.IWSProtocollo {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[14];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloEntrataRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataRequest"), it.inps.soa.WS00732.data.ProtocolloEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CodiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloUscitaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaRequest"), it.inps.soa.WS00732.data.ProtocolloUscitaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataEdAllega");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloEntrataRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataRequest"), it.inps.soa.WS00732.data.ProtocolloEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnaturaPrincipale"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaAllegaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataEdAllegaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscitaEdAllega");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloUscitaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaRequest"), it.inps.soa.WS00732.data.ProtocolloUscitaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnaturaPrincipale"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaAllegaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaEdAllegaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ModificaMetadatiProtocollo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "metadatiProtocollo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiProtocollo"), it.inps.soa.WS00732.data.MetadatiProtocollo.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ModificaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ModificaMetadatiProtocolloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AnnullaProtocollo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "annullaProtocolloRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AnnullaProtocolloRequest"), it.inps.soa.WS00732.data.AnnullaProtocolloRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AnnullaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.AnnullaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AnnullaProtocolloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiMetadatiDocumento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.MetadatiResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiMetadatiDocumentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GeneraRicevutaProtocollo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "GeneraRicevutaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.GeneraRicevutaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GeneraRicevutaProtocolloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AssociaDocumentoPrimario");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "documentoPrimarioRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrimarioRequest"), it.inps.soa.WS00732.data.DocumentoPrimarioRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AssociaDocumentoPrimarioResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AssociaDocumentoPrimarioResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AggiungiAllegatiContestuali");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "allegatiContestualiRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiContestualiRequest"), it.inps.soa.WS00732.data.AllegatiContestualiRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.AllegatiResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AggiungiAllegatiContestualiResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TrasferisciProtocolloInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "trasferisciRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciRequest"), it.inps.soa.WS00732.data.TrasferisciRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.TrasferisciResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TrasferisciProtocolloInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "trasferisciRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciRequest"), it.inps.soa.WS00732.data.TrasferisciRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.TrasferisciResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RichiestaProtocollazioneInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataRequest"), it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaProtocollazioneInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RichiestaInteroperabilitaInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataRequest"), it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaInteroperabilitaInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

    }

    public DNA_BASICHTTP_BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public DNA_BASICHTTP_BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public DNA_BASICHTTP_BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiContestualiRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AllegatiContestualiRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AllegatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Allegato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AllegatoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Annullamento");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Annullamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AnnullaProtocolloRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AnnullaProtocolloRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AnnullaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AnnullaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Applicazione");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Applicazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfAllegato");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Allegato[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfAllegatoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AllegatoResponse[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatoResponse");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatoResponse");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfClassifica");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Classifica[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfDestinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Destinatario[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatario");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatario");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfModificaDestinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaDestinatario[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaDestinatario");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaDestinatario");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfModificaSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaSoggettoAfferente[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaSoggettoAfferente");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaSoggettoAfferente");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfRelatedDataItem");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RelatedDataItem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RelatedDataItem");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RelatedDataItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfSegnaturaAllegata");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SegnaturaAllegata[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SegnaturaAllegata");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SegnaturaAllegata");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SoggettoAfferente[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettoAfferente");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettoAfferente");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfTrasferisciReq");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.TrasferisciReq[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciReq");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciReq");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AssociaDocumentoPrimarioResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "BaseRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.BaseRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CatenaDocumentale");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CatenaDocumentale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Classifica.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Destinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Documento");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Documento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoElettronico");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoElettronico.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoElettronicoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoElettronicoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrimarioRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoPrimarioRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrincipale");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoPrincipale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrincipaleResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoPrincipaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnEsito");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnEsito.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnRelationShipTypes");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnRelationShipTypes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoAcquisizione");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnTipoAcquisizione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoInvio");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnTipoInvio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoOperazione");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnTipoOperazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoSoggetto");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnTipoSoggetto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FingerPrint");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.FingerPrint.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "GeneraRicevutaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.GeneraRicevutaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Metadati");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Metadati.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiDocumento");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.MetadatiDocumento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiDocumentoPrincipale");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.MetadatiDocumentoPrincipale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiProtocollo");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.MetadatiProtocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.MetadatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Mittente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Mittente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaDestinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaDestinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaSoggettoAfferente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "OrganizationalAreaExt");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.OrganizationalAreaExt.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProcessoDestinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProcessoDestinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProcessoDestinatarioResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProcessoDestinatarioResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaAllegaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Protocollo");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Protocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrata");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloEntrata.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloEntrataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloExtResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloExtResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscita");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloUscita.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloUscitaRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistroEmergenza");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RegistroEmergenza.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RelatedDataItem");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RelatedDataItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Response");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SegnaturaAllegata");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SegnaturaAllegata.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettiProtocollo");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SoggettiProtocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Soggetto");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Soggetto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SoggettoAfferente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciReq");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.TrasferisciReq.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.TrasferisciRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.TrasferisciResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Utente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Utente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInEntrata(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, protocolloEntrataRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInUscita(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, protocolloUscitaRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInEntrataEdAllega(it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataEdAllega");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataEdAllega"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {protocolloEntrataRequest, segnaturaPrincipale});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInUscitaEdAllega(it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscitaEdAllega");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaEdAllega"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {protocolloUscitaRequest, segnaturaPrincipale});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ModificaResponse modificaMetadatiProtocollo(java.lang.String segnatura, it.inps.soa.WS00732.data.MetadatiProtocollo metadatiProtocollo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ModificaMetadatiProtocollo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ModificaMetadatiProtocollo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura, metadatiProtocollo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ModificaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ModificaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ModificaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.AnnullaResponse annullaProtocollo(java.lang.String segnatura, it.inps.soa.WS00732.data.AnnullaProtocolloRequest annullaProtocolloRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AnnullaProtocollo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AnnullaProtocollo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura, annullaProtocolloRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.AnnullaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.AnnullaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.AnnullaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.MetadatiResponse estraiMetadatiDocumento(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiMetadatiDocumento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiMetadatiDocumento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.MetadatiResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.MetadatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.MetadatiResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.GeneraRicevutaResponse generaRicevutaProtocollo(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/GeneraRicevutaProtocollo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GeneraRicevutaProtocollo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.GeneraRicevutaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.GeneraRicevutaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.GeneraRicevutaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse associaDocumentoPrimario(it.inps.soa.WS00732.data.DocumentoPrimarioRequest documentoPrimarioRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AssociaDocumentoPrimario");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AssociaDocumentoPrimario"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {documentoPrimarioRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.AllegatiResponse aggiungiAllegatiContestuali(it.inps.soa.WS00732.data.AllegatiContestualiRequest allegatiContestualiRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AggiungiAllegatiContestuali");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AggiungiAllegatiContestuali"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {allegatiContestualiRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.AllegatiResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.AllegatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.AllegatiResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInEntrata(it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/TrasferisciProtocolloInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {trasferisciRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.TrasferisciResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.TrasferisciResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.TrasferisciResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInUscita(it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/TrasferisciProtocolloInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {trasferisciRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.TrasferisciResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.TrasferisciResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.TrasferisciResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse richiestaProtocollazioneInEntrata(it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RichiestaProtocollazioneInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaProtocollazioneInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse richiestaInteroperabilitaInEntrata(it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RichiestaInteroperabilitaInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaInteroperabilitaInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

	@Override
	public AutorizzaRichiestaPECInUscitaResponse autorizzaRichiestaPECInUscita(
			String messageId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreaCatenaDocumentaleResponse creaCatenaDocumentale(
			String segnaturaPadre, String segnaturaFiglia)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreaCatenaFascicolazioneResponse creaCatenaFascicolazione(
			CreaCatenaFascicolazioneRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreaFascicoloProcedimentaleResponse creaFascicoloProcedimentale(
			String codiceAOO, CreaFascicoloProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListaCatenaDocumentaleResponse estraiCatenaDocumentale(
			String segnatura) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CatenaFascicolazioneResponse estraiCatenaFascicolazione(
			String codiceFascicolo) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DocumentiFascicoloResponse estraiDocumentiFascicolo(
			String codiceFascicolo) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListaAllegatiResponse estraiListaAllegati(String segnatura)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListaAllegatiResponse estraiListaCatenaTrasferimenti(String segnatura)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FascicolaDocumentoProtocollatoResponse fascicolaDocumentoProtocollato(
			FascicolaDocumentoProtocollatoRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RicevutaProtocolloBarcodeResponse generaRicevutaProtocolloBarcode(
			String segnatura) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getVersion() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConCatenaDiFascicolazioneProcedimentale(
			String codiceAOO,
			ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressa(
			String codiceAOO,
			ProtocollaInEntrataConFascicolazionePregressaRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressaEVariazioneDiStato(
			String codiceAOO,
			ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConFascicolazioneProcedimentale(
			String codiceAOO,
			ProtocollaInEntrataConFascicolazioneProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaInEntrataESpedisciResponse protocollaInEntrataESpedisci(
			String codiceAOO,
			ProtocolloEntrataESpedisciRequest protocollaInEntrataESpedisciRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaAllegaResponse protocollaInEntrataEdAllegaCrossAOO(
			ProtocolloEntrataRequest protocolloEntrataRequest,
			String segnaturaPrincipale, String AOOdelProtocollo)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaNAIResponse protocollaInEntrataNAI(String codiceAOO,
			ProtocollaInEntrataNAIRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConCatenaDiFascicolazioneProcedimentale(
			String codiceAOO,
			ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressa(
			String codiceAOO,
			ProtocollaInUscitaConFascicolazionePregressaRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressaEVariazioneDiStato(
			String codiceAOO,
			ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConFascicolazioneProcedimentale(
			String codiceAOO,
			ProtocollaInUscitaConFascicolazioneProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaInUscitaESpedisciResponse protocollaInUscitaESpedisci(
			String codiceAOO,
			ProtocolloUscitaESpedisciRequest protocolloUscitaESpedisciRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaAllegaResponse protocollaInUscitaEdAllegaCrossAOO(
			ProtocolloUscitaRequest protocolloUscitaRequest,
			String segnaturaPrincipale, String AOOdelProtocollo)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaPECInUscitaResponse protocollaPECInUscita(String messageId)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RegistraStatoConsegnaPECInUscitaResponse registraStatoConsegnaPECInUscita(
			RegistraStatoConsegnaPECInUscitaRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RegistraStatoInvioPECInUscitaResponse registraStatoInvioPECInUscita(
			RegistraStatoInvioPECInUscitaRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RichiestaCRMResponse richiestaCRM(RichiestaCRMRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RifiutaRichiestaPECInUscitaResponse rifiutaRichiestaPECInUscita(
			String messageId, String motivazione) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariaEsitoProcedimentoResponse variaEsitoProcedimento(
			VariaEsitoProcedimentoRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AssociaContainerAFascicoloResponse associaContainerAFascicolo(
			AssociaContainerAFascicoloRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StatoFascicoloNonProcedimentaleResponse chiudiFascicoloNonProcedimentale(
			StatoFascicoloNonProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContainerResponse creaContainer(String codiceAOO,
			ContainerRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreaFascicoloNonProcedimentaleResponse creaFascicoloNonProcedimentale(
			String codiceAOO, CreaFascicoloNonProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DocumentiFascicoloNonProcedimentaleResponse estraiPratica(
			String codicePratica, String tipoCodicePratica)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DocumentiFascicoloNonProcedimentaleResponse estraiPraticaDaFascicolo(
			String codiceFascicolo) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StatoFascicoloNonProcedimentaleResponse riapriFascicoloNonProcedimentale(
			StatoFascicoloNonProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

}
