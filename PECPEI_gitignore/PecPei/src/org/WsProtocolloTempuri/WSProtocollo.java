/**
 * WSProtocollo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.WsProtocolloTempuri;

public interface WSProtocollo extends javax.xml.rpc.Service {
    public java.lang.String getDNA_BASICHTTP_BindingAddress();

    public it.inps.soa.WS00732.IWSProtocollo getDNA_BASICHTTP_Binding() throws javax.xml.rpc.ServiceException;

    public it.inps.soa.WS00732.IWSProtocollo getDNA_BASICHTTP_Binding(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
