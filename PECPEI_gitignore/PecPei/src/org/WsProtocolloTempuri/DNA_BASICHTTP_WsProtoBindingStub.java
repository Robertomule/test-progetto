/**
 * DNA_BASICHTTP_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.WsProtocolloTempuri;

import java.rmi.RemoteException;

import org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse;

import it.inps.soa.WS00732.data.AssociaContainerAFascicoloRequest;
import it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse;
import it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse;
import it.inps.soa.WS00732.data.CatenaFascicolazioneResponse;
import it.inps.soa.WS00732.data.ContainerRequest;
import it.inps.soa.WS00732.data.ContainerResponse;
import it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse;
import it.inps.soa.WS00732.data.CreaCatenaFascicolazioneRequest;
import it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse;
import it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleRequest;
import it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse;
import it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleRequest;
import it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse;
import it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse;
import it.inps.soa.WS00732.data.DocumentiFascicoloResponse;
import it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoRequest;
import it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse;
import it.inps.soa.WS00732.data.GeneraRicevutaResponse;
import it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse;
import it.inps.soa.WS00732.data.MetadatiProtocollo;
import it.inps.soa.WS00732.data.MetadatiResponse;
import it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse;
import it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse;
import it.inps.soa.WS00732.data.ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest;
import it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest;
import it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaRequest;
import it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazioneProcedimentaleRequest;
import it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse;
import it.inps.soa.WS00732.data.ProtocollaInEntrataNAIRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazioneProcedimentaleRequest;
import it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse;
import it.inps.soa.WS00732.data.ProtocollaNAIResponse;
import it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse;
import it.inps.soa.WS00732.data.ProtocolloEntrataESpedisciRequest;
import it.inps.soa.WS00732.data.ProtocolloUscitaESpedisciRequest;
import it.inps.soa.WS00732.data.RegistraStatoConsegnaPECInUscitaRequest;
import it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaRequest;
import it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse;
import it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse;
import it.inps.soa.WS00732.data.RichiestaCRMRequest;
import it.inps.soa.WS00732.data.RichiestaCRMResponse;
import it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse;
import it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest;
import it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse;
import it.inps.soa.WS00732.data.VariaEsitoProcedimentoRequest;
import it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse;
import it.inps.soa.classi.*;

public class DNA_BASICHTTP_WsProtoBindingStub extends org.apache.axis.client.Stub implements it.inps.soa.WS00732.IWSProtocollo {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[14];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloEntrataRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("", "ProtocolloEntrataRequest"), ProtocolloEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "ProtocollaResponse"));
        oper.setReturnClass(ProtocollaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CodiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloUscitaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("", "ProtocolloUscitaRequest"), ProtocolloUscitaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "ProtocollaResponse"));
        oper.setReturnClass(ProtocollaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataEdAllega");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloEntrataRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("", "ProtocolloEntrataRequest"), ProtocolloEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnaturaPrincipale"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "ProtocollaAllegaResponse"));
        oper.setReturnClass(ProtocollaAllegaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataEdAllegaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscitaEdAllega");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloUscitaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("", "ProtocolloUscitaRequest"), ProtocolloUscitaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnaturaPrincipale"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "ProtocollaAllegaResponse"));
        oper.setReturnClass(ProtocollaAllegaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaEdAllegaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ModificaMetadatiProtocollo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "metadatiProtocollo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.RequestInput", "MetadatiProtocollo"), org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput.MetadatiProtocollo.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "ModificaResponse"));
        oper.setReturnClass(ModificaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ModificaMetadatiProtocolloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AnnullaProtocollo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "annullaProtocolloRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("", "AnnullaProtocolloRequest"), AnnullaProtocolloRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "AnnullaResponse"));
        oper.setReturnClass(AnnullaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AnnullaProtocolloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiMetadatiDocumento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "MetadatiResponse"));
        oper.setReturnClass(MetadatiResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiMetadatiDocumentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GeneraRicevutaProtocollo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "GeneraRicevutaResponse"));
        oper.setReturnClass(GeneraRicevutaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GeneraRicevutaProtocolloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AssociaDocumentoPrimario");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "documentoPrimarioRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("", "DocumentoPrimarioRequest"), DocumentoPrimarioRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "AssociaDocumentoPrimarioResponse"));
        oper.setReturnClass(AssociaDocumentoPrimarioResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AssociaDocumentoPrimarioResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AggiungiAllegatiContestuali");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "allegatiContestualiRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("", "AllegatiContestualiRequest"), AllegatiContestualiRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "AllegatiResponse"));
        oper.setReturnClass(AllegatiResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AggiungiAllegatiContestualiResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TrasferisciProtocolloInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "trasferisciRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("", "TrasferisciRequest"), TrasferisciRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "TrasferisciResponse"));
        oper.setReturnClass(TrasferisciResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TrasferisciProtocolloInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "trasferisciRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("", "TrasferisciRequest"), TrasferisciRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("", "TrasferisciResponse"));
        oper.setReturnClass(TrasferisciResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RichiestaProtocollazioneInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataRequest"), it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaProtocollazioneInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RichiestaInteroperabilitaInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataRequest"), it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaInteroperabilitaInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

    }

    public DNA_BASICHTTP_WsProtoBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public DNA_BASICHTTP_WsProtoBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public DNA_BASICHTTP_WsProtoBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("", "AddressBook");
            cachedSerQNames.add(qName);
            cls = AddressBook.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AllegatiContestualiRequest");
            cachedSerQNames.add(qName);
            cls = AllegatiContestualiRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AllegatiResponse");
            cachedSerQNames.add(qName);
            cls = AllegatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Allegato");
            cachedSerQNames.add(qName);
            cls = Allegato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AllegatoResponse");
            cachedSerQNames.add(qName);
            cls = AllegatoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AltroSoggetto");
            cachedSerQNames.add(qName);
            cls = AltroSoggetto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Annullamento");
            cachedSerQNames.add(qName);
            cls = Annullamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AnnullaProtocolloRequest");
            cachedSerQNames.add(qName);
            cls = AnnullaProtocolloRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AnnullaResponse");
            cachedSerQNames.add(qName);
            cls = AnnullaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Application");
            cachedSerQNames.add(qName);
            cls = Application.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Applicazione");
            cachedSerQNames.add(qName);
            cls = Applicazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ApplicazioniResponse");
            cachedSerQNames.add(qName);
            cls = ApplicazioniResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AreaOrganizzativa");
            cachedSerQNames.add(qName);
            cls = AreaOrganizzativa.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AreeOrganizzativeResponse");
            cachedSerQNames.add(qName);
            cls = AreeOrganizzativeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ArrayOfAllegato");
            cachedSerQNames.add(qName);
            cls = Allegato[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Allegato");
            qName2 = new javax.xml.namespace.QName("", "Allegato");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfAllegatoResponse");
            cachedSerQNames.add(qName);
            cls = AllegatoResponse[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "AllegatoResponse");
            qName2 = new javax.xml.namespace.QName("", "AllegatoResponse");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfAltroSoggetto");
            cachedSerQNames.add(qName);
            cls = AltroSoggetto[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "AltroSoggetto");
            qName2 = new javax.xml.namespace.QName("", "AltroSoggetto");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfApplication");
            cachedSerQNames.add(qName);
            cls = Application[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Application");
            qName2 = new javax.xml.namespace.QName("", "Application");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfAreaOrganizzativa");
            cachedSerQNames.add(qName);
            cls = AreaOrganizzativa[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "AreaOrganizzativa");
            qName2 = new javax.xml.namespace.QName("", "AreaOrganizzativa");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfAzienda");
            cachedSerQNames.add(qName);
            cls = Azienda[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Azienda");
            qName2 = new javax.xml.namespace.QName("", "Azienda");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfClassifica");
            cachedSerQNames.add(qName);
            cls = Classifica[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Classifica");
            qName2 = new javax.xml.namespace.QName("", "Classifica");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfClassificazione");
            cachedSerQNames.add(qName);
            cls = Classificazione[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Classificazione");
            qName2 = new javax.xml.namespace.QName("", "Classificazione");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfDestinatario");
            cachedSerQNames.add(qName);
            cls = Destinatario[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Destinatario");
            qName2 = new javax.xml.namespace.QName("", "Destinatario");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfDocumentoTrovato");
            cachedSerQNames.add(qName);
            cls = DocumentoTrovato[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "DocumentoTrovato");
            qName2 = new javax.xml.namespace.QName("", "DocumentoTrovato");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfEvento");
            cachedSerQNames.add(qName);
            cls = Evento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Evento");
            qName2 = new javax.xml.namespace.QName("", "Evento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfIstituzione");
            cachedSerQNames.add(qName);
            cls = Istituzione[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Istituzione");
            qName2 = new javax.xml.namespace.QName("", "Istituzione");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfMetadati");
            cachedSerQNames.add(qName);
            cls = Metadati[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Metadati");
            qName2 = new javax.xml.namespace.QName("", "Metadati");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfMetadatiPatrocinato");
            cachedSerQNames.add(qName);
            cls = MetadatiPatrocinato[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "MetadatiPatrocinato");
            qName2 = new javax.xml.namespace.QName("", "MetadatiPatrocinato");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfModificaDestinatario");
            cachedSerQNames.add(qName);
            cls = ModificaDestinatario[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "ModificaDestinatario");
            qName2 = new javax.xml.namespace.QName("", "ModificaDestinatario");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfModificaSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = ModificaSoggettoAfferente[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "ModificaSoggettoAfferente");
            qName2 = new javax.xml.namespace.QName("", "ModificaSoggettoAfferente");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfNomeStatoAvanzamento");
            cachedSerQNames.add(qName);
            cls = NomeStatoAvanzamento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "NomeStatoAvanzamento");
            qName2 = new javax.xml.namespace.QName("", "NomeStatoAvanzamento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfOperazione");
            cachedSerQNames.add(qName);
            cls = Operazione[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Operazione");
            qName2 = new javax.xml.namespace.QName("", "Operazione");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfOrganizzazione");
            cachedSerQNames.add(qName);
            cls = Organizzazione[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Organizzazione");
            qName2 = new javax.xml.namespace.QName("", "Organizzazione");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfPatronato");
            cachedSerQNames.add(qName);
            cls = Patronato[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Patronato");
            qName2 = new javax.xml.namespace.QName("", "Patronato");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfPersonaFisica");
            cachedSerQNames.add(qName);
            cls = PersonaFisica[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "PersonaFisica");
            qName2 = new javax.xml.namespace.QName("", "PersonaFisica");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfPoloSpecializzato");
            cachedSerQNames.add(qName);
            cls = PoloSpecializzato[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "PoloSpecializzato");
            qName2 = new javax.xml.namespace.QName("", "PoloSpecializzato");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfPrincipal");
            cachedSerQNames.add(qName);
            cls = Principal[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Principal");
            qName2 = new javax.xml.namespace.QName("", "Principal");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfPrincipalDetail");
            cachedSerQNames.add(qName);
            cls = PrincipalDetail[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "PrincipalDetail");
            qName2 = new javax.xml.namespace.QName("", "PrincipalDetail");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfProcesso");
            cachedSerQNames.add(qName);
            cls = Processo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Processo");
            qName2 = new javax.xml.namespace.QName("", "Processo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfProtocolloEntrata");
            cachedSerQNames.add(qName);
            cls = ProtocolloEntrata[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "ProtocolloEntrata");
            qName2 = new javax.xml.namespace.QName("", "ProtocolloEntrata");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfProtocolloUscita");
            cachedSerQNames.add(qName);
            cls = ProtocolloUscita[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "ProtocolloUscita");
            qName2 = new javax.xml.namespace.QName("", "ProtocolloUscita");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfRegistro");
            cachedSerQNames.add(qName);
            cls = Registro[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Registro");
            qName2 = new javax.xml.namespace.QName("", "Registro");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfRuolo");
            cachedSerQNames.add(qName);
            cls = Ruolo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Ruolo");
            qName2 = new javax.xml.namespace.QName("", "Ruolo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfSegnaturaAllegata");
            cachedSerQNames.add(qName);
            cls = SegnaturaAllegata[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "SegnaturaAllegata");
            qName2 = new javax.xml.namespace.QName("", "SegnaturaAllegata");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = SoggettoAfferente[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "SoggettoAfferente");
            qName2 = new javax.xml.namespace.QName("", "SoggettoAfferente");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfStatoAvanzamento");
            cachedSerQNames.add(qName);
            cls = StatoAvanzamento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "StatoAvanzamento");
            qName2 = new javax.xml.namespace.QName("", "StatoAvanzamento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfStatoAvanzamentoMap");
            cachedSerQNames.add(qName);
            cls = StatoAvanzamentoMap[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "StatoAvanzamentoMap");
            qName2 = new javax.xml.namespace.QName("", "StatoAvanzamentoMap");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfTitolarioPEIPEC");
            cachedSerQNames.add(qName);
            cls = TitolarioPEIPEC[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "TitolarioPEIPEC");
            qName2 = new javax.xml.namespace.QName("", "TitolarioPEIPEC");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfTrasferisciReq");
            cachedSerQNames.add(qName);
            cls = TrasferisciReq[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "TrasferisciReq");
            qName2 = new javax.xml.namespace.QName("", "TrasferisciReq");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfUnitaOrganizzativa");
            cachedSerQNames.add(qName);
            cls = UnitaOrganizzativa[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "UnitaOrganizzativa");
            qName2 = new javax.xml.namespace.QName("", "UnitaOrganizzativa");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfUserExt");
            cachedSerQNames.add(qName);
            cls = UserExt[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "UserExt");
            qName2 = new javax.xml.namespace.QName("", "UserExt");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfUt");
            cachedSerQNames.add(qName);
            cls = Ut[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Ut");
            qName2 = new javax.xml.namespace.QName("", "Ut");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "AssociaDocumentoPrimarioResponse");
            cachedSerQNames.add(qName);
            cls = AssociaDocumentoPrimarioResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Azienda");
            cachedSerQNames.add(qName);
            cls = Azienda.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "CategoriaProtocolloResponse");
            cachedSerQNames.add(qName);
            cls = CategoriaProtocolloResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "CatenaDocumentale");
            cachedSerQNames.add(qName);
            cls = CatenaDocumentale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Classifica");
            cachedSerQNames.add(qName);
            cls = Classifica.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ClassificationSchemas");
            cachedSerQNames.add(qName);
            cls = ClassificationSchemas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Classificazione");
            cachedSerQNames.add(qName);
            cls = Classificazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "CompletaMetadatiFatturaElettronicaResponse");
            cachedSerQNames.add(qName);
            cls = CompletaMetadatiFatturaElettronicaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DatiProtocollo");
            cachedSerQNames.add(qName);
            cls = DatiProtocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DescrStatiResponse");
            cachedSerQNames.add(qName);
            cls = DescrStatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DescrStatoResponse");
            cachedSerQNames.add(qName);
            cls = DescrStatoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Destinatario");
            cachedSerQNames.add(qName);
            cls = Destinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentiElettroniciContestualiResponse");
            cachedSerQNames.add(qName);
            cls = DocumentiElettroniciContestualiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Documento");
            cachedSerQNames.add(qName);
            cls = Documento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoElettronico");
            cachedSerQNames.add(qName);
            cls = DocumentoElettronico.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoElettronicoPrimarioResponse");
            cachedSerQNames.add(qName);
            cls = DocumentoElettronicoPrimarioResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoElettronicoResponse");
            cachedSerQNames.add(qName);
            cls = DocumentoElettronicoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoPrimarioRequest");
            cachedSerQNames.add(qName);
            cls = DocumentoPrimarioRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoPrincipale");
            cachedSerQNames.add(qName);
            cls = DocumentoPrincipale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoPrincipaleResponse");
            cachedSerQNames.add(qName);
            cls = DocumentoPrincipaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoResponse");
            cachedSerQNames.add(qName);
            cls = DocumentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoTrovato");
            cachedSerQNames.add(qName);
            cls = DocumentoTrovato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "EsisteRicevutaResponse");
            cachedSerQNames.add(qName);
            cls = EsisteRicevutaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "EventiResponse");
            cachedSerQNames.add(qName);
            cls = EventiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Evento");
            cachedSerQNames.add(qName);
            cls = Evento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "FingerPrint");
            cachedSerQNames.add(qName);
            cls = FingerPrint.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "GeneraRicevutaResponse");
            cachedSerQNames.add(qName);
            cls = GeneraRicevutaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Istituzione");
            cachedSerQNames.add(qName);
            cls = Istituzione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ListaAllegatiResponse");
            cachedSerQNames.add(qName);
            cls = ListaAllegatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Metadati");
            cachedSerQNames.add(qName);
            cls = Metadati.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "MetadatiPatrocinato");
            cachedSerQNames.add(qName);
            cls = MetadatiPatrocinato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "MetadatiProtocolliPatrocinatiResponse");
            cachedSerQNames.add(qName);
            cls = MetadatiProtocolliPatrocinatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "MetadatiResponse");
            cachedSerQNames.add(qName);
            cls = MetadatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Mittente");
            cachedSerQNames.add(qName);
            cls = Mittente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ModificaDestinatario");
            cachedSerQNames.add(qName);
            cls = ModificaDestinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ModificaResponse");
            cachedSerQNames.add(qName);
            cls = ModificaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ModificaSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = ModificaSoggettoAfferente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "NomeStatoAvanzamento");
            cachedSerQNames.add(qName);
            cls = NomeStatoAvanzamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "NotificaInteropEntrataResponse");
            cachedSerQNames.add(qName);
            cls = NotificaInteropEntrataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "NotificaInteropUscitaResponse");
            cachedSerQNames.add(qName);
            cls = NotificaInteropUscitaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Operazione");
            cachedSerQNames.add(qName);
            cls = Operazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "OperazioniResponse");
            cachedSerQNames.add(qName);
            cls = OperazioniResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "OrganizationalAreaExt");
            cachedSerQNames.add(qName);
            cls = OrganizationalAreaExt.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "OrganizationalAreaPlus");
            cachedSerQNames.add(qName);
            cls = OrganizationalAreaPlus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Organizzazione");
            cachedSerQNames.add(qName);
            cls = Organizzazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "OrganizzazioniResponse");
            cachedSerQNames.add(qName);
            cls = OrganizzazioniResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Patronato");
            cachedSerQNames.add(qName);
            cls = Patronato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "PersonaFisica");
            cachedSerQNames.add(qName);
            cls = PersonaFisica.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("", "PianoDiClassificazione");
            cachedSerQNames.add(qName);
            cls = PianoDiClassificazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "PoliSpecializzatiResponse");
            cachedSerQNames.add(qName);
            cls = PoliSpecializzatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "PoloSpecializzato");
            cachedSerQNames.add(qName);
            cls = PoloSpecializzato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Principal");
            cachedSerQNames.add(qName);
            cls = Principal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "PrincipalDetail");
            cachedSerQNames.add(qName);
            cls = PrincipalDetail.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "PrincipalDetailsResponse");
            cachedSerQNames.add(qName);
            cls = PrincipalDetailsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "PrincipalResponse");
            cachedSerQNames.add(qName);
            cls = PrincipalResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProcessiResponse");
            cachedSerQNames.add(qName);
            cls = ProcessiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Processo");
            cachedSerQNames.add(qName);
            cls = Processo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProcessoDestinatario");
            cachedSerQNames.add(qName);
            cls = ProcessoDestinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProcessoDestinatarioResponse");
            cachedSerQNames.add(qName);
            cls = ProcessoDestinatarioResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocollaAllegaResponse");
            cachedSerQNames.add(qName);
            cls = ProtocollaAllegaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocollaResponse");
            cachedSerQNames.add(qName);
            cls = ProtocollaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Protocollo");
            cachedSerQNames.add(qName);
            cls = Protocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloEntrata");
            cachedSerQNames.add(qName);
            cls = ProtocolloEntrata.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloEntrataRequest");
            cachedSerQNames.add(qName);
            cls = ProtocolloEntrataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloExt");
            cachedSerQNames.add(qName);
            cls = ProtocolloExt.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloExtResponse");
            cachedSerQNames.add(qName);
            cls = ProtocolloExtResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloResponse");
            cachedSerQNames.add(qName);
            cls = ProtocolloResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloUscita");
            cachedSerQNames.add(qName);
            cls = ProtocolloUscita.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloUscitaRequest");
            cachedSerQNames.add(qName);
            cls = ProtocolloUscitaRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Registro");
            cachedSerQNames.add(qName);
            cls = Registro.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RegistroEmergenza");
            cachedSerQNames.add(qName);
            cls = RegistroEmergenza.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RegistroProtocolloResponse");
            cachedSerQNames.add(qName);
            cls = RegistroProtocolloResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RegistroResponse");
            cachedSerQNames.add(qName);
            cls = RegistroResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Response");
            cachedSerQNames.add(qName);
            cls = Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ResponseBarcode");
            cachedSerQNames.add(qName);
            cls = ResponseBarcode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RicercaDocumentiProtocollatiConPaginazioneResponse");
            cachedSerQNames.add(qName);
            cls = RicercaDocumentiProtocollatiConPaginazioneResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RicercaDocumentiProtocollatiResponse");
            cachedSerQNames.add(qName);
            cls = RicercaDocumentiProtocollatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RichiestaInteroperabilitaEntrataResponse");
            cachedSerQNames.add(qName);
            cls = RichiestaInteroperabilitaEntrataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RichiestaProtocollazioneEntrataResponse");
            cachedSerQNames.add(qName);
            cls = RichiestaProtocollazioneEntrataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RubricaResponse");
            cachedSerQNames.add(qName);
            cls = RubricaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RuoliResponse");
            cachedSerQNames.add(qName);
            cls = RuoliResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Ruolo");
            cachedSerQNames.add(qName);
            cls = Ruolo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "SegnaturaAllegata");
            cachedSerQNames.add(qName);
            cls = SegnaturaAllegata.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "SetStatoResponse");
            cachedSerQNames.add(qName);
            cls = SetStatoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Soggetto");
            cachedSerQNames.add(qName);
            cls = Soggetto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "SoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = SoggettoAfferente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "StatiAvanzamentoMappingResponse");
            cachedSerQNames.add(qName);
            cls = StatiAvanzamentoMappingResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "StatiAvanzamentoResponse");
            cachedSerQNames.add(qName);
            cls = StatiAvanzamentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "StatoAvanzamento");
            cachedSerQNames.add(qName);
            cls = StatoAvanzamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "StatoAvanzamentoMap");
            cachedSerQNames.add(qName);
            cls = StatoAvanzamentoMap.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "StatoAvanzamentoResponse");
            cachedSerQNames.add(qName);
            cls = StatoAvanzamentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "TitolarioPEIPEC");
            cachedSerQNames.add(qName);
            cls = TitolarioPEIPEC.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "TitolarioResponse");
            cachedSerQNames.add(qName);
            cls = TitolarioResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "TrasferisciReq");
            cachedSerQNames.add(qName);
            cls = TrasferisciReq.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "TrasferisciRequest");
            cachedSerQNames.add(qName);
            cls = TrasferisciRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "TrasferisciResponse");
            cachedSerQNames.add(qName);
            cls = TrasferisciResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "UnitaOrganizzativa");
            cachedSerQNames.add(qName);
            cls = UnitaOrganizzativa.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "UnitaOrganizzativeResponse");
            cachedSerQNames.add(qName);
            cls = UnitaOrganizzativeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "UserExt");
            cachedSerQNames.add(qName);
            cls = UserExt.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Ut");
            cachedSerQNames.add(qName);
            cls = Ut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Utente");
            cachedSerQNames.add(qName);
            cls = Utente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "UtentiResponse");
            cachedSerQNames.add(qName);
            cls = UtentiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.BO.Common.Serializzazione", "Serializer");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_BO_Common_Serializzazione.Serializer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.BO.Soggetti", "SoggettiProtocollo");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_BO_Soggetti.SoggettiProtocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnEsito");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnProtocolType");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnProtocolType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnRelationShipTypes");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoAcquisizione");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoAcquisizione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoInvio");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoInvio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoOperazione");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoOperazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoSoggetto");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoSoggetto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.RequestInput", "MetadatiDocumento");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput.MetadatiDocumento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.RequestInput", "MetadatiDocumentoPrincipale");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput.MetadatiDocumentoPrincipale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.RequestInput", "MetadatiProtocollo");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput.MetadatiProtocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
            cachedSerQNames.add(qName);
            cls = org.apache.axis.types.Duration.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfRelatedDataItem");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RelatedDataItem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RelatedDataItem");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RelatedDataItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "BaseRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.BaseRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "BaseResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.BaseResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EsitoRichiesta");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EsitoRichiesta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RelatedDataItem");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RelatedDataItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public ProtocollaResponse protocollaInEntrata(java.lang.String codiceAOO, ProtocolloEntrataRequest protocolloEntrataRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, protocolloEntrataRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ProtocollaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (ProtocollaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, ProtocollaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ProtocollaResponse protocollaInUscita(java.lang.String codiceAOO, ProtocolloUscitaRequest protocolloUscitaRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, protocolloUscitaRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ProtocollaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (ProtocollaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, ProtocollaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ProtocollaAllegaResponse protocollaInEntrataEdAllega(ProtocolloEntrataRequest protocolloEntrataRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataEdAllega");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataEdAllega"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {protocolloEntrataRequest, segnaturaPrincipale});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ProtocollaAllegaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (ProtocollaAllegaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, ProtocollaAllegaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ProtocollaAllegaResponse protocollaInUscitaEdAllega(ProtocolloUscitaRequest protocolloUscitaRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscitaEdAllega");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaEdAllega"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {protocolloUscitaRequest, segnaturaPrincipale});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ProtocollaAllegaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (ProtocollaAllegaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, ProtocollaAllegaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ModificaResponse modificaMetadatiProtocollo(java.lang.String segnatura, org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput.MetadatiProtocollo metadatiProtocollo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ModificaMetadatiProtocollo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ModificaMetadatiProtocollo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura, metadatiProtocollo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ModificaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (ModificaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, ModificaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public AnnullaResponse annullaProtocollo(java.lang.String segnatura, AnnullaProtocolloRequest annullaProtocolloRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AnnullaProtocollo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AnnullaProtocollo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura, annullaProtocolloRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (AnnullaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (AnnullaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, AnnullaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public MetadatiResponse estraiMetadatiDocumento(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiMetadatiDocumento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiMetadatiDocumento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (MetadatiResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (MetadatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, MetadatiResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public GeneraRicevutaResponse generaRicevutaProtocollo(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/GeneraRicevutaProtocollo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GeneraRicevutaProtocollo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (GeneraRicevutaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (GeneraRicevutaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, GeneraRicevutaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public AssociaDocumentoPrimarioResponse associaDocumentoPrimario(DocumentoPrimarioRequest documentoPrimarioRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AssociaDocumentoPrimario");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AssociaDocumentoPrimario"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {documentoPrimarioRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (AssociaDocumentoPrimarioResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (AssociaDocumentoPrimarioResponse) org.apache.axis.utils.JavaUtils.convert(_resp, AssociaDocumentoPrimarioResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public AllegatiResponse aggiungiAllegatiContestuali(AllegatiContestualiRequest allegatiContestualiRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AggiungiAllegatiContestuali");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AggiungiAllegatiContestuali"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {allegatiContestualiRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (AllegatiResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (AllegatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, AllegatiResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public TrasferisciResponse trasferisciProtocolloInEntrata(TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/TrasferisciProtocolloInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {trasferisciRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (TrasferisciResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (TrasferisciResponse) org.apache.axis.utils.JavaUtils.convert(_resp, TrasferisciResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public TrasferisciResponse trasferisciProtocolloInUscita(TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/TrasferisciProtocolloInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {trasferisciRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (TrasferisciResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (TrasferisciResponse) org.apache.axis.utils.JavaUtils.convert(_resp, TrasferisciResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse richiestaProtocollazioneInEntrata(it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RichiestaProtocollazioneInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaProtocollazioneInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse richiestaInteroperabilitaInEntrata(it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RichiestaInteroperabilitaInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaInteroperabilitaInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

	@Override
	public it.inps.soa.WS00732.data.AllegatiResponse aggiungiAllegatiContestuali(
			it.inps.soa.WS00732.data.AllegatiContestualiRequest allegatiContestualiRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.AnnullaResponse annullaProtocollo(
			String segnatura,
			it.inps.soa.WS00732.data.AnnullaProtocolloRequest annullaProtocolloRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse associaDocumentoPrimario(
			it.inps.soa.WS00732.data.DocumentoPrimarioRequest documentoPrimarioRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.ModificaResponse modificaMetadatiProtocollo(
			String segnatura, MetadatiProtocollo metadatiProtocollo)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInEntrata(
			String codiceAOO,
			it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInEntrataEdAllega(
			it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest,
			String segnaturaPrincipale) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInUscita(
			String codiceAOO,
			it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInUscitaEdAllega(
			it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest,
			String segnaturaPrincipale) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInEntrata(
			it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInUscita(
			it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AutorizzaRichiestaPECInUscitaResponse autorizzaRichiestaPECInUscita(
			String messageId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreaCatenaDocumentaleResponse creaCatenaDocumentale(
			String segnaturaPadre, String segnaturaFiglia)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreaCatenaFascicolazioneResponse creaCatenaFascicolazione(
			CreaCatenaFascicolazioneRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreaFascicoloProcedimentaleResponse creaFascicoloProcedimentale(
			String codiceAOO, CreaFascicoloProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListaCatenaDocumentaleResponse estraiCatenaDocumentale(
			String segnatura) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CatenaFascicolazioneResponse estraiCatenaFascicolazione(
			String codiceFascicolo) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DocumentiFascicoloResponse estraiDocumentiFascicolo(
			String codiceFascicolo) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.ListaAllegatiResponse estraiListaAllegati(
			String segnatura) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.ListaAllegatiResponse estraiListaCatenaTrasferimenti(
			String segnatura) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FascicolaDocumentoProtocollatoResponse fascicolaDocumentoProtocollato(
			FascicolaDocumentoProtocollatoRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RicevutaProtocolloBarcodeResponse generaRicevutaProtocolloBarcode(
			String segnatura) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getVersion() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConCatenaDiFascicolazioneProcedimentale(
			String codiceAOO,
			ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressa(
			String codiceAOO,
			ProtocollaInEntrataConFascicolazionePregressaRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressaEVariazioneDiStato(
			String codiceAOO,
			ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConFascicolazioneProcedimentale(
			String codiceAOO,
			ProtocollaInEntrataConFascicolazioneProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaInEntrataESpedisciResponse protocollaInEntrataESpedisci(
			String codiceAOO,
			ProtocolloEntrataESpedisciRequest protocollaInEntrataESpedisciRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInEntrataEdAllegaCrossAOO(
			it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest,
			String segnaturaPrincipale, String AOOdelProtocollo)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaNAIResponse protocollaInEntrataNAI(String codiceAOO,
			ProtocollaInEntrataNAIRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConCatenaDiFascicolazioneProcedimentale(
			String codiceAOO,
			ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressa(
			String codiceAOO,
			ProtocollaInUscitaConFascicolazionePregressaRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressaEVariazioneDiStato(
			String codiceAOO,
			ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConFascicolazioneProcedimentale(
			String codiceAOO,
			ProtocollaInUscitaConFascicolazioneProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaInUscitaESpedisciResponse protocollaInUscitaESpedisci(
			String codiceAOO,
			ProtocolloUscitaESpedisciRequest protocolloUscitaESpedisciRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInUscitaEdAllegaCrossAOO(
			it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest,
			String segnaturaPrincipale, String AOOdelProtocollo)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProtocollaPECInUscitaResponse protocollaPECInUscita(String messageId)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RegistraStatoConsegnaPECInUscitaResponse registraStatoConsegnaPECInUscita(
			RegistraStatoConsegnaPECInUscitaRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RegistraStatoInvioPECInUscitaResponse registraStatoInvioPECInUscita(
			RegistraStatoInvioPECInUscitaRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RichiestaCRMResponse richiestaCRM(RichiestaCRMRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RifiutaRichiestaPECInUscitaResponse rifiutaRichiestaPECInUscita(
			String messageId, String motivazione) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariaEsitoProcedimentoResponse variaEsitoProcedimento(
			VariaEsitoProcedimentoRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AssociaContainerAFascicoloResponse associaContainerAFascicolo(
			AssociaContainerAFascicoloRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StatoFascicoloNonProcedimentaleResponse chiudiFascicoloNonProcedimentale(
			StatoFascicoloNonProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContainerResponse creaContainer(String codiceAOO,
			ContainerRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CreaFascicoloNonProcedimentaleResponse creaFascicoloNonProcedimentale(
			String codiceAOO, CreaFascicoloNonProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DocumentiFascicoloNonProcedimentaleResponse estraiPratica(
			String codicePratica, String tipoCodicePratica)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DocumentiFascicoloNonProcedimentaleResponse estraiPraticaDaFascicolo(
			String codiceFascicolo) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StatoFascicoloNonProcedimentaleResponse riapriFascicoloNonProcedimentale(
			StatoFascicoloNonProcedimentaleRequest request)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

}
