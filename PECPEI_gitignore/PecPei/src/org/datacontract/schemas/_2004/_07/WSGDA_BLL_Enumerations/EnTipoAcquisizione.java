/**
 * EnTipoAcquisizione.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations;

public class EnTipoAcquisizione implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EnTipoAcquisizione(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _A = "A";
    public static final java.lang.String _S = "S";
    public static final java.lang.String _F = "F";
    public static final java.lang.String _C = "C";
    public static final java.lang.String _X = "X";
    public static final java.lang.String _G = "G";
    public static final java.lang.String _E = "E";
    public static final java.lang.String _R = "R";
    public static final EnTipoAcquisizione A = new EnTipoAcquisizione(_A);
    public static final EnTipoAcquisizione S = new EnTipoAcquisizione(_S);
    public static final EnTipoAcquisizione F = new EnTipoAcquisizione(_F);
    public static final EnTipoAcquisizione C = new EnTipoAcquisizione(_C);
    public static final EnTipoAcquisizione X = new EnTipoAcquisizione(_X);
    public static final EnTipoAcquisizione G = new EnTipoAcquisizione(_G);
    public static final EnTipoAcquisizione E = new EnTipoAcquisizione(_E);
    public static final EnTipoAcquisizione R = new EnTipoAcquisizione(_R);
    public java.lang.String getValue() { return _value_;}
    public static EnTipoAcquisizione fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EnTipoAcquisizione enumeration = (EnTipoAcquisizione)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EnTipoAcquisizione fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EnTipoAcquisizione.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoAcquisizione"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
