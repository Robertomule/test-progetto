/**
 * DatiInvio.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests;

public class DatiInvio  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.EnCategoriaProtocollo categoriaProtocollo;

    private org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Mittente mittente;

    private org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Destinatario[] listaDestinatari;

    private org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Documento[] listaDocumenti;

    public DatiInvio() {
    }

    public DatiInvio(
           it.inps.soa.WS00732.data.EnCategoriaProtocollo categoriaProtocollo,
           org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Mittente mittente,
           org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Destinatario[] listaDestinatari,
           org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Documento[] listaDocumenti) {
           this.categoriaProtocollo = categoriaProtocollo;
           this.mittente = mittente;
           this.listaDestinatari = listaDestinatari;
           this.listaDocumenti = listaDocumenti;
    }


    /**
     * Gets the categoriaProtocollo value for this DatiInvio.
     * 
     * @return categoriaProtocollo
     */
    public it.inps.soa.WS00732.data.EnCategoriaProtocollo getCategoriaProtocollo() {
        return categoriaProtocollo;
    }


    /**
     * Sets the categoriaProtocollo value for this DatiInvio.
     * 
     * @param categoriaProtocollo
     */
    public void setCategoriaProtocollo(it.inps.soa.WS00732.data.EnCategoriaProtocollo categoriaProtocollo) {
        this.categoriaProtocollo = categoriaProtocollo;
    }


    /**
     * Gets the mittente value for this DatiInvio.
     * 
     * @return mittente
     */
    public org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Mittente getMittente() {
        return mittente;
    }


    /**
     * Sets the mittente value for this DatiInvio.
     * 
     * @param mittente
     */
    public void setMittente(org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Mittente mittente) {
        this.mittente = mittente;
    }


    /**
     * Gets the listaDestinatari value for this DatiInvio.
     * 
     * @return listaDestinatari
     */
    public org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Destinatario[] getListaDestinatari() {
        return listaDestinatari;
    }


    /**
     * Sets the listaDestinatari value for this DatiInvio.
     * 
     * @param listaDestinatari
     */
    public void setListaDestinatari(org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Destinatario[] listaDestinatari) {
        this.listaDestinatari = listaDestinatari;
    }


    /**
     * Gets the listaDocumenti value for this DatiInvio.
     * 
     * @return listaDocumenti
     */
    public org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Documento[] getListaDocumenti() {
        return listaDocumenti;
    }


    /**
     * Sets the listaDocumenti value for this DatiInvio.
     * 
     * @param listaDocumenti
     */
    public void setListaDocumenti(org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Documento[] listaDocumenti) {
        this.listaDocumenti = listaDocumenti;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DatiInvio)) return false;
        DatiInvio other = (DatiInvio) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.categoriaProtocollo==null && other.getCategoriaProtocollo()==null) || 
             (this.categoriaProtocollo!=null &&
              this.categoriaProtocollo.equals(other.getCategoriaProtocollo()))) &&
            ((this.mittente==null && other.getMittente()==null) || 
             (this.mittente!=null &&
              this.mittente.equals(other.getMittente()))) &&
            ((this.listaDestinatari==null && other.getListaDestinatari()==null) || 
             (this.listaDestinatari!=null &&
              java.util.Arrays.equals(this.listaDestinatari, other.getListaDestinatari()))) &&
            ((this.listaDocumenti==null && other.getListaDocumenti()==null) || 
             (this.listaDocumenti!=null &&
              java.util.Arrays.equals(this.listaDocumenti, other.getListaDocumenti())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCategoriaProtocollo() != null) {
            _hashCode += getCategoriaProtocollo().hashCode();
        }
        if (getMittente() != null) {
            _hashCode += getMittente().hashCode();
        }
        if (getListaDestinatari() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListaDestinatari());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListaDestinatari(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getListaDocumenti() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListaDocumenti());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListaDocumenti(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DatiInvio.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "DatiInvio"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoriaProtocollo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "CategoriaProtocollo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnCategoriaProtocollo"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mittente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Mittente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Mittente"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listaDestinatari");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "ListaDestinatari"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Destinatario"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Destinatario"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listaDocumenti");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "ListaDocumenti"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Documento"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Documento"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
