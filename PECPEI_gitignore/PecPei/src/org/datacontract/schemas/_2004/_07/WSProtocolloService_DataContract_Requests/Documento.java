/**
 * Documento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests;

public class Documento  implements java.io.Serializable {
    private java.lang.String classifica;

    private java.lang.String dataArrivo;

    private org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.SoggettoAfferente[] listaSoggettiAfferenti;

    private java.lang.String oggetto;

    private java.lang.String ownerAppl;

    public Documento() {
    }

    public Documento(
           java.lang.String classifica,
           java.lang.String dataArrivo,
           org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.SoggettoAfferente[] listaSoggettiAfferenti,
           java.lang.String oggetto,
           java.lang.String ownerAppl) {
           this.classifica = classifica;
           this.dataArrivo = dataArrivo;
           this.listaSoggettiAfferenti = listaSoggettiAfferenti;
           this.oggetto = oggetto;
           this.ownerAppl = ownerAppl;
    }


    /**
     * Gets the classifica value for this Documento.
     * 
     * @return classifica
     */
    public java.lang.String getClassifica() {
        return classifica;
    }


    /**
     * Sets the classifica value for this Documento.
     * 
     * @param classifica
     */
    public void setClassifica(java.lang.String classifica) {
        this.classifica = classifica;
    }


    /**
     * Gets the dataArrivo value for this Documento.
     * 
     * @return dataArrivo
     */
    public java.lang.String getDataArrivo() {
        return dataArrivo;
    }


    /**
     * Sets the dataArrivo value for this Documento.
     * 
     * @param dataArrivo
     */
    public void setDataArrivo(java.lang.String dataArrivo) {
        this.dataArrivo = dataArrivo;
    }


    /**
     * Gets the listaSoggettiAfferenti value for this Documento.
     * 
     * @return listaSoggettiAfferenti
     */
    public org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.SoggettoAfferente[] getListaSoggettiAfferenti() {
        return listaSoggettiAfferenti;
    }


    /**
     * Sets the listaSoggettiAfferenti value for this Documento.
     * 
     * @param listaSoggettiAfferenti
     */
    public void setListaSoggettiAfferenti(org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.SoggettoAfferente[] listaSoggettiAfferenti) {
        this.listaSoggettiAfferenti = listaSoggettiAfferenti;
    }


    /**
     * Gets the oggetto value for this Documento.
     * 
     * @return oggetto
     */
    public java.lang.String getOggetto() {
        return oggetto;
    }


    /**
     * Sets the oggetto value for this Documento.
     * 
     * @param oggetto
     */
    public void setOggetto(java.lang.String oggetto) {
        this.oggetto = oggetto;
    }


    /**
     * Gets the ownerAppl value for this Documento.
     * 
     * @return ownerAppl
     */
    public java.lang.String getOwnerAppl() {
        return ownerAppl;
    }


    /**
     * Sets the ownerAppl value for this Documento.
     * 
     * @param ownerAppl
     */
    public void setOwnerAppl(java.lang.String ownerAppl) {
        this.ownerAppl = ownerAppl;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Documento)) return false;
        Documento other = (Documento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.classifica==null && other.getClassifica()==null) || 
             (this.classifica!=null &&
              this.classifica.equals(other.getClassifica()))) &&
            ((this.dataArrivo==null && other.getDataArrivo()==null) || 
             (this.dataArrivo!=null &&
              this.dataArrivo.equals(other.getDataArrivo()))) &&
            ((this.listaSoggettiAfferenti==null && other.getListaSoggettiAfferenti()==null) || 
             (this.listaSoggettiAfferenti!=null &&
              java.util.Arrays.equals(this.listaSoggettiAfferenti, other.getListaSoggettiAfferenti()))) &&
            ((this.oggetto==null && other.getOggetto()==null) || 
             (this.oggetto!=null &&
              this.oggetto.equals(other.getOggetto()))) &&
            ((this.ownerAppl==null && other.getOwnerAppl()==null) || 
             (this.ownerAppl!=null &&
              this.ownerAppl.equals(other.getOwnerAppl())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClassifica() != null) {
            _hashCode += getClassifica().hashCode();
        }
        if (getDataArrivo() != null) {
            _hashCode += getDataArrivo().hashCode();
        }
        if (getListaSoggettiAfferenti() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListaSoggettiAfferenti());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListaSoggettiAfferenti(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOggetto() != null) {
            _hashCode += getOggetto().hashCode();
        }
        if (getOwnerAppl() != null) {
            _hashCode += getOwnerAppl().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Documento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Documento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classifica");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Classifica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataArrivo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "DataArrivo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listaSoggettiAfferenti");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "ListaSoggettiAfferenti"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "SoggettoAfferente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "SoggettoAfferente"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oggetto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Oggetto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerAppl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "OwnerAppl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
