/**
 * MetadatiProtocollo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput;

public class MetadatiProtocollo  implements java.io.Serializable {
    private java.util.Calendar dataArrivo;

    private org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput.MetadatiDocumentoPrincipale documentoPrincipale;

    private org.datacontract.schemas._2004._07.WSGDA_BLL_BO_Soggetti.SoggettiProtocollo soggettiProtocollo;

    public MetadatiProtocollo() {
    }

    public MetadatiProtocollo(
           java.util.Calendar dataArrivo,
           org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput.MetadatiDocumentoPrincipale documentoPrincipale,
           org.datacontract.schemas._2004._07.WSGDA_BLL_BO_Soggetti.SoggettiProtocollo soggettiProtocollo) {
           this.dataArrivo = dataArrivo;
           this.documentoPrincipale = documentoPrincipale;
           this.soggettiProtocollo = soggettiProtocollo;
    }


    /**
     * Gets the dataArrivo value for this MetadatiProtocollo.
     * 
     * @return dataArrivo
     */
    public java.util.Calendar getDataArrivo() {
        return dataArrivo;
    }


    /**
     * Sets the dataArrivo value for this MetadatiProtocollo.
     * 
     * @param dataArrivo
     */
    public void setDataArrivo(java.util.Calendar dataArrivo) {
        this.dataArrivo = dataArrivo;
    }


    /**
     * Gets the documentoPrincipale value for this MetadatiProtocollo.
     * 
     * @return documentoPrincipale
     */
    public org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput.MetadatiDocumentoPrincipale getDocumentoPrincipale() {
        return documentoPrincipale;
    }


    /**
     * Sets the documentoPrincipale value for this MetadatiProtocollo.
     * 
     * @param documentoPrincipale
     */
    public void setDocumentoPrincipale(org.datacontract.schemas._2004._07.WSGDA_BLL_RequestInput.MetadatiDocumentoPrincipale documentoPrincipale) {
        this.documentoPrincipale = documentoPrincipale;
    }


    /**
     * Gets the soggettiProtocollo value for this MetadatiProtocollo.
     * 
     * @return soggettiProtocollo
     */
    public org.datacontract.schemas._2004._07.WSGDA_BLL_BO_Soggetti.SoggettiProtocollo getSoggettiProtocollo() {
        return soggettiProtocollo;
    }


    /**
     * Sets the soggettiProtocollo value for this MetadatiProtocollo.
     * 
     * @param soggettiProtocollo
     */
    public void setSoggettiProtocollo(org.datacontract.schemas._2004._07.WSGDA_BLL_BO_Soggetti.SoggettiProtocollo soggettiProtocollo) {
        this.soggettiProtocollo = soggettiProtocollo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MetadatiProtocollo)) return false;
        MetadatiProtocollo other = (MetadatiProtocollo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dataArrivo==null && other.getDataArrivo()==null) || 
             (this.dataArrivo!=null &&
              this.dataArrivo.equals(other.getDataArrivo()))) &&
            ((this.documentoPrincipale==null && other.getDocumentoPrincipale()==null) || 
             (this.documentoPrincipale!=null &&
              this.documentoPrincipale.equals(other.getDocumentoPrincipale()))) &&
            ((this.soggettiProtocollo==null && other.getSoggettiProtocollo()==null) || 
             (this.soggettiProtocollo!=null &&
              this.soggettiProtocollo.equals(other.getSoggettiProtocollo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDataArrivo() != null) {
            _hashCode += getDataArrivo().hashCode();
        }
        if (getDocumentoPrincipale() != null) {
            _hashCode += getDocumentoPrincipale().hashCode();
        }
        if (getSoggettiProtocollo() != null) {
            _hashCode += getSoggettiProtocollo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MetadatiProtocollo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.RequestInput", "MetadatiProtocollo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataArrivo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.RequestInput", "DataArrivo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentoPrincipale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.RequestInput", "DocumentoPrincipale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.RequestInput", "MetadatiDocumentoPrincipale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soggettiProtocollo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.RequestInput", "SoggettiProtocollo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.BO.Soggetti", "SoggettiProtocollo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
