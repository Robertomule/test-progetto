/**
 * DNA_BASICHTTP_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

import wsProtocollo.AllegatiCatenaDocumentale;
import wsProtocollo.AllegatoResponse;
import wsProtocollo.Annullamento;
import wsProtocollo.Applicazione;
import wsProtocollo.AreaOrganizzativa;
import wsProtocollo.CatenaDocumentale;
import wsProtocollo.CcDestinatario;
import wsProtocollo.Classifica;
import wsProtocollo.Destinatario;
import wsProtocollo.DocumentoElettronicoResponse;
import wsProtocollo.DocumentoPrincipaleResponse;
import wsProtocollo.DocumentoResponse;
import wsProtocollo.FingerPrint;
import wsProtocollo.Messaggio;
import wsProtocollo.Metadati;
import wsProtocollo.Mittente;
import wsProtocollo.ModificaDestinatario;
import wsProtocollo.ModificaSoggettoAfferente;
import wsProtocollo.OrganizationalAreaExt;
import wsProtocollo.OrganizationalAreaPlus;
import wsProtocollo.Owner;
import wsProtocollo.PoloSpecializzato;
import wsProtocollo.ProcessoDestinatarioResponse;
import wsProtocollo.ProtocolloEntrata;
import wsProtocollo.ProtocolloExtResponse;
import wsProtocollo.ProtocolloResponse;
import wsProtocollo.ProtocolloUscita;
import wsProtocollo.RegistroEmergenza;
import wsProtocollo.SegnaturaAllegata;
import wsProtocollo.Soggetto;
import wsProtocollo.SoggettoAfferente;
import wsProtocollo.Utente;

public class DNA_BASICHTTP_BindingStub extends org.apache.axis.client.Stub implements it.inps.soa.WS00732.IWSProtocollo {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[52];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
        _initOperationDesc5();
        _initOperationDesc6();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetVersion");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GetVersionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloEntrataRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataRequest"), it.inps.soa.WS00732.data.ProtocolloEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CodiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloUscitaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaRequest"), it.inps.soa.WS00732.data.ProtocolloUscitaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataEdAllega");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloEntrataRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataRequest"), it.inps.soa.WS00732.data.ProtocolloEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnaturaPrincipale"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaAllegaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataEdAllegaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscitaEdAllegaCrossAOO");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloUscitaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaRequest"), it.inps.soa.WS00732.data.ProtocolloUscitaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnaturaPrincipale"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AOOdelProtocollo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaAllegaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaEdAllegaCrossAOOResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataEdAllegaCrossAOO");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloEntrataRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataRequest"), it.inps.soa.WS00732.data.ProtocolloEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnaturaPrincipale"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AOOdelProtocollo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaAllegaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataEdAllegaCrossAOOResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiListaAllegati");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "Segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ListaAllegatiResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ListaAllegatiResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiListaAllegatiResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiListaCatenaTrasferimenti");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "Segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ListaAllegatiResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ListaAllegatiResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiListaCatenaTrasferimentiResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiCatenaDocumentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "Segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ListaCatenaDocumentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiCatenaDocumentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreaCatenaDocumentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "SegnaturaPadre"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "SegnaturaFiglia"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaCatenaDocumentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaCatenaDocumentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscitaEdAllega");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "protocolloUscitaRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaRequest"), it.inps.soa.WS00732.data.ProtocolloUscitaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnaturaPrincipale"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaAllegaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaEdAllegaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ModificaMetadatiProtocollo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "metadatiProtocollo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiProtocollo"), it.inps.soa.WS00732.data.MetadatiProtocollo.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ModificaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ModificaMetadatiProtocolloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AnnullaProtocollo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "annullaProtocolloRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AnnullaProtocolloRequest"), it.inps.soa.WS00732.data.AnnullaProtocolloRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AnnullaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.AnnullaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AnnullaProtocolloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiMetadatiDocumento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.MetadatiResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiMetadatiDocumentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GeneraRicevutaProtocollo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "GeneraRicevutaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.GeneraRicevutaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GeneraRicevutaProtocolloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GeneraRicevutaProtocolloBarcode");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "segnatura"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RicevutaProtocolloBarcodeResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GeneraRicevutaProtocolloBarcodeResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AssociaDocumentoPrimario");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "documentoPrimarioRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrimarioRequest"), it.inps.soa.WS00732.data.DocumentoPrimarioRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AssociaDocumentoPrimarioResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AssociaDocumentoPrimarioResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AggiungiAllegatiContestuali");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "allegatiContestualiRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiContestualiRequest"), it.inps.soa.WS00732.data.AllegatiContestualiRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.AllegatiResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AggiungiAllegatiContestualiResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TrasferisciProtocolloInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "trasferisciRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciRequest"), it.inps.soa.WS00732.data.TrasferisciRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.TrasferisciResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TrasferisciProtocolloInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "trasferisciRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciRequest"), it.inps.soa.WS00732.data.TrasferisciRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.TrasferisciResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RichiestaProtocollazioneInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataRequest"), it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaProtocollazioneInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RichiestaInteroperabilitaInEntrata");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataRequest"), it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaInteroperabilitaInEntrataResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataNAI");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataNAIRequest"), it.inps.soa.WS00732.data.ProtocollaInEntrataNAIRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaNAIResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaNAIResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataNAIResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreaFascicoloProcedimentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaFascicoloProcedimentaleRequest"), it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaFascicoloProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaFascicoloProcedimentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreaCatenaFascicolazione");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaCatenaFascicolazioneRequest"), it.inps.soa.WS00732.data.CreaCatenaFascicolazioneRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaCatenaFascicolazioneResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaCatenaFascicolazioneResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataConFascicolazioneProcedimentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataConFascicolazioneProcedimentaleRequest"), it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazioneProcedimentaleRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazioneProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataConFascicolazioneProcedimentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest"), it.inps.soa.WS00732.data.ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazioneProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscitaConFascicolazioneProcedimentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaConFascicolazioneProcedimentaleRequest"), it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazioneProcedimentaleRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazioneProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaConFascicolazioneProcedimentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest"), it.inps.soa.WS00732.data.ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazioneProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataConFascicolazionePregressa");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataConFascicolazionePregressaRequest"), it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazionePregressaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataConFascicolazionePregressaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStato");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest"), it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazionePregressaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscitaConFascicolazionePregressa");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaConFascicolazionePregressaRequest"), it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazionePregressaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaConFascicolazionePregressaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStato");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest"), it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazionePregressaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiDocumentiFascicolo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceFascicolo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentiFascicoloResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.DocumentiFascicoloResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiDocumentiFascicoloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("FascicolaDocumentoProtocollato");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FascicolaDocumentoProtocollatoRequest"), it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FascicolaDocumentoProtocollatoResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "FascicolaDocumentoProtocollatoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiCatenaFascicolazione");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceFascicolo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CatenaFascicolazioneResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.CatenaFascicolazioneResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiCatenaFascicolazioneResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[35] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("VariaEsitoProcedimento");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "VariaEsitoProcedimentoRequest"), it.inps.soa.WS00732.data.VariaEsitoProcedimentoRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "VariaEsitoProcedimentoResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "VariaEsitoProcedimentoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[36] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RichiestaCRM");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaCRMRequest"), it.inps.soa.WS00732.data.RichiestaCRMRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaCRMResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RichiestaCRMResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaCRMResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[37] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInEntrataESpedisci");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CodiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataESpedisciRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataESpedisciRequest"), it.inps.soa.WS00732.data.ProtocolloEntrataESpedisciRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataESpedisciResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataESpedisciResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[38] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaInUscitaESpedisci");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CodiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocolloUscitaESpedisciRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaESpedisciRequest"), it.inps.soa.WS00732.data.ProtocolloUscitaESpedisciRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaESpedisciResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaESpedisciResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[39] = oper;

    }

    private static void _initOperationDesc5(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreaFascicoloNonProcedimentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaFascicoloNonProcedimentaleRequest"), it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaFascicoloNonProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaFascicoloNonProcedimentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[40] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreaContainer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceAOO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ContainerRequest"), it.inps.soa.WS00732.data.ContainerRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ContainerResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ContainerResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaContainerResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[41] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AssociaContainerAFascicolo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AssociaContainerAFascicoloRequest"), it.inps.soa.WS00732.data.AssociaContainerAFascicoloRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AssociaContainerAFascicoloResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AssociaContainerAFascicoloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[42] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ChiudiFascicoloNonProcedimentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StatoFascicoloNonProcedimentaleRequest"), it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StatoFascicoloNonProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ChiudiFascicoloNonProcedimentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[43] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RiapriFascicoloNonProcedimentale");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StatoFascicoloNonProcedimentaleRequest"), it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StatoFascicoloNonProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RiapriFascicoloNonProcedimentaleResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[44] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiPraticaDaFascicolo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codiceFascicolo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentiFascicoloNonProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiPraticaDaFascicoloResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[45] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EstraiPratica");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "codicePratica"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "tipoCodicePratica"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentiFascicoloNonProcedimentaleResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiPraticaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[46] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RifiutaRichiestaPECInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "messageId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "motivazione"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RifiutaRichiestaPECInUscitaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RifiutaRichiestaPECInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[47] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AutorizzaRichiestaPECInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "messageId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AutorizzaRichiestaPECInUscitaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AutorizzaRichiestaPECInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[48] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ProtocollaPECInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "messageId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaPECInUscitaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaPECInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[49] = oper;

    }

    private static void _initOperationDesc6(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegistraStatoInvioPECInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistraStatoInvioPECInUscitaRequest"), it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistraStatoInvioPECInUscitaResponse"));
        oper.setReturnClass(it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RegistraStatoInvioPECInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[50] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegistraStatoConsegnaPECInUscita");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistraStatoConsegnaPECInUscitaRequest"), it.inps.soa.WS00732.data.RegistraStatoConsegnaPECInUscitaRequest.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Responses", "RegistraStatoConsegnaPECInUscitaResponse"));
        oper.setReturnClass(org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RegistraStatoConsegnaPECInUscitaResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[51] = oper;

    }

    public DNA_BASICHTTP_BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public DNA_BASICHTTP_BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public DNA_BASICHTTP_BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("", "AllegatiCatenaDocumentale");
            cachedSerQNames.add(qName);
            cls = AllegatiCatenaDocumentale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AllegatoResponse");
            cachedSerQNames.add(qName);
            cls = AllegatoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Annullamento");
            cachedSerQNames.add(qName);
            cls = Annullamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Applicazione");
            cachedSerQNames.add(qName);
            cls = Applicazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "AreaOrganizzativa");
            cachedSerQNames.add(qName);
            cls = AreaOrganizzativa.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ArrayOfAllegatiCatenaDocumentale");
            cachedSerQNames.add(qName);
            cls = AllegatiCatenaDocumentale[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "AllegatiCatenaDocumentale");
            qName2 = new javax.xml.namespace.QName("", "AllegatiCatenaDocumentale");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfAllegatoResponse");
            cachedSerQNames.add(qName);
            cls = AllegatoResponse[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "AllegatoResponse");
            qName2 = new javax.xml.namespace.QName("", "AllegatoResponse");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfClassifica");
            cachedSerQNames.add(qName);
            cls = Classifica[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Classifica");
            qName2 = new javax.xml.namespace.QName("", "Classifica");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfDestinatario");
            cachedSerQNames.add(qName);
            cls = Destinatario[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Destinatario");
            qName2 = new javax.xml.namespace.QName("", "Destinatario");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfMetadati");
            cachedSerQNames.add(qName);
            cls = Metadati[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "Metadati");
            qName2 = new javax.xml.namespace.QName("", "Metadati");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfSegnaturaAllegata");
            cachedSerQNames.add(qName);
            cls = SegnaturaAllegata[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "SegnaturaAllegata");
            qName2 = new javax.xml.namespace.QName("", "SegnaturaAllegata");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "ArrayOfSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = SoggettoAfferente[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("", "SoggettoAfferente");
            qName2 = new javax.xml.namespace.QName("", "SoggettoAfferente");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("", "CatenaDocumentale");
            cachedSerQNames.add(qName);
            cls = CatenaDocumentale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "CcDestinatario");
            cachedSerQNames.add(qName);
            cls = CcDestinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Classifica");
            cachedSerQNames.add(qName);
            cls = Classifica.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Destinatario");
            cachedSerQNames.add(qName);
            cls = Destinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoElettronicoResponse");
            cachedSerQNames.add(qName);
            cls = DocumentoElettronicoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoPrincipaleResponse");
            cachedSerQNames.add(qName);
            cls = DocumentoPrincipaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "DocumentoResponse");
            cachedSerQNames.add(qName);
            cls = DocumentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "FingerPrint");
            cachedSerQNames.add(qName);
            cls = FingerPrint.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Messaggio");
            cachedSerQNames.add(qName);
            cls = Messaggio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Metadati");
            cachedSerQNames.add(qName);
            cls = Metadati.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Mittente");
            cachedSerQNames.add(qName);
            cls = Mittente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ModificaDestinatario");
            cachedSerQNames.add(qName);
            cls = ModificaDestinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ModificaSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = ModificaSoggettoAfferente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "OrganizationalAreaExt");
            cachedSerQNames.add(qName);
            cls = OrganizationalAreaExt.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "OrganizationalAreaPlus");
            cachedSerQNames.add(qName);
            cls = OrganizationalAreaPlus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Owner");
            cachedSerQNames.add(qName);
            cls = Owner.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "PoloSpecializzato");
            cachedSerQNames.add(qName);
            cls = PoloSpecializzato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProcessoDestinatarioResponse");
            cachedSerQNames.add(qName);
            cls = ProcessoDestinatarioResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloEntrata");
            cachedSerQNames.add(qName);
            cls = ProtocolloEntrata.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloExtResponse");
            cachedSerQNames.add(qName);
            cls = ProtocolloExtResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloResponse");
            cachedSerQNames.add(qName);
            cls = ProtocolloResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "ProtocolloUscita");
            cachedSerQNames.add(qName);
            cls = ProtocolloUscita.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "RegistroEmergenza");
            cachedSerQNames.add(qName);
            cls = RegistroEmergenza.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "SegnaturaAllegata");
            cachedSerQNames.add(qName);
            cls = SegnaturaAllegata.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Soggetto");
            cachedSerQNames.add(qName);
            cls = Soggetto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "SoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = SoggettoAfferente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("", "Utente");
            cachedSerQNames.add(qName);
            cls = Utente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Class.Folder", "AltraAmministrazione");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Class_Folder.AltraAmministrazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Class.Folder", "Richiedente");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Class_Folder.Richiedente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Class.Folder", "Titolare");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Class_Folder.Titolare.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnRelationShipTypes");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnRuoloNelProcedimento");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRuoloNelProcedimento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoAcquisizione");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoAcquisizione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoInvio");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoInvio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoOperazione");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoOperazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoSoggetto");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoSoggetto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Enumeration", "EnStatoConsegnaRichiestaPECInUscita");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Enumeration.EnStatoConsegnaRichiestaPECInUscita.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Enumeration", "EnStatoInvioRichiestaPECInUscita");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Enumeration.EnStatoInvioRichiestaPECInUscita.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "ArrayOfDestinatario");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Destinatario[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Destinatario");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Destinatario");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "ArrayOfDocumento");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Documento[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Documento");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Documento");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "ArrayOfSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.SoggettoAfferente[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "SoggettoAfferente");
            qName2 = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "SoggettoAfferente");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "CRMContatto");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.CRMContatto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "DatiInvio");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.DatiInvio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Destinatario");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Destinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Documento");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Documento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "Mittente");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.Mittente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "SoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.SoggettoAfferente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Responses", "RegistraStatoConsegnaPECInUscitaResponse");
            cachedSerQNames.add(qName);
            cls = org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiContestualiRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AllegatiContestualiRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AllegatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Allegato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AllegatoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Annullamento");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Annullamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AnnullaProtocolloRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AnnullaProtocolloRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AnnullaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AnnullaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Applicazione");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Applicazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfAllegato");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Allegato[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfAllegatoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AllegatoResponse[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatoResponse");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatoResponse");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfClassifica");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Classifica[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfContainer");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Container[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Container");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Container");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfDatiFascicolo");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DatiFascicolo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DatiFascicolo");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DatiFascicolo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfDestinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Destinatario[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatario");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatario");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfDocumentoFascicolato");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoFascicolato[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoFascicolato");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoFascicolato");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfModificaDestinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaDestinatario[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaDestinatario");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaDestinatario");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfModificaSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaSoggettoAfferente[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaSoggettoAfferente");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaSoggettoAfferente");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfProtocolRegistration");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolRegistration[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolRegistration");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolRegistration");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfRelatedDataItem");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RelatedDataItem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RelatedDataItem");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RelatedDataItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfSegnaturaAllegata");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SegnaturaAllegata[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SegnaturaAllegata");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SegnaturaAllegata");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfSoggetto");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Soggetto[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Soggetto");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Soggetto");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SoggettoAfferente[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettoAfferente");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettoAfferente");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ArrayOfTrasferisciReq");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.TrasferisciReq[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciReq");
            qName2 = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciReq");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AssociaContainerAFascicoloRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AssociaContainerAFascicoloRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AssociaContainerAFascicoloResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AssociaDocumentoPrimarioResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AutorizzaRichiestaPECInUscitaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "BaseDataRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.BaseDataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "BaseFascicolazioneProcedimentaleRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.BaseFascicolazioneProcedimentaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "BaseRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.BaseRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CatenaDocumentale");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CatenaDocumentale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CatenaFascicolazioneResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CatenaFascicolazioneResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ChiaveEsterna");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ChiaveEsterna.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Classifica.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Container");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Container.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ContainerRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ContainerRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ContainerResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ContainerResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaCatenaDocumentaleResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaCatenaFascicolazioneRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CreaCatenaFascicolazioneRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaCatenaFascicolazioneResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaFascicoloNonProcedimentaleRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaFascicoloNonProcedimentaleResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaFascicoloProcedimentaleRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaFascicoloProcedimentaleResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DatiFascicolo");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DatiFascicolo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Destinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentiFascicoloNonProcedimentaleResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentiFascicoloResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentiFascicoloResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Documento");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Documento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoElettronico");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoElettronico.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoElettronicoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoElettronicoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoFascicolato");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoFascicolato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrimarioRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoPrimarioRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrincipale");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoPrincipale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrincipaleResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoPrincipaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.DocumentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnCategoriaProtocollo");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnCategoriaProtocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnEsito");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnEsito.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnFolderCategory");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnFolderCategory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnRelationShipTypes");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnRelationShipTypes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnRoutingSystems");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnRoutingSystems.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnRuoloNelProcedimento");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnRuoloNelProcedimento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoAcquisizione");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnTipoAcquisizione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoInvio");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnTipoInvio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoOperazione");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnTipoOperazione.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoSoggetto");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.EnTipoSoggetto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FascicolaDocumentoProtocollatoRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FascicolaDocumentoProtocollatoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FingerPrint");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.FingerPrint.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FolderNotProceduralResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.FolderNotProceduralResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "GeneraRicevutaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.GeneraRicevutaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ListaAllegatiResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ListaAllegatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ListaCatenaDocumentaleResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Metadati");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Metadati.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiDocumento");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.MetadatiDocumento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiDocumentoPrincipale");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.MetadatiDocumentoPrincipale.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiProtocollo");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.MetadatiProtocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.MetadatiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Mittente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Mittente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaDestinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaDestinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaSoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ModificaSoggettoAfferente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "NAI");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.NAI.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "OrganizationalAreaExt");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.OrganizationalAreaExt.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProcessoDestinatario");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProcessoDestinatario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProcessoDestinatarioResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProcessoDestinatarioResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaAllegaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazionePregressaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaConFascicolazioneProcedimentaleResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataConFascicolazionePregressaRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataConFascicolazioneProcedimentaleRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazioneProcedimentaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataESpedisciResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataNAIRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInEntrataNAIRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaConFascicolazionePregressaRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaConFascicolazioneProcedimentaleRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazioneProcedimentaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaESpedisciResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaNAIResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaNAIResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaPECInUscitaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocollaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Protocollo");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Protocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrata");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloEntrata.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataESpedisciRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloEntrataESpedisciRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloEntrataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloExtResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloExtResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscita");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloUscita.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaESpedisciRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloUscitaESpedisciRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolloUscitaRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolRegistration");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.ProtocolRegistration.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistraStatoConsegnaPECInUscitaRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RegistraStatoConsegnaPECInUscitaRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistraStatoInvioPECInUscitaRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistraStatoInvioPECInUscitaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistroEmergenza");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RegistroEmergenza.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RelatedDataItem");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RelatedDataItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Response");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RicevutaProtocolloBarcodeResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaCRMRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaCRMRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaCRMResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaCRMResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaInteroperabilitaInEntrataResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaProtocollazioneInEntrataResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RifiutaRichiestaPECInUscitaResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SegnaturaAllegata");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SegnaturaAllegata.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettiProtocollo");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SoggettiProtocollo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Soggetto");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Soggetto.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettoAfferente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.SoggettoAfferente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StatoFascicoloNonProcedimentaleRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StatoFascicoloNonProcedimentaleResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciReq");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.TrasferisciReq.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.TrasferisciRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.TrasferisciResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Utente");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.Utente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "VariaEsitoProcedimentoRequest");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.VariaEsitoProcedimentoRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "VariaEsitoProcedimentoResponse");
            cachedSerQNames.add(qName);
            cls = it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public java.lang.String getVersion() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/GetVersion");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GetVersion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInEntrata(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, protocolloEntrataRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInUscita(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, protocolloUscitaRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInEntrataEdAllega(it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataEdAllega");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataEdAllega"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {protocolloEntrataRequest, segnaturaPrincipale});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInUscitaEdAllegaCrossAOO(it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest, java.lang.String segnaturaPrincipale, java.lang.String AOOdelProtocollo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscitaEdAllegaCrossAOO");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaEdAllegaCrossAOO"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {protocolloUscitaRequest, segnaturaPrincipale, AOOdelProtocollo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInEntrataEdAllegaCrossAOO(it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest, java.lang.String segnaturaPrincipale, java.lang.String AOOdelProtocollo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataEdAllegaCrossAOO");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataEdAllegaCrossAOO"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {protocolloEntrataRequest, segnaturaPrincipale, AOOdelProtocollo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ListaAllegatiResponse estraiListaAllegati(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiListaAllegati");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiListaAllegati"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ListaAllegatiResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ListaAllegatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ListaAllegatiResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ListaAllegatiResponse estraiListaCatenaTrasferimenti(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiListaCatenaTrasferimenti");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiListaCatenaTrasferimenti"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ListaAllegatiResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ListaAllegatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ListaAllegatiResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse estraiCatenaDocumentale(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiCatenaDocumentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiCatenaDocumentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse creaCatenaDocumentale(java.lang.String segnaturaPadre, java.lang.String segnaturaFiglia) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/CreaCatenaDocumentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaCatenaDocumentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnaturaPadre, segnaturaFiglia});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInUscitaEdAllega(it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscitaEdAllega");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaEdAllega"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {protocolloUscitaRequest, segnaturaPrincipale});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaAllegaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaAllegaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ModificaResponse modificaMetadatiProtocollo(java.lang.String segnatura, it.inps.soa.WS00732.data.MetadatiProtocollo metadatiProtocollo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ModificaMetadatiProtocollo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ModificaMetadatiProtocollo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura, metadatiProtocollo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ModificaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ModificaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ModificaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.AnnullaResponse annullaProtocollo(java.lang.String segnatura, it.inps.soa.WS00732.data.AnnullaProtocolloRequest annullaProtocolloRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AnnullaProtocollo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AnnullaProtocollo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura, annullaProtocolloRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.AnnullaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.AnnullaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.AnnullaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.MetadatiResponse estraiMetadatiDocumento(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiMetadatiDocumento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiMetadatiDocumento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.MetadatiResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.MetadatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.MetadatiResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.GeneraRicevutaResponse generaRicevutaProtocollo(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/GeneraRicevutaProtocollo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GeneraRicevutaProtocollo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.GeneraRicevutaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.GeneraRicevutaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.GeneraRicevutaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse generaRicevutaProtocolloBarcode(java.lang.String segnatura) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/GeneraRicevutaProtocolloBarcode");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "GeneraRicevutaProtocolloBarcode"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {segnatura});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse associaDocumentoPrimario(it.inps.soa.WS00732.data.DocumentoPrimarioRequest documentoPrimarioRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AssociaDocumentoPrimario");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AssociaDocumentoPrimario"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {documentoPrimarioRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.AllegatiResponse aggiungiAllegatiContestuali(it.inps.soa.WS00732.data.AllegatiContestualiRequest allegatiContestualiRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AggiungiAllegatiContestuali");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AggiungiAllegatiContestuali"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {allegatiContestualiRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.AllegatiResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.AllegatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.AllegatiResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInEntrata(it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/TrasferisciProtocolloInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {trasferisciRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.TrasferisciResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.TrasferisciResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.TrasferisciResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInUscita(it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/TrasferisciProtocolloInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "TrasferisciProtocolloInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {trasferisciRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.TrasferisciResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.TrasferisciResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.TrasferisciResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse richiestaProtocollazioneInEntrata(it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RichiestaProtocollazioneInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaProtocollazioneInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse richiestaInteroperabilitaInEntrata(it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RichiestaInteroperabilitaInEntrata");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaInteroperabilitaInEntrata"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaNAIResponse protocollaInEntrataNAI(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataNAIRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataNAI");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataNAI"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaNAIResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaNAIResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaNAIResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse creaFascicoloProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/CreaFascicoloProcedimentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaFascicoloProcedimentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse creaCatenaFascicolazione(it.inps.soa.WS00732.data.CreaCatenaFascicolazioneRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/CreaCatenaFascicolazione");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaCatenaFascicolazione"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataConFascicolazioneProcedimentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataConFascicolazioneProcedimentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConCatenaDiFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscitaConFascicolazioneProcedimentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaConFascicolazioneProcedimentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConCatenaDiFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressa(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataConFascicolazionePregressa");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataConFascicolazionePregressa"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressaEVariazioneDiStato(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStato");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStato"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressa(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscitaConFascicolazionePregressa");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaConFascicolazionePregressa"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressaEVariazioneDiStato(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStato");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStato"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.DocumentiFascicoloResponse estraiDocumentiFascicolo(java.lang.String codiceFascicolo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiDocumentiFascicolo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiDocumentiFascicolo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceFascicolo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.DocumentiFascicoloResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.DocumentiFascicoloResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.DocumentiFascicoloResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse fascicolaDocumentoProtocollato(it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/FascicolaDocumentoProtocollato");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "FascicolaDocumentoProtocollato"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.CatenaFascicolazioneResponse estraiCatenaFascicolazione(java.lang.String codiceFascicolo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiCatenaFascicolazione");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiCatenaFascicolazione"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceFascicolo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.CatenaFascicolazioneResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.CatenaFascicolazioneResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.CatenaFascicolazioneResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse variaEsitoProcedimento(it.inps.soa.WS00732.data.VariaEsitoProcedimentoRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[36]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/VariaEsitoProcedimento");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "VariaEsitoProcedimento"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RichiestaCRMResponse richiestaCRM(it.inps.soa.WS00732.data.RichiestaCRMRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[37]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RichiestaCRM");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RichiestaCRM"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RichiestaCRMResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RichiestaCRMResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RichiestaCRMResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse protocollaInEntrataESpedisci(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloEntrataESpedisciRequest protocollaInEntrataESpedisciRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[38]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInEntrataESpedisci");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInEntrataESpedisci"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, protocollaInEntrataESpedisciRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse protocollaInUscitaESpedisci(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloUscitaESpedisciRequest protocolloUscitaESpedisciRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[39]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaInUscitaESpedisci");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaInUscitaESpedisci"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, protocolloUscitaESpedisciRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse creaFascicoloNonProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[40]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/CreaFascicoloNonProcedimentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaFascicoloNonProcedimentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ContainerResponse creaContainer(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ContainerRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[41]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/CreaContainer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "CreaContainer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceAOO, request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ContainerResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ContainerResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ContainerResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse associaContainerAFascicolo(it.inps.soa.WS00732.data.AssociaContainerAFascicoloRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[42]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AssociaContainerAFascicolo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AssociaContainerAFascicolo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse chiudiFascicoloNonProcedimentale(it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[43]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ChiudiFascicoloNonProcedimentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ChiudiFascicoloNonProcedimentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse riapriFascicoloNonProcedimentale(it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[44]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RiapriFascicoloNonProcedimentale");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RiapriFascicoloNonProcedimentale"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse estraiPraticaDaFascicolo(java.lang.String codiceFascicolo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[45]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiPraticaDaFascicolo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiPraticaDaFascicolo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codiceFascicolo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse estraiPratica(java.lang.String codicePratica, java.lang.String tipoCodicePratica) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[46]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/EstraiPratica");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "EstraiPratica"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codicePratica, tipoCodicePratica});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse rifiutaRichiestaPECInUscita(java.lang.String messageId, java.lang.String motivazione) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[47]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RifiutaRichiestaPECInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RifiutaRichiestaPECInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {messageId, motivazione});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse autorizzaRichiestaPECInUscita(java.lang.String messageId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[48]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/AutorizzaRichiestaPECInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "AutorizzaRichiestaPECInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {messageId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse protocollaPECInUscita(java.lang.String messageId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[49]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/ProtocollaPECInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "ProtocollaPECInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {messageId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse registraStatoInvioPECInUscita(it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[50]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RegistraStatoInvioPECInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RegistraStatoInvioPECInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse registraStatoConsegnaPECInUscita(it.inps.soa.WS00732.data.RegistraStatoConsegnaPECInUscitaRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[51]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://soa.inps.it/WS00732/IWSProtocollo/RegistraStatoConsegnaPECInUscita");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732", "RegistraStatoConsegnaPECInUscita"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
