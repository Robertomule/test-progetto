/**
 * WSProtocolloLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class WSProtocolloLocator extends org.apache.axis.client.Service implements org.tempuri.WSProtocollo {

    public WSProtocolloLocator() {
    }


    public WSProtocolloLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WSProtocolloLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for DNA_BASICHTTP_Binding
    private java.lang.String DNA_BASICHTTP_Binding_address = "http://msws2.svil.inps/WSProtocollo/WSProtocolloService.svc";

    public java.lang.String getDNA_BASICHTTP_BindingAddress() {
        return DNA_BASICHTTP_Binding_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String DNA_BASICHTTP_BindingWSDDServiceName = "DNA_BASICHTTP_Binding";

    public java.lang.String getDNA_BASICHTTP_BindingWSDDServiceName() {
        return DNA_BASICHTTP_BindingWSDDServiceName;
    }

    public void setDNA_BASICHTTP_BindingWSDDServiceName(java.lang.String name) {
        DNA_BASICHTTP_BindingWSDDServiceName = name;
    }

    public it.inps.soa.WS00732.IWSProtocollo getDNA_BASICHTTP_Binding() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(DNA_BASICHTTP_Binding_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getDNA_BASICHTTP_Binding(endpoint);
    }

    public it.inps.soa.WS00732.IWSProtocollo getDNA_BASICHTTP_Binding(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.DNA_BASICHTTP_BindingStub _stub = new org.tempuri.DNA_BASICHTTP_BindingStub(portAddress, this);
            _stub.setPortName(getDNA_BASICHTTP_BindingWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setDNA_BASICHTTP_BindingEndpointAddress(java.lang.String address) {
        DNA_BASICHTTP_Binding_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (it.inps.soa.WS00732.IWSProtocollo.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.DNA_BASICHTTP_BindingStub _stub = new org.tempuri.DNA_BASICHTTP_BindingStub(new java.net.URL(DNA_BASICHTTP_Binding_address), this);
                _stub.setPortName(getDNA_BASICHTTP_BindingWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("DNA_BASICHTTP_Binding".equals(inputPortName)) {
            return getDNA_BASICHTTP_Binding();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "WSProtocollo");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "DNA_BASICHTTP_Binding"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("DNA_BASICHTTP_Binding".equals(portName)) {
            setDNA_BASICHTTP_BindingEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
