/**
 * GetDocumentoPerVersioneRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class GetDocumentoPerVersioneRequest  extends it.inps.soa.DocumentStore.data.DocumentStoreRequestBase  implements java.io.Serializable {
    private java.lang.String storeId;

    private org.apache.axis.types.UnsignedInt versione;

    public GetDocumentoPerVersioneRequest() {
    }

    public GetDocumentoPerVersioneRequest(
           java.lang.String storeId,
           org.apache.axis.types.UnsignedInt versione) {
        this.storeId = storeId;
        this.versione = versione;
    }


    /**
     * Gets the storeId value for this GetDocumentoPerVersioneRequest.
     * 
     * @return storeId
     */
    public java.lang.String getStoreId() {
        return storeId;
    }


    /**
     * Sets the storeId value for this GetDocumentoPerVersioneRequest.
     * 
     * @param storeId
     */
    public void setStoreId(java.lang.String storeId) {
        this.storeId = storeId;
    }


    /**
     * Gets the versione value for this GetDocumentoPerVersioneRequest.
     * 
     * @return versione
     */
    public org.apache.axis.types.UnsignedInt getVersione() {
        return versione;
    }


    /**
     * Sets the versione value for this GetDocumentoPerVersioneRequest.
     * 
     * @param versione
     */
    public void setVersione(org.apache.axis.types.UnsignedInt versione) {
        this.versione = versione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDocumentoPerVersioneRequest)) return false;
        GetDocumentoPerVersioneRequest other = (GetDocumentoPerVersioneRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.storeId==null && other.getStoreId()==null) || 
             (this.storeId!=null &&
              this.storeId.equals(other.getStoreId()))) &&
            ((this.versione==null && other.getVersione()==null) || 
             (this.versione!=null &&
              this.versione.equals(other.getVersione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getStoreId() != null) {
            _hashCode += getStoreId().hashCode();
        }
        if (getVersione() != null) {
            _hashCode += getVersione().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDocumentoPerVersioneRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoPerVersioneRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "StoreId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Versione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
