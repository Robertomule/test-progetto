/**
 * EsistonoDocumentiPerSegnaturaResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class EsistonoDocumentiPerSegnaturaResponse  extends it.inps.soa.DocumentStore.data.DocumentStoreResponseBase  implements java.io.Serializable {
    private it.inps.soa.DocumentStore.data.DocumentoProtocollatoExistsResponse[] exists;

    public EsistonoDocumentiPerSegnaturaResponse() {
    }

    public EsistonoDocumentiPerSegnaturaResponse(
           java.lang.String descrizione,
           it.inps.soa.DocumentStore.data.EsitoTypeEnum esito,
           it.inps.soa.DocumentStore.data.DocumentoProtocollatoExistsResponse[] exists) {
        super(
            descrizione,
            esito);
        this.exists = exists;
    }


    /**
     * Gets the exists value for this EsistonoDocumentiPerSegnaturaResponse.
     * 
     * @return exists
     */
    public it.inps.soa.DocumentStore.data.DocumentoProtocollatoExistsResponse[] getExists() {
        return exists;
    }


    /**
     * Sets the exists value for this EsistonoDocumentiPerSegnaturaResponse.
     * 
     * @param exists
     */
    public void setExists(it.inps.soa.DocumentStore.data.DocumentoProtocollatoExistsResponse[] exists) {
        this.exists = exists;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EsistonoDocumentiPerSegnaturaResponse)) return false;
        EsistonoDocumentiPerSegnaturaResponse other = (EsistonoDocumentiPerSegnaturaResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.exists==null && other.getExists()==null) || 
             (this.exists!=null &&
              java.util.Arrays.equals(this.exists, other.getExists())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getExists() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExists());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExists(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EsistonoDocumentiPerSegnaturaResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "EsistonoDocumentiPerSegnaturaResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exists");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Exists"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoProtocollatoExistsResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoProtocollatoExistsResponse"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
