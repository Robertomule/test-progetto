/**
 * ListaDocumentiResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class ListaDocumentiResponse  extends it.inps.soa.DocumentStore.data.DocumentStoreResponseBase  implements java.io.Serializable {
    private it.inps.soa.DocumentStore.data.DocumentoStore[] documenti;

    private java.lang.Integer recordCount;

    public ListaDocumentiResponse() {
    }

    public ListaDocumentiResponse(
           java.lang.String descrizione,
           it.inps.soa.DocumentStore.data.EsitoTypeEnum esito,
           it.inps.soa.DocumentStore.data.DocumentoStore[] documenti,
           java.lang.Integer recordCount) {
        super(
            descrizione,
            esito);
        this.documenti = documenti;
        this.recordCount = recordCount;
    }


    /**
     * Gets the documenti value for this ListaDocumentiResponse.
     * 
     * @return documenti
     */
    public it.inps.soa.DocumentStore.data.DocumentoStore[] getDocumenti() {
        return documenti;
    }


    /**
     * Sets the documenti value for this ListaDocumentiResponse.
     * 
     * @param documenti
     */
    public void setDocumenti(it.inps.soa.DocumentStore.data.DocumentoStore[] documenti) {
        this.documenti = documenti;
    }


    /**
     * Gets the recordCount value for this ListaDocumentiResponse.
     * 
     * @return recordCount
     */
    public java.lang.Integer getRecordCount() {
        return recordCount;
    }


    /**
     * Sets the recordCount value for this ListaDocumentiResponse.
     * 
     * @param recordCount
     */
    public void setRecordCount(java.lang.Integer recordCount) {
        this.recordCount = recordCount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListaDocumentiResponse)) return false;
        ListaDocumentiResponse other = (ListaDocumentiResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.documenti==null && other.getDocumenti()==null) || 
             (this.documenti!=null &&
              java.util.Arrays.equals(this.documenti, other.getDocumenti()))) &&
            ((this.recordCount==null && other.getRecordCount()==null) || 
             (this.recordCount!=null &&
              this.recordCount.equals(other.getRecordCount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDocumenti() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDocumenti());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDocumenti(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRecordCount() != null) {
            _hashCode += getRecordCount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListaDocumentiResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documenti");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Documenti"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoStore"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Documento"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "RecordCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
