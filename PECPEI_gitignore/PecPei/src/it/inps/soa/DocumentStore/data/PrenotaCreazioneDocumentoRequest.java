/**
 * PrenotaCreazioneDocumentoRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class PrenotaCreazioneDocumentoRequest  extends it.inps.soa.DocumentStore.data.DocumentStoreRequestBase  implements java.io.Serializable {
    private java.lang.String redirectTo;

    public PrenotaCreazioneDocumentoRequest() {
    }

    public PrenotaCreazioneDocumentoRequest(
           java.lang.String redirectTo) {
        this.redirectTo = redirectTo;
    }


    /**
     * Gets the redirectTo value for this PrenotaCreazioneDocumentoRequest.
     * 
     * @return redirectTo
     */
    public java.lang.String getRedirectTo() {
        return redirectTo;
    }


    /**
     * Sets the redirectTo value for this PrenotaCreazioneDocumentoRequest.
     * 
     * @param redirectTo
     */
    public void setRedirectTo(java.lang.String redirectTo) {
        this.redirectTo = redirectTo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PrenotaCreazioneDocumentoRequest)) return false;
        PrenotaCreazioneDocumentoRequest other = (PrenotaCreazioneDocumentoRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.redirectTo==null && other.getRedirectTo()==null) || 
             (this.redirectTo!=null &&
              this.redirectTo.equals(other.getRedirectTo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRedirectTo() != null) {
            _hashCode += getRedirectTo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PrenotaCreazioneDocumentoRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaCreazioneDocumentoRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("redirectTo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "RedirectTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
