/**
 * ArchiviaDocumentoPerContainerRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class ArchiviaDocumentoPerContainerRequest  extends it.inps.soa.DocumentStore.data.ArchiviaDocumentoRequest  implements java.io.Serializable {
    private java.lang.Integer containerId;

    private it.inps.soa.DocumentStore.data.DocumentType documentType;

    private java.lang.String hash;

    public ArchiviaDocumentoPerContainerRequest() {
    }

    public ArchiviaDocumentoPerContainerRequest(
           byte[] documento,
           it.inps.soa.DocumentStore.data.Metadati metadati,
           java.lang.Integer containerId,
           it.inps.soa.DocumentStore.data.DocumentType documentType,
           java.lang.String hash) {
        super(
            documento,
            metadati);
        this.containerId = containerId;
        this.documentType = documentType;
        this.hash = hash;
    }


    /**
     * Gets the containerId value for this ArchiviaDocumentoPerContainerRequest.
     * 
     * @return containerId
     */
    public java.lang.Integer getContainerId() {
        return containerId;
    }


    /**
     * Sets the containerId value for this ArchiviaDocumentoPerContainerRequest.
     * 
     * @param containerId
     */
    public void setContainerId(java.lang.Integer containerId) {
        this.containerId = containerId;
    }


    /**
     * Gets the documentType value for this ArchiviaDocumentoPerContainerRequest.
     * 
     * @return documentType
     */
    public it.inps.soa.DocumentStore.data.DocumentType getDocumentType() {
        return documentType;
    }


    /**
     * Sets the documentType value for this ArchiviaDocumentoPerContainerRequest.
     * 
     * @param documentType
     */
    public void setDocumentType(it.inps.soa.DocumentStore.data.DocumentType documentType) {
        this.documentType = documentType;
    }


    /**
     * Gets the hash value for this ArchiviaDocumentoPerContainerRequest.
     * 
     * @return hash
     */
    public java.lang.String getHash() {
        return hash;
    }


    /**
     * Sets the hash value for this ArchiviaDocumentoPerContainerRequest.
     * 
     * @param hash
     */
    public void setHash(java.lang.String hash) {
        this.hash = hash;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArchiviaDocumentoPerContainerRequest)) return false;
        ArchiviaDocumentoPerContainerRequest other = (ArchiviaDocumentoPerContainerRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.containerId==null && other.getContainerId()==null) || 
             (this.containerId!=null &&
              this.containerId.equals(other.getContainerId()))) &&
            ((this.documentType==null && other.getDocumentType()==null) || 
             (this.documentType!=null &&
              this.documentType.equals(other.getDocumentType()))) &&
            ((this.hash==null && other.getHash()==null) || 
             (this.hash!=null &&
              this.hash.equals(other.getHash())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getContainerId() != null) {
            _hashCode += getContainerId().hashCode();
        }
        if (getDocumentType() != null) {
            _hashCode += getDocumentType().hashCode();
        }
        if (getHash() != null) {
            _hashCode += getHash().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArchiviaDocumentoPerContainerRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerContainerRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("containerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ContainerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hash");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Hash"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
