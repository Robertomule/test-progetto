/**
 * DocumentoStore.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class DocumentoStore  extends it.inps.soa.DocumentStore.data.Documento  implements java.io.Serializable {
    private int[] containerId;

    private java.lang.String[] segnatura;

    private it.inps.soa.DocumentStore.data.DocumentStoreLastState stato;

    private java.lang.Long versione;

    public DocumentoStore() {
    }

    public DocumentoStore(
           java.lang.String documentId,
           it.inps.soa.DocumentStore.data.InternalDocumentType documentType,
           it.inps.soa.DocumentStore.data.Metadati metadati,
           int[] containerId,
           java.lang.String[] segnatura,
           it.inps.soa.DocumentStore.data.DocumentStoreLastState stato,
           java.lang.Long versione) {
        super(
            documentId,
            documentType,
            metadati);
        this.containerId = containerId;
        this.segnatura = segnatura;
        this.stato = stato;
        this.versione = versione;
    }


    /**
     * Gets the containerId value for this DocumentoStore.
     * 
     * @return containerId
     */
    public int[] getContainerId() {
        return containerId;
    }


    /**
     * Sets the containerId value for this DocumentoStore.
     * 
     * @param containerId
     */
    public void setContainerId(int[] containerId) {
        this.containerId = containerId;
    }


    /**
     * Gets the segnatura value for this DocumentoStore.
     * 
     * @return segnatura
     */
    public java.lang.String[] getSegnatura() {
        return segnatura;
    }


    /**
     * Sets the segnatura value for this DocumentoStore.
     * 
     * @param segnatura
     */
    public void setSegnatura(java.lang.String[] segnatura) {
        this.segnatura = segnatura;
    }


    /**
     * Gets the stato value for this DocumentoStore.
     * 
     * @return stato
     */
    public it.inps.soa.DocumentStore.data.DocumentStoreLastState getStato() {
        return stato;
    }


    /**
     * Sets the stato value for this DocumentoStore.
     * 
     * @param stato
     */
    public void setStato(it.inps.soa.DocumentStore.data.DocumentStoreLastState stato) {
        this.stato = stato;
    }


    /**
     * Gets the versione value for this DocumentoStore.
     * 
     * @return versione
     */
    public java.lang.Long getVersione() {
        return versione;
    }


    /**
     * Sets the versione value for this DocumentoStore.
     * 
     * @param versione
     */
    public void setVersione(java.lang.Long versione) {
        this.versione = versione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentoStore)) return false;
        DocumentoStore other = (DocumentoStore) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.containerId==null && other.getContainerId()==null) || 
             (this.containerId!=null &&
              java.util.Arrays.equals(this.containerId, other.getContainerId()))) &&
            ((this.segnatura==null && other.getSegnatura()==null) || 
             (this.segnatura!=null &&
              java.util.Arrays.equals(this.segnatura, other.getSegnatura()))) &&
            ((this.stato==null && other.getStato()==null) || 
             (this.stato!=null &&
              this.stato.equals(other.getStato()))) &&
            ((this.versione==null && other.getVersione()==null) || 
             (this.versione!=null &&
              this.versione.equals(other.getVersione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getContainerId() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContainerId());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContainerId(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSegnatura() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegnatura());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegnatura(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStato() != null) {
            _hashCode += getStato().hashCode();
        }
        if (getVersione() != null) {
            _hashCode += getVersione().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentoStore.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoStore"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("containerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ContainerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnatura");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Segnatura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Stato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentStoreLastState"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Versione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
