/**
 * ArchiviaDocumentoPerVersioneRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class ArchiviaDocumentoPerVersioneRequest  extends it.inps.soa.DocumentStore.data.ArchiviaDocumentoRequest  implements java.io.Serializable {
    private java.lang.String chiaveDocumentale;

    private java.lang.String documentId;

    private org.apache.axis.types.UnsignedInt versione;

    public ArchiviaDocumentoPerVersioneRequest() {
    }

    public ArchiviaDocumentoPerVersioneRequest(
           byte[] documento,
           it.inps.soa.DocumentStore.data.Metadati metadati,
           java.lang.String chiaveDocumentale,
           java.lang.String documentId,
           org.apache.axis.types.UnsignedInt versione) {
        super(
            documento,
            metadati);
        this.chiaveDocumentale = chiaveDocumentale;
        this.documentId = documentId;
        this.versione = versione;
    }


    /**
     * Gets the chiaveDocumentale value for this ArchiviaDocumentoPerVersioneRequest.
     * 
     * @return chiaveDocumentale
     */
    public java.lang.String getChiaveDocumentale() {
        return chiaveDocumentale;
    }


    /**
     * Sets the chiaveDocumentale value for this ArchiviaDocumentoPerVersioneRequest.
     * 
     * @param chiaveDocumentale
     */
    public void setChiaveDocumentale(java.lang.String chiaveDocumentale) {
        this.chiaveDocumentale = chiaveDocumentale;
    }


    /**
     * Gets the documentId value for this ArchiviaDocumentoPerVersioneRequest.
     * 
     * @return documentId
     */
    public java.lang.String getDocumentId() {
        return documentId;
    }


    /**
     * Sets the documentId value for this ArchiviaDocumentoPerVersioneRequest.
     * 
     * @param documentId
     */
    public void setDocumentId(java.lang.String documentId) {
        this.documentId = documentId;
    }


    /**
     * Gets the versione value for this ArchiviaDocumentoPerVersioneRequest.
     * 
     * @return versione
     */
    public org.apache.axis.types.UnsignedInt getVersione() {
        return versione;
    }


    /**
     * Sets the versione value for this ArchiviaDocumentoPerVersioneRequest.
     * 
     * @param versione
     */
    public void setVersione(org.apache.axis.types.UnsignedInt versione) {
        this.versione = versione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArchiviaDocumentoPerVersioneRequest)) return false;
        ArchiviaDocumentoPerVersioneRequest other = (ArchiviaDocumentoPerVersioneRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.chiaveDocumentale==null && other.getChiaveDocumentale()==null) || 
             (this.chiaveDocumentale!=null &&
              this.chiaveDocumentale.equals(other.getChiaveDocumentale()))) &&
            ((this.documentId==null && other.getDocumentId()==null) || 
             (this.documentId!=null &&
              this.documentId.equals(other.getDocumentId()))) &&
            ((this.versione==null && other.getVersione()==null) || 
             (this.versione!=null &&
              this.versione.equals(other.getVersione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getChiaveDocumentale() != null) {
            _hashCode += getChiaveDocumentale().hashCode();
        }
        if (getDocumentId() != null) {
            _hashCode += getDocumentId().hashCode();
        }
        if (getVersione() != null) {
            _hashCode += getVersione().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArchiviaDocumentoPerVersioneRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerVersioneRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chiaveDocumentale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ChiaveDocumentale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Versione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "unsignedInt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
