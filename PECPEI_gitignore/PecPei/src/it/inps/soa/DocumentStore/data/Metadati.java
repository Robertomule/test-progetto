/**
 * Metadati.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class Metadati  implements java.io.Serializable {
    private java.lang.String classeDocumentale;

    private java.lang.Integer contentLenght;

    private java.lang.String contentType;

    private java.util.Calendar dataCreazione;

    private java.util.Calendar dataScadenza;

    private java.lang.String datiXml;

    private java.lang.String descrizione;

    private com.microsoft.schemas._2003._10.Serialization.Arrays.ArrayOfKeyValueOfstringstringKeyValueOfstringstring[] extendedMetadati;

    private java.lang.Integer firmato;

    private java.lang.String originalFileName;

    private it.inps.soa.DocumentStore.data.DocumentStoreType tipologiaArchivio;

    public Metadati() {
    }

    public Metadati(
           java.lang.String classeDocumentale,
           java.lang.Integer contentLenght,
           java.lang.String contentType,
           java.util.Calendar dataCreazione,
           java.util.Calendar dataScadenza,
           java.lang.String datiXml,
           java.lang.String descrizione,
           com.microsoft.schemas._2003._10.Serialization.Arrays.ArrayOfKeyValueOfstringstringKeyValueOfstringstring[] extendedMetadati,
           java.lang.Integer firmato,
           java.lang.String originalFileName,
           it.inps.soa.DocumentStore.data.DocumentStoreType tipologiaArchivio) {
           this.classeDocumentale = classeDocumentale;
           this.contentLenght = contentLenght;
           this.contentType = contentType;
           this.dataCreazione = dataCreazione;
           this.dataScadenza = dataScadenza;
           this.datiXml = datiXml;
           this.descrizione = descrizione;
           this.extendedMetadati = extendedMetadati;
           this.firmato = firmato;
           this.originalFileName = originalFileName;
           this.tipologiaArchivio = tipologiaArchivio;
    }


    /**
     * Gets the classeDocumentale value for this Metadati.
     * 
     * @return classeDocumentale
     */
    public java.lang.String getClasseDocumentale() {
        return classeDocumentale;
    }


    /**
     * Sets the classeDocumentale value for this Metadati.
     * 
     * @param classeDocumentale
     */
    public void setClasseDocumentale(java.lang.String classeDocumentale) {
        this.classeDocumentale = classeDocumentale;
    }


    /**
     * Gets the contentLenght value for this Metadati.
     * 
     * @return contentLenght
     */
    public java.lang.Integer getContentLenght() {
        return contentLenght;
    }


    /**
     * Sets the contentLenght value for this Metadati.
     * 
     * @param contentLenght
     */
    public void setContentLenght(java.lang.Integer contentLenght) {
        this.contentLenght = contentLenght;
    }


    /**
     * Gets the contentType value for this Metadati.
     * 
     * @return contentType
     */
    public java.lang.String getContentType() {
        return contentType;
    }


    /**
     * Sets the contentType value for this Metadati.
     * 
     * @param contentType
     */
    public void setContentType(java.lang.String contentType) {
        this.contentType = contentType;
    }


    /**
     * Gets the dataCreazione value for this Metadati.
     * 
     * @return dataCreazione
     */
    public java.util.Calendar getDataCreazione() {
        return dataCreazione;
    }


    /**
     * Sets the dataCreazione value for this Metadati.
     * 
     * @param dataCreazione
     */
    public void setDataCreazione(java.util.Calendar dataCreazione) {
        this.dataCreazione = dataCreazione;
    }


    /**
     * Gets the dataScadenza value for this Metadati.
     * 
     * @return dataScadenza
     */
    public java.util.Calendar getDataScadenza() {
        return dataScadenza;
    }


    /**
     * Sets the dataScadenza value for this Metadati.
     * 
     * @param dataScadenza
     */
    public void setDataScadenza(java.util.Calendar dataScadenza) {
        this.dataScadenza = dataScadenza;
    }


    /**
     * Gets the datiXml value for this Metadati.
     * 
     * @return datiXml
     */
    public java.lang.String getDatiXml() {
        return datiXml;
    }


    /**
     * Sets the datiXml value for this Metadati.
     * 
     * @param datiXml
     */
    public void setDatiXml(java.lang.String datiXml) {
        this.datiXml = datiXml;
    }


    /**
     * Gets the descrizione value for this Metadati.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this Metadati.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the extendedMetadati value for this Metadati.
     * 
     * @return extendedMetadati
     */
    public com.microsoft.schemas._2003._10.Serialization.Arrays.ArrayOfKeyValueOfstringstringKeyValueOfstringstring[] getExtendedMetadati() {
        return extendedMetadati;
    }


    /**
     * Sets the extendedMetadati value for this Metadati.
     * 
     * @param extendedMetadati
     */
    public void setExtendedMetadati(com.microsoft.schemas._2003._10.Serialization.Arrays.ArrayOfKeyValueOfstringstringKeyValueOfstringstring[] extendedMetadati) {
        this.extendedMetadati = extendedMetadati;
    }


    /**
     * Gets the firmato value for this Metadati.
     * 
     * @return firmato
     */
    public java.lang.Integer getFirmato() {
        return firmato;
    }


    /**
     * Sets the firmato value for this Metadati.
     * 
     * @param firmato
     */
    public void setFirmato(java.lang.Integer firmato) {
        this.firmato = firmato;
    }


    /**
     * Gets the originalFileName value for this Metadati.
     * 
     * @return originalFileName
     */
    public java.lang.String getOriginalFileName() {
        return originalFileName;
    }


    /**
     * Sets the originalFileName value for this Metadati.
     * 
     * @param originalFileName
     */
    public void setOriginalFileName(java.lang.String originalFileName) {
        this.originalFileName = originalFileName;
    }


    /**
     * Gets the tipologiaArchivio value for this Metadati.
     * 
     * @return tipologiaArchivio
     */
    public it.inps.soa.DocumentStore.data.DocumentStoreType getTipologiaArchivio() {
        return tipologiaArchivio;
    }


    /**
     * Sets the tipologiaArchivio value for this Metadati.
     * 
     * @param tipologiaArchivio
     */
    public void setTipologiaArchivio(it.inps.soa.DocumentStore.data.DocumentStoreType tipologiaArchivio) {
        this.tipologiaArchivio = tipologiaArchivio;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Metadati)) return false;
        Metadati other = (Metadati) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.classeDocumentale==null && other.getClasseDocumentale()==null) || 
             (this.classeDocumentale!=null &&
              this.classeDocumentale.equals(other.getClasseDocumentale()))) &&
            ((this.contentLenght==null && other.getContentLenght()==null) || 
             (this.contentLenght!=null &&
              this.contentLenght.equals(other.getContentLenght()))) &&
            ((this.contentType==null && other.getContentType()==null) || 
             (this.contentType!=null &&
              this.contentType.equals(other.getContentType()))) &&
            ((this.dataCreazione==null && other.getDataCreazione()==null) || 
             (this.dataCreazione!=null &&
              this.dataCreazione.equals(other.getDataCreazione()))) &&
            ((this.dataScadenza==null && other.getDataScadenza()==null) || 
             (this.dataScadenza!=null &&
              this.dataScadenza.equals(other.getDataScadenza()))) &&
            ((this.datiXml==null && other.getDatiXml()==null) || 
             (this.datiXml!=null &&
              this.datiXml.equals(other.getDatiXml()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.extendedMetadati==null && other.getExtendedMetadati()==null) || 
             (this.extendedMetadati!=null &&
              java.util.Arrays.equals(this.extendedMetadati, other.getExtendedMetadati()))) &&
            ((this.firmato==null && other.getFirmato()==null) || 
             (this.firmato!=null &&
              this.firmato.equals(other.getFirmato()))) &&
            ((this.originalFileName==null && other.getOriginalFileName()==null) || 
             (this.originalFileName!=null &&
              this.originalFileName.equals(other.getOriginalFileName()))) &&
            ((this.tipologiaArchivio==null && other.getTipologiaArchivio()==null) || 
             (this.tipologiaArchivio!=null &&
              this.tipologiaArchivio.equals(other.getTipologiaArchivio())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClasseDocumentale() != null) {
            _hashCode += getClasseDocumentale().hashCode();
        }
        if (getContentLenght() != null) {
            _hashCode += getContentLenght().hashCode();
        }
        if (getContentType() != null) {
            _hashCode += getContentType().hashCode();
        }
        if (getDataCreazione() != null) {
            _hashCode += getDataCreazione().hashCode();
        }
        if (getDataScadenza() != null) {
            _hashCode += getDataScadenza().hashCode();
        }
        if (getDatiXml() != null) {
            _hashCode += getDatiXml().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getExtendedMetadati() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExtendedMetadati());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExtendedMetadati(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFirmato() != null) {
            _hashCode += getFirmato().hashCode();
        }
        if (getOriginalFileName() != null) {
            _hashCode += getOriginalFileName().hashCode();
        }
        if (getTipologiaArchivio() != null) {
            _hashCode += getTipologiaArchivio().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Metadati.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Metadati"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classeDocumentale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ClasseDocumentale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentLenght");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ContentLenght"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ContentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataCreazione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DataCreazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataScadenza");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DataScadenza"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datiXml");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DatiXml"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extendedMetadati");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ExtendedMetadati"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", ">ArrayOfKeyValueOfstringstring>KeyValueOfstringstring"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "KeyValueOfstringstring"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firmato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Firmato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalFileName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "OriginalFileName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipologiaArchivio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "TipologiaArchivio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentStoreType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
