/**
 * ListaDocumentiVersionatiRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class ListaDocumentiVersionatiRequest  extends it.inps.soa.DocumentStore.data.DocumentStoreRequestBase  implements java.io.Serializable {
    private java.lang.Integer endRow;

    private java.lang.Integer startRow;

    private java.lang.String storeId;

    public ListaDocumentiVersionatiRequest() {
    }

    public ListaDocumentiVersionatiRequest(
           java.lang.Integer endRow,
           java.lang.Integer startRow,
           java.lang.String storeId) {
        this.endRow = endRow;
        this.startRow = startRow;
        this.storeId = storeId;
    }


    /**
     * Gets the endRow value for this ListaDocumentiVersionatiRequest.
     * 
     * @return endRow
     */
    public java.lang.Integer getEndRow() {
        return endRow;
    }


    /**
     * Sets the endRow value for this ListaDocumentiVersionatiRequest.
     * 
     * @param endRow
     */
    public void setEndRow(java.lang.Integer endRow) {
        this.endRow = endRow;
    }


    /**
     * Gets the startRow value for this ListaDocumentiVersionatiRequest.
     * 
     * @return startRow
     */
    public java.lang.Integer getStartRow() {
        return startRow;
    }


    /**
     * Sets the startRow value for this ListaDocumentiVersionatiRequest.
     * 
     * @param startRow
     */
    public void setStartRow(java.lang.Integer startRow) {
        this.startRow = startRow;
    }


    /**
     * Gets the storeId value for this ListaDocumentiVersionatiRequest.
     * 
     * @return storeId
     */
    public java.lang.String getStoreId() {
        return storeId;
    }


    /**
     * Sets the storeId value for this ListaDocumentiVersionatiRequest.
     * 
     * @param storeId
     */
    public void setStoreId(java.lang.String storeId) {
        this.storeId = storeId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListaDocumentiVersionatiRequest)) return false;
        ListaDocumentiVersionatiRequest other = (ListaDocumentiVersionatiRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.endRow==null && other.getEndRow()==null) || 
             (this.endRow!=null &&
              this.endRow.equals(other.getEndRow()))) &&
            ((this.startRow==null && other.getStartRow()==null) || 
             (this.startRow!=null &&
              this.startRow.equals(other.getStartRow()))) &&
            ((this.storeId==null && other.getStoreId()==null) || 
             (this.storeId!=null &&
              this.storeId.equals(other.getStoreId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEndRow() != null) {
            _hashCode += getEndRow().hashCode();
        }
        if (getStartRow() != null) {
            _hashCode += getStartRow().hashCode();
        }
        if (getStoreId() != null) {
            _hashCode += getStoreId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListaDocumentiVersionatiRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiVersionatiRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endRow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "EndRow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startRow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "StartRow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "StoreId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
