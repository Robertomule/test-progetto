/**
 * DocumentoProtocollatoExistsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class DocumentoProtocollatoExistsResponse  implements java.io.Serializable {
    private java.lang.String documentId;

    private java.lang.String hash;

    private it.inps.soa.DocumentStore.data.InternalDocumentType internalDocumentType;

    public DocumentoProtocollatoExistsResponse() {
    }

    public DocumentoProtocollatoExistsResponse(
           java.lang.String documentId,
           java.lang.String hash,
           it.inps.soa.DocumentStore.data.InternalDocumentType internalDocumentType) {
           this.documentId = documentId;
           this.hash = hash;
           this.internalDocumentType = internalDocumentType;
    }


    /**
     * Gets the documentId value for this DocumentoProtocollatoExistsResponse.
     * 
     * @return documentId
     */
    public java.lang.String getDocumentId() {
        return documentId;
    }


    /**
     * Sets the documentId value for this DocumentoProtocollatoExistsResponse.
     * 
     * @param documentId
     */
    public void setDocumentId(java.lang.String documentId) {
        this.documentId = documentId;
    }


    /**
     * Gets the hash value for this DocumentoProtocollatoExistsResponse.
     * 
     * @return hash
     */
    public java.lang.String getHash() {
        return hash;
    }


    /**
     * Sets the hash value for this DocumentoProtocollatoExistsResponse.
     * 
     * @param hash
     */
    public void setHash(java.lang.String hash) {
        this.hash = hash;
    }


    /**
     * Gets the internalDocumentType value for this DocumentoProtocollatoExistsResponse.
     * 
     * @return internalDocumentType
     */
    public it.inps.soa.DocumentStore.data.InternalDocumentType getInternalDocumentType() {
        return internalDocumentType;
    }


    /**
     * Sets the internalDocumentType value for this DocumentoProtocollatoExistsResponse.
     * 
     * @param internalDocumentType
     */
    public void setInternalDocumentType(it.inps.soa.DocumentStore.data.InternalDocumentType internalDocumentType) {
        this.internalDocumentType = internalDocumentType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentoProtocollatoExistsResponse)) return false;
        DocumentoProtocollatoExistsResponse other = (DocumentoProtocollatoExistsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.documentId==null && other.getDocumentId()==null) || 
             (this.documentId!=null &&
              this.documentId.equals(other.getDocumentId()))) &&
            ((this.hash==null && other.getHash()==null) || 
             (this.hash!=null &&
              this.hash.equals(other.getHash()))) &&
            ((this.internalDocumentType==null && other.getInternalDocumentType()==null) || 
             (this.internalDocumentType!=null &&
              this.internalDocumentType.equals(other.getInternalDocumentType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDocumentId() != null) {
            _hashCode += getDocumentId().hashCode();
        }
        if (getHash() != null) {
            _hashCode += getHash().hashCode();
        }
        if (getInternalDocumentType() != null) {
            _hashCode += getInternalDocumentType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentoProtocollatoExistsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoProtocollatoExistsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hash");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Hash"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("internalDocumentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "InternalDocumentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "InternalDocumentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
