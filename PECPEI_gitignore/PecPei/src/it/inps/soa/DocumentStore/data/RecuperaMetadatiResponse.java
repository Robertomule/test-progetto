/**
 * RecuperaMetadatiResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class RecuperaMetadatiResponse  extends it.inps.soa.DocumentStore.data.DocumentStoreResponseBase  implements java.io.Serializable {
    private it.inps.soa.DocumentStore.data.Documento documento;

    public RecuperaMetadatiResponse() {
    }

    public RecuperaMetadatiResponse(
           java.lang.String descrizione,
           it.inps.soa.DocumentStore.data.EsitoTypeEnum esito,
           it.inps.soa.DocumentStore.data.Documento documento) {
        super(
            descrizione,
            esito);
        this.documento = documento;
    }


    /**
     * Gets the documento value for this RecuperaMetadatiResponse.
     * 
     * @return documento
     */
    public it.inps.soa.DocumentStore.data.Documento getDocumento() {
        return documento;
    }


    /**
     * Sets the documento value for this RecuperaMetadatiResponse.
     * 
     * @param documento
     */
    public void setDocumento(it.inps.soa.DocumentStore.data.Documento documento) {
        this.documento = documento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RecuperaMetadatiResponse)) return false;
        RecuperaMetadatiResponse other = (RecuperaMetadatiResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.documento==null && other.getDocumento()==null) || 
             (this.documento!=null &&
              this.documento.equals(other.getDocumento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDocumento() != null) {
            _hashCode += getDocumento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RecuperaMetadatiResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "RecuperaMetadatiResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Documento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Documento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
