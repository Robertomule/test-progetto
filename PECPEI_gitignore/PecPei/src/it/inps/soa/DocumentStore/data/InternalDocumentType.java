/**
 * InternalDocumentType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class InternalDocumentType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected InternalDocumentType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Allegato = "Allegato";
    public static final java.lang.String _Ricevuta = "Ricevuta";
    public static final java.lang.String _Sintesi = "Sintesi";
    public static final java.lang.String _Primario = "Primario";
    public static final java.lang.String _Correlato = "Correlato";
    public static final InternalDocumentType Allegato = new InternalDocumentType(_Allegato);
    public static final InternalDocumentType Ricevuta = new InternalDocumentType(_Ricevuta);
    public static final InternalDocumentType Sintesi = new InternalDocumentType(_Sintesi);
    public static final InternalDocumentType Primario = new InternalDocumentType(_Primario);
    public static final InternalDocumentType Correlato = new InternalDocumentType(_Correlato);
    public java.lang.String getValue() { return _value_;}
    public static InternalDocumentType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        InternalDocumentType enumeration = (InternalDocumentType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static InternalDocumentType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InternalDocumentType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "InternalDocumentType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
