/**
 * Documento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class Documento  implements java.io.Serializable {
    private java.lang.String documentId;

    private it.inps.soa.DocumentStore.data.InternalDocumentType documentType;

    private it.inps.soa.DocumentStore.data.Metadati metadati;

    public Documento() {
    }

    public Documento(
           java.lang.String documentId,
           it.inps.soa.DocumentStore.data.InternalDocumentType documentType,
           it.inps.soa.DocumentStore.data.Metadati metadati) {
           this.documentId = documentId;
           this.documentType = documentType;
           this.metadati = metadati;
    }


    /**
     * Gets the documentId value for this Documento.
     * 
     * @return documentId
     */
    public java.lang.String getDocumentId() {
        return documentId;
    }


    /**
     * Sets the documentId value for this Documento.
     * 
     * @param documentId
     */
    public void setDocumentId(java.lang.String documentId) {
        this.documentId = documentId;
    }


    /**
     * Gets the documentType value for this Documento.
     * 
     * @return documentType
     */
    public it.inps.soa.DocumentStore.data.InternalDocumentType getDocumentType() {
        return documentType;
    }


    /**
     * Sets the documentType value for this Documento.
     * 
     * @param documentType
     */
    public void setDocumentType(it.inps.soa.DocumentStore.data.InternalDocumentType documentType) {
        this.documentType = documentType;
    }


    /**
     * Gets the metadati value for this Documento.
     * 
     * @return metadati
     */
    public it.inps.soa.DocumentStore.data.Metadati getMetadati() {
        return metadati;
    }


    /**
     * Sets the metadati value for this Documento.
     * 
     * @param metadati
     */
    public void setMetadati(it.inps.soa.DocumentStore.data.Metadati metadati) {
        this.metadati = metadati;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Documento)) return false;
        Documento other = (Documento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.documentId==null && other.getDocumentId()==null) || 
             (this.documentId!=null &&
              this.documentId.equals(other.getDocumentId()))) &&
            ((this.documentType==null && other.getDocumentType()==null) || 
             (this.documentType!=null &&
              this.documentType.equals(other.getDocumentType()))) &&
            ((this.metadati==null && other.getMetadati()==null) || 
             (this.metadati!=null &&
              this.metadati.equals(other.getMetadati())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDocumentId() != null) {
            _hashCode += getDocumentId().hashCode();
        }
        if (getDocumentType() != null) {
            _hashCode += getDocumentType().hashCode();
        }
        if (getMetadati() != null) {
            _hashCode += getMetadati().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Documento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Documento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "InternalDocumentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metadati");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Metadati"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Metadati"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
