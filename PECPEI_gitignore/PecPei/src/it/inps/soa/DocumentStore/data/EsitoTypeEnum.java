/**
 * EsitoTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.DocumentStore.data;

public class EsitoTypeEnum implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EsitoTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Ok = "Ok";
    public static final java.lang.String _ViolazioneDiSicurezza = "ViolazioneDiSicurezza";
    public static final java.lang.String _RichiestaNonValida = "RichiestaNonValida";
    public static final java.lang.String _Errore = "Errore";
    public static final java.lang.String _RisorsaNonTrovata = "RisorsaNonTrovata";
    public static final java.lang.String _QuotaSuperata = "QuotaSuperata";
    public static final java.lang.String _DimensioniFileSuperate = "DimensioniFileSuperate";
    public static final java.lang.String _TipoFileInvalido = "TipoFileInvalido";
    public static final java.lang.String _TokenScaduto = "TokenScaduto";
    public static final java.lang.String _OperazioneNonAutorizzata = "OperazioneNonAutorizzata";
    public static final java.lang.String _TipoDocumentoNonVersionabile = "TipoDocumentoNonVersionabile";
    public static final EsitoTypeEnum Ok = new EsitoTypeEnum(_Ok);
    public static final EsitoTypeEnum ViolazioneDiSicurezza = new EsitoTypeEnum(_ViolazioneDiSicurezza);
    public static final EsitoTypeEnum RichiestaNonValida = new EsitoTypeEnum(_RichiestaNonValida);
    public static final EsitoTypeEnum Errore = new EsitoTypeEnum(_Errore);
    public static final EsitoTypeEnum RisorsaNonTrovata = new EsitoTypeEnum(_RisorsaNonTrovata);
    public static final EsitoTypeEnum QuotaSuperata = new EsitoTypeEnum(_QuotaSuperata);
    public static final EsitoTypeEnum DimensioniFileSuperate = new EsitoTypeEnum(_DimensioniFileSuperate);
    public static final EsitoTypeEnum TipoFileInvalido = new EsitoTypeEnum(_TipoFileInvalido);
    public static final EsitoTypeEnum TokenScaduto = new EsitoTypeEnum(_TokenScaduto);
    public static final EsitoTypeEnum OperazioneNonAutorizzata = new EsitoTypeEnum(_OperazioneNonAutorizzata);
    public static final EsitoTypeEnum TipoDocumentoNonVersionabile = new EsitoTypeEnum(_TipoDocumentoNonVersionabile);
    public java.lang.String getValue() { return _value_;}
    public static EsitoTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EsitoTypeEnum enumeration = (EsitoTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EsitoTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EsitoTypeEnum.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "EsitoTypeEnum"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
