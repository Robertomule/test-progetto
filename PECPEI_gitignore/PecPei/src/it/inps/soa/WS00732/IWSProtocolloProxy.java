package it.inps.soa.WS00732;

import it.eustema.inps.utility.ConfigProp;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;

import org.apache.axis.AxisFault;
import org.apache.axis.message.SOAPHeaderElement;

import org.tempuri.DNA_BASICHTTP_BindingStub;

public class IWSProtocolloProxy implements it.inps.soa.WS00732.IWSProtocollo {
	private String _endpoint = null;
	private it.inps.soa.WS00732.IWSProtocollo iWSProtocollo = null;

	public IWSProtocolloProxy() {
		_initIWSProtocolloProxy();
	}

	public IWSProtocolloProxy(String endpoint) {
		_endpoint = endpoint;
		_initIWSProtocolloProxy();
	}

	private void _initIWSProtocolloProxy() {
		try {
			iWSProtocollo = (new org.tempuri.WSProtocolloLocator()).getDNA_BASICHTTP_Binding();
			if (iWSProtocollo != null) {
				if (_endpoint != null)
					((javax.xml.rpc.Stub)iWSProtocollo)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
				else
					_endpoint = (String)((javax.xml.rpc.Stub)iWSProtocollo)._getProperty("javax.xml.rpc.service.endpoint.address");
			}

		}
		catch (javax.xml.rpc.ServiceException serviceException) {}
	}

	public String getEndpoint() {
		return _endpoint;
	}

	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (iWSProtocollo != null)
			((javax.xml.rpc.Stub)iWSProtocollo)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

	}

	public it.inps.soa.WS00732.IWSProtocollo getIWSProtocollo() {
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo;
	}

	public java.lang.String getVersion() throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.getVersion();
	}

	public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInEntrata(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInEntrata(codiceAOO, protocolloEntrataRequest);
	}

	public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInUscita(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInUscita(codiceAOO, protocolloUscitaRequest);
	}

	public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInEntrataEdAllega(it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInEntrataEdAllega(protocolloEntrataRequest, segnaturaPrincipale);
	}

	public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInUscitaEdAllegaCrossAOO(it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest, java.lang.String segnaturaPrincipale, java.lang.String AOOdelProtocollo) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInUscitaEdAllegaCrossAOO(protocolloUscitaRequest, segnaturaPrincipale, AOOdelProtocollo);
	}

	public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInEntrataEdAllegaCrossAOO(it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest, java.lang.String segnaturaPrincipale, java.lang.String AOOdelProtocollo) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInEntrataEdAllegaCrossAOO(protocolloEntrataRequest, segnaturaPrincipale, AOOdelProtocollo);
	}

	public it.inps.soa.WS00732.data.ListaAllegatiResponse estraiListaAllegati(java.lang.String segnatura) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.estraiListaAllegati(segnatura);
	}

	public it.inps.soa.WS00732.data.ListaAllegatiResponse estraiListaCatenaTrasferimenti(java.lang.String segnatura) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.estraiListaCatenaTrasferimenti(segnatura);
	}

	public it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse estraiCatenaDocumentale(java.lang.String segnatura) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.estraiCatenaDocumentale(segnatura);
	}

	public it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse creaCatenaDocumentale(java.lang.String segnaturaPadre, java.lang.String segnaturaFiglia) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.creaCatenaDocumentale(segnaturaPadre, segnaturaFiglia);
	}

	public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInUscitaEdAllega(it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInUscitaEdAllega(protocolloUscitaRequest, segnaturaPrincipale);
	}

	public it.inps.soa.WS00732.data.ModificaResponse modificaMetadatiProtocollo(java.lang.String segnatura, it.inps.soa.WS00732.data.MetadatiProtocollo metadatiProtocollo) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.modificaMetadatiProtocollo(segnatura, metadatiProtocollo);
	}

	public it.inps.soa.WS00732.data.AnnullaResponse annullaProtocollo(java.lang.String segnatura, it.inps.soa.WS00732.data.AnnullaProtocolloRequest annullaProtocolloRequest) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.annullaProtocollo(segnatura, annullaProtocolloRequest);
	}

	public it.inps.soa.WS00732.data.MetadatiResponse estraiMetadatiDocumento(java.lang.String segnatura) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.estraiMetadatiDocumento(segnatura);
	}

	public it.inps.soa.WS00732.data.GeneraRicevutaResponse generaRicevutaProtocollo(java.lang.String segnatura) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.generaRicevutaProtocollo(segnatura);
	}

	public it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse generaRicevutaProtocolloBarcode(java.lang.String segnatura) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.generaRicevutaProtocolloBarcode(segnatura);
	}

	public it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse associaDocumentoPrimario(it.inps.soa.WS00732.data.DocumentoPrimarioRequest documentoPrimarioRequest) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.associaDocumentoPrimario(documentoPrimarioRequest);
	}

	public it.inps.soa.WS00732.data.AllegatiResponse aggiungiAllegatiContestuali(it.inps.soa.WS00732.data.AllegatiContestualiRequest allegatiContestualiRequest) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.aggiungiAllegatiContestuali(allegatiContestualiRequest);
	}

	public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInEntrata(it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.trasferisciProtocolloInEntrata(trasferisciRequest);
	}

	public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInUscita(it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.trasferisciProtocolloInUscita(trasferisciRequest);
	}

	public it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse richiestaProtocollazioneInEntrata(it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.richiestaProtocollazioneInEntrata(request);
	}

	public it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse richiestaInteroperabilitaInEntrata(it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.richiestaInteroperabilitaInEntrata(request);
	}

	public it.inps.soa.WS00732.data.ProtocollaNAIResponse protocollaInEntrataNAI(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataNAIRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInEntrataNAI(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse creaFascicoloProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.creaFascicoloProcedimentale(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse creaCatenaFascicolazione(it.inps.soa.WS00732.data.CreaCatenaFascicolazioneRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.creaCatenaFascicolazione(request);
	}

	public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInEntrataConFascicolazioneProcedimentale(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConCatenaDiFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInEntrataConCatenaDiFascicolazioneProcedimentale(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInUscitaConFascicolazioneProcedimentale(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConCatenaDiFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInUscitaConCatenaDiFascicolazioneProcedimentale(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressa(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInEntrataConFascicolazionePregressa(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressaEVariazioneDiStato(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInEntrataConFascicolazionePregressaEVariazioneDiStato(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressa(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInUscitaConFascicolazionePregressa(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressaEVariazioneDiStato(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInUscitaConFascicolazionePregressaEVariazioneDiStato(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.DocumentiFascicoloResponse estraiDocumentiFascicolo(java.lang.String codiceFascicolo) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.estraiDocumentiFascicolo(codiceFascicolo);
	}

	public it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse fascicolaDocumentoProtocollato(it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.fascicolaDocumentoProtocollato(request);
	}

	public it.inps.soa.WS00732.data.CatenaFascicolazioneResponse estraiCatenaFascicolazione(java.lang.String codiceFascicolo) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.estraiCatenaFascicolazione(codiceFascicolo);
	}

	public it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse variaEsitoProcedimento(it.inps.soa.WS00732.data.VariaEsitoProcedimentoRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.variaEsitoProcedimento(request);
	}

	public it.inps.soa.WS00732.data.RichiestaCRMResponse richiestaCRM(it.inps.soa.WS00732.data.RichiestaCRMRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.richiestaCRM(request);
	}

	public it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse protocollaInEntrataESpedisci(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloEntrataESpedisciRequest protocollaInEntrataESpedisciRequest) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInEntrataESpedisci(codiceAOO, protocollaInEntrataESpedisciRequest);
	}

	public it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse protocollaInUscitaESpedisci(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloUscitaESpedisciRequest protocolloUscitaESpedisciRequest) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaInUscitaESpedisci(codiceAOO, protocolloUscitaESpedisciRequest);
	}

	public it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse creaFascicoloNonProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.creaFascicoloNonProcedimentale(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.ContainerResponse creaContainer(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ContainerRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.creaContainer(codiceAOO, request);
	}

	public it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse associaContainerAFascicolo(it.inps.soa.WS00732.data.AssociaContainerAFascicoloRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.associaContainerAFascicolo(request);
	}

	public it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse chiudiFascicoloNonProcedimentale(it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.chiudiFascicoloNonProcedimentale(request);
	}

	public it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse riapriFascicoloNonProcedimentale(it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.riapriFascicoloNonProcedimentale(request);
	}

	public it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse estraiPraticaDaFascicolo(java.lang.String codiceFascicolo) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.estraiPraticaDaFascicolo(codiceFascicolo);
	}

	public it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse estraiPratica(java.lang.String codicePratica, java.lang.String tipoCodicePratica) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.estraiPratica(codicePratica, tipoCodicePratica);
	}

	public it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse rifiutaRichiestaPECInUscita(java.lang.String messageId, java.lang.String motivazione) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.rifiutaRichiestaPECInUscita(messageId, motivazione);
	}

	public it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse autorizzaRichiestaPECInUscita(java.lang.String messageId) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.autorizzaRichiestaPECInUscita(messageId);
	}

	public it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse protocollaPECInUscita(java.lang.String messageId) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.protocollaPECInUscita(messageId);
	}

	public it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse registraStatoInvioPECInUscita(it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.registraStatoInvioPECInUscita(request);
	}

	public org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse registraStatoConsegnaPECInUscita(it.inps.soa.WS00732.data.RegistraStatoConsegnaPECInUscitaRequest request) throws java.rmi.RemoteException{
		if (iWSProtocollo == null)
			_initIWSProtocolloProxy();
		return iWSProtocollo.registraStatoConsegnaPECInUscita(request);
	}

	public static DNA_BASICHTTP_BindingStub getProxyHelper(){

		/*
		TEST:
		Appname: AB00445
		Appkey: AB00445
		URL del servizio: https://ws.svil.inps/Ws.Net/WSProtocollo

		COLLAUDO:
		Appname: AB00445
		Appkey: 0zxehsxknoj2wwcyq9iwa1ievxss2jn2
		URL del servizio: https://ws.ser-collaudo.inps/Ws.Net/WSProtocollo
		 */

		 DNA_BASICHTTP_BindingStub prx=null;

		try {

			URL url = new URL(ConfigProp.endPointWsProtocollo);
			prx = new DNA_BASICHTTP_BindingStub(url,null);

			SOAPHeaderElement header = new SOAPHeaderElement("http://inps.it/", "Identity");
			header.addNamespaceDeclaration("inps", "http://inps.it/");

			SOAPElement nodeName = header.addChildElement("AppName");
			nodeName.addTextNode(ConfigProp.appName);
			SOAPElement nodeKey = header.addChildElement("AppKey");
			nodeKey.addTextNode(ConfigProp.appKey);
			SOAPElement nodeUser = header.addChildElement("UserId");
			nodeUser.addTextNode(ConfigProp.userId);
			SOAPElement nodeIdProvider = header.addChildElement("IdentityProvider");
			nodeIdProvider.addTextNode(ConfigProp.identityProvider);

			//	  		SOAPElement nodeSession = header.addChildElement("SessionId");
			//	  		nodeSession.addTextNode("");
			//	  		SOAPElement nodeSequence = header.addChildElement("SequenceId");
			//	  		nodeSequence.addTextNode("");
			//	  		SOAPElement nodePerr = header.addChildElement("PeerHost");
			//	  		nodePerr.addTextNode("");
			//	  		
			//	  		SOAPElement nodeCorrelation = header.addChildElement("CorrelationId");
			//	  		nodeCorrelation.addTextNode("");

			prx.setHeader(header);
			//proxy._getCall().setTargetEndpointAddress(url);
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		}

		return prx;

	}

	//Se non passo per il data power
	public static DNA_BASICHTTP_BindingStub getProxyHelperNoDataPower(){
		/*
	TEST:
		Appname: AB00445
		Appkey: AB00445

		COLLAUDO:
		Appname: AB00445
		Appkey: 0zxehsxknoj2wwcyq9iwa1ievxss2jn2
		 */

		DNA_BASICHTTP_BindingStub prx=null;

		try {

			URL url = new URL(ConfigProp.endPointWsProtocollo);
			prx = new DNA_BASICHTTP_BindingStub(url,null);

			SOAPHeaderElement header = new SOAPHeaderElement("http://inps.it/", "Identity");
			header.addNamespaceDeclaration("inps", "http://inps.it/");
			SOAPElement nodeName = header.addChildElement("AppName","inps");
			nodeName.addTextNode("AB00445");
			SOAPElement nodeKey = header.addChildElement("AppKey","inps");
			nodeKey.addTextNode("0zxehsxknoj2wwcyq9iwa1ievxss2jn2");
			SOAPElement nodeUser = header.addChildElement("UserId","inps");
			nodeUser.addTextNode("regprot");
			SOAPElement nodeIdProvider = header.addChildElement("IdentityProvider","inps");
			nodeIdProvider.addTextNode("AD");
			SOAPElement nodeSession = header.addChildElement("SessionId");
			nodeSession.addTextNode("2F358F78-5782-4E7B-B645-396C7127A31D");
			SOAPElement nodeSequence = header.addChildElement("SequenceId");
			nodeSequence.addTextNode("0");
			SOAPElement nodePerr = header.addChildElement("PeerHost");
			nodePerr.addTextNode("127.0.0.1");
			SOAPElement nodeCorrelation = header.addChildElement("CorrelationId");
			nodeCorrelation.addTextNode("");
			SOAPElement nodeCoperation = header.addChildElement("OperationContextId");
			nodeCoperation.addTextNode("0");

			prx.setHeader(header);
			//proxy._getCall().setTargetEndpointAddress(url);
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		}

		return prx;

	}
}