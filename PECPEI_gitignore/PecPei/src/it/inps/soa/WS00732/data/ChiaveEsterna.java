/**
 * ChiaveEsterna.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ChiaveEsterna  implements java.io.Serializable {
    private java.lang.String externalKey;

    private java.lang.String externalKeyTypeCode;

    public ChiaveEsterna() {
    }

    public ChiaveEsterna(
           java.lang.String externalKey,
           java.lang.String externalKeyTypeCode) {
           this.externalKey = externalKey;
           this.externalKeyTypeCode = externalKeyTypeCode;
    }


    /**
     * Gets the externalKey value for this ChiaveEsterna.
     * 
     * @return externalKey
     */
    public java.lang.String getExternalKey() {
        return externalKey;
    }


    /**
     * Sets the externalKey value for this ChiaveEsterna.
     * 
     * @param externalKey
     */
    public void setExternalKey(java.lang.String externalKey) {
        this.externalKey = externalKey;
    }


    /**
     * Gets the externalKeyTypeCode value for this ChiaveEsterna.
     * 
     * @return externalKeyTypeCode
     */
    public java.lang.String getExternalKeyTypeCode() {
        return externalKeyTypeCode;
    }


    /**
     * Sets the externalKeyTypeCode value for this ChiaveEsterna.
     * 
     * @param externalKeyTypeCode
     */
    public void setExternalKeyTypeCode(java.lang.String externalKeyTypeCode) {
        this.externalKeyTypeCode = externalKeyTypeCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChiaveEsterna)) return false;
        ChiaveEsterna other = (ChiaveEsterna) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.externalKey==null && other.getExternalKey()==null) || 
             (this.externalKey!=null &&
              this.externalKey.equals(other.getExternalKey()))) &&
            ((this.externalKeyTypeCode==null && other.getExternalKeyTypeCode()==null) || 
             (this.externalKeyTypeCode!=null &&
              this.externalKeyTypeCode.equals(other.getExternalKeyTypeCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExternalKey() != null) {
            _hashCode += getExternalKey().hashCode();
        }
        if (getExternalKeyTypeCode() != null) {
            _hashCode += getExternalKeyTypeCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChiaveEsterna.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ChiaveEsterna"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ExternalKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalKeyTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ExternalKeyTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
