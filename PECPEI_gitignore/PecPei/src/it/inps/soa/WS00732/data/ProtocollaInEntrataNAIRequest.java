/**
 * ProtocollaInEntrataNAIRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ProtocollaInEntrataNAIRequest  extends it.inps.soa.WS00732.data.BaseDataRequest  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.NAI nai;

    private it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloInEntrata;

    public ProtocollaInEntrataNAIRequest() {
    }

    public ProtocollaInEntrataNAIRequest(
           java.lang.String requestId,
           it.inps.soa.WS00732.data.NAI nai,
           it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloInEntrata) {
        super(
            requestId);
        this.nai = nai;
        this.protocolloInEntrata = protocolloInEntrata;
    }


    /**
     * Gets the nai value for this ProtocollaInEntrataNAIRequest.
     * 
     * @return nai
     */
    public it.inps.soa.WS00732.data.NAI getNai() {
        return nai;
    }


    /**
     * Sets the nai value for this ProtocollaInEntrataNAIRequest.
     * 
     * @param nai
     */
    public void setNai(it.inps.soa.WS00732.data.NAI nai) {
        this.nai = nai;
    }


    /**
     * Gets the protocolloInEntrata value for this ProtocollaInEntrataNAIRequest.
     * 
     * @return protocolloInEntrata
     */
    public it.inps.soa.WS00732.data.ProtocolloEntrataRequest getProtocolloInEntrata() {
        return protocolloInEntrata;
    }


    /**
     * Sets the protocolloInEntrata value for this ProtocollaInEntrataNAIRequest.
     * 
     * @param protocolloInEntrata
     */
    public void setProtocolloInEntrata(it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloInEntrata) {
        this.protocolloInEntrata = protocolloInEntrata;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocollaInEntrataNAIRequest)) return false;
        ProtocollaInEntrataNAIRequest other = (ProtocollaInEntrataNAIRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.nai==null && other.getNai()==null) || 
             (this.nai!=null &&
              this.nai.equals(other.getNai()))) &&
            ((this.protocolloInEntrata==null && other.getProtocolloInEntrata()==null) || 
             (this.protocolloInEntrata!=null &&
              this.protocolloInEntrata.equals(other.getProtocolloInEntrata())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getNai() != null) {
            _hashCode += getNai().hashCode();
        }
        if (getProtocolloInEntrata() != null) {
            _hashCode += getProtocolloInEntrata().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocollaInEntrataNAIRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataNAIRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nai");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Nai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "NAI"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolloInEntrata");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloInEntrata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataRequest"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
