/**
 * DocumentoPrincipale.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class DocumentoPrincipale  extends it.inps.soa.WS00732.data.Documento  implements java.io.Serializable {
    private java.lang.Integer ownerAppl;

    private it.inps.soa.WS00732.data.Classifica[] classificazioni;

    private java.lang.String classeDocumentale;

    public DocumentoPrincipale() {
    }

    public DocumentoPrincipale(
           it.inps.soa.WS00732.data.FingerPrint fingerPrint,
           it.inps.soa.WS00732.data.EnTipoAcquisizione tipoAcquisizione,
           java.lang.Boolean autocertificazione,
           java.lang.String oggetto,
           it.inps.soa.WS00732.data.DocumentoElettronico documentoElettronico,
           java.lang.Boolean riservato,
           java.lang.Boolean sensibile,
           java.lang.String note,
           it.inps.soa.WS00732.data.ProcessoDestinatario processoDestinatario,
           java.lang.Integer ownerAppl,
           it.inps.soa.WS00732.data.Classifica[] classificazioni,
           java.lang.String classeDocumentale) {
        super(
            fingerPrint,
            tipoAcquisizione,
            autocertificazione,
            oggetto,
            documentoElettronico,
            riservato,
            sensibile,
            note,
            processoDestinatario);
        this.ownerAppl = ownerAppl;
        this.classificazioni = classificazioni;
        this.classeDocumentale = classeDocumentale;
    }


    /**
     * Gets the ownerAppl value for this DocumentoPrincipale.
     * 
     * @return ownerAppl
     */
    public java.lang.Integer getOwnerAppl() {
        return ownerAppl;
    }


    /**
     * Sets the ownerAppl value for this DocumentoPrincipale.
     * 
     * @param ownerAppl
     */
    public void setOwnerAppl(java.lang.Integer ownerAppl) {
        this.ownerAppl = ownerAppl;
    }


    /**
     * Gets the classificazioni value for this DocumentoPrincipale.
     * 
     * @return classificazioni
     */
    public it.inps.soa.WS00732.data.Classifica[] getClassificazioni() {
        return classificazioni;
    }


    /**
     * Sets the classificazioni value for this DocumentoPrincipale.
     * 
     * @param classificazioni
     */
    public void setClassificazioni(it.inps.soa.WS00732.data.Classifica[] classificazioni) {
        this.classificazioni = classificazioni;
    }


    /**
     * Gets the classeDocumentale value for this DocumentoPrincipale.
     * 
     * @return classeDocumentale
     */
    public java.lang.String getClasseDocumentale() {
        return classeDocumentale;
    }


    /**
     * Sets the classeDocumentale value for this DocumentoPrincipale.
     * 
     * @param classeDocumentale
     */
    public void setClasseDocumentale(java.lang.String classeDocumentale) {
        this.classeDocumentale = classeDocumentale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentoPrincipale)) return false;
        DocumentoPrincipale other = (DocumentoPrincipale) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ownerAppl==null && other.getOwnerAppl()==null) || 
             (this.ownerAppl!=null &&
              this.ownerAppl.equals(other.getOwnerAppl()))) &&
            ((this.classificazioni==null && other.getClassificazioni()==null) || 
             (this.classificazioni!=null &&
              java.util.Arrays.equals(this.classificazioni, other.getClassificazioni()))) &&
            ((this.classeDocumentale==null && other.getClasseDocumentale()==null) || 
             (this.classeDocumentale!=null &&
              this.classeDocumentale.equals(other.getClasseDocumentale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getOwnerAppl() != null) {
            _hashCode += getOwnerAppl().hashCode();
        }
        if (getClassificazioni() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClassificazioni());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClassificazioni(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getClasseDocumentale() != null) {
            _hashCode += getClasseDocumentale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentoPrincipale.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrincipale"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerAppl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "OwnerAppl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classificazioni");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classificazioni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classeDocumentale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ClasseDocumentale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
