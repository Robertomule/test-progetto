/**
 * ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest  extends it.inps.soa.WS00732.data.BaseFascicolazioneProcedimentaleRequest  implements java.io.Serializable {
    private java.lang.String codiceFascicoloPrincipale;

    private it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloInUscita;

    public ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest() {
    }

    public ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest(
           java.lang.String requestId,
           java.util.Calendar dataApertura,
           java.lang.String descrizione,
           java.lang.String note,
           java.lang.String procedimentoAmministrativo,
           java.lang.String tipologia,
           java.lang.String codiceFascicoloPrincipale,
           it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloInUscita) {
        super(
            requestId,
            dataApertura,
            descrizione,
            note,
            procedimentoAmministrativo,
            tipologia);
        this.codiceFascicoloPrincipale = codiceFascicoloPrincipale;
        this.protocolloInUscita = protocolloInUscita;
    }


    /**
     * Gets the codiceFascicoloPrincipale value for this ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest.
     * 
     * @return codiceFascicoloPrincipale
     */
    public java.lang.String getCodiceFascicoloPrincipale() {
        return codiceFascicoloPrincipale;
    }


    /**
     * Sets the codiceFascicoloPrincipale value for this ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest.
     * 
     * @param codiceFascicoloPrincipale
     */
    public void setCodiceFascicoloPrincipale(java.lang.String codiceFascicoloPrincipale) {
        this.codiceFascicoloPrincipale = codiceFascicoloPrincipale;
    }


    /**
     * Gets the protocolloInUscita value for this ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest.
     * 
     * @return protocolloInUscita
     */
    public it.inps.soa.WS00732.data.ProtocolloUscitaRequest getProtocolloInUscita() {
        return protocolloInUscita;
    }


    /**
     * Sets the protocolloInUscita value for this ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest.
     * 
     * @param protocolloInUscita
     */
    public void setProtocolloInUscita(it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloInUscita) {
        this.protocolloInUscita = protocolloInUscita;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest)) return false;
        ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest other = (ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codiceFascicoloPrincipale==null && other.getCodiceFascicoloPrincipale()==null) || 
             (this.codiceFascicoloPrincipale!=null &&
              this.codiceFascicoloPrincipale.equals(other.getCodiceFascicoloPrincipale()))) &&
            ((this.protocolloInUscita==null && other.getProtocolloInUscita()==null) || 
             (this.protocolloInUscita!=null &&
              this.protocolloInUscita.equals(other.getProtocolloInUscita())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodiceFascicoloPrincipale() != null) {
            _hashCode += getCodiceFascicoloPrincipale().hashCode();
        }
        if (getProtocolloInUscita() != null) {
            _hashCode += getProtocolloInUscita().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceFascicoloPrincipale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodiceFascicoloPrincipale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolloInUscita");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloInUscita"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscitaRequest"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
