/**
 * Destinatario.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class Destinatario  extends it.inps.soa.WS00732.data.Soggetto  implements java.io.Serializable {
    private java.lang.String categoria;

    private java.lang.Boolean confermaRicezione;

    private it.inps.soa.WS00732.data.EnTipoInvio tipoInvio;

    public Destinatario() {
    }

    public Destinatario(
           java.lang.String nominativo,
           java.lang.String codice,
           java.lang.String codiceInterno,
           java.lang.String email,
           it.inps.soa.WS00732.data.EnTipoSoggetto tipoSoggetto,
           it.inps.soa.WS00732.data.EnRuoloNelProcedimento ruoloNelProcedimento,
           java.lang.String categoria,
           java.lang.Boolean confermaRicezione,
           it.inps.soa.WS00732.data.EnTipoInvio tipoInvio) {
        super(
            nominativo,
            codice,
            codiceInterno,
            email,
            tipoSoggetto,
            ruoloNelProcedimento);
        this.categoria = categoria;
        this.confermaRicezione = confermaRicezione;
        this.tipoInvio = tipoInvio;
    }


    /**
     * Gets the categoria value for this Destinatario.
     * 
     * @return categoria
     */
    public java.lang.String getCategoria() {
        return categoria;
    }


    /**
     * Sets the categoria value for this Destinatario.
     * 
     * @param categoria
     */
    public void setCategoria(java.lang.String categoria) {
        this.categoria = categoria;
    }


    /**
     * Gets the confermaRicezione value for this Destinatario.
     * 
     * @return confermaRicezione
     */
    public java.lang.Boolean getConfermaRicezione() {
        return confermaRicezione;
    }


    /**
     * Sets the confermaRicezione value for this Destinatario.
     * 
     * @param confermaRicezione
     */
    public void setConfermaRicezione(java.lang.Boolean confermaRicezione) {
        this.confermaRicezione = confermaRicezione;
    }


    /**
     * Gets the tipoInvio value for this Destinatario.
     * 
     * @return tipoInvio
     */
    public it.inps.soa.WS00732.data.EnTipoInvio getTipoInvio() {
        return tipoInvio;
    }


    /**
     * Sets the tipoInvio value for this Destinatario.
     * 
     * @param tipoInvio
     */
    public void setTipoInvio(it.inps.soa.WS00732.data.EnTipoInvio tipoInvio) {
        this.tipoInvio = tipoInvio;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Destinatario)) return false;
        Destinatario other = (Destinatario) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.categoria==null && other.getCategoria()==null) || 
             (this.categoria!=null &&
              this.categoria.equals(other.getCategoria()))) &&
            ((this.confermaRicezione==null && other.getConfermaRicezione()==null) || 
             (this.confermaRicezione!=null &&
              this.confermaRicezione.equals(other.getConfermaRicezione()))) &&
            ((this.tipoInvio==null && other.getTipoInvio()==null) || 
             (this.tipoInvio!=null &&
              this.tipoInvio.equals(other.getTipoInvio())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCategoria() != null) {
            _hashCode += getCategoria().hashCode();
        }
        if (getConfermaRicezione() != null) {
            _hashCode += getConfermaRicezione().hashCode();
        }
        if (getTipoInvio() != null) {
            _hashCode += getTipoInvio().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Destinatario.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatario"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Categoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("confermaRicezione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ConfermaRicezione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoInvio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TipoInvio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoInvio"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
