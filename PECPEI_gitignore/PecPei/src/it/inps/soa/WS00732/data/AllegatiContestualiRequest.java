/**
 * AllegatiContestualiRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class AllegatiContestualiRequest  implements java.io.Serializable {
    private java.lang.String segnatura;

    private it.inps.soa.WS00732.data.Allegato[] allegatiContestuali;

    public AllegatiContestualiRequest() {
    }

    public AllegatiContestualiRequest(
           java.lang.String segnatura,
           it.inps.soa.WS00732.data.Allegato[] allegatiContestuali) {
           this.segnatura = segnatura;
           this.allegatiContestuali = allegatiContestuali;
    }


    /**
     * Gets the segnatura value for this AllegatiContestualiRequest.
     * 
     * @return segnatura
     */
    public java.lang.String getSegnatura() {
        return segnatura;
    }


    /**
     * Sets the segnatura value for this AllegatiContestualiRequest.
     * 
     * @param segnatura
     */
    public void setSegnatura(java.lang.String segnatura) {
        this.segnatura = segnatura;
    }


    /**
     * Gets the allegatiContestuali value for this AllegatiContestualiRequest.
     * 
     * @return allegatiContestuali
     */
    public it.inps.soa.WS00732.data.Allegato[] getAllegatiContestuali() {
        return allegatiContestuali;
    }


    /**
     * Sets the allegatiContestuali value for this AllegatiContestualiRequest.
     * 
     * @param allegatiContestuali
     */
    public void setAllegatiContestuali(it.inps.soa.WS00732.data.Allegato[] allegatiContestuali) {
        this.allegatiContestuali = allegatiContestuali;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AllegatiContestualiRequest)) return false;
        AllegatiContestualiRequest other = (AllegatiContestualiRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.segnatura==null && other.getSegnatura()==null) || 
             (this.segnatura!=null &&
              this.segnatura.equals(other.getSegnatura()))) &&
            ((this.allegatiContestuali==null && other.getAllegatiContestuali()==null) || 
             (this.allegatiContestuali!=null &&
              java.util.Arrays.equals(this.allegatiContestuali, other.getAllegatiContestuali())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSegnatura() != null) {
            _hashCode += getSegnatura().hashCode();
        }
        if (getAllegatiContestuali() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAllegatiContestuali());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAllegatiContestuali(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AllegatiContestualiRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiContestualiRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnatura");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Segnatura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allegatiContestuali");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatiContestuali"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
