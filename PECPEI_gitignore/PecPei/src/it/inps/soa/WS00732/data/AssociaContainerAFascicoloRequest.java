/**
 * AssociaContainerAFascicoloRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class AssociaContainerAFascicoloRequest  extends it.inps.soa.WS00732.data.BaseDataRequest  implements java.io.Serializable {
    private java.lang.String codiceFascicolo;

    private java.lang.Integer idContainer;

    public AssociaContainerAFascicoloRequest() {
    }

    public AssociaContainerAFascicoloRequest(
           java.lang.String requestId,
           java.lang.String codiceFascicolo,
           java.lang.Integer idContainer) {
        super(
            requestId);
        this.codiceFascicolo = codiceFascicolo;
        this.idContainer = idContainer;
    }


    /**
     * Gets the codiceFascicolo value for this AssociaContainerAFascicoloRequest.
     * 
     * @return codiceFascicolo
     */
    public java.lang.String getCodiceFascicolo() {
        return codiceFascicolo;
    }


    /**
     * Sets the codiceFascicolo value for this AssociaContainerAFascicoloRequest.
     * 
     * @param codiceFascicolo
     */
    public void setCodiceFascicolo(java.lang.String codiceFascicolo) {
        this.codiceFascicolo = codiceFascicolo;
    }


    /**
     * Gets the idContainer value for this AssociaContainerAFascicoloRequest.
     * 
     * @return idContainer
     */
    public java.lang.Integer getIdContainer() {
        return idContainer;
    }


    /**
     * Sets the idContainer value for this AssociaContainerAFascicoloRequest.
     * 
     * @param idContainer
     */
    public void setIdContainer(java.lang.Integer idContainer) {
        this.idContainer = idContainer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AssociaContainerAFascicoloRequest)) return false;
        AssociaContainerAFascicoloRequest other = (AssociaContainerAFascicoloRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codiceFascicolo==null && other.getCodiceFascicolo()==null) || 
             (this.codiceFascicolo!=null &&
              this.codiceFascicolo.equals(other.getCodiceFascicolo()))) &&
            ((this.idContainer==null && other.getIdContainer()==null) || 
             (this.idContainer!=null &&
              this.idContainer.equals(other.getIdContainer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodiceFascicolo() != null) {
            _hashCode += getCodiceFascicolo().hashCode();
        }
        if (getIdContainer() != null) {
            _hashCode += getIdContainer().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AssociaContainerAFascicoloRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AssociaContainerAFascicoloRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceFascicolo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodiceFascicolo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idContainer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdContainer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
