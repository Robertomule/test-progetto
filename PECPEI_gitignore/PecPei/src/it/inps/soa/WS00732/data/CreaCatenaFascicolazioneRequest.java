/**
 * CreaCatenaFascicolazioneRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class CreaCatenaFascicolazioneRequest  extends it.inps.soa.WS00732.data.BaseDataRequest  implements java.io.Serializable {
    private java.lang.String codiceFascicolo;

    private java.lang.String codiceFascicoloPrincipale;

    public CreaCatenaFascicolazioneRequest() {
    }

    public CreaCatenaFascicolazioneRequest(
           java.lang.String requestId,
           java.lang.String codiceFascicolo,
           java.lang.String codiceFascicoloPrincipale) {
        super(
            requestId);
        this.codiceFascicolo = codiceFascicolo;
        this.codiceFascicoloPrincipale = codiceFascicoloPrincipale;
    }


    /**
     * Gets the codiceFascicolo value for this CreaCatenaFascicolazioneRequest.
     * 
     * @return codiceFascicolo
     */
    public java.lang.String getCodiceFascicolo() {
        return codiceFascicolo;
    }


    /**
     * Sets the codiceFascicolo value for this CreaCatenaFascicolazioneRequest.
     * 
     * @param codiceFascicolo
     */
    public void setCodiceFascicolo(java.lang.String codiceFascicolo) {
        this.codiceFascicolo = codiceFascicolo;
    }


    /**
     * Gets the codiceFascicoloPrincipale value for this CreaCatenaFascicolazioneRequest.
     * 
     * @return codiceFascicoloPrincipale
     */
    public java.lang.String getCodiceFascicoloPrincipale() {
        return codiceFascicoloPrincipale;
    }


    /**
     * Sets the codiceFascicoloPrincipale value for this CreaCatenaFascicolazioneRequest.
     * 
     * @param codiceFascicoloPrincipale
     */
    public void setCodiceFascicoloPrincipale(java.lang.String codiceFascicoloPrincipale) {
        this.codiceFascicoloPrincipale = codiceFascicoloPrincipale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreaCatenaFascicolazioneRequest)) return false;
        CreaCatenaFascicolazioneRequest other = (CreaCatenaFascicolazioneRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codiceFascicolo==null && other.getCodiceFascicolo()==null) || 
             (this.codiceFascicolo!=null &&
              this.codiceFascicolo.equals(other.getCodiceFascicolo()))) &&
            ((this.codiceFascicoloPrincipale==null && other.getCodiceFascicoloPrincipale()==null) || 
             (this.codiceFascicoloPrincipale!=null &&
              this.codiceFascicoloPrincipale.equals(other.getCodiceFascicoloPrincipale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodiceFascicolo() != null) {
            _hashCode += getCodiceFascicolo().hashCode();
        }
        if (getCodiceFascicoloPrincipale() != null) {
            _hashCode += getCodiceFascicoloPrincipale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreaCatenaFascicolazioneRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaCatenaFascicolazioneRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceFascicolo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodiceFascicolo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceFascicoloPrincipale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodiceFascicoloPrincipale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
