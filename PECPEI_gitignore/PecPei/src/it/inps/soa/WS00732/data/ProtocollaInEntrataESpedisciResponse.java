/**
 * ProtocollaInEntrataESpedisciResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ProtocollaInEntrataESpedisciResponse  extends it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse  implements java.io.Serializable {
    private java.lang.String signatureProtocolloEntrata;

    public ProtocollaInEntrataESpedisciResponse() {
    }

    public ProtocollaInEntrataESpedisciResponse(
           it.inps.soa.WS00732.data.EnEsito codice,
           java.lang.String descrizione,
           java.lang.String signatureProtocolloUscita,
           java.lang.String[] signatureSpedizione,
           java.lang.String signatureProtocolloEntrata) {
        super(
            codice,
            descrizione,
            signatureProtocolloUscita,
            signatureSpedizione);
        this.signatureProtocolloEntrata = signatureProtocolloEntrata;
    }


    /**
     * Gets the signatureProtocolloEntrata value for this ProtocollaInEntrataESpedisciResponse.
     * 
     * @return signatureProtocolloEntrata
     */
    public java.lang.String getSignatureProtocolloEntrata() {
        return signatureProtocolloEntrata;
    }


    /**
     * Sets the signatureProtocolloEntrata value for this ProtocollaInEntrataESpedisciResponse.
     * 
     * @param signatureProtocolloEntrata
     */
    public void setSignatureProtocolloEntrata(java.lang.String signatureProtocolloEntrata) {
        this.signatureProtocolloEntrata = signatureProtocolloEntrata;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocollaInEntrataESpedisciResponse)) return false;
        ProtocollaInEntrataESpedisciResponse other = (ProtocollaInEntrataESpedisciResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.signatureProtocolloEntrata==null && other.getSignatureProtocolloEntrata()==null) || 
             (this.signatureProtocolloEntrata!=null &&
              this.signatureProtocolloEntrata.equals(other.getSignatureProtocolloEntrata())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSignatureProtocolloEntrata() != null) {
            _hashCode += getSignatureProtocolloEntrata().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocollaInEntrataESpedisciResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataESpedisciResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("signatureProtocolloEntrata");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SignatureProtocolloEntrata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
