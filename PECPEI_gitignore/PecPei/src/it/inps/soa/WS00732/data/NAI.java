/**
 * NAI.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class NAI  implements java.io.Serializable {
    private java.lang.String profileName;

    private java.lang.String descFascicoloNAI;

    private java.lang.String nomeDipendente;

    private java.lang.String cognomeDipendente;

    private java.lang.String matricolaDipendente;

    public NAI() {
    }

    public NAI(
           java.lang.String profileName,
           java.lang.String descFascicoloNAI,
           java.lang.String nomeDipendente,
           java.lang.String cognomeDipendente,
           java.lang.String matricolaDipendente) {
           this.profileName = profileName;
           this.descFascicoloNAI = descFascicoloNAI;
           this.nomeDipendente = nomeDipendente;
           this.cognomeDipendente = cognomeDipendente;
           this.matricolaDipendente = matricolaDipendente;
    }


    /**
     * Gets the profileName value for this NAI.
     * 
     * @return profileName
     */
    public java.lang.String getProfileName() {
        return profileName;
    }


    /**
     * Sets the profileName value for this NAI.
     * 
     * @param profileName
     */
    public void setProfileName(java.lang.String profileName) {
        this.profileName = profileName;
    }


    /**
     * Gets the descFascicoloNAI value for this NAI.
     * 
     * @return descFascicoloNAI
     */
    public java.lang.String getDescFascicoloNAI() {
        return descFascicoloNAI;
    }


    /**
     * Sets the descFascicoloNAI value for this NAI.
     * 
     * @param descFascicoloNAI
     */
    public void setDescFascicoloNAI(java.lang.String descFascicoloNAI) {
        this.descFascicoloNAI = descFascicoloNAI;
    }


    /**
     * Gets the nomeDipendente value for this NAI.
     * 
     * @return nomeDipendente
     */
    public java.lang.String getNomeDipendente() {
        return nomeDipendente;
    }


    /**
     * Sets the nomeDipendente value for this NAI.
     * 
     * @param nomeDipendente
     */
    public void setNomeDipendente(java.lang.String nomeDipendente) {
        this.nomeDipendente = nomeDipendente;
    }


    /**
     * Gets the cognomeDipendente value for this NAI.
     * 
     * @return cognomeDipendente
     */
    public java.lang.String getCognomeDipendente() {
        return cognomeDipendente;
    }


    /**
     * Sets the cognomeDipendente value for this NAI.
     * 
     * @param cognomeDipendente
     */
    public void setCognomeDipendente(java.lang.String cognomeDipendente) {
        this.cognomeDipendente = cognomeDipendente;
    }


    /**
     * Gets the matricolaDipendente value for this NAI.
     * 
     * @return matricolaDipendente
     */
    public java.lang.String getMatricolaDipendente() {
        return matricolaDipendente;
    }


    /**
     * Sets the matricolaDipendente value for this NAI.
     * 
     * @param matricolaDipendente
     */
    public void setMatricolaDipendente(java.lang.String matricolaDipendente) {
        this.matricolaDipendente = matricolaDipendente;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NAI)) return false;
        NAI other = (NAI) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.profileName==null && other.getProfileName()==null) || 
             (this.profileName!=null &&
              this.profileName.equals(other.getProfileName()))) &&
            ((this.descFascicoloNAI==null && other.getDescFascicoloNAI()==null) || 
             (this.descFascicoloNAI!=null &&
              this.descFascicoloNAI.equals(other.getDescFascicoloNAI()))) &&
            ((this.nomeDipendente==null && other.getNomeDipendente()==null) || 
             (this.nomeDipendente!=null &&
              this.nomeDipendente.equals(other.getNomeDipendente()))) &&
            ((this.cognomeDipendente==null && other.getCognomeDipendente()==null) || 
             (this.cognomeDipendente!=null &&
              this.cognomeDipendente.equals(other.getCognomeDipendente()))) &&
            ((this.matricolaDipendente==null && other.getMatricolaDipendente()==null) || 
             (this.matricolaDipendente!=null &&
              this.matricolaDipendente.equals(other.getMatricolaDipendente())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProfileName() != null) {
            _hashCode += getProfileName().hashCode();
        }
        if (getDescFascicoloNAI() != null) {
            _hashCode += getDescFascicoloNAI().hashCode();
        }
        if (getNomeDipendente() != null) {
            _hashCode += getNomeDipendente().hashCode();
        }
        if (getCognomeDipendente() != null) {
            _hashCode += getCognomeDipendente().hashCode();
        }
        if (getMatricolaDipendente() != null) {
            _hashCode += getMatricolaDipendente().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NAI.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "NAI"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProfileName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descFascicoloNAI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DescFascicoloNAI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeDipendente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "NomeDipendente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cognomeDipendente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CognomeDipendente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matricolaDipendente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MatricolaDipendente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
