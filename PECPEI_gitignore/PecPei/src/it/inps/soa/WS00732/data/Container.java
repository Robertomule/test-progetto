/**
 * Container.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class Container  implements java.io.Serializable {
    private java.util.Calendar consolidationDate;

    private java.lang.String dataDocumentManagment;

    private java.lang.Integer id;

    private java.lang.Integer idApplicationCreator;

    private java.lang.Integer idApplicationLastUpdate;

    private java.lang.Integer idPermission;

    private java.lang.Integer idUserCreator;

    private java.lang.Integer idUserLastUpdate;

    private java.util.Calendar insertDate;

    private java.lang.Boolean isConsolidated;

    private java.lang.Boolean isPreservation;

    private java.lang.Boolean isPreservationRule;

    private java.lang.Boolean isStorage;

    private java.util.Calendar lastUpdateDate;

    private java.util.Calendar preservationDate;

    private java.util.Calendar preservationRuleDate;

    private java.util.Calendar storageDate;

    private java.lang.Integer year;

    public Container() {
    }

    public Container(
           java.util.Calendar consolidationDate,
           java.lang.String dataDocumentManagment,
           java.lang.Integer id,
           java.lang.Integer idApplicationCreator,
           java.lang.Integer idApplicationLastUpdate,
           java.lang.Integer idPermission,
           java.lang.Integer idUserCreator,
           java.lang.Integer idUserLastUpdate,
           java.util.Calendar insertDate,
           java.lang.Boolean isConsolidated,
           java.lang.Boolean isPreservation,
           java.lang.Boolean isPreservationRule,
           java.lang.Boolean isStorage,
           java.util.Calendar lastUpdateDate,
           java.util.Calendar preservationDate,
           java.util.Calendar preservationRuleDate,
           java.util.Calendar storageDate,
           java.lang.Integer year) {
           this.consolidationDate = consolidationDate;
           this.dataDocumentManagment = dataDocumentManagment;
           this.id = id;
           this.idApplicationCreator = idApplicationCreator;
           this.idApplicationLastUpdate = idApplicationLastUpdate;
           this.idPermission = idPermission;
           this.idUserCreator = idUserCreator;
           this.idUserLastUpdate = idUserLastUpdate;
           this.insertDate = insertDate;
           this.isConsolidated = isConsolidated;
           this.isPreservation = isPreservation;
           this.isPreservationRule = isPreservationRule;
           this.isStorage = isStorage;
           this.lastUpdateDate = lastUpdateDate;
           this.preservationDate = preservationDate;
           this.preservationRuleDate = preservationRuleDate;
           this.storageDate = storageDate;
           this.year = year;
    }


    /**
     * Gets the consolidationDate value for this Container.
     * 
     * @return consolidationDate
     */
    public java.util.Calendar getConsolidationDate() {
        return consolidationDate;
    }


    /**
     * Sets the consolidationDate value for this Container.
     * 
     * @param consolidationDate
     */
    public void setConsolidationDate(java.util.Calendar consolidationDate) {
        this.consolidationDate = consolidationDate;
    }


    /**
     * Gets the dataDocumentManagment value for this Container.
     * 
     * @return dataDocumentManagment
     */
    public java.lang.String getDataDocumentManagment() {
        return dataDocumentManagment;
    }


    /**
     * Sets the dataDocumentManagment value for this Container.
     * 
     * @param dataDocumentManagment
     */
    public void setDataDocumentManagment(java.lang.String dataDocumentManagment) {
        this.dataDocumentManagment = dataDocumentManagment;
    }


    /**
     * Gets the id value for this Container.
     * 
     * @return id
     */
    public java.lang.Integer getId() {
        return id;
    }


    /**
     * Sets the id value for this Container.
     * 
     * @param id
     */
    public void setId(java.lang.Integer id) {
        this.id = id;
    }


    /**
     * Gets the idApplicationCreator value for this Container.
     * 
     * @return idApplicationCreator
     */
    public java.lang.Integer getIdApplicationCreator() {
        return idApplicationCreator;
    }


    /**
     * Sets the idApplicationCreator value for this Container.
     * 
     * @param idApplicationCreator
     */
    public void setIdApplicationCreator(java.lang.Integer idApplicationCreator) {
        this.idApplicationCreator = idApplicationCreator;
    }


    /**
     * Gets the idApplicationLastUpdate value for this Container.
     * 
     * @return idApplicationLastUpdate
     */
    public java.lang.Integer getIdApplicationLastUpdate() {
        return idApplicationLastUpdate;
    }


    /**
     * Sets the idApplicationLastUpdate value for this Container.
     * 
     * @param idApplicationLastUpdate
     */
    public void setIdApplicationLastUpdate(java.lang.Integer idApplicationLastUpdate) {
        this.idApplicationLastUpdate = idApplicationLastUpdate;
    }


    /**
     * Gets the idPermission value for this Container.
     * 
     * @return idPermission
     */
    public java.lang.Integer getIdPermission() {
        return idPermission;
    }


    /**
     * Sets the idPermission value for this Container.
     * 
     * @param idPermission
     */
    public void setIdPermission(java.lang.Integer idPermission) {
        this.idPermission = idPermission;
    }


    /**
     * Gets the idUserCreator value for this Container.
     * 
     * @return idUserCreator
     */
    public java.lang.Integer getIdUserCreator() {
        return idUserCreator;
    }


    /**
     * Sets the idUserCreator value for this Container.
     * 
     * @param idUserCreator
     */
    public void setIdUserCreator(java.lang.Integer idUserCreator) {
        this.idUserCreator = idUserCreator;
    }


    /**
     * Gets the idUserLastUpdate value for this Container.
     * 
     * @return idUserLastUpdate
     */
    public java.lang.Integer getIdUserLastUpdate() {
        return idUserLastUpdate;
    }


    /**
     * Sets the idUserLastUpdate value for this Container.
     * 
     * @param idUserLastUpdate
     */
    public void setIdUserLastUpdate(java.lang.Integer idUserLastUpdate) {
        this.idUserLastUpdate = idUserLastUpdate;
    }


    /**
     * Gets the insertDate value for this Container.
     * 
     * @return insertDate
     */
    public java.util.Calendar getInsertDate() {
        return insertDate;
    }


    /**
     * Sets the insertDate value for this Container.
     * 
     * @param insertDate
     */
    public void setInsertDate(java.util.Calendar insertDate) {
        this.insertDate = insertDate;
    }


    /**
     * Gets the isConsolidated value for this Container.
     * 
     * @return isConsolidated
     */
    public java.lang.Boolean getIsConsolidated() {
        return isConsolidated;
    }


    /**
     * Sets the isConsolidated value for this Container.
     * 
     * @param isConsolidated
     */
    public void setIsConsolidated(java.lang.Boolean isConsolidated) {
        this.isConsolidated = isConsolidated;
    }


    /**
     * Gets the isPreservation value for this Container.
     * 
     * @return isPreservation
     */
    public java.lang.Boolean getIsPreservation() {
        return isPreservation;
    }


    /**
     * Sets the isPreservation value for this Container.
     * 
     * @param isPreservation
     */
    public void setIsPreservation(java.lang.Boolean isPreservation) {
        this.isPreservation = isPreservation;
    }


    /**
     * Gets the isPreservationRule value for this Container.
     * 
     * @return isPreservationRule
     */
    public java.lang.Boolean getIsPreservationRule() {
        return isPreservationRule;
    }


    /**
     * Sets the isPreservationRule value for this Container.
     * 
     * @param isPreservationRule
     */
    public void setIsPreservationRule(java.lang.Boolean isPreservationRule) {
        this.isPreservationRule = isPreservationRule;
    }


    /**
     * Gets the isStorage value for this Container.
     * 
     * @return isStorage
     */
    public java.lang.Boolean getIsStorage() {
        return isStorage;
    }


    /**
     * Sets the isStorage value for this Container.
     * 
     * @param isStorage
     */
    public void setIsStorage(java.lang.Boolean isStorage) {
        this.isStorage = isStorage;
    }


    /**
     * Gets the lastUpdateDate value for this Container.
     * 
     * @return lastUpdateDate
     */
    public java.util.Calendar getLastUpdateDate() {
        return lastUpdateDate;
    }


    /**
     * Sets the lastUpdateDate value for this Container.
     * 
     * @param lastUpdateDate
     */
    public void setLastUpdateDate(java.util.Calendar lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }


    /**
     * Gets the preservationDate value for this Container.
     * 
     * @return preservationDate
     */
    public java.util.Calendar getPreservationDate() {
        return preservationDate;
    }


    /**
     * Sets the preservationDate value for this Container.
     * 
     * @param preservationDate
     */
    public void setPreservationDate(java.util.Calendar preservationDate) {
        this.preservationDate = preservationDate;
    }


    /**
     * Gets the preservationRuleDate value for this Container.
     * 
     * @return preservationRuleDate
     */
    public java.util.Calendar getPreservationRuleDate() {
        return preservationRuleDate;
    }


    /**
     * Sets the preservationRuleDate value for this Container.
     * 
     * @param preservationRuleDate
     */
    public void setPreservationRuleDate(java.util.Calendar preservationRuleDate) {
        this.preservationRuleDate = preservationRuleDate;
    }


    /**
     * Gets the storageDate value for this Container.
     * 
     * @return storageDate
     */
    public java.util.Calendar getStorageDate() {
        return storageDate;
    }


    /**
     * Sets the storageDate value for this Container.
     * 
     * @param storageDate
     */
    public void setStorageDate(java.util.Calendar storageDate) {
        this.storageDate = storageDate;
    }


    /**
     * Gets the year value for this Container.
     * 
     * @return year
     */
    public java.lang.Integer getYear() {
        return year;
    }


    /**
     * Sets the year value for this Container.
     * 
     * @param year
     */
    public void setYear(java.lang.Integer year) {
        this.year = year;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Container)) return false;
        Container other = (Container) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.consolidationDate==null && other.getConsolidationDate()==null) || 
             (this.consolidationDate!=null &&
              this.consolidationDate.equals(other.getConsolidationDate()))) &&
            ((this.dataDocumentManagment==null && other.getDataDocumentManagment()==null) || 
             (this.dataDocumentManagment!=null &&
              this.dataDocumentManagment.equals(other.getDataDocumentManagment()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.idApplicationCreator==null && other.getIdApplicationCreator()==null) || 
             (this.idApplicationCreator!=null &&
              this.idApplicationCreator.equals(other.getIdApplicationCreator()))) &&
            ((this.idApplicationLastUpdate==null && other.getIdApplicationLastUpdate()==null) || 
             (this.idApplicationLastUpdate!=null &&
              this.idApplicationLastUpdate.equals(other.getIdApplicationLastUpdate()))) &&
            ((this.idPermission==null && other.getIdPermission()==null) || 
             (this.idPermission!=null &&
              this.idPermission.equals(other.getIdPermission()))) &&
            ((this.idUserCreator==null && other.getIdUserCreator()==null) || 
             (this.idUserCreator!=null &&
              this.idUserCreator.equals(other.getIdUserCreator()))) &&
            ((this.idUserLastUpdate==null && other.getIdUserLastUpdate()==null) || 
             (this.idUserLastUpdate!=null &&
              this.idUserLastUpdate.equals(other.getIdUserLastUpdate()))) &&
            ((this.insertDate==null && other.getInsertDate()==null) || 
             (this.insertDate!=null &&
              this.insertDate.equals(other.getInsertDate()))) &&
            ((this.isConsolidated==null && other.getIsConsolidated()==null) || 
             (this.isConsolidated!=null &&
              this.isConsolidated.equals(other.getIsConsolidated()))) &&
            ((this.isPreservation==null && other.getIsPreservation()==null) || 
             (this.isPreservation!=null &&
              this.isPreservation.equals(other.getIsPreservation()))) &&
            ((this.isPreservationRule==null && other.getIsPreservationRule()==null) || 
             (this.isPreservationRule!=null &&
              this.isPreservationRule.equals(other.getIsPreservationRule()))) &&
            ((this.isStorage==null && other.getIsStorage()==null) || 
             (this.isStorage!=null &&
              this.isStorage.equals(other.getIsStorage()))) &&
            ((this.lastUpdateDate==null && other.getLastUpdateDate()==null) || 
             (this.lastUpdateDate!=null &&
              this.lastUpdateDate.equals(other.getLastUpdateDate()))) &&
            ((this.preservationDate==null && other.getPreservationDate()==null) || 
             (this.preservationDate!=null &&
              this.preservationDate.equals(other.getPreservationDate()))) &&
            ((this.preservationRuleDate==null && other.getPreservationRuleDate()==null) || 
             (this.preservationRuleDate!=null &&
              this.preservationRuleDate.equals(other.getPreservationRuleDate()))) &&
            ((this.storageDate==null && other.getStorageDate()==null) || 
             (this.storageDate!=null &&
              this.storageDate.equals(other.getStorageDate()))) &&
            ((this.year==null && other.getYear()==null) || 
             (this.year!=null &&
              this.year.equals(other.getYear())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsolidationDate() != null) {
            _hashCode += getConsolidationDate().hashCode();
        }
        if (getDataDocumentManagment() != null) {
            _hashCode += getDataDocumentManagment().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getIdApplicationCreator() != null) {
            _hashCode += getIdApplicationCreator().hashCode();
        }
        if (getIdApplicationLastUpdate() != null) {
            _hashCode += getIdApplicationLastUpdate().hashCode();
        }
        if (getIdPermission() != null) {
            _hashCode += getIdPermission().hashCode();
        }
        if (getIdUserCreator() != null) {
            _hashCode += getIdUserCreator().hashCode();
        }
        if (getIdUserLastUpdate() != null) {
            _hashCode += getIdUserLastUpdate().hashCode();
        }
        if (getInsertDate() != null) {
            _hashCode += getInsertDate().hashCode();
        }
        if (getIsConsolidated() != null) {
            _hashCode += getIsConsolidated().hashCode();
        }
        if (getIsPreservation() != null) {
            _hashCode += getIsPreservation().hashCode();
        }
        if (getIsPreservationRule() != null) {
            _hashCode += getIsPreservationRule().hashCode();
        }
        if (getIsStorage() != null) {
            _hashCode += getIsStorage().hashCode();
        }
        if (getLastUpdateDate() != null) {
            _hashCode += getLastUpdateDate().hashCode();
        }
        if (getPreservationDate() != null) {
            _hashCode += getPreservationDate().hashCode();
        }
        if (getPreservationRuleDate() != null) {
            _hashCode += getPreservationRuleDate().hashCode();
        }
        if (getStorageDate() != null) {
            _hashCode += getStorageDate().hashCode();
        }
        if (getYear() != null) {
            _hashCode += getYear().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Container.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Container"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consolidationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ConsolidationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataDocumentManagment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DataDocumentManagment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idApplicationCreator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdApplicationCreator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idApplicationLastUpdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdApplicationLastUpdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPermission");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdPermission"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idUserCreator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdUserCreator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idUserLastUpdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdUserLastUpdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insertDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "InsertDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isConsolidated");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IsConsolidated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPreservation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IsPreservation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPreservationRule");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IsPreservationRule"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isStorage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IsStorage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdateDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "LastUpdateDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preservationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "PreservationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preservationRuleDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "PreservationRuleDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storageDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StorageDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("year");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Year"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
