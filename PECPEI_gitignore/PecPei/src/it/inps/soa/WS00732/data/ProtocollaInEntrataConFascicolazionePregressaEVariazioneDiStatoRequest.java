/**
 * ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest  extends it.inps.soa.WS00732.data.BaseDataRequest  implements java.io.Serializable {
    private java.lang.String codiceFascicolo;

    private java.lang.String codiceStato;

    private it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloInEntrata;

    public ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest() {
    }

    public ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest(
           java.lang.String requestId,
           java.lang.String codiceFascicolo,
           java.lang.String codiceStato,
           it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloInEntrata) {
        super(
            requestId);
        this.codiceFascicolo = codiceFascicolo;
        this.codiceStato = codiceStato;
        this.protocolloInEntrata = protocolloInEntrata;
    }


    /**
     * Gets the codiceFascicolo value for this ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.
     * 
     * @return codiceFascicolo
     */
    public java.lang.String getCodiceFascicolo() {
        return codiceFascicolo;
    }


    /**
     * Sets the codiceFascicolo value for this ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.
     * 
     * @param codiceFascicolo
     */
    public void setCodiceFascicolo(java.lang.String codiceFascicolo) {
        this.codiceFascicolo = codiceFascicolo;
    }


    /**
     * Gets the codiceStato value for this ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.
     * 
     * @return codiceStato
     */
    public java.lang.String getCodiceStato() {
        return codiceStato;
    }


    /**
     * Sets the codiceStato value for this ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.
     * 
     * @param codiceStato
     */
    public void setCodiceStato(java.lang.String codiceStato) {
        this.codiceStato = codiceStato;
    }


    /**
     * Gets the protocolloInEntrata value for this ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.
     * 
     * @return protocolloInEntrata
     */
    public it.inps.soa.WS00732.data.ProtocolloEntrataRequest getProtocolloInEntrata() {
        return protocolloInEntrata;
    }


    /**
     * Sets the protocolloInEntrata value for this ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.
     * 
     * @param protocolloInEntrata
     */
    public void setProtocolloInEntrata(it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloInEntrata) {
        this.protocolloInEntrata = protocolloInEntrata;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest)) return false;
        ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest other = (ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codiceFascicolo==null && other.getCodiceFascicolo()==null) || 
             (this.codiceFascicolo!=null &&
              this.codiceFascicolo.equals(other.getCodiceFascicolo()))) &&
            ((this.codiceStato==null && other.getCodiceStato()==null) || 
             (this.codiceStato!=null &&
              this.codiceStato.equals(other.getCodiceStato()))) &&
            ((this.protocolloInEntrata==null && other.getProtocolloInEntrata()==null) || 
             (this.protocolloInEntrata!=null &&
              this.protocolloInEntrata.equals(other.getProtocolloInEntrata())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodiceFascicolo() != null) {
            _hashCode += getCodiceFascicolo().hashCode();
        }
        if (getCodiceStato() != null) {
            _hashCode += getCodiceStato().hashCode();
        }
        if (getProtocolloInEntrata() != null) {
            _hashCode += getProtocolloInEntrata().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceFascicolo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodiceFascicolo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceStato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodiceStato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolloInEntrata");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloInEntrata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloEntrataRequest"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
