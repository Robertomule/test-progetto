/**
 * AnnullaProtocolloRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class AnnullaProtocolloRequest  implements java.io.Serializable {
    private java.lang.String motivazioneAnnullamento;

    private java.lang.String numeroDirettiva;

    public AnnullaProtocolloRequest() {
    }

    public AnnullaProtocolloRequest(
           java.lang.String motivazioneAnnullamento,
           java.lang.String numeroDirettiva) {
           this.motivazioneAnnullamento = motivazioneAnnullamento;
           this.numeroDirettiva = numeroDirettiva;
    }


    /**
     * Gets the motivazioneAnnullamento value for this AnnullaProtocolloRequest.
     * 
     * @return motivazioneAnnullamento
     */
    public java.lang.String getMotivazioneAnnullamento() {
        return motivazioneAnnullamento;
    }


    /**
     * Sets the motivazioneAnnullamento value for this AnnullaProtocolloRequest.
     * 
     * @param motivazioneAnnullamento
     */
    public void setMotivazioneAnnullamento(java.lang.String motivazioneAnnullamento) {
        this.motivazioneAnnullamento = motivazioneAnnullamento;
    }


    /**
     * Gets the numeroDirettiva value for this AnnullaProtocolloRequest.
     * 
     * @return numeroDirettiva
     */
    public java.lang.String getNumeroDirettiva() {
        return numeroDirettiva;
    }


    /**
     * Sets the numeroDirettiva value for this AnnullaProtocolloRequest.
     * 
     * @param numeroDirettiva
     */
    public void setNumeroDirettiva(java.lang.String numeroDirettiva) {
        this.numeroDirettiva = numeroDirettiva;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AnnullaProtocolloRequest)) return false;
        AnnullaProtocolloRequest other = (AnnullaProtocolloRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.motivazioneAnnullamento==null && other.getMotivazioneAnnullamento()==null) || 
             (this.motivazioneAnnullamento!=null &&
              this.motivazioneAnnullamento.equals(other.getMotivazioneAnnullamento()))) &&
            ((this.numeroDirettiva==null && other.getNumeroDirettiva()==null) || 
             (this.numeroDirettiva!=null &&
              this.numeroDirettiva.equals(other.getNumeroDirettiva())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMotivazioneAnnullamento() != null) {
            _hashCode += getMotivazioneAnnullamento().hashCode();
        }
        if (getNumeroDirettiva() != null) {
            _hashCode += getNumeroDirettiva().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AnnullaProtocolloRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AnnullaProtocolloRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motivazioneAnnullamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MotivazioneAnnullamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroDirettiva");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "NumeroDirettiva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
