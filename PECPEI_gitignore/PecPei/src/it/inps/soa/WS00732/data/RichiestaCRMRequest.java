/**
 * RichiestaCRMRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class RichiestaCRMRequest  implements java.io.Serializable {
    private java.lang.String codAOO;

    private java.lang.String codApp;

    private java.lang.String idTransazione;

    private org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.DatiInvio request;

    public RichiestaCRMRequest() {
    }

    public RichiestaCRMRequest(
           java.lang.String codAOO,
           java.lang.String codApp,
           java.lang.String idTransazione,
           org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.DatiInvio request) {
           this.codAOO = codAOO;
           this.codApp = codApp;
           this.idTransazione = idTransazione;
           this.request = request;
    }


    /**
     * Gets the codAOO value for this RichiestaCRMRequest.
     * 
     * @return codAOO
     */
    public java.lang.String getCodAOO() {
        return codAOO;
    }


    /**
     * Sets the codAOO value for this RichiestaCRMRequest.
     * 
     * @param codAOO
     */
    public void setCodAOO(java.lang.String codAOO) {
        this.codAOO = codAOO;
    }


    /**
     * Gets the codApp value for this RichiestaCRMRequest.
     * 
     * @return codApp
     */
    public java.lang.String getCodApp() {
        return codApp;
    }


    /**
     * Sets the codApp value for this RichiestaCRMRequest.
     * 
     * @param codApp
     */
    public void setCodApp(java.lang.String codApp) {
        this.codApp = codApp;
    }


    /**
     * Gets the idTransazione value for this RichiestaCRMRequest.
     * 
     * @return idTransazione
     */
    public java.lang.String getIdTransazione() {
        return idTransazione;
    }


    /**
     * Sets the idTransazione value for this RichiestaCRMRequest.
     * 
     * @param idTransazione
     */
    public void setIdTransazione(java.lang.String idTransazione) {
        this.idTransazione = idTransazione;
    }


    /**
     * Gets the request value for this RichiestaCRMRequest.
     * 
     * @return request
     */
    public org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.DatiInvio getRequest() {
        return request;
    }


    /**
     * Sets the request value for this RichiestaCRMRequest.
     * 
     * @param request
     */
    public void setRequest(org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Requests.DatiInvio request) {
        this.request = request;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RichiestaCRMRequest)) return false;
        RichiestaCRMRequest other = (RichiestaCRMRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codAOO==null && other.getCodAOO()==null) || 
             (this.codAOO!=null &&
              this.codAOO.equals(other.getCodAOO()))) &&
            ((this.codApp==null && other.getCodApp()==null) || 
             (this.codApp!=null &&
              this.codApp.equals(other.getCodApp()))) &&
            ((this.idTransazione==null && other.getIdTransazione()==null) || 
             (this.idTransazione!=null &&
              this.idTransazione.equals(other.getIdTransazione()))) &&
            ((this.request==null && other.getRequest()==null) || 
             (this.request!=null &&
              this.request.equals(other.getRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodAOO() != null) {
            _hashCode += getCodAOO().hashCode();
        }
        if (getCodApp() != null) {
            _hashCode += getCodApp().hashCode();
        }
        if (getIdTransazione() != null) {
            _hashCode += getIdTransazione().hashCode();
        }
        if (getRequest() != null) {
            _hashCode += getRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RichiestaCRMRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RichiestaCRMRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAOO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodAOO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codApp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodApp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTransazione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdTransazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("request");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Request"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Requests", "DatiInvio"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
