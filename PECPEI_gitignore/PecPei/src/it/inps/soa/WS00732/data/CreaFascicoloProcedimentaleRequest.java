/**
 * CreaFascicoloProcedimentaleRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class CreaFascicoloProcedimentaleRequest  extends it.inps.soa.WS00732.data.BaseFascicolazioneProcedimentaleRequest  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.Soggetto[] altreAmministrazione;

    private java.lang.String classifica;

    private java.lang.String codiceFascicoloPrincipale;

    private it.inps.soa.WS00732.data.Soggetto richiedente;

    private it.inps.soa.WS00732.data.Soggetto titolare;

    public CreaFascicoloProcedimentaleRequest() {
    }

    public CreaFascicoloProcedimentaleRequest(
           java.lang.String requestId,
           java.util.Calendar dataApertura,
           java.lang.String descrizione,
           java.lang.String note,
           java.lang.String procedimentoAmministrativo,
           java.lang.String tipologia,
           it.inps.soa.WS00732.data.Soggetto[] altreAmministrazione,
           java.lang.String classifica,
           java.lang.String codiceFascicoloPrincipale,
           it.inps.soa.WS00732.data.Soggetto richiedente,
           it.inps.soa.WS00732.data.Soggetto titolare) {
        super(
            requestId,
            dataApertura,
            descrizione,
            note,
            procedimentoAmministrativo,
            tipologia);
        this.altreAmministrazione = altreAmministrazione;
        this.classifica = classifica;
        this.codiceFascicoloPrincipale = codiceFascicoloPrincipale;
        this.richiedente = richiedente;
        this.titolare = titolare;
    }


    /**
     * Gets the altreAmministrazione value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @return altreAmministrazione
     */
    public it.inps.soa.WS00732.data.Soggetto[] getAltreAmministrazione() {
        return altreAmministrazione;
    }


    /**
     * Sets the altreAmministrazione value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @param altreAmministrazione
     */
    public void setAltreAmministrazione(it.inps.soa.WS00732.data.Soggetto[] altreAmministrazione) {
        this.altreAmministrazione = altreAmministrazione;
    }


    /**
     * Gets the classifica value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @return classifica
     */
    public java.lang.String getClassifica() {
        return classifica;
    }


    /**
     * Sets the classifica value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @param classifica
     */
    public void setClassifica(java.lang.String classifica) {
        this.classifica = classifica;
    }


    /**
     * Gets the codiceFascicoloPrincipale value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @return codiceFascicoloPrincipale
     */
    public java.lang.String getCodiceFascicoloPrincipale() {
        return codiceFascicoloPrincipale;
    }


    /**
     * Sets the codiceFascicoloPrincipale value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @param codiceFascicoloPrincipale
     */
    public void setCodiceFascicoloPrincipale(java.lang.String codiceFascicoloPrincipale) {
        this.codiceFascicoloPrincipale = codiceFascicoloPrincipale;
    }


    /**
     * Gets the richiedente value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @return richiedente
     */
    public it.inps.soa.WS00732.data.Soggetto getRichiedente() {
        return richiedente;
    }


    /**
     * Sets the richiedente value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @param richiedente
     */
    public void setRichiedente(it.inps.soa.WS00732.data.Soggetto richiedente) {
        this.richiedente = richiedente;
    }


    /**
     * Gets the titolare value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @return titolare
     */
    public it.inps.soa.WS00732.data.Soggetto getTitolare() {
        return titolare;
    }


    /**
     * Sets the titolare value for this CreaFascicoloProcedimentaleRequest.
     * 
     * @param titolare
     */
    public void setTitolare(it.inps.soa.WS00732.data.Soggetto titolare) {
        this.titolare = titolare;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreaFascicoloProcedimentaleRequest)) return false;
        CreaFascicoloProcedimentaleRequest other = (CreaFascicoloProcedimentaleRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.altreAmministrazione==null && other.getAltreAmministrazione()==null) || 
             (this.altreAmministrazione!=null &&
              java.util.Arrays.equals(this.altreAmministrazione, other.getAltreAmministrazione()))) &&
            ((this.classifica==null && other.getClassifica()==null) || 
             (this.classifica!=null &&
              this.classifica.equals(other.getClassifica()))) &&
            ((this.codiceFascicoloPrincipale==null && other.getCodiceFascicoloPrincipale()==null) || 
             (this.codiceFascicoloPrincipale!=null &&
              this.codiceFascicoloPrincipale.equals(other.getCodiceFascicoloPrincipale()))) &&
            ((this.richiedente==null && other.getRichiedente()==null) || 
             (this.richiedente!=null &&
              this.richiedente.equals(other.getRichiedente()))) &&
            ((this.titolare==null && other.getTitolare()==null) || 
             (this.titolare!=null &&
              this.titolare.equals(other.getTitolare())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAltreAmministrazione() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAltreAmministrazione());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAltreAmministrazione(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getClassifica() != null) {
            _hashCode += getClassifica().hashCode();
        }
        if (getCodiceFascicoloPrincipale() != null) {
            _hashCode += getCodiceFascicoloPrincipale().hashCode();
        }
        if (getRichiedente() != null) {
            _hashCode += getRichiedente().hashCode();
        }
        if (getTitolare() != null) {
            _hashCode += getTitolare().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreaFascicoloProcedimentaleRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CreaFascicoloProcedimentaleRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("altreAmministrazione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AltreAmministrazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Soggetto"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Soggetto"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classifica");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceFascicoloPrincipale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodiceFascicoloPrincipale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("richiedente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Richiedente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Soggetto"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titolare");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Titolare"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Soggetto"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
