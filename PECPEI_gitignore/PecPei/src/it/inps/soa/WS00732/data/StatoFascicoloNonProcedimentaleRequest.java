/**
 * StatoFascicoloNonProcedimentaleRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class StatoFascicoloNonProcedimentaleRequest  extends it.inps.soa.WS00732.data.BaseDataRequest  implements java.io.Serializable {
    private java.lang.String codeNextStateDiagramSteps;

    private java.lang.String codiceFascicolo;

    private java.lang.String nota;

    public StatoFascicoloNonProcedimentaleRequest() {
    }

    public StatoFascicoloNonProcedimentaleRequest(
           java.lang.String requestId,
           java.lang.String codeNextStateDiagramSteps,
           java.lang.String codiceFascicolo,
           java.lang.String nota) {
        super(
            requestId);
        this.codeNextStateDiagramSteps = codeNextStateDiagramSteps;
        this.codiceFascicolo = codiceFascicolo;
        this.nota = nota;
    }


    /**
     * Gets the codeNextStateDiagramSteps value for this StatoFascicoloNonProcedimentaleRequest.
     * 
     * @return codeNextStateDiagramSteps
     */
    public java.lang.String getCodeNextStateDiagramSteps() {
        return codeNextStateDiagramSteps;
    }


    /**
     * Sets the codeNextStateDiagramSteps value for this StatoFascicoloNonProcedimentaleRequest.
     * 
     * @param codeNextStateDiagramSteps
     */
    public void setCodeNextStateDiagramSteps(java.lang.String codeNextStateDiagramSteps) {
        this.codeNextStateDiagramSteps = codeNextStateDiagramSteps;
    }


    /**
     * Gets the codiceFascicolo value for this StatoFascicoloNonProcedimentaleRequest.
     * 
     * @return codiceFascicolo
     */
    public java.lang.String getCodiceFascicolo() {
        return codiceFascicolo;
    }


    /**
     * Sets the codiceFascicolo value for this StatoFascicoloNonProcedimentaleRequest.
     * 
     * @param codiceFascicolo
     */
    public void setCodiceFascicolo(java.lang.String codiceFascicolo) {
        this.codiceFascicolo = codiceFascicolo;
    }


    /**
     * Gets the nota value for this StatoFascicoloNonProcedimentaleRequest.
     * 
     * @return nota
     */
    public java.lang.String getNota() {
        return nota;
    }


    /**
     * Sets the nota value for this StatoFascicoloNonProcedimentaleRequest.
     * 
     * @param nota
     */
    public void setNota(java.lang.String nota) {
        this.nota = nota;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StatoFascicoloNonProcedimentaleRequest)) return false;
        StatoFascicoloNonProcedimentaleRequest other = (StatoFascicoloNonProcedimentaleRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codeNextStateDiagramSteps==null && other.getCodeNextStateDiagramSteps()==null) || 
             (this.codeNextStateDiagramSteps!=null &&
              this.codeNextStateDiagramSteps.equals(other.getCodeNextStateDiagramSteps()))) &&
            ((this.codiceFascicolo==null && other.getCodiceFascicolo()==null) || 
             (this.codiceFascicolo!=null &&
              this.codiceFascicolo.equals(other.getCodiceFascicolo()))) &&
            ((this.nota==null && other.getNota()==null) || 
             (this.nota!=null &&
              this.nota.equals(other.getNota())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodeNextStateDiagramSteps() != null) {
            _hashCode += getCodeNextStateDiagramSteps().hashCode();
        }
        if (getCodiceFascicolo() != null) {
            _hashCode += getCodiceFascicolo().hashCode();
        }
        if (getNota() != null) {
            _hashCode += getNota().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StatoFascicoloNonProcedimentaleRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StatoFascicoloNonProcedimentaleRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeNextStateDiagramSteps");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodeNextStateDiagramSteps"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceFascicolo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CodiceFascicolo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Nota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
