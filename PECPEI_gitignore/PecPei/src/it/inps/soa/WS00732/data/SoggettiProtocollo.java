/**
 * SoggettiProtocollo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class SoggettiProtocollo  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.ModificaDestinatario[] destinatari;

    private it.inps.soa.WS00732.data.Mittente mittente;

    private it.inps.soa.WS00732.data.ModificaSoggettoAfferente[] soggettiAfferenti;

    public SoggettiProtocollo() {
    }

    public SoggettiProtocollo(
           it.inps.soa.WS00732.data.ModificaDestinatario[] destinatari,
           it.inps.soa.WS00732.data.Mittente mittente,
           it.inps.soa.WS00732.data.ModificaSoggettoAfferente[] soggettiAfferenti) {
           this.destinatari = destinatari;
           this.mittente = mittente;
           this.soggettiAfferenti = soggettiAfferenti;
    }


    /**
     * Gets the destinatari value for this SoggettiProtocollo.
     * 
     * @return destinatari
     */
    public it.inps.soa.WS00732.data.ModificaDestinatario[] getDestinatari() {
        return destinatari;
    }


    /**
     * Sets the destinatari value for this SoggettiProtocollo.
     * 
     * @param destinatari
     */
    public void setDestinatari(it.inps.soa.WS00732.data.ModificaDestinatario[] destinatari) {
        this.destinatari = destinatari;
    }


    /**
     * Gets the mittente value for this SoggettiProtocollo.
     * 
     * @return mittente
     */
    public it.inps.soa.WS00732.data.Mittente getMittente() {
        return mittente;
    }


    /**
     * Sets the mittente value for this SoggettiProtocollo.
     * 
     * @param mittente
     */
    public void setMittente(it.inps.soa.WS00732.data.Mittente mittente) {
        this.mittente = mittente;
    }


    /**
     * Gets the soggettiAfferenti value for this SoggettiProtocollo.
     * 
     * @return soggettiAfferenti
     */
    public it.inps.soa.WS00732.data.ModificaSoggettoAfferente[] getSoggettiAfferenti() {
        return soggettiAfferenti;
    }


    /**
     * Sets the soggettiAfferenti value for this SoggettiProtocollo.
     * 
     * @param soggettiAfferenti
     */
    public void setSoggettiAfferenti(it.inps.soa.WS00732.data.ModificaSoggettoAfferente[] soggettiAfferenti) {
        this.soggettiAfferenti = soggettiAfferenti;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SoggettiProtocollo)) return false;
        SoggettiProtocollo other = (SoggettiProtocollo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.destinatari==null && other.getDestinatari()==null) || 
             (this.destinatari!=null &&
              java.util.Arrays.equals(this.destinatari, other.getDestinatari()))) &&
            ((this.mittente==null && other.getMittente()==null) || 
             (this.mittente!=null &&
              this.mittente.equals(other.getMittente()))) &&
            ((this.soggettiAfferenti==null && other.getSoggettiAfferenti()==null) || 
             (this.soggettiAfferenti!=null &&
              java.util.Arrays.equals(this.soggettiAfferenti, other.getSoggettiAfferenti())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDestinatari() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDestinatari());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDestinatari(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMittente() != null) {
            _hashCode += getMittente().hashCode();
        }
        if (getSoggettiAfferenti() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSoggettiAfferenti());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSoggettiAfferenti(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SoggettiProtocollo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettiProtocollo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinatari");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatari"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaDestinatario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaDestinatario"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mittente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Mittente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Mittente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soggettiAfferenti");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettiAfferenti"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaSoggettoAfferente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ModificaSoggettoAfferente"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
