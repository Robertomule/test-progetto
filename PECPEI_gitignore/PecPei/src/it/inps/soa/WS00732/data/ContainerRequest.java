/**
 * ContainerRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ContainerRequest  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.Allegato[] allegati;

    private it.inps.soa.WS00732.data.DocumentoPrincipale documento;

    private java.lang.Integer idUserCreator;

    private java.util.Calendar insertDate;

    private java.lang.Integer year;

    public ContainerRequest() {
    }

    public ContainerRequest(
           it.inps.soa.WS00732.data.Allegato[] allegati,
           it.inps.soa.WS00732.data.DocumentoPrincipale documento,
           java.lang.Integer idUserCreator,
           java.util.Calendar insertDate,
           java.lang.Integer year) {
           this.allegati = allegati;
           this.documento = documento;
           this.idUserCreator = idUserCreator;
           this.insertDate = insertDate;
           this.year = year;
    }


    /**
     * Gets the allegati value for this ContainerRequest.
     * 
     * @return allegati
     */
    public it.inps.soa.WS00732.data.Allegato[] getAllegati() {
        return allegati;
    }


    /**
     * Sets the allegati value for this ContainerRequest.
     * 
     * @param allegati
     */
    public void setAllegati(it.inps.soa.WS00732.data.Allegato[] allegati) {
        this.allegati = allegati;
    }


    /**
     * Gets the documento value for this ContainerRequest.
     * 
     * @return documento
     */
    public it.inps.soa.WS00732.data.DocumentoPrincipale getDocumento() {
        return documento;
    }


    /**
     * Sets the documento value for this ContainerRequest.
     * 
     * @param documento
     */
    public void setDocumento(it.inps.soa.WS00732.data.DocumentoPrincipale documento) {
        this.documento = documento;
    }


    /**
     * Gets the idUserCreator value for this ContainerRequest.
     * 
     * @return idUserCreator
     */
    public java.lang.Integer getIdUserCreator() {
        return idUserCreator;
    }


    /**
     * Sets the idUserCreator value for this ContainerRequest.
     * 
     * @param idUserCreator
     */
    public void setIdUserCreator(java.lang.Integer idUserCreator) {
        this.idUserCreator = idUserCreator;
    }


    /**
     * Gets the insertDate value for this ContainerRequest.
     * 
     * @return insertDate
     */
    public java.util.Calendar getInsertDate() {
        return insertDate;
    }


    /**
     * Sets the insertDate value for this ContainerRequest.
     * 
     * @param insertDate
     */
    public void setInsertDate(java.util.Calendar insertDate) {
        this.insertDate = insertDate;
    }


    /**
     * Gets the year value for this ContainerRequest.
     * 
     * @return year
     */
    public java.lang.Integer getYear() {
        return year;
    }


    /**
     * Sets the year value for this ContainerRequest.
     * 
     * @param year
     */
    public void setYear(java.lang.Integer year) {
        this.year = year;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ContainerRequest)) return false;
        ContainerRequest other = (ContainerRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.allegati==null && other.getAllegati()==null) || 
             (this.allegati!=null &&
              java.util.Arrays.equals(this.allegati, other.getAllegati()))) &&
            ((this.documento==null && other.getDocumento()==null) || 
             (this.documento!=null &&
              this.documento.equals(other.getDocumento()))) &&
            ((this.idUserCreator==null && other.getIdUserCreator()==null) || 
             (this.idUserCreator!=null &&
              this.idUserCreator.equals(other.getIdUserCreator()))) &&
            ((this.insertDate==null && other.getInsertDate()==null) || 
             (this.insertDate!=null &&
              this.insertDate.equals(other.getInsertDate()))) &&
            ((this.year==null && other.getYear()==null) || 
             (this.year!=null &&
              this.year.equals(other.getYear())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAllegati() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAllegati());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAllegati(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDocumento() != null) {
            _hashCode += getDocumento().hashCode();
        }
        if (getIdUserCreator() != null) {
            _hashCode += getIdUserCreator().hashCode();
        }
        if (getInsertDate() != null) {
            _hashCode += getInsertDate().hashCode();
        }
        if (getYear() != null) {
            _hashCode += getYear().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ContainerRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ContainerRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allegati");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegati"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegato"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Documento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrincipale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idUserCreator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdUserCreator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insertDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "InsertDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("year");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Year"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
