/**
 * RegistraStatoInvioPECInUscitaRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class RegistraStatoInvioPECInUscitaRequest  implements java.io.Serializable {
    private java.lang.String[] documentIDes;

    private java.lang.String messageID;

    private org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Enumeration.EnStatoInvioRichiestaPECInUscita stato;

    public RegistraStatoInvioPECInUscitaRequest() {
    }

    public RegistraStatoInvioPECInUscitaRequest(
           java.lang.String[] documentIDes,
           java.lang.String messageID,
           org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Enumeration.EnStatoInvioRichiestaPECInUscita stato) {
           this.documentIDes = documentIDes;
           this.messageID = messageID;
           this.stato = stato;
    }


    /**
     * Gets the documentIDes value for this RegistraStatoInvioPECInUscitaRequest.
     * 
     * @return documentIDes
     */
    public java.lang.String[] getDocumentIDes() {
        return documentIDes;
    }


    /**
     * Sets the documentIDes value for this RegistraStatoInvioPECInUscitaRequest.
     * 
     * @param documentIDes
     */
    public void setDocumentIDes(java.lang.String[] documentIDes) {
        this.documentIDes = documentIDes;
    }


    /**
     * Gets the messageID value for this RegistraStatoInvioPECInUscitaRequest.
     * 
     * @return messageID
     */
    public java.lang.String getMessageID() {
        return messageID;
    }


    /**
     * Sets the messageID value for this RegistraStatoInvioPECInUscitaRequest.
     * 
     * @param messageID
     */
    public void setMessageID(java.lang.String messageID) {
        this.messageID = messageID;
    }


    /**
     * Gets the stato value for this RegistraStatoInvioPECInUscitaRequest.
     * 
     * @return stato
     */
    public org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Enumeration.EnStatoInvioRichiestaPECInUscita getStato() {
        return stato;
    }


    /**
     * Sets the stato value for this RegistraStatoInvioPECInUscitaRequest.
     * 
     * @param stato
     */
    public void setStato(org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Enumeration.EnStatoInvioRichiestaPECInUscita stato) {
        this.stato = stato;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistraStatoInvioPECInUscitaRequest)) return false;
        RegistraStatoInvioPECInUscitaRequest other = (RegistraStatoInvioPECInUscitaRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.documentIDes==null && other.getDocumentIDes()==null) || 
             (this.documentIDes!=null &&
              java.util.Arrays.equals(this.documentIDes, other.getDocumentIDes()))) &&
            ((this.messageID==null && other.getMessageID()==null) || 
             (this.messageID!=null &&
              this.messageID.equals(other.getMessageID()))) &&
            ((this.stato==null && other.getStato()==null) || 
             (this.stato!=null &&
              this.stato.equals(other.getStato())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDocumentIDes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDocumentIDes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDocumentIDes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMessageID() != null) {
            _hashCode += getMessageID().hashCode();
        }
        if (getStato() != null) {
            _hashCode += getStato().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistraStatoInvioPECInUscitaRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistraStatoInvioPECInUscitaRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentIDes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentIDes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MessageID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Stato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSProtocolloService.DataContract.Enumeration", "EnStatoInvioRichiestaPECInUscita"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
