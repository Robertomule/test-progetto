/**
 * ProtocollaInUscitaESpedisciResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ProtocollaInUscitaESpedisciResponse  extends it.inps.soa.WS00732.data.Response  implements java.io.Serializable {
    private java.lang.String signatureProtocolloUscita;

    private java.lang.String[] signatureSpedizione;

    public ProtocollaInUscitaESpedisciResponse() {
    }

    public ProtocollaInUscitaESpedisciResponse(
           it.inps.soa.WS00732.data.EnEsito codice,
           java.lang.String descrizione,
           java.lang.String signatureProtocolloUscita,
           java.lang.String[] signatureSpedizione) {
        super(
            codice,
            descrizione);
        this.signatureProtocolloUscita = signatureProtocolloUscita;
        this.signatureSpedizione = signatureSpedizione;
    }


    /**
     * Gets the signatureProtocolloUscita value for this ProtocollaInUscitaESpedisciResponse.
     * 
     * @return signatureProtocolloUscita
     */
    public java.lang.String getSignatureProtocolloUscita() {
        return signatureProtocolloUscita;
    }


    /**
     * Sets the signatureProtocolloUscita value for this ProtocollaInUscitaESpedisciResponse.
     * 
     * @param signatureProtocolloUscita
     */
    public void setSignatureProtocolloUscita(java.lang.String signatureProtocolloUscita) {
        this.signatureProtocolloUscita = signatureProtocolloUscita;
    }


    /**
     * Gets the signatureSpedizione value for this ProtocollaInUscitaESpedisciResponse.
     * 
     * @return signatureSpedizione
     */
    public java.lang.String[] getSignatureSpedizione() {
        return signatureSpedizione;
    }


    /**
     * Sets the signatureSpedizione value for this ProtocollaInUscitaESpedisciResponse.
     * 
     * @param signatureSpedizione
     */
    public void setSignatureSpedizione(java.lang.String[] signatureSpedizione) {
        this.signatureSpedizione = signatureSpedizione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocollaInUscitaESpedisciResponse)) return false;
        ProtocollaInUscitaESpedisciResponse other = (ProtocollaInUscitaESpedisciResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.signatureProtocolloUscita==null && other.getSignatureProtocolloUscita()==null) || 
             (this.signatureProtocolloUscita!=null &&
              this.signatureProtocolloUscita.equals(other.getSignatureProtocolloUscita()))) &&
            ((this.signatureSpedizione==null && other.getSignatureSpedizione()==null) || 
             (this.signatureSpedizione!=null &&
              java.util.Arrays.equals(this.signatureSpedizione, other.getSignatureSpedizione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSignatureProtocolloUscita() != null) {
            _hashCode += getSignatureProtocolloUscita().hashCode();
        }
        if (getSignatureSpedizione() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSignatureSpedizione());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSignatureSpedizione(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocollaInUscitaESpedisciResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocollaInUscitaESpedisciResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("signatureProtocolloUscita");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SignatureProtocolloUscita"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("signatureSpedizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SignatureSpedizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
