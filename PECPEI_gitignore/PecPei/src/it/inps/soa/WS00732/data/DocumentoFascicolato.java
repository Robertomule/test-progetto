/**
 * DocumentoFascicolato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class DocumentoFascicolato  implements java.io.Serializable {
    private java.lang.String classeDocumentale;

    private it.inps.soa.WS00732.data.Classifica[] classificazioni;

    private java.lang.String segnatura;

    public DocumentoFascicolato() {
    }

    public DocumentoFascicolato(
           java.lang.String classeDocumentale,
           it.inps.soa.WS00732.data.Classifica[] classificazioni,
           java.lang.String segnatura) {
           this.classeDocumentale = classeDocumentale;
           this.classificazioni = classificazioni;
           this.segnatura = segnatura;
    }


    /**
     * Gets the classeDocumentale value for this DocumentoFascicolato.
     * 
     * @return classeDocumentale
     */
    public java.lang.String getClasseDocumentale() {
        return classeDocumentale;
    }


    /**
     * Sets the classeDocumentale value for this DocumentoFascicolato.
     * 
     * @param classeDocumentale
     */
    public void setClasseDocumentale(java.lang.String classeDocumentale) {
        this.classeDocumentale = classeDocumentale;
    }


    /**
     * Gets the classificazioni value for this DocumentoFascicolato.
     * 
     * @return classificazioni
     */
    public it.inps.soa.WS00732.data.Classifica[] getClassificazioni() {
        return classificazioni;
    }


    /**
     * Sets the classificazioni value for this DocumentoFascicolato.
     * 
     * @param classificazioni
     */
    public void setClassificazioni(it.inps.soa.WS00732.data.Classifica[] classificazioni) {
        this.classificazioni = classificazioni;
    }


    /**
     * Gets the segnatura value for this DocumentoFascicolato.
     * 
     * @return segnatura
     */
    public java.lang.String getSegnatura() {
        return segnatura;
    }


    /**
     * Sets the segnatura value for this DocumentoFascicolato.
     * 
     * @param segnatura
     */
    public void setSegnatura(java.lang.String segnatura) {
        this.segnatura = segnatura;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentoFascicolato)) return false;
        DocumentoFascicolato other = (DocumentoFascicolato) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.classeDocumentale==null && other.getClasseDocumentale()==null) || 
             (this.classeDocumentale!=null &&
              this.classeDocumentale.equals(other.getClasseDocumentale()))) &&
            ((this.classificazioni==null && other.getClassificazioni()==null) || 
             (this.classificazioni!=null &&
              java.util.Arrays.equals(this.classificazioni, other.getClassificazioni()))) &&
            ((this.segnatura==null && other.getSegnatura()==null) || 
             (this.segnatura!=null &&
              this.segnatura.equals(other.getSegnatura())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClasseDocumentale() != null) {
            _hashCode += getClasseDocumentale().hashCode();
        }
        if (getClassificazioni() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClassificazioni());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClassificazioni(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSegnatura() != null) {
            _hashCode += getSegnatura().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentoFascicolato.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoFascicolato"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classeDocumentale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ClasseDocumentale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classificazioni");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classificazioni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnatura");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Segnatura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
