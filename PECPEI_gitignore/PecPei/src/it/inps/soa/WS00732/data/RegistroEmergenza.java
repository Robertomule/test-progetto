/**
 * RegistroEmergenza.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class RegistroEmergenza  implements java.io.Serializable {
    private java.util.Calendar dataCancellazione;

    private java.util.Calendar dataProtocollazione;

    private java.lang.String nomeRegistro;

    private java.lang.String segnatura;

    public RegistroEmergenza() {
    }

    public RegistroEmergenza(
           java.util.Calendar dataCancellazione,
           java.util.Calendar dataProtocollazione,
           java.lang.String nomeRegistro,
           java.lang.String segnatura) {
           this.dataCancellazione = dataCancellazione;
           this.dataProtocollazione = dataProtocollazione;
           this.nomeRegistro = nomeRegistro;
           this.segnatura = segnatura;
    }


    /**
     * Gets the dataCancellazione value for this RegistroEmergenza.
     * 
     * @return dataCancellazione
     */
    public java.util.Calendar getDataCancellazione() {
        return dataCancellazione;
    }


    /**
     * Sets the dataCancellazione value for this RegistroEmergenza.
     * 
     * @param dataCancellazione
     */
    public void setDataCancellazione(java.util.Calendar dataCancellazione) {
        this.dataCancellazione = dataCancellazione;
    }


    /**
     * Gets the dataProtocollazione value for this RegistroEmergenza.
     * 
     * @return dataProtocollazione
     */
    public java.util.Calendar getDataProtocollazione() {
        return dataProtocollazione;
    }


    /**
     * Sets the dataProtocollazione value for this RegistroEmergenza.
     * 
     * @param dataProtocollazione
     */
    public void setDataProtocollazione(java.util.Calendar dataProtocollazione) {
        this.dataProtocollazione = dataProtocollazione;
    }


    /**
     * Gets the nomeRegistro value for this RegistroEmergenza.
     * 
     * @return nomeRegistro
     */
    public java.lang.String getNomeRegistro() {
        return nomeRegistro;
    }


    /**
     * Sets the nomeRegistro value for this RegistroEmergenza.
     * 
     * @param nomeRegistro
     */
    public void setNomeRegistro(java.lang.String nomeRegistro) {
        this.nomeRegistro = nomeRegistro;
    }


    /**
     * Gets the segnatura value for this RegistroEmergenza.
     * 
     * @return segnatura
     */
    public java.lang.String getSegnatura() {
        return segnatura;
    }


    /**
     * Sets the segnatura value for this RegistroEmergenza.
     * 
     * @param segnatura
     */
    public void setSegnatura(java.lang.String segnatura) {
        this.segnatura = segnatura;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistroEmergenza)) return false;
        RegistroEmergenza other = (RegistroEmergenza) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dataCancellazione==null && other.getDataCancellazione()==null) || 
             (this.dataCancellazione!=null &&
              this.dataCancellazione.equals(other.getDataCancellazione()))) &&
            ((this.dataProtocollazione==null && other.getDataProtocollazione()==null) || 
             (this.dataProtocollazione!=null &&
              this.dataProtocollazione.equals(other.getDataProtocollazione()))) &&
            ((this.nomeRegistro==null && other.getNomeRegistro()==null) || 
             (this.nomeRegistro!=null &&
              this.nomeRegistro.equals(other.getNomeRegistro()))) &&
            ((this.segnatura==null && other.getSegnatura()==null) || 
             (this.segnatura!=null &&
              this.segnatura.equals(other.getSegnatura())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDataCancellazione() != null) {
            _hashCode += getDataCancellazione().hashCode();
        }
        if (getDataProtocollazione() != null) {
            _hashCode += getDataProtocollazione().hashCode();
        }
        if (getNomeRegistro() != null) {
            _hashCode += getNomeRegistro().hashCode();
        }
        if (getSegnatura() != null) {
            _hashCode += getSegnatura().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistroEmergenza.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistroEmergenza"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataCancellazione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DataCancellazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataProtocollazione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DataProtocollazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "NomeRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnatura");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Segnatura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
