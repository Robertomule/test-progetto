/**
 * ProtocolloUscita.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ProtocolloUscita  extends it.inps.soa.WS00732.data.ProtocolloResponse  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.Destinatario[] destinatari;

    public ProtocolloUscita() {
    }

    public ProtocolloUscita(
           java.lang.String segnatura,
           java.util.Calendar dataProtocollo,
           java.lang.Integer idProtocollo,
           it.inps.soa.WS00732.data.DocumentoPrincipaleResponse documento,
           it.inps.soa.WS00732.data.AllegatoResponse[] allegati,
           it.inps.soa.WS00732.data.SoggettoAfferente[] soggettiAfferenti,
           java.lang.String registrazioneProvenienza,
           java.lang.String primaRegistrazione,
           it.inps.soa.WS00732.data.Utente utente,
           it.inps.soa.WS00732.data.Applicazione applicazione,
           java.util.Calendar dataInvio,
           java.util.Calendar dataArrivo,
           it.inps.soa.WS00732.data.CatenaDocumentale catenaDocumentale,
           it.inps.soa.WS00732.data.RegistroEmergenza registroEmergenza,
           it.inps.soa.WS00732.data.Destinatario[] destinatari) {
        super(
            segnatura,
            dataProtocollo,
            idProtocollo,
            documento,
            allegati,
            soggettiAfferenti,
            registrazioneProvenienza,
            primaRegistrazione,
            utente,
            applicazione,
            dataInvio,
            dataArrivo,
            catenaDocumentale,
            registroEmergenza);
        this.destinatari = destinatari;
    }


    /**
     * Gets the destinatari value for this ProtocolloUscita.
     * 
     * @return destinatari
     */
    public it.inps.soa.WS00732.data.Destinatario[] getDestinatari() {
        return destinatari;
    }


    /**
     * Sets the destinatari value for this ProtocolloUscita.
     * 
     * @param destinatari
     */
    public void setDestinatari(it.inps.soa.WS00732.data.Destinatario[] destinatari) {
        this.destinatari = destinatari;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocolloUscita)) return false;
        ProtocolloUscita other = (ProtocolloUscita) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.destinatari==null && other.getDestinatari()==null) || 
             (this.destinatari!=null &&
              java.util.Arrays.equals(this.destinatari, other.getDestinatari())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDestinatari() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDestinatari());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDestinatari(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocolloUscita.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloUscita"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinatari");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatari"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatario"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Destinatario"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
