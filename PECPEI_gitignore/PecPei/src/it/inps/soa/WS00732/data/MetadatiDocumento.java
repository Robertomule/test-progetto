/**
 * MetadatiDocumento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class MetadatiDocumento  implements java.io.Serializable {
    private java.lang.Boolean autocertificazione;

    private java.lang.String note;

    private java.lang.String oggetto;

    private java.lang.Integer ownerAppl;

    private java.lang.Boolean riservato;

    private java.lang.Boolean sensibile;

    public MetadatiDocumento() {
    }

    public MetadatiDocumento(
           java.lang.Boolean autocertificazione,
           java.lang.String note,
           java.lang.String oggetto,
           java.lang.Integer ownerAppl,
           java.lang.Boolean riservato,
           java.lang.Boolean sensibile) {
           this.autocertificazione = autocertificazione;
           this.note = note;
           this.oggetto = oggetto;
           this.ownerAppl = ownerAppl;
           this.riservato = riservato;
           this.sensibile = sensibile;
    }


    /**
     * Gets the autocertificazione value for this MetadatiDocumento.
     * 
     * @return autocertificazione
     */
    public java.lang.Boolean getAutocertificazione() {
        return autocertificazione;
    }


    /**
     * Sets the autocertificazione value for this MetadatiDocumento.
     * 
     * @param autocertificazione
     */
    public void setAutocertificazione(java.lang.Boolean autocertificazione) {
        this.autocertificazione = autocertificazione;
    }


    /**
     * Gets the note value for this MetadatiDocumento.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this MetadatiDocumento.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }


    /**
     * Gets the oggetto value for this MetadatiDocumento.
     * 
     * @return oggetto
     */
    public java.lang.String getOggetto() {
        return oggetto;
    }


    /**
     * Sets the oggetto value for this MetadatiDocumento.
     * 
     * @param oggetto
     */
    public void setOggetto(java.lang.String oggetto) {
        this.oggetto = oggetto;
    }


    /**
     * Gets the ownerAppl value for this MetadatiDocumento.
     * 
     * @return ownerAppl
     */
    public java.lang.Integer getOwnerAppl() {
        return ownerAppl;
    }


    /**
     * Sets the ownerAppl value for this MetadatiDocumento.
     * 
     * @param ownerAppl
     */
    public void setOwnerAppl(java.lang.Integer ownerAppl) {
        this.ownerAppl = ownerAppl;
    }


    /**
     * Gets the riservato value for this MetadatiDocumento.
     * 
     * @return riservato
     */
    public java.lang.Boolean getRiservato() {
        return riservato;
    }


    /**
     * Sets the riservato value for this MetadatiDocumento.
     * 
     * @param riservato
     */
    public void setRiservato(java.lang.Boolean riservato) {
        this.riservato = riservato;
    }


    /**
     * Gets the sensibile value for this MetadatiDocumento.
     * 
     * @return sensibile
     */
    public java.lang.Boolean getSensibile() {
        return sensibile;
    }


    /**
     * Sets the sensibile value for this MetadatiDocumento.
     * 
     * @param sensibile
     */
    public void setSensibile(java.lang.Boolean sensibile) {
        this.sensibile = sensibile;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MetadatiDocumento)) return false;
        MetadatiDocumento other = (MetadatiDocumento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.autocertificazione==null && other.getAutocertificazione()==null) || 
             (this.autocertificazione!=null &&
              this.autocertificazione.equals(other.getAutocertificazione()))) &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote()))) &&
            ((this.oggetto==null && other.getOggetto()==null) || 
             (this.oggetto!=null &&
              this.oggetto.equals(other.getOggetto()))) &&
            ((this.ownerAppl==null && other.getOwnerAppl()==null) || 
             (this.ownerAppl!=null &&
              this.ownerAppl.equals(other.getOwnerAppl()))) &&
            ((this.riservato==null && other.getRiservato()==null) || 
             (this.riservato!=null &&
              this.riservato.equals(other.getRiservato()))) &&
            ((this.sensibile==null && other.getSensibile()==null) || 
             (this.sensibile!=null &&
              this.sensibile.equals(other.getSensibile())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAutocertificazione() != null) {
            _hashCode += getAutocertificazione().hashCode();
        }
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        if (getOggetto() != null) {
            _hashCode += getOggetto().hashCode();
        }
        if (getOwnerAppl() != null) {
            _hashCode += getOwnerAppl().hashCode();
        }
        if (getRiservato() != null) {
            _hashCode += getRiservato().hashCode();
        }
        if (getSensibile() != null) {
            _hashCode += getSensibile().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MetadatiDocumento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiDocumento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("autocertificazione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Autocertificazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oggetto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Oggetto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerAppl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "OwnerAppl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("riservato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Riservato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sensibile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Sensibile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
