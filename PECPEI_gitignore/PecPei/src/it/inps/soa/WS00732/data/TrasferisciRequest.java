/**
 * TrasferisciRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class TrasferisciRequest  implements java.io.Serializable {
    private java.lang.String segnatura;

    private it.inps.soa.WS00732.data.TrasferisciReq[] trasferimentoReq;

    public TrasferisciRequest() {
    }

    public TrasferisciRequest(
           java.lang.String segnatura,
           it.inps.soa.WS00732.data.TrasferisciReq[] trasferimentoReq) {
           this.segnatura = segnatura;
           this.trasferimentoReq = trasferimentoReq;
    }


    /**
     * Gets the segnatura value for this TrasferisciRequest.
     * 
     * @return segnatura
     */
    public java.lang.String getSegnatura() {
        return segnatura;
    }


    /**
     * Sets the segnatura value for this TrasferisciRequest.
     * 
     * @param segnatura
     */
    public void setSegnatura(java.lang.String segnatura) {
        this.segnatura = segnatura;
    }


    /**
     * Gets the trasferimentoReq value for this TrasferisciRequest.
     * 
     * @return trasferimentoReq
     */
    public it.inps.soa.WS00732.data.TrasferisciReq[] getTrasferimentoReq() {
        return trasferimentoReq;
    }


    /**
     * Sets the trasferimentoReq value for this TrasferisciRequest.
     * 
     * @param trasferimentoReq
     */
    public void setTrasferimentoReq(it.inps.soa.WS00732.data.TrasferisciReq[] trasferimentoReq) {
        this.trasferimentoReq = trasferimentoReq;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TrasferisciRequest)) return false;
        TrasferisciRequest other = (TrasferisciRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.segnatura==null && other.getSegnatura()==null) || 
             (this.segnatura!=null &&
              this.segnatura.equals(other.getSegnatura()))) &&
            ((this.trasferimentoReq==null && other.getTrasferimentoReq()==null) || 
             (this.trasferimentoReq!=null &&
              java.util.Arrays.equals(this.trasferimentoReq, other.getTrasferimentoReq())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSegnatura() != null) {
            _hashCode += getSegnatura().hashCode();
        }
        if (getTrasferimentoReq() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTrasferimentoReq());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTrasferimentoReq(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TrasferisciRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnatura");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Segnatura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trasferimentoReq");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferimentoReq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciReq"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TrasferisciReq"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
