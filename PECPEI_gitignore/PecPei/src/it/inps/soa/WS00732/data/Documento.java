/**
 * Documento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class Documento  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.FingerPrint fingerPrint;

    private it.inps.soa.WS00732.data.EnTipoAcquisizione tipoAcquisizione;

    private java.lang.Boolean autocertificazione;

    private java.lang.String oggetto;

    private it.inps.soa.WS00732.data.DocumentoElettronico documentoElettronico;

    private java.lang.Boolean riservato;

    private java.lang.Boolean sensibile;

    private java.lang.String note;

    private it.inps.soa.WS00732.data.ProcessoDestinatario processoDestinatario;

    public Documento() {
    }

    public Documento(
           it.inps.soa.WS00732.data.FingerPrint fingerPrint,
           it.inps.soa.WS00732.data.EnTipoAcquisizione tipoAcquisizione,
           java.lang.Boolean autocertificazione,
           java.lang.String oggetto,
           it.inps.soa.WS00732.data.DocumentoElettronico documentoElettronico,
           java.lang.Boolean riservato,
           java.lang.Boolean sensibile,
           java.lang.String note,
           it.inps.soa.WS00732.data.ProcessoDestinatario processoDestinatario) {
           this.fingerPrint = fingerPrint;
           this.tipoAcquisizione = tipoAcquisizione;
           this.autocertificazione = autocertificazione;
           this.oggetto = oggetto;
           this.documentoElettronico = documentoElettronico;
           this.riservato = riservato;
           this.sensibile = sensibile;
           this.note = note;
           this.processoDestinatario = processoDestinatario;
    }


    /**
     * Gets the fingerPrint value for this Documento.
     * 
     * @return fingerPrint
     */
    public it.inps.soa.WS00732.data.FingerPrint getFingerPrint() {
        return fingerPrint;
    }


    /**
     * Sets the fingerPrint value for this Documento.
     * 
     * @param fingerPrint
     */
    public void setFingerPrint(it.inps.soa.WS00732.data.FingerPrint fingerPrint) {
        this.fingerPrint = fingerPrint;
    }


    /**
     * Gets the tipoAcquisizione value for this Documento.
     * 
     * @return tipoAcquisizione
     */
    public it.inps.soa.WS00732.data.EnTipoAcquisizione getTipoAcquisizione() {
        return tipoAcquisizione;
    }


    /**
     * Sets the tipoAcquisizione value for this Documento.
     * 
     * @param tipoAcquisizione
     */
    public void setTipoAcquisizione(it.inps.soa.WS00732.data.EnTipoAcquisizione tipoAcquisizione) {
        this.tipoAcquisizione = tipoAcquisizione;
    }


    /**
     * Gets the autocertificazione value for this Documento.
     * 
     * @return autocertificazione
     */
    public java.lang.Boolean getAutocertificazione() {
        return autocertificazione;
    }


    /**
     * Sets the autocertificazione value for this Documento.
     * 
     * @param autocertificazione
     */
    public void setAutocertificazione(java.lang.Boolean autocertificazione) {
        this.autocertificazione = autocertificazione;
    }


    /**
     * Gets the oggetto value for this Documento.
     * 
     * @return oggetto
     */
    public java.lang.String getOggetto() {
        return oggetto;
    }


    /**
     * Sets the oggetto value for this Documento.
     * 
     * @param oggetto
     */
    public void setOggetto(java.lang.String oggetto) {
        this.oggetto = oggetto;
    }


    /**
     * Gets the documentoElettronico value for this Documento.
     * 
     * @return documentoElettronico
     */
    public it.inps.soa.WS00732.data.DocumentoElettronico getDocumentoElettronico() {
        return documentoElettronico;
    }


    /**
     * Sets the documentoElettronico value for this Documento.
     * 
     * @param documentoElettronico
     */
    public void setDocumentoElettronico(it.inps.soa.WS00732.data.DocumentoElettronico documentoElettronico) {
        this.documentoElettronico = documentoElettronico;
    }


    /**
     * Gets the riservato value for this Documento.
     * 
     * @return riservato
     */
    public java.lang.Boolean getRiservato() {
        return riservato;
    }


    /**
     * Sets the riservato value for this Documento.
     * 
     * @param riservato
     */
    public void setRiservato(java.lang.Boolean riservato) {
        this.riservato = riservato;
    }


    /**
     * Gets the sensibile value for this Documento.
     * 
     * @return sensibile
     */
    public java.lang.Boolean getSensibile() {
        return sensibile;
    }


    /**
     * Sets the sensibile value for this Documento.
     * 
     * @param sensibile
     */
    public void setSensibile(java.lang.Boolean sensibile) {
        this.sensibile = sensibile;
    }


    /**
     * Gets the note value for this Documento.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this Documento.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }


    /**
     * Gets the processoDestinatario value for this Documento.
     * 
     * @return processoDestinatario
     */
    public it.inps.soa.WS00732.data.ProcessoDestinatario getProcessoDestinatario() {
        return processoDestinatario;
    }


    /**
     * Sets the processoDestinatario value for this Documento.
     * 
     * @param processoDestinatario
     */
    public void setProcessoDestinatario(it.inps.soa.WS00732.data.ProcessoDestinatario processoDestinatario) {
        this.processoDestinatario = processoDestinatario;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Documento)) return false;
        Documento other = (Documento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fingerPrint==null && other.getFingerPrint()==null) || 
             (this.fingerPrint!=null &&
              this.fingerPrint.equals(other.getFingerPrint()))) &&
            ((this.tipoAcquisizione==null && other.getTipoAcquisizione()==null) || 
             (this.tipoAcquisizione!=null &&
              this.tipoAcquisizione.equals(other.getTipoAcquisizione()))) &&
            ((this.autocertificazione==null && other.getAutocertificazione()==null) || 
             (this.autocertificazione!=null &&
              this.autocertificazione.equals(other.getAutocertificazione()))) &&
            ((this.oggetto==null && other.getOggetto()==null) || 
             (this.oggetto!=null &&
              this.oggetto.equals(other.getOggetto()))) &&
            ((this.documentoElettronico==null && other.getDocumentoElettronico()==null) || 
             (this.documentoElettronico!=null &&
              this.documentoElettronico.equals(other.getDocumentoElettronico()))) &&
            ((this.riservato==null && other.getRiservato()==null) || 
             (this.riservato!=null &&
              this.riservato.equals(other.getRiservato()))) &&
            ((this.sensibile==null && other.getSensibile()==null) || 
             (this.sensibile!=null &&
              this.sensibile.equals(other.getSensibile()))) &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote()))) &&
            ((this.processoDestinatario==null && other.getProcessoDestinatario()==null) || 
             (this.processoDestinatario!=null &&
              this.processoDestinatario.equals(other.getProcessoDestinatario())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFingerPrint() != null) {
            _hashCode += getFingerPrint().hashCode();
        }
        if (getTipoAcquisizione() != null) {
            _hashCode += getTipoAcquisizione().hashCode();
        }
        if (getAutocertificazione() != null) {
            _hashCode += getAutocertificazione().hashCode();
        }
        if (getOggetto() != null) {
            _hashCode += getOggetto().hashCode();
        }
        if (getDocumentoElettronico() != null) {
            _hashCode += getDocumentoElettronico().hashCode();
        }
        if (getRiservato() != null) {
            _hashCode += getRiservato().hashCode();
        }
        if (getSensibile() != null) {
            _hashCode += getSensibile().hashCode();
        }
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        if (getProcessoDestinatario() != null) {
            _hashCode += getProcessoDestinatario().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Documento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Documento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fingerPrint");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FingerPrint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FingerPrint"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoAcquisizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "TipoAcquisizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnTipoAcquisizione"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("autocertificazione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Autocertificazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oggetto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Oggetto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentoElettronico");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoElettronico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoElettronico"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("riservato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Riservato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sensibile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Sensibile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processoDestinatario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProcessoDestinatario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProcessoDestinatario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
