/**
 * CatenaFascicolazioneResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class CatenaFascicolazioneResponse  extends it.inps.soa.WS00732.data.Response  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.DatiFascicolo datiFascicolo;

    public CatenaFascicolazioneResponse() {
    }

    public CatenaFascicolazioneResponse(
           it.inps.soa.WS00732.data.EnEsito codice,
           java.lang.String descrizione,
           it.inps.soa.WS00732.data.DatiFascicolo datiFascicolo) {
        super(
            codice,
            descrizione);
        this.datiFascicolo = datiFascicolo;
    }


    /**
     * Gets the datiFascicolo value for this CatenaFascicolazioneResponse.
     * 
     * @return datiFascicolo
     */
    public it.inps.soa.WS00732.data.DatiFascicolo getDatiFascicolo() {
        return datiFascicolo;
    }


    /**
     * Sets the datiFascicolo value for this CatenaFascicolazioneResponse.
     * 
     * @param datiFascicolo
     */
    public void setDatiFascicolo(it.inps.soa.WS00732.data.DatiFascicolo datiFascicolo) {
        this.datiFascicolo = datiFascicolo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CatenaFascicolazioneResponse)) return false;
        CatenaFascicolazioneResponse other = (CatenaFascicolazioneResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.datiFascicolo==null && other.getDatiFascicolo()==null) || 
             (this.datiFascicolo!=null &&
              this.datiFascicolo.equals(other.getDatiFascicolo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDatiFascicolo() != null) {
            _hashCode += getDatiFascicolo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CatenaFascicolazioneResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CatenaFascicolazioneResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datiFascicolo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DatiFascicolo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DatiFascicolo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
