/**
 * BaseFascicolazioneProcedimentaleRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class BaseFascicolazioneProcedimentaleRequest  extends it.inps.soa.WS00732.data.BaseDataRequest  implements java.io.Serializable {
    private java.util.Calendar dataApertura;

    private java.lang.String descrizione;

    private java.lang.String note;

    private java.lang.String procedimentoAmministrativo;

    private java.lang.String tipologia;

    public BaseFascicolazioneProcedimentaleRequest() {
    }

    public BaseFascicolazioneProcedimentaleRequest(
           java.lang.String requestId,
           java.util.Calendar dataApertura,
           java.lang.String descrizione,
           java.lang.String note,
           java.lang.String procedimentoAmministrativo,
           java.lang.String tipologia) {
        super(
            requestId);
        this.dataApertura = dataApertura;
        this.descrizione = descrizione;
        this.note = note;
        this.procedimentoAmministrativo = procedimentoAmministrativo;
        this.tipologia = tipologia;
    }


    /**
     * Gets the dataApertura value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @return dataApertura
     */
    public java.util.Calendar getDataApertura() {
        return dataApertura;
    }


    /**
     * Sets the dataApertura value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @param dataApertura
     */
    public void setDataApertura(java.util.Calendar dataApertura) {
        this.dataApertura = dataApertura;
    }


    /**
     * Gets the descrizione value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the note value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }


    /**
     * Gets the procedimentoAmministrativo value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @return procedimentoAmministrativo
     */
    public java.lang.String getProcedimentoAmministrativo() {
        return procedimentoAmministrativo;
    }


    /**
     * Sets the procedimentoAmministrativo value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @param procedimentoAmministrativo
     */
    public void setProcedimentoAmministrativo(java.lang.String procedimentoAmministrativo) {
        this.procedimentoAmministrativo = procedimentoAmministrativo;
    }


    /**
     * Gets the tipologia value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @return tipologia
     */
    public java.lang.String getTipologia() {
        return tipologia;
    }


    /**
     * Sets the tipologia value for this BaseFascicolazioneProcedimentaleRequest.
     * 
     * @param tipologia
     */
    public void setTipologia(java.lang.String tipologia) {
        this.tipologia = tipologia;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BaseFascicolazioneProcedimentaleRequest)) return false;
        BaseFascicolazioneProcedimentaleRequest other = (BaseFascicolazioneProcedimentaleRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.dataApertura==null && other.getDataApertura()==null) || 
             (this.dataApertura!=null &&
              this.dataApertura.equals(other.getDataApertura()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote()))) &&
            ((this.procedimentoAmministrativo==null && other.getProcedimentoAmministrativo()==null) || 
             (this.procedimentoAmministrativo!=null &&
              this.procedimentoAmministrativo.equals(other.getProcedimentoAmministrativo()))) &&
            ((this.tipologia==null && other.getTipologia()==null) || 
             (this.tipologia!=null &&
              this.tipologia.equals(other.getTipologia())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDataApertura() != null) {
            _hashCode += getDataApertura().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        if (getProcedimentoAmministrativo() != null) {
            _hashCode += getProcedimentoAmministrativo().hashCode();
        }
        if (getTipologia() != null) {
            _hashCode += getTipologia().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BaseFascicolazioneProcedimentaleRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "BaseFascicolazioneProcedimentaleRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataApertura");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DataApertura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("procedimentoAmministrativo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProcedimentoAmministrativo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipologia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Tipologia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
