/**
 * FolderNotProceduralResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class FolderNotProceduralResponse  implements java.io.Serializable {
    private java.lang.String administrativeContexts;

    private it.inps.soa.WS00732.data.EnFolderCategory category;

    private it.inps.soa.WS00732.data.ChiaveEsterna chiaveEsterna;

    private it.inps.soa.WS00732.data.Classifica classification;

    private java.lang.String code;

    private it.inps.soa.WS00732.data.Container[] containers;

    private java.lang.String description;

    private java.lang.Integer id;

    private it.inps.soa.WS00732.data.OrganizationalAreaExt organizationalArea;

    private it.inps.soa.WS00732.data.ProtocolRegistration[] protocolRegistrations;

    private java.lang.Boolean readOnlyState;

    private java.lang.String statusCode;

    private java.lang.String statusDescription;

    private java.lang.String type;

    private java.lang.Integer year;

    public FolderNotProceduralResponse() {
    }

    public FolderNotProceduralResponse(
           java.lang.String administrativeContexts,
           it.inps.soa.WS00732.data.EnFolderCategory category,
           it.inps.soa.WS00732.data.ChiaveEsterna chiaveEsterna,
           it.inps.soa.WS00732.data.Classifica classification,
           java.lang.String code,
           it.inps.soa.WS00732.data.Container[] containers,
           java.lang.String description,
           java.lang.Integer id,
           it.inps.soa.WS00732.data.OrganizationalAreaExt organizationalArea,
           it.inps.soa.WS00732.data.ProtocolRegistration[] protocolRegistrations,
           java.lang.Boolean readOnlyState,
           java.lang.String statusCode,
           java.lang.String statusDescription,
           java.lang.String type,
           java.lang.Integer year) {
           this.administrativeContexts = administrativeContexts;
           this.category = category;
           this.chiaveEsterna = chiaveEsterna;
           this.classification = classification;
           this.code = code;
           this.containers = containers;
           this.description = description;
           this.id = id;
           this.organizationalArea = organizationalArea;
           this.protocolRegistrations = protocolRegistrations;
           this.readOnlyState = readOnlyState;
           this.statusCode = statusCode;
           this.statusDescription = statusDescription;
           this.type = type;
           this.year = year;
    }


    /**
     * Gets the administrativeContexts value for this FolderNotProceduralResponse.
     * 
     * @return administrativeContexts
     */
    public java.lang.String getAdministrativeContexts() {
        return administrativeContexts;
    }


    /**
     * Sets the administrativeContexts value for this FolderNotProceduralResponse.
     * 
     * @param administrativeContexts
     */
    public void setAdministrativeContexts(java.lang.String administrativeContexts) {
        this.administrativeContexts = administrativeContexts;
    }


    /**
     * Gets the category value for this FolderNotProceduralResponse.
     * 
     * @return category
     */
    public it.inps.soa.WS00732.data.EnFolderCategory getCategory() {
        return category;
    }


    /**
     * Sets the category value for this FolderNotProceduralResponse.
     * 
     * @param category
     */
    public void setCategory(it.inps.soa.WS00732.data.EnFolderCategory category) {
        this.category = category;
    }


    /**
     * Gets the chiaveEsterna value for this FolderNotProceduralResponse.
     * 
     * @return chiaveEsterna
     */
    public it.inps.soa.WS00732.data.ChiaveEsterna getChiaveEsterna() {
        return chiaveEsterna;
    }


    /**
     * Sets the chiaveEsterna value for this FolderNotProceduralResponse.
     * 
     * @param chiaveEsterna
     */
    public void setChiaveEsterna(it.inps.soa.WS00732.data.ChiaveEsterna chiaveEsterna) {
        this.chiaveEsterna = chiaveEsterna;
    }


    /**
     * Gets the classification value for this FolderNotProceduralResponse.
     * 
     * @return classification
     */
    public it.inps.soa.WS00732.data.Classifica getClassification() {
        return classification;
    }


    /**
     * Sets the classification value for this FolderNotProceduralResponse.
     * 
     * @param classification
     */
    public void setClassification(it.inps.soa.WS00732.data.Classifica classification) {
        this.classification = classification;
    }


    /**
     * Gets the code value for this FolderNotProceduralResponse.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this FolderNotProceduralResponse.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the containers value for this FolderNotProceduralResponse.
     * 
     * @return containers
     */
    public it.inps.soa.WS00732.data.Container[] getContainers() {
        return containers;
    }


    /**
     * Sets the containers value for this FolderNotProceduralResponse.
     * 
     * @param containers
     */
    public void setContainers(it.inps.soa.WS00732.data.Container[] containers) {
        this.containers = containers;
    }


    /**
     * Gets the description value for this FolderNotProceduralResponse.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this FolderNotProceduralResponse.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the id value for this FolderNotProceduralResponse.
     * 
     * @return id
     */
    public java.lang.Integer getId() {
        return id;
    }


    /**
     * Sets the id value for this FolderNotProceduralResponse.
     * 
     * @param id
     */
    public void setId(java.lang.Integer id) {
        this.id = id;
    }


    /**
     * Gets the organizationalArea value for this FolderNotProceduralResponse.
     * 
     * @return organizationalArea
     */
    public it.inps.soa.WS00732.data.OrganizationalAreaExt getOrganizationalArea() {
        return organizationalArea;
    }


    /**
     * Sets the organizationalArea value for this FolderNotProceduralResponse.
     * 
     * @param organizationalArea
     */
    public void setOrganizationalArea(it.inps.soa.WS00732.data.OrganizationalAreaExt organizationalArea) {
        this.organizationalArea = organizationalArea;
    }


    /**
     * Gets the protocolRegistrations value for this FolderNotProceduralResponse.
     * 
     * @return protocolRegistrations
     */
    public it.inps.soa.WS00732.data.ProtocolRegistration[] getProtocolRegistrations() {
        return protocolRegistrations;
    }


    /**
     * Sets the protocolRegistrations value for this FolderNotProceduralResponse.
     * 
     * @param protocolRegistrations
     */
    public void setProtocolRegistrations(it.inps.soa.WS00732.data.ProtocolRegistration[] protocolRegistrations) {
        this.protocolRegistrations = protocolRegistrations;
    }


    /**
     * Gets the readOnlyState value for this FolderNotProceduralResponse.
     * 
     * @return readOnlyState
     */
    public java.lang.Boolean getReadOnlyState() {
        return readOnlyState;
    }


    /**
     * Sets the readOnlyState value for this FolderNotProceduralResponse.
     * 
     * @param readOnlyState
     */
    public void setReadOnlyState(java.lang.Boolean readOnlyState) {
        this.readOnlyState = readOnlyState;
    }


    /**
     * Gets the statusCode value for this FolderNotProceduralResponse.
     * 
     * @return statusCode
     */
    public java.lang.String getStatusCode() {
        return statusCode;
    }


    /**
     * Sets the statusCode value for this FolderNotProceduralResponse.
     * 
     * @param statusCode
     */
    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }


    /**
     * Gets the statusDescription value for this FolderNotProceduralResponse.
     * 
     * @return statusDescription
     */
    public java.lang.String getStatusDescription() {
        return statusDescription;
    }


    /**
     * Sets the statusDescription value for this FolderNotProceduralResponse.
     * 
     * @param statusDescription
     */
    public void setStatusDescription(java.lang.String statusDescription) {
        this.statusDescription = statusDescription;
    }


    /**
     * Gets the type value for this FolderNotProceduralResponse.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this FolderNotProceduralResponse.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the year value for this FolderNotProceduralResponse.
     * 
     * @return year
     */
    public java.lang.Integer getYear() {
        return year;
    }


    /**
     * Sets the year value for this FolderNotProceduralResponse.
     * 
     * @param year
     */
    public void setYear(java.lang.Integer year) {
        this.year = year;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FolderNotProceduralResponse)) return false;
        FolderNotProceduralResponse other = (FolderNotProceduralResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.administrativeContexts==null && other.getAdministrativeContexts()==null) || 
             (this.administrativeContexts!=null &&
              this.administrativeContexts.equals(other.getAdministrativeContexts()))) &&
            ((this.category==null && other.getCategory()==null) || 
             (this.category!=null &&
              this.category.equals(other.getCategory()))) &&
            ((this.chiaveEsterna==null && other.getChiaveEsterna()==null) || 
             (this.chiaveEsterna!=null &&
              this.chiaveEsterna.equals(other.getChiaveEsterna()))) &&
            ((this.classification==null && other.getClassification()==null) || 
             (this.classification!=null &&
              this.classification.equals(other.getClassification()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.containers==null && other.getContainers()==null) || 
             (this.containers!=null &&
              java.util.Arrays.equals(this.containers, other.getContainers()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.organizationalArea==null && other.getOrganizationalArea()==null) || 
             (this.organizationalArea!=null &&
              this.organizationalArea.equals(other.getOrganizationalArea()))) &&
            ((this.protocolRegistrations==null && other.getProtocolRegistrations()==null) || 
             (this.protocolRegistrations!=null &&
              java.util.Arrays.equals(this.protocolRegistrations, other.getProtocolRegistrations()))) &&
            ((this.readOnlyState==null && other.getReadOnlyState()==null) || 
             (this.readOnlyState!=null &&
              this.readOnlyState.equals(other.getReadOnlyState()))) &&
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.statusDescription==null && other.getStatusDescription()==null) || 
             (this.statusDescription!=null &&
              this.statusDescription.equals(other.getStatusDescription()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.year==null && other.getYear()==null) || 
             (this.year!=null &&
              this.year.equals(other.getYear())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdministrativeContexts() != null) {
            _hashCode += getAdministrativeContexts().hashCode();
        }
        if (getCategory() != null) {
            _hashCode += getCategory().hashCode();
        }
        if (getChiaveEsterna() != null) {
            _hashCode += getChiaveEsterna().hashCode();
        }
        if (getClassification() != null) {
            _hashCode += getClassification().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getContainers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContainers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContainers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getOrganizationalArea() != null) {
            _hashCode += getOrganizationalArea().hashCode();
        }
        if (getProtocolRegistrations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProtocolRegistrations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProtocolRegistrations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getReadOnlyState() != null) {
            _hashCode += getReadOnlyState().hashCode();
        }
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        if (getStatusDescription() != null) {
            _hashCode += getStatusDescription().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getYear() != null) {
            _hashCode += getYear().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FolderNotProceduralResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FolderNotProceduralResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("administrativeContexts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AdministrativeContexts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Category"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "EnFolderCategory"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chiaveEsterna");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ChiaveEsterna"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ChiaveEsterna"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classification");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classification"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("containers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Containers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Container"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Container"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organizationalArea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "OrganizationalArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "OrganizationalAreaExt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolRegistrations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolRegistrations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolRegistration"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolRegistration"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("readOnlyState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ReadOnlyState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StatusDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("year");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Year"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
