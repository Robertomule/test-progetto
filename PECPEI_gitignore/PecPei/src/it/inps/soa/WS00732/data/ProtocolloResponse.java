/**
 * ProtocolloResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ProtocolloResponse  extends it.inps.soa.WS00732.data.ProtocolloExtResponse  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.DocumentoPrincipaleResponse documento;

    private it.inps.soa.WS00732.data.AllegatoResponse[] allegati;

    private it.inps.soa.WS00732.data.SoggettoAfferente[] soggettiAfferenti;

    private java.lang.String registrazioneProvenienza;

    private java.lang.String primaRegistrazione;

    private it.inps.soa.WS00732.data.Utente utente;

    private it.inps.soa.WS00732.data.Applicazione applicazione;

    private java.util.Calendar dataInvio;

    private java.util.Calendar dataArrivo;

    private it.inps.soa.WS00732.data.CatenaDocumentale catenaDocumentale;

    private it.inps.soa.WS00732.data.RegistroEmergenza registroEmergenza;

    public ProtocolloResponse() {
    }

    public ProtocolloResponse(
           java.lang.String segnatura,
           java.util.Calendar dataProtocollo,
           java.lang.Integer idProtocollo,
           it.inps.soa.WS00732.data.DocumentoPrincipaleResponse documento,
           it.inps.soa.WS00732.data.AllegatoResponse[] allegati,
           it.inps.soa.WS00732.data.SoggettoAfferente[] soggettiAfferenti,
           java.lang.String registrazioneProvenienza,
           java.lang.String primaRegistrazione,
           it.inps.soa.WS00732.data.Utente utente,
           it.inps.soa.WS00732.data.Applicazione applicazione,
           java.util.Calendar dataInvio,
           java.util.Calendar dataArrivo,
           it.inps.soa.WS00732.data.CatenaDocumentale catenaDocumentale,
           it.inps.soa.WS00732.data.RegistroEmergenza registroEmergenza) {
        super(
            segnatura,
            dataProtocollo,
            idProtocollo);
        this.documento = documento;
        this.allegati = allegati;
        this.soggettiAfferenti = soggettiAfferenti;
        this.registrazioneProvenienza = registrazioneProvenienza;
        this.primaRegistrazione = primaRegistrazione;
        this.utente = utente;
        this.applicazione = applicazione;
        this.dataInvio = dataInvio;
        this.dataArrivo = dataArrivo;
        this.catenaDocumentale = catenaDocumentale;
        this.registroEmergenza = registroEmergenza;
    }


    /**
     * Gets the documento value for this ProtocolloResponse.
     * 
     * @return documento
     */
    public it.inps.soa.WS00732.data.DocumentoPrincipaleResponse getDocumento() {
        return documento;
    }


    /**
     * Sets the documento value for this ProtocolloResponse.
     * 
     * @param documento
     */
    public void setDocumento(it.inps.soa.WS00732.data.DocumentoPrincipaleResponse documento) {
        this.documento = documento;
    }


    /**
     * Gets the allegati value for this ProtocolloResponse.
     * 
     * @return allegati
     */
    public it.inps.soa.WS00732.data.AllegatoResponse[] getAllegati() {
        return allegati;
    }


    /**
     * Sets the allegati value for this ProtocolloResponse.
     * 
     * @param allegati
     */
    public void setAllegati(it.inps.soa.WS00732.data.AllegatoResponse[] allegati) {
        this.allegati = allegati;
    }


    /**
     * Gets the soggettiAfferenti value for this ProtocolloResponse.
     * 
     * @return soggettiAfferenti
     */
    public it.inps.soa.WS00732.data.SoggettoAfferente[] getSoggettiAfferenti() {
        return soggettiAfferenti;
    }


    /**
     * Sets the soggettiAfferenti value for this ProtocolloResponse.
     * 
     * @param soggettiAfferenti
     */
    public void setSoggettiAfferenti(it.inps.soa.WS00732.data.SoggettoAfferente[] soggettiAfferenti) {
        this.soggettiAfferenti = soggettiAfferenti;
    }


    /**
     * Gets the registrazioneProvenienza value for this ProtocolloResponse.
     * 
     * @return registrazioneProvenienza
     */
    public java.lang.String getRegistrazioneProvenienza() {
        return registrazioneProvenienza;
    }


    /**
     * Sets the registrazioneProvenienza value for this ProtocolloResponse.
     * 
     * @param registrazioneProvenienza
     */
    public void setRegistrazioneProvenienza(java.lang.String registrazioneProvenienza) {
        this.registrazioneProvenienza = registrazioneProvenienza;
    }


    /**
     * Gets the primaRegistrazione value for this ProtocolloResponse.
     * 
     * @return primaRegistrazione
     */
    public java.lang.String getPrimaRegistrazione() {
        return primaRegistrazione;
    }


    /**
     * Sets the primaRegistrazione value for this ProtocolloResponse.
     * 
     * @param primaRegistrazione
     */
    public void setPrimaRegistrazione(java.lang.String primaRegistrazione) {
        this.primaRegistrazione = primaRegistrazione;
    }


    /**
     * Gets the utente value for this ProtocolloResponse.
     * 
     * @return utente
     */
    public it.inps.soa.WS00732.data.Utente getUtente() {
        return utente;
    }


    /**
     * Sets the utente value for this ProtocolloResponse.
     * 
     * @param utente
     */
    public void setUtente(it.inps.soa.WS00732.data.Utente utente) {
        this.utente = utente;
    }


    /**
     * Gets the applicazione value for this ProtocolloResponse.
     * 
     * @return applicazione
     */
    public it.inps.soa.WS00732.data.Applicazione getApplicazione() {
        return applicazione;
    }


    /**
     * Sets the applicazione value for this ProtocolloResponse.
     * 
     * @param applicazione
     */
    public void setApplicazione(it.inps.soa.WS00732.data.Applicazione applicazione) {
        this.applicazione = applicazione;
    }


    /**
     * Gets the dataInvio value for this ProtocolloResponse.
     * 
     * @return dataInvio
     */
    public java.util.Calendar getDataInvio() {
        return dataInvio;
    }


    /**
     * Sets the dataInvio value for this ProtocolloResponse.
     * 
     * @param dataInvio
     */
    public void setDataInvio(java.util.Calendar dataInvio) {
        this.dataInvio = dataInvio;
    }


    /**
     * Gets the dataArrivo value for this ProtocolloResponse.
     * 
     * @return dataArrivo
     */
    public java.util.Calendar getDataArrivo() {
        return dataArrivo;
    }


    /**
     * Sets the dataArrivo value for this ProtocolloResponse.
     * 
     * @param dataArrivo
     */
    public void setDataArrivo(java.util.Calendar dataArrivo) {
        this.dataArrivo = dataArrivo;
    }


    /**
     * Gets the catenaDocumentale value for this ProtocolloResponse.
     * 
     * @return catenaDocumentale
     */
    public it.inps.soa.WS00732.data.CatenaDocumentale getCatenaDocumentale() {
        return catenaDocumentale;
    }


    /**
     * Sets the catenaDocumentale value for this ProtocolloResponse.
     * 
     * @param catenaDocumentale
     */
    public void setCatenaDocumentale(it.inps.soa.WS00732.data.CatenaDocumentale catenaDocumentale) {
        this.catenaDocumentale = catenaDocumentale;
    }


    /**
     * Gets the registroEmergenza value for this ProtocolloResponse.
     * 
     * @return registroEmergenza
     */
    public it.inps.soa.WS00732.data.RegistroEmergenza getRegistroEmergenza() {
        return registroEmergenza;
    }


    /**
     * Sets the registroEmergenza value for this ProtocolloResponse.
     * 
     * @param registroEmergenza
     */
    public void setRegistroEmergenza(it.inps.soa.WS00732.data.RegistroEmergenza registroEmergenza) {
        this.registroEmergenza = registroEmergenza;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocolloResponse)) return false;
        ProtocolloResponse other = (ProtocolloResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.documento==null && other.getDocumento()==null) || 
             (this.documento!=null &&
              this.documento.equals(other.getDocumento()))) &&
            ((this.allegati==null && other.getAllegati()==null) || 
             (this.allegati!=null &&
              java.util.Arrays.equals(this.allegati, other.getAllegati()))) &&
            ((this.soggettiAfferenti==null && other.getSoggettiAfferenti()==null) || 
             (this.soggettiAfferenti!=null &&
              java.util.Arrays.equals(this.soggettiAfferenti, other.getSoggettiAfferenti()))) &&
            ((this.registrazioneProvenienza==null && other.getRegistrazioneProvenienza()==null) || 
             (this.registrazioneProvenienza!=null &&
              this.registrazioneProvenienza.equals(other.getRegistrazioneProvenienza()))) &&
            ((this.primaRegistrazione==null && other.getPrimaRegistrazione()==null) || 
             (this.primaRegistrazione!=null &&
              this.primaRegistrazione.equals(other.getPrimaRegistrazione()))) &&
            ((this.utente==null && other.getUtente()==null) || 
             (this.utente!=null &&
              this.utente.equals(other.getUtente()))) &&
            ((this.applicazione==null && other.getApplicazione()==null) || 
             (this.applicazione!=null &&
              this.applicazione.equals(other.getApplicazione()))) &&
            ((this.dataInvio==null && other.getDataInvio()==null) || 
             (this.dataInvio!=null &&
              this.dataInvio.equals(other.getDataInvio()))) &&
            ((this.dataArrivo==null && other.getDataArrivo()==null) || 
             (this.dataArrivo!=null &&
              this.dataArrivo.equals(other.getDataArrivo()))) &&
            ((this.catenaDocumentale==null && other.getCatenaDocumentale()==null) || 
             (this.catenaDocumentale!=null &&
              this.catenaDocumentale.equals(other.getCatenaDocumentale()))) &&
            ((this.registroEmergenza==null && other.getRegistroEmergenza()==null) || 
             (this.registroEmergenza!=null &&
              this.registroEmergenza.equals(other.getRegistroEmergenza())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDocumento() != null) {
            _hashCode += getDocumento().hashCode();
        }
        if (getAllegati() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAllegati());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAllegati(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSoggettiAfferenti() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSoggettiAfferenti());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSoggettiAfferenti(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRegistrazioneProvenienza() != null) {
            _hashCode += getRegistrazioneProvenienza().hashCode();
        }
        if (getPrimaRegistrazione() != null) {
            _hashCode += getPrimaRegistrazione().hashCode();
        }
        if (getUtente() != null) {
            _hashCode += getUtente().hashCode();
        }
        if (getApplicazione() != null) {
            _hashCode += getApplicazione().hashCode();
        }
        if (getDataInvio() != null) {
            _hashCode += getDataInvio().hashCode();
        }
        if (getDataArrivo() != null) {
            _hashCode += getDataArrivo().hashCode();
        }
        if (getCatenaDocumentale() != null) {
            _hashCode += getCatenaDocumentale().hashCode();
        }
        if (getRegistroEmergenza() != null) {
            _hashCode += getRegistroEmergenza().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocolloResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolloResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Documento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DocumentoPrincipaleResponse"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allegati");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Allegati"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatoResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatoResponse"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soggettiAfferenti");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettiAfferenti"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettoAfferente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "SoggettoAfferente"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registrazioneProvenienza");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistrazioneProvenienza"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaRegistrazione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "PrimaRegistrazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("utente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Utente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Utente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("applicazione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Applicazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Applicazione"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataInvio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DataInvio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataArrivo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DataArrivo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catenaDocumentale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CatenaDocumentale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CatenaDocumentale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registroEmergenza");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistroEmergenza"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistroEmergenza"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
