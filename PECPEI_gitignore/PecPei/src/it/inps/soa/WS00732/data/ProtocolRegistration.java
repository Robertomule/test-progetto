/**
 * ProtocolRegistration.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class ProtocolRegistration  implements java.io.Serializable {
    private java.util.Calendar checkinDate;

    private java.lang.String dataDocumentManagment;

    private java.lang.String firstRegistration;

    private java.lang.Integer id;

    private java.lang.Integer idPermission;

    private java.lang.Integer idProtocolType;

    private java.lang.Boolean isPreservation;

    private java.lang.Boolean isPreservationRule;

    private java.lang.Boolean isStorage;

    private java.lang.Integer nrAllegati;

    private java.util.Calendar preservationDate;

    private java.util.Calendar preservationRuleDate;

    private java.util.Calendar protocolDate;

    private java.lang.Integer protocolNumber;

    private java.lang.String registerCode;

    private java.lang.String registrationOfOrigin;

    private java.lang.String signature;

    private java.lang.Boolean stateCanceled;

    private java.util.Calendar storageDate;

    private java.lang.Integer yearSignature;

    public ProtocolRegistration() {
    }

    public ProtocolRegistration(
           java.util.Calendar checkinDate,
           java.lang.String dataDocumentManagment,
           java.lang.String firstRegistration,
           java.lang.Integer id,
           java.lang.Integer idPermission,
           java.lang.Integer idProtocolType,
           java.lang.Boolean isPreservation,
           java.lang.Boolean isPreservationRule,
           java.lang.Boolean isStorage,
           java.lang.Integer nrAllegati,
           java.util.Calendar preservationDate,
           java.util.Calendar preservationRuleDate,
           java.util.Calendar protocolDate,
           java.lang.Integer protocolNumber,
           java.lang.String registerCode,
           java.lang.String registrationOfOrigin,
           java.lang.String signature,
           java.lang.Boolean stateCanceled,
           java.util.Calendar storageDate,
           java.lang.Integer yearSignature) {
           this.checkinDate = checkinDate;
           this.dataDocumentManagment = dataDocumentManagment;
           this.firstRegistration = firstRegistration;
           this.id = id;
           this.idPermission = idPermission;
           this.idProtocolType = idProtocolType;
           this.isPreservation = isPreservation;
           this.isPreservationRule = isPreservationRule;
           this.isStorage = isStorage;
           this.nrAllegati = nrAllegati;
           this.preservationDate = preservationDate;
           this.preservationRuleDate = preservationRuleDate;
           this.protocolDate = protocolDate;
           this.protocolNumber = protocolNumber;
           this.registerCode = registerCode;
           this.registrationOfOrigin = registrationOfOrigin;
           this.signature = signature;
           this.stateCanceled = stateCanceled;
           this.storageDate = storageDate;
           this.yearSignature = yearSignature;
    }


    /**
     * Gets the checkinDate value for this ProtocolRegistration.
     * 
     * @return checkinDate
     */
    public java.util.Calendar getCheckinDate() {
        return checkinDate;
    }


    /**
     * Sets the checkinDate value for this ProtocolRegistration.
     * 
     * @param checkinDate
     */
    public void setCheckinDate(java.util.Calendar checkinDate) {
        this.checkinDate = checkinDate;
    }


    /**
     * Gets the dataDocumentManagment value for this ProtocolRegistration.
     * 
     * @return dataDocumentManagment
     */
    public java.lang.String getDataDocumentManagment() {
        return dataDocumentManagment;
    }


    /**
     * Sets the dataDocumentManagment value for this ProtocolRegistration.
     * 
     * @param dataDocumentManagment
     */
    public void setDataDocumentManagment(java.lang.String dataDocumentManagment) {
        this.dataDocumentManagment = dataDocumentManagment;
    }


    /**
     * Gets the firstRegistration value for this ProtocolRegistration.
     * 
     * @return firstRegistration
     */
    public java.lang.String getFirstRegistration() {
        return firstRegistration;
    }


    /**
     * Sets the firstRegistration value for this ProtocolRegistration.
     * 
     * @param firstRegistration
     */
    public void setFirstRegistration(java.lang.String firstRegistration) {
        this.firstRegistration = firstRegistration;
    }


    /**
     * Gets the id value for this ProtocolRegistration.
     * 
     * @return id
     */
    public java.lang.Integer getId() {
        return id;
    }


    /**
     * Sets the id value for this ProtocolRegistration.
     * 
     * @param id
     */
    public void setId(java.lang.Integer id) {
        this.id = id;
    }


    /**
     * Gets the idPermission value for this ProtocolRegistration.
     * 
     * @return idPermission
     */
    public java.lang.Integer getIdPermission() {
        return idPermission;
    }


    /**
     * Sets the idPermission value for this ProtocolRegistration.
     * 
     * @param idPermission
     */
    public void setIdPermission(java.lang.Integer idPermission) {
        this.idPermission = idPermission;
    }


    /**
     * Gets the idProtocolType value for this ProtocolRegistration.
     * 
     * @return idProtocolType
     */
    public java.lang.Integer getIdProtocolType() {
        return idProtocolType;
    }


    /**
     * Sets the idProtocolType value for this ProtocolRegistration.
     * 
     * @param idProtocolType
     */
    public void setIdProtocolType(java.lang.Integer idProtocolType) {
        this.idProtocolType = idProtocolType;
    }


    /**
     * Gets the isPreservation value for this ProtocolRegistration.
     * 
     * @return isPreservation
     */
    public java.lang.Boolean getIsPreservation() {
        return isPreservation;
    }


    /**
     * Sets the isPreservation value for this ProtocolRegistration.
     * 
     * @param isPreservation
     */
    public void setIsPreservation(java.lang.Boolean isPreservation) {
        this.isPreservation = isPreservation;
    }


    /**
     * Gets the isPreservationRule value for this ProtocolRegistration.
     * 
     * @return isPreservationRule
     */
    public java.lang.Boolean getIsPreservationRule() {
        return isPreservationRule;
    }


    /**
     * Sets the isPreservationRule value for this ProtocolRegistration.
     * 
     * @param isPreservationRule
     */
    public void setIsPreservationRule(java.lang.Boolean isPreservationRule) {
        this.isPreservationRule = isPreservationRule;
    }


    /**
     * Gets the isStorage value for this ProtocolRegistration.
     * 
     * @return isStorage
     */
    public java.lang.Boolean getIsStorage() {
        return isStorage;
    }


    /**
     * Sets the isStorage value for this ProtocolRegistration.
     * 
     * @param isStorage
     */
    public void setIsStorage(java.lang.Boolean isStorage) {
        this.isStorage = isStorage;
    }


    /**
     * Gets the nrAllegati value for this ProtocolRegistration.
     * 
     * @return nrAllegati
     */
    public java.lang.Integer getNrAllegati() {
        return nrAllegati;
    }


    /**
     * Sets the nrAllegati value for this ProtocolRegistration.
     * 
     * @param nrAllegati
     */
    public void setNrAllegati(java.lang.Integer nrAllegati) {
        this.nrAllegati = nrAllegati;
    }


    /**
     * Gets the preservationDate value for this ProtocolRegistration.
     * 
     * @return preservationDate
     */
    public java.util.Calendar getPreservationDate() {
        return preservationDate;
    }


    /**
     * Sets the preservationDate value for this ProtocolRegistration.
     * 
     * @param preservationDate
     */
    public void setPreservationDate(java.util.Calendar preservationDate) {
        this.preservationDate = preservationDate;
    }


    /**
     * Gets the preservationRuleDate value for this ProtocolRegistration.
     * 
     * @return preservationRuleDate
     */
    public java.util.Calendar getPreservationRuleDate() {
        return preservationRuleDate;
    }


    /**
     * Sets the preservationRuleDate value for this ProtocolRegistration.
     * 
     * @param preservationRuleDate
     */
    public void setPreservationRuleDate(java.util.Calendar preservationRuleDate) {
        this.preservationRuleDate = preservationRuleDate;
    }


    /**
     * Gets the protocolDate value for this ProtocolRegistration.
     * 
     * @return protocolDate
     */
    public java.util.Calendar getProtocolDate() {
        return protocolDate;
    }


    /**
     * Sets the protocolDate value for this ProtocolRegistration.
     * 
     * @param protocolDate
     */
    public void setProtocolDate(java.util.Calendar protocolDate) {
        this.protocolDate = protocolDate;
    }


    /**
     * Gets the protocolNumber value for this ProtocolRegistration.
     * 
     * @return protocolNumber
     */
    public java.lang.Integer getProtocolNumber() {
        return protocolNumber;
    }


    /**
     * Sets the protocolNumber value for this ProtocolRegistration.
     * 
     * @param protocolNumber
     */
    public void setProtocolNumber(java.lang.Integer protocolNumber) {
        this.protocolNumber = protocolNumber;
    }


    /**
     * Gets the registerCode value for this ProtocolRegistration.
     * 
     * @return registerCode
     */
    public java.lang.String getRegisterCode() {
        return registerCode;
    }


    /**
     * Sets the registerCode value for this ProtocolRegistration.
     * 
     * @param registerCode
     */
    public void setRegisterCode(java.lang.String registerCode) {
        this.registerCode = registerCode;
    }


    /**
     * Gets the registrationOfOrigin value for this ProtocolRegistration.
     * 
     * @return registrationOfOrigin
     */
    public java.lang.String getRegistrationOfOrigin() {
        return registrationOfOrigin;
    }


    /**
     * Sets the registrationOfOrigin value for this ProtocolRegistration.
     * 
     * @param registrationOfOrigin
     */
    public void setRegistrationOfOrigin(java.lang.String registrationOfOrigin) {
        this.registrationOfOrigin = registrationOfOrigin;
    }


    /**
     * Gets the signature value for this ProtocolRegistration.
     * 
     * @return signature
     */
    public java.lang.String getSignature() {
        return signature;
    }


    /**
     * Sets the signature value for this ProtocolRegistration.
     * 
     * @param signature
     */
    public void setSignature(java.lang.String signature) {
        this.signature = signature;
    }


    /**
     * Gets the stateCanceled value for this ProtocolRegistration.
     * 
     * @return stateCanceled
     */
    public java.lang.Boolean getStateCanceled() {
        return stateCanceled;
    }


    /**
     * Sets the stateCanceled value for this ProtocolRegistration.
     * 
     * @param stateCanceled
     */
    public void setStateCanceled(java.lang.Boolean stateCanceled) {
        this.stateCanceled = stateCanceled;
    }


    /**
     * Gets the storageDate value for this ProtocolRegistration.
     * 
     * @return storageDate
     */
    public java.util.Calendar getStorageDate() {
        return storageDate;
    }


    /**
     * Sets the storageDate value for this ProtocolRegistration.
     * 
     * @param storageDate
     */
    public void setStorageDate(java.util.Calendar storageDate) {
        this.storageDate = storageDate;
    }


    /**
     * Gets the yearSignature value for this ProtocolRegistration.
     * 
     * @return yearSignature
     */
    public java.lang.Integer getYearSignature() {
        return yearSignature;
    }


    /**
     * Sets the yearSignature value for this ProtocolRegistration.
     * 
     * @param yearSignature
     */
    public void setYearSignature(java.lang.Integer yearSignature) {
        this.yearSignature = yearSignature;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocolRegistration)) return false;
        ProtocolRegistration other = (ProtocolRegistration) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.checkinDate==null && other.getCheckinDate()==null) || 
             (this.checkinDate!=null &&
              this.checkinDate.equals(other.getCheckinDate()))) &&
            ((this.dataDocumentManagment==null && other.getDataDocumentManagment()==null) || 
             (this.dataDocumentManagment!=null &&
              this.dataDocumentManagment.equals(other.getDataDocumentManagment()))) &&
            ((this.firstRegistration==null && other.getFirstRegistration()==null) || 
             (this.firstRegistration!=null &&
              this.firstRegistration.equals(other.getFirstRegistration()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.idPermission==null && other.getIdPermission()==null) || 
             (this.idPermission!=null &&
              this.idPermission.equals(other.getIdPermission()))) &&
            ((this.idProtocolType==null && other.getIdProtocolType()==null) || 
             (this.idProtocolType!=null &&
              this.idProtocolType.equals(other.getIdProtocolType()))) &&
            ((this.isPreservation==null && other.getIsPreservation()==null) || 
             (this.isPreservation!=null &&
              this.isPreservation.equals(other.getIsPreservation()))) &&
            ((this.isPreservationRule==null && other.getIsPreservationRule()==null) || 
             (this.isPreservationRule!=null &&
              this.isPreservationRule.equals(other.getIsPreservationRule()))) &&
            ((this.isStorage==null && other.getIsStorage()==null) || 
             (this.isStorage!=null &&
              this.isStorage.equals(other.getIsStorage()))) &&
            ((this.nrAllegati==null && other.getNrAllegati()==null) || 
             (this.nrAllegati!=null &&
              this.nrAllegati.equals(other.getNrAllegati()))) &&
            ((this.preservationDate==null && other.getPreservationDate()==null) || 
             (this.preservationDate!=null &&
              this.preservationDate.equals(other.getPreservationDate()))) &&
            ((this.preservationRuleDate==null && other.getPreservationRuleDate()==null) || 
             (this.preservationRuleDate!=null &&
              this.preservationRuleDate.equals(other.getPreservationRuleDate()))) &&
            ((this.protocolDate==null && other.getProtocolDate()==null) || 
             (this.protocolDate!=null &&
              this.protocolDate.equals(other.getProtocolDate()))) &&
            ((this.protocolNumber==null && other.getProtocolNumber()==null) || 
             (this.protocolNumber!=null &&
              this.protocolNumber.equals(other.getProtocolNumber()))) &&
            ((this.registerCode==null && other.getRegisterCode()==null) || 
             (this.registerCode!=null &&
              this.registerCode.equals(other.getRegisterCode()))) &&
            ((this.registrationOfOrigin==null && other.getRegistrationOfOrigin()==null) || 
             (this.registrationOfOrigin!=null &&
              this.registrationOfOrigin.equals(other.getRegistrationOfOrigin()))) &&
            ((this.signature==null && other.getSignature()==null) || 
             (this.signature!=null &&
              this.signature.equals(other.getSignature()))) &&
            ((this.stateCanceled==null && other.getStateCanceled()==null) || 
             (this.stateCanceled!=null &&
              this.stateCanceled.equals(other.getStateCanceled()))) &&
            ((this.storageDate==null && other.getStorageDate()==null) || 
             (this.storageDate!=null &&
              this.storageDate.equals(other.getStorageDate()))) &&
            ((this.yearSignature==null && other.getYearSignature()==null) || 
             (this.yearSignature!=null &&
              this.yearSignature.equals(other.getYearSignature())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCheckinDate() != null) {
            _hashCode += getCheckinDate().hashCode();
        }
        if (getDataDocumentManagment() != null) {
            _hashCode += getDataDocumentManagment().hashCode();
        }
        if (getFirstRegistration() != null) {
            _hashCode += getFirstRegistration().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getIdPermission() != null) {
            _hashCode += getIdPermission().hashCode();
        }
        if (getIdProtocolType() != null) {
            _hashCode += getIdProtocolType().hashCode();
        }
        if (getIsPreservation() != null) {
            _hashCode += getIsPreservation().hashCode();
        }
        if (getIsPreservationRule() != null) {
            _hashCode += getIsPreservationRule().hashCode();
        }
        if (getIsStorage() != null) {
            _hashCode += getIsStorage().hashCode();
        }
        if (getNrAllegati() != null) {
            _hashCode += getNrAllegati().hashCode();
        }
        if (getPreservationDate() != null) {
            _hashCode += getPreservationDate().hashCode();
        }
        if (getPreservationRuleDate() != null) {
            _hashCode += getPreservationRuleDate().hashCode();
        }
        if (getProtocolDate() != null) {
            _hashCode += getProtocolDate().hashCode();
        }
        if (getProtocolNumber() != null) {
            _hashCode += getProtocolNumber().hashCode();
        }
        if (getRegisterCode() != null) {
            _hashCode += getRegisterCode().hashCode();
        }
        if (getRegistrationOfOrigin() != null) {
            _hashCode += getRegistrationOfOrigin().hashCode();
        }
        if (getSignature() != null) {
            _hashCode += getSignature().hashCode();
        }
        if (getStateCanceled() != null) {
            _hashCode += getStateCanceled().hashCode();
        }
        if (getStorageDate() != null) {
            _hashCode += getStorageDate().hashCode();
        }
        if (getYearSignature() != null) {
            _hashCode += getYearSignature().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocolRegistration.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolRegistration"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkinDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "CheckinDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataDocumentManagment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "DataDocumentManagment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstRegistration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "FirstRegistration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPermission");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdPermission"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idProtocolType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IdProtocolType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPreservation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IsPreservation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPreservationRule");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IsPreservationRule"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isStorage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "IsStorage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nrAllegati");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "NrAllegati"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preservationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "PreservationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preservationRuleDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "PreservationRuleDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "ProtocolNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registerCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegisterCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registrationOfOrigin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "RegistrationOfOrigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("signature");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Signature"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateCanceled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StateCanceled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storageDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "StorageDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("yearSignature");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "YearSignature"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
