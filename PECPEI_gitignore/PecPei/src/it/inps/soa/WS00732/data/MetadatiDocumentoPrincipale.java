/**
 * MetadatiDocumentoPrincipale.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class MetadatiDocumentoPrincipale  extends it.inps.soa.WS00732.data.MetadatiDocumento  implements java.io.Serializable {
    private it.inps.soa.WS00732.data.Classifica[] classificazioni;

    public MetadatiDocumentoPrincipale() {
    }

    public MetadatiDocumentoPrincipale(
           java.lang.Boolean autocertificazione,
           java.lang.String note,
           java.lang.String oggetto,
           java.lang.Integer ownerAppl,
           java.lang.Boolean riservato,
           java.lang.Boolean sensibile,
           it.inps.soa.WS00732.data.Classifica[] classificazioni) {
        super(
            autocertificazione,
            note,
            oggetto,
            ownerAppl,
            riservato,
            sensibile);
        this.classificazioni = classificazioni;
    }


    /**
     * Gets the classificazioni value for this MetadatiDocumentoPrincipale.
     * 
     * @return classificazioni
     */
    public it.inps.soa.WS00732.data.Classifica[] getClassificazioni() {
        return classificazioni;
    }


    /**
     * Sets the classificazioni value for this MetadatiDocumentoPrincipale.
     * 
     * @param classificazioni
     */
    public void setClassificazioni(it.inps.soa.WS00732.data.Classifica[] classificazioni) {
        this.classificazioni = classificazioni;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MetadatiDocumentoPrincipale)) return false;
        MetadatiDocumentoPrincipale other = (MetadatiDocumentoPrincipale) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.classificazioni==null && other.getClassificazioni()==null) || 
             (this.classificazioni!=null &&
              java.util.Arrays.equals(this.classificazioni, other.getClassificazioni())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getClassificazioni() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClassificazioni());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClassificazioni(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MetadatiDocumentoPrincipale.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "MetadatiDocumentoPrincipale"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classificazioni");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classificazioni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "Classifica"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
