/**
 * AllegatoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732.data;

public class AllegatoResponse  extends it.inps.soa.WS00732.data.DocumentoResponse  implements java.io.Serializable {
    public AllegatoResponse() {
    }

    public AllegatoResponse(
           it.inps.soa.WS00732.data.FingerPrint fingerPrint,
           it.inps.soa.WS00732.data.EnTipoAcquisizione tipoAcquisizione,
           java.lang.Boolean autocertificazione,
           java.lang.String oggetto,
           it.inps.soa.WS00732.data.DocumentoElettronicoResponse documentoElettronico,
           java.lang.Boolean riservato,
           java.lang.Boolean sensibile,
           java.lang.String note,
           it.inps.soa.WS00732.data.ProcessoDestinatarioResponse processoDestinatario) {
        super(
            fingerPrint,
            tipoAcquisizione,
            autocertificazione,
            oggetto,
            documentoElettronico,
            riservato,
            sensibile,
            note,
            processoDestinatario);
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AllegatoResponse)) return false;
        AllegatoResponse other = (AllegatoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj);
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AllegatoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.inps.it/WS00732/data", "AllegatoResponse"));
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
