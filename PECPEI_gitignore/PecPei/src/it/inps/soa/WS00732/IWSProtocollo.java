/**
 * IWSProtocollo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00732;

public interface IWSProtocollo extends java.rmi.Remote {
    public java.lang.String getVersion() throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInEntrata(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaResponse protocollaInUscita(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInEntrataEdAllega(it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInUscitaEdAllegaCrossAOO(it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest, java.lang.String segnaturaPrincipale, java.lang.String AOOdelProtocollo) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInEntrataEdAllegaCrossAOO(it.inps.soa.WS00732.data.ProtocolloEntrataRequest protocolloEntrataRequest, java.lang.String segnaturaPrincipale, java.lang.String AOOdelProtocollo) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ListaAllegatiResponse estraiListaAllegati(java.lang.String segnatura) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ListaAllegatiResponse estraiListaCatenaTrasferimenti(java.lang.String segnatura) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ListaCatenaDocumentaleResponse estraiCatenaDocumentale(java.lang.String segnatura) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.CreaCatenaDocumentaleResponse creaCatenaDocumentale(java.lang.String segnaturaPadre, java.lang.String segnaturaFiglia) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaAllegaResponse protocollaInUscitaEdAllega(it.inps.soa.WS00732.data.ProtocolloUscitaRequest protocolloUscitaRequest, java.lang.String segnaturaPrincipale) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ModificaResponse modificaMetadatiProtocollo(java.lang.String segnatura, it.inps.soa.WS00732.data.MetadatiProtocollo metadatiProtocollo) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.AnnullaResponse annullaProtocollo(java.lang.String segnatura, it.inps.soa.WS00732.data.AnnullaProtocolloRequest annullaProtocolloRequest) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.MetadatiResponse estraiMetadatiDocumento(java.lang.String segnatura) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.GeneraRicevutaResponse generaRicevutaProtocollo(java.lang.String segnatura) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.RicevutaProtocolloBarcodeResponse generaRicevutaProtocolloBarcode(java.lang.String segnatura) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.AssociaDocumentoPrimarioResponse associaDocumentoPrimario(it.inps.soa.WS00732.data.DocumentoPrimarioRequest documentoPrimarioRequest) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.AllegatiResponse aggiungiAllegatiContestuali(it.inps.soa.WS00732.data.AllegatiContestualiRequest allegatiContestualiRequest) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInEntrata(it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.TrasferisciResponse trasferisciProtocolloInUscita(it.inps.soa.WS00732.data.TrasferisciRequest trasferisciRequest) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse richiestaProtocollazioneInEntrata(it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse richiestaInteroperabilitaInEntrata(it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaNAIResponse protocollaInEntrataNAI(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataNAIRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleResponse creaFascicoloProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.CreaFascicoloProcedimentaleRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.CreaCatenaFascicolazioneResponse creaCatenaFascicolazione(it.inps.soa.WS00732.data.CreaCatenaFascicolazioneRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInEntrataConCatenaDiFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConCatenaDiFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaConFascicolazioneProcedimentaleResponse protocollaInUscitaConCatenaDiFascicolazioneProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConCatenaDiFascicolazioneProcedimentaleRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressa(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInEntrataConFascicolazionePregressaEVariazioneDiStato(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInEntrataConFascicolazionePregressaEVariazioneDiStatoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressa(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaConFascicolazionePregressaResponse protocollaInUscitaConFascicolazionePregressaEVariazioneDiStato(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocollaInUscitaConFascicolazionePregressaEVariazioneDiStatoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.DocumentiFascicoloResponse estraiDocumentiFascicolo(java.lang.String codiceFascicolo) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoResponse fascicolaDocumentoProtocollato(it.inps.soa.WS00732.data.FascicolaDocumentoProtocollatoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.CatenaFascicolazioneResponse estraiCatenaFascicolazione(java.lang.String codiceFascicolo) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.VariaEsitoProcedimentoResponse variaEsitoProcedimento(it.inps.soa.WS00732.data.VariaEsitoProcedimentoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.RichiestaCRMResponse richiestaCRM(it.inps.soa.WS00732.data.RichiestaCRMRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaInEntrataESpedisciResponse protocollaInEntrataESpedisci(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloEntrataESpedisciRequest protocollaInEntrataESpedisciRequest) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaInUscitaESpedisciResponse protocollaInUscitaESpedisci(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ProtocolloUscitaESpedisciRequest protocolloUscitaESpedisciRequest) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleResponse creaFascicoloNonProcedimentale(java.lang.String codiceAOO, it.inps.soa.WS00732.data.CreaFascicoloNonProcedimentaleRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ContainerResponse creaContainer(java.lang.String codiceAOO, it.inps.soa.WS00732.data.ContainerRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.AssociaContainerAFascicoloResponse associaContainerAFascicolo(it.inps.soa.WS00732.data.AssociaContainerAFascicoloRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse chiudiFascicoloNonProcedimentale(it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleResponse riapriFascicoloNonProcedimentale(it.inps.soa.WS00732.data.StatoFascicoloNonProcedimentaleRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse estraiPraticaDaFascicolo(java.lang.String codiceFascicolo) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.DocumentiFascicoloNonProcedimentaleResponse estraiPratica(java.lang.String codicePratica, java.lang.String tipoCodicePratica) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.RifiutaRichiestaPECInUscitaResponse rifiutaRichiestaPECInUscita(java.lang.String messageId, java.lang.String motivazione) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.AutorizzaRichiestaPECInUscitaResponse autorizzaRichiestaPECInUscita(java.lang.String messageId) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse protocollaPECInUscita(java.lang.String messageId) throws java.rmi.RemoteException;
    public it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse registraStatoInvioPECInUscita(it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaRequest request) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse registraStatoConsegnaPECInUscita(it.inps.soa.WS00732.data.RegistraStatoConsegnaPECInUscitaRequest request) throws java.rmi.RemoteException;
}
