package it.inps.soa.classi;
/**
 * ApplicazioniResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class ApplicazioniResponse  extends Response  implements java.io.Serializable {
    private Application[] applicazioni;

    public ApplicazioniResponse() {
    }

    public ApplicazioniResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           Application[] applicazioni) {
        super(
            codice,
            descrizione);
        this.applicazioni = applicazioni;
    }


    /**
     * Gets the applicazioni value for this ApplicazioniResponse.
     * 
     * @return applicazioni
     */
    public Application[] getApplicazioni() {
        return applicazioni;
    }


    /**
     * Sets the applicazioni value for this ApplicazioniResponse.
     * 
     * @param applicazioni
     */
    public void setApplicazioni(Application[] applicazioni) {
        this.applicazioni = applicazioni;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ApplicazioniResponse)) return false;
        ApplicazioniResponse other = (ApplicazioniResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.applicazioni==null && other.getApplicazioni()==null) || 
             (this.applicazioni!=null &&
              java.util.Arrays.equals(this.applicazioni, other.getApplicazioni())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getApplicazioni() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getApplicazioni());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getApplicazioni(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ApplicazioniResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "ApplicazioniResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("applicazioni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Applicazioni"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Application"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Application"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
