package it.inps.soa.classi;
/**
 * DocumentoPrincipale.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class DocumentoPrincipale  extends Documento  implements java.io.Serializable {
    private java.lang.Integer ownerAppl;

    private Classifica[] classificazioni;

    public DocumentoPrincipale() {
    }

    public DocumentoPrincipale(
           FingerPrint fingerPrint,
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoAcquisizione tipoAcquisizione,
           java.lang.Boolean autocertificazione,
           java.lang.String oggetto,
           DocumentoElettronico documentoElettronico,
           java.lang.Boolean riservato,
           java.lang.Boolean sensibile,
           java.lang.String note,
           ProcessoDestinatario processoDestinatario,
           java.lang.Integer ownerAppl,
           Classifica[] classificazioni) {
        super(
            fingerPrint,
            tipoAcquisizione,
            autocertificazione,
            oggetto,
            documentoElettronico,
            riservato,
            sensibile,
            note,
            processoDestinatario);
        this.ownerAppl = ownerAppl;
        this.classificazioni = classificazioni;
    }


    /**
     * Gets the ownerAppl value for this DocumentoPrincipale.
     * 
     * @return ownerAppl
     */
    public java.lang.Integer getOwnerAppl() {
        return ownerAppl;
    }


    /**
     * Sets the ownerAppl value for this DocumentoPrincipale.
     * 
     * @param ownerAppl
     */
    public void setOwnerAppl(java.lang.Integer ownerAppl) {
        this.ownerAppl = ownerAppl;
    }


    /**
     * Gets the classificazioni value for this DocumentoPrincipale.
     * 
     * @return classificazioni
     */
    public Classifica[] getClassificazioni() {
        return classificazioni;
    }


    /**
     * Sets the classificazioni value for this DocumentoPrincipale.
     * 
     * @param classificazioni
     */
    public void setClassificazioni(Classifica[] classificazioni) {
        this.classificazioni = classificazioni;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentoPrincipale)) return false;
        DocumentoPrincipale other = (DocumentoPrincipale) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ownerAppl==null && other.getOwnerAppl()==null) || 
             (this.ownerAppl!=null &&
              this.ownerAppl.equals(other.getOwnerAppl()))) &&
            ((this.classificazioni==null && other.getClassificazioni()==null) || 
             (this.classificazioni!=null &&
              java.util.Arrays.equals(this.classificazioni, other.getClassificazioni())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getOwnerAppl() != null) {
            _hashCode += getOwnerAppl().hashCode();
        }
        if (getClassificazioni() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClassificazioni());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClassificazioni(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentoPrincipale.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "DocumentoPrincipale"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerAppl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OwnerAppl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classificazioni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Classificazioni"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Classifica"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Classifica"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
