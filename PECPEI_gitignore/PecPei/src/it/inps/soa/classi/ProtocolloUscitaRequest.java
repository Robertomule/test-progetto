package it.inps.soa.classi;
/**
 * ProtocolloUscitaRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class ProtocolloUscitaRequest  extends Protocollo  implements java.io.Serializable {
    private Destinatario[] destinatari;

    public ProtocolloUscitaRequest() {
    }

    public ProtocolloUscitaRequest(
           DocumentoPrincipale documento,
           Allegato[] allegati,
           SoggettoAfferente[] soggettiAfferenti,
           java.lang.String registrazioneProvenienza,
           java.lang.String primaRegistrazione,
           java.util.Calendar dataInvio,
           java.util.Calendar dataArrivo,
           Destinatario[] destinatari) {
        super(
            documento,
            allegati,
            soggettiAfferenti,
            registrazioneProvenienza,
            primaRegistrazione,
            dataInvio,
            dataArrivo);
        this.destinatari = destinatari;
    }


    /**
     * Gets the destinatari value for this ProtocolloUscitaRequest.
     * 
     * @return destinatari
     */
    public Destinatario[] getDestinatari() {
        return destinatari;
    }


    /**
     * Sets the destinatari value for this ProtocolloUscitaRequest.
     * 
     * @param destinatari
     */
    public void setDestinatari(Destinatario[] destinatari) {
        this.destinatari = destinatari;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocolloUscitaRequest)) return false;
        ProtocolloUscitaRequest other = (ProtocolloUscitaRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.destinatari==null && other.getDestinatari()==null) || 
             (this.destinatari!=null &&
              java.util.Arrays.equals(this.destinatari, other.getDestinatari())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDestinatari() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDestinatari());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDestinatari(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocolloUscitaRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "ProtocolloUscitaRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinatari");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Destinatari"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Destinatario"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Destinatario"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
