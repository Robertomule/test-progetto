package it.inps.soa.classi;
/**
 * AreeOrganizzativeResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class AreeOrganizzativeResponse  extends Response  implements java.io.Serializable {
    private AreaOrganizzativa[] areeOrganizzative;

    public AreeOrganizzativeResponse() {
    }

    public AreeOrganizzativeResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           AreaOrganizzativa[] areeOrganizzative) {
        super(
            codice,
            descrizione);
        this.areeOrganizzative = areeOrganizzative;
    }


    /**
     * Gets the areeOrganizzative value for this AreeOrganizzativeResponse.
     * 
     * @return areeOrganizzative
     */
    public AreaOrganizzativa[] getAreeOrganizzative() {
        return areeOrganizzative;
    }


    /**
     * Sets the areeOrganizzative value for this AreeOrganizzativeResponse.
     * 
     * @param areeOrganizzative
     */
    public void setAreeOrganizzative(AreaOrganizzativa[] areeOrganizzative) {
        this.areeOrganizzative = areeOrganizzative;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AreeOrganizzativeResponse)) return false;
        AreeOrganizzativeResponse other = (AreeOrganizzativeResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.areeOrganizzative==null && other.getAreeOrganizzative()==null) || 
             (this.areeOrganizzative!=null &&
              java.util.Arrays.equals(this.areeOrganizzative, other.getAreeOrganizzative())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAreeOrganizzative() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAreeOrganizzative());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAreeOrganizzative(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AreeOrganizzativeResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "AreeOrganizzativeResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("areeOrganizzative");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AreeOrganizzative"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "AreaOrganizzativa"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "AreaOrganizzativa"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
