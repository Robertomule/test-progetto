package it.inps.soa.classi;
/**
 * Classificazione.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Classificazione  implements java.io.Serializable {
    private java.lang.Boolean classificationAllowed;

    private java.lang.String codice;

    private java.lang.String codicePadre;

    private java.util.Calendar dataCreazione;

    private java.lang.String descrizione;

    private java.lang.Integer idPadre;

    private ClassificationSchemas schema;

    private java.lang.String statoCorrente;

    private java.util.Calendar validoAL;

    private java.util.Calendar validoDA;

    public Classificazione() {
    }

    public Classificazione(
           java.lang.Boolean classificationAllowed,
           java.lang.String codice,
           java.lang.String codicePadre,
           java.util.Calendar dataCreazione,
           java.lang.String descrizione,
           java.lang.Integer idPadre,
           ClassificationSchemas schema,
           java.lang.String statoCorrente,
           java.util.Calendar validoAL,
           java.util.Calendar validoDA) {
           this.classificationAllowed = classificationAllowed;
           this.codice = codice;
           this.codicePadre = codicePadre;
           this.dataCreazione = dataCreazione;
           this.descrizione = descrizione;
           this.idPadre = idPadre;
           this.schema = schema;
           this.statoCorrente = statoCorrente;
           this.validoAL = validoAL;
           this.validoDA = validoDA;
    }


    /**
     * Gets the classificationAllowed value for this Classificazione.
     * 
     * @return classificationAllowed
     */
    public java.lang.Boolean getClassificationAllowed() {
        return classificationAllowed;
    }


    /**
     * Sets the classificationAllowed value for this Classificazione.
     * 
     * @param classificationAllowed
     */
    public void setClassificationAllowed(java.lang.Boolean classificationAllowed) {
        this.classificationAllowed = classificationAllowed;
    }


    /**
     * Gets the codice value for this Classificazione.
     * 
     * @return codice
     */
    public java.lang.String getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this Classificazione.
     * 
     * @param codice
     */
    public void setCodice(java.lang.String codice) {
        this.codice = codice;
    }


    /**
     * Gets the codicePadre value for this Classificazione.
     * 
     * @return codicePadre
     */
    public java.lang.String getCodicePadre() {
        return codicePadre;
    }


    /**
     * Sets the codicePadre value for this Classificazione.
     * 
     * @param codicePadre
     */
    public void setCodicePadre(java.lang.String codicePadre) {
        this.codicePadre = codicePadre;
    }


    /**
     * Gets the dataCreazione value for this Classificazione.
     * 
     * @return dataCreazione
     */
    public java.util.Calendar getDataCreazione() {
        return dataCreazione;
    }


    /**
     * Sets the dataCreazione value for this Classificazione.
     * 
     * @param dataCreazione
     */
    public void setDataCreazione(java.util.Calendar dataCreazione) {
        this.dataCreazione = dataCreazione;
    }


    /**
     * Gets the descrizione value for this Classificazione.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this Classificazione.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the idPadre value for this Classificazione.
     * 
     * @return idPadre
     */
    public java.lang.Integer getIdPadre() {
        return idPadre;
    }


    /**
     * Sets the idPadre value for this Classificazione.
     * 
     * @param idPadre
     */
    public void setIdPadre(java.lang.Integer idPadre) {
        this.idPadre = idPadre;
    }


    /**
     * Gets the schema value for this Classificazione.
     * 
     * @return schema
     */
    public ClassificationSchemas getSchema() {
        return schema;
    }


    /**
     * Sets the schema value for this Classificazione.
     * 
     * @param schema
     */
    public void setSchema(ClassificationSchemas schema) {
        this.schema = schema;
    }


    /**
     * Gets the statoCorrente value for this Classificazione.
     * 
     * @return statoCorrente
     */
    public java.lang.String getStatoCorrente() {
        return statoCorrente;
    }


    /**
     * Sets the statoCorrente value for this Classificazione.
     * 
     * @param statoCorrente
     */
    public void setStatoCorrente(java.lang.String statoCorrente) {
        this.statoCorrente = statoCorrente;
    }


    /**
     * Gets the validoAL value for this Classificazione.
     * 
     * @return validoAL
     */
    public java.util.Calendar getValidoAL() {
        return validoAL;
    }


    /**
     * Sets the validoAL value for this Classificazione.
     * 
     * @param validoAL
     */
    public void setValidoAL(java.util.Calendar validoAL) {
        this.validoAL = validoAL;
    }


    /**
     * Gets the validoDA value for this Classificazione.
     * 
     * @return validoDA
     */
    public java.util.Calendar getValidoDA() {
        return validoDA;
    }


    /**
     * Sets the validoDA value for this Classificazione.
     * 
     * @param validoDA
     */
    public void setValidoDA(java.util.Calendar validoDA) {
        this.validoDA = validoDA;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Classificazione)) return false;
        Classificazione other = (Classificazione) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.classificationAllowed==null && other.getClassificationAllowed()==null) || 
             (this.classificationAllowed!=null &&
              this.classificationAllowed.equals(other.getClassificationAllowed()))) &&
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.codicePadre==null && other.getCodicePadre()==null) || 
             (this.codicePadre!=null &&
              this.codicePadre.equals(other.getCodicePadre()))) &&
            ((this.dataCreazione==null && other.getDataCreazione()==null) || 
             (this.dataCreazione!=null &&
              this.dataCreazione.equals(other.getDataCreazione()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.idPadre==null && other.getIdPadre()==null) || 
             (this.idPadre!=null &&
              this.idPadre.equals(other.getIdPadre()))) &&
            ((this.schema==null && other.getSchema()==null) || 
             (this.schema!=null &&
              this.schema.equals(other.getSchema()))) &&
            ((this.statoCorrente==null && other.getStatoCorrente()==null) || 
             (this.statoCorrente!=null &&
              this.statoCorrente.equals(other.getStatoCorrente()))) &&
            ((this.validoAL==null && other.getValidoAL()==null) || 
             (this.validoAL!=null &&
              this.validoAL.equals(other.getValidoAL()))) &&
            ((this.validoDA==null && other.getValidoDA()==null) || 
             (this.validoDA!=null &&
              this.validoDA.equals(other.getValidoDA())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClassificationAllowed() != null) {
            _hashCode += getClassificationAllowed().hashCode();
        }
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getCodicePadre() != null) {
            _hashCode += getCodicePadre().hashCode();
        }
        if (getDataCreazione() != null) {
            _hashCode += getDataCreazione().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getIdPadre() != null) {
            _hashCode += getIdPadre().hashCode();
        }
        if (getSchema() != null) {
            _hashCode += getSchema().hashCode();
        }
        if (getStatoCorrente() != null) {
            _hashCode += getStatoCorrente().hashCode();
        }
        if (getValidoAL() != null) {
            _hashCode += getValidoAL().hashCode();
        }
        if (getValidoDA() != null) {
            _hashCode += getValidoDA().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Classificazione.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Classificazione"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classificationAllowed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ClassificationAllowed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codicePadre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodicePadre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataCreazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataCreazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPadre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IdPadre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("schema");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Schema"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "ClassificationSchemas"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statoCorrente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StatoCorrente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validoAL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ValidoAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validoDA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ValidoDA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
