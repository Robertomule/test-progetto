package it.inps.soa.classi;
/**
 * Istituzione.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Istituzione  extends AddressBook  implements java.io.Serializable {
    private java.lang.String codiceAreaOrganizzativa;

    private java.lang.String codiceOrganizzazione;

    private java.lang.String codiceUnitaOrganizzativa;

    private java.lang.String descrAreaOrganizzativa;

    private java.lang.String descrOrganizzazione;

    private java.lang.String descrUnitaOrganizzativa;

    private java.lang.String numeroRegistrazione;

    private java.lang.String partitaIva;

    public Istituzione() {
    }

    public Istituzione(
           java.lang.String CAP,
           java.lang.String citta,
           java.lang.String country,
           java.util.Calendar dataCreazione,
           java.lang.Boolean disabilitato,
           java.lang.String EMailCertificata,
           java.lang.String email,
           java.lang.String fax,
           java.lang.Integer id,
           java.lang.Integer idParent,
           java.lang.String indirizzo,
           java.lang.String nazione,
           java.lang.String note,
           java.lang.Integer notificaPreferita,
           java.lang.Integer numeroTelPreferito,
           org.apache.axis.types.Duration orarioPreferito,
           java.lang.String provincia,
           java.lang.String regione,
           java.lang.String telefono,
           java.lang.String telefonoCasa,
           java.lang.String telefonoUfficio,
           java.lang.String tipoSoggetto,
           java.lang.String codiceAreaOrganizzativa,
           java.lang.String codiceOrganizzazione,
           java.lang.String codiceUnitaOrganizzativa,
           java.lang.String descrAreaOrganizzativa,
           java.lang.String descrOrganizzazione,
           java.lang.String descrUnitaOrganizzativa,
           java.lang.String numeroRegistrazione,
           java.lang.String partitaIva) {
        super(
            CAP,
            citta,
            country,
            dataCreazione,
            disabilitato,
            EMailCertificata,
            email,
            fax,
            id,
            idParent,
            indirizzo,
            nazione,
            note,
            notificaPreferita,
            numeroTelPreferito,
            orarioPreferito,
            provincia,
            regione,
            telefono,
            telefonoCasa,
            telefonoUfficio,
            tipoSoggetto);
        this.codiceAreaOrganizzativa = codiceAreaOrganizzativa;
        this.codiceOrganizzazione = codiceOrganizzazione;
        this.codiceUnitaOrganizzativa = codiceUnitaOrganizzativa;
        this.descrAreaOrganizzativa = descrAreaOrganizzativa;
        this.descrOrganizzazione = descrOrganizzazione;
        this.descrUnitaOrganizzativa = descrUnitaOrganizzativa;
        this.numeroRegistrazione = numeroRegistrazione;
        this.partitaIva = partitaIva;
    }


    /**
     * Gets the codiceAreaOrganizzativa value for this Istituzione.
     * 
     * @return codiceAreaOrganizzativa
     */
    public java.lang.String getCodiceAreaOrganizzativa() {
        return codiceAreaOrganizzativa;
    }


    /**
     * Sets the codiceAreaOrganizzativa value for this Istituzione.
     * 
     * @param codiceAreaOrganizzativa
     */
    public void setCodiceAreaOrganizzativa(java.lang.String codiceAreaOrganizzativa) {
        this.codiceAreaOrganizzativa = codiceAreaOrganizzativa;
    }


    /**
     * Gets the codiceOrganizzazione value for this Istituzione.
     * 
     * @return codiceOrganizzazione
     */
    public java.lang.String getCodiceOrganizzazione() {
        return codiceOrganizzazione;
    }


    /**
     * Sets the codiceOrganizzazione value for this Istituzione.
     * 
     * @param codiceOrganizzazione
     */
    public void setCodiceOrganizzazione(java.lang.String codiceOrganizzazione) {
        this.codiceOrganizzazione = codiceOrganizzazione;
    }


    /**
     * Gets the codiceUnitaOrganizzativa value for this Istituzione.
     * 
     * @return codiceUnitaOrganizzativa
     */
    public java.lang.String getCodiceUnitaOrganizzativa() {
        return codiceUnitaOrganizzativa;
    }


    /**
     * Sets the codiceUnitaOrganizzativa value for this Istituzione.
     * 
     * @param codiceUnitaOrganizzativa
     */
    public void setCodiceUnitaOrganizzativa(java.lang.String codiceUnitaOrganizzativa) {
        this.codiceUnitaOrganizzativa = codiceUnitaOrganizzativa;
    }


    /**
     * Gets the descrAreaOrganizzativa value for this Istituzione.
     * 
     * @return descrAreaOrganizzativa
     */
    public java.lang.String getDescrAreaOrganizzativa() {
        return descrAreaOrganizzativa;
    }


    /**
     * Sets the descrAreaOrganizzativa value for this Istituzione.
     * 
     * @param descrAreaOrganizzativa
     */
    public void setDescrAreaOrganizzativa(java.lang.String descrAreaOrganizzativa) {
        this.descrAreaOrganizzativa = descrAreaOrganizzativa;
    }


    /**
     * Gets the descrOrganizzazione value for this Istituzione.
     * 
     * @return descrOrganizzazione
     */
    public java.lang.String getDescrOrganizzazione() {
        return descrOrganizzazione;
    }


    /**
     * Sets the descrOrganizzazione value for this Istituzione.
     * 
     * @param descrOrganizzazione
     */
    public void setDescrOrganizzazione(java.lang.String descrOrganizzazione) {
        this.descrOrganizzazione = descrOrganizzazione;
    }


    /**
     * Gets the descrUnitaOrganizzativa value for this Istituzione.
     * 
     * @return descrUnitaOrganizzativa
     */
    public java.lang.String getDescrUnitaOrganizzativa() {
        return descrUnitaOrganizzativa;
    }


    /**
     * Sets the descrUnitaOrganizzativa value for this Istituzione.
     * 
     * @param descrUnitaOrganizzativa
     */
    public void setDescrUnitaOrganizzativa(java.lang.String descrUnitaOrganizzativa) {
        this.descrUnitaOrganizzativa = descrUnitaOrganizzativa;
    }


    /**
     * Gets the numeroRegistrazione value for this Istituzione.
     * 
     * @return numeroRegistrazione
     */
    public java.lang.String getNumeroRegistrazione() {
        return numeroRegistrazione;
    }


    /**
     * Sets the numeroRegistrazione value for this Istituzione.
     * 
     * @param numeroRegistrazione
     */
    public void setNumeroRegistrazione(java.lang.String numeroRegistrazione) {
        this.numeroRegistrazione = numeroRegistrazione;
    }


    /**
     * Gets the partitaIva value for this Istituzione.
     * 
     * @return partitaIva
     */
    public java.lang.String getPartitaIva() {
        return partitaIva;
    }


    /**
     * Sets the partitaIva value for this Istituzione.
     * 
     * @param partitaIva
     */
    public void setPartitaIva(java.lang.String partitaIva) {
        this.partitaIva = partitaIva;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Istituzione)) return false;
        Istituzione other = (Istituzione) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codiceAreaOrganizzativa==null && other.getCodiceAreaOrganizzativa()==null) || 
             (this.codiceAreaOrganizzativa!=null &&
              this.codiceAreaOrganizzativa.equals(other.getCodiceAreaOrganizzativa()))) &&
            ((this.codiceOrganizzazione==null && other.getCodiceOrganizzazione()==null) || 
             (this.codiceOrganizzazione!=null &&
              this.codiceOrganizzazione.equals(other.getCodiceOrganizzazione()))) &&
            ((this.codiceUnitaOrganizzativa==null && other.getCodiceUnitaOrganizzativa()==null) || 
             (this.codiceUnitaOrganizzativa!=null &&
              this.codiceUnitaOrganizzativa.equals(other.getCodiceUnitaOrganizzativa()))) &&
            ((this.descrAreaOrganizzativa==null && other.getDescrAreaOrganizzativa()==null) || 
             (this.descrAreaOrganizzativa!=null &&
              this.descrAreaOrganizzativa.equals(other.getDescrAreaOrganizzativa()))) &&
            ((this.descrOrganizzazione==null && other.getDescrOrganizzazione()==null) || 
             (this.descrOrganizzazione!=null &&
              this.descrOrganizzazione.equals(other.getDescrOrganizzazione()))) &&
            ((this.descrUnitaOrganizzativa==null && other.getDescrUnitaOrganizzativa()==null) || 
             (this.descrUnitaOrganizzativa!=null &&
              this.descrUnitaOrganizzativa.equals(other.getDescrUnitaOrganizzativa()))) &&
            ((this.numeroRegistrazione==null && other.getNumeroRegistrazione()==null) || 
             (this.numeroRegistrazione!=null &&
              this.numeroRegistrazione.equals(other.getNumeroRegistrazione()))) &&
            ((this.partitaIva==null && other.getPartitaIva()==null) || 
             (this.partitaIva!=null &&
              this.partitaIva.equals(other.getPartitaIva())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodiceAreaOrganizzativa() != null) {
            _hashCode += getCodiceAreaOrganizzativa().hashCode();
        }
        if (getCodiceOrganizzazione() != null) {
            _hashCode += getCodiceOrganizzazione().hashCode();
        }
        if (getCodiceUnitaOrganizzativa() != null) {
            _hashCode += getCodiceUnitaOrganizzativa().hashCode();
        }
        if (getDescrAreaOrganizzativa() != null) {
            _hashCode += getDescrAreaOrganizzativa().hashCode();
        }
        if (getDescrOrganizzazione() != null) {
            _hashCode += getDescrOrganizzazione().hashCode();
        }
        if (getDescrUnitaOrganizzativa() != null) {
            _hashCode += getDescrUnitaOrganizzativa().hashCode();
        }
        if (getNumeroRegistrazione() != null) {
            _hashCode += getNumeroRegistrazione().hashCode();
        }
        if (getPartitaIva() != null) {
            _hashCode += getPartitaIva().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Istituzione.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Istituzione"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceAreaOrganizzativa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceAreaOrganizzativa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceOrganizzazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceOrganizzazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceUnitaOrganizzativa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceUnitaOrganizzativa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrAreaOrganizzativa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DescrAreaOrganizzativa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrOrganizzazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DescrOrganizzazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrUnitaOrganizzativa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DescrUnitaOrganizzativa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroRegistrazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumeroRegistrazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partitaIva");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PartitaIva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
