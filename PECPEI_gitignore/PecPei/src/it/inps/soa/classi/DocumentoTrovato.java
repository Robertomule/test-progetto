package it.inps.soa.classi;
/**
 * DocumentoTrovato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class DocumentoTrovato  implements java.io.Serializable {
    private java.lang.Integer nrRiga;

    private ProtocolloEntrata protocolloEntrata;

    private ProtocolloUscita protocolloUscita;

    public DocumentoTrovato() {
    }

    public DocumentoTrovato(
           java.lang.Integer nrRiga,
           ProtocolloEntrata protocolloEntrata,
           ProtocolloUscita protocolloUscita) {
           this.nrRiga = nrRiga;
           this.protocolloEntrata = protocolloEntrata;
           this.protocolloUscita = protocolloUscita;
    }


    /**
     * Gets the nrRiga value for this DocumentoTrovato.
     * 
     * @return nrRiga
     */
    public java.lang.Integer getNrRiga() {
        return nrRiga;
    }


    /**
     * Sets the nrRiga value for this DocumentoTrovato.
     * 
     * @param nrRiga
     */
    public void setNrRiga(java.lang.Integer nrRiga) {
        this.nrRiga = nrRiga;
    }


    /**
     * Gets the protocolloEntrata value for this DocumentoTrovato.
     * 
     * @return protocolloEntrata
     */
    public ProtocolloEntrata getProtocolloEntrata() {
        return protocolloEntrata;
    }


    /**
     * Sets the protocolloEntrata value for this DocumentoTrovato.
     * 
     * @param protocolloEntrata
     */
    public void setProtocolloEntrata(ProtocolloEntrata protocolloEntrata) {
        this.protocolloEntrata = protocolloEntrata;
    }


    /**
     * Gets the protocolloUscita value for this DocumentoTrovato.
     * 
     * @return protocolloUscita
     */
    public ProtocolloUscita getProtocolloUscita() {
        return protocolloUscita;
    }


    /**
     * Sets the protocolloUscita value for this DocumentoTrovato.
     * 
     * @param protocolloUscita
     */
    public void setProtocolloUscita(ProtocolloUscita protocolloUscita) {
        this.protocolloUscita = protocolloUscita;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentoTrovato)) return false;
        DocumentoTrovato other = (DocumentoTrovato) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nrRiga==null && other.getNrRiga()==null) || 
             (this.nrRiga!=null &&
              this.nrRiga.equals(other.getNrRiga()))) &&
            ((this.protocolloEntrata==null && other.getProtocolloEntrata()==null) || 
             (this.protocolloEntrata!=null &&
              this.protocolloEntrata.equals(other.getProtocolloEntrata()))) &&
            ((this.protocolloUscita==null && other.getProtocolloUscita()==null) || 
             (this.protocolloUscita!=null &&
              this.protocolloUscita.equals(other.getProtocolloUscita())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNrRiga() != null) {
            _hashCode += getNrRiga().hashCode();
        }
        if (getProtocolloEntrata() != null) {
            _hashCode += getProtocolloEntrata().hashCode();
        }
        if (getProtocolloUscita() != null) {
            _hashCode += getProtocolloUscita().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentoTrovato.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "DocumentoTrovato"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nrRiga");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NrRiga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolloEntrata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProtocolloEntrata"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "ProtocolloEntrata"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolloUscita");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProtocolloUscita"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "ProtocolloUscita"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
