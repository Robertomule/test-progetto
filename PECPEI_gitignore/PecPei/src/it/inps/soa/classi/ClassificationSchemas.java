package it.inps.soa.classi;
/**
 * ClassificationSchemas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class ClassificationSchemas  implements java.io.Serializable {
    private java.lang.String statoCorrente;

    private java.lang.String title;

    private java.util.Calendar validAl;

    private java.util.Calendar validoDal;

    public ClassificationSchemas() {
    }

    public ClassificationSchemas(
           java.lang.String statoCorrente,
           java.lang.String title,
           java.util.Calendar validAl,
           java.util.Calendar validoDal) {
           this.statoCorrente = statoCorrente;
           this.title = title;
           this.validAl = validAl;
           this.validoDal = validoDal;
    }


    /**
     * Gets the statoCorrente value for this ClassificationSchemas.
     * 
     * @return statoCorrente
     */
    public java.lang.String getStatoCorrente() {
        return statoCorrente;
    }


    /**
     * Sets the statoCorrente value for this ClassificationSchemas.
     * 
     * @param statoCorrente
     */
    public void setStatoCorrente(java.lang.String statoCorrente) {
        this.statoCorrente = statoCorrente;
    }


    /**
     * Gets the title value for this ClassificationSchemas.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this ClassificationSchemas.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the validAl value for this ClassificationSchemas.
     * 
     * @return validAl
     */
    public java.util.Calendar getValidAl() {
        return validAl;
    }


    /**
     * Sets the validAl value for this ClassificationSchemas.
     * 
     * @param validAl
     */
    public void setValidAl(java.util.Calendar validAl) {
        this.validAl = validAl;
    }


    /**
     * Gets the validoDal value for this ClassificationSchemas.
     * 
     * @return validoDal
     */
    public java.util.Calendar getValidoDal() {
        return validoDal;
    }


    /**
     * Sets the validoDal value for this ClassificationSchemas.
     * 
     * @param validoDal
     */
    public void setValidoDal(java.util.Calendar validoDal) {
        this.validoDal = validoDal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClassificationSchemas)) return false;
        ClassificationSchemas other = (ClassificationSchemas) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statoCorrente==null && other.getStatoCorrente()==null) || 
             (this.statoCorrente!=null &&
              this.statoCorrente.equals(other.getStatoCorrente()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.validAl==null && other.getValidAl()==null) || 
             (this.validAl!=null &&
              this.validAl.equals(other.getValidAl()))) &&
            ((this.validoDal==null && other.getValidoDal()==null) || 
             (this.validoDal!=null &&
              this.validoDal.equals(other.getValidoDal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatoCorrente() != null) {
            _hashCode += getStatoCorrente().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getValidAl() != null) {
            _hashCode += getValidAl().hashCode();
        }
        if (getValidoDal() != null) {
            _hashCode += getValidoDal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClassificationSchemas.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "ClassificationSchemas"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statoCorrente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StatoCorrente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validAl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ValidAl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validoDal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ValidoDal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
