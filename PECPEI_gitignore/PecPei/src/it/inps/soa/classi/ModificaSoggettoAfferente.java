package it.inps.soa.classi;
/**
 * ModificaSoggettoAfferente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class ModificaSoggettoAfferente  extends SoggettoAfferente  implements java.io.Serializable {
    private org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoOperazione tipoOperazione;

    public ModificaSoggettoAfferente() {
    }

    public ModificaSoggettoAfferente(
           java.lang.String nominativo,
           java.lang.String codice,
           java.lang.String codiceInterno,
           java.lang.String email,
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoSoggetto tipoSoggetto,
           java.lang.String categoria,
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoOperazione tipoOperazione) {
        super(
            nominativo,
            codice,
            codiceInterno,
            email,
            tipoSoggetto,
            categoria);
        this.tipoOperazione = tipoOperazione;
    }


    /**
     * Gets the tipoOperazione value for this ModificaSoggettoAfferente.
     * 
     * @return tipoOperazione
     */
    public org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoOperazione getTipoOperazione() {
        return tipoOperazione;
    }


    /**
     * Sets the tipoOperazione value for this ModificaSoggettoAfferente.
     * 
     * @param tipoOperazione
     */
    public void setTipoOperazione(org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoOperazione tipoOperazione) {
        this.tipoOperazione = tipoOperazione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModificaSoggettoAfferente)) return false;
        ModificaSoggettoAfferente other = (ModificaSoggettoAfferente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.tipoOperazione==null && other.getTipoOperazione()==null) || 
             (this.tipoOperazione!=null &&
              this.tipoOperazione.equals(other.getTipoOperazione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTipoOperazione() != null) {
            _hashCode += getTipoOperazione().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModificaSoggettoAfferente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "ModificaSoggettoAfferente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoOperazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TipoOperazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoOperazione"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
