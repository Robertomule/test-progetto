package it.inps.soa.classi;
/**
 * AddressBook.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class AddressBook  implements java.io.Serializable {
    private java.lang.String CAP;

    private java.lang.String citta;

    private java.lang.String country;

    private java.util.Calendar dataCreazione;

    private java.lang.Boolean disabilitato;

    private java.lang.String EMailCertificata;

    private java.lang.String email;

    private java.lang.String fax;

    private java.lang.Integer id;

    private java.lang.Integer idParent;

    private java.lang.String indirizzo;

    private java.lang.String nazione;

    private java.lang.String note;

    private java.lang.Integer notificaPreferita;

    private java.lang.Integer numeroTelPreferito;

    private org.apache.axis.types.Duration orarioPreferito;

    private java.lang.String provincia;

    private java.lang.String regione;

    private java.lang.String telefono;

    private java.lang.String telefonoCasa;

    private java.lang.String telefonoUfficio;

    private java.lang.String tipoSoggetto;

    public AddressBook() {
    }

    public AddressBook(
           java.lang.String CAP,
           java.lang.String citta,
           java.lang.String country,
           java.util.Calendar dataCreazione,
           java.lang.Boolean disabilitato,
           java.lang.String EMailCertificata,
           java.lang.String email,
           java.lang.String fax,
           java.lang.Integer id,
           java.lang.Integer idParent,
           java.lang.String indirizzo,
           java.lang.String nazione,
           java.lang.String note,
           java.lang.Integer notificaPreferita,
           java.lang.Integer numeroTelPreferito,
           org.apache.axis.types.Duration orarioPreferito,
           java.lang.String provincia,
           java.lang.String regione,
           java.lang.String telefono,
           java.lang.String telefonoCasa,
           java.lang.String telefonoUfficio,
           java.lang.String tipoSoggetto) {
           this.CAP = CAP;
           this.citta = citta;
           this.country = country;
           this.dataCreazione = dataCreazione;
           this.disabilitato = disabilitato;
           this.EMailCertificata = EMailCertificata;
           this.email = email;
           this.fax = fax;
           this.id = id;
           this.idParent = idParent;
           this.indirizzo = indirizzo;
           this.nazione = nazione;
           this.note = note;
           this.notificaPreferita = notificaPreferita;
           this.numeroTelPreferito = numeroTelPreferito;
           this.orarioPreferito = orarioPreferito;
           this.provincia = provincia;
           this.regione = regione;
           this.telefono = telefono;
           this.telefonoCasa = telefonoCasa;
           this.telefonoUfficio = telefonoUfficio;
           this.tipoSoggetto = tipoSoggetto;
    }


    /**
     * Gets the CAP value for this AddressBook.
     * 
     * @return CAP
     */
    public java.lang.String getCAP() {
        return CAP;
    }


    /**
     * Sets the CAP value for this AddressBook.
     * 
     * @param CAP
     */
    public void setCAP(java.lang.String CAP) {
        this.CAP = CAP;
    }


    /**
     * Gets the citta value for this AddressBook.
     * 
     * @return citta
     */
    public java.lang.String getCitta() {
        return citta;
    }


    /**
     * Sets the citta value for this AddressBook.
     * 
     * @param citta
     */
    public void setCitta(java.lang.String citta) {
        this.citta = citta;
    }


    /**
     * Gets the country value for this AddressBook.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this AddressBook.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the dataCreazione value for this AddressBook.
     * 
     * @return dataCreazione
     */
    public java.util.Calendar getDataCreazione() {
        return dataCreazione;
    }


    /**
     * Sets the dataCreazione value for this AddressBook.
     * 
     * @param dataCreazione
     */
    public void setDataCreazione(java.util.Calendar dataCreazione) {
        this.dataCreazione = dataCreazione;
    }


    /**
     * Gets the disabilitato value for this AddressBook.
     * 
     * @return disabilitato
     */
    public java.lang.Boolean getDisabilitato() {
        return disabilitato;
    }


    /**
     * Sets the disabilitato value for this AddressBook.
     * 
     * @param disabilitato
     */
    public void setDisabilitato(java.lang.Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }


    /**
     * Gets the EMailCertificata value for this AddressBook.
     * 
     * @return EMailCertificata
     */
    public java.lang.String getEMailCertificata() {
        return EMailCertificata;
    }


    /**
     * Sets the EMailCertificata value for this AddressBook.
     * 
     * @param EMailCertificata
     */
    public void setEMailCertificata(java.lang.String EMailCertificata) {
        this.EMailCertificata = EMailCertificata;
    }


    /**
     * Gets the email value for this AddressBook.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this AddressBook.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the fax value for this AddressBook.
     * 
     * @return fax
     */
    public java.lang.String getFax() {
        return fax;
    }


    /**
     * Sets the fax value for this AddressBook.
     * 
     * @param fax
     */
    public void setFax(java.lang.String fax) {
        this.fax = fax;
    }


    /**
     * Gets the id value for this AddressBook.
     * 
     * @return id
     */
    public java.lang.Integer getId() {
        return id;
    }


    /**
     * Sets the id value for this AddressBook.
     * 
     * @param id
     */
    public void setId(java.lang.Integer id) {
        this.id = id;
    }


    /**
     * Gets the idParent value for this AddressBook.
     * 
     * @return idParent
     */
    public java.lang.Integer getIdParent() {
        return idParent;
    }


    /**
     * Sets the idParent value for this AddressBook.
     * 
     * @param idParent
     */
    public void setIdParent(java.lang.Integer idParent) {
        this.idParent = idParent;
    }


    /**
     * Gets the indirizzo value for this AddressBook.
     * 
     * @return indirizzo
     */
    public java.lang.String getIndirizzo() {
        return indirizzo;
    }


    /**
     * Sets the indirizzo value for this AddressBook.
     * 
     * @param indirizzo
     */
    public void setIndirizzo(java.lang.String indirizzo) {
        this.indirizzo = indirizzo;
    }


    /**
     * Gets the nazione value for this AddressBook.
     * 
     * @return nazione
     */
    public java.lang.String getNazione() {
        return nazione;
    }


    /**
     * Sets the nazione value for this AddressBook.
     * 
     * @param nazione
     */
    public void setNazione(java.lang.String nazione) {
        this.nazione = nazione;
    }


    /**
     * Gets the note value for this AddressBook.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this AddressBook.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }


    /**
     * Gets the notificaPreferita value for this AddressBook.
     * 
     * @return notificaPreferita
     */
    public java.lang.Integer getNotificaPreferita() {
        return notificaPreferita;
    }


    /**
     * Sets the notificaPreferita value for this AddressBook.
     * 
     * @param notificaPreferita
     */
    public void setNotificaPreferita(java.lang.Integer notificaPreferita) {
        this.notificaPreferita = notificaPreferita;
    }


    /**
     * Gets the numeroTelPreferito value for this AddressBook.
     * 
     * @return numeroTelPreferito
     */
    public java.lang.Integer getNumeroTelPreferito() {
        return numeroTelPreferito;
    }


    /**
     * Sets the numeroTelPreferito value for this AddressBook.
     * 
     * @param numeroTelPreferito
     */
    public void setNumeroTelPreferito(java.lang.Integer numeroTelPreferito) {
        this.numeroTelPreferito = numeroTelPreferito;
    }


    /**
     * Gets the orarioPreferito value for this AddressBook.
     * 
     * @return orarioPreferito
     */
    public org.apache.axis.types.Duration getOrarioPreferito() {
        return orarioPreferito;
    }


    /**
     * Sets the orarioPreferito value for this AddressBook.
     * 
     * @param orarioPreferito
     */
    public void setOrarioPreferito(org.apache.axis.types.Duration orarioPreferito) {
        this.orarioPreferito = orarioPreferito;
    }


    /**
     * Gets the provincia value for this AddressBook.
     * 
     * @return provincia
     */
    public java.lang.String getProvincia() {
        return provincia;
    }


    /**
     * Sets the provincia value for this AddressBook.
     * 
     * @param provincia
     */
    public void setProvincia(java.lang.String provincia) {
        this.provincia = provincia;
    }


    /**
     * Gets the regione value for this AddressBook.
     * 
     * @return regione
     */
    public java.lang.String getRegione() {
        return regione;
    }


    /**
     * Sets the regione value for this AddressBook.
     * 
     * @param regione
     */
    public void setRegione(java.lang.String regione) {
        this.regione = regione;
    }


    /**
     * Gets the telefono value for this AddressBook.
     * 
     * @return telefono
     */
    public java.lang.String getTelefono() {
        return telefono;
    }


    /**
     * Sets the telefono value for this AddressBook.
     * 
     * @param telefono
     */
    public void setTelefono(java.lang.String telefono) {
        this.telefono = telefono;
    }


    /**
     * Gets the telefonoCasa value for this AddressBook.
     * 
     * @return telefonoCasa
     */
    public java.lang.String getTelefonoCasa() {
        return telefonoCasa;
    }


    /**
     * Sets the telefonoCasa value for this AddressBook.
     * 
     * @param telefonoCasa
     */
    public void setTelefonoCasa(java.lang.String telefonoCasa) {
        this.telefonoCasa = telefonoCasa;
    }


    /**
     * Gets the telefonoUfficio value for this AddressBook.
     * 
     * @return telefonoUfficio
     */
    public java.lang.String getTelefonoUfficio() {
        return telefonoUfficio;
    }


    /**
     * Sets the telefonoUfficio value for this AddressBook.
     * 
     * @param telefonoUfficio
     */
    public void setTelefonoUfficio(java.lang.String telefonoUfficio) {
        this.telefonoUfficio = telefonoUfficio;
    }


    /**
     * Gets the tipoSoggetto value for this AddressBook.
     * 
     * @return tipoSoggetto
     */
    public java.lang.String getTipoSoggetto() {
        return tipoSoggetto;
    }


    /**
     * Sets the tipoSoggetto value for this AddressBook.
     * 
     * @param tipoSoggetto
     */
    public void setTipoSoggetto(java.lang.String tipoSoggetto) {
        this.tipoSoggetto = tipoSoggetto;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddressBook)) return false;
        AddressBook other = (AddressBook) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CAP==null && other.getCAP()==null) || 
             (this.CAP!=null &&
              this.CAP.equals(other.getCAP()))) &&
            ((this.citta==null && other.getCitta()==null) || 
             (this.citta!=null &&
              this.citta.equals(other.getCitta()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.dataCreazione==null && other.getDataCreazione()==null) || 
             (this.dataCreazione!=null &&
              this.dataCreazione.equals(other.getDataCreazione()))) &&
            ((this.disabilitato==null && other.getDisabilitato()==null) || 
             (this.disabilitato!=null &&
              this.disabilitato.equals(other.getDisabilitato()))) &&
            ((this.EMailCertificata==null && other.getEMailCertificata()==null) || 
             (this.EMailCertificata!=null &&
              this.EMailCertificata.equals(other.getEMailCertificata()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.fax==null && other.getFax()==null) || 
             (this.fax!=null &&
              this.fax.equals(other.getFax()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.idParent==null && other.getIdParent()==null) || 
             (this.idParent!=null &&
              this.idParent.equals(other.getIdParent()))) &&
            ((this.indirizzo==null && other.getIndirizzo()==null) || 
             (this.indirizzo!=null &&
              this.indirizzo.equals(other.getIndirizzo()))) &&
            ((this.nazione==null && other.getNazione()==null) || 
             (this.nazione!=null &&
              this.nazione.equals(other.getNazione()))) &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote()))) &&
            ((this.notificaPreferita==null && other.getNotificaPreferita()==null) || 
             (this.notificaPreferita!=null &&
              this.notificaPreferita.equals(other.getNotificaPreferita()))) &&
            ((this.numeroTelPreferito==null && other.getNumeroTelPreferito()==null) || 
             (this.numeroTelPreferito!=null &&
              this.numeroTelPreferito.equals(other.getNumeroTelPreferito()))) &&
            ((this.orarioPreferito==null && other.getOrarioPreferito()==null) || 
             (this.orarioPreferito!=null &&
              this.orarioPreferito.equals(other.getOrarioPreferito()))) &&
            ((this.provincia==null && other.getProvincia()==null) || 
             (this.provincia!=null &&
              this.provincia.equals(other.getProvincia()))) &&
            ((this.regione==null && other.getRegione()==null) || 
             (this.regione!=null &&
              this.regione.equals(other.getRegione()))) &&
            ((this.telefono==null && other.getTelefono()==null) || 
             (this.telefono!=null &&
              this.telefono.equals(other.getTelefono()))) &&
            ((this.telefonoCasa==null && other.getTelefonoCasa()==null) || 
             (this.telefonoCasa!=null &&
              this.telefonoCasa.equals(other.getTelefonoCasa()))) &&
            ((this.telefonoUfficio==null && other.getTelefonoUfficio()==null) || 
             (this.telefonoUfficio!=null &&
              this.telefonoUfficio.equals(other.getTelefonoUfficio()))) &&
            ((this.tipoSoggetto==null && other.getTipoSoggetto()==null) || 
             (this.tipoSoggetto!=null &&
              this.tipoSoggetto.equals(other.getTipoSoggetto())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCAP() != null) {
            _hashCode += getCAP().hashCode();
        }
        if (getCitta() != null) {
            _hashCode += getCitta().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getDataCreazione() != null) {
            _hashCode += getDataCreazione().hashCode();
        }
        if (getDisabilitato() != null) {
            _hashCode += getDisabilitato().hashCode();
        }
        if (getEMailCertificata() != null) {
            _hashCode += getEMailCertificata().hashCode();
        }
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        if (getFax() != null) {
            _hashCode += getFax().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getIdParent() != null) {
            _hashCode += getIdParent().hashCode();
        }
        if (getIndirizzo() != null) {
            _hashCode += getIndirizzo().hashCode();
        }
        if (getNazione() != null) {
            _hashCode += getNazione().hashCode();
        }
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        if (getNotificaPreferita() != null) {
            _hashCode += getNotificaPreferita().hashCode();
        }
        if (getNumeroTelPreferito() != null) {
            _hashCode += getNumeroTelPreferito().hashCode();
        }
        if (getOrarioPreferito() != null) {
            _hashCode += getOrarioPreferito().hashCode();
        }
        if (getProvincia() != null) {
            _hashCode += getProvincia().hashCode();
        }
        if (getRegione() != null) {
            _hashCode += getRegione().hashCode();
        }
        if (getTelefono() != null) {
            _hashCode += getTelefono().hashCode();
        }
        if (getTelefonoCasa() != null) {
            _hashCode += getTelefonoCasa().hashCode();
        }
        if (getTelefonoUfficio() != null) {
            _hashCode += getTelefonoUfficio().hashCode();
        }
        if (getTipoSoggetto() != null) {
            _hashCode += getTipoSoggetto().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddressBook.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "AddressBook"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CAP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CAP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("citta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Citta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataCreazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataCreazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Disabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMailCertificata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EMailCertificata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Fax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idParent");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IdParent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indirizzo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Indirizzo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Nazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notificaPreferita");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NotificaPreferita"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroTelPreferito");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumeroTelPreferito"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orarioPreferito");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OrarioPreferito"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "duration"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("provincia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Provincia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Regione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefono");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Telefono"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefonoCasa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TelefonoCasa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telefonoUfficio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TelefonoUfficio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoSoggetto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TipoSoggetto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
