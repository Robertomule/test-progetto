package it.inps.soa.classi;
/**
 * RubricaResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class RubricaResponse  extends Response  implements java.io.Serializable {
    private AltroSoggetto[] altriSoggetti;

    private Azienda[] aziende;

    private Istituzione[] istituzioni;

    private Patronato[] patronati;

    private PersonaFisica[] personeFisiche;

    public RubricaResponse() {
    }

    public RubricaResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           AltroSoggetto[] altriSoggetti,
           Azienda[] aziende,
           Istituzione[] istituzioni,
           Patronato[] patronati,
           PersonaFisica[] personeFisiche) {
        super(
            codice,
            descrizione);
        this.altriSoggetti = altriSoggetti;
        this.aziende = aziende;
        this.istituzioni = istituzioni;
        this.patronati = patronati;
        this.personeFisiche = personeFisiche;
    }


    /**
     * Gets the altriSoggetti value for this RubricaResponse.
     * 
     * @return altriSoggetti
     */
    public AltroSoggetto[] getAltriSoggetti() {
        return altriSoggetti;
    }


    /**
     * Sets the altriSoggetti value for this RubricaResponse.
     * 
     * @param altriSoggetti
     */
    public void setAltriSoggetti(AltroSoggetto[] altriSoggetti) {
        this.altriSoggetti = altriSoggetti;
    }


    /**
     * Gets the aziende value for this RubricaResponse.
     * 
     * @return aziende
     */
    public Azienda[] getAziende() {
        return aziende;
    }


    /**
     * Sets the aziende value for this RubricaResponse.
     * 
     * @param aziende
     */
    public void setAziende(Azienda[] aziende) {
        this.aziende = aziende;
    }


    /**
     * Gets the istituzioni value for this RubricaResponse.
     * 
     * @return istituzioni
     */
    public Istituzione[] getIstituzioni() {
        return istituzioni;
    }


    /**
     * Sets the istituzioni value for this RubricaResponse.
     * 
     * @param istituzioni
     */
    public void setIstituzioni(Istituzione[] istituzioni) {
        this.istituzioni = istituzioni;
    }


    /**
     * Gets the patronati value for this RubricaResponse.
     * 
     * @return patronati
     */
    public Patronato[] getPatronati() {
        return patronati;
    }


    /**
     * Sets the patronati value for this RubricaResponse.
     * 
     * @param patronati
     */
    public void setPatronati(Patronato[] patronati) {
        this.patronati = patronati;
    }


    /**
     * Gets the personeFisiche value for this RubricaResponse.
     * 
     * @return personeFisiche
     */
    public PersonaFisica[] getPersoneFisiche() {
        return personeFisiche;
    }


    /**
     * Sets the personeFisiche value for this RubricaResponse.
     * 
     * @param personeFisiche
     */
    public void setPersoneFisiche(PersonaFisica[] personeFisiche) {
        this.personeFisiche = personeFisiche;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RubricaResponse)) return false;
        RubricaResponse other = (RubricaResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.altriSoggetti==null && other.getAltriSoggetti()==null) || 
             (this.altriSoggetti!=null &&
              java.util.Arrays.equals(this.altriSoggetti, other.getAltriSoggetti()))) &&
            ((this.aziende==null && other.getAziende()==null) || 
             (this.aziende!=null &&
              java.util.Arrays.equals(this.aziende, other.getAziende()))) &&
            ((this.istituzioni==null && other.getIstituzioni()==null) || 
             (this.istituzioni!=null &&
              java.util.Arrays.equals(this.istituzioni, other.getIstituzioni()))) &&
            ((this.patronati==null && other.getPatronati()==null) || 
             (this.patronati!=null &&
              java.util.Arrays.equals(this.patronati, other.getPatronati()))) &&
            ((this.personeFisiche==null && other.getPersoneFisiche()==null) || 
             (this.personeFisiche!=null &&
              java.util.Arrays.equals(this.personeFisiche, other.getPersoneFisiche())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAltriSoggetti() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAltriSoggetti());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAltriSoggetti(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAziende() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAziende());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAziende(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIstituzioni() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIstituzioni());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIstituzioni(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPatronati() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPatronati());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPatronati(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPersoneFisiche() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPersoneFisiche());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPersoneFisiche(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RubricaResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "RubricaResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("altriSoggetti");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AltriSoggetti"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "AltroSoggetto"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "AltroSoggetto"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aziende");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Aziende"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Azienda"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Azienda"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("istituzioni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Istituzioni"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Istituzione"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Istituzione"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patronati");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Patronati"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Patronato"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Patronato"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personeFisiche");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PersoneFisiche"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "PersonaFisica"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "PersonaFisica"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
