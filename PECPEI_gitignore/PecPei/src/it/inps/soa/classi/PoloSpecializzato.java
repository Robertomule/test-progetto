package it.inps.soa.classi;
/**
 * PoloSpecializzato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class PoloSpecializzato  extends OrganizationalAreaPlus  implements java.io.Serializable {
    private java.lang.String acronimoEntePrevidenzialeEstero;

    private java.lang.String codiceCatastaleStatoEstero;

    private java.util.Calendar dataDisabilitato;

    private java.lang.Boolean disabilitato;

    private java.lang.String entePrevidenzialeEstero;

    private java.lang.Integer id;

    private java.lang.String regioneStatoEstero;

    private java.lang.String statoEstero;

    private java.lang.Integer tipoConvenzione;

    private java.lang.Boolean tutteLeAltre;

    public PoloSpecializzato() {
    }

    public PoloSpecializzato(
           java.lang.String codice,
           java.lang.String descrizione,
           java.lang.String indirizzo,
           java.lang.String regione,
           java.lang.String telefonoFax,
           java.lang.String acronimoEntePrevidenzialeEstero,
           java.lang.String codiceCatastaleStatoEstero,
           java.util.Calendar dataDisabilitato,
           java.lang.Boolean disabilitato,
           java.lang.String entePrevidenzialeEstero,
           java.lang.Integer id,
           java.lang.String regioneStatoEstero,
           java.lang.String statoEstero,
           java.lang.Integer tipoConvenzione,
           java.lang.Boolean tutteLeAltre) {
        super(
            codice,
            descrizione,
            indirizzo,
            regione,
            telefonoFax);
        this.acronimoEntePrevidenzialeEstero = acronimoEntePrevidenzialeEstero;
        this.codiceCatastaleStatoEstero = codiceCatastaleStatoEstero;
        this.dataDisabilitato = dataDisabilitato;
        this.disabilitato = disabilitato;
        this.entePrevidenzialeEstero = entePrevidenzialeEstero;
        this.id = id;
        this.regioneStatoEstero = regioneStatoEstero;
        this.statoEstero = statoEstero;
        this.tipoConvenzione = tipoConvenzione;
        this.tutteLeAltre = tutteLeAltre;
    }


    /**
     * Gets the acronimoEntePrevidenzialeEstero value for this PoloSpecializzato.
     * 
     * @return acronimoEntePrevidenzialeEstero
     */
    public java.lang.String getAcronimoEntePrevidenzialeEstero() {
        return acronimoEntePrevidenzialeEstero;
    }


    /**
     * Sets the acronimoEntePrevidenzialeEstero value for this PoloSpecializzato.
     * 
     * @param acronimoEntePrevidenzialeEstero
     */
    public void setAcronimoEntePrevidenzialeEstero(java.lang.String acronimoEntePrevidenzialeEstero) {
        this.acronimoEntePrevidenzialeEstero = acronimoEntePrevidenzialeEstero;
    }


    /**
     * Gets the codiceCatastaleStatoEstero value for this PoloSpecializzato.
     * 
     * @return codiceCatastaleStatoEstero
     */
    public java.lang.String getCodiceCatastaleStatoEstero() {
        return codiceCatastaleStatoEstero;
    }


    /**
     * Sets the codiceCatastaleStatoEstero value for this PoloSpecializzato.
     * 
     * @param codiceCatastaleStatoEstero
     */
    public void setCodiceCatastaleStatoEstero(java.lang.String codiceCatastaleStatoEstero) {
        this.codiceCatastaleStatoEstero = codiceCatastaleStatoEstero;
    }


    /**
     * Gets the dataDisabilitato value for this PoloSpecializzato.
     * 
     * @return dataDisabilitato
     */
    public java.util.Calendar getDataDisabilitato() {
        return dataDisabilitato;
    }


    /**
     * Sets the dataDisabilitato value for this PoloSpecializzato.
     * 
     * @param dataDisabilitato
     */
    public void setDataDisabilitato(java.util.Calendar dataDisabilitato) {
        this.dataDisabilitato = dataDisabilitato;
    }


    /**
     * Gets the disabilitato value for this PoloSpecializzato.
     * 
     * @return disabilitato
     */
    public java.lang.Boolean getDisabilitato() {
        return disabilitato;
    }


    /**
     * Sets the disabilitato value for this PoloSpecializzato.
     * 
     * @param disabilitato
     */
    public void setDisabilitato(java.lang.Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }


    /**
     * Gets the entePrevidenzialeEstero value for this PoloSpecializzato.
     * 
     * @return entePrevidenzialeEstero
     */
    public java.lang.String getEntePrevidenzialeEstero() {
        return entePrevidenzialeEstero;
    }


    /**
     * Sets the entePrevidenzialeEstero value for this PoloSpecializzato.
     * 
     * @param entePrevidenzialeEstero
     */
    public void setEntePrevidenzialeEstero(java.lang.String entePrevidenzialeEstero) {
        this.entePrevidenzialeEstero = entePrevidenzialeEstero;
    }


    /**
     * Gets the id value for this PoloSpecializzato.
     * 
     * @return id
     */
    public java.lang.Integer getId() {
        return id;
    }


    /**
     * Sets the id value for this PoloSpecializzato.
     * 
     * @param id
     */
    public void setId(java.lang.Integer id) {
        this.id = id;
    }


    /**
     * Gets the regioneStatoEstero value for this PoloSpecializzato.
     * 
     * @return regioneStatoEstero
     */
    public java.lang.String getRegioneStatoEstero() {
        return regioneStatoEstero;
    }


    /**
     * Sets the regioneStatoEstero value for this PoloSpecializzato.
     * 
     * @param regioneStatoEstero
     */
    public void setRegioneStatoEstero(java.lang.String regioneStatoEstero) {
        this.regioneStatoEstero = regioneStatoEstero;
    }


    /**
     * Gets the statoEstero value for this PoloSpecializzato.
     * 
     * @return statoEstero
     */
    public java.lang.String getStatoEstero() {
        return statoEstero;
    }


    /**
     * Sets the statoEstero value for this PoloSpecializzato.
     * 
     * @param statoEstero
     */
    public void setStatoEstero(java.lang.String statoEstero) {
        this.statoEstero = statoEstero;
    }


    /**
     * Gets the tipoConvenzione value for this PoloSpecializzato.
     * 
     * @return tipoConvenzione
     */
    public java.lang.Integer getTipoConvenzione() {
        return tipoConvenzione;
    }


    /**
     * Sets the tipoConvenzione value for this PoloSpecializzato.
     * 
     * @param tipoConvenzione
     */
    public void setTipoConvenzione(java.lang.Integer tipoConvenzione) {
        this.tipoConvenzione = tipoConvenzione;
    }


    /**
     * Gets the tutteLeAltre value for this PoloSpecializzato.
     * 
     * @return tutteLeAltre
     */
    public java.lang.Boolean getTutteLeAltre() {
        return tutteLeAltre;
    }


    /**
     * Sets the tutteLeAltre value for this PoloSpecializzato.
     * 
     * @param tutteLeAltre
     */
    public void setTutteLeAltre(java.lang.Boolean tutteLeAltre) {
        this.tutteLeAltre = tutteLeAltre;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PoloSpecializzato)) return false;
        PoloSpecializzato other = (PoloSpecializzato) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.acronimoEntePrevidenzialeEstero==null && other.getAcronimoEntePrevidenzialeEstero()==null) || 
             (this.acronimoEntePrevidenzialeEstero!=null &&
              this.acronimoEntePrevidenzialeEstero.equals(other.getAcronimoEntePrevidenzialeEstero()))) &&
            ((this.codiceCatastaleStatoEstero==null && other.getCodiceCatastaleStatoEstero()==null) || 
             (this.codiceCatastaleStatoEstero!=null &&
              this.codiceCatastaleStatoEstero.equals(other.getCodiceCatastaleStatoEstero()))) &&
            ((this.dataDisabilitato==null && other.getDataDisabilitato()==null) || 
             (this.dataDisabilitato!=null &&
              this.dataDisabilitato.equals(other.getDataDisabilitato()))) &&
            ((this.disabilitato==null && other.getDisabilitato()==null) || 
             (this.disabilitato!=null &&
              this.disabilitato.equals(other.getDisabilitato()))) &&
            ((this.entePrevidenzialeEstero==null && other.getEntePrevidenzialeEstero()==null) || 
             (this.entePrevidenzialeEstero!=null &&
              this.entePrevidenzialeEstero.equals(other.getEntePrevidenzialeEstero()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.regioneStatoEstero==null && other.getRegioneStatoEstero()==null) || 
             (this.regioneStatoEstero!=null &&
              this.regioneStatoEstero.equals(other.getRegioneStatoEstero()))) &&
            ((this.statoEstero==null && other.getStatoEstero()==null) || 
             (this.statoEstero!=null &&
              this.statoEstero.equals(other.getStatoEstero()))) &&
            ((this.tipoConvenzione==null && other.getTipoConvenzione()==null) || 
             (this.tipoConvenzione!=null &&
              this.tipoConvenzione.equals(other.getTipoConvenzione()))) &&
            ((this.tutteLeAltre==null && other.getTutteLeAltre()==null) || 
             (this.tutteLeAltre!=null &&
              this.tutteLeAltre.equals(other.getTutteLeAltre())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAcronimoEntePrevidenzialeEstero() != null) {
            _hashCode += getAcronimoEntePrevidenzialeEstero().hashCode();
        }
        if (getCodiceCatastaleStatoEstero() != null) {
            _hashCode += getCodiceCatastaleStatoEstero().hashCode();
        }
        if (getDataDisabilitato() != null) {
            _hashCode += getDataDisabilitato().hashCode();
        }
        if (getDisabilitato() != null) {
            _hashCode += getDisabilitato().hashCode();
        }
        if (getEntePrevidenzialeEstero() != null) {
            _hashCode += getEntePrevidenzialeEstero().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getRegioneStatoEstero() != null) {
            _hashCode += getRegioneStatoEstero().hashCode();
        }
        if (getStatoEstero() != null) {
            _hashCode += getStatoEstero().hashCode();
        }
        if (getTipoConvenzione() != null) {
            _hashCode += getTipoConvenzione().hashCode();
        }
        if (getTutteLeAltre() != null) {
            _hashCode += getTutteLeAltre().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PoloSpecializzato.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "PoloSpecializzato"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acronimoEntePrevidenzialeEstero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AcronimoEntePrevidenzialeEstero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceCatastaleStatoEstero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceCatastaleStatoEstero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataDisabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataDisabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Disabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entePrevidenzialeEstero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EntePrevidenzialeEstero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regioneStatoEstero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RegioneStatoEstero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statoEstero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StatoEstero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoConvenzione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TipoConvenzione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tutteLeAltre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TutteLeAltre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
