package it.inps.soa.classi;
/**
 * OperazioniResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class OperazioniResponse  extends Response  implements java.io.Serializable {
    private Operazione[] tipiOperazioni;

    public OperazioniResponse() {
    }

    public OperazioniResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           Operazione[] tipiOperazioni) {
        super(
            codice,
            descrizione);
        this.tipiOperazioni = tipiOperazioni;
    }


    /**
     * Gets the tipiOperazioni value for this OperazioniResponse.
     * 
     * @return tipiOperazioni
     */
    public Operazione[] getTipiOperazioni() {
        return tipiOperazioni;
    }


    /**
     * Sets the tipiOperazioni value for this OperazioniResponse.
     * 
     * @param tipiOperazioni
     */
    public void setTipiOperazioni(Operazione[] tipiOperazioni) {
        this.tipiOperazioni = tipiOperazioni;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OperazioniResponse)) return false;
        OperazioniResponse other = (OperazioniResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.tipiOperazioni==null && other.getTipiOperazioni()==null) || 
             (this.tipiOperazioni!=null &&
              java.util.Arrays.equals(this.tipiOperazioni, other.getTipiOperazioni())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTipiOperazioni() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipiOperazioni());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipiOperazioni(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OperazioniResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "OperazioniResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipiOperazioni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TipiOperazioni"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Operazione"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Operazione"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
