package it.inps.soa.classi;
/**
 * NotificaInteropEntrataResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class NotificaInteropEntrataResponse  extends Response  implements java.io.Serializable {
    private java.lang.String confermaRicezione;

    public NotificaInteropEntrataResponse() {
    }

    public NotificaInteropEntrataResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           java.lang.String confermaRicezione) {
        super(
            codice,
            descrizione);
        this.confermaRicezione = confermaRicezione;
    }


    /**
     * Gets the confermaRicezione value for this NotificaInteropEntrataResponse.
     * 
     * @return confermaRicezione
     */
    public java.lang.String getConfermaRicezione() {
        return confermaRicezione;
    }


    /**
     * Sets the confermaRicezione value for this NotificaInteropEntrataResponse.
     * 
     * @param confermaRicezione
     */
    public void setConfermaRicezione(java.lang.String confermaRicezione) {
        this.confermaRicezione = confermaRicezione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NotificaInteropEntrataResponse)) return false;
        NotificaInteropEntrataResponse other = (NotificaInteropEntrataResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.confermaRicezione==null && other.getConfermaRicezione()==null) || 
             (this.confermaRicezione!=null &&
              this.confermaRicezione.equals(other.getConfermaRicezione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConfermaRicezione() != null) {
            _hashCode += getConfermaRicezione().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NotificaInteropEntrataResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "NotificaInteropEntrataResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("confermaRicezione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ConfermaRicezione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
