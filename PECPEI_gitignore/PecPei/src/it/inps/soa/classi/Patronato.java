package it.inps.soa.classi;
/**
 * Patronato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Patronato  extends AddressBook  implements java.io.Serializable {
    private java.lang.String codice;

    private java.lang.String codiceUnitaOrganizzativa;

    private java.lang.String descrUnitaOrganizzativa;

    private java.lang.String nome;

    private java.lang.String numeroRegistrazione;

    public Patronato() {
    }

    public Patronato(
           java.lang.String CAP,
           java.lang.String citta,
           java.lang.String country,
           java.util.Calendar dataCreazione,
           java.lang.Boolean disabilitato,
           java.lang.String EMailCertificata,
           java.lang.String email,
           java.lang.String fax,
           java.lang.Integer id,
           java.lang.Integer idParent,
           java.lang.String indirizzo,
           java.lang.String nazione,
           java.lang.String note,
           java.lang.Integer notificaPreferita,
           java.lang.Integer numeroTelPreferito,
           org.apache.axis.types.Duration orarioPreferito,
           java.lang.String provincia,
           java.lang.String regione,
           java.lang.String telefono,
           java.lang.String telefonoCasa,
           java.lang.String telefonoUfficio,
           java.lang.String tipoSoggetto,
           java.lang.String codice,
           java.lang.String codiceUnitaOrganizzativa,
           java.lang.String descrUnitaOrganizzativa,
           java.lang.String nome,
           java.lang.String numeroRegistrazione) {
        super(
            CAP,
            citta,
            country,
            dataCreazione,
            disabilitato,
            EMailCertificata,
            email,
            fax,
            id,
            idParent,
            indirizzo,
            nazione,
            note,
            notificaPreferita,
            numeroTelPreferito,
            orarioPreferito,
            provincia,
            regione,
            telefono,
            telefonoCasa,
            telefonoUfficio,
            tipoSoggetto);
        this.codice = codice;
        this.codiceUnitaOrganizzativa = codiceUnitaOrganizzativa;
        this.descrUnitaOrganizzativa = descrUnitaOrganizzativa;
        this.nome = nome;
        this.numeroRegistrazione = numeroRegistrazione;
    }


    /**
     * Gets the codice value for this Patronato.
     * 
     * @return codice
     */
    public java.lang.String getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this Patronato.
     * 
     * @param codice
     */
    public void setCodice(java.lang.String codice) {
        this.codice = codice;
    }


    /**
     * Gets the codiceUnitaOrganizzativa value for this Patronato.
     * 
     * @return codiceUnitaOrganizzativa
     */
    public java.lang.String getCodiceUnitaOrganizzativa() {
        return codiceUnitaOrganizzativa;
    }


    /**
     * Sets the codiceUnitaOrganizzativa value for this Patronato.
     * 
     * @param codiceUnitaOrganizzativa
     */
    public void setCodiceUnitaOrganizzativa(java.lang.String codiceUnitaOrganizzativa) {
        this.codiceUnitaOrganizzativa = codiceUnitaOrganizzativa;
    }


    /**
     * Gets the descrUnitaOrganizzativa value for this Patronato.
     * 
     * @return descrUnitaOrganizzativa
     */
    public java.lang.String getDescrUnitaOrganizzativa() {
        return descrUnitaOrganizzativa;
    }


    /**
     * Sets the descrUnitaOrganizzativa value for this Patronato.
     * 
     * @param descrUnitaOrganizzativa
     */
    public void setDescrUnitaOrganizzativa(java.lang.String descrUnitaOrganizzativa) {
        this.descrUnitaOrganizzativa = descrUnitaOrganizzativa;
    }


    /**
     * Gets the nome value for this Patronato.
     * 
     * @return nome
     */
    public java.lang.String getNome() {
        return nome;
    }


    /**
     * Sets the nome value for this Patronato.
     * 
     * @param nome
     */
    public void setNome(java.lang.String nome) {
        this.nome = nome;
    }


    /**
     * Gets the numeroRegistrazione value for this Patronato.
     * 
     * @return numeroRegistrazione
     */
    public java.lang.String getNumeroRegistrazione() {
        return numeroRegistrazione;
    }


    /**
     * Sets the numeroRegistrazione value for this Patronato.
     * 
     * @param numeroRegistrazione
     */
    public void setNumeroRegistrazione(java.lang.String numeroRegistrazione) {
        this.numeroRegistrazione = numeroRegistrazione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Patronato)) return false;
        Patronato other = (Patronato) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.codiceUnitaOrganizzativa==null && other.getCodiceUnitaOrganizzativa()==null) || 
             (this.codiceUnitaOrganizzativa!=null &&
              this.codiceUnitaOrganizzativa.equals(other.getCodiceUnitaOrganizzativa()))) &&
            ((this.descrUnitaOrganizzativa==null && other.getDescrUnitaOrganizzativa()==null) || 
             (this.descrUnitaOrganizzativa!=null &&
              this.descrUnitaOrganizzativa.equals(other.getDescrUnitaOrganizzativa()))) &&
            ((this.nome==null && other.getNome()==null) || 
             (this.nome!=null &&
              this.nome.equals(other.getNome()))) &&
            ((this.numeroRegistrazione==null && other.getNumeroRegistrazione()==null) || 
             (this.numeroRegistrazione!=null &&
              this.numeroRegistrazione.equals(other.getNumeroRegistrazione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getCodiceUnitaOrganizzativa() != null) {
            _hashCode += getCodiceUnitaOrganizzativa().hashCode();
        }
        if (getDescrUnitaOrganizzativa() != null) {
            _hashCode += getDescrUnitaOrganizzativa().hashCode();
        }
        if (getNome() != null) {
            _hashCode += getNome().hashCode();
        }
        if (getNumeroRegistrazione() != null) {
            _hashCode += getNumeroRegistrazione().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Patronato.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Patronato"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceUnitaOrganizzativa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceUnitaOrganizzativa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrUnitaOrganizzativa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DescrUnitaOrganizzativa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nome");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Nome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroRegistrazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumeroRegistrazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
