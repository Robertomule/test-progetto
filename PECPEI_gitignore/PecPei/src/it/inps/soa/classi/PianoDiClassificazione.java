package it.inps.soa.classi;
/**
 * PianoDiClassificazione.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class PianoDiClassificazione  extends Response  implements java.io.Serializable {
    private TitolarioPEIPEC[] voce;

    public PianoDiClassificazione() {
    }

    public PianoDiClassificazione(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           TitolarioPEIPEC[] voce) {
        super(
            codice,
            descrizione);
        this.voce = voce;
    }


    /**
     * Gets the voce value for this PianoDiClassificazione.
     * 
     * @return voce
     */
    public TitolarioPEIPEC[] getVoce() {
        return voce;
    }


    /**
     * Sets the voce value for this PianoDiClassificazione.
     * 
     * @param voce
     */
    public void setVoce(TitolarioPEIPEC[] voce) {
        this.voce = voce;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PianoDiClassificazione)) return false;
        PianoDiClassificazione other = (PianoDiClassificazione) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.voce==null && other.getVoce()==null) || 
             (this.voce!=null &&
              java.util.Arrays.equals(this.voce, other.getVoce())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getVoce() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVoce());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVoce(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PianoDiClassificazione.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "PianoDiClassificazione"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voce");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Voce"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "TitolarioPEIPEC"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "TitolarioPEIPEC"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
