package it.inps.soa.classi;
/**
 * StatiAvanzamentoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class StatiAvanzamentoResponse  extends Response  implements java.io.Serializable {
    private StatoAvanzamento[] statiAvanzamento;

    public StatiAvanzamentoResponse() {
    }

    public StatiAvanzamentoResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           StatoAvanzamento[] statiAvanzamento) {
        super(
            codice,
            descrizione);
        this.statiAvanzamento = statiAvanzamento;
    }


    /**
     * Gets the statiAvanzamento value for this StatiAvanzamentoResponse.
     * 
     * @return statiAvanzamento
     */
    public StatoAvanzamento[] getStatiAvanzamento() {
        return statiAvanzamento;
    }


    /**
     * Sets the statiAvanzamento value for this StatiAvanzamentoResponse.
     * 
     * @param statiAvanzamento
     */
    public void setStatiAvanzamento(StatoAvanzamento[] statiAvanzamento) {
        this.statiAvanzamento = statiAvanzamento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StatiAvanzamentoResponse)) return false;
        StatiAvanzamentoResponse other = (StatiAvanzamentoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.statiAvanzamento==null && other.getStatiAvanzamento()==null) || 
             (this.statiAvanzamento!=null &&
              java.util.Arrays.equals(this.statiAvanzamento, other.getStatiAvanzamento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getStatiAvanzamento() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStatiAvanzamento());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStatiAvanzamento(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StatiAvanzamentoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "StatiAvanzamentoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statiAvanzamento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StatiAvanzamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "StatoAvanzamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "StatoAvanzamento"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
