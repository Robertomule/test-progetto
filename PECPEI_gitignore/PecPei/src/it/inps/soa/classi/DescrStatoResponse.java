package it.inps.soa.classi;
/**
 * DescrStatoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class DescrStatoResponse  extends Response  implements java.io.Serializable {
    private NomeStatoAvanzamento statoAvanzamento;

    public DescrStatoResponse() {
    }

    public DescrStatoResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           NomeStatoAvanzamento statoAvanzamento) {
        super(
            codice,
            descrizione);
        this.statoAvanzamento = statoAvanzamento;
    }


    /**
     * Gets the statoAvanzamento value for this DescrStatoResponse.
     * 
     * @return statoAvanzamento
     */
    public NomeStatoAvanzamento getStatoAvanzamento() {
        return statoAvanzamento;
    }


    /**
     * Sets the statoAvanzamento value for this DescrStatoResponse.
     * 
     * @param statoAvanzamento
     */
    public void setStatoAvanzamento(NomeStatoAvanzamento statoAvanzamento) {
        this.statoAvanzamento = statoAvanzamento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DescrStatoResponse)) return false;
        DescrStatoResponse other = (DescrStatoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.statoAvanzamento==null && other.getStatoAvanzamento()==null) || 
             (this.statoAvanzamento!=null &&
              this.statoAvanzamento.equals(other.getStatoAvanzamento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getStatoAvanzamento() != null) {
            _hashCode += getStatoAvanzamento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DescrStatoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "DescrStatoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statoAvanzamento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StatoAvanzamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "NomeStatoAvanzamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
