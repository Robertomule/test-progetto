package it.inps.soa.classi;
/**
 * CategoriaProtocolloResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class CategoriaProtocolloResponse  extends Response  implements java.io.Serializable {
    private org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnProtocolType categoriaProtocollo;

    public CategoriaProtocolloResponse() {
    }

    public CategoriaProtocolloResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnProtocolType categoriaProtocollo) {
        super(
            codice,
            descrizione);
        this.categoriaProtocollo = categoriaProtocollo;
    }


    /**
     * Gets the categoriaProtocollo value for this CategoriaProtocolloResponse.
     * 
     * @return categoriaProtocollo
     */
    public org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnProtocolType getCategoriaProtocollo() {
        return categoriaProtocollo;
    }


    /**
     * Sets the categoriaProtocollo value for this CategoriaProtocolloResponse.
     * 
     * @param categoriaProtocollo
     */
    public void setCategoriaProtocollo(org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnProtocolType categoriaProtocollo) {
        this.categoriaProtocollo = categoriaProtocollo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CategoriaProtocolloResponse)) return false;
        CategoriaProtocolloResponse other = (CategoriaProtocolloResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.categoriaProtocollo==null && other.getCategoriaProtocollo()==null) || 
             (this.categoriaProtocollo!=null &&
              this.categoriaProtocollo.equals(other.getCategoriaProtocollo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCategoriaProtocollo() != null) {
            _hashCode += getCategoriaProtocollo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CategoriaProtocolloResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "CategoriaProtocolloResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoriaProtocollo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CategoriaProtocollo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnProtocolType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
