package it.inps.soa.classi;
/**
 * UnitaOrganizzativeResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class UnitaOrganizzativeResponse  extends Response  implements java.io.Serializable {
    private java.lang.String codiceAOO;

    private UnitaOrganizzativa[] unitaOrganizzative;

    public UnitaOrganizzativeResponse() {
    }

    public UnitaOrganizzativeResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           java.lang.String codiceAOO,
           UnitaOrganizzativa[] unitaOrganizzative) {
        super(
            codice,
            descrizione);
        this.codiceAOO = codiceAOO;
        this.unitaOrganizzative = unitaOrganizzative;
    }


    /**
     * Gets the codiceAOO value for this UnitaOrganizzativeResponse.
     * 
     * @return codiceAOO
     */
    public java.lang.String getCodiceAOO() {
        return codiceAOO;
    }


    /**
     * Sets the codiceAOO value for this UnitaOrganizzativeResponse.
     * 
     * @param codiceAOO
     */
    public void setCodiceAOO(java.lang.String codiceAOO) {
        this.codiceAOO = codiceAOO;
    }


    /**
     * Gets the unitaOrganizzative value for this UnitaOrganizzativeResponse.
     * 
     * @return unitaOrganizzative
     */
    public UnitaOrganizzativa[] getUnitaOrganizzative() {
        return unitaOrganizzative;
    }


    /**
     * Sets the unitaOrganizzative value for this UnitaOrganizzativeResponse.
     * 
     * @param unitaOrganizzative
     */
    public void setUnitaOrganizzative(UnitaOrganizzativa[] unitaOrganizzative) {
        this.unitaOrganizzative = unitaOrganizzative;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UnitaOrganizzativeResponse)) return false;
        UnitaOrganizzativeResponse other = (UnitaOrganizzativeResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codiceAOO==null && other.getCodiceAOO()==null) || 
             (this.codiceAOO!=null &&
              this.codiceAOO.equals(other.getCodiceAOO()))) &&
            ((this.unitaOrganizzative==null && other.getUnitaOrganizzative()==null) || 
             (this.unitaOrganizzative!=null &&
              java.util.Arrays.equals(this.unitaOrganizzative, other.getUnitaOrganizzative())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodiceAOO() != null) {
            _hashCode += getCodiceAOO().hashCode();
        }
        if (getUnitaOrganizzative() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUnitaOrganizzative());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUnitaOrganizzative(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UnitaOrganizzativeResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "UnitaOrganizzativeResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceAOO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceAOO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitaOrganizzative");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UnitaOrganizzative"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "UnitaOrganizzativa"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "UnitaOrganizzativa"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
