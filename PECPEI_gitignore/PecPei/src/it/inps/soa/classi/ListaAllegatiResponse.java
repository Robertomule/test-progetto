package it.inps.soa.classi;
/**
 * ListaAllegatiResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class ListaAllegatiResponse  extends Response  implements java.io.Serializable {
    private Metadati[] allegatiMetadati;

    public ListaAllegatiResponse() {
    }

    public ListaAllegatiResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           Metadati[] allegatiMetadati) {
        super(
            codice,
            descrizione);
        this.allegatiMetadati = allegatiMetadati;
    }


    /**
     * Gets the allegatiMetadati value for this ListaAllegatiResponse.
     * 
     * @return allegatiMetadati
     */
    public Metadati[] getAllegatiMetadati() {
        return allegatiMetadati;
    }


    /**
     * Sets the allegatiMetadati value for this ListaAllegatiResponse.
     * 
     * @param allegatiMetadati
     */
    public void setAllegatiMetadati(Metadati[] allegatiMetadati) {
        this.allegatiMetadati = allegatiMetadati;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListaAllegatiResponse)) return false;
        ListaAllegatiResponse other = (ListaAllegatiResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.allegatiMetadati==null && other.getAllegatiMetadati()==null) || 
             (this.allegatiMetadati!=null &&
              java.util.Arrays.equals(this.allegatiMetadati, other.getAllegatiMetadati())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAllegatiMetadati() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAllegatiMetadati());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAllegatiMetadati(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListaAllegatiResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "ListaAllegatiResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allegatiMetadati");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AllegatiMetadati"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Metadati"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Metadati"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
