package it.inps.soa.classi;
/**
 * Principal.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Principal  implements java.io.Serializable {
    private java.lang.String codice;

    private java.lang.String descrizione;

    private java.util.Calendar dataCreazione;

    private java.lang.String tipo;

    private java.lang.Boolean disabilitato;

    private PrincipalDetail[] dettagliPrincipal;

    public Principal() {
    }

    public Principal(
           java.lang.String codice,
           java.lang.String descrizione,
           java.util.Calendar dataCreazione,
           java.lang.String tipo,
           java.lang.Boolean disabilitato,
           PrincipalDetail[] dettagliPrincipal) {
           this.codice = codice;
           this.descrizione = descrizione;
           this.dataCreazione = dataCreazione;
           this.tipo = tipo;
           this.disabilitato = disabilitato;
           this.dettagliPrincipal = dettagliPrincipal;
    }


    /**
     * Gets the codice value for this Principal.
     * 
     * @return codice
     */
    public java.lang.String getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this Principal.
     * 
     * @param codice
     */
    public void setCodice(java.lang.String codice) {
        this.codice = codice;
    }


    /**
     * Gets the descrizione value for this Principal.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this Principal.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the dataCreazione value for this Principal.
     * 
     * @return dataCreazione
     */
    public java.util.Calendar getDataCreazione() {
        return dataCreazione;
    }


    /**
     * Sets the dataCreazione value for this Principal.
     * 
     * @param dataCreazione
     */
    public void setDataCreazione(java.util.Calendar dataCreazione) {
        this.dataCreazione = dataCreazione;
    }


    /**
     * Gets the tipo value for this Principal.
     * 
     * @return tipo
     */
    public java.lang.String getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this Principal.
     * 
     * @param tipo
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the disabilitato value for this Principal.
     * 
     * @return disabilitato
     */
    public java.lang.Boolean getDisabilitato() {
        return disabilitato;
    }


    /**
     * Sets the disabilitato value for this Principal.
     * 
     * @param disabilitato
     */
    public void setDisabilitato(java.lang.Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }


    /**
     * Gets the dettagliPrincipal value for this Principal.
     * 
     * @return dettagliPrincipal
     */
    public PrincipalDetail[] getDettagliPrincipal() {
        return dettagliPrincipal;
    }


    /**
     * Sets the dettagliPrincipal value for this Principal.
     * 
     * @param dettagliPrincipal
     */
    public void setDettagliPrincipal(PrincipalDetail[] dettagliPrincipal) {
        this.dettagliPrincipal = dettagliPrincipal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Principal)) return false;
        Principal other = (Principal) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.dataCreazione==null && other.getDataCreazione()==null) || 
             (this.dataCreazione!=null &&
              this.dataCreazione.equals(other.getDataCreazione()))) &&
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            ((this.disabilitato==null && other.getDisabilitato()==null) || 
             (this.disabilitato!=null &&
              this.disabilitato.equals(other.getDisabilitato()))) &&
            ((this.dettagliPrincipal==null && other.getDettagliPrincipal()==null) || 
             (this.dettagliPrincipal!=null &&
              java.util.Arrays.equals(this.dettagliPrincipal, other.getDettagliPrincipal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getDataCreazione() != null) {
            _hashCode += getDataCreazione().hashCode();
        }
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        if (getDisabilitato() != null) {
            _hashCode += getDisabilitato().hashCode();
        }
        if (getDettagliPrincipal() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDettagliPrincipal());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDettagliPrincipal(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Principal.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Principal"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataCreazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataCreazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Disabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dettagliPrincipal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DettagliPrincipal"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "PrincipalDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "PrincipalDetail"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
