package it.inps.soa.classi;
/**
 * RicercaDocumentiProtocollatiResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class RicercaDocumentiProtocollatiResponse  extends Response  implements java.io.Serializable {
    private ProtocolloEntrata[] protocolliEntrata;

    private ProtocolloUscita[] protocolliUscita;

    private java.lang.Integer totali;

    public RicercaDocumentiProtocollatiResponse() {
    }

    public RicercaDocumentiProtocollatiResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           ProtocolloEntrata[] protocolliEntrata,
           ProtocolloUscita[] protocolliUscita,
           java.lang.Integer totali) {
        super(
            codice,
            descrizione);
        this.protocolliEntrata = protocolliEntrata;
        this.protocolliUscita = protocolliUscita;
        this.totali = totali;
    }


    /**
     * Gets the protocolliEntrata value for this RicercaDocumentiProtocollatiResponse.
     * 
     * @return protocolliEntrata
     */
    public ProtocolloEntrata[] getProtocolliEntrata() {
        return protocolliEntrata;
    }


    /**
     * Sets the protocolliEntrata value for this RicercaDocumentiProtocollatiResponse.
     * 
     * @param protocolliEntrata
     */
    public void setProtocolliEntrata(ProtocolloEntrata[] protocolliEntrata) {
        this.protocolliEntrata = protocolliEntrata;
    }


    /**
     * Gets the protocolliUscita value for this RicercaDocumentiProtocollatiResponse.
     * 
     * @return protocolliUscita
     */
    public ProtocolloUscita[] getProtocolliUscita() {
        return protocolliUscita;
    }


    /**
     * Sets the protocolliUscita value for this RicercaDocumentiProtocollatiResponse.
     * 
     * @param protocolliUscita
     */
    public void setProtocolliUscita(ProtocolloUscita[] protocolliUscita) {
        this.protocolliUscita = protocolliUscita;
    }


    /**
     * Gets the totali value for this RicercaDocumentiProtocollatiResponse.
     * 
     * @return totali
     */
    public java.lang.Integer getTotali() {
        return totali;
    }


    /**
     * Sets the totali value for this RicercaDocumentiProtocollatiResponse.
     * 
     * @param totali
     */
    public void setTotali(java.lang.Integer totali) {
        this.totali = totali;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RicercaDocumentiProtocollatiResponse)) return false;
        RicercaDocumentiProtocollatiResponse other = (RicercaDocumentiProtocollatiResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.protocolliEntrata==null && other.getProtocolliEntrata()==null) || 
             (this.protocolliEntrata!=null &&
              java.util.Arrays.equals(this.protocolliEntrata, other.getProtocolliEntrata()))) &&
            ((this.protocolliUscita==null && other.getProtocolliUscita()==null) || 
             (this.protocolliUscita!=null &&
              java.util.Arrays.equals(this.protocolliUscita, other.getProtocolliUscita()))) &&
            ((this.totali==null && other.getTotali()==null) || 
             (this.totali!=null &&
              this.totali.equals(other.getTotali())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getProtocolliEntrata() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProtocolliEntrata());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProtocolliEntrata(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProtocolliUscita() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProtocolliUscita());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProtocolliUscita(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTotali() != null) {
            _hashCode += getTotali().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RicercaDocumentiProtocollatiResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "RicercaDocumentiProtocollatiResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolliEntrata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProtocolliEntrata"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "ProtocolloEntrata"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "ProtocolloEntrata"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolliUscita");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProtocolliUscita"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "ProtocolloUscita"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "ProtocolloUscita"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totali");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Totali"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
