package it.inps.soa.classi;
/**
 * MetadatiProtocolliPatrocinatiResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class MetadatiProtocolliPatrocinatiResponse  extends Response  implements java.io.Serializable {
    private java.lang.String codAOO;

    private MetadatiPatrocinato[] metadatiPatrocinati;

    public MetadatiProtocolliPatrocinatiResponse() {
    }

    public MetadatiProtocolliPatrocinatiResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           java.lang.String codAOO,
           MetadatiPatrocinato[] metadatiPatrocinati) {
        super(
            codice,
            descrizione);
        this.codAOO = codAOO;
        this.metadatiPatrocinati = metadatiPatrocinati;
    }


    /**
     * Gets the codAOO value for this MetadatiProtocolliPatrocinatiResponse.
     * 
     * @return codAOO
     */
    public java.lang.String getCodAOO() {
        return codAOO;
    }


    /**
     * Sets the codAOO value for this MetadatiProtocolliPatrocinatiResponse.
     * 
     * @param codAOO
     */
    public void setCodAOO(java.lang.String codAOO) {
        this.codAOO = codAOO;
    }


    /**
     * Gets the metadatiPatrocinati value for this MetadatiProtocolliPatrocinatiResponse.
     * 
     * @return metadatiPatrocinati
     */
    public MetadatiPatrocinato[] getMetadatiPatrocinati() {
        return metadatiPatrocinati;
    }


    /**
     * Sets the metadatiPatrocinati value for this MetadatiProtocolliPatrocinatiResponse.
     * 
     * @param metadatiPatrocinati
     */
    public void setMetadatiPatrocinati(MetadatiPatrocinato[] metadatiPatrocinati) {
        this.metadatiPatrocinati = metadatiPatrocinati;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MetadatiProtocolliPatrocinatiResponse)) return false;
        MetadatiProtocolliPatrocinatiResponse other = (MetadatiProtocolliPatrocinatiResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codAOO==null && other.getCodAOO()==null) || 
             (this.codAOO!=null &&
              this.codAOO.equals(other.getCodAOO()))) &&
            ((this.metadatiPatrocinati==null && other.getMetadatiPatrocinati()==null) || 
             (this.metadatiPatrocinati!=null &&
              java.util.Arrays.equals(this.metadatiPatrocinati, other.getMetadatiPatrocinati())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodAOO() != null) {
            _hashCode += getCodAOO().hashCode();
        }
        if (getMetadatiPatrocinati() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMetadatiPatrocinati());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMetadatiPatrocinati(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MetadatiProtocolliPatrocinatiResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "MetadatiProtocolliPatrocinatiResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAOO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodAOO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metadatiPatrocinati");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MetadatiPatrocinati"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "MetadatiPatrocinato"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "MetadatiPatrocinato"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
