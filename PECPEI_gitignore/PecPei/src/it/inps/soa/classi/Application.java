package it.inps.soa.classi;
/**
 * Application.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Application  implements java.io.Serializable {
    private java.lang.String CAA;

    private java.lang.String codice;

    private java.lang.String descrizione;

    private java.lang.Boolean disabilitato;

    private java.lang.String legacyApplicationCode;

    private java.lang.String note;

    private java.lang.Integer stateManagement;

    private java.util.Calendar validoAl;

    private java.util.Calendar validoDal;

    public Application() {
    }

    public Application(
           java.lang.String CAA,
           java.lang.String codice,
           java.lang.String descrizione,
           java.lang.Boolean disabilitato,
           java.lang.String legacyApplicationCode,
           java.lang.String note,
           java.lang.Integer stateManagement,
           java.util.Calendar validoAl,
           java.util.Calendar validoDal) {
           this.CAA = CAA;
           this.codice = codice;
           this.descrizione = descrizione;
           this.disabilitato = disabilitato;
           this.legacyApplicationCode = legacyApplicationCode;
           this.note = note;
           this.stateManagement = stateManagement;
           this.validoAl = validoAl;
           this.validoDal = validoDal;
    }


    /**
     * Gets the CAA value for this Application.
     * 
     * @return CAA
     */
    public java.lang.String getCAA() {
        return CAA;
    }


    /**
     * Sets the CAA value for this Application.
     * 
     * @param CAA
     */
    public void setCAA(java.lang.String CAA) {
        this.CAA = CAA;
    }


    /**
     * Gets the codice value for this Application.
     * 
     * @return codice
     */
    public java.lang.String getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this Application.
     * 
     * @param codice
     */
    public void setCodice(java.lang.String codice) {
        this.codice = codice;
    }


    /**
     * Gets the descrizione value for this Application.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this Application.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the disabilitato value for this Application.
     * 
     * @return disabilitato
     */
    public java.lang.Boolean getDisabilitato() {
        return disabilitato;
    }


    /**
     * Sets the disabilitato value for this Application.
     * 
     * @param disabilitato
     */
    public void setDisabilitato(java.lang.Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }


    /**
     * Gets the legacyApplicationCode value for this Application.
     * 
     * @return legacyApplicationCode
     */
    public java.lang.String getLegacyApplicationCode() {
        return legacyApplicationCode;
    }


    /**
     * Sets the legacyApplicationCode value for this Application.
     * 
     * @param legacyApplicationCode
     */
    public void setLegacyApplicationCode(java.lang.String legacyApplicationCode) {
        this.legacyApplicationCode = legacyApplicationCode;
    }


    /**
     * Gets the note value for this Application.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this Application.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }


    /**
     * Gets the stateManagement value for this Application.
     * 
     * @return stateManagement
     */
    public java.lang.Integer getStateManagement() {
        return stateManagement;
    }


    /**
     * Sets the stateManagement value for this Application.
     * 
     * @param stateManagement
     */
    public void setStateManagement(java.lang.Integer stateManagement) {
        this.stateManagement = stateManagement;
    }


    /**
     * Gets the validoAl value for this Application.
     * 
     * @return validoAl
     */
    public java.util.Calendar getValidoAl() {
        return validoAl;
    }


    /**
     * Sets the validoAl value for this Application.
     * 
     * @param validoAl
     */
    public void setValidoAl(java.util.Calendar validoAl) {
        this.validoAl = validoAl;
    }


    /**
     * Gets the validoDal value for this Application.
     * 
     * @return validoDal
     */
    public java.util.Calendar getValidoDal() {
        return validoDal;
    }


    /**
     * Sets the validoDal value for this Application.
     * 
     * @param validoDal
     */
    public void setValidoDal(java.util.Calendar validoDal) {
        this.validoDal = validoDal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Application)) return false;
        Application other = (Application) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CAA==null && other.getCAA()==null) || 
             (this.CAA!=null &&
              this.CAA.equals(other.getCAA()))) &&
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.disabilitato==null && other.getDisabilitato()==null) || 
             (this.disabilitato!=null &&
              this.disabilitato.equals(other.getDisabilitato()))) &&
            ((this.legacyApplicationCode==null && other.getLegacyApplicationCode()==null) || 
             (this.legacyApplicationCode!=null &&
              this.legacyApplicationCode.equals(other.getLegacyApplicationCode()))) &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote()))) &&
            ((this.stateManagement==null && other.getStateManagement()==null) || 
             (this.stateManagement!=null &&
              this.stateManagement.equals(other.getStateManagement()))) &&
            ((this.validoAl==null && other.getValidoAl()==null) || 
             (this.validoAl!=null &&
              this.validoAl.equals(other.getValidoAl()))) &&
            ((this.validoDal==null && other.getValidoDal()==null) || 
             (this.validoDal!=null &&
              this.validoDal.equals(other.getValidoDal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCAA() != null) {
            _hashCode += getCAA().hashCode();
        }
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getDisabilitato() != null) {
            _hashCode += getDisabilitato().hashCode();
        }
        if (getLegacyApplicationCode() != null) {
            _hashCode += getLegacyApplicationCode().hashCode();
        }
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        if (getStateManagement() != null) {
            _hashCode += getStateManagement().hashCode();
        }
        if (getValidoAl() != null) {
            _hashCode += getValidoAl().hashCode();
        }
        if (getValidoDal() != null) {
            _hashCode += getValidoDal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Application.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Application"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CAA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CAA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Disabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("legacyApplicationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LegacyApplicationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateManagement");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StateManagement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validoAl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ValidoAl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validoDal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ValidoDal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
