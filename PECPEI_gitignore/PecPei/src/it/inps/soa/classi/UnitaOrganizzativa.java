package it.inps.soa.classi;
/**
 * UnitaOrganizzativa.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class UnitaOrganizzativa  implements java.io.Serializable {
    private java.lang.String codice;

    private java.lang.String codiceAO;

    private java.lang.String codiceUOPadre;

    private java.util.Calendar dataDisabilitato;

    private java.lang.String descrizione;

    private java.lang.Boolean disabilitato;

    private java.lang.Integer originalId;

    private java.lang.String responsabile;

    public UnitaOrganizzativa() {
    }

    public UnitaOrganizzativa(
           java.lang.String codice,
           java.lang.String codiceAO,
           java.lang.String codiceUOPadre,
           java.util.Calendar dataDisabilitato,
           java.lang.String descrizione,
           java.lang.Boolean disabilitato,
           java.lang.Integer originalId,
           java.lang.String responsabile) {
           this.codice = codice;
           this.codiceAO = codiceAO;
           this.codiceUOPadre = codiceUOPadre;
           this.dataDisabilitato = dataDisabilitato;
           this.descrizione = descrizione;
           this.disabilitato = disabilitato;
           this.originalId = originalId;
           this.responsabile = responsabile;
    }


    /**
     * Gets the codice value for this UnitaOrganizzativa.
     * 
     * @return codice
     */
    public java.lang.String getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this UnitaOrganizzativa.
     * 
     * @param codice
     */
    public void setCodice(java.lang.String codice) {
        this.codice = codice;
    }


    /**
     * Gets the codiceAO value for this UnitaOrganizzativa.
     * 
     * @return codiceAO
     */
    public java.lang.String getCodiceAO() {
        return codiceAO;
    }


    /**
     * Sets the codiceAO value for this UnitaOrganizzativa.
     * 
     * @param codiceAO
     */
    public void setCodiceAO(java.lang.String codiceAO) {
        this.codiceAO = codiceAO;
    }


    /**
     * Gets the codiceUOPadre value for this UnitaOrganizzativa.
     * 
     * @return codiceUOPadre
     */
    public java.lang.String getCodiceUOPadre() {
        return codiceUOPadre;
    }


    /**
     * Sets the codiceUOPadre value for this UnitaOrganizzativa.
     * 
     * @param codiceUOPadre
     */
    public void setCodiceUOPadre(java.lang.String codiceUOPadre) {
        this.codiceUOPadre = codiceUOPadre;
    }


    /**
     * Gets the dataDisabilitato value for this UnitaOrganizzativa.
     * 
     * @return dataDisabilitato
     */
    public java.util.Calendar getDataDisabilitato() {
        return dataDisabilitato;
    }


    /**
     * Sets the dataDisabilitato value for this UnitaOrganizzativa.
     * 
     * @param dataDisabilitato
     */
    public void setDataDisabilitato(java.util.Calendar dataDisabilitato) {
        this.dataDisabilitato = dataDisabilitato;
    }


    /**
     * Gets the descrizione value for this UnitaOrganizzativa.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this UnitaOrganizzativa.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the disabilitato value for this UnitaOrganizzativa.
     * 
     * @return disabilitato
     */
    public java.lang.Boolean getDisabilitato() {
        return disabilitato;
    }


    /**
     * Sets the disabilitato value for this UnitaOrganizzativa.
     * 
     * @param disabilitato
     */
    public void setDisabilitato(java.lang.Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }


    /**
     * Gets the originalId value for this UnitaOrganizzativa.
     * 
     * @return originalId
     */
    public java.lang.Integer getOriginalId() {
        return originalId;
    }


    /**
     * Sets the originalId value for this UnitaOrganizzativa.
     * 
     * @param originalId
     */
    public void setOriginalId(java.lang.Integer originalId) {
        this.originalId = originalId;
    }


    /**
     * Gets the responsabile value for this UnitaOrganizzativa.
     * 
     * @return responsabile
     */
    public java.lang.String getResponsabile() {
        return responsabile;
    }


    /**
     * Sets the responsabile value for this UnitaOrganizzativa.
     * 
     * @param responsabile
     */
    public void setResponsabile(java.lang.String responsabile) {
        this.responsabile = responsabile;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UnitaOrganizzativa)) return false;
        UnitaOrganizzativa other = (UnitaOrganizzativa) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.codiceAO==null && other.getCodiceAO()==null) || 
             (this.codiceAO!=null &&
              this.codiceAO.equals(other.getCodiceAO()))) &&
            ((this.codiceUOPadre==null && other.getCodiceUOPadre()==null) || 
             (this.codiceUOPadre!=null &&
              this.codiceUOPadre.equals(other.getCodiceUOPadre()))) &&
            ((this.dataDisabilitato==null && other.getDataDisabilitato()==null) || 
             (this.dataDisabilitato!=null &&
              this.dataDisabilitato.equals(other.getDataDisabilitato()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.disabilitato==null && other.getDisabilitato()==null) || 
             (this.disabilitato!=null &&
              this.disabilitato.equals(other.getDisabilitato()))) &&
            ((this.originalId==null && other.getOriginalId()==null) || 
             (this.originalId!=null &&
              this.originalId.equals(other.getOriginalId()))) &&
            ((this.responsabile==null && other.getResponsabile()==null) || 
             (this.responsabile!=null &&
              this.responsabile.equals(other.getResponsabile())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getCodiceAO() != null) {
            _hashCode += getCodiceAO().hashCode();
        }
        if (getCodiceUOPadre() != null) {
            _hashCode += getCodiceUOPadre().hashCode();
        }
        if (getDataDisabilitato() != null) {
            _hashCode += getDataDisabilitato().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getDisabilitato() != null) {
            _hashCode += getDisabilitato().hashCode();
        }
        if (getOriginalId() != null) {
            _hashCode += getOriginalId().hashCode();
        }
        if (getResponsabile() != null) {
            _hashCode += getResponsabile().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UnitaOrganizzativa.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "UnitaOrganizzativa"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceAO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceAO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceUOPadre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceUOPadre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataDisabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataDisabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Disabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OriginalId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responsabile");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Responsabile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
