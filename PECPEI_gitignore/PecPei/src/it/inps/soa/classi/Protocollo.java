package it.inps.soa.classi;
/**
 * Protocollo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Protocollo  extends ProtocolloExt  implements java.io.Serializable {
    private DocumentoPrincipale documento;

    private Allegato[] allegati;

    private SoggettoAfferente[] soggettiAfferenti;

    private java.lang.String registrazioneProvenienza;

    private java.lang.String primaRegistrazione;

    private java.util.Calendar dataInvio;

    private java.util.Calendar dataArrivo;

    public Protocollo() {
    }

    public Protocollo(
           DocumentoPrincipale documento,
           Allegato[] allegati,
           SoggettoAfferente[] soggettiAfferenti,
           java.lang.String registrazioneProvenienza,
           java.lang.String primaRegistrazione,
           java.util.Calendar dataInvio,
           java.util.Calendar dataArrivo) {
        this.documento = documento;
        this.allegati = allegati;
        this.soggettiAfferenti = soggettiAfferenti;
        this.registrazioneProvenienza = registrazioneProvenienza;
        this.primaRegistrazione = primaRegistrazione;
        this.dataInvio = dataInvio;
        this.dataArrivo = dataArrivo;
    }


    /**
     * Gets the documento value for this Protocollo.
     * 
     * @return documento
     */
    public DocumentoPrincipale getDocumento() {
        return documento;
    }


    /**
     * Sets the documento value for this Protocollo.
     * 
     * @param documento
     */
    public void setDocumento(DocumentoPrincipale documento) {
        this.documento = documento;
    }


    /**
     * Gets the allegati value for this Protocollo.
     * 
     * @return allegati
     */
    public Allegato[] getAllegati() {
        return allegati;
    }


    /**
     * Sets the allegati value for this Protocollo.
     * 
     * @param allegati
     */
    public void setAllegati(Allegato[] allegati) {
        this.allegati = allegati;
    }


    /**
     * Gets the soggettiAfferenti value for this Protocollo.
     * 
     * @return soggettiAfferenti
     */
    public SoggettoAfferente[] getSoggettiAfferenti() {
        return soggettiAfferenti;
    }


    /**
     * Sets the soggettiAfferenti value for this Protocollo.
     * 
     * @param soggettiAfferenti
     */
    public void setSoggettiAfferenti(SoggettoAfferente[] soggettiAfferenti) {
        this.soggettiAfferenti = soggettiAfferenti;
    }


    /**
     * Gets the registrazioneProvenienza value for this Protocollo.
     * 
     * @return registrazioneProvenienza
     */
    public java.lang.String getRegistrazioneProvenienza() {
        return registrazioneProvenienza;
    }


    /**
     * Sets the registrazioneProvenienza value for this Protocollo.
     * 
     * @param registrazioneProvenienza
     */
    public void setRegistrazioneProvenienza(java.lang.String registrazioneProvenienza) {
        this.registrazioneProvenienza = registrazioneProvenienza;
    }


    /**
     * Gets the primaRegistrazione value for this Protocollo.
     * 
     * @return primaRegistrazione
     */
    public java.lang.String getPrimaRegistrazione() {
        return primaRegistrazione;
    }


    /**
     * Sets the primaRegistrazione value for this Protocollo.
     * 
     * @param primaRegistrazione
     */
    public void setPrimaRegistrazione(java.lang.String primaRegistrazione) {
        this.primaRegistrazione = primaRegistrazione;
    }


    /**
     * Gets the dataInvio value for this Protocollo.
     * 
     * @return dataInvio
     */
    public java.util.Calendar getDataInvio() {
        return dataInvio;
    }


    /**
     * Sets the dataInvio value for this Protocollo.
     * 
     * @param dataInvio
     */
    public void setDataInvio(java.util.Calendar dataInvio) {
        this.dataInvio = dataInvio;
    }


    /**
     * Gets the dataArrivo value for this Protocollo.
     * 
     * @return dataArrivo
     */
    public java.util.Calendar getDataArrivo() {
        return dataArrivo;
    }


    /**
     * Sets the dataArrivo value for this Protocollo.
     * 
     * @param dataArrivo
     */
    public void setDataArrivo(java.util.Calendar dataArrivo) {
        this.dataArrivo = dataArrivo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Protocollo)) return false;
        Protocollo other = (Protocollo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.documento==null && other.getDocumento()==null) || 
             (this.documento!=null &&
              this.documento.equals(other.getDocumento()))) &&
            ((this.allegati==null && other.getAllegati()==null) || 
             (this.allegati!=null &&
              java.util.Arrays.equals(this.allegati, other.getAllegati()))) &&
            ((this.soggettiAfferenti==null && other.getSoggettiAfferenti()==null) || 
             (this.soggettiAfferenti!=null &&
              java.util.Arrays.equals(this.soggettiAfferenti, other.getSoggettiAfferenti()))) &&
            ((this.registrazioneProvenienza==null && other.getRegistrazioneProvenienza()==null) || 
             (this.registrazioneProvenienza!=null &&
              this.registrazioneProvenienza.equals(other.getRegistrazioneProvenienza()))) &&
            ((this.primaRegistrazione==null && other.getPrimaRegistrazione()==null) || 
             (this.primaRegistrazione!=null &&
              this.primaRegistrazione.equals(other.getPrimaRegistrazione()))) &&
            ((this.dataInvio==null && other.getDataInvio()==null) || 
             (this.dataInvio!=null &&
              this.dataInvio.equals(other.getDataInvio()))) &&
            ((this.dataArrivo==null && other.getDataArrivo()==null) || 
             (this.dataArrivo!=null &&
              this.dataArrivo.equals(other.getDataArrivo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDocumento() != null) {
            _hashCode += getDocumento().hashCode();
        }
        if (getAllegati() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAllegati());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAllegati(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSoggettiAfferenti() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSoggettiAfferenti());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSoggettiAfferenti(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRegistrazioneProvenienza() != null) {
            _hashCode += getRegistrazioneProvenienza().hashCode();
        }
        if (getPrimaRegistrazione() != null) {
            _hashCode += getPrimaRegistrazione().hashCode();
        }
        if (getDataInvio() != null) {
            _hashCode += getDataInvio().hashCode();
        }
        if (getDataArrivo() != null) {
            _hashCode += getDataArrivo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Protocollo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Protocollo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Documento"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "DocumentoPrincipale"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allegati");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Allegati"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Allegato"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Allegato"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soggettiAfferenti");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SoggettiAfferenti"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "SoggettoAfferente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "SoggettoAfferente"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registrazioneProvenienza");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RegistrazioneProvenienza"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaRegistrazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PrimaRegistrazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataInvio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataInvio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataArrivo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataArrivo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
