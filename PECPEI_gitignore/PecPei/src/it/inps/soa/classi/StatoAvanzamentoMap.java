package it.inps.soa.classi;
/**
 * StatoAvanzamentoMap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class StatoAvanzamentoMap  implements java.io.Serializable {
    private java.lang.String codiceExt;

    private java.lang.String descrizioneExt;

    private java.lang.Integer idStatoSGD;

    private java.lang.String descrizioneSGD;

    private java.lang.String codiceSGD;

    public StatoAvanzamentoMap() {
    }

    public StatoAvanzamentoMap(
           java.lang.String codiceExt,
           java.lang.String descrizioneExt,
           java.lang.Integer idStatoSGD,
           java.lang.String descrizioneSGD,
           java.lang.String codiceSGD) {
           this.codiceExt = codiceExt;
           this.descrizioneExt = descrizioneExt;
           this.idStatoSGD = idStatoSGD;
           this.descrizioneSGD = descrizioneSGD;
           this.codiceSGD = codiceSGD;
    }


    /**
     * Gets the codiceExt value for this StatoAvanzamentoMap.
     * 
     * @return codiceExt
     */
    public java.lang.String getCodiceExt() {
        return codiceExt;
    }


    /**
     * Sets the codiceExt value for this StatoAvanzamentoMap.
     * 
     * @param codiceExt
     */
    public void setCodiceExt(java.lang.String codiceExt) {
        this.codiceExt = codiceExt;
    }


    /**
     * Gets the descrizioneExt value for this StatoAvanzamentoMap.
     * 
     * @return descrizioneExt
     */
    public java.lang.String getDescrizioneExt() {
        return descrizioneExt;
    }


    /**
     * Sets the descrizioneExt value for this StatoAvanzamentoMap.
     * 
     * @param descrizioneExt
     */
    public void setDescrizioneExt(java.lang.String descrizioneExt) {
        this.descrizioneExt = descrizioneExt;
    }


    /**
     * Gets the idStatoSGD value for this StatoAvanzamentoMap.
     * 
     * @return idStatoSGD
     */
    public java.lang.Integer getIdStatoSGD() {
        return idStatoSGD;
    }


    /**
     * Sets the idStatoSGD value for this StatoAvanzamentoMap.
     * 
     * @param idStatoSGD
     */
    public void setIdStatoSGD(java.lang.Integer idStatoSGD) {
        this.idStatoSGD = idStatoSGD;
    }


    /**
     * Gets the descrizioneSGD value for this StatoAvanzamentoMap.
     * 
     * @return descrizioneSGD
     */
    public java.lang.String getDescrizioneSGD() {
        return descrizioneSGD;
    }


    /**
     * Sets the descrizioneSGD value for this StatoAvanzamentoMap.
     * 
     * @param descrizioneSGD
     */
    public void setDescrizioneSGD(java.lang.String descrizioneSGD) {
        this.descrizioneSGD = descrizioneSGD;
    }


    /**
     * Gets the codiceSGD value for this StatoAvanzamentoMap.
     * 
     * @return codiceSGD
     */
    public java.lang.String getCodiceSGD() {
        return codiceSGD;
    }


    /**
     * Sets the codiceSGD value for this StatoAvanzamentoMap.
     * 
     * @param codiceSGD
     */
    public void setCodiceSGD(java.lang.String codiceSGD) {
        this.codiceSGD = codiceSGD;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StatoAvanzamentoMap)) return false;
        StatoAvanzamentoMap other = (StatoAvanzamentoMap) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codiceExt==null && other.getCodiceExt()==null) || 
             (this.codiceExt!=null &&
              this.codiceExt.equals(other.getCodiceExt()))) &&
            ((this.descrizioneExt==null && other.getDescrizioneExt()==null) || 
             (this.descrizioneExt!=null &&
              this.descrizioneExt.equals(other.getDescrizioneExt()))) &&
            ((this.idStatoSGD==null && other.getIdStatoSGD()==null) || 
             (this.idStatoSGD!=null &&
              this.idStatoSGD.equals(other.getIdStatoSGD()))) &&
            ((this.descrizioneSGD==null && other.getDescrizioneSGD()==null) || 
             (this.descrizioneSGD!=null &&
              this.descrizioneSGD.equals(other.getDescrizioneSGD()))) &&
            ((this.codiceSGD==null && other.getCodiceSGD()==null) || 
             (this.codiceSGD!=null &&
              this.codiceSGD.equals(other.getCodiceSGD())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodiceExt() != null) {
            _hashCode += getCodiceExt().hashCode();
        }
        if (getDescrizioneExt() != null) {
            _hashCode += getDescrizioneExt().hashCode();
        }
        if (getIdStatoSGD() != null) {
            _hashCode += getIdStatoSGD().hashCode();
        }
        if (getDescrizioneSGD() != null) {
            _hashCode += getDescrizioneSGD().hashCode();
        }
        if (getCodiceSGD() != null) {
            _hashCode += getCodiceSGD().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StatoAvanzamentoMap.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "StatoAvanzamentoMap"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizioneExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DescrizioneExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idStatoSGD");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IdStatoSGD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizioneSGD");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DescrizioneSGD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceSGD");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceSGD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
