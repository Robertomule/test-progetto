package it.inps.soa.classi;
/**
 * Ut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Ut  extends UserExt  implements java.io.Serializable {
    private java.lang.Boolean amministratore;

    private java.lang.String codiceFiscale;

    private java.lang.String codiceProfiloUtente;

    private java.util.Calendar dataCreazione;

    private java.lang.String email;

    private java.lang.String numeroRegistrazione;

    private Ruolo[] ruoli;

    public Ut() {
    }

    public Ut(
           java.lang.String codice,
           java.lang.String cognome,
           java.lang.Boolean disabilitato,
           java.lang.String nome,
           java.lang.String nominativo,
           java.lang.Boolean amministratore,
           java.lang.String codiceFiscale,
           java.lang.String codiceProfiloUtente,
           java.util.Calendar dataCreazione,
           java.lang.String email,
           java.lang.String numeroRegistrazione,
           Ruolo[] ruoli) {
        super(
            codice,
            cognome,
            disabilitato,
            nome,
            nominativo);
        this.amministratore = amministratore;
        this.codiceFiscale = codiceFiscale;
        this.codiceProfiloUtente = codiceProfiloUtente;
        this.dataCreazione = dataCreazione;
        this.email = email;
        this.numeroRegistrazione = numeroRegistrazione;
        this.ruoli = ruoli;
    }


    /**
     * Gets the amministratore value for this Ut.
     * 
     * @return amministratore
     */
    public java.lang.Boolean getAmministratore() {
        return amministratore;
    }


    /**
     * Sets the amministratore value for this Ut.
     * 
     * @param amministratore
     */
    public void setAmministratore(java.lang.Boolean amministratore) {
        this.amministratore = amministratore;
    }


    /**
     * Gets the codiceFiscale value for this Ut.
     * 
     * @return codiceFiscale
     */
    public java.lang.String getCodiceFiscale() {
        return codiceFiscale;
    }


    /**
     * Sets the codiceFiscale value for this Ut.
     * 
     * @param codiceFiscale
     */
    public void setCodiceFiscale(java.lang.String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }


    /**
     * Gets the codiceProfiloUtente value for this Ut.
     * 
     * @return codiceProfiloUtente
     */
    public java.lang.String getCodiceProfiloUtente() {
        return codiceProfiloUtente;
    }


    /**
     * Sets the codiceProfiloUtente value for this Ut.
     * 
     * @param codiceProfiloUtente
     */
    public void setCodiceProfiloUtente(java.lang.String codiceProfiloUtente) {
        this.codiceProfiloUtente = codiceProfiloUtente;
    }


    /**
     * Gets the dataCreazione value for this Ut.
     * 
     * @return dataCreazione
     */
    public java.util.Calendar getDataCreazione() {
        return dataCreazione;
    }


    /**
     * Sets the dataCreazione value for this Ut.
     * 
     * @param dataCreazione
     */
    public void setDataCreazione(java.util.Calendar dataCreazione) {
        this.dataCreazione = dataCreazione;
    }


    /**
     * Gets the email value for this Ut.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this Ut.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the numeroRegistrazione value for this Ut.
     * 
     * @return numeroRegistrazione
     */
    public java.lang.String getNumeroRegistrazione() {
        return numeroRegistrazione;
    }


    /**
     * Sets the numeroRegistrazione value for this Ut.
     * 
     * @param numeroRegistrazione
     */
    public void setNumeroRegistrazione(java.lang.String numeroRegistrazione) {
        this.numeroRegistrazione = numeroRegistrazione;
    }


    /**
     * Gets the ruoli value for this Ut.
     * 
     * @return ruoli
     */
    public Ruolo[] getRuoli() {
        return ruoli;
    }


    /**
     * Sets the ruoli value for this Ut.
     * 
     * @param ruoli
     */
    public void setRuoli(Ruolo[] ruoli) {
        this.ruoli = ruoli;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Ut)) return false;
        Ut other = (Ut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.amministratore==null && other.getAmministratore()==null) || 
             (this.amministratore!=null &&
              this.amministratore.equals(other.getAmministratore()))) &&
            ((this.codiceFiscale==null && other.getCodiceFiscale()==null) || 
             (this.codiceFiscale!=null &&
              this.codiceFiscale.equals(other.getCodiceFiscale()))) &&
            ((this.codiceProfiloUtente==null && other.getCodiceProfiloUtente()==null) || 
             (this.codiceProfiloUtente!=null &&
              this.codiceProfiloUtente.equals(other.getCodiceProfiloUtente()))) &&
            ((this.dataCreazione==null && other.getDataCreazione()==null) || 
             (this.dataCreazione!=null &&
              this.dataCreazione.equals(other.getDataCreazione()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.numeroRegistrazione==null && other.getNumeroRegistrazione()==null) || 
             (this.numeroRegistrazione!=null &&
              this.numeroRegistrazione.equals(other.getNumeroRegistrazione()))) &&
            ((this.ruoli==null && other.getRuoli()==null) || 
             (this.ruoli!=null &&
              java.util.Arrays.equals(this.ruoli, other.getRuoli())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAmministratore() != null) {
            _hashCode += getAmministratore().hashCode();
        }
        if (getCodiceFiscale() != null) {
            _hashCode += getCodiceFiscale().hashCode();
        }
        if (getCodiceProfiloUtente() != null) {
            _hashCode += getCodiceProfiloUtente().hashCode();
        }
        if (getDataCreazione() != null) {
            _hashCode += getDataCreazione().hashCode();
        }
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        if (getNumeroRegistrazione() != null) {
            _hashCode += getNumeroRegistrazione().hashCode();
        }
        if (getRuoli() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRuoli());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRuoli(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Ut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Ut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amministratore");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Amministratore"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceFiscale");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceFiscale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceProfiloUtente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceProfiloUtente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataCreazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataCreazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroRegistrazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumeroRegistrazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruoli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Ruoli"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Ruolo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Ruolo"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
