package it.inps.soa.classi;
/**
 * RegistroProtocolloResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class RegistroProtocolloResponse  extends Response  implements java.io.Serializable {
    private java.util.Calendar dataDa;

    private java.util.Calendar dataA;

    private java.lang.String denominazioneSede;

    private java.lang.String codiceSede;

    private ProtocolloEntrata[] protocolliEntrata;

    private ProtocolloUscita[] protocolliUscita;

    public RegistroProtocolloResponse() {
    }

    public RegistroProtocolloResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           java.util.Calendar dataDa,
           java.util.Calendar dataA,
           java.lang.String denominazioneSede,
           java.lang.String codiceSede,
           ProtocolloEntrata[] protocolliEntrata,
           ProtocolloUscita[] protocolliUscita) {
        super(
            codice,
            descrizione);
        this.dataDa = dataDa;
        this.dataA = dataA;
        this.denominazioneSede = denominazioneSede;
        this.codiceSede = codiceSede;
        this.protocolliEntrata = protocolliEntrata;
        this.protocolliUscita = protocolliUscita;
    }


    /**
     * Gets the dataDa value for this RegistroProtocolloResponse.
     * 
     * @return dataDa
     */
    public java.util.Calendar getDataDa() {
        return dataDa;
    }


    /**
     * Sets the dataDa value for this RegistroProtocolloResponse.
     * 
     * @param dataDa
     */
    public void setDataDa(java.util.Calendar dataDa) {
        this.dataDa = dataDa;
    }


    /**
     * Gets the dataA value for this RegistroProtocolloResponse.
     * 
     * @return dataA
     */
    public java.util.Calendar getDataA() {
        return dataA;
    }


    /**
     * Sets the dataA value for this RegistroProtocolloResponse.
     * 
     * @param dataA
     */
    public void setDataA(java.util.Calendar dataA) {
        this.dataA = dataA;
    }


    /**
     * Gets the denominazioneSede value for this RegistroProtocolloResponse.
     * 
     * @return denominazioneSede
     */
    public java.lang.String getDenominazioneSede() {
        return denominazioneSede;
    }


    /**
     * Sets the denominazioneSede value for this RegistroProtocolloResponse.
     * 
     * @param denominazioneSede
     */
    public void setDenominazioneSede(java.lang.String denominazioneSede) {
        this.denominazioneSede = denominazioneSede;
    }


    /**
     * Gets the codiceSede value for this RegistroProtocolloResponse.
     * 
     * @return codiceSede
     */
    public java.lang.String getCodiceSede() {
        return codiceSede;
    }


    /**
     * Sets the codiceSede value for this RegistroProtocolloResponse.
     * 
     * @param codiceSede
     */
    public void setCodiceSede(java.lang.String codiceSede) {
        this.codiceSede = codiceSede;
    }


    /**
     * Gets the protocolliEntrata value for this RegistroProtocolloResponse.
     * 
     * @return protocolliEntrata
     */
    public ProtocolloEntrata[] getProtocolliEntrata() {
        return protocolliEntrata;
    }


    /**
     * Sets the protocolliEntrata value for this RegistroProtocolloResponse.
     * 
     * @param protocolliEntrata
     */
    public void setProtocolliEntrata(ProtocolloEntrata[] protocolliEntrata) {
        this.protocolliEntrata = protocolliEntrata;
    }


    /**
     * Gets the protocolliUscita value for this RegistroProtocolloResponse.
     * 
     * @return protocolliUscita
     */
    public ProtocolloUscita[] getProtocolliUscita() {
        return protocolliUscita;
    }


    /**
     * Sets the protocolliUscita value for this RegistroProtocolloResponse.
     * 
     * @param protocolliUscita
     */
    public void setProtocolliUscita(ProtocolloUscita[] protocolliUscita) {
        this.protocolliUscita = protocolliUscita;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistroProtocolloResponse)) return false;
        RegistroProtocolloResponse other = (RegistroProtocolloResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.dataDa==null && other.getDataDa()==null) || 
             (this.dataDa!=null &&
              this.dataDa.equals(other.getDataDa()))) &&
            ((this.dataA==null && other.getDataA()==null) || 
             (this.dataA!=null &&
              this.dataA.equals(other.getDataA()))) &&
            ((this.denominazioneSede==null && other.getDenominazioneSede()==null) || 
             (this.denominazioneSede!=null &&
              this.denominazioneSede.equals(other.getDenominazioneSede()))) &&
            ((this.codiceSede==null && other.getCodiceSede()==null) || 
             (this.codiceSede!=null &&
              this.codiceSede.equals(other.getCodiceSede()))) &&
            ((this.protocolliEntrata==null && other.getProtocolliEntrata()==null) || 
             (this.protocolliEntrata!=null &&
              java.util.Arrays.equals(this.protocolliEntrata, other.getProtocolliEntrata()))) &&
            ((this.protocolliUscita==null && other.getProtocolliUscita()==null) || 
             (this.protocolliUscita!=null &&
              java.util.Arrays.equals(this.protocolliUscita, other.getProtocolliUscita())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDataDa() != null) {
            _hashCode += getDataDa().hashCode();
        }
        if (getDataA() != null) {
            _hashCode += getDataA().hashCode();
        }
        if (getDenominazioneSede() != null) {
            _hashCode += getDenominazioneSede().hashCode();
        }
        if (getCodiceSede() != null) {
            _hashCode += getCodiceSede().hashCode();
        }
        if (getProtocolliEntrata() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProtocolliEntrata());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProtocolliEntrata(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProtocolliUscita() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProtocolliUscita());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProtocolliUscita(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistroProtocolloResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "RegistroProtocolloResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataDa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataDa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("denominazioneSede");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DenominazioneSede"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceSede");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceSede"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolliEntrata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProtocolliEntrata"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "ProtocolloEntrata"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "ProtocolloEntrata"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolliUscita");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProtocolliUscita"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "ProtocolloUscita"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "ProtocolloUscita"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
