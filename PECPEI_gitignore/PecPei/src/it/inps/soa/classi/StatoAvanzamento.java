package it.inps.soa.classi;
/**
 * StatoAvanzamento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class StatoAvanzamento  extends NomeStatoAvanzamento  implements java.io.Serializable {
    private java.lang.String diagrammaDiStatoStandard;

    private java.lang.Boolean disabilitato;

    private java.lang.Boolean statoIniziale;

    private java.lang.Boolean statoLettura;

    public StatoAvanzamento() {
    }

    public StatoAvanzamento(
           java.lang.String codice,
           java.lang.String descrizione,
           java.lang.String diagrammaDiStatoStandard,
           java.lang.Boolean disabilitato,
           java.lang.Boolean statoIniziale,
           java.lang.Boolean statoLettura) {
        super(
            codice,
            descrizione);
        this.diagrammaDiStatoStandard = diagrammaDiStatoStandard;
        this.disabilitato = disabilitato;
        this.statoIniziale = statoIniziale;
        this.statoLettura = statoLettura;
    }


    /**
     * Gets the diagrammaDiStatoStandard value for this StatoAvanzamento.
     * 
     * @return diagrammaDiStatoStandard
     */
    public java.lang.String getDiagrammaDiStatoStandard() {
        return diagrammaDiStatoStandard;
    }


    /**
     * Sets the diagrammaDiStatoStandard value for this StatoAvanzamento.
     * 
     * @param diagrammaDiStatoStandard
     */
    public void setDiagrammaDiStatoStandard(java.lang.String diagrammaDiStatoStandard) {
        this.diagrammaDiStatoStandard = diagrammaDiStatoStandard;
    }


    /**
     * Gets the disabilitato value for this StatoAvanzamento.
     * 
     * @return disabilitato
     */
    public java.lang.Boolean getDisabilitato() {
        return disabilitato;
    }


    /**
     * Sets the disabilitato value for this StatoAvanzamento.
     * 
     * @param disabilitato
     */
    public void setDisabilitato(java.lang.Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }


    /**
     * Gets the statoIniziale value for this StatoAvanzamento.
     * 
     * @return statoIniziale
     */
    public java.lang.Boolean getStatoIniziale() {
        return statoIniziale;
    }


    /**
     * Sets the statoIniziale value for this StatoAvanzamento.
     * 
     * @param statoIniziale
     */
    public void setStatoIniziale(java.lang.Boolean statoIniziale) {
        this.statoIniziale = statoIniziale;
    }


    /**
     * Gets the statoLettura value for this StatoAvanzamento.
     * 
     * @return statoLettura
     */
    public java.lang.Boolean getStatoLettura() {
        return statoLettura;
    }


    /**
     * Sets the statoLettura value for this StatoAvanzamento.
     * 
     * @param statoLettura
     */
    public void setStatoLettura(java.lang.Boolean statoLettura) {
        this.statoLettura = statoLettura;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StatoAvanzamento)) return false;
        StatoAvanzamento other = (StatoAvanzamento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.diagrammaDiStatoStandard==null && other.getDiagrammaDiStatoStandard()==null) || 
             (this.diagrammaDiStatoStandard!=null &&
              this.diagrammaDiStatoStandard.equals(other.getDiagrammaDiStatoStandard()))) &&
            ((this.disabilitato==null && other.getDisabilitato()==null) || 
             (this.disabilitato!=null &&
              this.disabilitato.equals(other.getDisabilitato()))) &&
            ((this.statoIniziale==null && other.getStatoIniziale()==null) || 
             (this.statoIniziale!=null &&
              this.statoIniziale.equals(other.getStatoIniziale()))) &&
            ((this.statoLettura==null && other.getStatoLettura()==null) || 
             (this.statoLettura!=null &&
              this.statoLettura.equals(other.getStatoLettura())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDiagrammaDiStatoStandard() != null) {
            _hashCode += getDiagrammaDiStatoStandard().hashCode();
        }
        if (getDisabilitato() != null) {
            _hashCode += getDisabilitato().hashCode();
        }
        if (getStatoIniziale() != null) {
            _hashCode += getStatoIniziale().hashCode();
        }
        if (getStatoLettura() != null) {
            _hashCode += getStatoLettura().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StatoAvanzamento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "StatoAvanzamento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diagrammaDiStatoStandard");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DiagrammaDiStatoStandard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Disabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statoIniziale");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StatoIniziale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statoLettura");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StatoLettura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
