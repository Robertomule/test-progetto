package it.inps.soa.classi;
/**
 * OrganizzazioniResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class OrganizzazioniResponse  extends Response  implements java.io.Serializable {
    private Organizzazione[] organizzazioni;

    public OrganizzazioniResponse() {
    }

    public OrganizzazioniResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           Organizzazione[] organizzazioni) {
        super(
            codice,
            descrizione);
        this.organizzazioni = organizzazioni;
    }


    /**
     * Gets the organizzazioni value for this OrganizzazioniResponse.
     * 
     * @return organizzazioni
     */
    public Organizzazione[] getOrganizzazioni() {
        return organizzazioni;
    }


    /**
     * Sets the organizzazioni value for this OrganizzazioniResponse.
     * 
     * @param organizzazioni
     */
    public void setOrganizzazioni(Organizzazione[] organizzazioni) {
        this.organizzazioni = organizzazioni;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrganizzazioniResponse)) return false;
        OrganizzazioniResponse other = (OrganizzazioniResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.organizzazioni==null && other.getOrganizzazioni()==null) || 
             (this.organizzazioni!=null &&
              java.util.Arrays.equals(this.organizzazioni, other.getOrganizzazioni())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getOrganizzazioni() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrganizzazioni());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrganizzazioni(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrganizzazioniResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "OrganizzazioniResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organizzazioni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Organizzazioni"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Organizzazione"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Organizzazione"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
