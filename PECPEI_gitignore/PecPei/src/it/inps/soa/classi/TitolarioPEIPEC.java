package it.inps.soa.classi;
/**
 * TitolarioPEIPEC.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class TitolarioPEIPEC  implements java.io.Serializable {
    private java.lang.String codiceTitolario;

    private java.lang.String denominazione;

    private java.lang.String tipo_Titolario;

    private java.lang.String cod2;

    private java.lang.String cod3;

    private java.lang.String cod4;

    private java.lang.String desc2;

    private java.lang.String desc3;

    private java.lang.String desc4;

    private java.lang.String pad1;

    private java.lang.String pad2;

    private java.lang.String pad3;

    private java.lang.String tip2;

    private java.lang.String tip3;

    private java.lang.String tip4;

    public TitolarioPEIPEC() {
    }

    public TitolarioPEIPEC(
           java.lang.String codiceTitolario,
           java.lang.String denominazione,
           java.lang.String tipo_Titolario,
           java.lang.String cod2,
           java.lang.String cod3,
           java.lang.String cod4,
           java.lang.String desc2,
           java.lang.String desc3,
           java.lang.String desc4,
           java.lang.String pad1,
           java.lang.String pad2,
           java.lang.String pad3,
           java.lang.String tip2,
           java.lang.String tip3,
           java.lang.String tip4) {
           this.codiceTitolario = codiceTitolario;
           this.denominazione = denominazione;
           this.tipo_Titolario = tipo_Titolario;
           this.cod2 = cod2;
           this.cod3 = cod3;
           this.cod4 = cod4;
           this.desc2 = desc2;
           this.desc3 = desc3;
           this.desc4 = desc4;
           this.pad1 = pad1;
           this.pad2 = pad2;
           this.pad3 = pad3;
           this.tip2 = tip2;
           this.tip3 = tip3;
           this.tip4 = tip4;
    }


    /**
     * Gets the codiceTitolario value for this TitolarioPEIPEC.
     * 
     * @return codiceTitolario
     */
    public java.lang.String getCodiceTitolario() {
        return codiceTitolario;
    }


    /**
     * Sets the codiceTitolario value for this TitolarioPEIPEC.
     * 
     * @param codiceTitolario
     */
    public void setCodiceTitolario(java.lang.String codiceTitolario) {
        this.codiceTitolario = codiceTitolario;
    }


    /**
     * Gets the denominazione value for this TitolarioPEIPEC.
     * 
     * @return denominazione
     */
    public java.lang.String getDenominazione() {
        return denominazione;
    }


    /**
     * Sets the denominazione value for this TitolarioPEIPEC.
     * 
     * @param denominazione
     */
    public void setDenominazione(java.lang.String denominazione) {
        this.denominazione = denominazione;
    }


    /**
     * Gets the tipo_Titolario value for this TitolarioPEIPEC.
     * 
     * @return tipo_Titolario
     */
    public java.lang.String getTipo_Titolario() {
        return tipo_Titolario;
    }


    /**
     * Sets the tipo_Titolario value for this TitolarioPEIPEC.
     * 
     * @param tipo_Titolario
     */
    public void setTipo_Titolario(java.lang.String tipo_Titolario) {
        this.tipo_Titolario = tipo_Titolario;
    }


    /**
     * Gets the cod2 value for this TitolarioPEIPEC.
     * 
     * @return cod2
     */
    public java.lang.String getCod2() {
        return cod2;
    }


    /**
     * Sets the cod2 value for this TitolarioPEIPEC.
     * 
     * @param cod2
     */
    public void setCod2(java.lang.String cod2) {
        this.cod2 = cod2;
    }


    /**
     * Gets the cod3 value for this TitolarioPEIPEC.
     * 
     * @return cod3
     */
    public java.lang.String getCod3() {
        return cod3;
    }


    /**
     * Sets the cod3 value for this TitolarioPEIPEC.
     * 
     * @param cod3
     */
    public void setCod3(java.lang.String cod3) {
        this.cod3 = cod3;
    }


    /**
     * Gets the cod4 value for this TitolarioPEIPEC.
     * 
     * @return cod4
     */
    public java.lang.String getCod4() {
        return cod4;
    }


    /**
     * Sets the cod4 value for this TitolarioPEIPEC.
     * 
     * @param cod4
     */
    public void setCod4(java.lang.String cod4) {
        this.cod4 = cod4;
    }


    /**
     * Gets the desc2 value for this TitolarioPEIPEC.
     * 
     * @return desc2
     */
    public java.lang.String getDesc2() {
        return desc2;
    }


    /**
     * Sets the desc2 value for this TitolarioPEIPEC.
     * 
     * @param desc2
     */
    public void setDesc2(java.lang.String desc2) {
        this.desc2 = desc2;
    }


    /**
     * Gets the desc3 value for this TitolarioPEIPEC.
     * 
     * @return desc3
     */
    public java.lang.String getDesc3() {
        return desc3;
    }


    /**
     * Sets the desc3 value for this TitolarioPEIPEC.
     * 
     * @param desc3
     */
    public void setDesc3(java.lang.String desc3) {
        this.desc3 = desc3;
    }


    /**
     * Gets the desc4 value for this TitolarioPEIPEC.
     * 
     * @return desc4
     */
    public java.lang.String getDesc4() {
        return desc4;
    }


    /**
     * Sets the desc4 value for this TitolarioPEIPEC.
     * 
     * @param desc4
     */
    public void setDesc4(java.lang.String desc4) {
        this.desc4 = desc4;
    }


    /**
     * Gets the pad1 value for this TitolarioPEIPEC.
     * 
     * @return pad1
     */
    public java.lang.String getPad1() {
        return pad1;
    }


    /**
     * Sets the pad1 value for this TitolarioPEIPEC.
     * 
     * @param pad1
     */
    public void setPad1(java.lang.String pad1) {
        this.pad1 = pad1;
    }


    /**
     * Gets the pad2 value for this TitolarioPEIPEC.
     * 
     * @return pad2
     */
    public java.lang.String getPad2() {
        return pad2;
    }


    /**
     * Sets the pad2 value for this TitolarioPEIPEC.
     * 
     * @param pad2
     */
    public void setPad2(java.lang.String pad2) {
        this.pad2 = pad2;
    }


    /**
     * Gets the pad3 value for this TitolarioPEIPEC.
     * 
     * @return pad3
     */
    public java.lang.String getPad3() {
        return pad3;
    }


    /**
     * Sets the pad3 value for this TitolarioPEIPEC.
     * 
     * @param pad3
     */
    public void setPad3(java.lang.String pad3) {
        this.pad3 = pad3;
    }


    /**
     * Gets the tip2 value for this TitolarioPEIPEC.
     * 
     * @return tip2
     */
    public java.lang.String getTip2() {
        return tip2;
    }


    /**
     * Sets the tip2 value for this TitolarioPEIPEC.
     * 
     * @param tip2
     */
    public void setTip2(java.lang.String tip2) {
        this.tip2 = tip2;
    }


    /**
     * Gets the tip3 value for this TitolarioPEIPEC.
     * 
     * @return tip3
     */
    public java.lang.String getTip3() {
        return tip3;
    }


    /**
     * Sets the tip3 value for this TitolarioPEIPEC.
     * 
     * @param tip3
     */
    public void setTip3(java.lang.String tip3) {
        this.tip3 = tip3;
    }


    /**
     * Gets the tip4 value for this TitolarioPEIPEC.
     * 
     * @return tip4
     */
    public java.lang.String getTip4() {
        return tip4;
    }


    /**
     * Sets the tip4 value for this TitolarioPEIPEC.
     * 
     * @param tip4
     */
    public void setTip4(java.lang.String tip4) {
        this.tip4 = tip4;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitolarioPEIPEC)) return false;
        TitolarioPEIPEC other = (TitolarioPEIPEC) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codiceTitolario==null && other.getCodiceTitolario()==null) || 
             (this.codiceTitolario!=null &&
              this.codiceTitolario.equals(other.getCodiceTitolario()))) &&
            ((this.denominazione==null && other.getDenominazione()==null) || 
             (this.denominazione!=null &&
              this.denominazione.equals(other.getDenominazione()))) &&
            ((this.tipo_Titolario==null && other.getTipo_Titolario()==null) || 
             (this.tipo_Titolario!=null &&
              this.tipo_Titolario.equals(other.getTipo_Titolario()))) &&
            ((this.cod2==null && other.getCod2()==null) || 
             (this.cod2!=null &&
              this.cod2.equals(other.getCod2()))) &&
            ((this.cod3==null && other.getCod3()==null) || 
             (this.cod3!=null &&
              this.cod3.equals(other.getCod3()))) &&
            ((this.cod4==null && other.getCod4()==null) || 
             (this.cod4!=null &&
              this.cod4.equals(other.getCod4()))) &&
            ((this.desc2==null && other.getDesc2()==null) || 
             (this.desc2!=null &&
              this.desc2.equals(other.getDesc2()))) &&
            ((this.desc3==null && other.getDesc3()==null) || 
             (this.desc3!=null &&
              this.desc3.equals(other.getDesc3()))) &&
            ((this.desc4==null && other.getDesc4()==null) || 
             (this.desc4!=null &&
              this.desc4.equals(other.getDesc4()))) &&
            ((this.pad1==null && other.getPad1()==null) || 
             (this.pad1!=null &&
              this.pad1.equals(other.getPad1()))) &&
            ((this.pad2==null && other.getPad2()==null) || 
             (this.pad2!=null &&
              this.pad2.equals(other.getPad2()))) &&
            ((this.pad3==null && other.getPad3()==null) || 
             (this.pad3!=null &&
              this.pad3.equals(other.getPad3()))) &&
            ((this.tip2==null && other.getTip2()==null) || 
             (this.tip2!=null &&
              this.tip2.equals(other.getTip2()))) &&
            ((this.tip3==null && other.getTip3()==null) || 
             (this.tip3!=null &&
              this.tip3.equals(other.getTip3()))) &&
            ((this.tip4==null && other.getTip4()==null) || 
             (this.tip4!=null &&
              this.tip4.equals(other.getTip4())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodiceTitolario() != null) {
            _hashCode += getCodiceTitolario().hashCode();
        }
        if (getDenominazione() != null) {
            _hashCode += getDenominazione().hashCode();
        }
        if (getTipo_Titolario() != null) {
            _hashCode += getTipo_Titolario().hashCode();
        }
        if (getCod2() != null) {
            _hashCode += getCod2().hashCode();
        }
        if (getCod3() != null) {
            _hashCode += getCod3().hashCode();
        }
        if (getCod4() != null) {
            _hashCode += getCod4().hashCode();
        }
        if (getDesc2() != null) {
            _hashCode += getDesc2().hashCode();
        }
        if (getDesc3() != null) {
            _hashCode += getDesc3().hashCode();
        }
        if (getDesc4() != null) {
            _hashCode += getDesc4().hashCode();
        }
        if (getPad1() != null) {
            _hashCode += getPad1().hashCode();
        }
        if (getPad2() != null) {
            _hashCode += getPad2().hashCode();
        }
        if (getPad3() != null) {
            _hashCode += getPad3().hashCode();
        }
        if (getTip2() != null) {
            _hashCode += getTip2().hashCode();
        }
        if (getTip3() != null) {
            _hashCode += getTip3().hashCode();
        }
        if (getTip4() != null) {
            _hashCode += getTip4().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitolarioPEIPEC.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "TitolarioPEIPEC"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceTitolario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceTitolario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("denominazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Denominazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo_Titolario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Tipo_Titolario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cod2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cod2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cod3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cod3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cod4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cod4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desc2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desc3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desc4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pad1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pad1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pad2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pad2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pad3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pad3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tip2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tip2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tip3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tip3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tip4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tip4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
