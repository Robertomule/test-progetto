package it.inps.soa.classi;
/**
 * Registro.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Registro  implements java.io.Serializable {
    private java.lang.String codice;

    private java.util.Calendar dataApertura;

    private java.util.Calendar dataChiusura;

    private java.lang.String descrizione;

    private java.lang.Boolean disabilitato;

    private java.lang.String organizzazione;

    private java.lang.Integer progressivoProtocolloLen;

    public Registro() {
    }

    public Registro(
           java.lang.String codice,
           java.util.Calendar dataApertura,
           java.util.Calendar dataChiusura,
           java.lang.String descrizione,
           java.lang.Boolean disabilitato,
           java.lang.String organizzazione,
           java.lang.Integer progressivoProtocolloLen) {
           this.codice = codice;
           this.dataApertura = dataApertura;
           this.dataChiusura = dataChiusura;
           this.descrizione = descrizione;
           this.disabilitato = disabilitato;
           this.organizzazione = organizzazione;
           this.progressivoProtocolloLen = progressivoProtocolloLen;
    }


    /**
     * Gets the codice value for this Registro.
     * 
     * @return codice
     */
    public java.lang.String getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this Registro.
     * 
     * @param codice
     */
    public void setCodice(java.lang.String codice) {
        this.codice = codice;
    }


    /**
     * Gets the dataApertura value for this Registro.
     * 
     * @return dataApertura
     */
    public java.util.Calendar getDataApertura() {
        return dataApertura;
    }


    /**
     * Sets the dataApertura value for this Registro.
     * 
     * @param dataApertura
     */
    public void setDataApertura(java.util.Calendar dataApertura) {
        this.dataApertura = dataApertura;
    }


    /**
     * Gets the dataChiusura value for this Registro.
     * 
     * @return dataChiusura
     */
    public java.util.Calendar getDataChiusura() {
        return dataChiusura;
    }


    /**
     * Sets the dataChiusura value for this Registro.
     * 
     * @param dataChiusura
     */
    public void setDataChiusura(java.util.Calendar dataChiusura) {
        this.dataChiusura = dataChiusura;
    }


    /**
     * Gets the descrizione value for this Registro.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this Registro.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the disabilitato value for this Registro.
     * 
     * @return disabilitato
     */
    public java.lang.Boolean getDisabilitato() {
        return disabilitato;
    }


    /**
     * Sets the disabilitato value for this Registro.
     * 
     * @param disabilitato
     */
    public void setDisabilitato(java.lang.Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }


    /**
     * Gets the organizzazione value for this Registro.
     * 
     * @return organizzazione
     */
    public java.lang.String getOrganizzazione() {
        return organizzazione;
    }


    /**
     * Sets the organizzazione value for this Registro.
     * 
     * @param organizzazione
     */
    public void setOrganizzazione(java.lang.String organizzazione) {
        this.organizzazione = organizzazione;
    }


    /**
     * Gets the progressivoProtocolloLen value for this Registro.
     * 
     * @return progressivoProtocolloLen
     */
    public java.lang.Integer getProgressivoProtocolloLen() {
        return progressivoProtocolloLen;
    }


    /**
     * Sets the progressivoProtocolloLen value for this Registro.
     * 
     * @param progressivoProtocolloLen
     */
    public void setProgressivoProtocolloLen(java.lang.Integer progressivoProtocolloLen) {
        this.progressivoProtocolloLen = progressivoProtocolloLen;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Registro)) return false;
        Registro other = (Registro) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.dataApertura==null && other.getDataApertura()==null) || 
             (this.dataApertura!=null &&
              this.dataApertura.equals(other.getDataApertura()))) &&
            ((this.dataChiusura==null && other.getDataChiusura()==null) || 
             (this.dataChiusura!=null &&
              this.dataChiusura.equals(other.getDataChiusura()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.disabilitato==null && other.getDisabilitato()==null) || 
             (this.disabilitato!=null &&
              this.disabilitato.equals(other.getDisabilitato()))) &&
            ((this.organizzazione==null && other.getOrganizzazione()==null) || 
             (this.organizzazione!=null &&
              this.organizzazione.equals(other.getOrganizzazione()))) &&
            ((this.progressivoProtocolloLen==null && other.getProgressivoProtocolloLen()==null) || 
             (this.progressivoProtocolloLen!=null &&
              this.progressivoProtocolloLen.equals(other.getProgressivoProtocolloLen())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getDataApertura() != null) {
            _hashCode += getDataApertura().hashCode();
        }
        if (getDataChiusura() != null) {
            _hashCode += getDataChiusura().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getDisabilitato() != null) {
            _hashCode += getDisabilitato().hashCode();
        }
        if (getOrganizzazione() != null) {
            _hashCode += getOrganizzazione().hashCode();
        }
        if (getProgressivoProtocolloLen() != null) {
            _hashCode += getProgressivoProtocolloLen().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Registro.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Registro"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataApertura");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataApertura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataChiusura");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataChiusura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Disabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organizzazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Organizzazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("progressivoProtocolloLen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProgressivoProtocolloLen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
