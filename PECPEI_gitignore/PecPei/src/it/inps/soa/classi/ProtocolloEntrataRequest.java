package it.inps.soa.classi;
/**
 * ProtocolloEntrataRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class ProtocolloEntrataRequest  extends Protocollo  implements java.io.Serializable {
    private Mittente mittente;

    public ProtocolloEntrataRequest() {
    }

    public ProtocolloEntrataRequest(
           DocumentoPrincipale documento,
           Allegato[] allegati,
           SoggettoAfferente[] soggettiAfferenti,
           java.lang.String registrazioneProvenienza,
           java.lang.String primaRegistrazione,
           java.util.Calendar dataInvio,
           java.util.Calendar dataArrivo,
           Mittente mittente) {
        super(
            documento,
            allegati,
            soggettiAfferenti,
            registrazioneProvenienza,
            primaRegistrazione,
            dataInvio,
            dataArrivo);
        this.mittente = mittente;
    }


    /**
     * Gets the mittente value for this ProtocolloEntrataRequest.
     * 
     * @return mittente
     */
    public Mittente getMittente() {
        return mittente;
    }


    /**
     * Sets the mittente value for this ProtocolloEntrataRequest.
     * 
     * @param mittente
     */
    public void setMittente(Mittente mittente) {
        this.mittente = mittente;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProtocolloEntrataRequest)) return false;
        ProtocolloEntrataRequest other = (ProtocolloEntrataRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.mittente==null && other.getMittente()==null) || 
             (this.mittente!=null &&
              this.mittente.equals(other.getMittente())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMittente() != null) {
            _hashCode += getMittente().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProtocolloEntrataRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "ProtocolloEntrataRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mittente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Mittente"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Mittente"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
