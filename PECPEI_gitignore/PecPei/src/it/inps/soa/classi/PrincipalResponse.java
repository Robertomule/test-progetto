package it.inps.soa.classi;
/**
 * PrincipalResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class PrincipalResponse  extends Response  implements java.io.Serializable {
    private Principal[] principals;

    public PrincipalResponse() {
    }

    public PrincipalResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           Principal[] principals) {
        super(
            codice,
            descrizione);
        this.principals = principals;
    }


    /**
     * Gets the principals value for this PrincipalResponse.
     * 
     * @return principals
     */
    public Principal[] getPrincipals() {
        return principals;
    }


    /**
     * Sets the principals value for this PrincipalResponse.
     * 
     * @param principals
     */
    public void setPrincipals(Principal[] principals) {
        this.principals = principals;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PrincipalResponse)) return false;
        PrincipalResponse other = (PrincipalResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.principals==null && other.getPrincipals()==null) || 
             (this.principals!=null &&
              java.util.Arrays.equals(this.principals, other.getPrincipals())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPrincipals() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPrincipals());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPrincipals(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PrincipalResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "PrincipalResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("principals");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Principals"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Principal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Principal"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
