package it.inps.soa.classi;
/**
 * PoliSpecializzatiResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class PoliSpecializzatiResponse  extends Response  implements java.io.Serializable {
    private PoloSpecializzato[] poliSpecializzati;

    public PoliSpecializzatiResponse() {
    }

    public PoliSpecializzatiResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           PoloSpecializzato[] poliSpecializzati) {
        super(
            codice,
            descrizione);
        this.poliSpecializzati = poliSpecializzati;
    }


    /**
     * Gets the poliSpecializzati value for this PoliSpecializzatiResponse.
     * 
     * @return poliSpecializzati
     */
    public PoloSpecializzato[] getPoliSpecializzati() {
        return poliSpecializzati;
    }


    /**
     * Sets the poliSpecializzati value for this PoliSpecializzatiResponse.
     * 
     * @param poliSpecializzati
     */
    public void setPoliSpecializzati(PoloSpecializzato[] poliSpecializzati) {
        this.poliSpecializzati = poliSpecializzati;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PoliSpecializzatiResponse)) return false;
        PoliSpecializzatiResponse other = (PoliSpecializzatiResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.poliSpecializzati==null && other.getPoliSpecializzati()==null) || 
             (this.poliSpecializzati!=null &&
              java.util.Arrays.equals(this.poliSpecializzati, other.getPoliSpecializzati())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPoliSpecializzati() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPoliSpecializzati());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPoliSpecializzati(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PoliSpecializzatiResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "PoliSpecializzatiResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("poliSpecializzati");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PoliSpecializzati"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "PoloSpecializzato"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "PoloSpecializzato"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
