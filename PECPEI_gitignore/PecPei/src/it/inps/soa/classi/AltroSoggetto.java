package it.inps.soa.classi;
/**
 * AltroSoggetto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class AltroSoggetto  extends AddressBook  implements java.io.Serializable {
    private java.lang.String codice;

    private java.lang.String nome;

    private java.lang.String nomeSocietario;

    private java.lang.String numeroRegistrazione;

    private java.lang.String partitaIva;

    private java.lang.String ragioneSociale;

    public AltroSoggetto() {
    }

    public AltroSoggetto(
           java.lang.String CAP,
           java.lang.String citta,
           java.lang.String country,
           java.util.Calendar dataCreazione,
           java.lang.Boolean disabilitato,
           java.lang.String EMailCertificata,
           java.lang.String email,
           java.lang.String fax,
           java.lang.Integer id,
           java.lang.Integer idParent,
           java.lang.String indirizzo,
           java.lang.String nazione,
           java.lang.String note,
           java.lang.Integer notificaPreferita,
           java.lang.Integer numeroTelPreferito,
           org.apache.axis.types.Duration orarioPreferito,
           java.lang.String provincia,
           java.lang.String regione,
           java.lang.String telefono,
           java.lang.String telefonoCasa,
           java.lang.String telefonoUfficio,
           java.lang.String tipoSoggetto,
           java.lang.String codice,
           java.lang.String nome,
           java.lang.String nomeSocietario,
           java.lang.String numeroRegistrazione,
           java.lang.String partitaIva,
           java.lang.String ragioneSociale) {
        super(
            CAP,
            citta,
            country,
            dataCreazione,
            disabilitato,
            EMailCertificata,
            email,
            fax,
            id,
            idParent,
            indirizzo,
            nazione,
            note,
            notificaPreferita,
            numeroTelPreferito,
            orarioPreferito,
            provincia,
            regione,
            telefono,
            telefonoCasa,
            telefonoUfficio,
            tipoSoggetto);
        this.codice = codice;
        this.nome = nome;
        this.nomeSocietario = nomeSocietario;
        this.numeroRegistrazione = numeroRegistrazione;
        this.partitaIva = partitaIva;
        this.ragioneSociale = ragioneSociale;
    }


    /**
     * Gets the codice value for this AltroSoggetto.
     * 
     * @return codice
     */
    public java.lang.String getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this AltroSoggetto.
     * 
     * @param codice
     */
    public void setCodice(java.lang.String codice) {
        this.codice = codice;
    }


    /**
     * Gets the nome value for this AltroSoggetto.
     * 
     * @return nome
     */
    public java.lang.String getNome() {
        return nome;
    }


    /**
     * Sets the nome value for this AltroSoggetto.
     * 
     * @param nome
     */
    public void setNome(java.lang.String nome) {
        this.nome = nome;
    }


    /**
     * Gets the nomeSocietario value for this AltroSoggetto.
     * 
     * @return nomeSocietario
     */
    public java.lang.String getNomeSocietario() {
        return nomeSocietario;
    }


    /**
     * Sets the nomeSocietario value for this AltroSoggetto.
     * 
     * @param nomeSocietario
     */
    public void setNomeSocietario(java.lang.String nomeSocietario) {
        this.nomeSocietario = nomeSocietario;
    }


    /**
     * Gets the numeroRegistrazione value for this AltroSoggetto.
     * 
     * @return numeroRegistrazione
     */
    public java.lang.String getNumeroRegistrazione() {
        return numeroRegistrazione;
    }


    /**
     * Sets the numeroRegistrazione value for this AltroSoggetto.
     * 
     * @param numeroRegistrazione
     */
    public void setNumeroRegistrazione(java.lang.String numeroRegistrazione) {
        this.numeroRegistrazione = numeroRegistrazione;
    }


    /**
     * Gets the partitaIva value for this AltroSoggetto.
     * 
     * @return partitaIva
     */
    public java.lang.String getPartitaIva() {
        return partitaIva;
    }


    /**
     * Sets the partitaIva value for this AltroSoggetto.
     * 
     * @param partitaIva
     */
    public void setPartitaIva(java.lang.String partitaIva) {
        this.partitaIva = partitaIva;
    }


    /**
     * Gets the ragioneSociale value for this AltroSoggetto.
     * 
     * @return ragioneSociale
     */
    public java.lang.String getRagioneSociale() {
        return ragioneSociale;
    }


    /**
     * Sets the ragioneSociale value for this AltroSoggetto.
     * 
     * @param ragioneSociale
     */
    public void setRagioneSociale(java.lang.String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AltroSoggetto)) return false;
        AltroSoggetto other = (AltroSoggetto) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.nome==null && other.getNome()==null) || 
             (this.nome!=null &&
              this.nome.equals(other.getNome()))) &&
            ((this.nomeSocietario==null && other.getNomeSocietario()==null) || 
             (this.nomeSocietario!=null &&
              this.nomeSocietario.equals(other.getNomeSocietario()))) &&
            ((this.numeroRegistrazione==null && other.getNumeroRegistrazione()==null) || 
             (this.numeroRegistrazione!=null &&
              this.numeroRegistrazione.equals(other.getNumeroRegistrazione()))) &&
            ((this.partitaIva==null && other.getPartitaIva()==null) || 
             (this.partitaIva!=null &&
              this.partitaIva.equals(other.getPartitaIva()))) &&
            ((this.ragioneSociale==null && other.getRagioneSociale()==null) || 
             (this.ragioneSociale!=null &&
              this.ragioneSociale.equals(other.getRagioneSociale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getNome() != null) {
            _hashCode += getNome().hashCode();
        }
        if (getNomeSocietario() != null) {
            _hashCode += getNomeSocietario().hashCode();
        }
        if (getNumeroRegistrazione() != null) {
            _hashCode += getNumeroRegistrazione().hashCode();
        }
        if (getPartitaIva() != null) {
            _hashCode += getPartitaIva().hashCode();
        }
        if (getRagioneSociale() != null) {
            _hashCode += getRagioneSociale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AltroSoggetto.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "AltroSoggetto"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nome");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Nome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeSocietario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NomeSocietario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroRegistrazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumeroRegistrazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partitaIva");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PartitaIva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ragioneSociale");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RagioneSociale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
