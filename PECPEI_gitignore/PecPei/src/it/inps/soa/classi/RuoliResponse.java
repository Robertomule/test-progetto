package it.inps.soa.classi;
/**
 * RuoliResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class RuoliResponse  extends Response  implements java.io.Serializable {
    private Ruolo[] ruoli;

    public RuoliResponse() {
    }

    public RuoliResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           Ruolo[] ruoli) {
        super(
            codice,
            descrizione);
        this.ruoli = ruoli;
    }


    /**
     * Gets the ruoli value for this RuoliResponse.
     * 
     * @return ruoli
     */
    public Ruolo[] getRuoli() {
        return ruoli;
    }


    /**
     * Sets the ruoli value for this RuoliResponse.
     * 
     * @param ruoli
     */
    public void setRuoli(Ruolo[] ruoli) {
        this.ruoli = ruoli;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RuoliResponse)) return false;
        RuoliResponse other = (RuoliResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ruoli==null && other.getRuoli()==null) || 
             (this.ruoli!=null &&
              java.util.Arrays.equals(this.ruoli, other.getRuoli())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRuoli() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRuoli());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRuoli(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RuoliResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "RuoliResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruoli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Ruoli"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Ruolo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Ruolo"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
