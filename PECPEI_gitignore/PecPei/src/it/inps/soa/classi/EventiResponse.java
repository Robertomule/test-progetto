package it.inps.soa.classi;
/**
 * EventiResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class EventiResponse  extends Response  implements java.io.Serializable {
    private Evento[] tipiEventi;

    public EventiResponse() {
    }

    public EventiResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           Evento[] tipiEventi) {
        super(
            codice,
            descrizione);
        this.tipiEventi = tipiEventi;
    }


    /**
     * Gets the tipiEventi value for this EventiResponse.
     * 
     * @return tipiEventi
     */
    public Evento[] getTipiEventi() {
        return tipiEventi;
    }


    /**
     * Sets the tipiEventi value for this EventiResponse.
     * 
     * @param tipiEventi
     */
    public void setTipiEventi(Evento[] tipiEventi) {
        this.tipiEventi = tipiEventi;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EventiResponse)) return false;
        EventiResponse other = (EventiResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.tipiEventi==null && other.getTipiEventi()==null) || 
             (this.tipiEventi!=null &&
              java.util.Arrays.equals(this.tipiEventi, other.getTipiEventi())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTipiEventi() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipiEventi());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipiEventi(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EventiResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "EventiResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipiEventi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TipiEventi"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Evento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Evento"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
