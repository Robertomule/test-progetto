package it.inps.soa.classi;
/**
 * Ruolo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Ruolo  implements java.io.Serializable {
    private java.lang.String codice;

    private java.lang.String codiceOrganizzazione;

    private java.lang.String codiceUnitaOrganizzativa;

    private java.lang.String descrizione;

    private java.lang.Boolean disabilitato;

    private java.lang.Boolean responsabile;

    private java.lang.Boolean ruoloUtenteDisabilitato;

    private java.lang.String tipo;

    private UserExt[] utenti;

    public Ruolo() {
    }

    public Ruolo(
           java.lang.String codice,
           java.lang.String codiceOrganizzazione,
           java.lang.String codiceUnitaOrganizzativa,
           java.lang.String descrizione,
           java.lang.Boolean disabilitato,
           java.lang.Boolean responsabile,
           java.lang.Boolean ruoloUtenteDisabilitato,
           java.lang.String tipo,
           UserExt[] utenti) {
           this.codice = codice;
           this.codiceOrganizzazione = codiceOrganizzazione;
           this.codiceUnitaOrganizzativa = codiceUnitaOrganizzativa;
           this.descrizione = descrizione;
           this.disabilitato = disabilitato;
           this.responsabile = responsabile;
           this.ruoloUtenteDisabilitato = ruoloUtenteDisabilitato;
           this.tipo = tipo;
           this.utenti = utenti;
    }


    /**
     * Gets the codice value for this Ruolo.
     * 
     * @return codice
     */
    public java.lang.String getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this Ruolo.
     * 
     * @param codice
     */
    public void setCodice(java.lang.String codice) {
        this.codice = codice;
    }


    /**
     * Gets the codiceOrganizzazione value for this Ruolo.
     * 
     * @return codiceOrganizzazione
     */
    public java.lang.String getCodiceOrganizzazione() {
        return codiceOrganizzazione;
    }


    /**
     * Sets the codiceOrganizzazione value for this Ruolo.
     * 
     * @param codiceOrganizzazione
     */
    public void setCodiceOrganizzazione(java.lang.String codiceOrganizzazione) {
        this.codiceOrganizzazione = codiceOrganizzazione;
    }


    /**
     * Gets the codiceUnitaOrganizzativa value for this Ruolo.
     * 
     * @return codiceUnitaOrganizzativa
     */
    public java.lang.String getCodiceUnitaOrganizzativa() {
        return codiceUnitaOrganizzativa;
    }


    /**
     * Sets the codiceUnitaOrganizzativa value for this Ruolo.
     * 
     * @param codiceUnitaOrganizzativa
     */
    public void setCodiceUnitaOrganizzativa(java.lang.String codiceUnitaOrganizzativa) {
        this.codiceUnitaOrganizzativa = codiceUnitaOrganizzativa;
    }


    /**
     * Gets the descrizione value for this Ruolo.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this Ruolo.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the disabilitato value for this Ruolo.
     * 
     * @return disabilitato
     */
    public java.lang.Boolean getDisabilitato() {
        return disabilitato;
    }


    /**
     * Sets the disabilitato value for this Ruolo.
     * 
     * @param disabilitato
     */
    public void setDisabilitato(java.lang.Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }


    /**
     * Gets the responsabile value for this Ruolo.
     * 
     * @return responsabile
     */
    public java.lang.Boolean getResponsabile() {
        return responsabile;
    }


    /**
     * Sets the responsabile value for this Ruolo.
     * 
     * @param responsabile
     */
    public void setResponsabile(java.lang.Boolean responsabile) {
        this.responsabile = responsabile;
    }


    /**
     * Gets the ruoloUtenteDisabilitato value for this Ruolo.
     * 
     * @return ruoloUtenteDisabilitato
     */
    public java.lang.Boolean getRuoloUtenteDisabilitato() {
        return ruoloUtenteDisabilitato;
    }


    /**
     * Sets the ruoloUtenteDisabilitato value for this Ruolo.
     * 
     * @param ruoloUtenteDisabilitato
     */
    public void setRuoloUtenteDisabilitato(java.lang.Boolean ruoloUtenteDisabilitato) {
        this.ruoloUtenteDisabilitato = ruoloUtenteDisabilitato;
    }


    /**
     * Gets the tipo value for this Ruolo.
     * 
     * @return tipo
     */
    public java.lang.String getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this Ruolo.
     * 
     * @param tipo
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the utenti value for this Ruolo.
     * 
     * @return utenti
     */
    public UserExt[] getUtenti() {
        return utenti;
    }


    /**
     * Sets the utenti value for this Ruolo.
     * 
     * @param utenti
     */
    public void setUtenti(UserExt[] utenti) {
        this.utenti = utenti;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Ruolo)) return false;
        Ruolo other = (Ruolo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.codiceOrganizzazione==null && other.getCodiceOrganizzazione()==null) || 
             (this.codiceOrganizzazione!=null &&
              this.codiceOrganizzazione.equals(other.getCodiceOrganizzazione()))) &&
            ((this.codiceUnitaOrganizzativa==null && other.getCodiceUnitaOrganizzativa()==null) || 
             (this.codiceUnitaOrganizzativa!=null &&
              this.codiceUnitaOrganizzativa.equals(other.getCodiceUnitaOrganizzativa()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.disabilitato==null && other.getDisabilitato()==null) || 
             (this.disabilitato!=null &&
              this.disabilitato.equals(other.getDisabilitato()))) &&
            ((this.responsabile==null && other.getResponsabile()==null) || 
             (this.responsabile!=null &&
              this.responsabile.equals(other.getResponsabile()))) &&
            ((this.ruoloUtenteDisabilitato==null && other.getRuoloUtenteDisabilitato()==null) || 
             (this.ruoloUtenteDisabilitato!=null &&
              this.ruoloUtenteDisabilitato.equals(other.getRuoloUtenteDisabilitato()))) &&
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            ((this.utenti==null && other.getUtenti()==null) || 
             (this.utenti!=null &&
              java.util.Arrays.equals(this.utenti, other.getUtenti())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getCodiceOrganizzazione() != null) {
            _hashCode += getCodiceOrganizzazione().hashCode();
        }
        if (getCodiceUnitaOrganizzativa() != null) {
            _hashCode += getCodiceUnitaOrganizzativa().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getDisabilitato() != null) {
            _hashCode += getDisabilitato().hashCode();
        }
        if (getResponsabile() != null) {
            _hashCode += getResponsabile().hashCode();
        }
        if (getRuoloUtenteDisabilitato() != null) {
            _hashCode += getRuoloUtenteDisabilitato().hashCode();
        }
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        if (getUtenti() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUtenti());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUtenti(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Ruolo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Ruolo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceOrganizzazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceOrganizzazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceUnitaOrganizzativa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceUnitaOrganizzativa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Disabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responsabile");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Responsabile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruoloUtenteDisabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RuoloUtenteDisabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("utenti");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Utenti"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "UserExt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "UserExt"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
