package it.inps.soa.classi;
/**
 * PrincipalDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class PrincipalDetail  implements java.io.Serializable {
    private java.lang.Boolean disabledDetail;

    public PrincipalDetail() {
    }

    public PrincipalDetail(
           java.lang.Boolean disabledDetail) {
           this.disabledDetail = disabledDetail;
    }


    /**
     * Gets the disabledDetail value for this PrincipalDetail.
     * 
     * @return disabledDetail
     */
    public java.lang.Boolean getDisabledDetail() {
        return disabledDetail;
    }


    /**
     * Sets the disabledDetail value for this PrincipalDetail.
     * 
     * @param disabledDetail
     */
    public void setDisabledDetail(java.lang.Boolean disabledDetail) {
        this.disabledDetail = disabledDetail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PrincipalDetail)) return false;
        PrincipalDetail other = (PrincipalDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.disabledDetail==null && other.getDisabledDetail()==null) || 
             (this.disabledDetail!=null &&
              this.disabledDetail.equals(other.getDisabledDetail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDisabledDetail() != null) {
            _hashCode += getDisabledDetail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PrincipalDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "PrincipalDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabledDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DisabledDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
