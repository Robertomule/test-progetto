package it.inps.soa.classi;
/**
 * RegistroResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class RegistroResponse  extends Response  implements java.io.Serializable {
    private Registro[] registri;

    public RegistroResponse() {
    }

    public RegistroResponse(
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnEsito codice,
           java.lang.String descrizione,
           Registro[] registri) {
        super(
            codice,
            descrizione);
        this.registri = registri;
    }


    /**
     * Gets the registri value for this RegistroResponse.
     * 
     * @return registri
     */
    public Registro[] getRegistri() {
        return registri;
    }


    /**
     * Sets the registri value for this RegistroResponse.
     * 
     * @param registri
     */
    public void setRegistri(Registro[] registri) {
        this.registri = registri;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistroResponse)) return false;
        RegistroResponse other = (RegistroResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.registri==null && other.getRegistri()==null) || 
             (this.registri!=null &&
              java.util.Arrays.equals(this.registri, other.getRegistri())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRegistri() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRegistri());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRegistri(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistroResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "RegistroResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Registri"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Registro"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Registro"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
