package it.inps.soa.classi;
/**
 * TrasferisciReq.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class TrasferisciReq  implements java.io.Serializable {
    private java.lang.String codiceArea;

    private java.lang.String codiceClassifica;

    private java.lang.String codiceProcessoDestinatario;

    public TrasferisciReq() {
    }

    public TrasferisciReq(
           java.lang.String codiceArea,
           java.lang.String codiceClassifica,
           java.lang.String codiceProcessoDestinatario) {
           this.codiceArea = codiceArea;
           this.codiceClassifica = codiceClassifica;
           this.codiceProcessoDestinatario = codiceProcessoDestinatario;
    }


    /**
     * Gets the codiceArea value for this TrasferisciReq.
     * 
     * @return codiceArea
     */
    public java.lang.String getCodiceArea() {
        return codiceArea;
    }


    /**
     * Sets the codiceArea value for this TrasferisciReq.
     * 
     * @param codiceArea
     */
    public void setCodiceArea(java.lang.String codiceArea) {
        this.codiceArea = codiceArea;
    }


    /**
     * Gets the codiceClassifica value for this TrasferisciReq.
     * 
     * @return codiceClassifica
     */
    public java.lang.String getCodiceClassifica() {
        return codiceClassifica;
    }


    /**
     * Sets the codiceClassifica value for this TrasferisciReq.
     * 
     * @param codiceClassifica
     */
    public void setCodiceClassifica(java.lang.String codiceClassifica) {
        this.codiceClassifica = codiceClassifica;
    }


    /**
     * Gets the codiceProcessoDestinatario value for this TrasferisciReq.
     * 
     * @return codiceProcessoDestinatario
     */
    public java.lang.String getCodiceProcessoDestinatario() {
        return codiceProcessoDestinatario;
    }


    /**
     * Sets the codiceProcessoDestinatario value for this TrasferisciReq.
     * 
     * @param codiceProcessoDestinatario
     */
    public void setCodiceProcessoDestinatario(java.lang.String codiceProcessoDestinatario) {
        this.codiceProcessoDestinatario = codiceProcessoDestinatario;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TrasferisciReq)) return false;
        TrasferisciReq other = (TrasferisciReq) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codiceArea==null && other.getCodiceArea()==null) || 
             (this.codiceArea!=null &&
              this.codiceArea.equals(other.getCodiceArea()))) &&
            ((this.codiceClassifica==null && other.getCodiceClassifica()==null) || 
             (this.codiceClassifica!=null &&
              this.codiceClassifica.equals(other.getCodiceClassifica()))) &&
            ((this.codiceProcessoDestinatario==null && other.getCodiceProcessoDestinatario()==null) || 
             (this.codiceProcessoDestinatario!=null &&
              this.codiceProcessoDestinatario.equals(other.getCodiceProcessoDestinatario())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodiceArea() != null) {
            _hashCode += getCodiceArea().hashCode();
        }
        if (getCodiceClassifica() != null) {
            _hashCode += getCodiceClassifica().hashCode();
        }
        if (getCodiceProcessoDestinatario() != null) {
            _hashCode += getCodiceProcessoDestinatario().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TrasferisciReq.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "TrasferisciReq"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceArea");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceClassifica");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceClassifica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceProcessoDestinatario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceProcessoDestinatario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
