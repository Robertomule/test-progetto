/**
 * IStoreAttachmentDocument.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.inps.soa.WS00541;

public interface IStoreAttachmentDocument extends java.rmi.Remote {
    public it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaResponse esistonoDocumentiPerSegnatura(it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.GetDocumentoResponse getDocumento(it.inps.soa.DocumentStore.data.GetDocumentoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.GetDocumentoResponse getDocumentoPerContainer(it.inps.soa.DocumentStore.data.GetDocumentoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiPerContainer(it.inps.soa.DocumentStore.data.ListaDocumentiPerContainerRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiCorrelatiPerContainer(it.inps.soa.DocumentStore.data.ListaDocumentiPerContainerRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiPerSegnatura(it.inps.soa.DocumentStore.data.ListaDocumentiPerSegnaturaRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiCorrelatiPerSegnatura(it.inps.soa.DocumentStore.data.ListaDocumentiPerSegnaturaRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerSegnatura(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerContainer(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerContainerRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse prenotaVisualizzazioneDocumento(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiResponse prenotaVisualizzazioneSintesi(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaResponse prenotaVisualizzazioneRicevuta(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.GetAllegatoResponse getAllegato(it.inps.soa.DocumentStore.data.GetAllegatoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.RecuperaMetadatiResponse recuperaMetadatiAllegato(it.inps.soa.DocumentStore.data.RecuperaMetadatiRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaAllegato(it.inps.soa.DocumentStore.data.ArchiviaDocumentoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.CancellaDocumentoResponse cancellaAllegato(it.inps.soa.DocumentStore.data.CancellaDocumentoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse prenotaVisualizzazioneAllegato(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoResponse prenotaCreazioneAllegato(it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoResponse prenotaAggiornamentoAllegato(it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerSegnaturaPregressaNonStandard(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaPregressaRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerSegnaturaPregressa(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaPregressaRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerVersione(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerVersioneRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.GetDocumentoResponse getDocumentoPerVersione(it.inps.soa.DocumentStore.data.GetDocumentoPerVersioneRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiVersionati(it.inps.soa.DocumentStore.data.ListaDocumentiVersionatiRequest request) throws java.rmi.RemoteException;
    public it.inps.soa.DocumentStore.data.AggiornaMetadatiResponse aggiornaMetadati(it.inps.soa.DocumentStore.data.AggiornaMetadatiRequest request) throws java.rmi.RemoteException;
}
