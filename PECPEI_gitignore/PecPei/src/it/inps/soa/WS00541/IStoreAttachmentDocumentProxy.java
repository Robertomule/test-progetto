package it.inps.soa.WS00541;

import it.eustema.inps.utility.ConfigProp;
import it.inps.soa.DocumentStore.data.AggiornaMetadatiRequest;
import it.inps.soa.DocumentStore.data.AggiornaMetadatiResponse;
import it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerContainerRequest;
import it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaPregressaRequest;
import it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaRequest;
import it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerVersioneRequest;
import it.inps.soa.DocumentStore.data.ArchiviaDocumentoRequest;
import it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse;
import it.inps.soa.DocumentStore.data.CancellaDocumentoRequest;
import it.inps.soa.DocumentStore.data.CancellaDocumentoResponse;
import it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaRequest;
import it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaResponse;
import it.inps.soa.DocumentStore.data.GetAllegatoRequest;
import it.inps.soa.DocumentStore.data.GetAllegatoResponse;
import it.inps.soa.DocumentStore.data.GetDocumentoPerVersioneRequest;
import it.inps.soa.DocumentStore.data.GetDocumentoRequest;
import it.inps.soa.DocumentStore.data.GetDocumentoResponse;
import it.inps.soa.DocumentStore.data.ListaDocumentiPerContainerRequest;
import it.inps.soa.DocumentStore.data.ListaDocumentiPerSegnaturaRequest;
import it.inps.soa.DocumentStore.data.ListaDocumentiResponse;
import it.inps.soa.DocumentStore.data.ListaDocumentiVersionatiRequest;
import it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoRequest;
import it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoResponse;
import it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoRequest;
import it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoResponse;
import it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoRequest;
import it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse;
import it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaRequest;
import it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaResponse;
import it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiRequest;
import it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiResponse;
import it.inps.soa.DocumentStore.data.RecuperaMetadatiRequest;
import it.inps.soa.DocumentStore.data.RecuperaMetadatiResponse;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;

import org.apache.axis.AxisFault;
import org.apache.axis.message.SOAPHeaderElement;
import wsPciClient.org.tempuri.DNA_BASICHTTP_BindingStub;


public class IStoreAttachmentDocumentProxy implements it.inps.soa.WS00541.IStoreAttachmentDocument {
  private String _endpoint = null;
  private it.inps.soa.WS00541.IStoreAttachmentDocument iStoreAttachmentDocument = null;
  
  public IStoreAttachmentDocumentProxy() {
    _initIStoreAttachmentDocumentProxy();
  }
  
  public IStoreAttachmentDocumentProxy(String endpoint) {
    _endpoint = endpoint;
    _initIStoreAttachmentDocumentProxy();
  }
  
  private void _initIStoreAttachmentDocumentProxy() {
    try {
      iStoreAttachmentDocument = (new org.tempuri.StoreAttachmentDocumentServiceLocator()).getDNA_BASICHTTP_Binding();
      if (iStoreAttachmentDocument != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iStoreAttachmentDocument)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iStoreAttachmentDocument)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iStoreAttachmentDocument != null)
      ((javax.xml.rpc.Stub)iStoreAttachmentDocument)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.inps.soa.WS00541.IStoreAttachmentDocument getIStoreAttachmentDocument() {
    if (iStoreAttachmentDocument == null)
      _initIStoreAttachmentDocumentProxy();
    return iStoreAttachmentDocument;
  }

@Override
public AggiornaMetadatiResponse aggiornaMetadati(AggiornaMetadatiRequest request)
		throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ArchiviaDocumentoResponse archiviaAllegato(
		ArchiviaDocumentoRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ArchiviaDocumentoResponse archiviaDocumentoPerContainer(
		ArchiviaDocumentoPerContainerRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ArchiviaDocumentoResponse archiviaDocumentoPerSegnatura(
		ArchiviaDocumentoPerSegnaturaRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ArchiviaDocumentoResponse archiviaDocumentoPerSegnaturaPregressa(
		ArchiviaDocumentoPerSegnaturaPregressaRequest request)
		throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ArchiviaDocumentoResponse archiviaDocumentoPerSegnaturaPregressaNonStandard(
		ArchiviaDocumentoPerSegnaturaPregressaRequest request)
		throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ArchiviaDocumentoResponse archiviaDocumentoPerVersione(
		ArchiviaDocumentoPerVersioneRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public CancellaDocumentoResponse cancellaAllegato(
		CancellaDocumentoRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public EsistonoDocumentiPerSegnaturaResponse esistonoDocumentiPerSegnatura(
		EsistonoDocumentiPerSegnaturaRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public GetAllegatoResponse getAllegato(GetAllegatoRequest request)
		throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public GetDocumentoResponse getDocumento(GetDocumentoRequest request)
		throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public GetDocumentoResponse getDocumentoPerContainer(GetDocumentoRequest request)
		throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public GetDocumentoResponse getDocumentoPerVersione(
		GetDocumentoPerVersioneRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ListaDocumentiResponse listaDocumentiCorrelatiPerContainer(
		ListaDocumentiPerContainerRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ListaDocumentiResponse listaDocumentiCorrelatiPerSegnatura(
		ListaDocumentiPerSegnaturaRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ListaDocumentiResponse listaDocumentiPerContainer(
		ListaDocumentiPerContainerRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ListaDocumentiResponse listaDocumentiPerSegnatura(
		ListaDocumentiPerSegnaturaRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ListaDocumentiResponse listaDocumentiVersionati(
		ListaDocumentiVersionatiRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public PrenotaAggiornamentoDocumentoResponse prenotaAggiornamentoAllegato(
		PrenotaAggiornamentoDocumentoRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public PrenotaCreazioneDocumentoResponse prenotaCreazioneAllegato(
		PrenotaCreazioneDocumentoRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public PrenotaVisualizzazioneDocumentoResponse prenotaVisualizzazioneAllegato(
		PrenotaVisualizzazioneDocumentoRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public PrenotaVisualizzazioneDocumentoResponse prenotaVisualizzazioneDocumento(
		PrenotaVisualizzazioneDocumentoRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public PrenotaVisualizzazioneRicevutaResponse prenotaVisualizzazioneRicevuta(
		PrenotaVisualizzazioneRicevutaRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public PrenotaVisualizzazioneSintesiResponse prenotaVisualizzazioneSintesi(
		PrenotaVisualizzazioneSintesiRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public RecuperaMetadatiResponse recuperaMetadatiAllegato(
		RecuperaMetadatiRequest request) throws RemoteException {
	// TODO Auto-generated method stub
	return null;
}

public static DNA_BASICHTTP_BindingStub getProxyHelper(){
	
	DNA_BASICHTTP_BindingStub prx=null;

	try {

		URL url = new URL(ConfigProp.endPoint);
		prx = new DNA_BASICHTTP_BindingStub(url,null);
		
		SOAPHeaderElement header = new SOAPHeaderElement("http://inps.it/", "Identity");
		header.addNamespaceDeclaration("inps", "http://inps.it/");
		
  		SOAPElement nodeName = header.addChildElement("AppName");
  		nodeName.addTextNode(ConfigProp.appNameDoc);
  		SOAPElement nodeKey = header.addChildElement("AppKey");
  		nodeKey.addTextNode(ConfigProp.appKeyDoc);
  		SOAPElement nodeUser = header.addChildElement("UserId");
  		nodeUser.addTextNode(ConfigProp.userIdDoc);
  		SOAPElement nodeIdProvider = header.addChildElement("IdentityProvider");
  		nodeIdProvider.addTextNode(ConfigProp.identityProviderDoc);
  		
 		SOAPElement nodeSession = header.addChildElement("SessionId");
  		nodeSession.addTextNode("");
//  		SOAPElement nodeSequence = header.addChildElement("SequenceId");
//  		nodeSequence.addTextNode("");
//  		SOAPElement nodePerr = header.addChildElement("PeerHost");
//  		nodePerr.addTextNode("");
//  		
//  		SOAPElement nodeCorrelation = header.addChildElement("CorrelationId");
//  		nodeCorrelation.addTextNode("");
  		
		prx.setHeader(header);
		//proxy._getCall().setTargetEndpointAddress(url);
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		
	return prx;

}

//Se non passo per il data power
public static DNA_BASICHTTP_BindingStub getProxyHelperNoDataPower(){
	
	DNA_BASICHTTP_BindingStub prx=null;

	try {

		URL url = new URL(ConfigProp.endPoint);
		prx = new DNA_BASICHTTP_BindingStub(url,null);
		
		SOAPHeaderElement header = new SOAPHeaderElement("http://inps.it/", "Identity");
		header.addNamespaceDeclaration("inps", "http://inps.it/");
		SOAPElement nodeName = header.addChildElement("AppName","inps");
		nodeName.addTextNode("AB00445");
		SOAPElement nodeKey = header.addChildElement("AppKey","inps");
		nodeKey.addTextNode("0zxehsxknoj2wwcyq9iwa1ievxss2jn2");
		SOAPElement nodeUser = header.addChildElement("UserId","inps");
		nodeUser.addTextNode("regprot");
		SOAPElement nodeIdProvider = header.addChildElement("IdentityProvider","inps");
		nodeIdProvider.addTextNode("AD");
		SOAPElement nodeSession = header.addChildElement("SessionId");
		nodeSession.addTextNode("2F358F78-5782-4E7B-B645-396C7127A31D");
		SOAPElement nodeSequence = header.addChildElement("SequenceId");
		nodeSequence.addTextNode("0");
		SOAPElement nodePerr = header.addChildElement("PeerHost");
		nodePerr.addTextNode("127.0.0.1");
		SOAPElement nodeCorrelation = header.addChildElement("CorrelationId");
		nodeCorrelation.addTextNode("");
		SOAPElement nodeCoperation = header.addChildElement("OperationContextId");
		nodeCoperation.addTextNode("0");
  		
		prx.setHeader(header);
		//proxy._getCall().setTargetEndpointAddress(url);
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		
	return prx;

}
  
  
}