package it.inps.www.interop2013;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public class TestNuovoSegnatura {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Stub di metodo generato automaticamente
		try {
			JAXBContext context = JAXBContext.newInstance(it.inps.www.interop2013.Segnatura.class);
			it.inps.www.interop2013.Segnatura segInterop = new it.inps.www.interop2013.Segnatura();
			String fileTest = "IU_2013_1Documento_2Allegati_2Cittadini.xml";
			fileTest = "IE_2013_completo_MultiClassifica_ERRATO.xml";
//			fileTest = "IE_2013_completo_MultiClassifica.xml";
			Reader reader = new InputStreamReader(new FileInputStream("C:\\testPdf\\provaInterop\\"+fileTest), "ISO-8859-1");
			segInterop = (it.inps.www.interop2013.Segnatura) context.createUnmarshaller().unmarshal(reader);
			context.createMarshaller().marshal(segInterop, new FileOutputStream("C:\\testPdf\\provaInterop\\SegnaturaOut.xml"));
		} catch (JAXBException e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		}

	}

}
