package it.inps.agentPec.verificaPec.exception;

public class InsertIndirizzoDAOException extends Exception
{
	
	  public InsertIndirizzoDAOException(Exception e)
	  {
	    super(e);
	  }
}
