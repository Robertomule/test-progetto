package it.inps.agentPec.verificaPec.business;

import java.util.List;
import java.util.concurrent.Callable;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.inps.agentPec.verificaPec.dao.AcquisizioneIndirizziDAO;

public class AggiornaStatoIndirizzo48H implements Command, Callable<Object> {

	public AggiornaStatoIndirizzo48H(){
	}
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		try {
			AcquisizioneIndirizziDAO indirizziDao = new AcquisizioneIndirizziDAO();
			indirizziDao.aggiornaIndirizziSenzaRicevuteH48();
		} catch ( DAOException de ){
			throw new NotifyMailBusinessException( -4010, "Errore nel recupero dati dell'account PEC : Dettaglio Errore: "+ de.getMessage() );
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new NotifyMailBusinessException(-4000, "Errore nel recupero dati dell'account PEC : Dettaglio Errore: "+ e.getMessage());
		}
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "AggiornaStatoIndirizzo48H";
	}
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
