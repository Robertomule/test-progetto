package it.inps.agentPec.verificaPec.business;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import org.apache.log4j.Logger;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.builder.MessageMessaggioDTOBuilder;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommand;
import it.eustema.inps.agentPecPei.business.pec.mail.CheckCorrettezzaMessaggioCommand;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.agentPecPei.util.CrittografiaPasswordPEC;
import it.eustema.inps.agentPecPei.util.DebugConstants;
import it.eustema.inps.agentPecPei.util.TripleDES;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.logging.LoggingConfigurator;
import it.inps.agentPec.verificaPec.business.builder.MessageMessaggioVerificaDTOBuilder;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;
import it.inps.agentPec.verificaPec.dao.MessaggiVerificaPecDAO;

public class ReadAndElabMessageVerificaCommand implements Command, Callable<Object> {

	private static Logger log = Logger.getLogger(ReadAndElabMessageVerificaCommand.class);
	private AccountPecDTO accountPecDTO;
	public ReadAndElabMessageVerificaCommand(AccountPecDTO accountPecDTO){
		this.accountPecDTO = accountPecDTO;
	}
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		MessaggiVerificaPecDAO messaggiDAO = new MessaggiVerificaPecDAO();
		Properties props = new Properties();
		ConfigDAO config = new ConfigDAO();
		TripleDES crittografia = new TripleDES();
	    props.put("mail.host", ConfigProp.hostPopAccountPEC );
	    props.put("mail.store.protocol", ConfigProp.storePopProtocolAccountPEC);
	    props.put("mail.pop3s.port", ConfigProp.portPopAccountPEC );
	    props.put("mail.pop3.ssl.trust",ConfigProp.sslPopTrustAccountPEC );
	    Session session = Session.getInstance(props);
	    final ConfigDAO configDao = new ConfigDAO();
        Store store;
		try {
			store = session.getStore();
			
			if ( DebugConstants.ENABLE_DEBUG_CONSTANTS )
				try{
					store.connect( DebugConstants.ACCOUNT_PEC_USER_NAME, crittografia.decrypt(DebugConstants.ACCOUNT_PEC_USER_PASSWORD));
				}catch(Exception e){
					executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6051, "Errore generico nella decifratura della password. Dettaglio Errore: " + e.getMessage() ) ) );
		    		executor.executeCommand();
				}
			else{ 
//					store.connect( configDao.getProperty("accountPec","")+"@"+configDao.getProperty("providerPec",""), crittografia.decrypt(config.getProperty("accountPecPassword","")));
					store.connect( accountPecDTO.getUserName()+"@"+configDao.getProperty("providerPec",""), crittografia.decrypt(accountPecDTO.getPassword()));
				}

		Folder inbox = store.getFolder("inbox");
	    inbox.open(Folder.READ_WRITE); // Folder.READ_ONLY
	    int messageCount = inbox.getMessageCount();
	   
	    for ( int i=1; i<=messageCount; i++ ){
	    	
	    	if(i>Integer.parseInt(configDao.getProperty("maxMsgRead", "1000"))) break;
	    	
	    	MessaggioDTO messaggioInEntrata = new MessaggioDTO();
	    	try{
		    	Message message = inbox.getMessage(i);
		    	
		    	executor.setCommand( new MessageMessaggioVerificaDTOBuilder( message, accountPecDTO, Constants.VERSO_MSG_ENTRANTE ) );
		    	messaggioInEntrata = (MessaggioDTO) executor.executeCommand();
		    	messaggioInEntrata.setAccountPec(accountPecDTO.getUserName());
//	    		for ( AllegatoDTO a: messaggioInEntrata.getElencoFileAllegati() ){
//	    			if ( "postacert.eml".equalsIgnoreCase( a.getFileName() ) ){
//	    				a.setFileName("allegato.eml");
//	    				log.info("Messaggio pec in Entrata Dalla DPL.SALERNO ******* : "+messaggioInEntrata.getMessageId()+" account "+messaggioInEntrata.getAccountPec()+" sulla sede "+messaggioInEntrata.getCodiceAOO() + " RINOMINATO ALLEGATO POSTACERT.EML IN ALLEGATO.EML");
//	    				break;
//	    			}
//	    		}

		    	//Verifica Correttezza messaggio
		    	executor.setCommand( new CheckCorrettezzaMessaggioCommand( messaggioInEntrata ) );
		    	CheckCorrettezzaMessaggioCommand.Out checkOut = (CheckCorrettezzaMessaggioCommand.Out) executor.executeCommand();
				
				if(!messaggiDAO.verificaMessaggioEntrante(messaggioInEntrata)){

					messaggiDAO.inserisciMessaggioVerificaPec(null, messaggioInEntrata );
	
					if ( checkOut == CheckCorrettezzaMessaggioCommand.Out.OK ){
							messaggioInEntrata.setStato( Constants.STATO_MSG_RICEVUTO );
							messaggioInEntrata.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
							messaggiDAO.aggiornaMsgEntranteRicevuto( null, messaggioInEntrata );
					} else {
						if ( checkOut != CheckCorrettezzaMessaggioCommand.Out.CAMPO_TIPO_MESSAGGIO_RICEVUTA ){
							executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6500, "Errore nella validazione del messaggio [" + messaggioInEntrata.getMessageId() + "] in entrata. Dettaglio Errore: codice: " + checkOut.getCodice() + "   descrizione : " + checkOut.getDesc() ) ) );
							executor.executeCommand();
							throw new BusinessException( -6500, "Errore nella validazione del messaggio [" + messaggioInEntrata.getMessageId() + "] in entrata. Dettaglio Errore: codice: " + checkOut.getCodice() + "   descrizione : " + checkOut.getDesc() ) ;
						}	
					}
				}				
				//Rimozione del messaggio dalla casella
		        message.setFlag(Flags.Flag.SEEN, true);
		        message.setFlag(Flags.Flag.DELETED, true);
		       
		
	    	} catch ( BusinessException e ){
				executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6051, "Errore nell'nserimento su DB->Messaggi. Dettaglio Errore: " + e.getMessage() ) ) );
	    		executor.executeCommand();
	    	} catch (DAOException e) {
				if(messaggioInEntrata.getSubject()!=null && !messaggioInEntrata.getSubject().contains("****SPAM****")){
		    		executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6051, "Errore nell'nserimento su DB->Messaggi. Dettaglio Errore: " + e.getMessage() ) ) );
		    		executor.executeCommand();
	    		}
	    	} catch (Exception e) {
				executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6051, "Errore generico nell'nserimento su DB->Messaggi. Dettaglio Errore: " + e.getMessage() ) ) );
	    		executor.executeCommand();
			}
	    }
	    
	    inbox.close(true);
	    store.close();
		
			} catch (NoSuchProviderException e) {
				throw new NotifyMailBusinessException(-2010, "Errore nella connesisone con l'account PEC[ " + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				throw new NotifyMailBusinessException(-2020, "Errore nella connesisone con l'utenza [" + accountPecDTO.getUserName() + "] nell'account PEC[ " + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());
			} catch (Exception e) {
				// TODO Blocco catch generato automaticamente
				e.printStackTrace();
			}
	        return null;
			
	}
	
	public static void main( String[] args ){
		try {
			
			new ConfigProp().init();
				
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
			AccountPecDTO acc = new AccountPecDTO();
			acc.setUserName( "utente03" );
			acc.setCodAOO("0040");
//			new ReadAndElabMessageCommand( acc ).execute();
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Read Message MAIL";
	}
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
