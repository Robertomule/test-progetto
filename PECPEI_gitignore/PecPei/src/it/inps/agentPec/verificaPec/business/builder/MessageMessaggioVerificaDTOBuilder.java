package it.inps.agentPec.verificaPec.business.builder;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.cert.RetrieveCertAuthDTOCommand;
import it.eustema.inps.agentPecPei.business.util.RetrieveUiDocFromMessageIDCommand;
import it.eustema.inps.agentPecPei.dao.IndicePADAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.CertAuthDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.IndicePADTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.StaticUtil;
import it.inps.agentPec.verificaPec.dao.AcquisizioneIndirizziDAO;
import it.inps.agentPec.verificaPec.dao.MessaggiVerificaPecDAO;
import it.inps.agentPec.verificaPec.util.StatoType;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.SimpleFormatter;

import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.xml.bind.JAXBContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.sun.mail.util.UUDecoderStream;



public class MessageMessaggioVerificaDTOBuilder implements Command {

	private Message message;
	private AccountPecDTO accountPecDTO;
	private String verso;
	private final static String UU_DELIM = "begin 666";

	public MessageMessaggioVerificaDTOBuilder(Message message,
			AccountPecDTO accountPecDTO, String verso) {
		this.message = message;
		this.accountPecDTO = accountPecDTO;
		this.verso = verso;
	}
	public MessageMessaggioVerificaDTOBuilder() {
	}
	
	@Override
	public Object execute() throws BusinessException {
		MessaggioDTO messaggio = new MessaggioDTO();
		messaggio.setElencoFileAllegati(new ArrayList<AllegatoDTO>());
		messaggio.setElencoDestinatariPer(new ArrayList<DestinatarioDTO>());
		messaggio.setElencoDestinatariCc(new ArrayList<DestinatarioDTO>());

		CommandExecutor executor = CommandExecutorManager.getInstance()
				.getExecutor();
		
		try {
			
			
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			message.writeTo(os);
			messaggio.setRawContent(new String(os.toByteArray()));

			StringTokenizer st = new StringTokenizer(messaggio.getRawContent(),"\n");
			
			String from = readHeaders("From", message.getAllHeaders()).get(0);
		
			// 1. UIDOC
			List<String> s1 = readHeaders("Message-Id", message.getAllHeaders());
			
			executor.setCommand(new RetrieveUiDocFromMessageIDCommand(s1.get(0)));
			messaggio.setUiDoc((String) executor.executeCommand());
			// 2. ACCOUNT PEC
			// executor.setCommand( new RetrieveReplyToFromFromCommand(
			// from.trim() ) );
			// messaggio.setAccountPec( (String)executor.executeCommand() );

			// Si mette l'account PEC che si ha in input e non si deve
			// recuperare dal messaggio
			messaggio.setAccountPec(accountPecDTO.getUserName());

			// 3. DOMINIO PEC
			String dominioPec = from.substring(0, from.lastIndexOf("\"") + 1)
					.trim();
			if (dominioPec == null || dominioPec.equals(""))
				dominioPec = from;
			messaggio.setDominoPec(dominioPec.substring(
					dominioPec.lastIndexOf("@") + 1, dominioPec.length())
					.replaceAll("\"", "").replaceAll(">", ""));

			// 73. DESMITT
			messaggio.setDesMitt(dominioPec.substring(0,
					dominioPec.lastIndexOf("@") + 1).replaceAll("@", "")
					.replaceAll("Per conto di:", "").replaceAll("\"", "")
					.trim());

			// 4. VERSO
			messaggio.setVerso(verso);

			// 5. COD AOO
			messaggio.setCodiceAOO(accountPecDTO.getCodAOO());

			// 6. MESSAGEID
			messaggio.setMessageId(readHeaders("Message-ID",message.getAllHeaders()).get(0));
			
			// 11. FROMADDR
			// messaggio.setFromAddr( message.getFrom()[0].toString() );
			// Impostato pi� avanti insieme al InReplyTO
//			if(messaggio.getMessageId().equals("<opec275.20140825091900.19829.10.1.102@spcoop.postacert.it>"))
//				System.out.println("<opec275.20140825091900.19829.10.1.102@spcoop.postacert.it>");
			// 12. FROMPHRASE
			if(from != null && from.contains("<"))
				messaggio.setFromPhrase(from.trim().substring(0,from.trim().lastIndexOf("<")).replace("\"", ""));
			else
				messaggio.setFromPhrase(from);

			// 13. REPLYTO

			// executor.setCommand( new RetrieveReplyToFromFromCommand(
			// from.trim() ) );
			// messaggio.setReplyTo( (String)executor.executeCommand() );
			messaggio.setReplyTo(messaggio.getDesMitt() + "@"
					+ messaggio.getDominoPec());

			// 14. POSTEDDATE E DELIVEREDDATE
			if(message.getSentDate()!=null)
				messaggio.setPostedDate(message.getSentDate());
			else
				try {
					messaggio.setPostedDate(readHeaders("Date", message.getAllHeaders())!=null?null:new SimpleDateFormat("dd/MM/yyyy hh.mm.ss").parse(readHeaders("Date", message.getAllHeaders()).get(0)));
				} catch (ParseException e) {
					// TODO Blocco catch generato automaticamente
					e.printStackTrace();
				}
			
			messaggio.setDeliveredDate(message.getReceivedDate());
			
			// 16. SUBJECT
			messaggio.setSubject(message.getSubject());
			
			if(messaggio.getSubject().startsWith("POSTA CERTIFICATA:"))
				messaggio.setPostaCertEMLSubject(messaggio.getSubject().replaceAll("POSTA CERTIFICATA:", ""));
			
			// 17. XTIPORICEVUTA
			List<String> tipoRicevuta = readHeaders("X-TipoRicevuta", message.getAllHeaders());
			if (tipoRicevuta != null && !tipoRicevuta.isEmpty())
				messaggio.setTipoRicevuta(tipoRicevuta.get(0).trim());
			else
				messaggio.setTipoRicevuta("");

			// 18. RISERVATO
			List<String> riservato = readRowContent("X-Sensitivity", st);
			if (riservato != null && !riservato.isEmpty()){
				String riservatoString = riservato.get(0);
				if(riservatoString.equals("1")){
					messaggio.setRiservato("S");
				}else{
					messaggio.setRiservato("N");
				}
			}else messaggio.setRiservato("N");

			// 19. SENSIBILE
			messaggio.setSensibile("N");

			// 20. CLASSIFICA
			messaggio.setClassifica("800.020");

			// 21. DES CLASSIFICA
			messaggio.setDesClassifica("PEC - Posta Elettronica Certificata");

			// 22. BODY

			// 25.INREPLYTO & 11. FROMADDR
			String inReplyTo = "";
			if (from != null && (from.trim().startsWith("\"") || from.contains("<")))
				if(from.contains("\"")){
				inReplyTo = from.substring(from.lastIndexOf("\""),
						from.length()).replaceAll("<", "").replaceAll(">", "")
						.replaceAll("\"", "").trim();
				}else{
					inReplyTo = from.substring(from.indexOf("<"),
							from.length()).replaceAll("<", "").replaceAll(">", "")
							.replaceAll("\"", "").trim();
				}
			else
				inReplyTo = from.trim();

			messaggio.setInReplyTo(inReplyTo);
			//
			messaggio.setFromAddr(inReplyTo);

			// 28. HEADERS
			String header = "";
			Enumeration e = message.getAllHeaders(); 
			while (e.hasMoreElements()) {
				Header h = (Header) e.nextElement();
				header += (h.getName()+": "+h.getValue()+"\n");
			}
			messaggio.setHeaders(header);

			// 30. XTRASPORTO
			messaggio.setXTrasporto("");
			List<String> xtrasporto = readHeaders("X-Trasporto", message.getAllHeaders());
			if (xtrasporto != null && !xtrasporto.isEmpty())
				messaggio.setXTrasporto(xtrasporto.get(0).trim());

			// 31. XRICEVUTA
			messaggio.setRicevuta("");
			List<String> xricevuta = readHeaders("X-Ricevuta", message.getAllHeaders());
			if (xricevuta != null && !xricevuta.isEmpty())
				messaggio.setRicevuta(xricevuta.get(0).trim());
			
			// 23. DOCUMENTO PRINCIPALE
			// TODO nome documento principale
			messaggio.setDocumentoPrincipale(messaggio.getRicevuta()+messaggio.getXTrasporto()+".eml");
			
			// 29. TIPOLOGIA
			// 32. TIPO MESSAGGIO
			// .. RICEVUTA
			if(messaggio.getRicevuta()!=null && !messaggio.getRicevuta().isEmpty()){
				messaggio.setTipologia("X-Ricevuta");
				messaggio.setTipoMessaggio(messaggio.getRicevuta());
				messaggio.setTipoMitt("A");
			}else{
				if(messaggio.getXTrasporto()!=null&&messaggio.getXTrasporto().equals("errore")){
					messaggio.setTipologia("X-Ricevuta");
					
					if(messaggio.getSubject().trim().startsWith("ANOMALIA")){
						messaggio.setTipoMessaggio("errore");
					}else{
						messaggio.setTipoMessaggio("errore-consegna");
//						messaggio.setRicevuta("errore-consegna");
					}
					messaggio.setTipoMitt("A");
				}else{
					messaggio.setTipologia(Constants.TIPOLOGIADB_XTRASPORTO);
					messaggio.setTipoMessaggio(messaggio.getXTrasporto());
					IndicePADAO indicePADao = new IndicePADAO();
					IndicePADTO indice = indicePADao.selectEmailIndicePA(messaggio.getReplyTo());
					if(indice!=null){
						messaggio.setTipoMitt("M");
						messaggio.setCodiceAMMMitt(indice.getCodamm());
						messaggio.setCodiceAOOMitt(indice.getCodaoo());
						messaggio.setDirettoreAOOMitt(indice.getCognomeresp()+" "+indice.getNomeresp());
					}else if(messaggio.getReplyTo()!= null && messaggio.getReplyTo().contains(ConfigProp.dominioCecPac)){
						messaggio.setTipoMitt("F");
						messaggio.setCodiceAMMMitt("inps");
						messaggio.setCodiceAOOMitt(messaggio.getReplyTo().substring(0, messaggio.getReplyTo().indexOf("@")));
					}else{
						messaggio.setTipoMitt("A");
					}
				}
			}
			if(messaggio.getSubject()!=null&&messaggio.getSubject().trim().startsWith("ANOMALIA")){
				messaggio.setTipologia(Constants.TIPOLOGIADB_XTRASPORTO);
			}
			
			messaggio.setNomeRegione(accountPecDTO.getRegioneSede());
			
			if(verso.equals("E"))
				messaggio.setNomeUfficio("Direzione");
			else	
				messaggio.setNomeUfficio(accountPecDTO.getUfficioSede());
			
			messaggio.setNomeSede(accountPecDTO.getDescrizioneSede());

			// 54. STATO
			messaggio.setStato(Constants.STATO_MSG_DA_PROTOCOLLARE);

			// 55. SUB STATO
			messaggio.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);

			
			// Valorizzazione di tutti i campi ottenibili dall'Header
			/*
			 * PSTMessaggioDTOHeaderFiller headerFiller = new
			 * PSTMessaggioDTOHeaderFiller(); headerFiller.setMessaggioDTO(
			 * messaggio ); headerFiller.setPSTMessage( mail );
			 * headerFiller.fill();
			 */

			boolean	flagCCn = false;
			ArrayList<AllegatoDTO> a = new ArrayList<AllegatoDTO>();
			ByteArrayInputStream bo = new ByteArrayInputStream(messaggio.getRawContent().getBytes());
			MimeBodyPart mbp = new MimeBodyPart(bo);
			elabPostaCertEML(messaggio, mbp, a);
			bonificaAllegati(a);
			messaggio.setElencoFileAllegati(a);
		    it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert datiCertificati = new it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert();
//		    List<String> listaDestinatariCert = new ArrayList<String>();
		    for (AllegatoDTO all : messaggio.getElencoFileAllegati()) {
				if(all.getFileName().equalsIgnoreCase("daticert.xml")){
					 String dati = new String(all.getFileData());
					 JAXBContext context = JAXBContext.newInstance(it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert.class);
					 try{
						 datiCertificati = (it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert) context.createUnmarshaller().unmarshal(new ByteArrayInputStream(dati.replaceAll("<!DOCTYPE Segnatura SYSTEM \"Segnatura.dtd\">", "").replaceAll("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "").getBytes()));
					 }catch(Exception exp){
						 new NotifyMailBusinessException(-8010,
								"Errore nella costruzione dei dati ceritificati del messaggio: ["
										+ messaggio.getMessageId() + "sede : "+messaggio.getCodiceAOO()+"] Dettaglio Errore: "
										+ exp.getMessage());
					 }
				}
			}
			if(messaggio.getBodyHtml()!=null && messaggio.getPostaCertEMLBody()==null){
				messaggio.setPostaCertEMLBody(messaggio.getBodyHtml());
			}
			
			// Destinatari
		    List<String> listaDestinatari = new ArrayList<String>();
		    List<String> listaDestinatariElab = new ArrayList<String>();
			listaDestinatari.addAll(readHeaders("To", message.getAllHeaders()));
		    listaDestinatari.addAll(readHeaders("Delivered-To", message.getAllHeaders()));
		    for(String dest: listaDestinatari){
		    	if(listaDestinatariElab.size()==0){listaDestinatariElab.add(dest);
				}else{
					boolean insDest = true;
					for(String destElab: listaDestinatariElab){
						if(destElab.toLowerCase().contains(dest.toLowerCase())){insDest = false;}
					}
					if(insDest)listaDestinatariElab.add(dest);
				}
			}
			if(listaDestinatariElab!=null && !listaDestinatariElab.isEmpty()){
				for(String sdest:listaDestinatariElab){
					StringTokenizer stdest = new StringTokenizer(sdest, ";|,");
					while(stdest.hasMoreElements()){
						String s = (String)stdest.nextElement(); 
						String addr = s.indexOf("<") == -1 ? s : s.substring(s.indexOf("<") + 1, s.length() - 1);
						String name = s.indexOf("<") != -1 ? s.substring(0, s.indexOf("<")) : "";
						DestinatarioDTO dest = new DestinatarioDTO();
	//					if((accountPecDTO.getUserName()+ConfigProp.dominioPEC).equalsIgnoreCase(addr.trim())&& messaggio.getVerso().equals("E"))
	//						flagCCn = true;
						dest.setAddr(addr.trim());
						dest.setName(name);
						dest.setPhrase(s.trim());
						dest.setMessageID(messaggio.getMessageId());
						dest.setType("Per");
						messaggio.getElencoDestinatariPer().add(dest);
					}
				}
			}
			List<String> listaDestinatariCC = readHeaders("Cc", message.getAllHeaders());
			if(listaDestinatariCC!=null && !listaDestinatariCC.isEmpty()){
				for(String sdestCC:listaDestinatariCC){
					StringTokenizer stdest = new StringTokenizer(sdestCC, ";|,");
					while(stdest.hasMoreElements()){
						String s = (String)stdest.nextElement(); 
						String addr = s.indexOf("<") == -1 ? s : s.substring(s.indexOf("<") + 1, s.length() - 1);
						String name = s.indexOf("<") != -1 ? s.substring(0, s.indexOf("<")) : "";
						DestinatarioDTO dest = new DestinatarioDTO();
	//					if((accountPecDTO.getUserName()+ConfigProp.dominioPEC).equalsIgnoreCase(addr.trim())&&messaggio.getVerso().equals("E"))
	//						flagCCn = true;
						
						dest.setAddr(addr.trim());
						dest.setName(name);
						dest.setPhrase(s.trim());
						dest.setMessageID(messaggio.getMessageId());
						dest.setType("cc");
						messaggio.getElencoDestinatariCc().add(dest);
					}
				}
			}
			 boolean esistenzaDestDatiCert = true;
			 if(datiCertificati.getIntestazione()!=null){
			 for(it.eustema.inps.agentPecPei.business.pec.datiCert.Destinatari dest : datiCertificati.getIntestazione().getDestinatari()){
				 for(DestinatarioDTO d : messaggio.getElencoDestinatariPer()){
					 if(dest.getValue()!= null && dest.getValue().trim().contains(d.getAddr()))
						 esistenzaDestDatiCert = false;
				 }
				 for(DestinatarioDTO d : messaggio.getElencoDestinatariCc()){
					 if(dest.getValue()!= null && dest.getValue().trim().contains(d.getAddr()))
						 esistenzaDestDatiCert = false;
				 }
				 if(esistenzaDestDatiCert){
 				    DestinatarioDTO destinatario = new DestinatarioDTO();
 				    destinatario.setAddr(dest.getValue().trim());
 				    destinatario.setName("");
 				    destinatario.setPhrase(dest.getValue());
 				    destinatario.setMessageID(messaggio.getMessageId());
 				    destinatario.setType("cc");
					messaggio.getElencoDestinatariCc().add(destinatario);
//					if((accountPecDTO.getUserName()+ConfigProp.dominioPEC).equalsIgnoreCase(destinatario.getAddr().trim())&&messaggio.getVerso().equals("E"))
//						flagCCn = true;
				 }
			 }
			}
			if(messaggio.getSubject()!=null && ((messaggio.getSubject().contains("****SPAM****"))))
				messaggio.setIsSPAMorCCN(1);
			else
				messaggio.setIsSPAMorCCN(0);
			
			messaggio.setSize(message.getSize());
			
			if((messaggio.getXTrasporto().equals(Constants.Posta_Certificata) || messaggio.getRicevuta().equalsIgnoreCase(Constants.avvenutaConsegna))&& 
					(messaggio.getTipoRicevuta()==null||messaggio.getTipoRicevuta().equals(""))){
//					for (AllegatoDTO all : messaggio.getElencoFileAllegati()) {
//						String segnatura = "";
//						 if(all.getFileName() != null && all.getFileName().equalsIgnoreCase("daticert.xml")){
//							 segnatura = new String(all.getFileData());
//							 JAXBContext context = JAXBContext.newInstance(it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert.class);
//							 it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert datiCertificati = new it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert();
//							 try{
//								 datiCertificati = (it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert) context.createUnmarshaller().unmarshal(new ByteArrayInputStream(segnatura.replaceAll("<!DOCTYPE Segnatura SYSTEM \"Segnatura.dtd\">", "").replaceAll("xmlns=\"http://www.digitPa.gov.it/protocollo/\"", "").getBytes()));
							 if(datiCertificati.getDati()!=null&&datiCertificati.getDati().getRicevuta()!=null)
								 messaggio.setTipoRicevuta(datiCertificati.getDati().getRicevuta().getTipo());
//							 }catch(Exception exp){
//								 new NotifyMailBusinessException(-8010,
//											"Errore nella costruzione dei dati ceritificati del messaggio: ["
//													+ messaggio.getMessageId() + "sede : "+messaggio.getCodiceAOO()+"] Dettaglio Errore: "
//													+ exp.getMessage());
//							 }
//							 System.out.println("Tipo Ricevuta :"+messaggio.getTipoRicevuta());
//						 }
				}
//			}
			CertAuthDTO certAuthDTO = null;
			for (AllegatoDTO allegato : messaggio.getElencoFileAllegati()) {
				if (allegato.getTipoAllegato().indexOf("p7") != -1) {
					executor.setCommand(new RetrieveCertAuthDTOCommand(allegato.getFileData()));
					certAuthDTO = (CertAuthDTO) executor.executeCommand();
					if (certAuthDTO != null) 
						break;
				}
			}
			
			// 41. SignValidFromDate
			if (certAuthDTO != null) {
				messaggio.setSignValidFromDate(certAuthDTO.getSignValidFrom());
				messaggio.setSignIssuerName(certAuthDTO.getIssuerName());
				messaggio.setSignSubjectName(certAuthDTO.getSubjectName());
				messaggio.setIsSigned(certAuthDTO.getSigned());
				messaggio.setIsSignedVer(certAuthDTO.getSignedVer());
			}
			// 42. SignValidToDate
			if (certAuthDTO != null) {
				messaggio.setSignValidToDate(certAuthDTO.getSignValidTo());
			}

			// 52. PostacertEml_SignValidFromDate
			if (certAuthDTO != null) {
				messaggio.setPemlSignValidFromDate(certAuthDTO
						.getSignValidFrom());
			}

			// 53. PostacertEml_SignValidToDate
			if (certAuthDTO != null) {
				messaggio.setPemlSignValidToDate(certAuthDTO.getSignValidTo());
			}
			
			messaggio.setAutoreCompositore("Batch Verifica PecPei");
			
			st = new StringTokenizer(messaggio.getRawContent(),"\n");
			
			List<String> messaggiDiRiferimento = readRowContent("X-Riferimento-Message-ID", st);
			messaggiDiRiferimento.addAll(readHeaders("X-Riferimento-Message-ID", message.getAllHeaders()));
			MessaggiVerificaPecDAO messDao = new MessaggiVerificaPecDAO();
			MessaggioDTO messaggioResult = new MessaggioDTO();
			if(messaggiDiRiferimento!=null){ 
				if(messaggiDiRiferimento.size()>0){
					for(String s : messaggiDiRiferimento){
						if(messDao.verificaMessaggio(s)>0){
//							messaggioResult = messDao.verificaMessaggioSensOrRiserv(s);//, messaggio.getAccountPec(), messaggio.getCodiceAOO());
//							if(messaggioResult!=null){
//								messaggio.setRiservato(messaggioResult.getRiservato());
//								messaggio.setSensibile(messaggioResult.getSensibile());
//							}
							messaggio.setMsgId(s.trim());
							
//							try{
//								messaggio.setSubject(messaggio.getSubject() + " [" + messDao.leggiSegnatura( messaggio.getMsgId() ) +"]");
//							}catch (Exception ex){
//								
//							}
							break;
						}
					}
				}
				if(messaggio.getMsgId()==null && messaggiDiRiferimento.size()>0){
					messaggio.setMsgId(messaggiDiRiferimento.get(0).trim());
				}
//				messaggio.setMessaggio(messagge);
			}
			
			if(messaggio.getSubject()!=null && messaggio.getSubject().equals("ANOMALIA MESSAGGIO: Undelivered Mail Returned to Sender")){
				messaggio.setTipologia("X-Ricevuta");
				messaggio.setRicevuta("errore-consegna");
			}
			
			AcquisizioneIndirizziDAO acquisizioneDao = new AcquisizioneIndirizziDAO();
			if(messaggio.getTipologia().equalsIgnoreCase("X-Ricevuta")){
				if(messaggio.getRicevuta().equals("avvenuta-consegna")){
					acquisizioneDao.updateIndirizziVerificati(messaggio.getMsgId(),StatoType.VERIFICATO, StatoType.STATO_PEC_OK,"Aggiornamento effettuato tramite ricevuta");
				}else if(	messaggio.getRicevuta().equals("avviso-mancata-consegna") ||
							messaggio.getRicevuta().equals("errore-consegna") ||
							messaggio.getRicevuta().equals("non-accettazione")
					){
					acquisizioneDao.updateIndirizziVerificati(messaggio.getMsgId(),StatoType.VERIFICATO, StatoType.STATO_PEC_KO,"Aggiornamento effettuato tramite ricevuta");
				}
			}
			// 24. ISREPLY
			messaggio.setIsReply("0");

		} catch (MessagingException e) {
			throw new NotifyMailBusinessException(-8010,
					"Errore nella costruzione del messaggio: ["
							+ messaggio.getMessageId() + "sede : "+accountPecDTO.getCodAOO()+"] Dettaglio Errore: "
							+ e.getMessage());
		}catch (Exception e) {
			throw new NotifyMailBusinessException(-8010,
					"Errore nella costruzione del messaggio: ["
					+ messaggio.getMessageId() + "sede : "+accountPecDTO.getCodAOO()+"] Dettaglio Errore: "
							+ e.getMessage());
		}
		return messaggio;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "MessageMessaggioDTOBuilder";
	}
	
	public static void bonificaAllegati (ArrayList<AllegatoDTO> allegati){
		ArrayList<String> nomeAllegati = new ArrayList<String>();
		ArrayList<AllegatoDTO> allegatiResult = new ArrayList<AllegatoDTO>();
		int i = 1;
		for(AllegatoDTO a:allegati){
			if(!nomeAllegati.contains(a.getFileName())){
				allegatiResult.add(a);
			}else{
				a.setFileName(i+"_"+a.getFileName());
				allegatiResult.add(a);
				i++;
			}
			nomeAllegati.add(a.getFileName());
		}
		allegati = allegatiResult;
	}
	
	public static List<String> readRowContent(String pattern, Enumeration headers) {
		ArrayList<String> listaParamentri = new ArrayList<String>();
		while (headers.hasMoreElements()) {
			String h = (String) headers.nextElement();
//			System.out.println(h);
			if (h.startsWith(pattern)) {
				String msgId = (h.replaceAll(pattern, "").replaceAll("\r|\t", ""));
				if(msgId!=null&&!msgId.trim().equals("")&&msgId.length()>3)
					listaParamentri.add(h.replaceAll(pattern, "").replaceAll("\r|\t", "").substring(2));
//				break;
			}
		}

		return listaParamentri;
	}
	
	public static List<String> readHeaders(String pattern, Enumeration headers) {
		ArrayList<String> listaParamentri = new ArrayList<String>();
		while (headers.hasMoreElements()) {
			Header h = (Header) headers.nextElement();
			if (h.getName().equalsIgnoreCase(pattern)) {
				listaParamentri.add(h.getValue());
			}
		}
		return listaParamentri;
	}

//	private List<AllegatoDTO> getAllegati(MessaggioDTO messaggioDTO)
//			throws BusinessException {
//		List<AllegatoDTO> out = new ArrayList<AllegatoDTO>();
////		AllegatoDTO allegato = null;
//		try {
//			Multipart multipart = (Multipart) message.getContent();
//			ByteArrayInputStream bo = new ByteArrayInputStream(messaggioDTO.getRawContent().getBytes());
//			MimeBodyPart mbp = new MimeBodyPart(bo);
//			addMultipartAttach(multipart, messaggioDTO, out);
////			elabPostaCertEML(messaggioDTO, mbp, out);
//		} catch (MessagingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//		// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return out;
//	}

	private void addMultipartAttach(Multipart multipart,
			MessaggioDTO messaggio, List<AllegatoDTO> listaAllegati)
			throws NotifyMailBusinessException {
//		InputStream is = null;

		AllegatoDTO allegato = null;

		try {

			for (int i = 0; i < multipart.getCount(); i++) {
				Part bodyPart = multipart.getBodyPart(i);
				if (!Part.ATTACHMENT
						.equalsIgnoreCase(bodyPart.getDisposition())
						&& !StringUtils.isNotBlank(bodyPart.getFileName())) {

					Object mm = bodyPart.getContent();
					if (mm instanceof Multipart){
						
						addMultipartAttach((Multipart) mm, messaggio,listaAllegati);
						
					}
//					else if (mm instanceof String) {
//						messaggioDTO.setBodyHtml((String) mm);
//						messaggioDTO.setBody((String) mm);
//					}
					else if (bodyPart.isMimeType("text/plain")){
						if(messaggio.getBody()==null)
							messaggio.setBody((String) mm);
						else
							messaggio.setPostaCertEMLBody((String)mm);
						
					}else if (bodyPart.isMimeType("text/html")){
						if(messaggio.getBodyHtml()==null){
							messaggio.setBodyHtml((String) mm);
//							 System.out.println((String) mm);
						}
					}
					continue;
				}

				allegato = new AllegatoDTO();
				allegato.setContentType(bodyPart.getContentType());
				
				allegato.setFileName(new String((bodyPart.getFileName() != null ? MimeUtility.decodeText(bodyPart.getFileName()) : "").getBytes("ISO-8859-1")));
				
				allegato.setFileData(IOUtils.toByteArray(bodyPart.getInputStream()));
				
				if(allegato.getFileName().toLowerCase().endsWith(".xml")){
					allegato.setTestoAllegato(new String(IOUtils.toByteArray(bodyPart.getInputStream())));
					messaggio.setDatiSignature(allegato.getTestoAllegato());
				}else if(allegato.getFileName().toLowerCase().endsWith(".txt") || allegato.getFileName().toLowerCase().endsWith(".eml")){
					allegato.setTestoAllegato(new String(IOUtils.toByteArray(bodyPart.getInputStream())));
				}
//				System.out.println("bodyPart.getFileName() : "+bodyPart.getFileName());
				allegato.setFileSize(allegato.getFileData().length);
				allegato.setMessageID(messaggio.getMessageId());
				allegato.setTipoAllegato(allegato.getFileName()
						.lastIndexOf(".") != -1 ? allegato.getFileName()
						.substring(allegato.getFileName().lastIndexOf(".") + 1)
						: "");
				if(messaggio.getPostedDate()!=null){
					allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
					SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
					String meseAnno = sfd.format(messaggio.getPostedDate());
					allegato.setMeseAnno(meseAnno);
				}
//				aos.close();
//				is.close();
				
				if(!listaAllegati.contains(allegato))
					listaAllegati.add(allegato);

				if ("postacert.eml".equalsIgnoreCase(allegato.getFileName())) {
					ByteArrayInputStream bo = new ByteArrayInputStream(allegato.getFileData());
    				MimeBodyPart mbp = new MimeBodyPart(bo);
    				// 28. HEADERS
    				String pemlHeaders = "";
    				Enumeration e = mbp.getAllHeaders(); 
    				while (e.hasMoreElements()) {
    					Header h = (Header) e.nextElement();
    					pemlHeaders += (h.getName()+": "+h.getValue()+"\n");
    				}
    				messaggio.setPemlHeaders(pemlHeaders);
					elabPostaCertEML(messaggio, mbp, listaAllegati);
				}
			}
//		} catch (ParseException e) {
//			throw new NotifyMailBusinessException(-8000,
//					"Errore nel recupero degli allegati del messaggio: ["
//							+ messaggioDTO.getMessageId()
//							+ "] Dettaglio Errore: " + e.getMessage());	
		} catch (IOException e) {
			
			throw new NotifyMailBusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio: ["
							+ messaggio.getMessageId()
							+ "] Dettaglio Errore: " + e.getMessage());
		} catch (MessagingException e) {
			
			throw new NotifyMailBusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio: ["
							+ messaggio.getMessageId()
							+ "] Dettaglio Errore: " + e.getMessage());
		}

	}

	private void elabPostaCertEML(MessaggioDTO messaggio, Part bodyPart,
			List<AllegatoDTO> listaAllegati) throws NotifyMailBusinessException {
		try {
			Object content = bodyPart.getContent();
			if (content instanceof MimeMessage) {
				content = ((MimeMessage) content).getContent();
			}
			
			if (bodyPart.isMimeType("text/plain")){
				
				elabUUMessage(messaggio, (String)content, listaAllegati);
		
			
				if(messaggio.getPostaCertEMLBody()==null && messaggio.getTipologia()!=null && !messaggio.getTipologia().equals("X-Ricevuta")){
					messaggio.setPostaCertEMLBody((String) content);
					// System.out.println((String) content);
				}
				
			}else if (bodyPart.isMimeType("text/html")){
				if(messaggio.getPostaCertEMLBody()==null){
					messaggio.setPostaCertEMLBody((String) content);
				}
				if(messaggio.getBodyHtml()==null){
					messaggio.setBodyHtml((String) content);
				}
			}else if (content instanceof Multipart) {
				for (int i = 0; i < ((Multipart) content).getCount(); i++) {
					Part p = ((Multipart) content).getBodyPart(i);
					try{
					if (p.getContent() instanceof Multipart) {
						
							addMultipartAttach((Multipart) p.getContent(), messaggio, listaAllegati);
						
					} else if (p instanceof Part) {
//						InputStream is = null;
						AllegatoDTO allegato = null;
						if (!Part.ATTACHMENT.equalsIgnoreCase(p.getDisposition()) && !StringUtils.isNotBlank(p.getFileName())) {
							Object mm = p.getContent();
							if (mm instanceof Multipart)
								addMultipartAttach((Multipart) mm, messaggio,listaAllegati);
//							else if (mm instanceof String) {
//								messaggio.setPostaCertEMLBody((String) mm);
//							}
							else if (p.isMimeType("text/plain")){
								if(messaggio.getPostaCertEMLBody()==null){
									messaggio.setPostaCertEMLBody((String) mm);
									// System.out.println((String) mm);
								}
								
							}else if (p.isMimeType("text/html")){
								if(messaggio.getPostaCertEMLBody()==null){
									messaggio.setPostaCertEMLBody((String) mm);
									// System.out.println((String) mm);
								}
								if(messaggio.getBodyHtml()==null){
									messaggio.setBodyHtml((String) mm);
									// System.out.println((String) mm);
								}
							}  /*else {
								callTribunaleSalernoUtil( mm, messaggio, listaAllegati );
							}*/
								
							continue;
						}

						allegato = new AllegatoDTO();
						allegato.setContentType(p.getContentType());
						allegato.setFileName(new String((p.getFileName() != null ? MimeUtility.decodeText(p.getFileName()) : "").getBytes("ISO-8859-1")));

						if(p.getFileName()==null){
							allegato.setFileName("postacert.eml");
						}
						
						allegato.setFileData(IOUtils.toByteArray(p.getInputStream()));
						allegato.setFileSize(allegato.getFileData().length);
						allegato.setMessageID(messaggio.getMessageId());
						allegato.setTipoAllegato(allegato.getFileName()
										.lastIndexOf(".") != -1  ? allegato
										.getFileName().substring(
												allegato.getFileName()
														.lastIndexOf(".") + 1)
										: "");
						if(messaggio.getPostedDate()!=null){
							allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
							SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
							String meseAnno = sfd.format(messaggio.getPostedDate());
							allegato.setMeseAnno(meseAnno);
						}
//						aos.close();
//						is.close();
						
						if(!listaAllegati.contains(allegato))
							listaAllegati.add(allegato);
						
						if ("postacert.eml".equalsIgnoreCase(allegato.getFileName())) {
							ByteArrayInputStream bo = new ByteArrayInputStream(allegato.getFileData());
		    				MimeBodyPart mbp = new MimeBodyPart(bo);
		    				// 28. HEADERS
		    				String pemlHeaders = "";
		    				Enumeration e = mbp.getAllHeaders(); 
		    				while (e.hasMoreElements()) {
		    					Header h = (Header) e.nextElement();
		    					pemlHeaders += (h.getName()+": "+h.getValue()+"\n");
		    				}
		    				messaggio.setPemlHeaders(pemlHeaders);
							elabPostaCertEML(messaggio, mbp, listaAllegati);
						}
					}else if (p.getContent() instanceof String) {
						messaggio.setPostaCertEMLBody((String) p.getContent());
					}  
					} catch (Exception e){
							AllegatoDTO allegato = new AllegatoDTO();
							allegato.setContentType(p.getContentType());
							allegato.setFileName("BodyPart.txt");
							allegato.setFileData(IOUtils.toByteArray(p.getInputStream()));
							allegato.setFileSize( allegato.getFileData().length );
							
							allegato.setMessageID(messaggio.getMessageId());
							allegato.setTipoAllegato(allegato.getFileName()
											.lastIndexOf(".") != -1  ? allegato
											.getFileName().substring(
													allegato.getFileName()
															.lastIndexOf(".") + 1)
											: "");
							if(messaggio.getPostedDate()!=null){
								allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
								SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
								String meseAnno = sfd.format(messaggio.getPostedDate());
								allegato.setMeseAnno(meseAnno);
							}
							if ( "SI".equalsIgnoreCase( ConfigProp.insBodyPart ) ){
								listaAllegati.add( allegato );
							}
						
					}
					// test
				}
			}else if(bodyPart instanceof MimeBodyPart){
				Enumeration e = bodyPart.getAllHeaders();
				while(e.hasMoreElements()){
//					 System.out.println(e.nextElement());
					 Header h = ((Header)e.nextElement());
					 System.out.println(h.getName()+" : "+h.getValue());
				}
				AllegatoDTO allegato = new AllegatoDTO();
//				allegato.setContentType(p.getContentType());
				allegato.setFileName(bodyPart.getFileName());
				allegato.setFileData(IOUtils.toByteArray(bodyPart.getInputStream()));
				allegato.setFileSize( allegato.getFileData().length );
				allegato.setMessageID(messaggio.getMessageId());
				allegato.setTipoAllegato(allegato.getFileName()
								.lastIndexOf(".") != -1  ? allegato
								.getFileName().substring(
										allegato.getFileName()
												.lastIndexOf(".") + 1)
								: "");
				if(messaggio.getPostedDate()!=null){
					allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
					SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
					String meseAnno = sfd.format(messaggio.getPostedDate());
					allegato.setMeseAnno(meseAnno);
				}
				listaAllegati.add( allegato );
			}
//		} catch (ParseException e) {
//			throw new NotifyMailBusinessException(-8000,
//					"Errore nel recupero degli allegati del messaggio: ["
//							+ messaggio.getMessageId()
//							+ "] Dettaglio Errore: " + e.getMessage());	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new NotifyMailBusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio: ["
							+ messaggio.getMessageId()
							+ "] Dettaglio Errore: " + e.getMessage());	
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			
			
			try {
				AllegatoDTO allegato = new AllegatoDTO();
				
				allegato.setFileName("BodyPart.txt");
				allegato.setContentType(bodyPart.getContentType());
				allegato.setFileData(IOUtils.toByteArray(bodyPart.getInputStream()));
				allegato.setFileSize( allegato.getFileData().length );
				
				allegato.setMessageID(messaggio.getMessageId());
				allegato.setTipoAllegato(allegato.getFileName()
								.lastIndexOf(".") != -1  ? allegato
								.getFileName().substring(
										allegato.getFileName()
												.lastIndexOf(".") + 1)
								: "");
				if(messaggio.getPostedDate()!=null){
					allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
					SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
					String meseAnno = sfd.format(messaggio.getPostedDate());
					allegato.setMeseAnno(meseAnno);
				}
				if ( "SI".equalsIgnoreCase( ConfigProp.insBodyPart ) ){
					listaAllegati.add( allegato );
				}
			} catch (IOException e1) {
				throw new NotifyMailBusinessException(-8000,
						"Errore nel recupero degli allegati del messaggio: ["
								+ messaggio.getMessageId()
								+ "] Dettaglio Errore: " + e.getMessage());	
			} catch (MessagingException e1) {
				throw new NotifyMailBusinessException(-8000,
						"Errore nel recupero degli allegati del messaggio: ["
								+ messaggio.getMessageId()
								+ "] Dettaglio Errore: " + e.getMessage());	
			}
			
			
			
			
			
		}

	}
	public void elabPostaCertEML(MessaggioDTO messaggio, Part bodyPart,
			List<AllegatoDTO> listaAllegati, boolean isTest) throws NotifyMailBusinessException {
		try {
			Object content = bodyPart.getContent();
			if (content instanceof MimeMessage) {
				content = ((MimeMessage) content).getContent();
			}
			if (bodyPart.isMimeType("text/plain")){
				if(messaggio.getPostaCertEMLBody()==null && messaggio.getTipologia()!=null && !messaggio.getTipologia().equals("X-Ricevuta")){
					messaggio.setPostaCertEMLBody((String) content);
					// System.out.println((String) content);
				}
			}else if (bodyPart.isMimeType("text/html")){
				if(messaggio.getPostaCertEMLBody()==null){
					messaggio.setPostaCertEMLBody((String) content);
					// System.out.println((String) content);
				}
				if(messaggio.getBodyHtml()==null){
					messaggio.setBodyHtml((String) content);
					// System.out.println((String) content);
				}
//				System.out.println((String) content);
			}else if (content instanceof Multipart) {
				for (int i = 0; i < ((Multipart) content).getCount(); i++) {
					Part p = ((Multipart) content).getBodyPart(i);
					if (p.getContent() instanceof Multipart) {
						addMultipartAttach((Multipart) p.getContent(), messaggio, listaAllegati);
					} else if (p instanceof Part) {
//						InputStream is = null;
						AllegatoDTO allegato = null;
						if (!Part.ATTACHMENT.equalsIgnoreCase(p.getDisposition()) && !StringUtils.isNotBlank(p.getFileName())) {
							Object mm = p.getContent();
							if (mm instanceof Multipart){
								addMultipartAttach((Multipart) mm, messaggio,listaAllegati);
//							else if (mm instanceof String) {
//								messaggio.setPostaCertEMLBody((String) mm);
//							}
							}else if (p.isMimeType("text/plain")){
								if(messaggio.getPostaCertEMLBody()==null){
									messaggio.setPostaCertEMLBody((String) mm);
									// System.out.println((String) mm);
								}
								
							}else if (p.isMimeType("text/html")){
								if(messaggio.getPostaCertEMLBody()==null){
									messaggio.setPostaCertEMLBody((String) mm);
									// System.out.println((String) mm);
								}
								if(messaggio.getBodyHtml()==null){
									messaggio.setBodyHtml((String) mm);
									// System.out.println((String) mm);
								}
							}
							continue;
						}

						allegato = new AllegatoDTO();
						allegato.setContentType(p.getContentType());
						allegato.setFileName(new String((p.getFileName() != null ? MimeUtility.decodeText(p.getFileName()) : "").getBytes("ISO-8859-1")));
						allegato.setFileData(IOUtils.toByteArray(p.getInputStream()));
						allegato.setFileSize(allegato.getFileData().length);
						allegato.setMessageID(messaggio.getMessageId());
						allegato.setTipoAllegato(allegato.getFileName()
										.lastIndexOf(".") != -1  ? allegato
										.getFileName().substring(
												allegato.getFileName()
														.lastIndexOf(".") + 1)
										: "");
						if(messaggio.getPostedDate()!=null){
							allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
							SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
							String meseAnno = sfd.format(messaggio.getPostedDate());
							allegato.setMeseAnno(meseAnno);
						}
//						aos.close();
//						is.close();
						
						if(!listaAllegati.contains(allegato))
							listaAllegati.add(allegato);
						
					}else if (p.getContent() instanceof String) {
						messaggio.setPostaCertEMLBody((String) p.getContent());
					}  
					// test
				}
			}
//		} catch (ParseException e) {
//			throw new NotifyMailBusinessException(-8000,
//					"Errore nel recupero degli allegati del messaggio: ["
//							+ messaggio.getMessageId()
//							+ "] Dettaglio Errore: " + e.getMessage());	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new NotifyMailBusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio: ["
							+ messaggio.getMessageId()
							+ "] Dettaglio Errore: " + e.getMessage());	
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			throw new NotifyMailBusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio: ["
							+ messaggio.getMessageId()
							+ "] Dettaglio Errore: " + e.getMessage());	
		}

	}
	public static void handleMultipart(Multipart multipart)
			throws MessagingException, IOException {
		for (int i = 0, n = multipart.getCount(); i < n; i++) {
			handlePart(multipart.getBodyPart(i));
		}
	}

	public static void handlePart(Part part) throws MessagingException,
			IOException {
		String disposition = part.getDisposition();
		String contentType = part.getContentType();
		if (disposition == null) { // When just body
			// System.out.println("Null: " + contentType);
			// Check if plain
			if ((contentType.length() >= 10)
					&& (contentType.toLowerCase().substring(0, 10).equals("text/plain"))) {
				part.writeTo(System.out);
			} else { // Don't think this will happen
				// System.out.println("Other body: " + contentType);
//				part.writeTo(System.out);
				// System.out.println("Other body fileName: " + part.getFileName());
			}
		} else if (disposition.equalsIgnoreCase(Part.ATTACHMENT)) {
			// System.out.println("Attachment: " + part.getFileName() + " : "+ contentType);
			saveFile(part.getFileName(), part.getInputStream());
		} else if (disposition.equalsIgnoreCase(Part.INLINE)) {
			// System.out.println("Inline: " + part.getFileName() + " : "+ contentType);
			saveFile(part.getFileName(), part.getInputStream());
		} else { // Should never happen
			// System.out.println("Other: " + disposition);
		}
	}

	public static void saveFile(String filename, InputStream input)
			throws IOException {
		if (filename == null) {
			filename = File.createTempFile("xx", ".out").getName();
		}
		// Do no overwrite existing file
		File file = new File(filename);
		for (int i = 0; file.exists(); i++) {
			file = new File(filename + i);
		}
		FileOutputStream fos = new FileOutputStream(file);
		BufferedOutputStream bos = new BufferedOutputStream(fos);

		BufferedInputStream bis = new BufferedInputStream(input);
		int aByte;
		while ((aByte = bis.read()) != -1) {
			bos.write(aByte);
		}
		bos.flush();
		bos.close();
		bis.close();
	}
	
	public static void elaboraDestinatari(MimeBodyPart bodypart, MessaggioDTO messaggio){
		
		try {
			List<String> listaDestinatari = readHeaders("To", bodypart.getAllHeaders());
			if(listaDestinatari!=null && !listaDestinatari.isEmpty()){
				StringTokenizer stdest = new StringTokenizer(listaDestinatari.get(0), ";|,");
				while(stdest.hasMoreElements()){
					String s = (String)stdest.nextElement(); 
					String addr = s.indexOf("<") == -1 ? s : s.substring(s.indexOf("<") + 1, s.length() - 1);
					String name = s.indexOf("<") != -1 ? s.substring(0, s.indexOf("<")) : "";
					DestinatarioDTO dest = new DestinatarioDTO();
					dest.setAddr(addr.trim());
					dest.setName(name);
					dest.setPhrase(s.trim());
					dest.setType("Per");
					messaggio.getElencoDestinatariPer().add(dest);
				}
			}
			List<String> listaDestinatariCC = readHeaders("Cc", bodypart.getAllHeaders());
			if(listaDestinatariCC!=null && !listaDestinatariCC.isEmpty()){
				StringTokenizer stdest = new StringTokenizer(listaDestinatariCC.get(0), ";|,");
				while(stdest.hasMoreElements()){
					String s = (String)stdest.nextElement(); 
					String addr = s.indexOf("<") == -1 ? s : s.substring(s.indexOf("<") + 1, s.length() - 1);
					String name = s.indexOf("<") != -1 ? s.substring(0, s.indexOf("<")) : "";
					DestinatarioDTO dest = new DestinatarioDTO();
					dest.setAddr(addr.trim());
					dest.setName(name);
					dest.setPhrase(s.trim());
					dest.setMessageID(messaggio.getMessageId());
					dest.setType("cc");
					messaggio.getElencoDestinatariCc().add(dest);
				}
			}
		} catch (MessagingException e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		}	
	}
	
	
	private void elabUUMessage( MessaggioDTO messaggio, String content, List<AllegatoDTO> listaAllegati ) throws MessagingException, IOException, NotifyMailBusinessException{
		//InputStream is = MimeUtility.decode(bodyPart.getInputStream(), "uuencode");
		if ( ((String) content).indexOf( UU_DELIM ) != -1 ){
			InputStream is = MimeUtility.decode(new ByteArrayInputStream(content.getBytes()), "uuencode");
			AllegatoDTO allegato = new AllegatoDTO();
			
			if ( is instanceof UUDecoderStream ){
				allegato.setFileName(( (UUDecoderStream)is ).getName() );
			}
			
			allegato.setFileData(IOUtils.toByteArray(is));
			allegato.setFileSize(allegato.getFileData().length);
			allegato.setMessageID(messaggio.getMessageId());
			
			allegato.setTipoAllegato(allegato.getFileName()
							.lastIndexOf(".") != -1  ? allegato
							.getFileName().substring(
									allegato.getFileName()
											.lastIndexOf(".") + 1)
							: "");
			
			if(messaggio.getPostedDate()!=null){
				allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
				SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
				String meseAnno = sfd.format(messaggio.getPostedDate());
				allegato.setMeseAnno(meseAnno);
			}
	
			if(!listaAllegati.contains(allegato))
				listaAllegati.add(allegato);
			
			
			//Elaborazione anche di ogni .EML allegato
			/*if (allegato.getFileName() != null && allegato.getFileName().toUpperCase().indexOf(".EML") != -1) {
				ByteArrayInputStream bo = new ByteArrayInputStream(allegato.getFileData());
				MimeBodyPart mbp = new MimeBodyPart(bo);
				
				String pemlHeaders = "";
				Enumeration e = mbp.getAllHeaders(); 
				while (e.hasMoreElements()) {
					Header h = (Header) e.nextElement();
					pemlHeaders += (h.getName()+": "+h.getValue()+"\n");
				}
				messaggio.setPemlHeaders(pemlHeaders);
				elabPostaCertEML(messaggio, mbp, listaAllegati);
			}*/
			
			StringBuilder sb = new StringBuilder();
			sb.append(content.substring(0, content.indexOf( UU_DELIM )));
			String x = content.substring(content.indexOf( UU_DELIM )+9 );
			if ( x.indexOf( UU_DELIM ) != -1 ){
				sb.append( x.substring(x.indexOf( UU_DELIM )) );
				elabUUMessage(messaggio, sb.toString(), listaAllegati);
			}
			
			
			
	
		}
		
	}
	
	

	
	private void callTribunaleSalernoUtil( Object mm, MessaggioDTO messaggio, List<AllegatoDTO> listaAllegati ) throws IOException, MessagingException, NotifyMailBusinessException{
		
		if ( mm instanceof MimeMessage ){
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			((MimeMessage)mm).writeTo(baos);
			byte[] bytes = baos.toByteArray(); 

			AllegatoDTO allegato = new AllegatoDTO();
			allegato.setContentType("");
			allegato.setFileName("Allegato.eml");
			
			allegato.setFileData(bytes);
			allegato.setFileSize(allegato.getFileData().length);
			allegato.setMessageID(messaggio.getMessageId());
			allegato.setTipoAllegato("EML");
			
			listaAllegati.add( allegato );
			
			//Scommentando Questo BLOCCO DI CODICE SI AGGIUNGONO GLI ALLEGATI DELL'EML
			//-----------------------------------------------------------------------
			/*mm = ((MimeMessage) mm).getContent();
			
			if ( mm instanceof Multipart )
				addMultipartAttach((Multipart)mm, messaggio, listaAllegati);
			*/
			//-----------------------------------------------------------------------
			//elabPostaCertEML(messaggio, mbp, listaAllegati);

		}
	}
}
