package it.inps.agentPec.verificaPec.business;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.mail.ReadAndElabMessageCommand;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.hermes.dao.MessaggioDAO;
import it.inps.agentPec.verificaPec.dao.AcquisizioneIndirizziDAO;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;
import it.inps.agentPec.verificaPec.dao.MessaggiVerificaPecDAO;
import it.inps.agentPec.verificaPec.dto.AcquisizioneIndirizziDTO;
import it.inps.agentPec.verificaPec.dto.DestinatarioPecVerifica;
import it.inps.agentPec.verificaPec.dto.MessaggioPecVerifica;
import it.inps.agentPec.verificaPec.util.StatoType;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class CreaMessaggiPecInUscita implements Command, Callable<Object> {

	private AcquisizioneIndirizziDTO indirizzo;
	private MessaggioPecVerifica messaggio;
	
	public CreaMessaggiPecInUscita( AcquisizioneIndirizziDTO indirizzo , MessaggioPecVerifica messaggio){
		this.indirizzo = indirizzo;
		this.messaggio = messaggio;
	}
	@Override
	public Object execute() throws BusinessException {
		ConfigDAO config = new ConfigDAO();
		messaggio.setSubject(config.getProperty("Subject",""));
		messaggio.setBody(config.getProperty("Body",""));
		messaggio.setMessageId(indirizzo.getMessageId());
		messaggio.setMsgId(indirizzo.getMessageId());
		messaggio.setUiDoc(indirizzo.getUID());
		messaggio.setPostedDate(new Date(System.currentTimeMillis()));
		messaggio.setStato("DI");
		messaggio.setAccountPec(config.getProperty("accountPec",""));
		messaggio.setTipoRicevuta("completa");
		messaggio.setVerso("U");
		messaggio.setTipoMessaggio("posta-certificata");
		messaggio.setXTrasporto("posta-certificata");
		
		List<DestinatarioDTO> destinatariPer = new ArrayList<DestinatarioDTO>();
		DestinatarioDTO destinatarioPer = new DestinatarioDTO();
		destinatarioPer.setAddr(indirizzo.getEmail());
		destinatarioPer.setMessageID(indirizzo.getMessageId());
		destinatarioPer.setType("per");
		destinatarioPer.setPostedDate(messaggio.getPostedDate());
		destinatariPer.add(destinatarioPer);

		messaggio.setElencoDestinatariPer(destinatariPer);
		
		MessaggiVerificaPecDAO messaggioDao  = new MessaggiVerificaPecDAO();
		AcquisizioneIndirizziDAO acquisizioneDao = new AcquisizioneIndirizziDAO();
		try{
			messaggioDao.inserisciMessaggioVerificaPec(null,messaggio);
			acquisizioneDao.updateIndirizziDaValidare(indirizzo,indirizzo.getMessageId(),StatoType.IN_FASE_DI_CONVALIDA);
		}catch(DAOException de){
			throw new BusinessException(500, de.getMessage());
		}
		return new Integer(1);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "RiceviMessaggiPECInIngresso";
	}
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
