package it.inps.agentPec.verificaPec.business;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.inps.agentPec.verificaPec.dao.StatisticheHermesMsgInOutDAO;

import java.util.concurrent.Callable;

public class EsecuzioneProceduraStatisticheHermes implements Command, Callable<Object> {
	private Integer anno;
    private Integer mese;
    private Integer giorno;
    
	public EsecuzioneProceduraStatisticheHermes(Integer anno, Integer mese, Integer giorno)
	{
		this.anno = anno;
		this.mese = mese;
		this.giorno = giorno;
	}
	
	@Override
	public Object execute() throws BusinessException 
	{
		Integer ret = 0;
		
		try
		{
			StatisticheHermesMsgInOutDAO daoSPStataHermes = new StatisticheHermesMsgInOutDAO();
			ret = daoSPStataHermes.spCaricaStatisticheHermesMsgInOut(this.anno, this.mese, this.giorno);
		}
		catch(Exception e)
		{
			throw new NotifyMailBusinessException(-4000, "Errore in EsecuzioneProceduraStatistiche : Dettaglio Errore: " + e.getMessage());
		}
		
		return ret;
	}

	private Object NotifyMailBusinessException(int i, String string) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "EsecuzioneProceduraStatistiche";
	}
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
