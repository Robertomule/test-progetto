package it.inps.agentPec.verificaPec.business;

import java.util.List;
import java.util.concurrent.Callable;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioUscitaCommand;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.DebugConstants;
import it.eustema.inps.agentPecPei.util.TripleDES;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;
import it.inps.agentPec.verificaPec.dao.MessaggiVerificaPecDAO;
import it.inps.agentPec.verificaPec.util.PostaVerificaPec;

/**
 * Classe Command che si occupa di inviare una PEC a partire da un messaggioDTO
 * @author MCARPENTIERI
 *
 */
public class InviaPecVerificaCommand implements Command, Callable<Object> {

	private MessaggioDTO messaggio;
	private AccountPecDTO accountPEC;
	
	public InviaPecVerificaCommand( MessaggioDTO messaggio,AccountPecDTO accountPEC ){
		this.messaggio = messaggio;
		this.accountPEC = accountPEC;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		try {
			TripleDES crittografia = new TripleDES();
			ConfigDAO configDao = new ConfigDAO();
			
//			accountPEC.setUserName(configDao.getProperty("accountPec",""));
			accountPEC.setPassword(crittografia.decrypt(accountPEC.getPassword()));
			
			if ( accountPEC == null ){
				throw new NotifyMailBusinessException( -4010, "Account PEC [" +messaggio.getAccountPec() +"] non esistente su DB" );
			}
			
			if ( DebugConstants.ENABLE_DEBUG_CONSTANTS ){
				accountPEC.setUserName( DebugConstants.ACCOUNT_PEC_USER_NAME );
				accountPEC.setPassword( DebugConstants.ACCOUNT_PEC_USER_PASSWORD );
			}
			PostaVerificaPec posta = new PostaVerificaPec();
			posta.inviaEmail(accountPEC, messaggio.getElencoDestinatariPer(), messaggio.getElencoDestinatariCc(), messaggio.getSubject(), messaggio.getBody(), messaggio.getElencoFileAllegati(), messaggio.getMessageId(), messaggio );
			MessaggiVerificaPecDAO messaggiVerificaPecDao = new MessaggiVerificaPecDAO();
			messaggio.setStato("IN");
			messaggio.setAccountPec(accountPEC.getUserName());
			messaggiVerificaPecDao.aggiornaMsgUscenteInviato(null, messaggio);
			
		} catch ( DAOException de ){
			throw new NotifyMailBusinessException( -4010, "Errore nel recupero ddati ell'account PEC [" +messaggio.getAccountPec() +"] Dettaglio Errore: "+ de.getMessage() );
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new NotifyMailBusinessException(-4000, "Errore nell'invio AccountPEC all'account: [" + messaggio.getAccountPec() +"] Dettaglio Errore: " + e.getMessage());
		}
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "InviaPecVerificaCommand";
	}
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
