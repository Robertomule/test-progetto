package it.inps.agentPec.verificaPec.business;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dao.LogInfoCasellePecDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.LogInfoCasellePecDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.util.DebugConstants;
import it.eustema.inps.agentPecPei.util.TripleDES;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.logging.LoggingConfigurator;

import java.util.Properties;
import java.util.concurrent.Callable;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Quota;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.log4j.Logger;

import com.sun.mail.imap.IMAPStore;

public class LogInfoCasellePecCommand implements Command, Callable<Object>
{
	private static Logger log = Logger.getLogger(LogInfoCasellePecCommand.class);
	private AccountPecDTO accountPecDTO;
	
	public LogInfoCasellePecCommand(AccountPecDTO accountPecDTO){
		this.accountPecDTO = accountPecDTO;
	}
	
	@Override
	public Object execute() throws BusinessException {
        Store store = null;

		try {
			Properties props = System.getProperties();

		    props.setProperty("mail.host", ConfigProp.hostPopAccountPEC);
		    props.setProperty("mail.imaps.port", "993");
		    props.setProperty("mail.store.protocol", "imaps") ;		    
		    props.setProperty("mail.imaps.ssl.trust",accountPecDTO.getSslTrustPop());
	        Session session = Session.getDefaultInstance(props, null);
	        //session.setDebug(true);
	        TripleDES crittografia = new TripleDES();
	        
	        store = session.getStore();
	        if (DebugConstants.ENABLE_DEBUG_CONSTANTS)
				try {
					store.connect(DebugConstants.ACCOUNT_PEC_USER_NAME, crittografia.decrypt(DebugConstants.ACCOUNT_PEC_USER_PASSWORD));
				} catch(Exception e) {
	        		log.error(e.getStackTrace());
	    			throw new NotifyMailBusinessException(-2020, "Errore nella connessione con l'utenza [" + accountPecDTO.getUserName() + "] nell'account PEC[ " + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());	
				}
			else
				store.connect(accountPecDTO.getUserName() + ConfigProp.dominioPEC, accountPecDTO.getPassword());
	        
	        if (!IMAPStore.class.isInstance(store))
	            throw new IllegalStateException("Is not IMAPStore");

	        IMAPStore imapStore = (IMAPStore) store;
	        Quota[] quotas = imapStore.getQuota("INBOX");
	        LogInfoCasellePecDAO daoLogInfo = new LogInfoCasellePecDAO();
	        
	        for (Quota quota : quotas) {
	            //System.out.println(String.format("quotaRoot:'%s'", quota.quotaRoot));

	            for (Quota.Resource resource : quota.resources) {
	            	LogInfoCasellePecDTO logInfoDTO = new LogInfoCasellePecDTO();
	            	logInfoDTO.setAccountPEC(this.accountPecDTO.getUserName());
	            	logInfoDTO.setCodiceSede(this.accountPecDTO.getCodAOO());
	            	logInfoDTO.setLimite(resource.limit);
	            	logInfoDTO.setUtilizzato(resource.usage);
	            	logInfoDTO.setLibero(resource.limit - resource.usage);
	                
	            	daoLogInfo.insertLogInfo(logInfoDTO);
	            	
	            	//System.out.println(String.format("name:'%s', limit:'%s', usage:'%s'", resource.name, resource.limit, resource.usage));
	            }
	        }

		    store.close();
		
		} catch (NoSuchProviderException e) {
			log.error(e.getMessage());
			System.err.println("Errore nella connessione con l'account PEC[" + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());
			throw new NotifyMailBusinessException(-2010, "Errore nella connessione con l'account PEC[" + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());	
		} catch (MessagingException e) {
			log.error(e.getMessage());
			System.err.println("Errore nella connessione con l'utenza [" + accountPecDTO.getUserName() + "] nell'account PEC[ " + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());
			throw new NotifyMailBusinessException(-2020, "Errore nella connessione con l'utenza [" + accountPecDTO.getUserName() + "] nell'account PEC[ " + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());	
		} catch(IllegalStateException e) {
			log.error(e.getMessage());
			System.err.println("Errore nella connessione con l'account PEC[ " + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());
			throw new NotifyMailBusinessException(-2010, "Errore nella connessione con l'account PEC[ " + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());	
		} catch (Exception e) {
			e.printStackTrace();	
			log.error(e.getStackTrace());		
		} finally {          
	          if (store != null) 
	          { 
	        	  try 
	        	  {
	        		  store.close();
				  } 
	        	  catch (MessagingException e) 
	        	  {
	        		  e.printStackTrace();
				  } 
	          }
        }
	        
		return null;
			
	}
	
	public static void main( String[] args ) {
		try {
			
			new ConfigProp().init();
				
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
			AccountPecDTO acc = new AccountPecDTO();
			acc.setUserName( "utente03" );
			acc.setCodAOO("0040");

		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Read Caselle PEC info";
	}
	
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}
}
