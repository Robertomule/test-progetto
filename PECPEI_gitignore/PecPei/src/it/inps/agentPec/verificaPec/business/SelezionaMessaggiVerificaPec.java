package it.inps.agentPec.verificaPec.business;

import java.util.List;
import java.util.concurrent.Callable;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.util.DebugConstants;
import it.eustema.inps.utility.Posta;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;
import it.inps.agentPec.verificaPec.dao.MessaggiVerificaPecDAO;

/**
 * Classe Command che si occupa di selezionare le PEC che devono essere inviate
 * @author MCARPENTIERI
 *
 */
public class SelezionaMessaggiVerificaPec implements Command, Callable<Object> {
	
	public SelezionaMessaggiVerificaPec(){
	}
	@Override
	public List<MessaggioDTO> execute() throws BusinessException {
		// TODO Auto-generated method stub
		List<MessaggioDTO> result = null;
		try {
			MessaggiVerificaPecDAO messaggi = new MessaggiVerificaPecDAO();
			result = messaggi.selezionaMessaggiInUscitaPEC();
			
		} catch ( DAOException de ){
			throw new NotifyMailBusinessException( -4010, "Errore nel recupero dati dell'account PEC : Dettaglio Errore: "+ de.getMessage() );
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new NotifyMailBusinessException(-4000, "Errore nel recupero dati dell'account PEC : Dettaglio Errore: "+ e.getMessage());
		}
		return result;
	}

	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "SelezionaMessaggiVerificaPec";
	}

}
