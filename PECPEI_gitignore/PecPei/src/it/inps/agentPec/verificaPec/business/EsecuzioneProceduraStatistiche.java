package it.inps.agentPec.verificaPec.business;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.mail.ReadAndElabMessageCommand;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.hermes.dao.MessaggioDAO;
import it.inps.agentPec.verificaPec.dao.AcquisizioneIndirizziDAO;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;
import it.inps.agentPec.verificaPec.dao.MessaggiVerificaPecDAO;
import it.inps.agentPec.verificaPec.dao.StatistichePECPEIMsgInOutDAO;
import it.inps.agentPec.verificaPec.dto.AcquisizioneIndirizziDTO;
import it.inps.agentPec.verificaPec.dto.DestinatarioPecVerifica;
import it.inps.agentPec.verificaPec.dto.MessaggioPecVerifica;
import it.inps.agentPec.verificaPec.util.StatoType;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class EsecuzioneProceduraStatistiche implements Command, Callable<Object> {
	private Integer anno;
    private Integer mese;
    private Integer giorno;
    
	public EsecuzioneProceduraStatistiche(Integer anno, Integer mese, Integer giorno)
	{
		this.anno = anno;
		this.mese = mese;
		this.giorno = giorno;
	}
	
	@Override
	public Object execute() throws BusinessException 
	{
		Integer ret = 0;
		
		try
		{
			StatistichePECPEIMsgInOutDAO daoSPStataPecPei = new StatistichePECPEIMsgInOutDAO();
			ret = daoSPStataPecPei.spCaricaStatistichePECPEIMsgInOut(this.anno, this.mese, this.giorno);
		}
		catch(Exception e)
		{
			throw new NotifyMailBusinessException(-4000, "Errore in EsecuzioneProceduraStatistiche : Dettaglio Errore: " + e.getMessage());
		}
		
		return ret;
	}

	private Object NotifyMailBusinessException(int i, String string) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "EsecuzioneProceduraStatistiche";
	}
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
