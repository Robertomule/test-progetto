package it.inps.agentPec.verificaPec.business;

import java.util.concurrent.Callable;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.mail.ReadAndElabMessageCommand;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;

public class RiceviMessaggiVerificaPecCommand implements Command, Callable<Object> {

	public RiceviMessaggiVerificaPecCommand(){}
	
	@Override
	public Object execute() throws BusinessException {
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		AccountPecDTO accountPEC = new AccountPecDTO();
		ConfigDAO configDao = new ConfigDAO();
		accountPEC.setUserName(configDao.getProperty("accountPec",""));
		accountPEC.setPassword(configDao.getProperty("passwordPec",""));

		try {
			executor.setCommand( new ReadAndElabMessageVerificaCommand(accountPEC) );
			executor.executeCommandAndLog();
		} catch ( BusinessException e){
			//Qualsiasi Errore, si passa all'elaborazione dell'account successivo
		}
		return new Integer(1);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "RiceviMessaggiVerificaPecCommand";
	}
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
