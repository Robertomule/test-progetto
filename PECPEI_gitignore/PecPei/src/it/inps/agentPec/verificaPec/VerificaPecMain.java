package it.inps.agentPec.verificaPec;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import it.eustema.inps.agentPecPei.AgentCallable;
import it.eustema.inps.agentPecPei.aggiornaIndicePA.AggiornaIndicePaFile;
import it.eustema.inps.agentPecPei.aggiornaIndicePA.ConnessioneIndicePA;
import it.eustema.inps.agentPecPei.aggiornaTitolari.TitolarioThread;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dto.AgentExecutionLogDTO;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.manager.CommandExecutorMultiThread;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;
import it.eustema.inps.agentPecPei.util.AgentStatus;
import it.eustema.inps.agentPecPei.util.AgentType;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.logging.LoggingConfigurator;
import it.inps.agentPec.verificaPec.dao.Agent_execution_logDAO;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;

public class VerificaPecMain {

	private static Log log = LogFactory.getLog("it.inps.agentPec.verificaPec");
	
	static {
        log.error("VerificaPecMain :: Esegue blocco di inizializzazione");
        System.out.println("VerificaPecMain :: Esegue blocco di inizializzazione");
		//blocco eseguito una volta al caricamento della classe
        try {
			// carica i parametri dal file di cfg
			//new ConfigProp(cfgFileName);
			new ConfigProp().init();
			
			// inizializzazione configurazione del logging
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
		} catch (Exception e) {
			System.err.println("" + e.getMessage());
		}        
    }

	@SuppressWarnings({ "unused", "unchecked" })
	public void run()  throws Exception {
			
			ExecutorService executor;
			CommandExecutor executorService = CommandExecutorManager.getInstance().getExecutor();
			
			Set<Future<String>> futureSet = new HashSet<Future<String>>();
			Agent_execution_logDAO execLogDAO = new Agent_execution_logDAO();
			AgentExecutionLogDTO executionLogDTO = new AgentExecutionLogDTO();
			
			int executionID = -1;
			
			Callable<String> verificaPec;
			Callable<String> agentPEI;
			Callable<String> verificaAcquisizioni;
			
			Future<String> futureVerificaPEC;
			Future<String> futurePEI;
			Future<String> futureVerificaAcquisizioni;

			ConfigDAO configdao = new ConfigDAO();
			
			try {
				executionLogDTO.setAgent_type(AgentExecutionLogDTO.type_verificapec);
				executionID = execLogDAO.insertAgentExecution(executionLogDTO);
				executionLogDTO.setExecution_id(executionID);
				
				if(executionLogDTO.getAgent_status().equals(AgentStatus.STARTED.name())) {
					
					log.error("VerificaPecMain :: Caso Agent Verifica PEC");
					System.out.println("VerificaPecMain :: Caso Agent Verifica PEC");
					
					executor = Executors.newFixedThreadPool(3);

					log.error("VerificaPecMain :: Inizio Verifica Acquisizioni");
					System.out.println("VerificaPecMain :: Inizio Verifica Acquisizioni");
					
					verificaAcquisizioni = new VerificaAcquisizioniCallable();
					futureVerificaAcquisizioni = executor.submit(verificaAcquisizioni);
					futureSet.add(futureVerificaAcquisizioni);

					log.error("VerificaPecMain :: Inizio Verifica PEC");
					System.out.println("VerificaPecMain :: Inizio Verifica PEC");
					
					verificaPec = new AgentCallable(AgentType.VERIFICAPEC,-2);
					futureVerificaPEC = executor.submit(verificaPec);
					futureSet.add(futureVerificaPEC);

					log.error("VerificaPecMain :: Inizio Agent PEI");
					System.out.println("VerificaPecMain :: Inizio Agent PEI");
					
					agentPEI = new AgentCallable(AgentType.PEI,-2);
					futurePEI = executor.submit(agentPEI);
					futureSet.add(futurePEI);
					
				}
				
				log.error("VerificaPecMain :: Fine esecuzione Step");
				System.out.println("VerificaPecMain :: Fine esecuzione Step");
				
				boolean stepsTerminated = true; 
				for (Future<String> retCode : futureSet) {
					log.error("VerificaPecMain :: retCode = " + retCode.get());
					System.out.println("VerificaPecMain :: retCode = " + retCode.get());	
					
					if(!retCode.isDone()) {
						stepsTerminated = false;
					}
				}
				
				log.error("VerificaPecMain :: stepsTerminated = " + stepsTerminated);
				System.out.println("VerificaPecMain :: stepsTerminated = " + stepsTerminated);
				
				if(stepsTerminated) {
					long tempoEndAgent = System.currentTimeMillis();
					executionLogDTO.setEnd_time(new Timestamp(tempoEndAgent));
					executionLogDTO.setAgent_status(AgentStatus.COMPLETED.name());
					execLogDAO.updateAgentExecution(executionLogDTO);		
				}
				
				CommandExecutor executorUpdate = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, 2);
				
//				Inserita possibilitÓ di far partire il thread dei titolari
				if(Boolean.parseBoolean(configdao.getProperty("abilitaSincronizzaTitolari", "true"))) {
					try {
						executorUpdate.setCommand(new TitolarioThread());
						executorUpdate.executeCommandAndLog();
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				
				log.error("Indici PA --> Carico il flag abilitaUpdateIndiciPA");
				System.out.println("Indici PA --> Carico il Flag abilitaUpdateIndiciPA");
				
//				Inserita possibilitÓ di far partire il thread per aggiornare gli IndiciPA				
				if(Boolean.parseBoolean(configdao.getProperty("abilitaUpdateIndiciPA", "true"))) {
					try {
						log.error("Indici PA --> Flag abilitaUpdateIndiciPA a true");
						System.out.println("Indici PA --> Flag abilitaUpdateIndiciPA a true");
						
						if(Boolean.parseBoolean(configdao.getProperty("nuovaProceduraIndiciPA", "true"))) {
							log.error("Indici PA --> Flag nuovaProceduraIndiciPA a true");
							System.out.println("Indici PA --> Flag nuovaProceduraIndiciPA a true");
							
							executorUpdate.setCommand(new AggiornaIndicePaFile());
							executorUpdate.executeCommandAndLog();
						} else {
							log.error("Indici PA --> Flag nuovaProceduraIndiciPA a false");
							System.out.println("Indici PA --> Flag nuovaProceduraIndiciPA a false");
							
							executorUpdate.setCommand(new ConnessioneIndicePA());
							executorUpdate.executeCommandAndLog();
						}
					} catch(Exception e) {
						log.error("Indici PA --> Flag nuovaProceduraIndiciPA a false");
						System.out.println("Indici PA --> Flag nuovaProceduraIndiciPA a false");
						
						e.printStackTrace();
					}
				} else {
					log.error("Indici PA --> Flag abilitaUpdateIndiciPA a false");
					System.out.println("Indici PA --> Flag abilitaUpdateIndiciPA a false");
				}
				
				List<Object> out = ((CommandExecutorMultiThread)executorUpdate).waitResult();
				
			} catch(Exception e){
				executorService.setCommand( new SendMailCommand( new NotifyMailBusinessException( -9999, "Errore caricamento parametri di configurazione verificaPec: " + e.getMessage() ) ) );
				executorService.executeCommand();
			}
	}
	
	public static void main(String[] args){
		try {
			log.error("main :: START");
			System.out.println("main :: START");
			
			//26/01/2015 Nuove modifica NotificaErrori
	    	System.setProperty("NotificaErrori.DbName", Constants.DB_VERIFICAPEC);
	    	
			VerificaPecMain main = new VerificaPecMain();
			main.run();
			System.exit( 0 );
			
			//26/01/2015 Nuove modifica NotificaErrori
			System.setProperty("NotificaErrori.DbName", "");
			
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}