package it.inps.agentPec.verificaPec.util;

public enum VerificaPecType {

	VERIFICA_PEC(1), 
//    PEI(2),
//    HERMES(3),
//    PEC_PEI(4),
//    PEC_HERMES(5),
//    PEI_HERMES(6),
//    PEC_PEI_HERMES(7),
    ;

    private int typeId;

    VerificaPecType(int typeId) {
        this.typeId = typeId;
    }

    public int getTypeId() { 
        return typeId;
    }
}
