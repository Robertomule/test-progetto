package it.inps.agentPec.verificaPec.util;

public enum StatoType {

	DA_VERIFICARE("DV"),
	IN_FASE_DI_CONVALIDA("IFC"),
	VERIFICATO("VE"),
	STATO_PEC_OK("OK"),
	STATO_PEC_KO("KO");
	

    private String statoId;

    StatoType(String typeId) {
        this.statoId = typeId;
    }

    public String getStatoId() { 
        return statoId;
    }
}