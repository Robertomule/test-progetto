package it.inps.agentPec.verificaPec.manager;

import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.exception.PecBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.manager.CommandExecutorMultiThread;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;
import it.inps.agentPec.verificaPec.business.LogInfoCasellePecCommand;

import java.util.List;

import org.apache.log4j.Logger;

public class VerificaCasellePec_Manager {

	private static Logger log = Logger.getLogger(VerificaCasellePec_Manager.class);
	private static int idBatch;
	
	public VerificaCasellePec_Manager(int batchId){
			idBatch = batchId;
	}
	
	//@SuppressWarnings("unchecked")
	public String elabora() throws PecBusinessException {
		
		log.info("VerificaCasellePec_Manager :: elabora");
		String exitCode = "";
		
		try{

			CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, 10);
			List<AccountPecDTO> listaAccountPEC = null;
			//Lettura di tutti gli account PEC attivi e non temporaneamente disabilitati
			AccountPecDAO accountPecDAO = new AccountPecDAO();
			List<String> codAOOAccounts = new AccountPecDAO().selectAllAOO(idBatch);
			for(String account: codAOOAccounts)
			{
				listaAccountPEC = accountPecDAO.selectAllAccountPecAttivi( account );
				for(AccountPecDTO acc:listaAccountPEC){ 
					executor.setCommand( new LogInfoCasellePecCommand(acc));
					executor.executeCommandAndLog();
				}
			}			
			
			List<Object> out = ((CommandExecutorMultiThread)executor).waitResult();
			
		}catch (Exception e){
			e.printStackTrace();
			throw new PecBusinessException(e.getMessage());			
		}
		
		log.info("VerificaCasellePec_Manager :: fine");
		
		
		return exitCode;
	}
	
	/*public static void main(String args[]){
		System.out.println(7%7);
	}*/
}
