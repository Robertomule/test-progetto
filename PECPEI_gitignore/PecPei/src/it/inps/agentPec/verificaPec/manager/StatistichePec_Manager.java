package it.inps.agentPec.verificaPec.manager;

import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.exception.PecBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.manager.CommandExecutorMultiThread;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;
import it.inps.agentPec.verificaPec.business.AggiornaStatoIndirizzo48H;
import it.inps.agentPec.verificaPec.business.CreaMessaggiPecInUscita;
import it.inps.agentPec.verificaPec.business.EsecuzioneProceduraStatistiche;
import it.inps.agentPec.verificaPec.business.InviaPecVerificaCommand;
import it.inps.agentPec.verificaPec.business.ReadAndElabMessageVerificaCommand;
import it.inps.agentPec.verificaPec.business.SelezionaMessaggiVerificaPec;
import it.inps.agentPec.verificaPec.dao.AcquisizioneIndirizziDAO;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;
import it.inps.agentPec.verificaPec.dao.MessaggiVerificaPecDAO;
import it.inps.agentPec.verificaPec.dto.AccountVerificaPec;
import it.inps.agentPec.verificaPec.dto.AcquisizioneIndirizziDTO;
import it.inps.agentPec.verificaPec.dto.MessaggioPecVerifica;

import java.util.List;

import org.apache.log4j.Logger;

public class StatistichePec_Manager {
	private static Logger log = Logger.getLogger(StatistichePec_Manager.class);
	
	
	@SuppressWarnings("unchecked")
	public String elabora() throws PecBusinessException {
		
		log.info("StatistichePec_Manager :: elabora");
		String exitCode = "";
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, 30);
		CommandExecutor executorSend = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, 30);
		int anno=0;
		int mese=0;
		try {
			ConfigDAO configDao = new ConfigDAO();
			
			//Per processare la data odierna basta eseguire executor.setCommand( new EsecuzioneProceduraStatistiche(null , null, null) )
			for(anno = 2010; anno <= 2015; anno++)
			{
				for(mese = 1; mese <= 12; mese++)
				{
//					Verifichiamo nella tabella delle statistiche i mesi e gli anni elaborati
//					SELECT DISTINCT anno, mese FROM dbo.StatistichePECPEIMsgInOut WHERE (anno = @Param1) AND (mese = @Param2)
//					Se il periodo non � stato caricato eseguiamo la procedura
					
					executor.setCommand( new EsecuzioneProceduraStatistiche(anno , mese, null) );
					executor.executeCommandAndLog();
				}
			}
			
			List<Object> out = ((CommandExecutorMultiThread)executor).waitResult();
			
		} catch (Exception e) {
//			e.printStackTrace();
//			throw new PecBusinessException(e.getMessage());
			executorSend.setCommand( new SendMailCommand( new NotifyMailBusinessException( -9876, "Errore caricamento parametri di configurazione statistiche: mese : " + mese +" anno : "+anno) )  );
			try {
				executorSend.executeCommand();
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		
		log.info("StatistichePec_Manager :: fine");
		
		
		return exitCode;
	}
	public static void main(String args[]){
		System.out.println(7%7);
	}
	
	
}