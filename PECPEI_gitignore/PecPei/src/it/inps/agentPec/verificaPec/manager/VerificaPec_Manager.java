package it.inps.agentPec.verificaPec.manager;

import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.PecBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.manager.CommandExecutorMultiThread;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;
import it.inps.agentPec.verificaPec.business.AggiornaStatoIndirizzo48H;
import it.inps.agentPec.verificaPec.business.CreaMessaggiPecInUscita;
import it.inps.agentPec.verificaPec.business.InviaPecVerificaCommand;
import it.inps.agentPec.verificaPec.business.ReadAndElabMessageVerificaCommand;
import it.inps.agentPec.verificaPec.business.SelezionaMessaggiVerificaPec;
import it.inps.agentPec.verificaPec.dao.AcquisizioneIndirizziDAO;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;
import it.inps.agentPec.verificaPec.dao.MessaggiVerificaPecDAO;
import it.inps.agentPec.verificaPec.dto.AccountVerificaPec;
import it.inps.agentPec.verificaPec.dto.AcquisizioneIndirizziDTO;
import it.inps.agentPec.verificaPec.dto.MessaggioPecVerifica;

import java.util.List;

import org.apache.log4j.Logger;

public class VerificaPec_Manager {
	private static Logger log = Logger.getLogger(VerificaPec_Manager.class);
	
	
	@SuppressWarnings("unchecked")
	public String elabora() throws PecBusinessException {
		
		log.info("VerificaPec_Manager :: elabora");
		String exitCode = "";
		
		try {
			ConfigDAO configDao = new ConfigDAO();
			CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, 10);
			CommandExecutor executorSend = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, 10);
			
			List<AcquisizioneIndirizziDTO> indirizziDaVerificare = new AcquisizioneIndirizziDAO().selectIndirizziDaValidare();
			for ( AcquisizioneIndirizziDTO indirizzo: indirizziDaVerificare ){
					MessaggioPecVerifica messaggio = new MessaggioPecVerifica();
					//Compongo i messaggi in uscita da Inserireme
					executor.setCommand( new CreaMessaggiPecInUscita( indirizzo , messaggio) );
//					executor.executeCommand();
					executor.executeCommandAndLogVerificaNew(messaggio);

			}

			List<AccountVerificaPec> accounts = configDao.getAccountVerificaPec();
			MessaggiVerificaPecDAO messaggi = new MessaggiVerificaPecDAO();
			
			int totaliMessaggiInviati = 0;
			if(Boolean.parseBoolean(configDao.getProperty("abilitaTotaliMessaggiDaInviare", "false")))
				totaliMessaggiInviati = messaggi.verificaTotaleMessaggiInviati();
			
			int messaggiInviati = 0;
			if(totaliMessaggiInviati<Integer.parseInt(configDao.getProperty("maxMsgDay", "5500"))){
				List<MessaggioDTO> result = messaggi.selezionaMessaggiInUscitaPEC();
				for(MessaggioDTO messaggio : result){
					//Invio della PEC
					if(messaggiInviati%accounts.size()==0)
						messaggiInviati = 0;
					AccountVerificaPec acc = accounts.get(messaggiInviati%accounts.size());
					AccountPecDTO apec = new AccountPecDTO(acc.getAccountLoginName(), acc.getAccountPassword());
					executorSend.setCommand( new InviaPecVerificaCommand( messaggio,apec ) );
//					executorSend.executeCommandAndLog();
					executorSend.executeCommandAndLogNew(messaggio);
					messaggiInviati++;
				}
				List<Object> out = ((CommandExecutorMultiThread)executorSend).waitResult(); 	
			}

//			ReadAndElabMessageVerifica della PEC
			for(AccountVerificaPec account: accounts){
				AccountPecDTO a = new AccountPecDTO(account.getAccountLoginName(), account.getAccountPassword());
				executor.setCommand( new ReadAndElabMessageVerificaCommand(a));
//				executor.executeCommandAndLog();
				executor.executeCommandAndLogVerificaNew(a);
			}

			
//			 Aggiorno gli stati degli indirizzi che non hanno ricevuto ricevute entro le 48H
			executor.setCommand( new AggiornaStatoIndirizzo48H() );
			executor.executeCommandAndLog();
			
			
			List<Object> out = ((CommandExecutorMultiThread)executor).waitResult();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new PecBusinessException(e.getMessage());			
		}
		
		log.info("VerificaPec_Manager :: fine");
		
		
		return exitCode;
	}
	public static void main(String args[]){
		System.out.println(7%7);
	}
	
	
}