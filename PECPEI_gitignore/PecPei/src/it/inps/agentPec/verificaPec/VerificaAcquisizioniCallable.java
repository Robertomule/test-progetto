package it.inps.agentPec.verificaPec;

import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.Posta;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;
import it.inps.agentPec.verificaPec.dao.InvioNotificheMailDao;
import it.inps.agentPec.verificaPec.dao.MessaggiVerificaPecDAO;
import it.inps.agentPec.verificaPec.dto.IndirizziInvioNotificheMail;
import it.inps.agentPec.verificaPec.dto.InvioNotificheMail;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author rstabile
 *
 */

public class VerificaAcquisizioniCallable implements Callable<String> {
	private static Log log = LogFactory.getLog("it.inps.agentPec.verificaPec");
	
	@Override
	public String call() throws Exception {
		ConfigDAO configdao = new ConfigDAO();
		MessaggiVerificaPecDAO dao = new MessaggiVerificaPecDAO();
		float perc80 = 80f;
		
		try{
			float maxMsgDay = Integer.parseInt(configdao.getProperty("maxMsgDay", "13000"));
			float msgInviati = dao.verificaTotaleMessaggiInviati();
			
			float percInviati = (msgInviati/maxMsgDay)*100;
					
			log.error("Verifica Acquisizione Indirizzi :: limite di invio "+maxMsgDay+" messaggi inviati "+msgInviati);
			System.out.println("Verifica Acquisizione Indirizzi :: limite di invio "+maxMsgDay+" messaggi inviati "+msgInviati);
			
			//Se la percentuale dei messaggi inviati supera perc80 invio la mail 
			if(Math.round(percInviati) > Math.round(perc80)){
				log.error("Verifica Acquisizione Indirizzi :: inviati "+Math.round(percInviati)+"% dei messaggi disponibili oggi. SUPERATO LIMITE - INVIO MAIL");
				System.out.println("Verifica Acquisizione Indirizzi :: inviati "+Math.round(percInviati)+"% dei messaggi disponibili oggi. SUPERATO LIMITE - INVIO MAIL");
				
				inviaMail(percInviati);
			}else{
				log.error("Verifica Acquisizione Indirizzi :: inviati "+Math.round(percInviati)+"% dei messaggi disponibili oggi");
				System.out.println("Verifica Acquisizione Indirizzi :: inviati "+Math.round(percInviati)+"% dei messaggi disponibili oggi");
			}
			
		} catch(Exception e) {
			log.error("Errore in Verifica Acquisizione Indirizzi :: "+e.getMessage());	
			System.out.println("Errore in Verifica Acquisizione Indirizzi :: "+e.getMessage());	
		}
		
		return "Terminato";
	}

	private void inviaMail(float percInviati){
		Posta posta = new Posta();
		Vector<String> destinatari = new Vector<String>();
		InvioNotificheMailDao dao = new InvioNotificheMailDao();
		
				
		try {
			InvioNotificheMail n = dao.getProcedura("VerificaAcquisizioni");
			List<IndirizziInvioNotificheMail> dest = dao.getDestinatari(n.getProcedura());
			
			for(IndirizziInvioNotificheMail d:dest)
				destinatari.add(d.getMail());
			
			posta.inviaEmail(ConfigProp.serverPosta,
							ConfigProp.uidPosta,
							ConfigProp.pwdPosta,
							ConfigProp.emailMittDettaglioEccezione,
							destinatari,
							null,
							n.getOggettoMail()+ " "+percInviati+"%",
							n.getCorpoMail(),
							true);
		} catch (Exception e) {
			log.error("Errore in Verifica Acquisizione Indirizzi :: INVIO MAIL :: "+e.getMessage());	
			System.out.println("Errore in Verifica Acquisizione Indirizzi :: INVIO MAIL :: "+e.getMessage());	
			e.printStackTrace();
		}
	}
}


