package it.inps.agentPec.verificaPec.dto;

import java.io.InputStream;
import java.sql.Timestamp;

public class AccountVerificaPec{

	private String accountLoginName;
	private String accountPassword;
	private Timestamp dataAttivazione;
	private Timestamp dataDisattivazione;
	private int accountAttivo; 
	private int accountDisattivo; 
	private int accountLimiteMessaggi;
	
	
	public String getAccountLoginName() {
		return accountLoginName;
	}
	public void setAccountLoginName(String accountLoginName) {
		this.accountLoginName = accountLoginName;
	}
	public String getAccountPassword() {
		return accountPassword;
	}
	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}
	public Timestamp getDataAttivazione() {
		return dataAttivazione;
	}
	public void setDataAttivazione(Timestamp dataAttivazione) {
		this.dataAttivazione = dataAttivazione;
	}
	public Timestamp getDataDisattivazione() {
		return dataDisattivazione;
	}
	public void setDataDisattivazione(Timestamp dataDisattivazione) {
		this.dataDisattivazione = dataDisattivazione;
	}
	public int getAccountAttivo() {
		return accountAttivo;
	}
	public void setAccountAttivo(int accountAttivo) {
		this.accountAttivo = accountAttivo;
	}
	public int getAccountDisattivo() {
		return accountDisattivo;
	}
	public void setAccountDisattivo(int accountDisattivo) {
		this.accountDisattivo = accountDisattivo;
	}
	public int getAccountLimiteMessaggi() {
		return accountLimiteMessaggi;
	}
	public void setAccountLimiteMessaggi(int accountLimiteMessaggi) {
		this.accountLimiteMessaggi = accountLimiteMessaggi;
	}
	
	
	
	
}
