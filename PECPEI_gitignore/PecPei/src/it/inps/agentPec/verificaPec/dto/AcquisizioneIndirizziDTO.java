package it.inps.agentPec.verificaPec.dto;

import java.sql.Timestamp;

public class AcquisizioneIndirizziDTO{
	
	private String UID ;
	private String MessageId ;
	private String CodiceFiscale ;
	private String Email ;
	private Timestamp DataInserimento ;
	private Timestamp DataVerifica; 
	private String StatoVerifica ;
	private String Stato ;
	private String DescrizioneVerifica;
	
	public String getUID() {
		return UID;
	}
	public void setUID(String uid) {
		UID = uid;
	}
	public String getMessageId() {
		return MessageId;
	}
	public void setMessageId(String messageId) {
		MessageId = messageId;
	}
	public String getCodiceFiscale() {
		return CodiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		CodiceFiscale = codiceFiscale;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public Timestamp getDataInserimento() {
		return DataInserimento;
	}
	public void setDataInserimento(Timestamp dataInserimento) {
		DataInserimento = dataInserimento;
	}
	public Timestamp getDataVerifica() {
		return DataVerifica;
	}
	public void setDataVerifica(Timestamp dataVerifica) {
		DataVerifica = dataVerifica;
	}
	public String getStatoVerifica() {
		return StatoVerifica;
	}
	public void setStatoVerifica(String statoVerifica) {
		StatoVerifica = statoVerifica;
	}
	public String getStato() {
		return Stato;
	}
	public void setStato(String stato) {
		Stato = stato;
	}
	public String getDescrizioneVerifica() {
		return DescrizioneVerifica;
	}
	public void setDescrizioneVerifica(String descrizioneVerifica) {
		DescrizioneVerifica = descrizioneVerifica;
	}
	
	
	
	
}
