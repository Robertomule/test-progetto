package it.inps.agentPec.verificaPec.dto;

import it.eustema.inps.agentPecPei.dto.DTO;

import java.io.Serializable;
import java.util.Date;

public class NotificaErroriDTO implements Serializable, DTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7067580851590223918L;
	
	private long ID;
	private int codice;
	private String messaggio;
	private String messageID;
	private Date dataCreazione;
	
	public void setID(long iD) {
		ID = iD;
	}
	public long getID() {
		return ID;
	}
	public void setCodice(int codice) {
		this.codice = codice;
	}
	public int getCodice() {
		return codice;
	}
	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}
	public String getMessaggio() {
		return messaggio;
	}
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}
	public String getMessageID() {
		return messageID;
	}
	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	public Date getDataCreazione() {
		return dataCreazione;
	}

}
