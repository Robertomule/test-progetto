package it.inps.agentPec.verificaPec.dto;

import java.sql.Timestamp;

/**
 * 
 * @author rstabile
 *
 */

public class IndirizziInvioNotificheMail {

	private int id;
	private String procedura;
	private String mail;
	private Timestamp dtUltimaModifica;
	private String matrUltimaModifica;
	private boolean flgAttivo;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProcedura() {
		return procedura;
	}
	public void setProcedura(String procedura) {
		this.procedura = procedura;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getMail() {
		return mail;
	}
	public Timestamp getDtUltimaModifica() {
		return dtUltimaModifica;
	}
	public void setDtUltimaModifica(Timestamp dtUltimaModifica) {
		this.dtUltimaModifica = dtUltimaModifica;
	}
	public String getMatrUltimaModifica() {
		return matrUltimaModifica;
	}
	public void setMatrUltimaModifica(String matrUltimaModifica) {
		this.matrUltimaModifica = matrUltimaModifica;
	}
	public boolean isFlgAttivo() {
		return flgAttivo;
	}
	public void setFlgAttivo(boolean flgAttivo) {
		this.flgAttivo = flgAttivo;
	}
}


