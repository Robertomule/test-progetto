package it.inps.agentPec.verificaPec.dto;

import java.sql.Timestamp;

/**
 * 
 * @author rstabile
 *
 */

public class InvioNotificheMail {

	private int id;
	private String procedura;
	private String descrizione;
	private String oggettoMail;
	private String corpoMail;
	private Timestamp dtUltimaModifica;
	private String matrUltimaModifica;
	private boolean flgAttivo;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProcedura() {
		return procedura;
	}
	public void setProcedura(String procedura) {
		this.procedura = procedura;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getOggettoMail() {
		return oggettoMail;
	}
	public void setOggettoMail(String oggettoMail) {
		this.oggettoMail = oggettoMail;
	}
	public String getCorpoMail() {
		return corpoMail;
	}
	public void setCorpoMail(String corpoMail) {
		this.corpoMail = corpoMail;
	}
	public Timestamp getDtUltimaModifica() {
		return dtUltimaModifica;
	}
	public void setDtUltimaModifica(Timestamp dtUltimaModifica) {
		this.dtUltimaModifica = dtUltimaModifica;
	}
	public String getMatrUltimaModifica() {
		return matrUltimaModifica;
	}
	public void setMatrUltimaModifica(String matrUltimaModifica) {
		this.matrUltimaModifica = matrUltimaModifica;
	}
	public boolean isFlgAttivo() {
		return flgAttivo;
	}
	public void setFlgAttivo(boolean flgAttivo) {
		this.flgAttivo = flgAttivo;
	}
}


