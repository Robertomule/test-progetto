package it.inps.agentPec.verificaPec.dao;

import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class StatisticheHermesMsgInOutDAO extends AgentBaseDAO
{	
	private final static String exex_SP_StatsHermes = "{call dbo.caricaStatisticheHermesMsgInOut(?, ?, ?)}";
	
	public Integer spCaricaStatisticheHermesMsgInOut(Integer anno, Integer mese, Integer giorno) throws DAOException 
	{			
		CallableStatement cstmt = null;
	    Integer spStasHermesRes = 0;
	    Connection connHermes = null;
	    
	    try 
	    {
	    	connHermes = cm.getConnection(Constants.DB_HERMES);  
	        cstmt = connHermes.prepareCall(exex_SP_StatsHermes);
	        
	        if(anno == null)
	        	cstmt.setNull(1, Types.INTEGER);
			else
				cstmt.setInt(1, anno);
			
			if(mese == null)
				cstmt.setNull(2, Types.INTEGER);
			else
				cstmt.setInt(2, mese);
			
			if(giorno == null)
				cstmt.setNull(3, Types.INTEGER);
			else
				cstmt.setInt(3, giorno);

	        cstmt.execute();
	        
	        spStasHermesRes = cstmt.getUpdateCount();
	        
	    } 
	    catch (Exception ex) 
	    {
	    	throw new DAOException("Errore metodo spCaricaStatisticheHermesMsgInOut: anno: " + ((anno == null)? " - ": anno) + "; mese: " + ((mese == null)? " - ": mese) + "; giorno: " + ((giorno == null)? " - ": giorno) + " | Dettaglio errore: " + ex.getStackTrace());
		}
		finally
		{
			if (cstmt != null) 
			{
	            try 
	            {
	                cstmt.close();
	            } 
	            catch (SQLException ex) 
	            {
	            	ex.printStackTrace();
	            }
	        }
			
			try 
			{
				if(connHermes != null)
					connHermes.close();
		    }
			catch (Exception e) 
		    {
		    	e.printStackTrace();
		    }
		}
		
	    return spStasHermesRes;
	
	}
}
