package it.inps.agentPec.verificaPec.dao;

import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.util.Constants;
import it.inps.agentPec.verificaPec.dto.AccountVerificaPec;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConfigDAO extends AgentBaseDAO{
	
	public String getProperty(String nome_config, String default_options) {
		String result = null;
		PreparedStatement ps = null;
		ResultSet rs =null;
		Connection conn = null;
		
		try {
			conn = cm.getConnection(Constants.DB_VERIFICAPEC);
			String query = 
				"SELECT descrizione_config " +
				"FROM config a WITH(NOLOCK) " +
				"WHERE a.nome_config=?";
			
			ps = conn.prepareStatement(query);
			ps.setString(1, nome_config);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				result = rs.getString("descrizione_config");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
			try {
				if(conn!=null) conn.close();
				if(ps!=null) ps.close();
				if(rs!=null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		if(result == null) 
			result = default_options;
		
		return result;

	}
	
	public List<AccountVerificaPec> getAccountVerificaPec() {
		List<AccountVerificaPec> result = null;
		PreparedStatement ps = null;
		ResultSet rs =null;
		Connection conn = null;
		AccountVerificaPec account;
		try {
			conn = cm.getConnection(Constants.DB_VERIFICAPEC);
			String query = 
				"SELECT AccountLoginName, AccountPassword, DataAttivazione, DataDisattivazione, AccountAttivo, AccountDisattivo, AccountLimiteMessaggi " +
				"FROM AccountVerificaPec WITH(NOLOCK) " +
				"WHERE AccountDisattivo = 0 AND AccountAttivo = 1";
			
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			result = new ArrayList<AccountVerificaPec>();
			
			while(rs.next()) {
				account = new AccountVerificaPec();
				account.setAccountLoginName(rs.getString("AccountLoginName"));
				account.setAccountPassword(rs.getString("AccountPassword"));
				account.setAccountLimiteMessaggi(rs.getInt("AccountLimiteMessaggi"));
				result.add(account);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
			try {
				if(conn!=null) conn.close();
				if(ps!=null) ps.close();
				if(rs!=null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
		
	}

}