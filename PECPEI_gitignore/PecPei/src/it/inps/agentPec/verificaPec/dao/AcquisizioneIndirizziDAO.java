package it.inps.agentPec.verificaPec.dao;

import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.inps.agentPec.verificaPec.dto.AcquisizioneIndirizziDTO;
import it.inps.agentPec.verificaPec.entities.Esito;
import it.inps.agentPec.verificaPec.entities.IndirizzoDaVerificare;
import it.inps.agentPec.verificaPec.exception.InsertIndirizzoDAOException;
import it.inps.agentPec.verificaPec.util.StatoType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class AcquisizioneIndirizziDAO extends AgentBaseDAO {

	private static Logger log = Logger.getLogger(AcquisizioneIndirizziDAO.class);
	
	private static String InserimentoIndirizzo = 
		"INSERT INTO acquisizioneIndirizzi ( UID ,MessageId ,CodiceFiscale ,Email ,DataInserimento ," +
		" DataVerifica, StatoVerifica ,Stato ,DescrizioneVerifica) VALUES (?,?,?,?,?,?,?,?,?) ";
	
	private static String UpdateInserimentoIndirizzo = " update acquisizioneIndirizzi set Email = ?,DataInserimento=?, messageId = ?, uid=?, StatoVerifica='DV',DescrizioneVerifica='Da Verificare'  where CodiceFiscale = ? ";
	
	private static String VerificaIndirizzoByCF = 
		" select UID ,MessageId ,CodiceFiscale ,Email ,DataInserimento ," +
		" DataVerifica, StatoVerifica ,Stato ,DescrizioneVerifica from acquisizioneIndirizzi where CodiceFiscale = ?";
	
	private static String SelezionaMessaggiDaValidare = 
		" select UID ,MessageId ,CodiceFiscale ,Email ,DataInserimento ," +
		" DataVerifica, StatoVerifica ,Stato ,DescrizioneVerifica from acquisizioneIndirizzi " +
		" where StatoVerifica = 'DV' and Email not like '%postacert.inps.gov.it%'order by DataInserimento desc";
	
	private static String InsertIndirizzoStorico = 
		" insert into storicoAcquisizioneIndirizzi (CodiceFiscale, MessageId, Email, DataInserimento, DataVerifica, StatoVerifica, DescrizioneVerifica) "+ 
		" VALUES (?,?,?,?,?,?,?)";
	
	private static String UpdateIndirizzo1 = " UPDATE acquisizioneIndirizzi set StatoVerifica = ? where MessageId = ? and CodiceFiscale = ? ";
	
	private static String UpdateIndirizzo2 = " UPDATE acquisizioneIndirizzi set StatoVerifica = ? , Stato = ?,DataVerifica=?,DescrizioneVerifica=? where MessageId = ? ";
	
	private static String verificaDataBefore48H =
		" select UID ,MessageId ,CodiceFiscale ,Email ,DataInserimento , datediff(minute, DataInserimento,getdate()) 'differenza'," +
		" DataVerifica, StatoVerifica ,Stato , DescrizioneVerifica from acquisizioneIndirizzi " +
		" where stato is null and StatoVerifica = "+"'"+StatoType.IN_FASE_DI_CONVALIDA.getStatoId()+"'";
 
	public ConfigDAO configDao = new ConfigDAO();
//	datediff(minute, max(PostedDate),getdate()) 'differenza' 2900
	
public int updateIndirizziDaValidare(AcquisizioneIndirizziDTO verifica, String messageId , StatoType stato) throws DAOException{
		
		Connection verificaPec = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int out = 0;
		try
		{
			verificaPec = cm.getConnection(Constants.DB_VERIFICAPEC);
			pstmt = verificaPec.prepareStatement(UpdateIndirizzo1);
			pstmt.setString(1, stato.getStatoId());
			pstmt.setString(2, messageId);
			pstmt.setString(3, verifica.getCodiceFiscale());
			out = pstmt.executeUpdate();
		}
		catch( Exception eh )
		{
			throw new DAOException("Errore recupero dati da updateIndirizziDaValidare");
		}
		finally{
			try {
				if(verificaPec!=null)
					verificaPec.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		return out;
	} 

public int updateIndirizziVerificati(String messageId , StatoType statoVerifica, StatoType stato, String descrizione ) throws DAOException{
	
	Connection verificaPec = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int out = 0;
	try
	{
		String desc = "Validazione tramite ricevuta di consegna";
		if(descrizione!=null) desc = descrizione ;
		
		verificaPec = cm.getConnection(Constants.DB_VERIFICAPEC);
		pstmt = verificaPec.prepareStatement(UpdateIndirizzo2);
		pstmt.setString(1, statoVerifica.getStatoId());
		pstmt.setString(2, stato.getStatoId());
		pstmt.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
		pstmt.setString(4, desc);
		pstmt.setString(5, messageId);
		out = pstmt.executeUpdate();
	}
	catch( Exception eh )
	{
		throw new DAOException("Errore recupero dati da updateIndirizziDaValidare");
	}
	finally{
		try {
			if(verificaPec!=null)
				verificaPec.close();
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	return out;
} 

public int aggiornaIndirizziSenzaRicevuteH48() throws DAOException{
	
	Connection verificaPec = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int result = 0;
	try
	{
		verificaPec = cm.getConnection(Constants.DB_VERIFICAPEC);
		pstmt = verificaPec.prepareStatement(verificaDataBefore48H);
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			AcquisizioneIndirizziDTO acquIndirizzoDTO = new AcquisizioneIndirizziDTO();
			acquIndirizzoDTO.setUID(rs.getString("UID"));
			acquIndirizzoDTO.setMessageId(rs.getString("messageId"));
			acquIndirizzoDTO.setCodiceFiscale(rs.getString("codiceFiscale"));
			acquIndirizzoDTO.setEmail(rs.getString("email"));
			acquIndirizzoDTO.setStato(rs.getString("Stato"));
			acquIndirizzoDTO.setStatoVerifica(rs.getString("statoVerifica"));
			acquIndirizzoDTO.setDescrizioneVerifica(rs.getString("DescrizioneVerifica"));
			acquIndirizzoDTO.setDataInserimento(rs.getTimestamp("DataInserimento"));
			acquIndirizzoDTO.setDataVerifica(rs.getTimestamp("DataVerifica"));
			int differenza = rs.getInt("differenza");
			if(differenza >= Integer.parseInt(configDao.getProperty("limiteMinutiAttesa","3000"))){
				updateIndirizziVerificati(acquIndirizzoDTO.getMessageId(), StatoType.VERIFICATO, StatoType.STATO_PEC_KO, "Nessuna Ricevuta pervenuta entro le 48H");
			}
			
	    }
	}
	catch( Exception eh )
	{
		throw new DAOException("Errore recupero dati da acquisizioneIndirizzi");
	}
	finally{
		try {
			if(verificaPec!=null)
				verificaPec.close();
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	return 0;
} 

	public List<AcquisizioneIndirizziDTO> selectIndirizziDaValidare() throws DAOException{
		
		Connection verificaPec = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<AcquisizioneIndirizziDTO> out = new ArrayList<AcquisizioneIndirizziDTO>();
		try
		{
			verificaPec = cm.getConnection(Constants.DB_VERIFICAPEC);
			pstmt = verificaPec.prepareStatement(SelezionaMessaggiDaValidare);
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				AcquisizioneIndirizziDTO acquIndirizzoDTO = new AcquisizioneIndirizziDTO();
				acquIndirizzoDTO.setUID(rs.getString("UID"));
				acquIndirizzoDTO.setMessageId(rs.getString("messageId"));
				acquIndirizzoDTO.setCodiceFiscale(rs.getString("codiceFiscale"));
				acquIndirizzoDTO.setEmail(rs.getString("email"));
				acquIndirizzoDTO.setStato(rs.getString("Stato"));
				acquIndirizzoDTO.setStatoVerifica(rs.getString("statoVerifica"));
				acquIndirizzoDTO.setDescrizioneVerifica(rs.getString("DescrizioneVerifica"));
				acquIndirizzoDTO.setDataInserimento(rs.getTimestamp("DataInserimento"));
				acquIndirizzoDTO.setDataVerifica(rs.getTimestamp("DataVerifica"));
				out.add(acquIndirizzoDTO);
		    }
		}
		catch( Exception eh )
		{
			throw new DAOException("Errore recupero dati da acquisizioneIndirizzi");
		}
		finally{
			try {
				if(verificaPec!=null)
					verificaPec.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		return out;
	} 

	
	public Esito inserisciIndirizzo(IndirizzoDaVerificare verifica, String messageId , String uuid)throws Exception {
		int result = 0 ;
		Connection verificaPec = cm.getConnection(Constants.DB_VERIFICAPEC);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AcquisizioneIndirizziDTO acquIndirizzoDTO = new AcquisizioneIndirizziDTO();
		Esito esito = null;
		try
		{
//		  select UID ,MessageId ,CodiceFiscale ,Email ,DataInserimento ," +
//		  DataVerifica, StatoVerifica ,Stato ,DescrizioneVerifica from acquisizioneIndirizzi where CodiceFiscale = ?";
			log.info("verifica esistenza ...");
			pstmt = verificaPec.prepareStatement(VerificaIndirizzoByCF);
			pstmt.setString(1, verifica.getCodiceFiscale().trim());
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				log.info("codice fiscale trovato .... "+verifica.getCodiceFiscale());
				acquIndirizzoDTO.setUID(rs.getString("UID"));
				acquIndirizzoDTO.setMessageId(rs.getString("messageId"));
				acquIndirizzoDTO.setCodiceFiscale(rs.getString("codiceFiscale"));
				acquIndirizzoDTO.setEmail(rs.getString("email"));
				acquIndirizzoDTO.setStato(rs.getString("Stato"));
				acquIndirizzoDTO.setStatoVerifica(rs.getString("statoVerifica"));
				acquIndirizzoDTO.setDescrizioneVerifica(rs.getString("DescrizioneVerifica"));
				acquIndirizzoDTO.setDataInserimento(rs.getTimestamp("DataInserimento"));
				acquIndirizzoDTO.setDataVerifica(rs.getTimestamp("DataVerifica"));
				
//			insert into storicoAcquisizioneIndirizzi (CodiceFiscale, MessageId, Email, DataInserimento, DataVerifica, StatoVerifica, DescrizioneVerifica) 
//			VALUES (?,?,?,?,?,?,?)
				pstmt = verificaPec.prepareStatement(InsertIndirizzoStorico);
				pstmt.setString(1, acquIndirizzoDTO.getCodiceFiscale());
				pstmt.setString(2, acquIndirizzoDTO.getMessageId());
				pstmt.setString(3, acquIndirizzoDTO.getEmail());
				pstmt.setTimestamp(4,acquIndirizzoDTO.getDataInserimento());
				pstmt.setTimestamp(5,acquIndirizzoDTO.getDataVerifica());
				pstmt.setString(6, acquIndirizzoDTO.getStatoVerifica());
				pstmt.setString(7, acquIndirizzoDTO.getDescrizioneVerifica());
				pstmt.executeUpdate();
				
//				update acquisizioneIndirizzi set Email = ? where CodiceFiscale = ?
				pstmt = verificaPec.prepareStatement(UpdateInserimentoIndirizzo);
				pstmt.setString(1, verifica.getIndirizzo().trim());
				pstmt.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
				pstmt.setString(3, messageId);
				pstmt.setString(4, uuid);
				pstmt.setString(5, verifica.getCodiceFiscale().trim());
				result = pstmt.executeUpdate();
				if(result>0){
					esito = new Esito(); 
					esito.setIdentificativo(uuid);
					esito.setEsitoAcquisizione("OK");
					esito.setCodiceEsito("00");
					esito.setDescrizioneEsito("L'email per il codice fiscale : "+verifica.getCodiceFiscale()+ " � stato modificato");
				}
			}else{
//			INSERT INTO acquisizioneIndirizzi ( UID ,MessageId ,CodiceFiscale ,Email ,DataInserimento ," +
//			DataVerifica, StatoVerifica ,Stato ,DescrizioneVerifica)
				pstmt = verificaPec.prepareStatement(InserimentoIndirizzo);
				pstmt.setString(1, uuid);
				pstmt.setString(2, messageId);
				pstmt.setString(3, verifica.getCodiceFiscale().trim().toUpperCase());
				pstmt.setString(4, verifica.getIndirizzo().trim());
				pstmt.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
				pstmt.setString(6, null);
				pstmt.setString(7, "DV"); // Da verificare
				pstmt.setString(8, null);
				pstmt.setString(9, "Da Verificare");
				result = pstmt.executeUpdate();
				if(result>0){
					esito = new Esito(); 
					esito.setIdentificativo(uuid);
					esito.setEsitoAcquisizione("OK");
					esito.setCodiceEsito("00");
					esito.setDescrizioneEsito("L'email per il codice fiscale : "+verifica.getCodiceFiscale()+ " � stato inserito");
				}

			}	
		}
		catch( Exception eh )
		{
			esito = new Esito();
			esito.setIdentificativo(uuid);
			esito.setEsitoAcquisizione("KO");
			esito.setCodiceEsito("200");
			esito.setDescrizioneEsito("L'errore generato � : "+eh.getMessage());
			throw new InsertIndirizzoDAOException(eh);
		}finally{
			if(esito == null){
				esito = new Esito(); 
				esito.setIdentificativo(uuid);
				esito.setEsitoAcquisizione("KO");
				esito.setCodiceEsito("200");
				esito.setDescrizioneEsito("Si � verificato un errore durante la fase di acquisizione del messaggio si prega di verificare i log.");
			}
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(verificaPec!=null)
					verificaPec.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return esito;
	}
	
	
	
}
