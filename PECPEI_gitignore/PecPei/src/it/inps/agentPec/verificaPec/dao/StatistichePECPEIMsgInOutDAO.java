package it.inps.agentPec.verificaPec.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;

public class StatistichePECPEIMsgInOutDAO extends AgentBaseDAO
{
	private final static String exex_SP_StatsPecPei = "{call dbo.caricaStatistichePECPEIMsgInOut(?, ?, ?)}";
	
	public Integer spCaricaStatistichePECPEIMsgInOut(Integer anno, Integer mese, Integer giorno) throws DAOException 
	{			
		CallableStatement cstmt = null;
	    Integer spStasPecPeiRes = 0;
	    Connection connPecPei = null;
	    
	    try 
	    {
	    	connPecPei = cm.getConnection(Constants.DB_PECPEI);  
	        cstmt = connPecPei.prepareCall(exex_SP_StatsPecPei);
	        
	        if(anno == null)
	        	cstmt.setNull(1, Types.INTEGER);
			else
				cstmt.setInt(1, anno);
			
			if(mese == null)
				cstmt.setNull(2, Types.INTEGER);
			else
				cstmt.setInt(2, mese);
			
			if(giorno == null)
				cstmt.setNull(3, Types.INTEGER);
			else
				cstmt.setInt(3, giorno);
	        
	        cstmt.execute();
	        
	        spStasPecPeiRes = cstmt.getUpdateCount();
	        
	    } 
	    catch (Exception ex) 
	    {
	    	throw new DAOException("Errore metodo spCaricaStatistichePECPEIMsgInOut: anno: " + ((anno == null)? " - ": anno) + "; mese: " + ((mese == null)? " - ": mese) + "; giorno: " + ((giorno == null)? " - ": giorno) + " | Dettaglio errore: " + ex.getStackTrace());
		}
		finally
		{
			if (cstmt != null) 
			{
	            try 
	            {
	                cstmt.close();
	            } 
	            catch (SQLException ex) 
	            {
	            	ex.printStackTrace();
	            }
	        }
			
			try 
			{
				if(connPecPei != null)
					connPecPei.close();
		    }
			catch (Exception e) 
		    {
		    	e.printStackTrace();
		    }
		}
		
	    return spStasPecPeiRes;
	
	}
}
