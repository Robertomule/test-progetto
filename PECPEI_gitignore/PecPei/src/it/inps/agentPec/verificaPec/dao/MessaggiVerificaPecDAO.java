package it.inps.agentPec.verificaPec.dao;

//import it.eustema.inps.agentPecPei.dto.AgentExecutionParamDTO;
import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SedeDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.DBConnectionManager;
import it.eustema.inps.utility.StaticUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class MessaggiVerificaPecDAO extends AgentBaseDAO{ 

	private static Log log = LogFactory.getLog(MessaggiVerificaPecDAO.class);
	
	private static final String SELECT_MSG_PEI_USCENTI_QRY = "select distinct m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura , m.nomeUfficio, " +
																" m.tipoMessaggio,m.tipologia,d.Addr,d.Type_per_cc, m.subject, m.body, m.verso, m.classifica , m.confermaRicezione, m.nomeSede, " +
																" m.FromPhrase,m.FromAddr, m.Sensibile, m.Riservato, d.Name, m.desMitt "+
																" from messaggi m with(nolock) " + 
																" LEFT JOIN destinatari d ON m.MessageID = d.MessageID " +																
																" where m.verso = 'U' and m.X_Trasporto = 'messaggio-pei' and m.Stato = 'DI' " +
																" order by m.MessageID";
	
	private static final String SELECT_MSG_PEI_DA_PROTOCOLLARE_QRY = "select m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura, " +
																" m.tipoMessaggio,m.tipologia,m.subject, m.body, m.verso, m.classifica, " +
																" m.Sensibile, m.Riservato"+
																" from messaggi m with(nolock) " + 
																" where m.X_Trasporto = 'messaggio-pei' and m.Stato = 'DP' " +
																" order by m.MessageID";
	
	private static final String SELECT_MSG_PEI_USCENTI_QRY2 = " select distinct m.MessageID, m.CodiceAOO, m.UiDoc, m.verso, m.nomeSede, m.PostedDate, m.Subject, m.Body, m.Stato, m.Riservato,"
		+ "m.Sensibile, m.size, m.IPAddress, m.nomeRegione, m.classifica, m.desClassifica, m.nomeUfficio, m.accountPEC, m.FromAddr, m.FromPhrase,"
		+ "m.AutoreCompositore, m.AutoreUltimaModifica, m.AutoreMittente,"
		+ "m.confermaRicezione, m.AddrConfermaRicezione, m.dominioPEC, m.documentoPrincipale, m.messaggio, m.tipologia, m.X_trasporto,"
		+ "m.MsgID, m.tipoMessaggio, m.ReplyTo, m.isReply, m.inReplyTo, m.inReplyToSegnatura, m.inReplyToSubject, m.SegnaturaRif, m.SegnaturaRoot, m.CodiceAMM,"
		+ "m.CodiceAOOMitt, m.CodiceAMMMitt, m.X_TipoRicevuta, m.BodyHtml, m.isSPAMorCCN, m.tipoMitt, m.desMitt, m.DirettoreAOO, m.DirettoreAOOMitt, m.substato,"
		+ "m.X_Ricevuta, m.DeliveredDate, d.Addr,d.Type_per_cc,d.Name from messaggi m with(nolock) " +
		" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
		" where m.verso = 'U' and m.X_Trasporto = 'messaggio-pei' and m.Stato = 'DI' and m.MessageID = ? " +
		" order by m.MessageID";
	
	
	
	private static final String SELECT_MSG_PEC_ENTRANTI_DA_RIPROTOCCOLARE_QRY = "select m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura, " +
	" m.tipoMessaggio,m.tipologia,d.Addr,d.Name,d.Type_per_cc, m.subject, m.body, m.verso, m.classifica " +
	" from messaggi m with(nolock)" + 
	" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
	" where m.verso = 'E' and m.X_Trasporto = 'posta-certificata' and m.Stato = 'DP' and m.CodiceAOO = ?  " +
	" order by m.MessageID";
	
	private static final String SELECT_MSG_PEC_USCENTI_QRY = "select m.MessageID,m.CodiceAOO,m.accountPEC, m.Stato,  " +
	" m.tipoMessaggio,m.tipologia,d.Addr,d.Name,d.Type_per_cc, m.subject, m.body, m.verso, m.X_TipoRicevuta " +
	" from messaggi m with(nolock)" + 
	" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
	" where m.verso = 'U' and m.X_Trasporto = 'posta-certificata' and m.Stato = 'DI' " +
	" order by m.postedDate asc";
	
	private static final String SELECT_MSG = "select count(m.MessageID) tot_messaggio"+
												" from messaggi m with(nolock)" + 
												" where m.MessageID = ?";	
	private static final String SELECT_DEST = "select count(m.MessageID) tot_messaggio"+
													" from destinatari m with(nolock)" + 
													" where m.MessageID = ?";	
	private static final String SELECT_ALLEGATI = "select count(m.MessageID) tot_messaggio"+
													" from destinatari m with(nolock)" + 
													" where m.MessageID = ?";
	
	private static final String SELECT_MSG_PEC_USCENTI_QRY2 = "select m.MessageID, m.CodiceAOO, m.UiDoc, m.verso, m.nomeSede, m.PostedDate, m.Subject, m.Body, m.Stato, m.Riservato, "
		+ "m.Sensibile, m.size, m.IPAddress, m.nomeRegione, m.classifica, m.desClassifica, m.nomeUfficio, m.accountPEC, m.FromAddr, m.FromPhrase, "
		+ "m.AutoreCompositore, m.AutoreUltimaModifica, m.AutoreMittente, m.PostedDate, m.Segnatura, "
		+ "m.confermaRicezione, m.AddrConfermaRicezione, m.dominioPEC, m.documentoPrincipale, m.messaggio, m.tipologia, m.X_trasporto, "
		+ "m.MsgID, m.tipoMessaggio, m.ReplyTo, m.isReply, m.inReplyTo, m.inReplyToSegnatura, m.inReplyToSubject, m.SegnaturaRif, m.SegnaturaRoot, m.CodiceAMM, "
		+ "m.CodiceAOOMitt, m.CodiceAMMMitt, m.X_TipoRicevuta, m.BodyHtml, m.isSPAMorCCN, m.tipoMitt, m.desMitt, m.DirettoreAOO, m.DirettoreAOOMitt, m.substato, "
		+ "m.X_Ricevuta, m.DeliveredDate, d.Addr,d.Name,d.Type_per_cc, d.phrase from messaggi m with(nolock)" +
		" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
		" where m.verso = 'U' and m.X_Trasporto = 'posta-certificata' and m.Stato = 'DI' and m.MessageID = ? " +
		" order by m.MessageID";
	
	private static final String SELECT_MSG_PEC_ENTRANTI_QRY_DA_PROTOCOLLARE = "select m.MessageID, m.CodiceAOO, m.UiDoc, m.verso, m.nomeSede, m.PostedDate, m.Subject, m.Body, m.Stato, m.Riservato,"
		+ "m.Sensibile, m.size, m.IPAddress, m.nomeRegione, m.classifica, m.desClassifica, m.nomeUfficio, m.accountPEC, m.FromAddr, m.FromPhrase,"
		+ "m.AutoreCompositore, m.AutoreUltimaModifica, m.AutoreMittente,"
		+ "m.confermaRicezione, m.AddrConfermaRicezione, m.dominioPEC, m.documentoPrincipale, m.messaggio, m.tipologia, m.X_trasporto,"
		+ "m.MsgID, m.tipoMessaggio, m.ReplyTo, m.isReply, m.inReplyTo, m.inReplyToSegnatura, m.inReplyToSubject, m.SegnaturaRif, m.SegnaturaRoot, m.CodiceAMM,"
		+ "m.CodiceAOOMitt, m.CodiceAMMMitt, m.X_TipoRicevuta, m.BodyHtml, m.isSPAMorCCN, m.tipoMitt, m.desMitt, m.DirettoreAOO, m.DirettoreAOOMitt, m.substato,"
		+ "m.X_Ricevuta, m.DeliveredDate, d.Addr,d.Name,d.Type_per_cc, d.phrase from messaggi m with(nolock)"  +
		" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
		" where m.verso = 'E' and m.X_Trasporto = 'posta-certificata' and m.Stato = 'DP' and m.MessageID = ? " +
		" order by m.MessageID";
	
	private static final String SELECT_COD_AOO_FROM_MESSAGGI = "select distinct m.CodiceAOO from messaggi m with(nolock), AccountsPEC a with(nolock) where m.CodiceAOO = a.AOO and a.gestioneNuovoAgent = 0 order by CodiceAOO asc";
	
	
	private static final String UPDATE_MSG_INVIATO_QRY = "update messaggi SET Stato=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?, Subject=?" +
															" WHERE MessageID=? ";
	
	private static final String UPDATE_MSG_USCENTE_PROTOCOLLATO_QRY = "update messaggi SET Segnatura=? " +
															" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";

	
	private static final String UPDATE_MSG_RICEVUTO_QRY = "update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?" +
															" WHERE MessageID=? AND CodiceAOO=? ";
	
	private static final String UPDATE_MSG_RICEVUTO_DOCPRIN_QRY = "update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?, documentoPrincipale=?" +
															" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";

	
	private static final String INS_MSG_VERIFICA_QRY =  "INSERT INTO messaggi"
		+ " (MessageID, CodiceAOO,  verso, PostedDate, Subject, Body, Stato,"
		+ " size, IPAddress,   accountPEC, FromAddr, FromPhrase,"
		+ " AutoreCompositore, AutoreUltimaModifica, AutoreMittente,"
		+ " confermaRicezione, AddrConfermaRicezione, dominioPEC, documentoPrincipale, tipologia, X_trasporto,  "
		+ " MsgID, tipoMessaggio, ReplyTo, isReply, inReplyTo, inReplyToSegnatura, inReplyToSubject, X_TipoRicevuta, BodyHtml,  "
		+ " isSPAMorCCN, tipoMitt, desMitt, substato, X_Ricevuta, DeliveredDate, postacertEml_Body, PostacertEml_Subject, SignIssuerName,SignSUbjectName , "				
		+ " isSigned, isSignedVer, SignValidFromDate, SignValidToDate,headers ,PostacertEml_Headers, DatiCertificazione, PostacertEml_PostedDate, PostacertEml_From )"
		+ " VALUES(?,?,?,?,?,?," + "?,?,?,?,?,?,"
		+ "?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,"
		+ "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?)";

	
	private static final String SELECT_MSG_Sens_Riser = "select m.MessageID , m.Riservato, m.Sensibile"+
														" from messaggi m with(nolock) " + 
														" where m.MessageID = ? ";
	
	private static final String SELECT_SEGNATURA = "select m.segnatura from messaggi m with (nolock) where m.MessageID = ? ";
	
	private static final String Select_Tot_Messaggi_Inviati = "select count(messageid) as totaleMessaggi FROM messaggi with(nolock) where DeliveredDate >= ?  and stato = N'IN'";
	
	//public int aggiornaMsgInviatiRicevuti(MessaggioDTO msgUscente, List<MessaggioDTO> elencoMsgEntranti) throws DAOException{
	public int aggiornaMsgInviatiRicevuti(Map<MessaggioDTO,List<MessaggioDTO>> messaggiUscentiEntrantiMap) throws DAOException{
		
		Connection connPecPei = null;		
		int ret = 0;
		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			log.debug("aggiornaMsgInviatiRicevuti :: usa la connection: " + connPecPei);
			connPecPei.setAutoCommit(false);
		}
		catch(Exception e){
			e.printStackTrace();
			System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
		}
		try{
			Set<MessaggioDTO> messaggiUscentiChiavi = messaggiUscentiEntrantiMap.keySet();
			for(MessaggioDTO msgUscente: messaggiUscentiChiavi){
				
				aggiornaMsgUscenteInviato(connPecPei, msgUscente);
				List<MessaggioDTO> listaMsgEntranti = messaggiUscentiEntrantiMap.get(msgUscente); 
				inserisciMsgEntranti(connPecPei, msgUscente, listaMsgEntranti);
			}											
			connPecPei.commit(); // solo per test, poi @ TODO decommentare la commit
			log.debug("aggiornaMsgUscenteInviato: Commit eseguito");
			//connPecPei.rollback();	// solo per test elaborazione senza aggiornamento DB ... poi cancellare		
		}
		catch(Exception eh){			   
		   if(connPecPei!=null){
				try {
					log.error("aggiornaMsgUscenteInviato: esegue rollback");					
					connPecPei.rollback();
				} catch (SQLException sqlex) {
					sqlex.printStackTrace();
				}	
		   }
		   throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		return ret;
		
	}
	public int aggiornaMsgRicevutoDaRiprotocollare(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{
		boolean internal = false;
		if ( connPecPei == null ){
			internal = true;
			try{
				connPecPei = cm.getConnection(Constants.DB_PECPEI);
				log.debug("aggiornaMsgRicevutoDaRiprotocollare :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}
		
		log.debug("aggiornaMsgRicevutoDaRiprotocollare START");
		
		int ret = 0;
		PreparedStatement pstmt = null;
		
		try	{
			if(msgUscente.getDocumentoPrincipale()!=null && !msgUscente.getDocumentoPrincipale().equals(""))
			{
//				"update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?, documentoPrincipale=?" +
//				" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";
				pstmt = connPecPei.prepareStatement(UPDATE_MSG_RICEVUTO_DOCPRIN_QRY);
				// segnatura
				if(msgUscente.getSegnatura()!=null)
					pstmt.setString(1, msgUscente.getSegnatura());
				else
					pstmt.setNull(1, java.sql.Types.VARCHAR);
				pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setString(4, msgUscente.getSubstato());
				pstmt.setString(5, msgUscente.getDocumentoPrincipale());
				pstmt.setString(6, msgUscente.getMessageId());
				pstmt.setString(7, msgUscente.getCodiceAOO());
				pstmt.setString(8, msgUscente.getAccountPec());
			}else{			
//				"update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?" +
//				" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";
				pstmt = connPecPei.prepareStatement(UPDATE_MSG_RICEVUTO_QRY);
				// segnatura
					if(msgUscente.getSegnatura()!=null)
						pstmt.setString(1, msgUscente.getSegnatura());
					else
						pstmt.setNull(1, java.sql.Types.VARCHAR);
					pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
					pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
					pstmt.setString(4, msgUscente.getSubstato());
					pstmt.setString(5, msgUscente.getMessageId());
					pstmt.setString(6, msgUscente.getCodiceAOO());
					pstmt.setString(7, msgUscente.getAccountPec());
			}
			ret = pstmt.executeUpdate();
			
			if ( internal )
				connPecPei.commit();
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgRicevutoDaRiprotocollare Exception: " + eh.getMessage());		
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if( internal && connPecPei!=null)
					connPecPei.close();
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		log.debug("aggiornaMsgRicevutoDaRiprotocollare END");		
		return ret;		
	}
	public int aggiornaMsgUscenteInviato(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{
		boolean internal = false;
		if ( connPecPei == null ){
			internal = true;
			try{
				connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);
				log.debug("aggiornaMsgUscenteInviato :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}
		
		log.debug("aggiornaMsgUscenteInviato START");
		
		int ret = 0;
		PreparedStatement pstmt = null;
		try	{
			pstmt = connPecPei.prepareStatement(UPDATE_MSG_INVIATO_QRY);
			
			// segnatura
			pstmt.setString(1, msgUscente.getStato());
			pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
			pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
			pstmt.setString(4, msgUscente.getSubstato());
			pstmt.setString(5, msgUscente.getSubject());
			pstmt.setString(6, msgUscente.getMessageId());
//			pstmt.setString(7, msgUscente.getAccountPec());
			
			ret = pstmt.executeUpdate();
			
//			doInsertAllegati(connPecPei, msgUscente.getMessageId(), msgUscente.getCodiceAOO(), msgUscente.getElencoFileAllegati());
			
			if ( internal )
				connPecPei.commit();
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgUscenteInviato Exception: " + eh.getMessage());
			//System.err.println("MessaggiDAO :: aggiornaMsgUscenteInviato Exception: " + eh.getMessage());
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if( internal && connPecPei!=null)
					connPecPei.close();
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		log.debug("aggiornaMsgUscenteInviato END");		
		return ret;		
	}
	
	
	
	public int aggiornaMsgUscenteProtocollato(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{
		boolean internal = false;
		if ( connPecPei == null ){
			internal = true;
			try{
				connPecPei = cm.getConnection(Constants.DB_PECPEI);
				log.debug("aggiornaMsgUscenteProtocollato :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}
		
		log.debug("aggiornaMsgUscenteProtocollato START");
		
		int ret = 0;
		PreparedStatement pstmt = null;
		try	{
			pstmt = connPecPei.prepareStatement(UPDATE_MSG_USCENTE_PROTOCOLLATO_QRY);
			
			// segnatura
			
			
			pstmt.setString(1, msgUscente.getSegnatura());
			pstmt.setString(2, msgUscente.getMessageId());
			pstmt.setString(3, msgUscente.getCodiceAOO());
			pstmt.setString(4, msgUscente.getAccountPec());
			
			ret = pstmt.executeUpdate();
			
			if ( internal )
				connPecPei.commit();
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgUscenteProtocollato Exception: " + eh.getMessage());
			//System.err.println("MessaggiDAO :: aggiornaMsgUscenteInviato Exception: " + eh.getMessage());
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if( internal && connPecPei!=null)
					connPecPei.close();
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		log.debug("aggiornaMsgUscenteProtocollato END");		
		return ret;		
	}
	public int verificaTotaleMessaggiInviati(){
		PreparedStatement pstmtNew = null;
		Connection connPecPei = null;
		int totaliMerssaggiInviati = 0;
		ResultSet rs = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		try{
			connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);	
			pstmtNew =  connPecPei.prepareStatement(Select_Tot_Messaggi_Inviati);
			String data = sdf.format(new Date());
			System.out.println(data);
			pstmtNew.setString(1,data);
			rs = pstmtNew.executeQuery();
			if(rs.next()){
				totaliMerssaggiInviati=Integer.parseInt(rs.getString("totaleMessaggi"));
			}
		}catch(Exception e){
			log.error("errore verificaMessaggioEntrante :: usa la connection: " + connPecPei+ e.getMessage());
		}finally{
			try{
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmtNew!=null)
					pstmtNew.close();
				if(rs!=null)
					rs.close();
			}catch(Exception e){}
			
		}
		return totaliMerssaggiInviati;
	}
	public boolean verificaMessaggioEntrante(MessaggioDTO msgEntrante){
		PreparedStatement pstmtNew = null;
		Connection connPecPei = null;
		boolean messaggioPresente = false;
		ResultSet rs = null;
		try{
			connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);	
			pstmtNew =  connPecPei.prepareStatement("select messageId from Messaggi with(nolock) where messageId = ? ");
			pstmtNew.setString(1, msgEntrante.getMessageId());
//			pstmtNew.setString(2, msgEntrante.getAccountPec());
			rs = pstmtNew.executeQuery();
			if(rs.next()){
				messaggioPresente=true;
			}
		}catch(Exception e){
			log.error("errore verificaMessaggioEntrante :: usa la connection: " + connPecPei+ e.getMessage());
		}finally{
			try{
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmtNew!=null)
					pstmtNew.close();
				if(rs!=null)
					rs.close();
			}catch(Exception e){}
			
		}
		return messaggioPresente;
	}
	public int aggiornaMsgEntranteRicevuto(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{
		boolean internal = false;
		if ( connPecPei == null ){
			internal = true;
			try{
				connPecPei = cm.getConnection(Constants.DB_PECPEI);
				log.debug("aggiornaMsgUscenteInviato :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}
		
		
		log.debug("aggiornaMsgEntranteRicevuto START");
		int ret = 0;
		PreparedStatement pstmt = null;
		try	{
			pstmt = connPecPei.prepareStatement(UPDATE_MSG_RICEVUTO_QRY);
			
			// segnatura
			if(msgUscente.getSegnatura()!=null)
				pstmt.setString(1, msgUscente.getSegnatura());
			else
				pstmt.setNull(1, java.sql.Types.VARCHAR);
			pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
			pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
			pstmt.setString(4, msgUscente.getSubstato());
			pstmt.setString(5, msgUscente.getMessageId());
			pstmt.setString(6, msgUscente.getCodiceAOO());
//			pstmt.setString(7, msgUscente.getAccountPec());
			
			ret = pstmt.executeUpdate();
			
			if ( internal )
				connPecPei.commit();
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgEntranteRicevuto Exception: " + eh.getMessage());
			//System.err.println("MessaggiDAO :: aggiornaMsgUscenteInviato Exception: " + eh.getMessage());
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if( internal && connPecPei!=null)
					connPecPei.close();
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		log.debug("aggiornaMsgUscenteInviato END");		
		return ret;		
	}
	
	private int inserisciMsgEntranti(Connection connPecPei, MessaggioDTO messaggioUscente, List<MessaggioDTO> elencoMsgEntranti) throws Exception{
				
		log.debug("inserisciMsgEntranti START");
		int rowCount = -1;

		for(MessaggioDTO msgDTO: elencoMsgEntranti){
			try{	
				inserisciMessaggioVerificaPec(connPecPei, msgDTO);
				doInsertDestinatari(connPecPei, msgDTO.getMessageId(), msgDTO.getElencoDestinatariPer(), msgDTO);
				doInsertDestinatari(connPecPei, msgDTO.getMessageId(), msgDTO.getElencoDestinatariCc(), msgDTO);
				doInsertAllegati(connPecPei, msgDTO.getMessageId(), msgDTO.getCodiceAOO(), msgDTO.getElencoFileAllegati() );
			}catch(Exception ex){
				ex.printStackTrace();
				log.error("inserisciMsgEntranti Exception: " + ex.getMessage());
				//System.err.println("MessaggiDAO :: inserisciMsgEntranti Exception: " + ex.getMessage());
				throw ex;
			}
		} // for su elencoMsgEntranti
		
		log.debug("inserisciMsgEntranti END");				
		return rowCount;
		
	} //inserisciMsgEntranti
	
	public List<MessaggioDTO> selezionaMessaggiInUscitaPEI() throws DAOException{
		
		log.debug("selezionaMessaggiInUscitaPEI :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEI_USCENTI_QRY);
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {
				
				String msgID = rs.getString("MessageID");
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();
					
					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setCodiceAOO(rs.getString("CodiceAOO"));
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
					messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
					messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
					messaggioPeiDTO.setNomeSede(rs.getString("nomeSede"));
					messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
					messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));
					messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
					messaggioPeiDTO.setSensibile(rs.getString("Sensibile"));
					messaggioPeiDTO.setRiservato(rs.getString("Riservato"));
					messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
					messaggioPeiDTO.setSegnatura(rs.getString("segnatura"));
					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));
						destDTO.setName(rs.getString("Name"));
						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));	
						destDTO.setName(rs.getString("Name"));
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);
				
				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);
				
		    } //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita;
	}
	
public MessaggioDTO selezionaDettaglioMessaggioInUscitaPEI( String messageID ) throws DAOException{
		
		log.debug("selezionaMessaggiInUscitaPEI :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
		
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEI_USCENTI_QRY2);
			pstmt.setString( 1, messageID );
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {
				
				String msgID = rs.getString("MessageID");
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();
					
					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setCodiceAOO( rs.getString("CodiceAOO") );
					messaggioPeiDTO.setUiDoc( rs.getString("UiDoc") );
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setNomeSede( rs.getString("nomeSede") );
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setStato( rs.getString( "Stato" ) );
					messaggioPeiDTO.setRiservato( rs.getString("Riservato") );
					messaggioPeiDTO.setSensibile( rs.getString("Sensibile") );
					messaggioPeiDTO.setSize(rs.getInt("size"));
					messaggioPeiDTO.setIpAddress(rs.getString("IPAddress"));
					messaggioPeiDTO.setNomeRegione(rs.getString("nomeRegione"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setDesClassifica(rs.getString("desClassifica"));
					messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
					messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));					
					messaggioPeiDTO.setAutoreCompositore(rs.getString("AutoreCompositore"));
					messaggioPeiDTO.setAutoreUltimaModifica(rs.getString("AutoreUltimaModifica"));
					messaggioPeiDTO.setAutoreMittente(rs.getString("AutoreMittente"));
					messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
					messaggioPeiDTO.setAddrConfermaRicezione(rs.getString("AddrConfermaRicezione"));
					messaggioPeiDTO.setDominoPec(rs.getString("dominioPEC"));
					messaggioPeiDTO.setDocumentoPrincipale(rs.getString("documentoPrincipale"));
					messaggioPeiDTO.setMessaggio(rs.getBytes("messaggio"));
					messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
					messaggioPeiDTO.setXTrasporto(rs.getString("X_trasporto"));
					messaggioPeiDTO.setMsgId(rs.getString("MsgID"));
					messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
					messaggioPeiDTO.setReplyTo(rs.getString("ReplyTo"));
					messaggioPeiDTO.setIsReply(rs.getString("isReply"));
					messaggioPeiDTO.setInReplyTo(rs.getString("inReplyTo"));
					messaggioPeiDTO.setInReplyToSegnatura(rs.getString("inReplyToSegnatura"));
					messaggioPeiDTO.setInReplyToSubject(rs.getString("inReplyToSubject"));
					messaggioPeiDTO.setSegnaturaRif(rs.getString("SegnaturaRif"));
					messaggioPeiDTO.setSegnaturaRoot(rs.getString("SegnaturaRoot"));
					messaggioPeiDTO.setCodiceAMM(rs.getString("CodiceAMM"));
					messaggioPeiDTO.setCodiceAOOMitt(rs.getString("CodiceAOOMitt"));
					messaggioPeiDTO.setCodiceAMMMitt(rs.getString("CodiceAMMMitt"));
					messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
					messaggioPeiDTO.setBodyHtml(rs.getString("BodyHtml"));
					messaggioPeiDTO.setIsSPAMorCCN(rs.getInt("isSPAMorCCN"));
					messaggioPeiDTO.setTipoMitt(rs.getString("tipoMitt"));
					messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
					messaggioPeiDTO.setDirettoreAOO(rs.getString("DirettoreAOO"));
					messaggioPeiDTO.setDirettoreAOOMitt(rs.getString("DirettoreAOOMitt"));
					messaggioPeiDTO.setSubstato(rs.getString("substato"));
					messaggioPeiDTO.setRicevuta(rs.getString("X_Ricevuta"));
					
					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));
						destDTO.setName(rs.getString("name"));
//						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
					
					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));	
						destDTO.setName(rs.getString("name"));
//						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
					
				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);
				
				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);
				
		    } //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita.get(0);
	}

	public int doInsertDestinatari(Connection conn, String messageID, List<DestinatarioDTO> destinatari, MessaggioDTO msg) throws Exception {
		if ( destinatari == null )
			return -1;
		int key = -1;

		for ( DestinatarioDTO destinatario: destinatari ){
			PreparedStatement pstmt = null;
			
			String querySelect = "SELECT * FROM DESTINATARI WHERE MessageID = ? AND Addr=? AND Name=? ";
			pstmt = conn.prepareStatement(querySelect);
			pstmt.setString(1, messageID);
			if (destinatario.getAddr() != null)
				pstmt.setString(2, destinatario.getAddr());
			else
				pstmt.setNull(2, java.sql.Types.VARCHAR);
			if (destinatario.getName() != null)
				pstmt.setString(3, destinatario.getName());
			else {
				// in caso di PEC il Name non � valorizzato
				if (destinatario.getAddr() != null) {
					// viene valorizzato con l'addr
					pstmt.setString(3, destinatario.getAddr());
				} else {
					// se non � valorizzato neanche l'addr si mette x perch� Name �
					// chiave!!!
					pstmt.setString(3, "x");
				}
			}
			ResultSet rs = pstmt.executeQuery();
			if(!rs.next()){
				String query = "INSERT INTO destinatari"
						+ " (MessageID,Addr,Phrase,Type_per_cc,Name, postedDate)"
						+ " VALUES(?,?,?,?,?,?)";
				pstmt = conn.prepareStatement(query);
		
				pstmt.setString(1, messageID);
		
				if (destinatario.getAddr() != null)
					pstmt.setString(2, destinatario.getAddr());
				else
					pstmt.setNull(2, java.sql.Types.VARCHAR);
		
				if (destinatario.getPhrase() != null)
					pstmt.setString(3, destinatario.getPhrase());
				else
					pstmt.setNull(3, java.sql.Types.VARCHAR);
		
				if (destinatario.getType() != null)
					pstmt.setString(4, destinatario.getType());
				else
					pstmt.setNull(4, java.sql.Types.VARCHAR);
		
				if (destinatario.getName() != null)
					pstmt.setString(5, destinatario.getName());
				else {
					// in caso di PEC il Name non � valorizzato
					if (destinatario.getAddr() != null) {
						// viene valorizzato con l'addr
						pstmt.setString(5, destinatario.getAddr());
					} else {
						// se non � valorizzato neanche l'addr si mette x perch� Name �
						// chiave!!!
						pstmt.setString(5, "x");
					}
				}
				if (msg.getPostedDate() != null)
					pstmt.setTimestamp(6, new Timestamp(msg.getPostedDate().getTime()));
				else
					pstmt.setNull(6, java.sql.Types.TIMESTAMP);
		
				key = pstmt.executeUpdate();
			}
			pstmt.close();
		}

		return key;

	}
	
	public int doInsertAllegato(Connection connPecPei, String messageId, String codAOO, AllegatoDTO allegato) throws Exception {

		PreparedStatement pstmt = null;
		int result=0;
		boolean internal = false;
		boolean error = false;
		StringBuilder errors = new StringBuilder("");
		
		try{			
			if ( connPecPei == null ){
				internal = true;
				connPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
			}
			// Con Mese Anno
//			String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,postedDate,meseAnno )" +
//			" VALUES(?,?,?,?,?,?,?,?,?)";
			
			String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,postedDate)" +
			" VALUES(?,?,?,?,?,?,?,?)";

			pstmt =  connPecPei.prepareStatement(query);
			
			pstmt.setString(1, messageId);
			pstmt.setString(2, allegato.getFileName());
			pstmt.setString(3, allegato.getTipoAllegato());
			pstmt.setInt(4, allegato.getFileSize());
			pstmt.setBytes(5, allegato.getFileData());
			pstmt.setString(6, allegato.getContentType());
			if(allegato.getTestoAllegato()!=null) {
				pstmt.setString(7, allegato.getTestoAllegato());
			}else{
				pstmt.setNull(7,java.sql.Types.VARCHAR);
			}
			pstmt.setTimestamp(8, allegato.getPostedDate());
//Con Mese Anno			
/*		
			if(allegato.getMeseAnno()!=null){
				pstmt.setString(9, allegato.getMeseAnno());
			}else{
				pstmt.setNull(9, java.sql.Types.VARCHAR);
			}
*/
		result = pstmt.executeUpdate();
		
	}catch(Exception e){
		error = true;
		errors.append( e.getMessage() );
		errors.append("\n");
	}finally{
		if(internal){
			if(connPecPei!=null)
				connPecPei.close();
		}
		if(pstmt!=null)
			pstmt.close();
		if (error){
			throw new BusinessException(-6060, "Errori nell'inserimento allegati. Dettaglio:\n" + errors.toString());
	}
	}
	
	return result;
}
	
	public int doInsertAllegati(Connection conn, String messageId,
			String codAOO, List<AllegatoDTO> elencoFileAllegati)
			throws Exception {

		PreparedStatement pstmt = null;
		int result = 0;
		StringBuilder errors = new StringBuilder();
		ResultSet rs = null;
		boolean error = false;
		if (elencoFileAllegati != null) {
			Iterator<AllegatoDTO> it = elencoFileAllegati.iterator();
			while (it.hasNext()) {
				try {
					AllegatoDTO allegato = it.next();
					String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,PostedDate)"
							+ " VALUES(?,?,?,?,?,?,?,?)";
					pstmt = conn.prepareStatement(query);

					pstmt.setString(1, messageId);
					pstmt.setString(2, allegato.getFileName());
					pstmt.setString(3,
							allegato.getTipoAllegato().length() >= 7 ? ""
									: allegato.getTipoAllegato());
					pstmt.setInt(4, allegato.getFileSize());
					pstmt.setBytes(5, allegato.getFileData());

					if (allegato.getContentType() != null
							&& allegato.getContentType().indexOf(";") != -1)
						pstmt.setString(6, allegato.getContentType().substring(
								0, allegato.getContentType().indexOf(";")));
					else
						pstmt.setString(6, "");
					if (allegato.getTestoAllegato() != null) {
						pstmt.setString(7, allegato.getTestoAllegato());
					} else {
						pstmt.setNull(7, java.sql.Types.VARCHAR);
					}
					if (allegato.getPostedDate() != null) {
						pstmt.setTimestamp(8, allegato.getPostedDate());
					} else {
						pstmt.setNull(8, java.sql.Types.TIMESTAMP);
					}
					/*
					 * if(allegato.getMeseAnno()!=null){ pstmt.setString(9,
					 * allegato.getMeseAnno()); }else{ pstmt.setNull(9,
					 * java.sql.Types.VARCHAR); }
					 */
					pstmt.setQueryTimeout(300);
					result = pstmt.executeUpdate();

				} catch (Exception e) {
					error = true;
					errors.append(e.getMessage());
					errors.append("\n");
				}
			}
		}
		if (pstmt != null)
			pstmt.close();

		if (error) {
			throw new BusinessException(-6060,
					"Errori nell'inserimento allegati. Dettaglio:\n"
							+ errors.toString());
		}

		return result;
	}
	
	
	public int inserisciMessaggioVerificaPec(Connection connPecPei, MessaggioDTO msgDTO) throws DAOException{
		
		log.debug("inserisciMessaggioVerificaPec START");
		int rowCount = -1;
		PreparedStatement pstmt = null;
		boolean internal = false;
		ResultSet rs = null;
	
		try{			
				if ( connPecPei == null ){
					internal = true;
					connPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_VERIFICAPEC);
				}
				
				pstmt =  connPecPei.prepareStatement("select messageId from MessaggiBlob with(nolock) where messageId = ?");
				pstmt.setString(1, msgDTO.getMessageId());
				rs = pstmt.executeQuery(); 
				
				if(!rs.next()){
					try{
						pstmt =  connPecPei.prepareStatement("insert into MessaggiBlob ( Messageid ,postedDate ,rawContent ,messaggio) VALUES ( ? ,?,?,?)");
						pstmt.setString(1, msgDTO.getMessageId());
						pstmt.setTimestamp(2, new Timestamp(msgDTO.getPostedDate().getTime()));
						pstmt.setString(3, msgDTO.getRawContent());
						pstmt.setBytes(4,  creaMessageTxt(msgDTO).getFileData());
//						pstmt.setString(5, "DE");
//						pstmt.setInt(6, msgDTO.getSize());
						pstmt.executeUpdate();
					}catch(Exception e){
						log.error("inserisciMessaggioVerificaPec Exception: " + e.getMessage()+"sul messaggio  : "+msgDTO.getMessageId()+ "codice sede "+msgDTO.getCodiceAOO());
					}
				}
				connPecPei.setAutoCommit(false);
				
				int i = 0;
				
				if(verificaDestinatari(msgDTO.getMessageId())<=0){
					doInsertDestinatari(connPecPei, msgDTO.getMessageId(), msgDTO.getElencoDestinatariPer(), msgDTO);
					if(!msgDTO.getTipoMessaggio().equalsIgnoreCase("ricezione-protocollazione-pei") && !Constants.AUTORE_COMPOSITORE_PST.equals( msgDTO.getAutoreCompositore() ) )
						doInsertAllegati(connPecPei, msgDTO.getMessageId(), msgDTO.getCodiceAOO(), msgDTO.getElencoFileAllegati() );
				}
				
				pstmt =  connPecPei.prepareStatement(INS_MSG_VERIFICA_QRY);			
				i=1;
				pstmt.setString(i++, msgDTO.getMessageId());

				pstmt.setString(i++, msgDTO.getCodiceAOO());

				if(msgDTO.getVerso()!=null)
					pstmt.setString(i++, msgDTO.getVerso());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				java.sql.Timestamp postedDate = null;
				if(msgDTO.getStato()!=null && (msgDTO.getStato().equals(Constants.STATO_MSG_DAINVIARE) || msgDTO.getStato().equals(Constants.STATO_MSG_RICEVUTO))){
					if(msgDTO.getPostedDate()!= null){
						postedDate = new java.sql.Timestamp(msgDTO.getPostedDate().getTime());
					}
					pstmt.setTimestamp(i++, postedDate);
				}else{
					if(msgDTO.getPostedDate()!=null)
						pstmt.setTimestamp(i++,  new java.sql.Timestamp(msgDTO.getPostedDate().getTime()));
					else
						pstmt.setTimestamp(i++,  new java.sql.Timestamp(new java.util.Date().getTime()));
				}
				
				if(msgDTO.getSubject()!=null)
					pstmt.setString(i++, msgDTO.getSubject());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
					
				if(msgDTO.getBody()!=null)
					pstmt.setString(i++, msgDTO.getBody());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				if((msgDTO.getStato()!=null && msgDTO.getRicevuta() != null && !msgDTO.getRicevuta().equals("")) || msgDTO.getSubject().startsWith("ANOMALIA")){
					pstmt.setString(i++, Constants.STATO_RICEVUTO);
				}else if(msgDTO.getStato()!=null){
					pstmt.setString(i++, msgDTO.getStato());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				pstmt.setInt(i++, msgDTO.getSize());

				pstmt.setString(i++, "Batch Verifica Pec");

				if(msgDTO.getAccountPec()!=null){
					pstmt.setString(i++, msgDTO.getAccountPec());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getFromAddr()!=null){
					pstmt.setString(i++, msgDTO.getFromAddr());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}

				if(msgDTO.getFromPhrase()!=null){
					pstmt.setString(i++, msgDTO.getFromPhrase());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getAutoreCompositore()!=null){
					pstmt.setString(i++,msgDTO.getAutoreCompositore());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getAutoreUltimaModifica()!=null){
					pstmt.setString(i++, msgDTO.getAutoreUltimaModifica());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getAutoreMittente()!=null){
					pstmt.setString(i++, msgDTO.getAutoreMittente());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getConfermaRicezione()!=null){
					pstmt.setString(i++, msgDTO.getConfermaRicezione());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getAddrConfermaRicezione()!=null){
					pstmt.setString(i++, msgDTO.getAddrConfermaRicezione());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getDominoPec()!=null){
					pstmt.setString(i++, msgDTO.getDominoPec());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getDocumentoPrincipale()!=null){
					pstmt.setString(i++, msgDTO.getDocumentoPrincipale());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getTipologia()!=null){
					pstmt.setString(i++, msgDTO.getTipologia());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getXTrasporto()!=null){
					pstmt.setString(i++, msgDTO.getXTrasporto());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}

				if(msgDTO.getMsgId()!=null){
					pstmt.setString(i++, msgDTO.getMsgId());
				}else{
					pstmt.setString(i++, msgDTO.getMessageId());
				}
				
				if(msgDTO.getTipoMessaggio()!=null){
					pstmt.setString(i++, msgDTO.getTipoMessaggio());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				//ReplyTo se PEC = FromAddr
				if(msgDTO.getReplyTo()!=null){
					pstmt.setString(i++, msgDTO.getReplyTo());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getIsReply()!=null){
					pstmt.setString(i++, msgDTO.getIsReply());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getInReplyTo()!=null){
					pstmt.setString(i++, msgDTO.getInReplyTo());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getInReplyToSegnatura()!=null){
					pstmt.setString(i++, msgDTO.getInReplyToSegnatura());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getInReplyToSubject()!=null){
					pstmt.setString(i++, msgDTO.getInReplyToSubject());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getTipoRicevuta()!=null){
					pstmt.setString(i++,msgDTO.getTipoRicevuta());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getBodyHtml()!=null){
					pstmt.setString(i++,msgDTO.getBodyHtml());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getIsSPAMorCCN()!=null){
					pstmt.setInt(i++,msgDTO.getIsSPAMorCCN());
				}else{
					pstmt.setNull(i++, java.sql.Types.INTEGER);
				}
				
				if(msgDTO.getTipoMitt()!=null){
					pstmt.setString(i++,msgDTO.getTipoMitt());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				
				if(msgDTO.getDesMitt()!=null){
					pstmt.setString(i++,msgDTO.getDesMitt());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				int substato=i++;
				if(msgDTO.getSubstato()!=null){
					pstmt.setString(substato,msgDTO.getSubstato());
				}else{
					pstmt.setNull(substato, java.sql.Types.VARCHAR);
				}
				 
				if(msgDTO.getRicevuta()!=null){
					pstmt.setString(i++,msgDTO.getRicevuta());
				}else{
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				}
				int delivered = i++;
				if(msgDTO.getStato()!=null && msgDTO.getStato().equals(Constants.STATO_MSG_RICEVUTO)){
					java.sql.Timestamp deliveredDate = null;
					if(msgDTO.getPostedDate()!=null){
						deliveredDate = new java.sql.Timestamp(msgDTO.getDeliveredDate().getTime());
					}
					pstmt.setTimestamp(delivered, deliveredDate);
				}else{
					pstmt.setNull(delivered, java.sql.Types.TIMESTAMP);
				}
				// Per le ricevute 
				if((msgDTO.getRicevuta()!=null && (msgDTO.getRicevuta().equals(Constants.nonAccettazione) || msgDTO.getRicevuta().equals(Constants.erroreConsegna)) && msgDTO.getIsSPAMorCCN().equals(0) )
					||
  				   (msgDTO.getSubject()!=null && msgDTO.getSubject().startsWith(Constants.anomaliaMessaggio) && msgDTO.getIsSPAMorCCN().equals(0))
				){
					java.sql.Timestamp deliveredDate = new java.sql.Timestamp(new java.util.Date().getTime());
					pstmt.setTimestamp(delivered, deliveredDate);
					pstmt.setString(substato,Constants.SUBSTATO_INVIO_NOTIFICA);
				}else if(msgDTO.getRicevuta()!=null && (!msgDTO.getRicevuta().equals(Constants.nonAccettazione)|| msgDTO.getIsSPAMorCCN().equals(1))){
					java.sql.Timestamp deliveredDate = new java.sql.Timestamp(new java.util.Date().getTime());
					pstmt.setTimestamp(delivered, deliveredDate);
					// SubStato
					pstmt.setString(substato,"A");
				}
				
				if ( msgDTO.getPostaCertEMLBody() != null )
					pstmt.setString( i++, msgDTO.getPostaCertEMLBody());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				if ( msgDTO.getPostaCertEMLSubject() != null )
					pstmt.setString( i++, msgDTO.getPostaCertEMLSubject());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);

				if ( msgDTO.getSignIssuerName() != null )
					pstmt.setString( i++, msgDTO.getSignIssuerName() );
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);

				if ( msgDTO.getSignSubjectName() != null )
					pstmt.setString( i++, msgDTO.getSignSubjectName() );
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				if ( msgDTO.getIsSigned() != null )
					pstmt.setString( i++, msgDTO.getIsSigned());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				if ( msgDTO.getIsSignedVer() != null )
					pstmt.setString( i++, msgDTO.getIsSignedVer());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				if(msgDTO.getSignValidFromDate()!=null){
					pstmt.setTimestamp(i++, new java.sql.Timestamp(msgDTO.getSignValidFromDate().getTime()));
				}else{
					pstmt.setNull(i++, java.sql.Types.TIMESTAMP);
				}
				
				if(msgDTO.getSignValidToDate()!=null){
					pstmt.setTimestamp(i++, new java.sql.Timestamp(msgDTO.getSignValidToDate().getTime()));
				}else{
					pstmt.setNull(i++, java.sql.Types.TIMESTAMP);
				}
				
				if ( msgDTO.getHeaders() != null )
					pstmt.setString( i++, msgDTO.getHeaders().toString());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);

				if ( msgDTO.getPemlHeaders() != null )
					pstmt.setString( i++, msgDTO.getPemlHeaders().toString());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				if ( msgDTO.getDatiSignature() != null )
					pstmt.setString( i++, msgDTO.getDatiSignature());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				if ( msgDTO.getPemlPostedDate() != null )
					pstmt.setTimestamp( i++, new Timestamp( msgDTO.getPemlPostedDate().getTime() ));
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				if ( msgDTO.getPemlFrom() != null )
					pstmt.setString( i++, msgDTO.getPemlFrom());
				else
					pstmt.setNull(i++, java.sql.Types.VARCHAR);
				
				rowCount = pstmt.executeUpdate();
				
				pstmt.close();	

			}
			catch(Exception ex){
				ex.printStackTrace();
				if ( internal ){
					try {
						connPecPei.rollback();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				log.error("INS_MSG_ENTRANTE_QRY Exception: " + ex.getMessage()+"sul messaggio  : "+msgDTO.getMessageId());
				throw new DAOException( ex.getMessage()+"sul messaggio  : "+msgDTO.getMessageId());
			} finally{
				if ( internal ){
					try {
						connPecPei.commit();
						connPecPei.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}		
					
				}
				try{
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

				}catch(Exception e){
					log.error("inserisci messaggio :"+e.getMessage());
				}
				
			}
		log.debug("inserisciMessaggio END");				
		return rowCount;
		
	} //inserisciMsgEntranti

	public List<MessaggioDTO> selezionaMessaggiInUscitaPEC() throws DAOException{
		
		log.debug("selezionaMessaggiInUscitaPEC :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_USCENTI_QRY);
//			pstmt.setString( 1, codAOO );
//			pstmt.setString( 2, accountPec );
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {
				
				String msgID = rs.getString("MessageID");
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();
					
					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setCodiceAOO(rs.getString("CodiceAOO"));
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setVerso(rs.getString("verso"));
//					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
					
					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));
						
						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
//						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));					
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
//						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);
				
				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);
				
		    } //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEC :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		log.debug("selezionaMessaggiInUscitaPEC :: END, numero messaggi PEC in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita;
	}
public int verificaAllegati( String messageId ) throws DAOException{
	
	log.debug("verificaMessaggio :: START");
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int msgID = 0;
	try
	{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(SELECT_ALLEGATI);
		pstmt.setString( 1, messageId );
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			msgID = rs.getInt("tot_messaggio");
	    } //while	
	}
	catch( Exception eh )
	{
		eh.printStackTrace();
		log.error("verificaMessaggio :: exception=" + eh.getMessage());
		//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
		throw new DAOException(eh.getMessage());
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(pstmt!=null)
				pstmt.close();
			if(rs!=null)
				rs.close();

	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	log.debug("verificaMessaggio :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
	return msgID;
}
public int verificaDestinatari( String messageId ) throws DAOException{
	
	log.debug("verificaMessaggio :: START");
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int msgID = 0;
	try
	{
		connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);
		pstmt = connPecPei.prepareStatement(SELECT_DEST);
		pstmt.setString( 1, messageId );
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			msgID = rs.getInt("tot_messaggio");
	    } //while	
	}
	catch( Exception eh )
	{
		eh.printStackTrace();
		log.error("verificaMessaggio :: exception=" + eh.getMessage());
		//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
		throw new DAOException(eh.getMessage());
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(pstmt!=null)
				pstmt.close();
			if(rs!=null)
				rs.close();

	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	log.debug("verificaMessaggio :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
	return msgID;
}
public int verificaMessaggio( String messageId ) throws DAOException{
	
	log.debug("verificaMessaggio :: START");
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int msgID = 0;
	try
	{
		connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);
		pstmt = connPecPei.prepareStatement(SELECT_MSG);
		pstmt.setString( 1, messageId );
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			msgID = rs.getInt("tot_messaggio");
	    } //while	
	}
	catch( Exception eh )
	{
		eh.printStackTrace();
		log.error("verificaMessaggio :: exception=" + eh.getMessage());
		//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
		throw new DAOException(eh.getMessage());
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(pstmt!=null)
				pstmt.close();
			if(rs!=null)
				rs.close();

	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	log.debug("verificaMessaggio :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
	return msgID;
}
public MessaggioDTO verificaMessaggioSensOrRiserv( String messageId) throws DAOException{
	
	log.debug("verificaMessaggioSensOrRiserv :: START");
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int msgID = 0;
	MessaggioDTO messaggioPeiDTO = null;
	 
	try
	{
		connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);
		pstmt = connPecPei.prepareStatement(SELECT_MSG_Sens_Riser);
		pstmt.setString( 1, messageId );
//		pstmt.setString( 2, acoountPEC );
//		pstmt.setString( 3, codiceAOO );
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			messaggioPeiDTO = new MessaggioDTO();
			messaggioPeiDTO.setSensibile(rs.getString("Sensibile"));
			messaggioPeiDTO.setRiservato(rs.getString("Riservato"));
	    } //while	
	}
	catch( Exception eh )
	{
		eh.printStackTrace();
		log.error("verificaMessaggioSensOrRiserv :: exception=" + eh.getMessage());
		//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
		throw new DAOException(eh.getMessage());
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(pstmt!=null)
				pstmt.close();
			if(rs!=null)
				rs.close();

	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	log.debug("verificaMessaggioSensOrRiserv :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
	return messaggioPeiDTO;
}
public List<MessaggioDTO> selezionaMessaggiInUscitaPECDaRiprotocollare( String codAOO ) throws DAOException{
	
	log.debug("selezionaMessaggiInUscitaPEC :: START");
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	MessaggioDTO messaggioPeiDTO = null;
	DestinatarioDTO destDTO = null;
	List<DestinatarioDTO> listaDestinatariPer = null;
	List<DestinatarioDTO> listaDestinatariCC = null;
	List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
	try
	{
		//DBConnectionManager cm = new DBConnectionManager();
//		connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
		//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_ENTRANTI_DA_RIPROTOCCOLARE_QRY);
		pstmt.setString( 1, codAOO );
//		pstmt.setString( 2, accountPec );
		rs  = pstmt.executeQuery();			
		String currentMsgID = null;
		while (rs.next()) {
			
			String msgID = rs.getString("MessageID");
			if(!msgID.equals(currentMsgID)){
				//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
				// reset
				messaggioPeiDTO = new MessaggioDTO();
				destDTO = new DestinatarioDTO();
				listaDestinatariPer = new ArrayList<DestinatarioDTO>();
				listaDestinatariCC = new ArrayList<DestinatarioDTO>();
				
				messaggioPeiDTO.setMessageId(msgID);
				messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
				messaggioPeiDTO.setCodiceAOO(rs.getString("CodiceAOO"));
				messaggioPeiDTO.setSubject(rs.getString("subject"));
				messaggioPeiDTO.setBody(rs.getString("body"));
				messaggioPeiDTO.setVerso(rs.getString("verso"));
				messaggioPeiDTO.setClassifica(rs.getString("classifica"));
				
				String addr = rs.getString("Addr");
				if(addr != null){	
					destDTO.setAddr(addr);
					destDTO.setType(rs.getString("Type_per_cc"));
					
					//int slashIdx = addr.indexOf("/");
					String codiceAOO_dest = addr.substring(0, 4);
					log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
					destDTO.setCodiceAOO(codiceAOO_dest);
					if(destDTO.getType().equals("per"))
						listaDestinatariPer.add(destDTO);
					else
						listaDestinatariCC.add(destDTO);
				}
				currentMsgID = msgID;
			}				
			else{
				// caso di stesso msg, altro destinatario
				if(rs.getString("Addr") != null){	
					destDTO = new DestinatarioDTO();
					destDTO.setAddr(rs.getString("Addr"));
					destDTO.setType(rs.getString("Type_per_cc"));					
					//int slashIdx =  destDTO.getAddr().indexOf("/");
					String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
					log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
					destDTO.setCodiceAOO(codiceAOO_dest);
					
					if(destDTO.getType().equals("per"))
						listaDestinatariPer.add(destDTO);
					else
						listaDestinatariCC.add(destDTO);
				}
			}
			messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
			messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);
			
			if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
				listaMessaggiInUscita.add(messaggioPeiDTO);
			
	    } //while	
	}
	catch( Exception eh )
	{
		eh.printStackTrace();
		log.error("selezionaMessaggiInUscitaPEC :: exception=" + eh.getMessage());
		//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
		throw new DAOException(eh.getMessage());
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	log.debug("selezionaMessaggiInUscitaPEC :: END, numero messaggi PEC in uscita=" + listaMessaggiInUscita.size());		
	return listaMessaggiInUscita;
}
	
public MessaggioDTO selezionaDettaglioMessaggioInUscitaPEC( String messageID ) throws DAOException{
	
	log.debug("selezionaMessaggiInUscitaPEI :: START");
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	MessaggioDTO messaggioPeiDTO = null;
	DestinatarioDTO destDTO = null;
	List<DestinatarioDTO> listaDestinatariPer = null;
	List<DestinatarioDTO> listaDestinatariCC = null;
	List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
	
	try
	{
		//DBConnectionManager cm = new DBConnectionManager();
//		connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
		//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_USCENTI_QRY2);
		pstmt.setString( 1, messageID );
		rs  = pstmt.executeQuery();			
		String currentMsgID = null;
		while (rs.next()) {
			
			String msgID = rs.getString("MessageID");
			if(!msgID.equals(currentMsgID)){
				//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
				// reset
				messaggioPeiDTO = new MessaggioDTO();
				destDTO = new DestinatarioDTO();
				listaDestinatariPer = new ArrayList<DestinatarioDTO>();
				listaDestinatariCC = new ArrayList<DestinatarioDTO>();
				
				messaggioPeiDTO.setMessageId(msgID);
				messaggioPeiDTO.setPostedDate( rs.getTimestamp("PostedDate") );
				messaggioPeiDTO.setCodiceAOO( rs.getString("CodiceAOO") );
				messaggioPeiDTO.setUiDoc( rs.getString("UiDoc") );
				messaggioPeiDTO.setVerso(rs.getString("verso"));
				messaggioPeiDTO.setNomeSede( rs.getString("nomeSede") );
				messaggioPeiDTO.setSubject(rs.getString("subject"));
				messaggioPeiDTO.setBody(rs.getString("body"));
				messaggioPeiDTO.setStato( rs.getString( "Stato" ) );
				messaggioPeiDTO.setRiservato( rs.getString("Riservato") );
				messaggioPeiDTO.setSensibile( rs.getString("Sensibile") );
				messaggioPeiDTO.setSize(rs.getInt("size"));
				messaggioPeiDTO.setIpAddress(rs.getString("IPAddress"));
				messaggioPeiDTO.setNomeRegione(rs.getString("nomeRegione"));
				messaggioPeiDTO.setClassifica(rs.getString("classifica"));
				messaggioPeiDTO.setDesClassifica(rs.getString("desClassifica"));
				messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
				messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
				messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
				messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));					
				messaggioPeiDTO.setAutoreCompositore(rs.getString("AutoreCompositore"));
				messaggioPeiDTO.setAutoreUltimaModifica(rs.getString("AutoreUltimaModifica"));
				messaggioPeiDTO.setAutoreMittente(rs.getString("AutoreMittente"));
				messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
				messaggioPeiDTO.setAddrConfermaRicezione(rs.getString("AddrConfermaRicezione"));
				messaggioPeiDTO.setDominoPec(rs.getString("dominioPEC"));
				messaggioPeiDTO.setDocumentoPrincipale(rs.getString("documentoPrincipale"));
				messaggioPeiDTO.setMessaggio(rs.getBytes("messaggio"));
				messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
				messaggioPeiDTO.setXTrasporto(rs.getString("X_trasporto"));
				messaggioPeiDTO.setMsgId(rs.getString("MsgID"));
				messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
				messaggioPeiDTO.setReplyTo(rs.getString("ReplyTo"));
				messaggioPeiDTO.setIsReply(rs.getString("isReply"));
				messaggioPeiDTO.setInReplyTo(rs.getString("inReplyTo"));
				messaggioPeiDTO.setInReplyToSegnatura(rs.getString("inReplyToSegnatura"));
				messaggioPeiDTO.setInReplyToSubject(rs.getString("inReplyToSubject"));
				messaggioPeiDTO.setSegnaturaRif(rs.getString("SegnaturaRif"));
				messaggioPeiDTO.setSegnaturaRoot(rs.getString("SegnaturaRoot"));
				messaggioPeiDTO.setSegnatura(rs.getString("Segnatura"));
				messaggioPeiDTO.setCodiceAMM(rs.getString("CodiceAMM"));
				messaggioPeiDTO.setCodiceAOOMitt(rs.getString("CodiceAOOMitt"));
				messaggioPeiDTO.setCodiceAMMMitt(rs.getString("CodiceAMMMitt"));
				messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
				messaggioPeiDTO.setBodyHtml(rs.getString("BodyHtml"));
				messaggioPeiDTO.setIsSPAMorCCN(rs.getInt("isSPAMorCCN"));
				messaggioPeiDTO.setTipoMitt(rs.getString("tipoMitt"));
				messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
				messaggioPeiDTO.setDirettoreAOO(rs.getString("DirettoreAOO"));
				messaggioPeiDTO.setDirettoreAOOMitt(rs.getString("DirettoreAOOMitt"));
				messaggioPeiDTO.setSubstato(rs.getString("substato"));
				messaggioPeiDTO.setRicevuta(rs.getString("X_Ricevuta"));
				
				String addr = rs.getString("Addr");
				if(addr != null){	
					destDTO.setAddr(addr);
					destDTO.setType(rs.getString("Type_per_cc"));
					destDTO.setName(rs.getString("name"));
					destDTO.setPhrase(rs.getString("phrase"));
					//int slashIdx = addr.indexOf("/");
					String codiceAOO_dest = addr.substring(0, 4);
					log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
					destDTO.setCodiceAOO(codiceAOO_dest);
					if(destDTO.getType().equals("per"))
						listaDestinatariPer.add(destDTO);
					else
						listaDestinatariCC.add(destDTO);
				}
				
				currentMsgID = msgID;
			}				
			else{
				// caso di stesso msg, altro destinatario
				if(rs.getString("Addr") != null){	
					destDTO = new DestinatarioDTO();
					destDTO.setAddr(rs.getString("Addr"));
					destDTO.setType(rs.getString("Type_per_cc"));	
					destDTO.setName(rs.getString("name"));
					destDTO.setPhrase(rs.getString("phrase"));
					//int slashIdx =  destDTO.getAddr().indexOf("/");
					String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
					log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
					destDTO.setCodiceAOO(codiceAOO_dest);
					
					if(destDTO.getType().equals("per"))
						listaDestinatariPer.add(destDTO);
					else
						listaDestinatariCC.add(destDTO);
				}
				
			}
			messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
			messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);
			
			if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
				listaMessaggiInUscita.add(messaggioPeiDTO);
			
	    } //while	
	}
	catch( Exception eh )
	{
		eh.printStackTrace();
		log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
		//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
		throw new DAOException(eh.getMessage());
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
	return listaMessaggiInUscita.get(0);
}
public MessaggioDTO selezionaDettaglioMessaggioInUscitaPECDaProtocollare( String messageID ) throws DAOException{
	
	log.debug("selezionaMessaggiInUscitaPEI :: START");
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	MessaggioDTO messaggioPeiDTO = null;
	DestinatarioDTO destDTO = null;
	List<DestinatarioDTO> listaDestinatariPer = null;
	List<DestinatarioDTO> listaDestinatariCC = null;
	List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
	
	try
	{
		//DBConnectionManager cm = new DBConnectionManager();
//		connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
		//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_ENTRANTI_QRY_DA_PROTOCOLLARE);
		pstmt.setString( 1, messageID );
		rs  = pstmt.executeQuery();			
		String currentMsgID = null;
		while (rs.next()) {
			
			String msgID = rs.getString("MessageID");
			if(!msgID.equals(currentMsgID)){
				//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
				// reset
				messaggioPeiDTO = new MessaggioDTO();
				destDTO = new DestinatarioDTO();
				listaDestinatariPer = new ArrayList<DestinatarioDTO>();
				listaDestinatariCC = new ArrayList<DestinatarioDTO>();
				
				messaggioPeiDTO.setMessageId(msgID);
				messaggioPeiDTO.setCodiceAOO( rs.getString("CodiceAOO") );
				messaggioPeiDTO.setUiDoc( rs.getString("UiDoc") );
				messaggioPeiDTO.setVerso(rs.getString("verso"));
				messaggioPeiDTO.setNomeSede( rs.getString("nomeSede") );
				messaggioPeiDTO.setSubject(rs.getString("subject"));
				messaggioPeiDTO.setBody(rs.getString("body"));
				messaggioPeiDTO.setStato( rs.getString( "Stato" ) );
				messaggioPeiDTO.setRiservato( rs.getString("Riservato") );
				messaggioPeiDTO.setSensibile( rs.getString("Sensibile") );
				messaggioPeiDTO.setSize(rs.getInt("size"));
				messaggioPeiDTO.setIpAddress(rs.getString("IPAddress"));
				messaggioPeiDTO.setNomeRegione(rs.getString("nomeRegione"));
				messaggioPeiDTO.setClassifica(rs.getString("classifica"));
				messaggioPeiDTO.setDesClassifica(rs.getString("desClassifica"));
				messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
				messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
				messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
				messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));					
				messaggioPeiDTO.setAutoreCompositore(rs.getString("AutoreCompositore"));
				messaggioPeiDTO.setAutoreUltimaModifica(rs.getString("AutoreUltimaModifica"));
				messaggioPeiDTO.setAutoreMittente(rs.getString("AutoreMittente"));
				messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
				messaggioPeiDTO.setAddrConfermaRicezione(rs.getString("AddrConfermaRicezione"));
				messaggioPeiDTO.setDominoPec(rs.getString("dominioPEC"));
				messaggioPeiDTO.setDocumentoPrincipale(rs.getString("documentoPrincipale"));
				messaggioPeiDTO.setMessaggio(rs.getBytes("messaggio"));
				messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
				messaggioPeiDTO.setXTrasporto(rs.getString("X_trasporto"));
				messaggioPeiDTO.setMsgId(rs.getString("MsgID"));
				messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
				messaggioPeiDTO.setReplyTo(rs.getString("ReplyTo"));
				messaggioPeiDTO.setIsReply(rs.getString("isReply"));
				messaggioPeiDTO.setInReplyTo(rs.getString("inReplyTo"));
				messaggioPeiDTO.setInReplyToSegnatura(rs.getString("inReplyToSegnatura"));
				messaggioPeiDTO.setInReplyToSubject(rs.getString("inReplyToSubject"));
				messaggioPeiDTO.setSegnaturaRif(rs.getString("SegnaturaRif"));
				messaggioPeiDTO.setSegnaturaRoot(rs.getString("SegnaturaRoot"));
				messaggioPeiDTO.setCodiceAMM(rs.getString("CodiceAMM"));
				messaggioPeiDTO.setCodiceAOOMitt(rs.getString("CodiceAOOMitt"));
				messaggioPeiDTO.setCodiceAMMMitt(rs.getString("CodiceAMMMitt"));
				messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
				messaggioPeiDTO.setBodyHtml(rs.getString("BodyHtml"));
				messaggioPeiDTO.setIsSPAMorCCN(rs.getInt("isSPAMorCCN"));
				messaggioPeiDTO.setTipoMitt(rs.getString("tipoMitt"));
				messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
				messaggioPeiDTO.setDirettoreAOO(rs.getString("DirettoreAOO"));
				messaggioPeiDTO.setDirettoreAOOMitt(rs.getString("DirettoreAOOMitt"));
				messaggioPeiDTO.setSubstato(rs.getString("substato"));
				messaggioPeiDTO.setRicevuta(rs.getString("X_Ricevuta"));
				
				String addr = rs.getString("Addr");
				if(addr != null){	
					destDTO.setAddr(addr);
					destDTO.setType(rs.getString("Type_per_cc"));
					destDTO.setName(rs.getString("name"));
					destDTO.setPhrase(rs.getString("phrase"));
					//int slashIdx = addr.indexOf("/");
					String codiceAOO_dest = addr.substring(0, 4);
					log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
					destDTO.setCodiceAOO(codiceAOO_dest);
					if(destDTO.getType().equals("per"))
						listaDestinatariPer.add(destDTO);
					else
						listaDestinatariCC.add(destDTO);
				}
				
				currentMsgID = msgID;
			}				
			else{
				// caso di stesso msg, altro destinatario
				if(rs.getString("Addr") != null){	
					destDTO = new DestinatarioDTO();
					destDTO.setAddr(rs.getString("Addr"));
					destDTO.setType(rs.getString("Type_per_cc"));	
					destDTO.setName(rs.getString("name"));
					destDTO.setPhrase(rs.getString("phrase"));
					//int slashIdx =  destDTO.getAddr().indexOf("/");
					String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
					log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
					destDTO.setCodiceAOO(codiceAOO_dest);
					
					if(destDTO.getType().equals("per"))
						listaDestinatariPer.add(destDTO);
					else
						listaDestinatariCC.add(destDTO);
				}
				
			}
			messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
			messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);
			
			pstmt =  connPecPei.prepareStatement("select rawContent from MessaggiBlob with(nolock) where messageId = ?");
			pstmt.setString(1, messaggioPeiDTO.getMessageId());
			rs = pstmt.executeQuery(); 
			if(rs.next()){
				messaggioPeiDTO.setRawContent(rs.getString("rawContent"));
			}
			
			if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
				listaMessaggiInUscita.add(messaggioPeiDTO);
			
	    } //while	
	}
	catch( Exception eh )
	{
		eh.printStackTrace();
		log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
		//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
		throw new DAOException(eh.getMessage());
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
	return listaMessaggiInUscita.get(0);
}
	public List<String> selectAllAOO() throws DAOException{
	
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<String> out = new ArrayList<String>();
		
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_COD_AOO_FROM_MESSAGGI);
			
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				
				out.add( rs.getString(1) );
				
		    }
	
		}
		catch( Exception eh )
		{
			throw new DAOException("Errore recupero dati da MESSAGGI");
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		
		return out;

	}
	
	public AllegatoDTO creaMessageTxt(MessaggioDTO messaggio){
		
		AllegatoDTO allegato = new AllegatoDTO();
		try{
			/*
//			File temp = File.createTempFile("TestoDelMessaggio", ".txt");
//			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			StringBuilder str = new StringBuilder();
			if ("messaggio-pei".equalsIgnoreCase( messaggio.getTipologia() )){
				if(messaggio.getVerso()!=null && messaggio.getVerso().equals("U"))
					str.append("\r\n\nmessaggio pei in uscita");
			}else if(messaggio.getVerso()!=null && messaggio.getVerso().equals("U"))
				str.append("\r\n\nposta certificata in uscita");
			else 
				str.append("\r\n\nposta certificata in entrata");
			
			str.append("\r\n\nSede "+messaggio.getCodiceAOO());
			str.append("\r\n\nCompositore: "+messaggio.getAutoreCompositore());
			str.append("\r\n\nMittente: "+ messaggio.getAutoreMittente());
			str.append("\r\n\nSegnatura padre:");
			str.append("\r\n\nSegnatura origine:");
			str.append("\r\n\nSegnatura mittente:");
			str.append("\r\n\nTitolario: "+messaggio.getClassifica()+" "+messaggio.getDesClassifica());
			str.append("\r\n\nRiservato: "+messaggio.getRiservato());
			str.append("\r\n\nSensibile: "+messaggio.getSensibile());
			str.append("\r\n\nTipo Ricevuta: "+messaggio.getTipoRicevuta());
			str.append("\r\n\nConferma ricezione: "+messaggio.getConfermaRicezione()+" "+messaggio.getAddrConfermaRicezione());
			str.append("\r\n\n-------------------------");
			str.append("\r\n\nRisposta: "+messaggio.getIsReply());
			str.append("\r\n\nMessage-ID: "+messaggio.getMessageId());
			str.append("\r\n\nDa: "+messaggio.getFromAddr());
			str.append("\r\n\nPosted date: "+messaggio.getPostedDate());
			str.append("\r\n\nPer ");
			for(DestinatarioDTO a : messaggio.getElencoDestinatariPer()){
				str.append("\r\n\n"+a.getAddr());
			}
			str.append("\r\n\nCc ");
			for(DestinatarioDTO a : messaggio.getElencoDestinatariCc()){
				str.append("\r\n\n"+a.getAddr());
			}
			str.append("\r\n\nAllegati "+messaggio.getFromAddr());
			if(messaggio.getElencoFileAllegati().size()>0){
				for(AllegatoDTO a : messaggio.getElencoFileAllegati()){
					str.append("\r\n\n"+a.getFileName().replaceAll("&", "e"));
				}
			}else{
				str.append("\r\n\nnon presenti");
			}
			str.append("\r\n\nOggetto: "+messaggio.getSubject());
			str.append("\r\n\n-------------------------");
			str.append("\r\n\n"+messaggio.getBody());
//			bw.write(str.toString());
//			bw.close();
			
			allegato.setContentType("txt");
			allegato.setFileData(str.toString().getBytes());
			allegato.setFileName("TestoDelMessaggio.txt");
			allegato.setFileSize(str.length());
			allegato.setMessageID(messaggio.getMessageId());
			if(messaggio.getPostedDate()!=null){
				allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
				SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
				String meseAnno = sfd.format(messaggio.getPostedDate());
				allegato.setMeseAnno(meseAnno);
//				allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
			}
			allegato.setTestoAllegato(str.toString());
			allegato.setTipoAllegato("TXT");
    	    
//    	    temp.delete();
    	    */
		}catch(Exception e){
			 log.error("Errore nella creazione del TestoDelMessaggio.txt per il messaggio "+messaggio.getMessageId(),e);
		}
		return allegato;
	}
public AllegatoDTO creaMessagePecCompleto(MessaggioDTO messaggio){
		
		AllegatoDTO allegato = new AllegatoDTO();
		try{
//			File temp = File.createTempFile("postacert", ".eml");
			allegato.setContentType("eml");
			allegato.setFileData(messaggio.getRawContent().getBytes());
			allegato.setFileName("postacert.eml");
			allegato.setFileSize(messaggio.getRawContent().getBytes().length);
			allegato.setMessageID(messaggio.getMessageId());
			if(messaggio.getPostedDate()!=null){
				allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
				SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
				String meseAnno = sfd.format(messaggio.getPostedDate());
				allegato.setMeseAnno(meseAnno);
//				allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
			}
			allegato.setTestoAllegato(null);
			allegato.setTipoAllegato("EML");
			
//			File temp = new File("C:\\testPdf\\messaggiBonificati\\postacert.eml");
//			FileOutputStream fo = new FileOutputStream(temp);
//			fo.write(allegato.getFileData());
//			fo.flush();
//			fo.close();
			
		}catch(Exception e){
			 log.error("Errore nella creazione del TestoDelMessaggio.txt per il messaggio "+messaggio.getMessageId(),e);
		}
		return allegato;
	}
	public static AllegatoDTO creaAllegatoInterop(String nomeFile, String contenuto, String messageId){
		
		AllegatoDTO allegato = new AllegatoDTO();
		try{
//			File temp = File.createTempFile(nomeFile, ".xml");
//			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			StringBuilder str = new StringBuilder(contenuto);
			allegato.setContentType("text/xml");
			allegato.setFileData(str.toString().getBytes());
			allegato.setFileName(nomeFile+".xml");
			allegato.setFileSize(str.length());
			allegato.setTestoAllegato(str.toString());
			allegato.setTipoAllegato("xml");
    	    
    	    
		}catch(Exception e){
			 log.error("Errore nella creazione del RisultatoXml per interoperabilit� per il messaggio "+messageId);
		}
		return allegato;
	}
	public byte[] estraiASCIItoHTML(byte ascii) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException, Exception{

		String query = "select [Html Entity] FROM ConversionTable where [ASCII] = ?";
	    Connection conn = cm.getConnection(Constants.DB_PECPEI);

//		per chiamate locali
//		String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
//	    Class.forName(driver).newInstance();
//	    String urlDB = "jdbc:sqlserver://SQLINPS04.inps\\SQLINPS04;databaseName=PECPEI";
//	    Connection conn=DriverManager.getConnection(urlDB, "PECPEI","aB7-9Hk$pg");
//
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String valoreHtml=null;
		try
		{
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, ascii+256);
			rs  = pstmt.executeQuery();
			boolean trovato = false;
			while (rs.next()) {
				valoreHtml = rs.getString("Html Entity");
				trovato = true;
		    }
			if(!trovato){
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, "99999999");
				rs  = pstmt.executeQuery();
				while (rs.next()) {
					valoreHtml = rs.getString("Html Entity");
					trovato = true;
			    }
			}
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
					conn.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return valoreHtml.getBytes();
		
	}
	public byte[] estraiASCIItoCarattere(byte ascii) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException, Exception{

		String query = "select [Carattere] FROM ConversionTable where [ASCII] = ?";
	    Connection conn = cm.getConnection(Constants.DB_PECPEI);

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String valoreHtml=null;
		try
		{
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, ascii+256);
			rs  = pstmt.executeQuery();
			boolean trovato = false;
			while (rs.next()) {
				valoreHtml = rs.getString("Carattere");
				trovato = true;
		    }
			if(!trovato){
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, "99999999");
				rs  = pstmt.executeQuery();
				while (rs.next()) {
					valoreHtml = rs.getString("Carattere");
					trovato = true;
			    }
			}
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
					conn.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return valoreHtml.getBytes();
		
	}
	
	public String leggiSegnatura( String messageId) throws DAOException{
		
		log.debug("leggiSegnatura :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int msgID = 0;
		String out = null;
		 
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_SEGNATURA);
			pstmt.setString( 1, messageId );
//			pstmt.setString( 2, acoountPEC );
//			pstmt.setString( 3, codiceAOO );
			rs  = pstmt.executeQuery();			
			if (rs.next()) {
				
				out = rs.getString("Segnatura");
				
		    } //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("leggiSegnatura :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		log.debug("leggiSegnatura :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
		return out;
	}
	
	public static void main(String args[]){
		
		ConfigProp config = new ConfigProp();
		try {
			config.init();
			MessaggiVerificaPecDAO dao = new MessaggiVerificaPecDAO();
			System.out.println(dao.verificaTotaleMessaggiInviati());
		} catch (Exception e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		}
		
	}
	
}
