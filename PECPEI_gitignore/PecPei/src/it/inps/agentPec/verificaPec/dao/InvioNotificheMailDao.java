package it.inps.agentPec.verificaPec.dao;

import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.util.Constants;
import it.inps.agentPec.verificaPec.dto.IndirizziInvioNotificheMail;
import it.inps.agentPec.verificaPec.dto.InvioNotificheMail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author rstabile
 *
 */

public class InvioNotificheMailDao extends AgentBaseDAO{

	public InvioNotificheMail getProcedura(String name) {
		
		PreparedStatement ps = null;
		ResultSet rs =null;
		Connection conn = null;
		InvioNotificheMail p = new InvioNotificheMail();
		try {
			conn = cm.getConnection(Constants.DB_VERIFICAPEC);
			String query = "select id,procedura,descrizione,oggettoMail,corpoMail,dtUltimaModifica,matrUltimaModifica,flgAttivo from InvioNotificheMail where procedura = ? and flgAttivo = 1";
			ps = conn.prepareStatement(query);
			ps.setString(1, name);
			rs = ps.executeQuery();
			
			while(rs.next()){
				
				p.setId(rs.getInt("id"));
				p.setProcedura(rs.getString("procedura"));
				p.setDescrizione(rs.getString("descrizione"));
				p.setOggettoMail(rs.getString("oggettoMail"));
				p.setCorpoMail(rs.getString("corpoMail"));
				p.setDtUltimaModifica(rs.getTimestamp("dtUltimaModifica"));
				p.setMatrUltimaModifica(rs.getString("matrUltimaModifica"));
				p.setFlgAttivo(rs.getBoolean("flgAttivo"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}catch(Exception e){
		    e.printStackTrace();
		}finally{
			try {
				if(conn!=null) conn.close();
				if(ps!=null) ps.close();
				if(rs!=null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return p;
	}
	
public List<IndirizziInvioNotificheMail> getDestinatari(String procedura) {
		
		List<IndirizziInvioNotificheMail> destinatari = new ArrayList<IndirizziInvioNotificheMail>();
		PreparedStatement ps = null;
		ResultSet rs =null;
		Connection conn = null;
		IndirizziInvioNotificheMail i;
		try {
			conn = cm.getConnection(Constants.DB_VERIFICAPEC);
			String query = "select id,procedura,mail,dtUltimaModifica,matrUltimaModifica,flgAttivo from IndirizziInvioNotificheMail where procedura = ? and flgAttivo = 1";
			ps = conn.prepareStatement(query);
			ps.setString(1, procedura);
			rs = ps.executeQuery();
			
			while(rs.next()){
				i = new IndirizziInvioNotificheMail();
				i.setId(rs.getInt("id"));
				i.setProcedura(rs.getString("procedura"));
				i.setMail(rs.getString("mail"));
				i.setDtUltimaModifica(rs.getTimestamp("dtUltimaModifica"));
				i.setMatrUltimaModifica(rs.getString("matrUltimaModifica"));
				i.setFlgAttivo(rs.getBoolean("flgAttivo"));
				
				destinatari.add(i);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}catch(Exception e){
		    e.printStackTrace();
		}finally{
			try {
				if(conn!=null) conn.close();
				if(ps!=null) ps.close();
				if(rs!=null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return destinatari;
	}
}


