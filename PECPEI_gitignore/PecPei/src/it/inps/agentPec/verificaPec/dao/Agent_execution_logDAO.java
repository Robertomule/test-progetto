package it.inps.agentPec.verificaPec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.dto.AgentExecutionLogDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.DBConnectionManager;

public class Agent_execution_logDAO extends AgentBaseDAO{

	private static final String INS_AGENT_EXEC_LOG_QRY = "INSERT INTO Agent_execution_log (start_time, end_time, agent_status, error_desc, agent_type) " + 
								" VALUES (?,?,?,?,?)";
	
	private static final String UPD_AGENT_EXEC_LOG_QRY = "UPDATE Agent_execution_log SET " +
								" end_time=? , agent_status=?, error_desc=? " + 
								" WHERE execution_id=? AND agent_type=? ";
	
	private static final String INS_AGENT_EXEC_LOG_QRY_OLD = "INSERT INTO Agent_execution_log (start_time, end_time, agent_status, error_desc) " + 
								" VALUES (?,?,?,?)";

	private static final String UPD_AGENT_EXEC_LOG_QRY_OLD = "UPDATE Agent_execution_log SET " +
								" end_time=? , agent_status=?, error_desc=? " + 
								" WHERE execution_id=?";

	
	private static final String SELECT_AGENT_EXECUTE = " select max(start_time) start_time, agent_status from Agent_execution_log WHERE agent_type = ? group by agent_status";
	
	public int insertAgentExecution(AgentExecutionLogDTO agentExecDTO) throws DAOException  {
		
		System.out.println("Agent_execution_logDAO :: insertAgentExecution START");
		int executionID = -1;				
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);
			ConfigDAO configDao = new ConfigDAO();
			if(Boolean.parseBoolean(configDao.getProperty("abilitaTotaliMessaggiDaInviare", "false")))
				pstmt = connPecPei.prepareStatement(INS_AGENT_EXEC_LOG_QRY, Statement.RETURN_GENERATED_KEYS);
			else
				pstmt = connPecPei.prepareStatement(INS_AGENT_EXEC_LOG_QRY_OLD, Statement.RETURN_GENERATED_KEYS);
			
			pstmt.setTimestamp(1, agentExecDTO.getStart_time());
			if(agentExecDTO.getEnd_time()!=null)
				pstmt.setTimestamp(2, agentExecDTO.getEnd_time());
			else
				pstmt.setNull(2, java.sql.Types.TIMESTAMP);
			pstmt.setString(3, agentExecDTO.getAgent_status());
			if(agentExecDTO.getError_desc()!=null)
				pstmt.setString(4, agentExecDTO.getError_desc());
			else
				pstmt.setNull(4, java.sql.Types.VARCHAR);
			
			if(Boolean.parseBoolean(configDao.getProperty("abilitaTotaliMessaggiDaInviare", "false"))){			
				if(agentExecDTO.getAgent_type()>0)
					pstmt.setInt(5, agentExecDTO.getAgent_type());
				else
					pstmt.setNull(5, java.sql.Types.INTEGER);
			}
			pstmt.executeUpdate();
			System.out.println("Agent_execution_logDAO :: insertAgentExecution :: dopo executeUpdate");
			generatedKeys = pstmt.getGeneratedKeys();
			if (generatedKeys.next()) 
				executionID = generatedKeys.getInt(1);
			else 
				throw new SQLException("Impossibile ottenere la chiava generata executionID");			
			pstmt.close();
			if(generatedKeys != null) 
				try {
					generatedKeys.close();
				} catch (Exception e){}	
	
		}
		catch( Exception eh )
		{
			System.err.println("Agent_execution_logDAO :: insertAgentExecution Exception: " + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		System.out.println("Agent_execution_logDAO :: insertAgentExecution END, executionID="+executionID);
		return executionID;
	}
	
	
	
	public int updateAgentExecution(AgentExecutionLogDTO agentExecDTO)
		throws Exception {
				
		System.out.println("Agent_execution_logDAO :: updateAgentExecution START");
		int ret = -1;
		Connection connPecPei = null;
		PreparedStatement pstmt = null;		
		long execId = agentExecDTO.getExecution_id();
		System.out.println("Agent_execution_logDAO :: updateAgentExecution agent_execution_id=" + execId);
		if(execId < 0){
			throw new Exception("Impossibile aggiornare l'esecuzione del job, identificativo non definito.");
		}
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);
			ConfigDAO configDao = new ConfigDAO();
			if(Boolean.parseBoolean(configDao.getProperty("abilitaTotaliMessaggiDaInviare", "false")))
				pstmt = connPecPei.prepareStatement(UPD_AGENT_EXEC_LOG_QRY);
			else
				pstmt = connPecPei.prepareStatement(UPD_AGENT_EXEC_LOG_QRY_OLD);
			
			pstmt.setTimestamp(1, agentExecDTO.getEnd_time());			
			pstmt.setString(2, agentExecDTO.getAgent_status());
			if(agentExecDTO.getError_desc()!=null)
				pstmt.setString(3, agentExecDTO.getError_desc());
			else
				pstmt.setNull(3, java.sql.Types.VARCHAR);

			pstmt.setLong(4, agentExecDTO.getExecution_id());
			
			if(Boolean.parseBoolean(configDao.getProperty("abilitaTotaliMessaggiDaInviare", "false"))){	
				if(agentExecDTO.getAgent_type()>0)
					pstmt.setInt(5, agentExecDTO.getAgent_type());
				else
					pstmt.setNull(5, java.sql.Types.INTEGER);
			}
			
			ret = pstmt.executeUpdate();		
	
		}
		catch( Exception eh )
		{
			System.out.println("Agent_execution_logDAO :: updateAgentExecution Exception: " + eh.getMessage());
			throw eh;
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}		
		
		
		System.out.println("Agent_execution_logDAO :: updateAgentExecution END");
		return ret;
	}
	
	public Date selectAgentExecution(int type) throws Exception {
		
		int ret = -1;
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Date dataReturn = null;
		try
		{
			connPecPei = cm.getConnection(Constants.DB_VERIFICAPEC);
			pstmt = connPecPei.prepareStatement(SELECT_AGENT_EXECUTE);
			pstmt.setInt(1, type);
			rs = pstmt.executeQuery();
			while(rs.next()){
				dataReturn = rs.getTimestamp("start_time");
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}		
		return dataReturn;
	}
	
	public static void main (String[] s){
		Date data = new Date();
		Date dataUltimaEsecuzione = new Date();
		long differenzaGiorni = (data.getTime()-((dataUltimaEsecuzione.getTime())-(1000 * 60 * 60 * 48))) / (1000 * 60 * 60 * 24);
		System.out.println(differenzaGiorni);
	}
	
}
