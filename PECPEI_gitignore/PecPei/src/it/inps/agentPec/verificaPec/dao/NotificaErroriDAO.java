package it.inps.agentPec.verificaPec.dao;

import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.inps.agentPec.verificaPec.dto.NotificaErroriDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class NotificaErroriDAO extends AgentBaseDAO
{
	private final static String INS_NOTIFICA_ERRORE = "INSERT INTO NotificaErrori (Codice, Messaggio, MessageID)" +											
	" VALUES(?,?,?)";
	
	public long insertNotifica(NotificaErroriDTO erroreDTO) throws DAOException {			
		
		//System.out.println("erroreDTO :: insertNotifica");
		
		long idNuovaNotifica = -1;

		if(erroreDTO != null){
			Connection connVerificaPec = null;
			try {
				connVerificaPec = cm.getConnection(Constants.DB_VERIFICAPEC);    
				connVerificaPec.setAutoCommit(false);
      		
				idNuovaNotifica = doInsertNotifica(connVerificaPec, erroreDTO);									

				if(idNuovaNotifica != -1){
					connVerificaPec.commit();
					//System.out.println("erroreDTO :: insertNotifica :: commit eseguito");
				}
			}
			catch(DAOException ex){
				ex.printStackTrace();
				//System.err.println("erroreDTO :: insertNotifica :: errore " + ex.getMessage());				
				if(connVerificaPec!=null)
					try {
						connVerificaPec.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}	
				throw ex;
			}
			catch(Exception eh){
				eh.printStackTrace();
				//System.err.println("erroreDTO :: insertNotifica :: errore " + eh.getMessage());				
				if(connVerificaPec!=null)
					try {
						connVerificaPec.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}				
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB");
			}			
			finally{
				try {
					if(connVerificaPec != null)
						connVerificaPec.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}		
		return idNuovaNotifica;
	
	} //insertNotifica
	
	
	private long doInsertNotifica(Connection conn, NotificaErroriDTO notificaDTO) throws DAOException {
		
		long idNotifica = -1;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try{
				pstmt = conn.prepareStatement(INS_NOTIFICA_ERRORE, Statement.RETURN_GENERATED_KEYS);			
				pstmt.setInt(1, notificaDTO.getCodice());
				pstmt.setString(2, notificaDTO.getMessaggio());
				
				if(notificaDTO.getMessageID() != null)
					pstmt.setString(3, notificaDTO.getMessageID());
				else
					pstmt.setNull(3, java.sql.Types.NVARCHAR);
				
				pstmt.executeUpdate();
				generatedKeys = pstmt.getGeneratedKeys();
				if (generatedKeys.next()) {
					idNotifica = generatedKeys.getLong(1);
				} else {
					throw new SQLException("Impossibile ottenere la chiava generata per la notifica");
				}			
				//System.out.println("Inserimento in NotificaErrori effettuato correttamente: idNotifica:" + idNotifica);
				pstmt.close();
				if(generatedKeys != null) 
					try {
						generatedKeys.close();
					} catch (Exception e){}
			}
			catch(SQLException ex){
				System.err.println("Errore durante inserimento in NotificaErrori: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB" );
			}
			catch(Exception ex){
				System.err.println("Errore durante inserimento in NotificaErrori: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException(ex.getMessage());
			}
			
			return idNotifica;
		}
}
