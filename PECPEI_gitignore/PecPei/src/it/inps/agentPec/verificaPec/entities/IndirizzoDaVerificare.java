package it.inps.agentPec.verificaPec.entities;

import java.io.Serializable;
import java.sql.Timestamp;

public class IndirizzoDaVerificare implements Serializable{

	private static final long serialVersionUID = -4019215109222151844L;

	private String indirizzo ;
	private String codiceFiscale ;
	private String parametriExtra ; 
	private String codiceApplicazione ;
//	private Timestamp dataRichiesta;
	
	public String getIndirizzo() {
		return indirizzo;
	}
	public String getParametriExtra() {
		return parametriExtra;
	}
	public void setParametriExtra(String parametriExtra) {
		this.parametriExtra = parametriExtra;
	}
	public String getCodiceApplicazione() {
		return codiceApplicazione;
	}
	public void setCodiceApplicazione(String codiceApplicazione) {
		this.codiceApplicazione = codiceApplicazione;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
//	public Timestamp getDataRichiesta() {
//		return dataRichiesta;
//	}
//	public void setDataRichiesta(Timestamp dataRichiesta) {
//		this.dataRichiesta = dataRichiesta;
//	}
	
	
}
