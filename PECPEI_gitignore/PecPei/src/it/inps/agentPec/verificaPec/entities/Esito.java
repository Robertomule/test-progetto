package it.inps.agentPec.verificaPec.entities;

import java.io.Serializable;

public class Esito implements Serializable{

	private static final long serialVersionUID = -69066300206430454L;
	
	private String codiceEsito;
	private String descrizioneEsito;
	private String esitoAcquisizione;
	private String identificativo;
	
	public String getIdentificativo() {
		return identificativo;
	}
	public void setIdentificativo(String identificativo) {
		this.identificativo = identificativo;
	}
	public String getCodiceEsito() {
		return codiceEsito;
	}
	public void setCodiceEsito(String codiceEsito) {
		this.codiceEsito = codiceEsito;
	}
	public String getDescrizioneEsito() {
		return descrizioneEsito;
	}
	public void setDescrizioneEsito(String descrizioneEsito) {
		this.descrizioneEsito = descrizioneEsito;
	}
	public String getEsitoAcquisizione() {
		return esitoAcquisizione;
	}
	public void setEsitoAcquisizione(String esitoAcquisizione) {
		this.esitoAcquisizione = esitoAcquisizione;
	}
	
	
	

}
