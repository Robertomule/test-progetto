package it.inps.agentPec.verificaPec;

import it.inps.agentPec.verificaPec.manager.VerificaPec_Manager;
import it.inps.agentPec.verificaPec.util.VerificaPecType;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

public class VerificaPecCallable implements Callable<String> {
	private static Logger log = Logger.getLogger(VerificaPecCallable.class);
	private VerificaPecType verificaPecType;
	private VerificaPec_Manager verificaPec_Manager;
	public VerificaPecCallable(VerificaPecType agentType){
		this.verificaPecType = agentType;
	}
	
	@Override
	public String call() {
		// invece di scegliere in base ad uno switch-case eseguire una getInstance dell'oggetto
		// che serve e chiamare il metodo elabora
		log.debug("AgentRunnable :: call :: verificaPecType="+verificaPecType);
		switch(verificaPecType) {
			
			case VERIFICA_PEC:
				System.out.println("crea un nuovo VerificaPec_Manager");
				try {
					verificaPec_Manager = new VerificaPec_Manager();
					verificaPec_Manager.elabora();
				} catch (Exception e) {
					log.error( e.getMessage() );
				}					
				break;
		}
		
		return "0";
	}

}
