package it.eustema.inps.mailCommander.vo;

import java.io.Serializable;
import java.util.Collection;

public class QueryVO  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3433226318216762361L;
	
	private String query;
	private Collection<ParamVO> listaParametri = null;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Collection<ParamVO> getListaParametri() {
		return listaParametri;
	}

	public void setListaParametri(Collection<ParamVO> listaParametri) {
		this.listaParametri = listaParametri;
	}

	
	
	
}
