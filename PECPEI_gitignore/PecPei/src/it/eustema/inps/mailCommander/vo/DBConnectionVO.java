package it.eustema.inps.mailCommander.vo;

import java.io.Serializable;

public class DBConnectionVO  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7292368949378508132L;
	
	private String url;
	private String user;
	private String password;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
