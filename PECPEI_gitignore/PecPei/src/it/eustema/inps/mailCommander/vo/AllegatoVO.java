package it.eustema.inps.mailCommander.vo;

import java.io.Serializable;

public class AllegatoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4920348970206771463L;
	
	private byte[] data;
	private String contentType;
	private String fileName;

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	

}
