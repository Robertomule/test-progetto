package it.eustema.inps.mailCommander.vo;

import java.io.Serializable;

public class MessageVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 563598010918173046L;
	
	private String message;
	private String subject;
	private String mittente;
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMittente() {
		return mittente;
	}
	public void setMittente(String mittente) {
		this.mittente = mittente;
	}
	
	

}
