package it.eustema.inps.mailCommander.vo;

import java.io.Serializable;

public class AutorizzazioneEmailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5978010577210647562L;
	
	private String email;
	private String nome;
	private String cognome;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	
	

}
