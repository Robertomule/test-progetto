package it.eustema.inps.mailCommander.vo;

import java.io.Serializable;

public class ParamVO implements Comparable<ParamVO>, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3727843088230108857L;
	
	private String name;
	private String value;
	private int index = -1;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	@Override
	public int compareTo(ParamVO o) {
		// TODO Auto-generated method stub
		if ( index < o.getIndex() ) return -100;
		else if ( index > o.getIndex() ) return 100;
		else return 0;
	}
	
	
}
