package it.eustema.inps.mailCommander.vo;

import java.io.Serializable;

public class LogVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5888336414275278760L;
	
	private String idMail;
	private CommandVO comando;
	private QueryVO query;
	private MessageVO messaggio;
	private int responseCode;
	private String responseDesc;
	
	public CommandVO getComando() {
		return comando;
	}
	public void setComando(CommandVO comando) {
		this.comando = comando;
	}
	public QueryVO getQuery() {
		return query;
	}
	public void setQuery(QueryVO query) {
		this.query = query;
	}
	public MessageVO getMessaggio() {
		return messaggio;
	}
	public void setMessaggio(MessageVO messaggio) {
		this.messaggio = messaggio;
	}
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDesc() {
		return responseDesc;
	}
	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}
	public String getIdMail() {
		return idMail;
	}
	public void setIdMail(String idMail) {
		this.idMail = idMail;
	}
	
	
	
}
