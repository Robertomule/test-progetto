package it.eustema.inps.mailCommander.vo;

import java.io.Serializable;
import java.util.Collection;

public class CommandVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8773051744368399269L;
	
	private String nomeComando;
	private String nomeDB;
	private Collection<ParamVO> parametri;
	
	public String getNomeComando() {
		return nomeComando;
	}
	public void setNomeComando(String nomeComando) {
		this.nomeComando = nomeComando;
	}
	public String getNomeDB() {
		return nomeDB;
	}
	public void setNomeDB(String nomeDB) {
		this.nomeDB = nomeDB;
	}
	public Collection<ParamVO> getParametri() {
		return parametri;
	}
	public void setParametri(Collection<ParamVO> parametri) {
		this.parametri = parametri;
	}
	
	
}
