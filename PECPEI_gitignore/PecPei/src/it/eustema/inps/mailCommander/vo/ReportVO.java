package it.eustema.inps.mailCommander.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ReportVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3212253354706811688L;
	
	private List<String> etichette = new ArrayList<String>();
	private List<List<String>> values = new ArrayList<List<String>>();
	
	public String getEtichetta( int index ){
		return etichette.get( index );
	}
	
	public String getValue( int row, int col ){
		return values.get( row ).get( col );
	}
	
	public void addValue( String value ){
		values.get( values.size() - 1 ).add( value );
	}
	
	public void addNewRow(){
		values.add( new ArrayList<String>() );
	}
	
	public void addEtichetta( String etichetta ){
		etichette.add( etichetta );
	}
	
	public int getNumRows(){
		return values.size();
	}
	
	public int getNumCols(){
		return etichette.size();
	}
}
