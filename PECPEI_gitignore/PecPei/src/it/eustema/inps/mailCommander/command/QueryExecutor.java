package it.eustema.inps.mailCommander.command;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.db.DBConnectionManager;
import it.eustema.inps.mailCommander.vo.QueryVO;
import it.eustema.inps.mailCommander.vo.ReportVO;


public class QueryExecutor implements Command {

	private QueryVO query;
	private String nomeDB;
	
	public QueryExecutor( QueryVO query, String nomeDB ){
		this.query = query;
		this.nomeDB = nomeDB;
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ReportVO out = null;
		
		if ( query.getQuery().toUpperCase().indexOf("INSERT") != -1 ){
			throw new BusinessException( -2002, "Non � possibile eseguire INSERT");
		} else if ( query.getQuery().toUpperCase().indexOf("DELETE") != -1 ){
			throw new BusinessException( -2003, "Non � possibile eseguire DELETE");
		} else if ( query.getQuery().toUpperCase().indexOf("NOLOCK") == -1 && query.getQuery().toUpperCase().indexOf("HELP") == -1 && query.getQuery().toUpperCase().indexOf("UPDATE") == -1 ){
			throw new BusinessException( -2004, "Non � possibile eseguire Query senza WITH (NOLOCK)");
		}
		
		try {
			conn = DBConnectionManager.getInstance().getConnection( nomeDB );
			pst = conn.prepareStatement( query.getQuery() );
			
			if ( query.getQuery().toUpperCase().startsWith("SELECT") ){
			
				rs = pst.executeQuery();
				
				out = new ReportVO();
				
				for ( int i=1; i<= pst.getMetaData().getColumnCount(); i++ ){
					out.addEtichetta( pst.getMetaData().getColumnLabel(i) );
				}
				
				while ( rs.next() ){
					out.addNewRow();
					Object el = null;
					
					
					for ( int i=1; i<= pst.getMetaData().getColumnCount(); i++ ){
						el = rs.getObject(i);
						if ( el == null ) out.addValue("NULL");
						else if ( el instanceof String ) out.addValue( ((String)el).trim() );
						else if ( el instanceof Integer ) out.addValue( Integer.toString( ((Integer)el) ));
						else if ( el instanceof Long ) out.addValue( Long.toString( ((Long)el) ));
						else if ( el instanceof Short ) out.addValue( Short.toString( ((Short)el) ));
						else if ( el instanceof Float ) out.addValue( Float.toString( ((Float)el) ));
						else if ( el instanceof Double ) out.addValue( Double.toString( ((Double)el) ));
						else if ( el instanceof Byte ) out.addValue( Byte.toString( ((Byte)el) ));
						else if ( el instanceof Timestamp ) {
							SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy hh:mm:ss" );
							out.addValue( sdf.format( (Timestamp)el ) );
						}
						
						else if ( el instanceof Date ) {
							SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );
							out.addValue( sdf.format( (Date)el ) );
						} else out.addValue("TIPO DATO NON GESTITO");
						
					}
				}
			
			} else {
				int res = pst.executeUpdate();
				out = new ReportVO();
				out.addEtichetta("NUMERO RIGHE UPDATE");
				
				out.addNewRow();
				out.addValue( Integer.toString( res ));
			}
			
		} catch (Exception e) {
			throw new BusinessException( -2001, "Errore nell'esecuzione della query: " + e.getMessage() );
		} 
		
		
		return out;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "QueryExecutor";
	}

}
