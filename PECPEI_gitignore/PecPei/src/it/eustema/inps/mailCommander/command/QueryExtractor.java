package it.eustema.inps.mailCommander.command;

import java.sql.SQLException;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.db.MailCommanderDAO;
import it.eustema.inps.mailCommander.vo.CommandVO;
import it.eustema.inps.mailCommander.vo.QueryVO;


public class QueryExtractor implements Command {

	private CommandVO command;
	
	public QueryExtractor( CommandVO command ){
		this.command = command;
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		QueryVO out = null;
		MailCommanderDAO dao = new MailCommanderDAO();
		
		try {
			out = dao.getQueryFromNomeComando( command.getNomeComando() , command.getNomeDB() );
		} catch ( SQLException s){
			throw new BusinessException(-1005, "Errore esecuzione Query: " + s.getMessage() );
		}
		
		if ( out == null ) throw new BusinessException( -1006, "Comando non trovato" );
		
		return out;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "QueryExtractor";
	}

}
