package it.eustema.inps.mailCommander.command;

import java.util.ArrayList;
import java.util.Collection;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.vo.CommandVO;
import it.eustema.inps.mailCommander.vo.MessageVO;
import it.eustema.inps.mailCommander.vo.ParamVO;
import it.eustema.inps.utility.ConfigProp;


public class MessageExtractor implements Command {

	private MessageVO message;
	
	
	public MessageExtractor( MessageVO message ){
		this.message = message;
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		
		if ( !message.getSubject().toUpperCase().startsWith( ConfigProp.subjectTrigger ) ){
			throw new BusinessException( 9999, "Non � un messaggio da gestire" );
		}
		
		CommandVO out = new CommandVO();
		out.setNomeDB( message.getSubject().toUpperCase().replaceAll(ConfigProp.subjectTrigger, "") );
		
		int nci = message.getMessage().indexOf( '[' ) + 1;
		int ncf = message.getMessage().indexOf( ']' );
		out.setNomeComando( message.getMessage().substring(nci, ncf) );
		
		StringBuilder b = null;
		ParamVO p = null;
		Collection<ParamVO> parametri = new ArrayList<ParamVO>();
		int index = 0;
		
		for ( int i=0; i<message.getMessage().length(); i++ ){
			if ( message.getMessage().charAt(i) == '{' ){
				b = new StringBuilder();
				p = new ParamVO();
				int itemp = -1;
				for ( int j = i+1; j<message.getMessage().length(); j++ ){
					//if ( message.getMessage().charAt(j) == '=' && message.getMessage().toUpperCase().indexOf("SELECT") == -1 && message.getMessage().toUpperCase().indexOf("UPDATE") == -1 ){
					if ( message.getMessage().charAt(j) == '=' && !message.getMessage().substring(i+1).toUpperCase().startsWith("SELECT") && !message.getMessage().substring(i+1).toUpperCase().startsWith("UPDATE") && !message.getMessage().substring(i+1).toUpperCase().startsWith("DELETE") && !message.getMessage().substring(i+1).toUpperCase().startsWith("INSERT")){
						p.setName( b.toString().trim() );
						itemp = j;
						break;
					} else if ( message.getMessage().charAt(j) == '}' ){
						p.setValue( b.toString().trim() );
						break;
					} else {
						b.append( message.getMessage().charAt(j) );
					}
				}
				
				b = new StringBuilder();
				if ( p.getValue() == null ) 
					for ( int j = itemp+1; j<message.getMessage().length(); j++ ){
						if ( message.getMessage().charAt(j) == '}' ){
							p.setValue( b.toString().trim() );
							break;
						} else {
							b.append( message.getMessage().charAt(j) );
						}
					}
				
				p.setIndex( ++index );
				parametri.add( p );
				
			}
		}
		
		out.setParametri( parametri );
		
		return out;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "MessageExtractor";
	}

}
