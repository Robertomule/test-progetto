package it.eustema.inps.mailCommander.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.vo.ParamVO;
import it.eustema.inps.mailCommander.vo.QueryVO;

public class QueryExaminator implements Command{
	private QueryVO query;
	
	public QueryExaminator( QueryVO query ){
		this.query = query;
	}
	
	
	//Elaborazione singolo parametro
	private String getParam( String query, int finalIndex ){
		
		if ( query.trim().equals("?") )
			return "";
		
		boolean fineTrovato = false;
		boolean inizioTrovato = false;
		int indexFine=-1;
		int indexInizio=-1;
		for ( int i= finalIndex; i>=0; i-- ){
			if ( indexFine != -1 && indexInizio != -1 ) break;
			
			if ( !fineTrovato && query.charAt(i) != '=' && query.charAt(i) != '<' && query.charAt(i) != '>' && query.charAt(i) != ' ' && query.charAt(i) != '?' && query.charAt(i) != '\'') {
				fineTrovato = true;
				indexFine = i+1;
			}
			
			
			
			else if ( !inizioTrovato && (query.charAt(i) != ' ' && query.charAt(i) != '\'') && fineTrovato && indexFine != -1 ){
				
				inizioTrovato = true;
			} else if ( inizioTrovato && (query.charAt(i) == ' ') && fineTrovato && indexFine != -1 ){
				indexInizio = i+1;
				break;
			}
		}
		
		if ( indexInizio < 0 ) indexInizio = 0;
		
		return query.substring(indexInizio, indexFine).trim().toUpperCase().replaceAll("LIKE", "").trim();
	}
	

	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		Collection<ParamVO> listaParametri = new ArrayList<ParamVO>();
		ParamVO vo = null;
		
		int index = 0;
		
		
		//VERIFICA DEL NUMERO DI PARAMETRI TOTALE
		for ( int i= query.getQuery().length()-1; i>=0; i-- ){
			if ( query.getQuery().charAt(i) == '?' ){
				 index++;
			}
		}
		
		
		//Elaborazione
		for ( int i= query.getQuery().length()-1; i>=0; i-- ){
			if ( query.getQuery().charAt(i) == '?' ){
				vo = new ParamVO();
				vo.setIndex( index-- );
				vo.setName( getParam(query.getQuery(), i) );
				listaParametri.add( vo );
			}
		}
		
		Collections.sort( (List<ParamVO>)listaParametri );
		query.setListaParametri( listaParametri );
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "QueryExaminator";
	}
		
}
