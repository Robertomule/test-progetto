package it.eustema.inps.mailCommander.command;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import sun.misc.BASE64Encoder;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.db.MailCommanderDAO;
import it.eustema.inps.mailCommander.vo.LogVO;


public class WriteLog implements Command {

	private LogVO log;
	
	public WriteLog( LogVO log ){
		this.log = log;
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		try {
			SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy hh:mm:ss" );
			String dig = log.getMessaggio().getMittente() + "-" + sdf.format( new Timestamp( new Date().getTime() ) ) + new Random().nextInt();
			byte[] bytesOfMessage = dig.getBytes("UTF-8");

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);
			
			log.setIdMail( new BASE64Encoder().encode( thedigest ) );
			new MailCommanderDAO().writeLog(log);
		} catch (SQLException e) {
			throw new BusinessException( -3030, "Errore nella scrittura del LOG: " + e.getMessage() );
		} catch (NoSuchAlgorithmException e) {
			throw new BusinessException( -3031, "Errore nella scrittura del LOG: " + e.getMessage() );
		} catch (UnsupportedEncodingException e) {
			throw new BusinessException( -3032, "Errore nella scrittura del LOG: " + e.getMessage() );
		}
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

}
