package it.eustema.inps.mailCommander.command;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.vo.ParamVO;
import it.eustema.inps.mailCommander.vo.QueryVO;

public class QueryInjector implements Command{
	private QueryVO query;
	private Collection<ParamVO> listaParametri;
	
	public QueryInjector( QueryVO query, Collection<ParamVO> listaParametri ){
		this.query = query;
		this.listaParametri = listaParametri;
	}

	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		if ( listaParametri.size() !=  query.getListaParametri().size() ){
			throw new BusinessException( -3000, "La query si aspetta " + query.getListaParametri().size() + " parametri in input, mentre ne sono stati passati " + listaParametri.size() );
		}
		
		Collections.sort( (List<ParamVO>)listaParametri );
		
		boolean findIndex = false;
		for ( ParamVO param : listaParametri ){
			if ( param.getName() != null ){
				findIndex = true;
				break;
			}
		}
		
		if ( findIndex ){
			findIndex();
		}
		
		
		for ( ParamVO param: listaParametri ){
			
			String newQuery = query.getQuery().substring(0, query.getQuery().indexOf('?') ) + param.getValue() + query.getQuery().substring(query.getQuery().indexOf('?') + 1, query.getQuery().length() );
			
			query.setQuery( newQuery );
		}
			
		
		return null;
	}
	
	
	private void findIndex(){
		Map<String, Integer> mappaParametriInseriti = new HashMap<String, Integer>();
		int num = 1;
		for ( ParamVO param: listaParametri ){
			if ( mappaParametriInseriti.containsKey( param.getName() ) ){
				num = mappaParametriInseriti.get( param.getName() ) + 1;
			}
			
			mappaParametriInseriti.put(param.getName(), num);
			int qn = 1;
			for ( ParamVO qp: query.getListaParametri() ){
				if ( qp.getName().equalsIgnoreCase( param.getName() ) ){
					if ( qn == num ){
						param.setIndex( qp.getIndex() );
						break;
					} else {
						qn++;
					}
				}
			}
		}
		
		Collections.sort( (List<ParamVO>)listaParametri );
	}
	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "QueryInjector";
	}
	
}
