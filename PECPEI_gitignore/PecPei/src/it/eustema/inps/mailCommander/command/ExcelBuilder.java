package it.eustema.inps.mailCommander.command;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.vo.ReportVO;



public class ExcelBuilder implements Command {

	private ReportVO report;
	private String queryTxt;
	
	public ExcelBuilder( ReportVO report, String queryTxt ){
		this.report = report;
		this.queryTxt = queryTxt;
	}
	@Override
	public Object execute() throws BusinessException {
		
		//Blank workbook
	   XSSFWorkbook workbook = new XSSFWorkbook();
	    
	   XSSFFont font = workbook.createFont();
	   font.setItalic(true);
	   font.setBold( true );
	   
	   XSSFFont fontBig = workbook.createFont();
	   fontBig.setItalic(true);
	   fontBig.setBold( true );
	   fontBig.setFontHeight(15.0);
	   
	   XSSFRichTextString t;
	   

		

	   //Create a blank sheet
	   XSSFSheet sheet = workbook.createSheet("Report Mail Commander");
	   Row row = null;
	   

	   //TITOLO
	   row = sheet.createRow(0);
	   for ( int i=0; i< report.getNumCols(); i++ ){
		   Cell cell = row.createCell(i);
		   cell.setCellValue("");
	   }
	   sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, report.getNumCols() ));
	   t = new XSSFRichTextString( "REPORT ESECUZIONE COMANDO" ); 
	   t.applyFont(fontBig);
	   row.getCell(0).setCellValue( t );
	   
	   
	   /*
	   //RIGA VUOTA
	   row = sheet.createRow(1);
	   for ( int i=0; i< report.getNumCols(); i++ ){
		   Cell cell = row.createCell(i);
		   cell.setCellValue("");
	   }
	   sheet.addMergedRegion(new CellRangeAddress(1, 0, 1, report.getNumCols() ));
	   row.getCell(0).setCellValue(" ");
	   
	   */
	   
	   //QUERY
	   row = sheet.createRow(2);
	   for ( int i=0; i< report.getNumCols(); i++ ){
		   Cell cell = row.createCell(i);
		   cell.setCellValue("");
	   }
	   
	   t = new XSSFRichTextString( "Query eseguita" ); 
	   t.applyFont(font);
	   row.getCell(0).setCellValue( t );
	   
	   
	   //QUERY
	   row = sheet.createRow(3);
	   for ( int i=0; i< report.getNumCols(); i++ ){
		   Cell cell = row.createCell(i);
		   cell.setCellValue("");
	   }
	   sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, report.getNumCols() ));
	   row.getCell(0).setCellValue(new XSSFRichTextString( queryTxt )  );
	   
	   
	   	   
	   
	   //ETICHETTE
	   row = sheet.createRow(6);
	   
	   for ( int i=0; i< report.getNumCols(); i++ ){
		   Cell cell = row.createCell(i);
		   t = new XSSFRichTextString( report.getEtichetta( i ) ); 
		   t.applyFont(font);
		   cell.setCellValue( t );
	   }

	  
	   //VALORI
	   for (int i=0; i<report.getNumRows(); i++ ) {
	        //create a row of excelsheet
	        row = sheet.createRow(i+7);

	        for (int j=0; j<report.getNumCols(); j++) {
	            Cell cell = row.createCell(j);
	            cell.setCellValue(report.getValue(i, j));
	        }
	    }
	    
	    
	    
	   //REFACTORING AUTOSIZE
	   for ( int i=0; i< report.getNumCols(); i++ ){
		   sheet.autoSizeColumn( i );
	   }
	    
	    
	    try 
	    {
	        //Write the workbook in file system
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        workbook.write(out);
	        
	        
	        return out.toByteArray();
	    } 
	    catch (Exception e)
	    {
	        throw new BusinessException( -5000, "Errore nella creazione del REPORT: " + e );
	    }
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "ExcelBuilder";
	}

}
