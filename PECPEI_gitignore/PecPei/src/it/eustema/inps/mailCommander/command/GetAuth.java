package it.eustema.inps.mailCommander.command;

import java.sql.SQLException;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.db.MailCommanderDAO;


public class GetAuth implements Command {

	private String email;
	
	public GetAuth( String email ){
		this.email = email;
	}
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		try {
			if ( new MailCommanderDAO().getAutorizzazione(email) == null) 
				throw new BusinessException(-50, "NON SI DISPONE DELL'AUTORIZZAZIONE PER OPERARE.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new BusinessException( -3500, "Errore nel recupero dati autorizzazione: " + e);
		}
		
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

}
