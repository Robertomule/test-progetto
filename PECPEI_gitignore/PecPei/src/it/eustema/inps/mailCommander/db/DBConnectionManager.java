package it.eustema.inps.mailCommander.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.vo.DBConnectionVO;
import it.eustema.inps.utility.ConfigProp;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DBConnectionManager {

	private static final Log log = LogFactory.getLog(DBConnectionManager.class);
	private static DBConnectionManager cm = null;
	private Map<String, DBConnectionVO> mapConnections = null;
	
	public static final String DB_MAIL_COMMANDER = "DB_MAIL_COMMANDER";
	
	private DBConnectionManager() throws Exception{
	
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try{
			mapConnections = new HashMap<String, DBConnectionVO>();
			// registra il driver
			// NB: non necessario chiamare il metodo Class.forName per registrare il driver se si utilizza la libreria di classi sqljdbc4.jar
			Class.forName(ConfigProp.jdbcDriver);
			
			//INIT CONNESSIONI
			DBConnectionVO vo = new DBConnectionVO();
			vo.setUrl( ConfigProp.dbMailCommanderConnectionUrl );
			vo.setUser( ConfigProp.usrMailCommander );
			vo.setPassword( ConfigProp.pwdMailCommander );
			
			mapConnections.put( DBConnectionManager.DB_MAIL_COMMANDER, vo );
			
			conn = getConnection(vo);
			pst = conn.prepareStatement( "SELECT NOME_DB, URL, [USER], [PASSWORD] FROM DBGESTIONEEMAIL" );
			rs = pst.executeQuery();
			
			
			while ( rs.next() ){
				vo = new DBConnectionVO();
				vo.setUrl( rs.getString("URL") );
				vo.setUser( rs.getString("USER") );
				vo.setPassword( rs.getString("PASSWORD") );
				
				mapConnections.put( rs.getString("NOME_DB").toUpperCase(), vo );
			}
			
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			if ( rs != null ) rs.close();
			if ( pst != null ) pst.close();
			if ( conn != null ) conn.close();
		}
		
	}	
	
	public static DBConnectionManager getInstance(){
		
		if(cm == null)
			try {
				cm = new DBConnectionManager();
			} catch (Exception e) {				
				e.printStackTrace();
				log.error("getInstance :: Errore durante il caricamento del driver per la connessione al DB: " + e.getMessage());
				//System.err.println("Errore durante il caricamento del driver per la connessione al DB: " + e.getMessage());
			}
		
		return cm;
	}
	
	
	private Connection getConnection( DBConnectionVO vo ) throws BusinessException{		
		Connection conn = null;
		try{
			if(vo!=null){
				
				conn = DriverManager.getConnection(vo.getUrl(), vo.getUser(), vo.getPassword() );
			}
			else
				throw new BusinessException(-1001, "DBConnectionManager :: Impossibile ottenere una connessione: DB non specificato");
		}
		catch(Exception e){
			log.error("Impossibile ottenere una connessione al DB: " + e.getMessage());
			
			throw new BusinessException( -1000, "Errore nella Creazione della connessione: " + e.getMessage());
		}		
//		log.info("getConnection :: connection=" + ConfigProp.dbPecPeiConnectionUrl +"--"+ConfigProp.usrPecPei +"--"+ ConfigProp.pwdPecPei);
//		log.debug("getConnection :: connection=" + conn);		
		return conn;
	}
	
	
	public Connection getConnection( String nomeDB ) throws BusinessException{
		try{
			return getConnection( mapConnections.get( nomeDB ) );
		} catch ( BusinessException be ){
			if ( nomeDB != null && !nomeDB.isEmpty() )
				throw new BusinessException( -1010, "Il DB " + nomeDB + " non � gestito.");
			else throw be;
		}
	}
	
	/*
	private DataSource lookup(String jndi){
		
		System.out.println("DBConnectionManager :: lookup");
		try{			
			Hashtable<String, String> environment = new Hashtable<String, String>();  
			environment.put(Context.INITIAL_CONTEXT_FACTORY, ConfigProp.initialContextFactory);  
			environment.put(Context.PROVIDER_URL, ConfigProp.providerUrl);
			
			Context ctx = new InitialContext(environment);			
			//Context ctx = new InitialContext();			
			
			ds_PECPEI = (DataSource) ctx.lookup(jndi);
			ctx.close();			
			System.out.println("DBConnectionManager :: eseguita lookup :: ds=" + ds_PECPEI);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return ds_PECPEI;
	}
	*/
}
