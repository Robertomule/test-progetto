package it.eustema.inps.mailCommander.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.vo.AutorizzazioneEmailVO;
import it.eustema.inps.mailCommander.vo.LogVO;
import it.eustema.inps.mailCommander.vo.QueryVO;


public class MailCommanderDAO {

	
	public QueryVO getQueryFromNomeComando( String nomeComando, String nomeDB ) throws BusinessException, SQLException{
		QueryVO out = null;
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		
		
		try {
			conn = DBConnectionManager.getInstance().getConnection( DBConnectionManager.DB_MAIL_COMMANDER );
			pst = conn.prepareStatement("SELECT COMANDO FROM Comandi_Gestione_Email where NOME_DB = ? AND NOME_COMANDO = ? ");
			pst.setString(1, nomeDB);
			pst.setString(2, nomeComando);
			rs = pst.executeQuery();
			
			if ( rs.next() ){
				out = new QueryVO();
				out.setQuery( rs.getString(1) );
				
			}
		} finally {
			if ( rs != null )
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if ( pst != null )
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if ( conn != null )
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return out;
	}
	
	
	
	public void writeLog( LogVO log ) throws BusinessException, SQLException{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnectionManager.getInstance().getConnection( DBConnectionManager.DB_MAIL_COMMANDER );
			pst = conn.prepareStatement("INSERT INTO LOG_GESTIONE_EMAIL ( idEmail, nomeComando,comando,emailMittente,responseCode,responseDesc ) VALUES (?,?,?,?,?,?)");
			
			pst.setString(1, log.getIdMail() );
			
			if ( log.getComando() != null && log.getComando().getNomeComando() != null )
				pst.setString(2, log.getComando().getNomeComando() );
			else 
				pst.setString(2, "-" );
			if ( log.getQuery() != null && log.getQuery().getQuery() != null )
				pst.setString(3, log.getQuery().getQuery() );
			else 
				pst.setString(3, "-" );
			pst.setString(4, log.getMessaggio().getMittente() );
			pst.setInt(5, log.getResponseCode() );
			pst.setString(6, log.getResponseDesc() );
			
			pst.executeUpdate();
			
		} finally {
			if ( rs != null )
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if ( pst != null )
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if ( conn != null )
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
	}
	
	
	public AutorizzazioneEmailVO getAutorizzazione( String email ) throws BusinessException, SQLException{
		AutorizzazioneEmailVO out = null;
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		
		
		try {
			conn = DBConnectionManager.getInstance().getConnection( DBConnectionManager.DB_MAIL_COMMANDER );
			pst = conn.prepareStatement("SELECT email, nome, cognome FROM AutorizzazioneEmailGestioneEmail where email = ? AND dataValidita > getDate() and flgAttivo = 1");
			pst.setString(1, email);
			
			rs = pst.executeQuery();
			
			if ( rs.next() ){
				out = new AutorizzazioneEmailVO();
				out.setEmail ( rs.getString(1) );
				out.setNome ( rs.getString(2) );
				out.setCognome ( rs.getString(3) );
				
			}
		} finally {
			if ( rs != null )
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if ( pst != null )
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if ( conn != null )
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return out;
	}
}
