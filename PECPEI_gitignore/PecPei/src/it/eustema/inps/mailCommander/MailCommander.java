package it.eustema.inps.mailCommander;


import org.apache.log4j.Logger;

import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.mailCommander.command.ExcelBuilder;
import it.eustema.inps.mailCommander.command.GetAuth;
import it.eustema.inps.mailCommander.command.MessageExtractor;
import it.eustema.inps.mailCommander.command.QueryExaminator;
import it.eustema.inps.mailCommander.command.QueryExecutor;
import it.eustema.inps.mailCommander.command.QueryExtractor;
import it.eustema.inps.mailCommander.command.QueryInjector;
import it.eustema.inps.mailCommander.command.WriteLog;
import it.eustema.inps.mailCommander.db.DBConnectionManager;
import it.eustema.inps.mailCommander.vo.CommandVO;
import it.eustema.inps.mailCommander.vo.LogVO;
import it.eustema.inps.mailCommander.vo.MessageVO;
import it.eustema.inps.mailCommander.vo.QueryVO;
import it.eustema.inps.mailCommander.vo.ReportVO;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.MailScanner;
import it.eustema.inps.utility.MailScannerPec;
import it.eustema.inps.utility.SendMailCommandMC;
import it.eustema.inps.utility.logging.LoggingConfigurator;


public class MailCommander {
	
	private static Logger logger = Logger.getLogger(MailCommander.class);
	private static boolean LOOP = false;
	private static int sleepTime = 60000;
	
	
	
	public void elab(){
		
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		logger.info("START MAIL COMMANDER");
		MessageVO message = null;
		CommandVO command = null;
		QueryVO query = null;
		ReportVO report = null;
		byte[] excel  = null;
		LogVO log = null;
		
		MailScanner scanner = null;
		if ( "SI".equalsIgnoreCase( ConfigProp.useAccountPec )){
			scanner = new MailScannerPec();
		} else {
			scanner = new MailScanner();
		}
		
		try {
			logger.info("Apertura casella MAIL");
			scanner.openConnection();
		} catch (BusinessException e2) {
			logger.error(" Errore nell'apertura della connessione con la casella di posta");
		}
		
		while ( scanner.nextMessage() ){
			//Recupero messaggio dalla casella
			try {
				message = scanner.getMessage();
			} catch (BusinessException e2) {
				message = null;
				logger.error(" Errore nel recupero di un messaggio");
			}
			
			if ( message == null )
				continue;
			
			try {
				
				//Estrazione del comando contenuto nel messaggio
				executor.setCommand( new MessageExtractor( message ) );
				command = (CommandVO) executor.executeCommand();
				logger.info("Trovato nuovo messaggio da elaborare");
				
				scanner.removeMessage();
				
				//Verifica autorizzazione
				executor.setCommand( new GetAuth( message.getMittente() ) );
				executor.executeCommand();
				logger.info("Mittente autorizzato");
				
				//Recupero della Query associata al comando
				executor.setCommand( new QueryExtractor( command ) );
				query = (QueryVO) executor.executeCommand();
				
				//Analisi della query per estrarre i parametri di input
				executor.setCommand( new QueryExaminator( query ) );
				executor.executeCommand();
				
				//Valorizzazione dei parametri della query con i valori passati in input
				//e contenuti nel messaggio
				executor.setCommand( new QueryInjector(query, command.getParametri() ) );
				executor.executeCommand();
				
				//Esecuzione della query
				executor.setCommand( new QueryExecutor( query, command.getNomeDB() ) );
				report = (ReportVO) executor.executeCommand();
				logger.info("Comando eseguito");
				
				//Scrittura del log d'esecuzione del comando
				log = new LogVO();
				log.setComando( command );
				log.setMessaggio( message );
				log.setQuery( query );
				log.setResponseCode(0);
				log.setResponseDesc("OK");
				
				executor.setCommand( new WriteLog( log ) );
				try{
					executor.executeCommand();
				} catch ( BusinessException be ){}
				
				
				//Generazione Report risultato esecuzione
				executor.setCommand( new ExcelBuilder( report, query.getQuery() ) );
				excel =  (byte[]) executor.executeCommand();
				
//				FileOutputStream os = new FileOutputStream("c:\\test.xls"); 
//				os.write(excel);
//				os.flush();
//				os.close();
				
				//Invio mail in risposta
				executor.setCommand( new SendMailCommandMC( message, command.getNomeComando(), excel ) );
				executor.executeCommand();
				
				
			} catch (BusinessException e) {
				//Viene notificato al mittente e scritto sui log
				//Solo se � un errore (errorCode < 0). 
				//Altrimenti � normale esecuzione e non viene notificato nulla
				if ( e.getErrorCode() < 0 ){
					
					//Scrittura del log d'esecuzione del comando
					log = new LogVO();
					log.setComando( command );
					log.setMessaggio( message );
					log.setQuery( query );
					log.setResponseCode( e.getErrorCode() );
					log.setResponseDesc( e.getMessage() );
					
					executor.setCommand( new WriteLog( log ) );
					try{
						executor.executeCommand();
					} catch ( BusinessException be ){}
					
					//Invio mail in risposta contenente l'errore verificatosi
					executor.setCommand( new SendMailCommandMC( e, message ) );
					try {
						executor.executeCommand();
					} catch (BusinessException e1) {
						
					}
				}
			} catch ( Exception e ){
				//Scrittura del log d'esecuzione del comando
				String messageTXT = "Errore generico nell'elaborazione: " + e.getMessage();
				log = new LogVO();
				log.setComando( command );
				log.setMessaggio( message );
				log.setQuery( query );
				log.setResponseCode( -8888 );
				log.setResponseDesc( messageTXT );
				
				executor.setCommand( new WriteLog( log ) );
				try{
					executor.executeCommand();
				} catch ( BusinessException be ){}
				
				//Invio mail in risposta contenente l'errore verificatosi
				executor.setCommand( new SendMailCommandMC( new BusinessException(-8888, messageTXT), message ) );
				try {
					executor.executeCommand();
				} catch (BusinessException e1) {
					
				}
			}
		}
		
		
		try {
			logger.info("Chiusura casella MAIL");
			scanner.closeConnection();
		} catch (BusinessException e) {
			logger.error(" Errore nella chiusura della connessione con la casella di posta");
		}
		
		logger.info("STOP MAIL COMMANDER");
	}
	
}
