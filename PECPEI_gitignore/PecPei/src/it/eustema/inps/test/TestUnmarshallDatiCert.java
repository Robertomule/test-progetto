package it.eustema.inps.test;

import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.inps.agentPec.verificaPec.dto.NotificaErroriDTO;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

public class TestUnmarshallDatiCert {

	/**
	 * @param args
	 * @throws JAXBException 
	 * @throws BusinessException 
	 */
	private static Logger log = Logger.getLogger(TestUnmarshallDatiCert.class);
	
	private static NotifyMailBusinessException exception;
	private static MessaggioDTO messaggio;
	private final static String INS_NOTIFICA_ERRORE_VP = "INSERT INTO NotificaErrori (Codice, Messaggio, MessageID)" + " VALUES(?,?,?)";
	private final static String INS_NOTIFICA_ERRORE_PP = "INSERT INTO NotificaErrori (Codice, Messaggio, AccountPEC, CodiceAOO, MessageID)" + " VALUES(?,?,?,?,?)";
	private final static String INS_NOTIFICA_ERRORE_HM = "INSERT INTO NotificaErrori (Codice, Messaggio, CodiceAOO, MessageID)" + " VALUES(?,?,?,?)";
	
	public static void main(String[] args) throws Exception 
	{
		try
		{
			it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert datiCertificati = new it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert();
			
			File file = new File("C:\\Users\\Pino\\Desktop\\daticert.xml");
			
			byte[] buffer = new byte[(int) file.length()];
			
		    InputStream ios = null;
		    
		    try {
		        ios = new FileInputStream(file);
		        if (ios.read(buffer) == -1) 
		        {
		            throw new IOException("Il file risulta vuoto!");
		        }
		    } 
		    finally 
		    {
		        try 
		        {
		            if (ios != null)
		                ios.close();
		        } 
		        catch (IOException e) 
		        {
		        	System.out.println(e.getMessage());
		        }
		    }
			
			String dati = new String(buffer);
			
			JAXBContext context = JAXBContext.newInstance(it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert.class);
			
			//JAXBContext context = null;

			datiCertificati = (it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert) context.createUnmarshaller().unmarshal(new ByteArrayInputStream(dati.replaceAll("<!DOCTYPE Segnatura SYSTEM \"Segnatura.dtd\">", "").replaceAll("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "").getBytes()));
			
			System.out.println(datiCertificati.getDati().getErroreEsteso());
			
			throw new Exception("Exception Test: simulazione SendMailCommand");
		}
		catch(Exception exp)
		{
			//Test PECPEI 
			System.setProperty("NotificaErrori.DbName", Constants.DB_PECPEI);
			
			System.out.println(System.getProperty("NotificaErrori.DbName"));
			
			messaggio = new MessaggioDTO();
			
			messaggio.setAccountPec("AcTest");
			
			messaggio.setMessageId("IdTest");
			
			messaggio.setCodiceAOO("AOOCod");
			
			exception = new NotifyMailBusinessException( -1981, "Errore nel metodo main TestUnmarshallDatiCert: " + exp.getMessage() ) ;

			InserisciNotificaErrori();
			
			//Test VERIFICAPEC 
			System.setProperty("NotificaErrori.DbName", Constants.DB_VERIFICAPEC);
			
			System.out.println(System.getProperty("NotificaErrori.DbName"));
			
			messaggio = new MessaggioDTO();
			
			messaggio.setAccountPec("AcTest");
			
			messaggio.setMessageId("IdTest");
			
			messaggio.setCodiceAOO("AOOCod");
			
			exception = new NotifyMailBusinessException( -1981, "Errore nel metodo main TestUnmarshallDatiCert: " + exp.getMessage() ) ;

			InserisciNotificaErrori();
			
			//Test HERMES 
			System.setProperty("NotificaErrori.DbName", Constants.DB_HERMES);
			
			System.out.println(System.getProperty("NotificaErrori.DbName"));
			
			messaggio = new MessaggioDTO();
			
			messaggio.setAccountPec("AcTest");
			
			messaggio.setMessageId("IdTest");
			
			messaggio.setCodiceAOO("AOOCod");
			
			exception = new NotifyMailBusinessException( -1981, "Errore nel metodo main TestUnmarshallDatiCert: " + exp.getMessage() ) ;

			//InserisciNotificaErrori(); Authentication SQL Server 2012 expired: user: HERMES - pwd: jar3-hK.m2

			
			System.setProperty("NotificaErrori.DbName", "");
				
			 
			 //throw new JAXBException(exp);
		}

	}
	
	private static void InserisciNotificaErrori() throws DAOException, SQLException
	{
		String dbName = System.getProperty("NotificaErrori.DbName");
		
		Object notifica = null;
		
		//Object notificaDAO = null;
		
		long resultID = -1;
		
		if(dbName != null)					
		{
			if(dbName == Constants.DB_PECPEI)
			{
				notifica = new it.eustema.inps.agentPecPei.dto.NotificaErroriDTO();
				
				((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setCodice(exception.getErrorCode());
				
				((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setMessaggio(exception.getMessage());
				
				if(messaggio != null)
				{
					((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setAccountPEC(messaggio.getAccountPec());
					((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setCodiceAOO(messaggio.getCodiceAOO());
					((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setMessageID(messaggio.getMessageId());
				}
				
				//notificaDAO = new it.eustema.inps.agentPecPei.dao.NotificaErroriDAO();
				
				//resultID = ((it.eustema.inps.agentPecPei.dao.NotificaErroriDAO) notificaDAO).insertNotifica((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica);
				
				resultID = insertNotificaPECPEI((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica);
				
			}else if(dbName == Constants.DB_VERIFICAPEC)
			{
				notifica = new it.inps.agentPec.verificaPec.dto.NotificaErroriDTO();
				
				((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica).setCodice(exception.getErrorCode());
				
				((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica).setMessaggio(exception.getMessage());
				
				if(messaggio != null)
				{
					((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica).setMessageID(messaggio.getMessageId());
				}
				
				//notificaDAO = new it.inps.agentPec.verificaPec.dao.NotificaErroriDAO();			
				
				//resultID = ((it.inps.agentPec.verificaPec.dao.NotificaErroriDAO) notificaDAO).insertNotifica((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica);
				
				resultID = insertNotificaVP((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica);

			}
			else if(dbName == Constants.DB_HERMES)
			{
				notifica = new it.eustema.inps.hermes.dto.NotificaErroriDTO();
				
				((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setCodice(exception.getErrorCode());
				
				((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setMessaggio(exception.getMessage());
				
				if(messaggio != null)
				{
					((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setCodiceAOO(messaggio.getCodiceAOO());
					((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setMessageID(messaggio.getMessageId());
				}
				
				//notificaDAO = new it.eustema.inps.hermes.dao.NotificaErroriDAO();
				
				//resultID = ((it.eustema.inps.hermes.dao.NotificaErroriDAO) notificaDAO).insertNotifica((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica);
				
				resultID = insertNotificaHM((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica);
			}
			else
				log.info("Notifica errori fallita: valore proprietÓ NotificaErrori.DbName non riconosciuto!");
		}
		else
			log.info("Notifica errori fallita: proprietÓ NotificaErrori.DbName non trovata!");
		
		if(resultID < 0)
			log.info("Notifica errori fallita: inserimento in " + dbName + ".NotificaErrori fallito! Codice Errore: " + exception.getErrorCode() + "; Messaggio: " + exception.getMessage() + "");

	}
	
	private static long insertNotificaVP(NotificaErroriDTO erroreDTO) throws DAOException {			
		
		//System.out.println("erroreDTO :: insertNotifica");
		
		long idNuovaNotifica = -1;

		if(erroreDTO != null){
			Connection connVerificaPec = null;
			try {
				connVerificaPec = DriverManager.getConnection("jdbc:sqlserver://SQLINPSSVIL15.INPS:2434;databaseName=VerificaPEC", "VerificaPec", "fec.33sHp-9");    
				connVerificaPec.setAutoCommit(false);
      		
				idNuovaNotifica = doInsertNotificaVP(connVerificaPec, erroreDTO);									

				if(idNuovaNotifica != -1){
					connVerificaPec.commit();
					//System.out.println("erroreDTO :: insertNotifica :: commit eseguito");
				}
			}
			catch(DAOException ex){
				ex.printStackTrace();
				//System.err.println("erroreDTO :: insertNotifica :: errore " + ex.getMessage());				
				if(connVerificaPec!=null)
					try {
						connVerificaPec.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}	
				throw ex;
			}
			catch(Exception eh){
				eh.printStackTrace();
				//System.err.println("erroreDTO :: insertNotifica :: errore " + eh.getMessage());				
				if(connVerificaPec!=null)
					try {
						connVerificaPec.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}				
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB");
			}			
			finally{
				try {
					if(connVerificaPec != null)
						connVerificaPec.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}		
		return idNuovaNotifica;
	
	} //insertNotifica
	
	private static long doInsertNotificaVP(Connection conn, NotificaErroriDTO notificaDTO) throws DAOException {
		
		long idNotifica = -1;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try{
				pstmt = conn.prepareStatement(INS_NOTIFICA_ERRORE_VP, Statement.RETURN_GENERATED_KEYS);			
				pstmt.setInt(1, notificaDTO.getCodice());
				pstmt.setString(2, notificaDTO.getMessaggio());
				
				if(notificaDTO.getMessageID() != null)
					pstmt.setString(3, notificaDTO.getMessageID());
				else
					pstmt.setNull(3, java.sql.Types.NVARCHAR);
				
				pstmt.executeUpdate();
				generatedKeys = pstmt.getGeneratedKeys();
				if (generatedKeys.next()) {
					idNotifica = generatedKeys.getLong(1);
				} else {
					throw new SQLException("Impossibile ottenere la chiava generata per la notifica");
				}			
				//System.out.println("Inserimento in NotificaErrori effettuato correttamente: idNotifica:" + idNotifica);
				pstmt.close();
				if(generatedKeys != null) 
					try {
						generatedKeys.close();
					} catch (Exception e){}
			}
			catch(SQLException ex){
				System.err.println("Errore durante inserimento in NotificaErrori: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB" );
			}
			catch(Exception ex){
				System.err.println("Errore durante inserimento in NotificaErrori: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException(ex.getMessage());
			}
			
			return idNotifica;
		}
	
	private static long insertNotificaPECPEI(it.eustema.inps.agentPecPei.dto.NotificaErroriDTO erroreDTO) throws DAOException {			
		
		//System.out.println("erroreDTO :: insertNotifica");
		
		long idNuovaNotifica = -1;

		if(erroreDTO != null){
			Connection connPECPEI = null;
			try {
				connPECPEI = DriverManager.getConnection("jdbc:sqlserver://SQLINPSSVIL06:2059;databaseName=PECPEI", "PECPEI", "jar3-hK.m2");    
				connPECPEI.setAutoCommit(false);
      		
				idNuovaNotifica = doInsertNotificaPECPEI(connPECPEI, erroreDTO);									

				if(idNuovaNotifica != -1){
					connPECPEI.commit();
					//System.out.println("erroreDTO :: insertNotifica :: commit eseguito");
				}
			}
			catch(DAOException ex){
				ex.printStackTrace();
				//System.err.println("erroreDTO :: insertNotifica :: errore " + ex.getMessage());				
				if(connPECPEI!=null)
					try {
						connPECPEI.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}	
				throw ex;
			}
			catch(Exception eh){
				eh.printStackTrace();
				//System.err.println("erroreDTO :: insertNotifica :: errore " + eh.getMessage());				
				if(connPECPEI!=null)
					try {
						connPECPEI.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}				
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB");
			}			
			finally{
				try {
					if(connPECPEI != null)
						connPECPEI.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}		
		return idNuovaNotifica;
	
	} //insertNotifica
	
	private static long doInsertNotificaPECPEI(Connection conn, it.eustema.inps.agentPecPei.dto.NotificaErroriDTO notificaDTO) throws DAOException {
		
		long idNotifica = -1;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try{
				pstmt = conn.prepareStatement(INS_NOTIFICA_ERRORE_PP, Statement.RETURN_GENERATED_KEYS);			
				pstmt.setInt(1, notificaDTO.getCodice());
				pstmt.setString(2, notificaDTO.getMessaggio());
	
				if(notificaDTO.getAccountPEC() != null)
					pstmt.setString(3, notificaDTO.getAccountPEC());
				else
					pstmt.setNull(3, java.sql.Types.NVARCHAR);
				
				if(notificaDTO.getCodiceAOO() != null)
					pstmt.setString(4, notificaDTO.getCodiceAOO());
				else
					pstmt.setNull(4, java.sql.Types.NVARCHAR);
				
				if(notificaDTO.getMessageID() != null)
					pstmt.setString(5, notificaDTO.getMessageID());
				else
					pstmt.setNull(5, java.sql.Types.NVARCHAR);
				
				pstmt.executeUpdate();
				generatedKeys = pstmt.getGeneratedKeys();
				if (generatedKeys.next()) {
					idNotifica = generatedKeys.getLong(1);
				} else {
					throw new SQLException("Impossibile ottenere la chiava generata per la notifica");
				}			
				//System.out.println("Inserimento in NotificaErrori effettuato correttamente: idNotifica:" + idNotifica);
				pstmt.close();
				if(generatedKeys != null) 
					try {
						generatedKeys.close();
					} catch (Exception e){}
			}
			catch(SQLException ex){
				System.err.println("Errore durante inserimento in NotificaErrori: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB" );
			}
			catch(Exception ex){
				System.err.println("Errore durante inserimento in NotificaErrori: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException(ex.getMessage());
			}
			
			return idNotifica;
		}
	
	private static long insertNotificaHM(it.eustema.inps.hermes.dto.NotificaErroriDTO erroreDTO) throws DAOException {			
		
		//System.out.println("erroreDTO :: insertNotifica");
		
		long idNuovaNotifica = -1;

		if(erroreDTO != null){
			Connection connHermes = null;
			try {
				connHermes = DriverManager.getConnection("jdbc:sqlserver://SQLINPSSVIL06:2059;databaseName=Hermes", "HERMES", "jar3-hK.m2");   
				connHermes.setAutoCommit(false);
      		
				idNuovaNotifica = doInsertNotificaHM(connHermes, erroreDTO);									

				if(idNuovaNotifica != -1){
					connHermes.commit();
					//System.out.println("erroreDTO :: insertNotifica :: commit eseguito");
				}
			}
			catch(DAOException ex){
				ex.printStackTrace();
				//System.err.println("erroreDTO :: insertNotifica :: errore " + ex.getMessage());				
				if(connHermes!=null)
					try {
						connHermes.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}	
				throw ex;
			}
			catch(Exception eh){
				eh.printStackTrace();
				//System.err.println("erroreDTO :: insertNotifica :: errore " + eh.getMessage());				
				if(connHermes!=null)
					try {
						connHermes.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}				
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB");
			}			
			finally{
				try {
					if(connHermes != null)
						connHermes.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}		
		return idNuovaNotifica;
	
	} //insertNotifica
	
	private static long doInsertNotificaHM(Connection conn, it.eustema.inps.hermes.dto.NotificaErroriDTO notificaDTO) throws DAOException {
		
		long idNotifica = -1;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try{
			pstmt = conn.prepareStatement(INS_NOTIFICA_ERRORE_HM, Statement.RETURN_GENERATED_KEYS);			
			pstmt.setInt(1, notificaDTO.getCodice());
			pstmt.setString(2, notificaDTO.getMessaggio());
			
			if(notificaDTO.getCodiceAOO() != null)
				pstmt.setString(3, notificaDTO.getCodiceAOO());
			else
				pstmt.setNull(3, java.sql.Types.NVARCHAR);
			
			if(notificaDTO.getMessageID() != null)
				pstmt.setString(4, notificaDTO.getMessageID());
			else
				pstmt.setNull(4, java.sql.Types.NVARCHAR);
				
				pstmt.executeUpdate();
				generatedKeys = pstmt.getGeneratedKeys();
				if (generatedKeys.next()) {
					idNotifica = generatedKeys.getLong(1);
				} else {
					throw new SQLException("Impossibile ottenere la chiava generata per la notifica");
				}			
				//System.out.println("Inserimento in NotificaErrori effettuato correttamente: idNotifica:" + idNotifica);
				pstmt.close();
				if(generatedKeys != null) 
					try {
						generatedKeys.close();
					} catch (Exception e){}
			}
			catch(SQLException ex){
				System.err.println("Errore durante inserimento in NotificaErrori: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB" );
			}
			catch(Exception ex){
				System.err.println("Errore durante inserimento in NotificaErrori: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException(ex.getMessage());
			}
			
			return idNotifica;
		}

}
