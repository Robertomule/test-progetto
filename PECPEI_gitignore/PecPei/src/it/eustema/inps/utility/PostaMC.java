package it.eustema.inps.utility;

import it.eustema.inps.mailCommander.vo.AllegatoVO;

import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import java.util.*;
import javax.activation.*;


import java.io.*;

public class PostaMC {
	
	private Session getMailSession(String serverPosta,String uid,String password){
		Properties props = new Properties();
		props.put("mail.smtp.host", serverPosta);
		props.put("mail.smtp.username", uid);
		props.put("mail.smtp.password", password);	
		
		
		return  Session.getDefaultInstance(props,null);
	}
	
	private void validateParams(String serverPosta, String uid, String password, Collection destinatari, String oggetto,String testoEmail)
		throws IOException{
		
		if ( (serverPosta == null || serverPosta.trim().equals("")) ||
				(uid == null || uid.trim().equals("")) ||
				(password == null || password.trim().equals(""))
			)
			throw new IOException("I parametri di connessione al server di posta non sono corretti.Impossibile inviare il messaggio");

		if (destinatari == null || destinatari.size() == 0)
			throw new IOException("Il contenitore dei destinatari � vuoto.Impossibile inviare il messaggio");

		if (oggetto == null || oggetto.trim().equals(""))
			throw new IOException("L'oggetto del messaggio non � valorizzato.Impossibile inviare il messaggio");

		if (testoEmail == null || testoEmail.trim().equals(""))
			throw new IOException("Il testo del messaggio non � valorizzato.Impossibile inviare il messaggio");
		
	}

	public void inviaEmail(String serverPosta, String uid, String password,
								String mittente, Vector<String> destinatari, Vector<String> destinatariCC,
								String oggetto, String testoEmail, List<AllegatoVO> allegati, boolean isHtml) throws Exception {
			try {
				validateParams(serverPosta, uid, password, destinatari, oggetto, testoEmail);
//				if ((serverPosta == null || serverPosta.trim().equals("")) ||
//					(uid == null || uid.trim().equals("")) ||
//					(password == null || password.trim().equals("")))
//					throw new IOException("I parametri di connessione al server di posta non sono corretti.Impossibile inviare il messaggio");
//				
//				if (destinatari == null || destinatari.size() == 0)
//					throw new IOException("Il contenitore dei destinatari � vuoto.Impossibile inviare il messaggio");
//				
//				if (oggetto == null || oggetto.trim().equals(""))
//					throw new IOException("L'oggetto del messaggio non � valorizzato.Impossibile inviare il messaggio");
//				
//				if (testoEmail == null || testoEmail.trim().equals(""))
//					throw new IOException("Il testo del messaggio non � valorizzato.Impossibile inviare il messaggio");
					
				// Creazione di una mail session
				Session sessionMail = getMailSession(serverPosta,uid,password);
//				Properties props = new Properties();
//				props.put("mail.smtp.host", serverPosta);
//				props.put("mail.smtp.username", uid);
//				props.put("mail.smtp.password", password);
//				
//				Session sessionMail =  Session.getDefaultInstance(props,null);
				
				// Creazione del messaggio da inviare
				MimeMessage message = new MimeMessage(sessionMail);

				// Aggiunta degli indirizzi del mittente 
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				InternetAddress fromAddress = new InternetAddress(mittente);
				//InternetAddress fromAddress = new InternetAddress("noReply.pecpei@inps.it");
				message.setFrom(fromAddress);
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
				
				
				//Aggiunta degli indirizzi dei destinatari
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++						
				InternetAddress[] toAddress = new InternetAddress[destinatari.size()];
				Enumeration<String> eDest = destinatari.elements();
				int cont = 0;
				while (eDest.hasMoreElements())
				{
					String destX = (String)eDest.nextElement();
					InternetAddress to = new InternetAddress(destX);		
					toAddress[cont] = to;	
					cont++;
				}		
				message.setRecipients(Message.RecipientType.TO, toAddress);				
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				

				//Aggiunta degli indirizzi dei destinatari in CC
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			  
				if (destinatariCC != null && destinatariCC.size() > 0)
				{	
					InternetAddress[] toAddressCC = new InternetAddress[destinatariCC.size()];
					Enumeration<String> eDestCC = destinatariCC.elements();
					int contCC = 0;
					while (eDestCC.hasMoreElements())
					{
						String destX = (String)eDestCC.nextElement();
						InternetAddress to = new InternetAddress(destX);		
						toAddressCC[contCC] = to;	
						contCC++;
					}		
						message.setRecipients(Message.RecipientType.CC, toAddressCC);	
				}
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
						  
				
				message.setSubject(oggetto);    
				
				//create the Multipart and add its parts to it
				Multipart mp = new MimeMultipart();
				
				// create and fill the first message part
				MimeBodyPart mbp1 = new MimeBodyPart();
				
				if(isHtml){
					
					mbp1.setContent(testoEmail, "text/html");
				}else{
					
					mbp1.setContent(testoEmail, "text/plain; charset=us-ascii");
				}
				
				
				if (allegati != null && allegati.size() > 0)
				{					
				 
				 
				 for ( AllegatoVO f: allegati )
				 {		
						
					MimeBodyPart mbp2 = new MimeBodyPart();
					    
					ByteArrayDataSource fds = new ByteArrayDataSource(f.getData() , f.getContentType());
					mbp2.setDataHandler(new DataHandler(fds));
					mbp2.setFileName(f.getFileName());
													
					mp.addBodyPart(mbp2);				
				 }
				}
					
				mp.addBodyPart(mbp1);					
				message.setContent(mp);				
				Transport.send(message);
				try{
					sessionMail.getStore().getFolder("Posta inviata").appendMessages( new Message[]{ message } );	
				} catch (Exception e){
					
				}
				
				
				
				
			}
			catch (Exception e){
				throw e;  
			}
		}

		
		public void inviaEmailPosta(String serverPosta, String uid, String password,
									String mittente, Vector<String> destinatari, Vector<String> destinatariCC,
									String oggetto, String testoEmail, boolean isHtml) throws Exception {

			try {
				validateParams(serverPosta, uid, password, destinatari, oggetto, testoEmail);
//				if ((serverPosta == null || serverPosta.trim().equals("")) ||
//						(uid == null || uid.trim().equals("")) ||
//						(password == null || password.trim().equals("")))
//					throw new IOException("I parametri di connessione al server di posta non sono corretti.Impossibile inviare il messaggio");
//				
//				if (destinatari == null || destinatari.size() == 0)
//					throw new IOException("Il contenitore dei destinatari � vuoto.Impossibile inviare il messaggio");
//				
//				if (oggetto == null || oggetto.trim().equals(""))
//					throw new IOException("L'oggetto del messaggio non � valorizzato.Impossibile inviare il messaggio");
//				
//				if (testoEmail == null || testoEmail.trim().equals(""))
//					throw new IOException("Il testo del messaggio non � valorizzato.Impossibile inviare il messaggio");
				
				// Creazione di una mail session
				Session sessionMail = getMailSession(serverPosta,uid,password);				
//				Properties props = new Properties();
//				props.put("mail.smtp.host", serverPosta);
//				props.put("mail.smtp.username", uid);
//				props.put("mail.smtp.password", password);
//				
//				Session sessionMail =  Session.getDefaultInstance(props,null);
				
				// Creazione del messaggio da inviare
				MimeMessage message = new MimeMessage(sessionMail);
				
				// Aggiunta degli indirizzi del mittente 
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				InternetAddress fromAddress = new InternetAddress(mittente);
				//InternetAddress fromAddress = new InternetAddress("noReply.pecpei@inps.it");
				message.setFrom(fromAddress);
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
				
				
				//Aggiunta degli indirizzi dei destinatari
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++						
				InternetAddress[] toAddress = new InternetAddress[destinatari.size()];
				Enumeration<String> eDest = destinatari.elements();
				int cont = 0;
				while (eDest.hasMoreElements())
				{
				String destX = (String)eDest.nextElement();
				InternetAddress to = new InternetAddress(destX);		
				toAddress[cont] = to;	
				cont++;
				}		
				message.setRecipients(Message.RecipientType.TO, toAddress);				
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				
				
				//Aggiunta degli indirizzi dei destinatari in CC
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			  
				if (destinatariCC != null && destinatariCC.size() > 0)
				{	
				InternetAddress[] toAddressCC = new InternetAddress[destinatariCC.size()];
				Enumeration<String> eDestCC = destinatariCC.elements();
				int contCC = 0;
				while (eDestCC.hasMoreElements())
				{
					String destX = (String)eDestCC.nextElement();
					InternetAddress to = new InternetAddress(destX);		
					toAddressCC[contCC] = to;	
					contCC++;
				}		
					message.setRecipients(Message.RecipientType.CC, toAddressCC);	
				}
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
					  
				
				message.setSubject(oggetto);    
				
				//create the Multipart and add its parts to it
				Multipart mp = new MimeMultipart();
				
				// create and fill the first message part
				MimeBodyPart mbp1 = new MimeBodyPart();
				
				if(isHtml){
					System.out.println(isHtml);
					mbp1.setContent(testoEmail, "text/html");
				}else{
					System.out.println(isHtml);
					mbp1.setContent(testoEmail, "text/plain; charset=us-ascii");
				}
				
				mp.addBodyPart(mbp1);	
				message.setContent(mp);				
				Transport.send(message);
				
			}
			catch (Exception e){
				throw e;  
			}
		}

		/*
		 * �  progettopa@pec.it
		 * �  sandro.moretti@progettopa.it
		   �  gianluca.solda@wizardsgroup.it
		   �  sandro.mo@libero.it
		   �  giuseppe.fucci@inps.it
		   �  m.carpentieri@eustema.it
		   �  utente04@postacert.inps.gov.it
		   �  utente03@postacert.inps.gov.it
		 * */
		
		public Vector<String> caricaDestinatariTest(){
			Vector<String> destinatariTest = new Vector<String>(6);
			destinatariTest.add("utente04@postacert.inps.gov.it");
			destinatariTest.add("utente03@postacert.inps.gov.it");
			destinatariTest.add("utente01@postacert.inps.gov.it");
			destinatariTest.add("utente02@postacert.inps.gov.it");
			destinatariTest.add("m.carpentieri@eustema.it");
			destinatariTest.add("giuseppe.fucci@inps.it");
			destinatariTest.add("sandro.mo@libero.it");
			destinatariTest.add("gianluca.solda@wizardsgroup.it");
			destinatariTest.add("sandro.moretti@progettopa.it");
			destinatariTest.add("sandro.morelli@progettopa.it");
			destinatariTest.add("progettopa@pec.it");
			destinatariTest.add("mcarpentieri@gmail.com");
			destinatariTest.add("sandro.moretti-2881@postacertificata.gov.it");
			return destinatariTest;
		}
	}
