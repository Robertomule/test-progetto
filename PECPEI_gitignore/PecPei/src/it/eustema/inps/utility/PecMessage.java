package it.eustema.inps.utility;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class PecMessage extends MimeMessage {
    
	private String messageId;
    private String XRiferimentoMessageID;
    private String tipoRicevuta;
	
	public String getMessageId() {
		return messageId;
	}

	public String getXRiferimentoMessageID() {
		return XRiferimentoMessageID;
	}

	public void setXRiferimentoMessageID(String riferimentoMessageID) {
		XRiferimentoMessageID = riferimentoMessageID;
	}

	public String getTipoRicevuta() {
		return tipoRicevuta;
	}

	public void setTipoRicevuta(String tipoRicevuta) {
		this.tipoRicevuta = tipoRicevuta;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public PecMessage(Session session) {
		super(session);
		// TODO Stub di costruttore generato automaticamente
	}

	public void updateHeaders() throws MessagingException {
	super.updateHeaders();
	setHeader(ConfigProp.invioPostaMessageID, messageId);
//	setHeader(ConfigProp.invioPostaXRiferimentoMessageID, XRiferimentoMessageID);
	setHeader(ConfigProp.invioPostaTipoRicevuta, tipoRicevuta);
    }
}
