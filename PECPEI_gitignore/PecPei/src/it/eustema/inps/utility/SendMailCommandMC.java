package it.eustema.inps.utility;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.vo.AllegatoVO;
import it.eustema.inps.mailCommander.vo.MessageVO;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import org.apache.log4j.Logger;


public class SendMailCommandMC implements Command {
	private static Logger log = Logger.getLogger(SendMailCommandMC.class);
	private String nomeComando;
	private MessageVO message;
	private byte[] excel;
	
	private BusinessException be;
	
	public SendMailCommandMC( MessageVO message, String nomeComando, byte[] excel ){
		this.message = message;
		this.nomeComando = nomeComando;
		this.excel = excel;
	}
	
	public SendMailCommandMC( BusinessException be, MessageVO message ){
		this.be = be;
		this.message = message;
	}
	
	@Override
	public Object execute() throws BusinessException {
		
			
			log.debug("InvioMail");
			if ( be != null ){
				try{
					Vector<String> destinatari = new Vector<String>();
					destinatari.add( message.getMittente() );
					
					PostaMC posta = new PostaMC();
					List<AllegatoVO> allegati = new ArrayList<AllegatoVO>();
					
					
					posta.inviaEmail(ConfigProp.serverPostaSMTP,
							ConfigProp.uidPostaMC,
							ConfigProp.pwdPostaMC,
							ConfigProp.uidPostaMC+"@inps.it",
									destinatari,
									null,
									"Re: " + message.getSubject(),
									"Attenzione!!! Non � stato possibile eseguire il comando: \n\nCodice Errore: " + be.getErrorCode() + "\n" + be.getMessage() + " \n\n\n\n---------------------- Messaggio originale -------------------------\n\n" + message.getMessage() ,
									allegati,
									false);
				}
				catch(Exception e){}
			} else {
			
				try{
					Vector<String> destinatari = new Vector<String>();
					destinatari.add( message.getMittente() );
					
					PostaMC posta = new PostaMC();
					List<AllegatoVO> allegati = new ArrayList<AllegatoVO>();
					
					if ( excel != null){
						if ( excel.length < ConfigProp.maxSizeReportExcel ){
							AllegatoVO allegato = new AllegatoVO();
							allegato.setData( excel );
							SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy hh.mm  ");
							allegato.setFileName( sdf.format( new Date() ) + nomeComando +".xlsx");
							allegato.setContentType( "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" );
							
							allegati.add( allegato );
						}
					}
					
					String outMessage = null;
					if ( excel != null && allegati.isEmpty() ){
						outMessage = "Report non allegato perch� supera le dimensioni massime consentite di " + ConfigProp.maxSizeReportExcel + " byte. \n\n\n\n---------------------- Messaggio originale -------------------------\n\n" + message.getMessage();
					} else {
						outMessage = "In allegato il report. \n\n\n\n---------------------- Messaggio originale -------------------------\n\n" + message.getMessage();
					}
					
					posta.inviaEmail(ConfigProp.serverPostaSMTP,
									ConfigProp.uidPosta,
									ConfigProp.pwdPosta,
									ConfigProp.uidPosta+"@inps.it",
									destinatari,
									null,
									"Re: " + message.getSubject(),
									outMessage,
									allegati,
									false);
				}
				catch(Exception e){}
			}
			
			return null;
		} 
	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "SendMailCommand";
	}

}
