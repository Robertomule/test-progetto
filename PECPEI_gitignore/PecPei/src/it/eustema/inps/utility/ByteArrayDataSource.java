package it.eustema.inps.utility;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.*;

import org.apache.commons.io.output.ByteArrayOutputStream;

	public class ByteArrayDataSource implements DataSource {  
       
		private String name;  
        private String contentType;  
        private ByteArrayOutputStream baos;  
          
        ByteArrayDataSource(String name, String contentType, InputStream inputStream, int fileSize) throws IOException {  
        	this.name = name;  
        	this.contentType = contentType;  
             
        	baos = new ByteArrayOutputStream();  
             
        	int read;  
        	byte[] buff = new byte[fileSize];  
        	while((read = inputStream.read(buff)) != -1) {  
        		baos.write(buff, 0, read);  
        	}  
        }
        
        ByteArrayDataSource(String name, String contentType, byte[] byteArray) throws IOException {  
        	this.name = name;  
        	this.contentType = contentType;  
             
        	baos = new ByteArrayOutputStream();  
            
        	baos.write(byteArray);  
        	 
        }
             
        public String getContentType() {  
        	return contentType;  
        }  
      
        public InputStream getInputStream() throws IOException {  
        	return new ByteArrayInputStream(baos.toByteArray());  
        }  
  
        public String getName() {  
        	return name;  
        }

        public OutputStream getOutputStream() throws IOException {  
        	throw new IOException("Cannot write to this read-only resource");  
        }  
             
   }  