package it.eustema.inps.utility;

import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.mailCommander.vo.MessageVO;

import java.io.IOException;
import java.util.Properties;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

public class MailScanner {

	
	private static Logger log = Logger.getLogger(MailScanner.class);
	protected Folder inbox = null;
	protected Store store = null;
	protected int messageCount = -1;
	protected int counter = -1;
	protected Message message;
	
	public MailScanner(  ){
		
	}
	
	
	

	public void openConnection() throws BusinessException{
		Properties props = System.getProperties();
        Session session;
        
		try {
			session = Session.getDefaultInstance(props, null);
			//session.setDebug(true);
			store = session.getStore("imaps");
			store.connect(ConfigProp.serverPostaIMAPS,ConfigProp.uidPostaMC,ConfigProp.pwdPostaMC);
		
			System.out.println("OKKKK");
		log.debug("Connected to " + store);	
	
	    inbox = store.getFolder("inbox");
	    inbox.open(Folder.READ_WRITE); // Folder.READ_ONLY
	    messageCount = inbox.getMessageCount();
	    counter = 0;
	   
	
	    
		} catch(Exception e){
			throw new BusinessException( -60, "Errore nell'apertura della casella di posta in input" );
			
		}
	   
	}
	
	public boolean nextMessage(){
		counter++;
		if ( counter <= messageCount  )
			return true;
		else return false;
	}
	
	public MessageVO getMessage() throws BusinessException{
		
	
		MessageVO out = null;
		
		try {
		message = inbox.getMessage( counter );
	     
		
         out = new MessageVO();
		 out.setMessage( getBody(message) );
         out.setMittente( InternetAddress.toString( message.getFrom() ));
         out.setSubject( message.getSubject() );
		
		} catch (MessagingException e) {
			throw new BusinessException( -70, "Errore nel recupero del messaggio" );
		}
		
		return out;
	}
	
	public void removeMessage(){
		//Rimozione del messaggio dalla casella
        try {
			message.setFlag(Flags.Flag.SEEN, true);
			message.setFlag(Flags.Flag.DELETED, true);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
	}
	
	public void closeConnection() throws BusinessException{
		try {
			inbox.close(true);
			store.close();
		} catch (Exception e) {
			throw new BusinessException( -80, "Errore nella chiusura della connessione alla casella mail" );
		}
	    
	}
	
	
	protected String getBody( Message message ) throws BusinessException{
		String out = null;
		
		
		try {
			message.getContentType();
			if ( message.isMimeType("text/plain") || message.isMimeType("text/html") ){
				out = (String) message.getContent();
			}
		} catch (MessagingException e) {
			throw new BusinessException( -90, "Errore nel recupero dati del messaggio" );
		} catch (IOException e) {
			throw new BusinessException( -90, "Errore nel recupero dati del messaggio" );
		}
		
		
		return out;
	}
		

}
