package it.eustema.inps.utility;

import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.util.DebugConstants;
import it.eustema.inps.hermes.dto.Allegato;

import javax.mail.*;
import javax.mail.internet.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import javax.activation.*;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.*;

public class Posta {
	
	private Session getMailSession(String serverPosta,String uid,String password){
		Properties props = new Properties();
		props.put("mail.smtp.host", serverPosta);
		props.put("mail.smtp.username", uid);
		props.put("mail.smtp.password", password);	
		
		
		return  Session.getDefaultInstance(props,null);
	}
	
	private void validateParams(String serverPosta, String uid, String password, Collection destinatari, String oggetto,String testoEmail)
		throws IOException{
		
		if ( (serverPosta == null || serverPosta.trim().equals("")) ||
				(uid == null || uid.trim().equals("")) ||
				(password == null || password.trim().equals(""))
			)
			throw new IOException("I parametri di connessione al server di posta non sono corretti.Impossibile inviare il messaggio");

		if (destinatari == null || destinatari.size() == 0)
			throw new IOException("Il contenitore dei destinatari � vuoto.Impossibile inviare il messaggio");

		if (oggetto == null || oggetto.trim().equals(""))
			throw new IOException("L'oggetto del messaggio non � valorizzato.Impossibile inviare il messaggio");

		if (testoEmail == null || testoEmail.trim().equals(""))
			throw new IOException("Il testo del messaggio non � valorizzato.Impossibile inviare il messaggio");
		
	}
	
	
	
	public void inviaEmail(final AccountPecDTO accountPEC, List<DestinatarioDTO> destinatari, List<DestinatarioDTO> destinatariCC, 
             				String oggetto, String testoEmail, List<AllegatoDTO> allegati, String MessageID, MessaggioDTO messaggio) throws Exception {

			try
			{
					validateParams(accountPEC.getHostSmtp(), accountPEC.getUserName(), accountPEC.getPassword(), destinatari, oggetto, testoEmail);
					// Creazione di una mail session
					// Creazione di una mail session
					Properties props = new Properties();
					props.setProperty("mail.transport.protocol", "smtp");
					props.setProperty("mail.host", accountPEC.getHostSmtp());
					props.put("mail.smtp.auth", "true");
					props.put("mail.smtp.port", accountPEC.getPortSmtp());
					props.put("mail.smtp.socketFactory.port", accountPEC.getPortSmtp());
					props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
					props.put("mail.smtp.socketFactory.fallback", "false");
					props.setProperty("mail.smtp.quitwait", "false");
//					props.setProperty("mail.smtp.timeout", "20000");
//					props.put("mail.smtp.timeout", 20000);
					
					Session sessionMail = Session.getInstance(props, new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication( accountPEC.getUserName()+ConfigProp.dominioPEC,	accountPEC.getPassword() );
						}
					});

					// Creazione del messaggio da inviare
					PecMessage message = new PecMessage(sessionMail);
					
					message.setMessageId(MessageID);
//					message.setXRiferimentoMessageID(MessageID);
					message.setTipoRicevuta(messaggio.getTipoRicevuta());
					
//					message.addHeader(ConfigProp.invioPostaXRiferimentoMessageID, MessageID);
//					message.addHeader(ConfigProp.invioPostaMessageID, MessageID);
//					message.addHeader(ConfigProp.invioPostaTipoRicevuta, messaggio.getTipoRicevuta());

					// Aggiunta degli indirizzi del mittente 
					//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					InternetAddress fromAddress = new InternetAddress(accountPEC.getUserName()+ConfigProp.dominioPEC);
					message.setFrom(fromAddress);
					//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
					
					
					//Aggiunta degli indirizzi dei destinatari
					//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++						
//					if ( DebugConstants.ENABLE_DEBUG_CONSTANTS ){
//						destinatari = new ArrayList<DestinatarioDTO>();
//						destinatariCC = new ArrayList<DestinatarioDTO>();
//						DestinatarioDTO d = new DestinatarioDTO();
//						d.setAddr( DebugConstants.ACCOUNT_PEC_USER_NAME );
//						destinatari.add( d );
//					}
					
					InternetAddress[] toAddress;
					
//					if(DebugConstants.ENABLE_DEBUG_CONSTANTS){
//						toAddress = new InternetAddress[1];
//						InternetAddress to = new InternetAddress(destinatari.get(0).getAddr());		
//						toAddress[0] = to;	
//					}else{
					
					Vector<String> destinatariTest = new Vector<String>();
					if(DebugConstants.ENABLE_DEBUG_CONSTANTS_DESTINATARI)
						destinatariTest = caricaDestinatariTest();
					
					toAddress = new InternetAddress[destinatari.size()];
					int cont = 0;
					for (DestinatarioDTO dest: destinatari)
						{
							String destX = dest.getAddr();
							if(destinatariTest.size()==0){
								InternetAddress to = new InternetAddress(destX);		
								toAddress[cont] = to;	
								cont++;
							}else{
								if(destinatariTest.contains(destX)){
									InternetAddress to = new InternetAddress(destX);
									toAddress[cont] = to;	
									cont++;
								}
							}
						}
//					}
					message.setRecipients(Message.RecipientType.TO, toAddress);				
					//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					

					//Aggiunta degli indirizzi dei destinatari in CC
					//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			  
					if (destinatariCC != null && destinatariCC.size() > 0)
					{	
						InternetAddress[] toAddressCC = new InternetAddress[destinatariCC.size()];
						int contCC = 0;
						for ( DestinatarioDTO dest: destinatariCC )
							{
								String destX = dest.getAddr();
								if(destinatariTest.size()==0){
									InternetAddress to = new InternetAddress(destX);		
									toAddressCC[contCC] = to;	
									contCC++;
								}else{
									if(destinatariTest.contains(destX)){
										InternetAddress to = new InternetAddress(destX);		
										toAddressCC[contCC] = to;	
										contCC++;
									}
								}

							}		
						message.setRecipients(Message.RecipientType.CC, toAddressCC);	
					}
					//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
							  
					
					message.setSubject(oggetto.replaceAll("(\\r\\n|\\n|\\t)", ""));    
					
					//create the Multipart and add its parts to it
					Multipart mp = new MimeMultipart();
	
					// create and fill the first message part
					MimeBodyPart mbp1 = new MimeBodyPart();
					mbp1.setText(testoEmail);
					mp.addBodyPart(mbp1);	
					
					if (allegati != null && allegati.size() > 0)
					{					
					 for ( AllegatoDTO f: allegati )
					 {		
							MimeBodyPart mbp2 = new MimeBodyPart();
//							if(f.getContentType().equals("xml"))
//								f.setContentType("text/xml");
//							ByteArrayDataSource fds = new ByteArrayDataSource(f.getFileData(), f.getContentType());
//							mbp2.setDataHandler(new DataHandler(fds));
//							mbp2.setFileName(f.getFileName());
							FileDataSource isds = new FileDataSource(init(f));
							mbp2.setDataHandler(new DataHandler(isds));
							mbp2.setFileName(f.getFileName());
							mp.addBodyPart(mbp2);
					 }
					}
					
					message.setContent(testoEmail,"text/html");
					message.setContent(mp);
					
					message.updateHeaders();
					Transport.send(message);
//					message.updateHeaders();
/*
					try{
						props.setProperty("mail.transport.protocol", "smtp");
						props.setProperty("mail.host", accountPEC.getHostSmtp());
						props.put("mail.smtp.auth", "true");
						props.put("mail.smtp.port", accountPEC.getPortSmtp());
						props.put("mail.smtp.socketFactory.port", accountPEC.getPortSmtp());
						props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
						props.put("mail.smtp.socketFactory.fallback", "false");
						props.setProperty("mail.smtp.quitwait", "false");

						Session s = Session.getInstance(props,
								new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication( accountPEC.getUserName()+ConfigProp.dominioPEC,
							accountPEC.getPassword() );
							}
							}); 
						Folder sent = s.getStore().getFolder("Posta inviata");
						sent.appendMessages(new Message[] { message });
					}catch(Exception e){
						e.printStackTrace();
					}
*/					
			}
			catch (Exception e){
					throw e;  
			}

       }//inviaEmail
	
	//Invia mail per le pec di test
	public void inviaEmailDiTest(final AccountPecDTO accountPEC, List<DestinatarioDTO> destinatari, List<DestinatarioDTO> destinatariCC, 
				String oggetto, String testoEmail, List<AllegatoDTO> allegati, String MessageID, MessaggioDTO messaggio) throws Exception {

		try
		{
			validateParams(accountPEC.getHostSmtp(), accountPEC.getUserName(), accountPEC.getPassword(), destinatari, oggetto, testoEmail);
			// Creazione di una mail session
			// Creazione di una mail session
			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			props.setProperty("mail.host", accountPEC.getHostSmtp());
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", accountPEC.getPortSmtp());
			props.put("mail.smtp.socketFactory.port", accountPEC.getPortSmtp());
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.socketFactory.fallback", "false");
			props.setProperty("mail.smtp.quitwait", "false");

			
			Session sessionMail = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication( accountPEC.getUserName()+ConfigProp.dominioPECTest,	accountPEC.getPassword() );
				}
			});
		
			// Creazione del messaggio da inviare
			PecMessage message = new PecMessage(sessionMail);
			
			message.setMessageId(MessageID);
			message.setTipoRicevuta(messaggio.getTipoRicevuta());

		
			// Aggiunta degli indirizzi del mittente 
			//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			InternetAddress fromAddress = new InternetAddress(accountPEC.getUserName()+ConfigProp.dominioPECTest);
			message.setFrom(fromAddress);
			//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
			
			InternetAddress[] toAddress;
			
			Vector<String> destinatariTest = new Vector<String>();
			if(DebugConstants.ENABLE_DEBUG_CONSTANTS_DESTINATARI)
				destinatariTest = caricaDestinatariTest();
			
			toAddress = new InternetAddress[destinatari.size()];
			int cont = 0;
			for (DestinatarioDTO dest: destinatari)
				{
					String destX = dest.getAddr();
					if(destinatariTest.size()==0){
						InternetAddress to = new InternetAddress(destX);		
						toAddress[cont] = to;	
						cont++;
					}else{
						if(destinatariTest.contains(destX)){
							InternetAddress to = new InternetAddress(destX);
							toAddress[cont] = to;	
							cont++;
						}
					}
				}
			message.setRecipients(Message.RecipientType.TO, toAddress);				
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			
		
			//Aggiunta degli indirizzi dei destinatari in CC
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			  
			if (destinatariCC != null && destinatariCC.size() > 0)
			{	
				InternetAddress[] toAddressCC = new InternetAddress[destinatariCC.size()];
				int contCC = 0;
				for ( DestinatarioDTO dest: destinatariCC )
					{
						String destX = dest.getAddr();
						if(destinatariTest.size()==0){
							InternetAddress to = new InternetAddress(destX);		
							toAddressCC[contCC] = to;	
							contCC++;
						}else{
							if(destinatariTest.contains(destX)){
								InternetAddress to = new InternetAddress(destX);		
								toAddressCC[contCC] = to;	
								contCC++;
							}
						}
		
					}		
				message.setRecipients(Message.RecipientType.CC, toAddressCC);	
			}
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
					  
			
			message.setSubject(oggetto.replaceAll("(\\r\\n|\\n|\\t)", ""));    
			
			//create the Multipart and add its parts to it
			Multipart mp = new MimeMultipart();
		
			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			mbp1.setText(testoEmail);
			mp.addBodyPart(mbp1);	
			
			if (allegati != null && allegati.size() > 0)
			{					
			 for ( AllegatoDTO f: allegati )
			 {		
					MimeBodyPart mbp2 = new MimeBodyPart();
					FileDataSource isds = new FileDataSource(init(f));
					mbp2.setDataHandler(new DataHandler(isds));
					mbp2.setFileName(f.getFileName());
					mp.addBodyPart(mbp2);
			 }
			}
			
			message.setContent(testoEmail,"text/html");
			message.setContent(mp);
			
			message.updateHeaders();
			Transport.send(message);
		}
		catch (Exception e){
			throw e;  
		}
		
	}//inviaEmail test
	
	
	public void inviaInsertDBEmailAllegati(String serverPosta, String uid,
			String password, String mittente, Vector<String> destinatari,
			Vector<String> destinatariCC, String oggetto, String testoEmail,
			AllegatoDTO allegati, boolean isHtml) throws Exception {
		try {
			Session sessionMail = getMailSession(serverPosta, uid, password);

			// Creazione del messaggio da inviare
			MimeMessage message = new MimeMessage(sessionMail);

			// Aggiunta degli indirizzi del mittente
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			InternetAddress fromAddress = new InternetAddress(mittente);
			// InternetAddress fromAddress = new
			// InternetAddress("noReply.pecpei@inps.it");
			message.setFrom(fromAddress);
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			// Aggiunta degli indirizzi dei destinatari
			// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			InternetAddress[] toAddress = new InternetAddress[destinatari
					.size()];
			Enumeration<String> eDest = destinatari.elements();
			int cont = 0;
			while (eDest.hasMoreElements()) {
				String destX = (String) eDest.nextElement();
				InternetAddress to = new InternetAddress(destX);
				toAddress[cont] = to;
				cont++;
			}
			message.setRecipients(Message.RecipientType.TO, toAddress);
			// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// Aggiunta degli indirizzi dei destinatari in CC
			// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			if (destinatariCC != null && destinatariCC.size() > 0) {
				InternetAddress[] toAddressCC = new InternetAddress[destinatariCC
						.size()];
				Enumeration<String> eDestCC = destinatariCC.elements();
				int contCC = 0;
				while (eDestCC.hasMoreElements()) {
					String destX = (String) eDestCC.nextElement();
					InternetAddress to = new InternetAddress(destX);
					toAddressCC[contCC] = to;
					contCC++;
				}
				message.setRecipients(Message.RecipientType.CC, toAddressCC);
			}
			// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			message.setSubject(oggetto);

			// create the Multipart and add its parts to it
			Multipart mp = new MimeMultipart();

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
//			mbp1.setText(testoEmail);
//			mp.addBodyPart(mbp1);
			
			MimeBodyPart mbp2 = new MimeBodyPart();
//			if (allegati.getContentType().equals("xml"))
//				allegati.setContentType("text/xml");
//			ByteArrayDataSource fds = new ByteArrayDataSource(allegati.getFileData(), allegati.getContentType());
//			mbp2.setDataHandler(new DataHandler(fds));
//			mbp2.setFileName(allegati.getFileName());
			FileDataSource isds = new FileDataSource(init(allegati));
			mbp2.setDataHandler(new DataHandler(isds));
			mbp2.setFileName(allegati.getFileName());
			mp.addBodyPart(mbp2);

			if (isHtml) {
				//System.out.println(isHtml);
				mbp1.setContent(testoEmail, "text/html");
			} else {
				//System.out.println(isHtml);
				mbp1.setContent(testoEmail, "text/plain; charset=us-ascii");
			}

			mp.addBodyPart(mbp1);
			message.setContent(mp);
			Transport.send(message);

		} catch (Exception e) {
			throw e;
		}
	}	
	public void inviaEmailAllegati(String serverPosta, String uid,
			String password, String mittente, Vector<String> destinatari,
			Vector<String> destinatariCC, String oggetto, String testoEmail,
			AllegatoDTO allegati, boolean isHtml) throws Exception {
		try {
			Session sessionMail = getMailSession(serverPosta, uid, password);

			// Creazione del messaggio da inviare
			MimeMessage message = new MimeMessage(sessionMail);

			// Aggiunta degli indirizzi del mittente
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			InternetAddress fromAddress = new InternetAddress(mittente);
			// InternetAddress fromAddress = new
			// InternetAddress("noReply.pecpei@inps.it");
			message.setFrom(fromAddress);
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			// Aggiunta degli indirizzi dei destinatari
			// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			InternetAddress[] toAddress = new InternetAddress[destinatari
					.size()];
			Enumeration<String> eDest = destinatari.elements();
			int cont = 0;
			while (eDest.hasMoreElements()) {
				String destX = (String) eDest.nextElement();
				InternetAddress to = new InternetAddress(destX);
				toAddress[cont] = to;
				cont++;
			}
			message.setRecipients(Message.RecipientType.TO, toAddress);
			// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// Aggiunta degli indirizzi dei destinatari in CC
			// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			if (destinatariCC != null && destinatariCC.size() > 0) {
				InternetAddress[] toAddressCC = new InternetAddress[destinatariCC
						.size()];
				Enumeration<String> eDestCC = destinatariCC.elements();
				int contCC = 0;
				while (eDestCC.hasMoreElements()) {
					String destX = (String) eDestCC.nextElement();
					InternetAddress to = new InternetAddress(destX);
					toAddressCC[contCC] = to;
					contCC++;
				}
				message.setRecipients(Message.RecipientType.CC, toAddressCC);
			}
			// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			message.setSubject(oggetto);

			// create the Multipart and add its parts to it
			Multipart mp = new MimeMultipart();

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			mbp1.setText(testoEmail);
			mp.addBodyPart(mbp1);
			
			MimeBodyPart mbp2 = new MimeBodyPart();
//			if (allegati.getContentType().equals("xml"))
//				allegati.setContentType("text/xml");
//			ByteArrayDataSource fds = new ByteArrayDataSource(allegati.getFileData(), allegati.getContentType());
//			mbp2.setDataHandler(new DataHandler(fds));
//			mbp2.setFileName(allegati.getFileName());
			FileDataSource isds = new FileDataSource(init(allegati));
			mbp2.setDataHandler(new DataHandler(isds));
			mbp2.setFileName(allegati.getFileName());
			mp.addBodyPart(mbp2);

			if (isHtml) {
				//System.out.println(isHtml);
				mbp1.setContent(testoEmail, "text/html");
			} else {
				//System.out.println(isHtml);
				mbp1.setContent(testoEmail, "text/plain; charset=us-ascii");
			}

			mp.addBodyPart(mbp1);
			message.setContent(mp);
			Transport.send(message);

		} catch (Exception e) {
			throw e;
		}
	}
		public void inviaEmail(String serverPosta, String uid, String password,
								String mittente, Vector<String> destinatari, Vector<String> destinatariCC,
								String oggetto, String testoEmail, boolean isHtml) throws Exception {
			try {
				validateParams(serverPosta, uid, password, destinatari, oggetto, testoEmail);
					
				// Creazione di una mail session
				Session sessionMail = getMailSession(serverPosta,uid,password);
				
				// Creazione del messaggio da inviare
				MimeMessage message = new MimeMessage(sessionMail);

				// Aggiunta degli indirizzi del mittente 
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				InternetAddress fromAddress = new InternetAddress(mittente);
				//InternetAddress fromAddress = new InternetAddress("noReply.pecpei@inps.it");
				message.setFrom(fromAddress);
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
				
				
				//Aggiunta degli indirizzi dei destinatari
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++						
				InternetAddress[] toAddress = new InternetAddress[destinatari.size()];
				Enumeration<String> eDest = destinatari.elements();
				int cont = 0;
				while (eDest.hasMoreElements())
				{
					String destX = (String)eDest.nextElement();
					InternetAddress to = new InternetAddress(destX);		
					toAddress[cont] = to;	
					cont++;
				}		
				message.setRecipients(Message.RecipientType.TO, toAddress);				
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				

				//Aggiunta degli indirizzi dei destinatari in CC
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			  
				if (destinatariCC != null && destinatariCC.size() > 0)
				{	
					InternetAddress[] toAddressCC = new InternetAddress[destinatariCC.size()];
					Enumeration<String> eDestCC = destinatariCC.elements();
					int contCC = 0;
					while (eDestCC.hasMoreElements())
					{
						String destX = (String)eDestCC.nextElement();
						InternetAddress to = new InternetAddress(destX);		
						toAddressCC[contCC] = to;	
						contCC++;
					}		
						message.setRecipients(Message.RecipientType.CC, toAddressCC);	
				}
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
						  
				
				message.setSubject(oggetto);    
				
				//create the Multipart and add its parts to it
				Multipart mp = new MimeMultipart();
				
				// create and fill the first message part
				MimeBodyPart mbp1 = new MimeBodyPart();
				
				if(isHtml){
					//System.out.println(isHtml);
					mbp1.setContent(testoEmail, "text/html");
				}else{
					//System.out.println(isHtml);
					mbp1.setContent(testoEmail, "text/plain; charset=us-ascii");
				}
					
				mp.addBodyPart(mbp1);					
				message.setContent(mp);				
				Transport.send(message);
//				try{
//				Folder sent = sessionMail.getStore().getFolder("Sent");
//				sent.appendMessages(new Message[] { message });
//				}catch(Exception e){
//					e.printStackTrace();
//				}
			}
			catch (Exception e){
				throw e;  
			}
		}

		
		public void inviaEmailPosta(String serverPosta, String uid, String password,
									String mittente, Vector<String> destinatari, Vector<String> destinatariCC,
									String oggetto, String testoEmail, boolean isHtml) throws Exception {

			try {
				validateParams(serverPosta, uid, password, destinatari, oggetto, testoEmail);
//				if ((serverPosta == null || serverPosta.trim().equals("")) ||
//						(uid == null || uid.trim().equals("")) ||
//						(password == null || password.trim().equals("")))
//					throw new IOException("I parametri di connessione al server di posta non sono corretti.Impossibile inviare il messaggio");
//				
//				if (destinatari == null || destinatari.size() == 0)
//					throw new IOException("Il contenitore dei destinatari � vuoto.Impossibile inviare il messaggio");
//				
//				if (oggetto == null || oggetto.trim().equals(""))
//					throw new IOException("L'oggetto del messaggio non � valorizzato.Impossibile inviare il messaggio");
//				
//				if (testoEmail == null || testoEmail.trim().equals(""))
//					throw new IOException("Il testo del messaggio non � valorizzato.Impossibile inviare il messaggio");
				
				// Creazione di una mail session
				Session sessionMail = getMailSession(serverPosta,uid,password);				
//				Properties props = new Properties();
//				props.put("mail.smtp.host", serverPosta);
//				props.put("mail.smtp.username", uid);
//				props.put("mail.smtp.password", password);
//				
//				Session sessionMail =  Session.getDefaultInstance(props,null);
				
				// Creazione del messaggio da inviare
				MimeMessage message = new MimeMessage(sessionMail);
				
				// Aggiunta degli indirizzi del mittente 
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				InternetAddress fromAddress = new InternetAddress(mittente);
				//InternetAddress fromAddress = new InternetAddress("noReply.pecpei@inps.it");
				message.setFrom(fromAddress);
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
				
				
				//Aggiunta degli indirizzi dei destinatari
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++						
				InternetAddress[] toAddress = new InternetAddress[destinatari.size()];
				Enumeration<String> eDest = destinatari.elements();
				int cont = 0;
				while (eDest.hasMoreElements())
				{
				String destX = (String)eDest.nextElement();
				InternetAddress to = new InternetAddress(destX);		
				toAddress[cont] = to;	
				cont++;
				}		
				message.setRecipients(Message.RecipientType.TO, toAddress);				
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				
				
				//Aggiunta degli indirizzi dei destinatari in CC
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			  
				if (destinatariCC != null && destinatariCC.size() > 0)
				{	
				InternetAddress[] toAddressCC = new InternetAddress[destinatariCC.size()];
				Enumeration<String> eDestCC = destinatariCC.elements();
				int contCC = 0;
				while (eDestCC.hasMoreElements())
				{
					String destX = (String)eDestCC.nextElement();
					InternetAddress to = new InternetAddress(destX);		
					toAddressCC[contCC] = to;	
					contCC++;
				}		
					message.setRecipients(Message.RecipientType.CC, toAddressCC);	
				}
				//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
					  
				
				message.setSubject(oggetto);    
				
				//create the Multipart and add its parts to it
				Multipart mp = new MimeMultipart();
				
				// create and fill the first message part
				MimeBodyPart mbp1 = new MimeBodyPart();
				
				if(isHtml){
					//System.out.println(isHtml);
					mbp1.setContent(testoEmail, "text/html");
				}else{
					//System.out.println(isHtml);
					mbp1.setContent(testoEmail, "text/plain; charset=us-ascii");
				}
				
				mp.addBodyPart(mbp1);	
				message.setContent(mp);				
				Transport.send(message);
				
			}
			catch (Exception e){
				throw e;  
			}
		}

		/*
		 * �  progettopa@pec.it
		 * �  sandro.moretti@progettopa.it
		   �  gianluca.solda@wizardsgroup.it
		   �  sandro.mo@libero.it
		   �  giuseppe.fucci@inps.it
		   �  m.carpentieri@eustema.it
		   �  utente04@postacert.inps.gov.it
		   �  utente03@postacert.inps.gov.it
		 * */
		
		public Vector<String> caricaDestinatariTest(){
			Vector<String> destinatariTest = new Vector<String>(6);
			destinatariTest.add("utente04"+ConfigProp.dominioPECTest);
			destinatariTest.add("utente03"+ConfigProp.dominioPECTest);
			destinatariTest.add("utente01"+ConfigProp.dominioPECTest);
			destinatariTest.add("utente02"+ConfigProp.dominioPECTest);
			destinatariTest.add("m.carpentieri@eustema.it");
			destinatariTest.add("giuseppe.fucci@inps.it");
			destinatariTest.add("sandro.mo@libero.it");
			destinatariTest.add("gianluca.solda@wizardsgroup.it");
			destinatariTest.add("sandro.moretti@progettopa.it");
			destinatariTest.add("sandro.morelli@progettopa.it");
			destinatariTest.add("progettopa@pec.it");
			destinatariTest.add("mcarpentieri@gmail.com");
			destinatariTest.add("sandro.moretti-2881@postacertificata.gov.it");
			
			//Carico i destinatari presenti in tabella
			for(String s: ConfigProp.indirizziPerTest)
				destinatariTest.add(s+ConfigProp.dominioPECTest);
			
			return destinatariTest;
		}
		private File init(AllegatoDTO a) throws IOException {  
			 
			 File tempFile;
		     tempFile = File.createTempFile(md5HashString(a.getFileName()),a.getTipoAllegato());  
		     tempFile.deleteOnExit();  
		     InputStream in = a.getInputStream();
		     FileOutputStream fout = null;  
		     try {  
		         fout = new FileOutputStream(tempFile);  
		         fout.write(a.getFileData());  
		     }finally {  
		         if (in != null) {  
		             in.close();  
		         }  
		         if (fout != null) {  
		             fout.close();  
		         }  
		     }
		     return tempFile;
		 }  
		
		
		private static String md5HashString(String inputString){
			String md5HashString = "";
			
			MessageDigest md;
			try {
				md = MessageDigest.getInstance("MD5");
				md.update(inputString.getBytes());
			 
		        byte byteData[] = md.digest();
		 
		        //convert the byte to hex format method 1
		        StringBuffer sb = new StringBuffer();
		        for (int i = 0; i < byteData.length; i++) {
		        	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		        }
		        
		        md5HashString = sb.toString();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
	       		
			return md5HashString;
		}	
		
		
		public void inviaEmailHermes(String serverPosta,String uid,String password,
		         String mittente,Vector destinatari,Vector destinatariCC,
		         String oggetto,String testoEmail,ArrayList<Allegato> allegati) throws Exception {
		
		 try {
			 
//			 destinatari.removeAllElements();
//			 destinatari.add("a.leanzi@eustema.it");
//			 destinatari.add("m.carpentieri@eustema.it");
//			 destinatariCC.removeAllElements();
			 
			if ((serverPosta == null || serverPosta.trim().equals("")) ||
				(uid == null || uid.trim().equals("")) ||
				(password == null || password.trim().equals("")))
				throw new IOException("I parametri di connessione al server di posta non sono corretti.Impossibile inviare il messaggio");
		
			if (destinatari == null || destinatari.size() == 0)
			throw new IOException("Il contenitore dei destinatari � vuoto.Impossibile inviare il messaggio");
			
			if (oggetto == null || oggetto.trim().equals(""))
					throw new IOException("L'oggetto del messaggio non � valorizzato.Impossibile inviare il messaggio");
			
			if (testoEmail == null || testoEmail.trim().equals(""))
							throw new IOException("Il testo del messaggio non � valorizzato.Impossibile inviare il messaggio");
				
			// Creazione di una mail session
			Properties props = new Properties();
			props.put("mail.smtp.host", serverPosta);
			props.put("mail.smtp.username", uid);
			props.put("mail.smtp.password", password);
			
			Session sessionMail =  Session.getDefaultInstance(props,null);
			
			// Creazione del messaggio da inviare
			MimeMessage message = new MimeMessage(sessionMail);
			
			// Aggiunta degli indirizzi del mittente 
			//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			InternetAddress fromAddress = new InternetAddress(mittente);
			message.setFrom(fromAddress);
			//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
			
			
			//Aggiunta degli indirizzi dei destinatari
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++						
			InternetAddress[] toAddress = new InternetAddress[destinatari.size()];
			Enumeration eDest = destinatari.elements();
			int cont = 0;
			while (eDest.hasMoreElements())
			{
				String destX = (String)eDest.nextElement();
				InternetAddress to = new InternetAddress(destX);		
				toAddress[cont] = to;	
				cont++;
			}		
			message.setRecipients(Message.RecipientType.TO, toAddress);		
			
			
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			
			//Aggiunta degli indirizzi dei destinatari in CC
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			  
			if (destinatariCC != null && destinatariCC.size() > 0)
			{	
				InternetAddress[] toAddressCC = new InternetAddress[destinatariCC.size()];
				Enumeration eDestCC = destinatariCC.elements();
				int contCC = 0;
				while (eDestCC.hasMoreElements())
				{
					String destX = (String)eDestCC.nextElement();
					InternetAddress to = new InternetAddress(destX);		
					toAddressCC[contCC] = to;	
					contCC++;
				}		
					message.setRecipients(Message.RecipientType.CC, toAddressCC);	
			}
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
			
					  
			if(ConfigProp.abilitaCopiaNascostaHermes.equals("true")){
				InternetAddress[] toAddressBCC = new InternetAddress[1];
				InternetAddress to = new InternetAddress(ConfigProp.emailCopiaNascostaHermes);		
				toAddressBCC[0] = to;	
				message.setRecipients(Message.RecipientType.BCC, toAddressBCC);
			}
			
		    message.setSubject(oggetto);    
			
			//create the Multipart and add its parts to it
			Multipart mp = new MimeMultipart();
			
			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			

			mbp1.setContent(testoEmail, "text/html");
			
			
			mp.addBodyPart(mbp1);	
			
			if (allegati != null && allegati.size() > 0) {					
				Iterator it = allegati.iterator();
				
				while (it.hasNext()){		
					//create the second message part
					MimeBodyPart mbp2 = new MimeBodyPart();
				 
					//attach the file to the message		    
					Allegato allegato = (Allegato)it.next();		
					
					ByteArrayDataSource isds = new ByteArrayDataSource(allegato.getFileName(),
							allegato.getContentType(),
							allegato.getFileData());
					mbp2.setDataHandler(new DataHandler(isds));
					mbp2.setFileName(allegato.getFileName());
					mp.addBodyPart(mbp2);				
				}
			}
			
			message.setContent(mp);
//			message.setContent(testoEmail,"text/html");
		
			Transport.send(message);
		
		}
		catch (Exception e){
			throw e;  
		}
	 }
	 

		public static void main(String...strings){
			Posta p = new Posta();
			Vector allegati = new Vector();
			String linkMsg = "http://intranet.inps.it/APP01/Servizi.NetNT/PortaleComunicazione/default.aspx?msgHermes="+"test"+"&data=";
			System.err.println("linkMsg: "+linkMsg);
			String oggettoProtocollato = "Il Messaggio HERMES con oggetto: [ Rilascio nuova procedura Intranet per la gestione delle comunicazioni di sospensione dell�obbligo contributivo nell�ambito del lavoro domestico. ] � stato inviato con numero di protocollo [ test ]";
			String testoEmail = "Oggetto del Messaggio Inviato:"+
			"<br/>"+
			StringEscapeUtils.escapeHtml("Rilascio nuova procedura Intranet per la gestione delle comunicazioni di sospensione dell�obbligo contributivo nell�ambito del lavoro domestico.")+
			"<br/><br/>"+
			"--------------------------------"+
			"<br/><br/>"+
			"Numero di Protocollo assegnato tramite il sistema del Protocollo Informatico: "+
			"<br/><br/><br/><br/>"+
			"Per qualunque chiarimento e/o problematica in merito contattare " + ConfigProp.emailInfoHermes + " via posta elettronica."+						
			"<br/><br/><br/><a href=\"" + linkMsg + "\">Visualizza Messaggio Hermes inviato</a>";
			Vector destinatariCC = new Vector();
			Vector<String> destinatari = new Vector<String>();
			destinatari.add("supporto.pecpei@inps.it");
			//ArrayList<Allegato> listaAllegati = messaggio.getElencoFileAllegati();
			ArrayList<Allegato> listaAllegati = new ArrayList<Allegato>();
			try {
				p.inviaEmailHermes("smtp.inps", "hermes01", "hhermes-2010", "Hermes.Notifica@inps.it", destinatari, destinatariCC, oggettoProtocollato, testoEmail, listaAllegati);
			} catch (Exception e) {
				// TODO Blocco catch generato automaticamente
				e.printStackTrace();
			}
		}

		
		
	}
