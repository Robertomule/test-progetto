package it.eustema.inps.utility;

import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.util.TripleDES;
import it.eustema.inps.mailCommander.vo.MessageVO;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;




import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;







public class MailScannerPec extends MailScanner{

	
	private static Logger log = Logger.getLogger(MailScannerPec.class);
	private String from;
	public MailScannerPec(  ){
		
	}
	
	public void openConnection() throws BusinessException{
		TripleDES u = new TripleDES();
		Properties props = new Properties();
	    props.put("mail.host", "inm.telecompost.it" );
	    props.put("mail.store.protocol", "pop3s" );
	    props.put("mail.pop3s.port", "995" );
	    props.put("mail.pop3.ssl.trust","inm.telecompost.it" ); // or "*"
	    Session session = Session.getInstance(props);
	    //session.setDebug(true);
        
		try {
			store = session.getStore();
			
			store.connect( "nominativa.appl1@test.telecompost.it", u.decrypt("JDIWx1J5peTxKUF1Ub6F4g==") );
			
		log.debug("Connected to " + store);	
	
		inbox = store.getFolder("inbox");
	    inbox.open(Folder.READ_WRITE); // Folder.READ_ONLY
	    messageCount = inbox.getMessageCount();
	    counter = 0;
	   
	
	    
		} catch(Exception e){
			throw new BusinessException( -60, "Errore nell'apertura della casella di posta in input" );
			
		}
	   
	}
	
	
	@Override
	protected String getBody( Message message ) throws BusinessException{
		String out = null;
		
		
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			message.writeTo(os);
			String raw = (new String(os.toByteArray()));
			MimeBodyPart mbp = new MimeBodyPart(new ByteArrayInputStream(raw.getBytes()) );
			
			
			
			message.getContentType();
			if ( message.isMimeType("text/plain") || message.isMimeType("text/html") ){
				out = (String) message.getContent();
			} else out = elabPostaCertEML( mbp );
		} catch (MessagingException e) {
			throw new BusinessException( -90, "Errore nel recupero dati del messaggio" );
		} catch (IOException e) {
			throw new BusinessException( -90, "Errore nel recupero dati del messaggio" );
		}
		
		
		return out;
	}
	
	@Override
	public MessageVO getMessage() throws BusinessException {
	
		MessageVO out = null;
		
		try {
			message = inbox.getMessage( counter );
			//Rimozione del messaggio dalla casella
	        //message.setFlag(Flags.Flag.SEEN, true);
	        //message.setFlag(Flags.Flag.DELETED, true);
	        
	     
		
         out = new MessageVO();
		
         
         out.setMessage( getBody(message) );
         if ( from.indexOf("<") != -1 )
        	 out.setMittente( from.substring( from.indexOf("<")+1, from.indexOf(">") ) );
         else out.setMittente( from );  
         out.setSubject( message.getSubject().replaceAll("ANOMALIA MESSAGGIO: ", "").replaceAll("FW: ", "").trim()  );
         
        
         //out.setMessage( message.getContent() ) );
		
		} catch (MessagingException e) {
			throw new BusinessException( -70, "Errore nel recupero del messaggio" );
		}
		
		
		
		
		
		return out;
	}
	
	
	
	
	private String elabPostaCertEML( Part bodyPart
			) throws BusinessException {
		try {
			Object content = bodyPart.getContent();
			from = readHeaders("From", bodyPart.getAllHeaders()).get(0);
	        
	         
			if (content instanceof MimeMessage) {
				content = ((MimeMessage) content).getContent();
			}
			if (bodyPart.isMimeType("text/plain")){
				
					return (String) content;
					
			}else if (bodyPart.isMimeType("text/html")){
				return (String) content;
			}else if (content instanceof Multipart) {
				for (int i = 0; i < ((Multipart) content).getCount(); i++) {
					Part p = ((Multipart) content).getBodyPart(i);
					if (p.getContent() instanceof Multipart) {
						return addMultipartAttach((Multipart) p.getContent() );
					} else if (p instanceof Part) {
						InputStream is = null;
						
						if (!Part.ATTACHMENT.equalsIgnoreCase(p.getDisposition()) && !StringUtils.isNotBlank(p.getFileName())) {
							Object mm = p.getContent();
							if (mm instanceof Multipart)
								return addMultipartAttach( (Multipart) mm );
//							else if (mm instanceof String) {
//								messaggio.setPostaCertEMLBody((String) mm);
//							}
							else if (p.isMimeType("text/plain")){
								return (String) mm;
								
							}else if (p.isMimeType("text/html")){
								return (String) mm;
							}
							continue;
						}
						return "";

						
						
						
						
						
					
					// test
				}
			}
		}
			
			return "";
//		} catch (ParseException e) {
//			throw new NotifyMailBusinessException(-8000,
//					"Errore nel recupero degli allegati del messaggio: ["
//							+ messaggio.getMessageId()
//							+ "] Dettaglio Errore: " + e.getMessage());	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new BusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio. Dettaglio Errore: " + e.getMessage());	
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			throw new BusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio. Dettaglio Errore: " + e.getMessage());	
		}

	}
	
	
	
	private String addMultipartAttach( Multipart multipart )
			throws BusinessException {
		InputStream is = null;

		
		String out = null;
		try {

			for (int i = 0; i < multipart.getCount(); i++) {
				Part bodyPart = multipart.getBodyPart(i);
				

				if (!Part.ATTACHMENT
						.equalsIgnoreCase(bodyPart.getDisposition())
						&& !StringUtils.isNotBlank(bodyPart.getFileName())) {

					Object mm = bodyPart.getContent();
					if (mm instanceof Multipart){
						addMultipartAttach((Multipart) mm );
					}
//					else if (mm instanceof String) {
//						messaggioDTO.setBodyHtml((String) mm);
//						messaggioDTO.setBody((String) mm);
//					}
					else if (bodyPart.isMimeType("text/plain")){
						out = (String) mm;
					}else if (bodyPart.isMimeType("text/html")){
						out = (String) mm;
					}
					continue;
				}

				is = bodyPart.getInputStream();
				ByteArrayOutputStream aos = new ByteArrayOutputStream();
				byte[] buf = new byte[4096];
				int bytesRead;

				while ((bytesRead = is.read(buf)) != -1) {
					aos.write(buf, 0, bytesRead);
				}

				
				
				if ( "postacert.eml".equalsIgnoreCase( (bodyPart.getFileName() != null ? MimeUtility.decodeText(bodyPart.getFileName()): "") )){
					ByteArrayInputStream bo = new ByteArrayInputStream(aos.toByteArray());
    				MimeBodyPart mbp = new MimeBodyPart(bo);
    				
					out = elabPostaCertEML( mbp );
					
					
				}
				
				aos.close();
				is.close();
				
				
			}
			
			return out;
//		} catch (ParseException e) {
//			throw new NotifyMailBusinessException(-8000,
//					"Errore nel recupero degli allegati del messaggio: ["
//							+ messaggioDTO.getMessageId()
//							+ "] Dettaglio Errore: " + e.getMessage());	
		} catch (IOException e) {
			throw new BusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio. Dettaglio Errore: " + e.getMessage());
		} catch (MessagingException e) {
			throw new BusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio. Dettaglio Errore: " + e.getMessage());
		}

	}
	
	public static List<String> readHeaders(String pattern, Enumeration headers) {
		ArrayList<String> listaParamentri = new ArrayList<String>();
		while (headers.hasMoreElements()) {
			Header h = (Header) headers.nextElement();
			if (h.getName().equalsIgnoreCase(pattern)) {
				listaParamentri.add(h.getValue());
			}
		}

		return listaParamentri;
	}

		

}
