package it.eustema.inps.utility;

import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AgentExecutionParamDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.hermes.dto.ComboElement;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StaticUtil {

	
	/**
	 * 
	 * @param listaParams
	 * @return
	 */
	public static Map<String, String> convertParamsListToMap(List<AgentExecutionParamDTO> listaParams){
		
		Map<String, String> paramsMap = new HashMap<String, String>();
		for(AgentExecutionParamDTO agentParam: listaParams){				
			paramsMap.put(agentParam.getParamName(), agentParam.getParamValue());
		}
		
		return paramsMap;
	}
	
	public static String generateUiDoc(String codiceAMM, String matricola,String idSede,String ipAddr){
		String uiDoc="";
		
		java.util.Date date= new java.util.Date();
		//recupera il timestamp
		String timestamp = new Timestamp(date.getTime()).toString();
		
		//da requisiti viene calcolata la stringa uiDoc in questo modo
		uiDoc = codiceAMM.concat("-")
						.concat(idSede).concat("-")
						.concat(matricola).concat("-")
						.concat(ipAddr).concat("-")
						.concat(timestamp);
		
		return uiDoc;
	}
	
	private static String md5HashString(String inputString){
		String md5HashString = "";
		
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(inputString.getBytes());
		 
	        byte byteData[] = md.digest();
	 
	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	        	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        
	        md5HashString = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
       		
		return md5HashString;
	}
	
	
	public static String generateMessageIdPei(String uiDoc){
		
		String messageId="<"+md5HashString(uiDoc).toUpperCase();
		messageId += ">";
		
		
		return messageId;
		
	}
	public static String generateMessageIdRicevutaPei(String uiDoc, String codiceAOO){
		
		String messageId="<("+md5HashString(uiDoc+new java.util.Date()+codiceAOO).toUpperCase();
		messageId += ")@inps.it>";
		
		
		return messageId;
		
	}
	public static String preparaBodyPerRicevutaPEI(MessaggioDTO messaggioUscente,MessaggioDTO messaggioEntrante ){
		StringBuilder body = new StringBuilder();
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		body.append("Ricevuta di Protocollazione PEI \n");
		body.append("\n");
		body.append("\n");
		body.append("\n");
		body.append("Il "+sf.format(new java.util.Date())+" il messaggio \""+messaggioUscente.getSubject()+"\" inviato da "+messaggioUscente.getDesMitt()+" ed indirizzato a "+messaggioEntrante.getDesMitt()+"\n");
		body.append("\n");
		body.append("� stato ricevuto e protocollato "+messaggioEntrante.getSegnaturaRoot()+"\n");
		body.append("\n");
		body.append("Identificativo del messaggio "+messaggioUscente.getMessageId()+"\n");
		body.append("\n");
		return body.toString();
	}
	public static String decodicaCaratteriNonStampabili(String testo) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException, Exception{
		String newTesto ="";
		byte[] b = testo.getBytes();
		byte[] r = new byte[testo.getBytes().length];
		int idx = 0;
		for(byte by:b){
			if((by>0 && by<32) || by == 127 ){
//				r[idx]=0;
//				idx++;
			}else if(by>=32 && by < 127){
				r[idx]=by;
				idx++;
			}else{
				if(by >= -127 && by <= -1 ){
					byte[] resultHtml = new MessaggiDAO().estraiASCIItoHTML(by);
//				incremento la dimensione dell'array aggiungendo la nuova stringa
					byte[] rc = new byte[r.length+resultHtml.length];
					System.arraycopy(r, 0, rc, 0, r.length);
					for(byte bh:resultHtml){
						rc[idx]=bh;
						idx++;
					}
//				riassegno al vecchio array i nuovi valori/dimensione
					r = new byte[r.length+resultHtml.length-1];
					System.arraycopy(rc, 0, r, 0, r.length);
				}
			}
		}
//		definiamo l'array definito eliminando i caratteri superflui
		idx = 0;
		for(byte by:r){
			if(by == 0) idx++;
		}

		byte[] fr = new byte[r.length-idx];
		idx = 0;
		for(byte by:r){
			if(by != 0){
				fr[idx]=by;
				idx++;
			}
		}
		newTesto = new String(fr);
		return newTesto;
		
	}
	public static String decodicaCaratteriNonStampabiliCarattere(String testo) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException, Exception{
		String newTesto ="";
		byte[] b = testo.getBytes();
		byte[] r = new byte[testo.getBytes().length];
		int idx = 0;
		for(byte by:b){
			if((by>0 && by<32) || by == 127 ){
//				r[idx]=0;
//				idx++;
			}else if(by>=32 && by < 127){
				r[idx]=by;
				idx++;
			}else{
				if(by >= -127 && by <= -1 ){
					byte[] resultHtml = new MessaggiDAO().estraiASCIItoCarattere(by);
//				incremento la dimensione dell'array aggiungendo la nuova stringa
					byte[] rc = new byte[r.length+resultHtml.length];
					System.arraycopy(r, 0, rc, 0, r.length);
					for(byte bh:resultHtml){
						rc[idx]=bh;
						idx++;
					}
//				riassegno al vecchio array i nuovi valori/dimensione
					r = new byte[r.length+resultHtml.length-1];
					System.arraycopy(rc, 0, r, 0, r.length);
				}
			}
		}
//		definiamo l'array definito eliminando i caratteri superflui
		idx = 0;
		for(byte by:r){
			if(by == 0) idx++;
		}

		byte[] fr = new byte[r.length-idx];
		idx = 0;
		for(byte by:r){
			if(by != 0){
				fr[idx]=by;
				idx++;
			}
		}
		newTesto = new String(fr);
		return newTesto;
		
	}
	
	public static String[] getStringFromArrayListCE(ArrayList<ComboElement> listaCE,String separatore){
		String s1="";
		String s2="";
		String[] result = new String[2];
		ComboElement ce;
		Iterator it = listaCE.iterator();
		while (it.hasNext()){
			ce = (ComboElement)it.next();
			s1 += ce.getId() + separatore;
			s2 += ce.getDescription() + separatore;
		}
		//elimina l'ultima virgola
		s1 = s1.substring(0,s1.length()-separatore.length());
		s2 = s2.substring(0,s2.length()-separatore.length());
		result[0] = s1;
		result[1] = s2;
		return result;
	}
	
	//Data una stringa nel formato x,y,..z ComboElement restituisce un arrayList di ComboElement 
	public static ArrayList<ComboElement> getListaCEFromString(String listaStr, String separatore){
		
		ArrayList<ComboElement> listaCE = new ArrayList<ComboElement>();
		
		if (listaStr != null && !listaStr.equals("")){
		
			ComboElement ce;
			String[] arrStr = listaStr.split(separatore);
			if(arrStr != null && arrStr.length>0){
				for(int i=0;i<arrStr.length;i++){
					ce = new ComboElement(); 
					ce.setId(arrStr[i]);
					ce.setDescription(arrStr[i]);
					listaCE.add(ce);
				}
			}
		
		}
		
		return listaCE;
	}
	
	
	public static String removeAllSpecialChar( String s ){
		StringBuffer sb = new StringBuffer( removeAllSpecialLecter( s ) );
			
		singleReplace(sb,"/", "");
		singleReplace(sb,"\\\\", "");
		//singleReplace(sb,"\\(", "");
		//singleReplace(sb,"\\)", "");
		//singleReplace(sb,"\\{", "");
		//singleReplace(sb,"\\}", "");
		//singleReplace(sb,"\\[", "");
		//singleReplace(sb,"\\]", "");
		
		return sb.toString();
	}
	
	public static String removeAllSpecialLecter( String s ){
		StringBuffer sb = new StringBuffer(s);
		singleReplace(sb,"�","A");
		singleReplace(sb,"�","A");
		singleReplace(sb,"�","A");
		singleReplace(sb,"�","E");
		singleReplace(sb,"�","E");
		singleReplace(sb,"�","E");
		singleReplace(sb,"&", "e");
		singleReplace(sb,"�", "a");
		singleReplace(sb,"�", "a");
		singleReplace(sb,"�", "a");
		singleReplace(sb,"�", "e");
		singleReplace(sb,"�", "e");
		singleReplace(sb,"�", "e");
		singleReplace(sb,"�", "i");
		singleReplace(sb,"�", "i");
		singleReplace(sb,"�", "i");
		singleReplace(sb,"�", "o");
		singleReplace(sb,"�", "o");
		singleReplace(sb,"�", "o");
		singleReplace(sb,"\\?", "");
		
		return sb.toString();
	}
	
	private static void singleReplace( StringBuffer sb, String regex, String replaceString ){
		
		StringBuffer tmp = new StringBuffer( sb );
		Pattern p = Pattern.compile( regex );
		Matcher m = p.matcher( tmp );
		sb.delete( 0, sb.length() );
		while ( m.find() ){
			m.appendReplacement(sb, replaceString);
		}
		m.appendTail( sb );
	}
	
	public static String subString( String input, int maxLenght ){
		
		if ( input == null || input.length() <= maxLenght ) 
			return input;
		else 
			return input.substring(0, maxLenght - 1 );
	} 
	
}
