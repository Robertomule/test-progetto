package it.eustema.inps.utility.logging;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LoggingConfigurator{
    
    public static final String DEFAULT_CONFIG_LOCAL_PATH = "cfg/log4j.xml";
    
   
    protected static  Log logger = LogFactory.getLog("it.eustema.inps.utility.logging.LoggingConfigurator");
    
    private void logMessage(String message)
    {
    	logger.info(message);
     	System.out.println(message);   	
    }
    
   public void init() {     	
     	init(null);
    }
    
    
    public void init(String internalLocation) {
     	
     	StringBuffer sb = new StringBuffer();       	
    	if ((internalLocation != null) && (internalLocation.length() > 0))
    	{             	
         	sb.append("path specificato: [")
         	  .append(internalLocation)
         	  .append("]");
         	
         	logMessage(sb.toString());            	
    		ConfigurationLoad.loadInternal(internalLocation);
    	}
    	else
    	{
            sb.append("path di default: [")
              .append(DEFAULT_CONFIG_LOCAL_PATH)
              .append("]");
         	
            logMessage(sb.toString());            	
    		ConfigurationLoad.loadInternal(DEFAULT_CONFIG_LOCAL_PATH);
    	}
    }
}
