package it.eustema.inps.utility.logging;

import org.w3c.dom.Document;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.parsers.DocumentBuilderFactory;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Questa classe espone dei metodi statici, che consentono il caricamento del file
 * di configurazione di Log4j.
 * 
 * Il metodo corretto va richiamato una sola volta prima che venga invocato il codice 
 * che usa il framework.
 * 
 */

public class ConfigurationLoad {

	protected static  Log logger = LogFactory.getLog("it.eustema.inps.utility.logging.ConfigurationLoad");
	
	
    private static void logMessage(String message)
    {
    	logger.info(message);
     	System.out.println(message);   	
    }
    
	/**
	 * Il metodo carica il file di configurazione di Log4j (che sia un file XML o un file PROPERTIES)
	 * localizzato al di fuori dell'EAR.
	 * 
	 * 
	 * @param configPath: path assoluto comprensivo del nome del file e della relativa estensione
	 */
    /*
	protected static void loadExternal(final String configPath) {
        
		boolean isXMLConfigFile = (configPath.endsWith(".xml"));

        StringBuffer sb = new StringBuffer();
        
        try {
            if (isXMLConfigFile) {
            	
            	sb.append("ConfigurationLoad - loadExternal --> file di configurazione XML: [")
       	          .append(configPath)
       	          .append("]");
       	
            	logMessage(sb.toString());
            	
            	DOMConfigurator.configure(configPath);	

            } else {
            	
            	sb.append("ConfigurationLoad - loadExternal --> file di configurazione .properties: [")
     	          .append(configPath)
     	          .append("]");
     	
            	logMessage(sb.toString());             	
            	
                PropertyConfigurator.configure(configPath);
            }
        } catch (Exception e) {  
        	logMessage("ConfigurationLoad - loadExternal --> Impossibile inizializzare log4j");
        	//nonCatalogLogger.error("ConfigurationLoad - loadExternal --> Impossibile inizializzare log4j", e);	
        	logger.error("ConfigurationLoad - loadExternal --> Impossibile inizializzare log4j", e);
        }
    }
    */
	
    /*
	protected static void loadExternal(final String configPath, long delay) {
        boolean isXMLConfigFile = (configPath.endsWith(".xml"));

        StringBuffer sb = new StringBuffer();
        
        try {
            if (isXMLConfigFile) {
            	
            	sb.append("ConfigurationLoad - loadExternal --> file di configurazione XML, reload interval: [")
       	          .append(configPath)
       	          .append(", ")
       	          .append(delay)       	          
       	          .append("]");
       	
            	logMessage(sb.toString());
            	
            	DOMConfigurator.configureAndWatch(configPath, delay);

            } else {
            	
            	sb.append("ConfigurationLoad - loadExternal --> file di configurazione .properties, reload interval: [")
     	          .append(configPath)
       	          .append(", ")
       	          .append(delay)        	          
     	          .append("]");
     	
            	logMessage(sb.toString());              	
            	
                PropertyConfigurator.configureAndWatch(configPath, delay);
            }
        } catch (Exception e) {
        	logMessage("ConfigurationLoad - loadExternal --> Impossibile inizializzare log4j");      	
        	//nonCatalogLogger.error("ConfigurationLoad - loadExternal --> Impossibile inizializzare log4j", e);
        	logger.error("ConfigurationLoad - loadExternal --> Impossibile inizializzare log4j", e);
        }
    }	
	*/
	
	protected static void loadInternal(final String configPath) {
       				
        StringBuffer sb = new StringBuffer();       
        try {                        
            	sb.append("ConfigurationLoad :: loadInternal --> file di configurazione XML: [")
         	      .append(configPath)
         	      .append("]");
         	
            	logMessage(sb.toString());
            	
//                InputStream log4JConfig = new FileInputStream(configPath);
//                Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(log4JConfig);
//                DOMConfigurator.configure(doc.getDocumentElement());
                
                PropertyConfigurator.configureAndWatch(configPath);
             
        } catch (Exception e) {
 
        	logMessage("ConfigurationLoad - loadInternal --> Impossibile inizializzare log4j");
        	logger.error("ConfigurationLoad - loadInternal --> Impossibile inizializzare log4j", e);
        }
    }

}
