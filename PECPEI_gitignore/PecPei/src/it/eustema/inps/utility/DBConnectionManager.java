package it.eustema.inps.utility;

import java.sql.Connection;
import java.sql.DriverManager;
//import java.util.Hashtable;
//import javax.naming.Context;
//import javax.naming.InitialContext;
//import javax.sql.DataSource;

import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DBConnectionManager {

	private static final Log log = LogFactory.getLog(DBConnectionManager.class);
	private static DBConnectionManager cm = null;
	//DataSource ds_PECPEI = null;
	//DataSource ds_HERMES = null;
		
	/*private DBConnectionManager(String jndi){
		ds_PECPEI = lookup(jndi); 
	}*/
	
	private DBConnectionManager() throws Exception{
	
		// registra il driver
		// NB: non necessario chiamare il metodo Class.forName per registrare il driver se si utilizza la libreria di classi sqljdbc4.jar
		Class.forName(ConfigProp.jdbcDriver);     
	}	
	
	public static DBConnectionManager getInstance(){
		
		if(cm == null)
			try {
				cm = new DBConnectionManager();
			} catch (Exception e) {				
				e.printStackTrace();
				log.error("getInstance :: Errore durante il caricamento del driver per la connessione al DB: " + e.getMessage());
				//System.err.println("Errore durante il caricamento del driver per la connessione al DB: " + e.getMessage());
			}
		
		return cm;
	}
	
	
	public Connection getConnection(String dbName) throws Exception{		
		Connection conn = null;
		try{
			if(dbName!=null){
				if(dbName.equals(Constants.DB_PECPEI_PASSW))
					conn = DriverManager.getConnection(ConfigProp.dbPecPeiConnectionUrlPass, ConfigProp.usrPecPei, ConfigProp.pwdPecPeiPassw);
				else if(dbName.equals(Constants.DB_PECPEI) && !ConfigProp.switchDbPecPei)
					conn = DriverManager.getConnection(ConfigProp.dbPecPeiConnectionUrl, ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
				else if(dbName.equals(Constants.DB_PECPEI) && ConfigProp.switchDbPecPei)//se il flag � true va sul database di backup
					conn = DriverManager.getConnection(ConfigProp.dbPecPeiBckConnectionUrl, ConfigProp.usrPecPeiBck, ConfigProp.pwdPecPeiBck);
				else if(dbName.equals(Constants.DB_HERMES))
					conn = DriverManager.getConnection(ConfigProp.dbHermesConnectionUrl, ConfigProp.usrHermes, ConfigProp.pwdHermes);
				else if(dbName.equals(Constants.DB_VERIFICAPEC))
					conn = DriverManager.getConnection(ConfigProp.dbPecVerificaConnectionUrl, ConfigProp.usrVerificaPec, ConfigProp.pwdVerficaPec);
				else 
					conn = DriverManager.getConnection(ConfigProp.dbCommonConnectionUrl, ConfigProp.usrPecPei, ConfigProp.pwdPecPei);

			}
			else
				throw new Exception("DBConnectionManager :: Impossibile ottenere una connessione: DB non specificato");
		}
		catch(Exception e){
			log.error("Impossibile ottenere una connessione al DB: " + e.getMessage());
			System.out.println("Impossibile ottenere una connessione al DB "+e.getMessage());
			e.printStackTrace();
			throw e;
		}		
//		log.info("getConnection :: connection=" + ConfigProp.dbPecPeiConnectionUrl +"--"+ConfigProp.usrPecPei +"--"+ ConfigProp.pwdPecPei);
//		log.debug("getConnection :: connection=" + conn);		
		return conn;
	}
	
	/*
	private DataSource lookup(String jndi){
		
		System.out.println("DBConnectionManager :: lookup");
		try{			
			Hashtable<String, String> environment = new Hashtable<String, String>();  
			environment.put(Context.INITIAL_CONTEXT_FACTORY, ConfigProp.initialContextFactory);  
			environment.put(Context.PROVIDER_URL, ConfigProp.providerUrl);
			
			Context ctx = new InitialContext(environment);			
			//Context ctx = new InitialContext();			
			
			ds_PECPEI = (DataSource) ctx.lookup(jndi);
			ctx.close();			
			System.out.println("DBConnectionManager :: eseguita lookup :: ds=" + ds_PECPEI);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return ds_PECPEI;
	}
	*/
}
