package it.eustema.inps.hermes.dao;


import it.eustema.inps.hermes.dto.ComboElement;
import it.eustema.inps.hermes.dto.Funzionalita;
import it.eustema.inps.hermes.dto.GAUtente;
import it.eustema.inps.hermes.dto.Sede;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import asconnection.connectionpool.WSConnect;
import asconnection.interfaccia.ApplicationServerDriver;




public class ComboDAO {
	

	
	private static final String TIPOLOGIA_FIELD_ID = "id_tipologia";
	private static final String TIPOLOGIA_FIELD_DESC = "descrizione";
	
	private static final String CLASSIFICAZIONE_FIELD_ID = "id_classificazione";
	private static final String CLASSIFICAZIONE_FIELD_DESC = "descrizione";

	private static final String LISTEPRED_FIELD_ID = "id_listePredefinite";
	private static final String LISTEPRED_FIELD_DESC = "descrizione";
	
	private static final String LISTADISTUT_FIELD_ID = "id_ListaDistribuzione";
	private static final String LISTADISTUT_FIELD_DESC = "descrizione";
	
	private static final String DESTLP_FIELD_ID = "codiceSede";
	private static final String DESTLP_FIELD_DESC = "descrSede";
	
	private static final String DBC_FIELD_UFF_DESC_ID = "descrizione";
	private static final String DBC_FIELD_UFF_DESC_NOM = "descrNom";
	private static final String DBC_FIELD_UFF_DESC_MATRICOLA = "Matricola";
	private static final String DBC_FIELD_UFF_DESC_NOMINATIVO= "Nominativo";
	
	private static final String DBC_FIELD_CODICE_SEDE = "Codice";
	private static final String DBC_FIELD_DESCRIZIONE_SEDE = "DescrSede";
	
	private boolean abilitaSystemOut = false;
	
	public ArrayList<ComboElement> selectIndirizziPatronati(String strWhereCondition) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			

			String query = "SELECT email" +
				" FROM configurazione_patronati " +
				" WHERE " + strWhereCondition ;
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString("email"));
					ce.setDescription(rs.getString("email"));
					
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectTipologia(String idNotIn) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			

			String query = "SELECT *" +
				" FROM tipologia";
			if(idNotIn!=null && !idNotIn.equals("")){
				query += " WHERE id_tipologia NOT IN ("+idNotIn+")";
			}
			
			
			pstmt = wsc.getConnection().prepareStatement(query);
//			pstmt.setString(1, matricola);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(((Integer)rs.getInt((TIPOLOGIA_FIELD_ID))).toString());
					ce.setDescription(rs.getString(TIPOLOGIA_FIELD_DESC));
					listaCombo.add(ce);
			    }
			}
			
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}

	
	public ArrayList<ComboElement> selectClassificazione() throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			

			String query = "SELECT *" +
				" FROM classificazione";
			
			
			pstmt = wsc.getConnection().prepareStatement(query);
//			pstmt.setString(1, matricola);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(((Integer)rs.getInt((CLASSIFICAZIONE_FIELD_ID))).toString());
					ce.setDescription(rs.getString(CLASSIFICAZIONE_FIELD_DESC));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectListePredefinite() throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			

			String query = "SELECT id_listePredefinite, descrizione" +
					" FROM liste_predefinite"+
					" ORDER BY ordinamento";
			
			
			pstmt = wsc.getConnection().prepareStatement(query);

			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
//					ce.setId(((Integer)rs.getInt((LISTEPRED_FIELD_ID))).toString());
					ce.setId(rs.getString(LISTEPRED_FIELD_DESC));
					ce.setDescription(rs.getString(LISTEPRED_FIELD_DESC));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectVisualizzaPer(String p_nomeutente,int flagTipo, boolean isLD) throws Exception
	{
		ArrayList<ComboElement> comboVisualizza = null;
		ComboElement ce;
		
//		comboVisualizza.add(new ComboElement("1","Liste personali di: " + p_nomeutente));
//		comboVisualizza.add(new ComboElement("2","Liste personali della Sede"));
//		comboVisualizza.add(new ComboElement("3","Liste Predefinite"));
//		comboVisualizza.add(new ComboElement("4","Codice Sede"));
//		comboVisualizza.add(new ComboElement("5","Descrizione Sede"));
//		comboVisualizza.add(new ComboElement("6","Ricerca Manuale"));
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT menu, link" +
					" FROM funzionalita" +
					" WHERE flagTipo = ?";
			
			//parametro passato in caso di form Lista Distribuzione per il quale non servono le opzioni 1 e 2
			if (isLD){
				query = query + " AND link NOT IN (1,2)";
			}
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, flagTipo);
			
			rs  = pstmt.executeQuery();
			if(rs!=null){
				comboVisualizza = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();
					ce.setId(rs.getString("link"));
					if(rs.getString("link").equals("1")){
						ce.setDescription(rs.getString("menu") + " " + p_nomeutente);
					}else{
						ce.setDescription(rs.getString("menu"));
					}
					
					
					comboVisualizza.add(ce);
			    }
			}
			
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		
		
		return comboVisualizza;
	}
	
	public ArrayList<ComboElement> selectDestDaCodiceSede(ArrayList<String> listIdToAdd) throws Exception
	{
		//Dato 1 o pi� codice_sedi eseguo la query su common e ritorno codici e descrizione Sede
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			

			String query = "SELECT codiceSede, descrSede"+
				  " FROM liste_predefinite_sedi"+
				  " WHERE chiaveDescr in ";
			
			String interval="(";
			if(listIdToAdd.size()>0){
				Iterator it = listIdToAdd.iterator();
				while(it.hasNext()){
					interval = interval+(String)it.next()+",";
				}
				//"(10,18,24,"
				interval = interval.substring(0,interval.length()-1);
			}
			query = query + interval + ")"+ " ORDER BY codiceSede";
			
			if(abilitaSystemOut)
				System.out.println("***selectDestDaCodiceSede = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
//			pstmt.setInt(1,listIdToAdd);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DESTLP_FIELD_ID));
					ce.setDescription(rs.getString(DESTLP_FIELD_DESC));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	


	public ArrayList<ComboElement> selectDestListaPredSel(ArrayList<String> listaPredSel,String codSedeHermes, String codiceNotIn) throws Exception
	{
		WSConnect wsc = null;
		WSConnect wsCommon = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = new ArrayList<ComboElement>();
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT codiceSede, descrSede"+
				  " FROM liste_predefinite_sedi";
				  
			if(listaPredSel!=null && listaPredSel.size()>0){
				query += " WHERE chiaveDescr IN ";
				String interval = "(";
				Iterator it = listaPredSel.iterator();
				String chiaveDescr= "";
				
				boolean contieneSediRegionali = false;
				while(it.hasNext()){
					chiaveDescr = (String)it.next();
					if(chiaveDescr.equals("ALL")){
//						interval += "'STRUTTURE CENTRALI', 'ABRUZZO','BASILICATA','CALABRIA','CAMPANIA','EMILIA ROMAGNA','FRIULI VENEZIA GIULIA','LAZIO','LIGURIA','LOMBARDIA','MARCHE','MOLISE','PIEMONTE','PUGLIA','SARDEGNA','SICILIA','TOSCANA','TRENTINO ALTO ADIGE','UMBRIA','VALLE D''AOSTA','VENETO',";
						try{
							wsCommon = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
							String queryAll = "select distinct Codice4 as codiceSede ,Codice4+\'/\'+DescrSede as descrSede from Common.dbo.MetaprocessoSediAnalitiche  where not Codice4 is null and not  TipoDirezione is null ";
							if(codiceNotIn!=null && !codiceNotIn.equals("")){
								queryAll += " AND codiceSede NOT IN ("+codiceNotIn+")";
							}
							queryAll += "ORDER BY codiceSede";
							System.out.println("queryAll: "+queryAll);
							pstmt = wsc.getConnection().prepareStatement(queryAll);
	//						pstmt.setInt(1,listIdToAdd);
							rs  = pstmt.executeQuery();
							if(rs!=null){
								while (rs.next()) {
									ce = new ComboElement();				
									ce.setId(rs.getString(DESTLP_FIELD_ID));
									ce.setDescription(rs.getString(DESTLP_FIELD_DESC));
									listaCombo.add(ce);
							    }
							}
							rs.close();
							pstmt.close();
							wsCommon.close();
						}catch(Exception e){
							e.printStackTrace();
						}
					}else if(chiaveDescr.equals("DIREZIONI CENTRALI e SEDI REGIONALI")){
						contieneSediRegionali = true;
						interval += "'SEDI REGIONALI',";
					}else if(chiaveDescr.equals("SEDI")){
						interval += "'ABRUZZO','BASILICATA','CALABRIA','CAMPANIA','EMILIA ROMAGNA','FRIULI VENEZIA GIULIA','LAZIO','LIGURIA','LOMBARDIA','MARCHE','MOLISE','PIEMONTE','PUGLIA','SARDEGNA','SICILIA','TOSCANA','TRENTINO ALTO ADIGE','UMBRIA','VALLE D''AOSTA','VENETO',";
					}else if(chiaveDescr.contains("SEDI REGIONALI")){
						contieneSediRegionali = true;
						interval += "'" + chiaveDescr + "',";
					}else{
						interval += "'" + chiaveDescr + "',";
					}
				}
				//"es. interval = ('ABRUZZO','BASILICATA'"
				interval = interval.substring(0,interval.length()-1);
				query +=  interval + ")";
				
				if(codiceNotIn!=null && !codiceNotIn.equals("")){
					query += " AND codiceSede NOT IN ("+codiceNotIn+")";
				}
				
				
				if(codSedeHermes.substring(0,2).equals("00") && 
						contieneSediRegionali){
					query += " UNION SELECT codiceSede, descrSede"+
							" FROM liste_predefinite_sedi" +
							" WHERE chiaveDescr = ('DIREZIONI CENTRALI')";
				}
				
				query += " ORDER BY descrSede";
			}
			
			if(abilitaSystemOut)
				System.out.println("***selectDestListaPredSel = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
//			pstmt.setInt(1,listIdToAdd);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DESTLP_FIELD_ID));
					ce.setDescription(rs.getString(DESTLP_FIELD_DESC));
					listaCombo.add(ce);
			    }
			}
		}
		catch( Exception eh )
		{
			
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectInoltraUffici(Integer idSede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT DISTINCT [Matricola]  ,[Nominativo]"+
				" FROM [Common].[dbo].[MetaprocessoComponentiAnalitico]"+
				" WHERE [Codice Sede] = ? AND [MTPDepartmentChef]=1"+
				" ORDER BY Nominativo";
			
			if(abilitaSystemOut)
				System.out.println("***selectInoltraUffici = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1,idSede);
			
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DBC_FIELD_UFF_DESC_MATRICOLA));
					ce.setDescription(rs.getString(DBC_FIELD_UFF_DESC_NOMINATIVO));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectNomiByMatricola(Integer idSede, String elencoMatricole) throws Exception
	{
		//data una lista matricole separata da ,
		//ritorna l'elenco dei nomi utente da common
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT DISTINCT [Matricola]  ,[Nominativo]"+
				" FROM [Common].[dbo].[MetaprocessoComponentiAnalitico]"+
				//" WHERE [Codice Sede] = ? AND [MTPDepartmentChef]=1"+
				" WHERE [Codice Sede] = ?"+
				" AND [Matricola] IN (" + elencoMatricole + ")"+
				//" AND [Matricola] IN (?)"+
				" ORDER BY Nominativo";
			
			if(abilitaSystemOut)
				System.out.println("***selectNomiByMatricola = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1,idSede);
			//pstmt.setString(2,elencoMatricole);
			
			rs  = pstmt.executeQuery();
			
			listaCombo = new ArrayList<ComboElement>();
			
			if(rs!=null){
				
				while (rs.next()) {
					ce = new ComboElement();
					if(abilitaSystemOut)
						System.out.println("***output = " + rs.getString(DBC_FIELD_UFF_DESC_NOMINATIVO));
					ce.setId(rs.getString(DBC_FIELD_UFF_DESC_NOMINATIVO));
					ce.setDescription(rs.getString(DBC_FIELD_UFF_DESC_NOMINATIVO));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectNomiByMatricole(String elencoMatricole) throws Exception
	{
		//data una lista matricole separata da ,
		//ritorna l'elenco dei nomi utente da common
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT DISTINCT [Matricola]  ,[Nominativo]"+
			" FROM [Common].[dbo].[MetaprocessoComponentiAnalitico]"+
			" WHERE [Matricola] IN (" + elencoMatricole + ")"+
			" ORDER BY Nominativo";
			
			if(abilitaSystemOut)
				System.out.println("***selectNomiByMatricole = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			rs  = pstmt.executeQuery();
			
			listaCombo = new ArrayList<ComboElement>();
			
			if(rs!=null){
				
				while (rs.next()) {
					ce = new ComboElement();
					//System.out.println("***output = " + rs.getString(DBC_FIELD_UFF_DESC_NOMINATIVO));
					ce.setId(rs.getString(DBC_FIELD_UFF_DESC_NOMINATIVO));
					ce.setDescription(rs.getString(DBC_FIELD_UFF_DESC_NOMINATIVO));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	
	
	public ArrayList<ComboElement> selectDirigenteArea(Integer idSede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Nominativo],"+
				" [descrizione], [descrizione] + ' [' + [Nominativo] + ']' as descrNom"+        
				" FROM [Common].[dbo].[MetaprocessoComponentiAnalitico]"+
				" WHERE [Codice Sede] = ? AND [MTPDepartmentChef]=1 AND [descrizione] is not NULL "+
				" ORDER BY descrizione";
			
			if(abilitaSystemOut)
				System.out.println("***selectDirigenteArea = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1,idSede);
			
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DBC_FIELD_UFF_DESC_NOM).replaceAll("�", "'"));
					ce.setDescription(rs.getString(DBC_FIELD_UFF_DESC_NOM).replaceAll("�", "'"));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectUtentiCommonBySede(String idSede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT DISTINCT [Matricola],[Nominativo]"+
				" FROM [Common].[dbo].[MetaprocessoComponentiAnalitico]"+
				" WHERE [Codice Sede] = ? "+
				" ORDER BY Nominativo";
			
			if(abilitaSystemOut)
				System.out.println("***selectUtentiCommonBySede = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1,idSede);
			
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString("Matricola"));
					ce.setDescription(rs.getString("Nominativo"));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	
	public ArrayList<ComboElement> selectUffici(Integer idSede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Nominativo],"+
				" [descrizione], [descrizione] + ' [' + [Nominativo] + ']' as descrNom"+        
				" FROM [Common].[dbo].[MetaprocessoComponentiAnalitico]"+
				" WHERE [Codice Sede] = ? AND [MTPDepartmentChef]=1 AND [descrizione] is not NULL "+
				" ORDER BY descrizione";
			
			if(abilitaSystemOut)
				System.out.println("***selectUffici = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1,idSede);
			
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DBC_FIELD_UFF_DESC_ID).replaceAll("�", "'"));
					ce.setDescription(rs.getString(DBC_FIELD_UFF_DESC_NOM).replaceAll("�", "'"));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectUfficiUtentiFC(Integer idSede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Nominativo],"+
				" [descrizione], [Matricola], [descrizione] + ' [' + [Nominativo] + ']' as descrNom"+        
				" FROM [Common].[dbo].[MetaprocessoComponentiAnalitico]"+
				" WHERE [Codice Sede] = ? AND [MTPDepartmentChef]=1"+
				" ORDER BY descrizione";
			
			if(abilitaSystemOut)
				System.out.println("***selectUfficiUtentiFC = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1,idSede);
			
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString("Matricola"));
					ce.setDescription(rs.getString(DBC_FIELD_UFF_DESC_NOM));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectListaSedi00() throws Exception
	{
		//Seleziona la lista di tutte le sedi da MetaprocessoSediAnalitiche

		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Codice], [DescrSede], Codice + '/' + DescrSede AS descrizione" +       
			" FROM [Common].[dbo].[MetaprocessoSediAnalitiche]"+
			" WHERE  (LEN(Codice) = 4)" + 
			" AND Codice LIKE '00%'";
			
			query += " ORDER BY Codice";
			
			if(abilitaSystemOut)
				System.out.println("***selectListaSedi00 = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			//pstmt.setInt(1,idSede);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DBC_FIELD_CODICE_SEDE));
					ce.setDescription(rs.getString("descrizione"));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectListaSediInNotIn(String codiceIn, String codiceNotIn) throws Exception
	{
		//Seleziona la lista di tutte le sedi da MetaprocessoSediAnalitiche
		//usando come parametri IN e NOT IN

		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Codice], [DescrSede], Codice + '/' + DescrSede AS descrizione" +       
			" FROM [Common].[dbo].[MetaprocessoSediAnalitiche]"+
			" WHERE  (LEN(Codice) = 4)";
			if(codiceIn!=null && !codiceIn.equals("")){
				query += " AND [Codice] IN (" + codiceIn + ")";
			}
			if(codiceNotIn!=null && !codiceNotIn.equals("")){
				query += " AND [Codice] NOT IN (" + codiceNotIn + ")";
			}
			
			query += " ORDER BY Codice";
			
			if(abilitaSystemOut)
				System.out.println("***selectListaSediInNotIn = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			//pstmt.setInt(1,idSede);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DBC_FIELD_CODICE_SEDE));
					ce.setDescription(rs.getString("descrizione"));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectListaSedi(String codiceNotIn) throws Exception
	{
		//Seleziona la lista di tutte le sedi da MetaprocessoSediAnalitiche

		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Codice], [DescrSede], Codice + '/' + DescrSede AS descrizione" +       
			" FROM [Common].[dbo].[MetaprocessoSediAnalitiche]"+
			" WHERE  (LEN(Codice) = 4)";
			if(codiceNotIn!=null && !codiceNotIn.equals("")){
				
				//Ulteriore controllo
				//Controllo se NOT IN arriva gia con le parentesi
				//
				if(codiceNotIn.indexOf("(")>=0){
					query += " AND [Codice] NOT IN " + codiceNotIn + "";
				}else query += " AND [Codice] NOT IN (" + codiceNotIn + ")";
				
				
			}
			query += " ORDER BY Codice";
			
			if(abilitaSystemOut)
				System.out.println("***selectListaSedi = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			//pstmt.setInt(1,idSede);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DBC_FIELD_CODICE_SEDE));
					ce.setDescription(rs.getString("descrizione").replaceAll("�", "'"));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectListaSedi_Description_Uguale_Id(String codiceNotIn) throws Exception
	{
		//Seleziona la lista di tutte le sedi da MetaprocessoSediAnalitiche
		//mette l'id della combo uguale alla descrizione sede nel formato Codice/Descrizione

		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Codice], [DescrSede], Codice + '/' + DescrSede AS descrizione" +       
			" FROM [Common].[dbo].[MetaprocessoSediAnalitiche]"+
			" WHERE  (LEN(Codice) = 4)";
			if(codiceNotIn!=null && !codiceNotIn.equals("")){
				
				//Ulteriore controllo
				//Controllo se NOT IN arriva gia con le parentesi
				//
				if(codiceNotIn.indexOf("(")>=0){
					query += " AND [Codice] NOT IN " + codiceNotIn + "";
				}else query += " AND [Codice] NOT IN (" + codiceNotIn + ")";
				
				
			}
			query += " ORDER BY Codice";
			
			if(abilitaSystemOut)
				System.out.println("***selectListaSedi = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			//pstmt.setInt(1,idSede);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString("descrizione"));
					ce.setDescription(rs.getString("descrizione"));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectListaSediDescr(String codiceNotIn) throws Exception
	{
		//Seleziona la lista di tutte le sedi da MetaprocessoSediAnalitiche

		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Codice], [DescrSede]" +       
			" FROM [Common].[dbo].[MetaprocessoSediAnalitiche]"+
			" WHERE  (LEN(Codice) = 4)";
			if(codiceNotIn!=null && !codiceNotIn.equals("")){
				query += " AND [Codice] NOT IN" + codiceNotIn;
			}
			query += " ORDER BY DescrSede";
			
			if(abilitaSystemOut)
				System.out.println("***selectListaSediDescr = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			//pstmt.setInt(1,idSede);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DBC_FIELD_CODICE_SEDE));
					ce.setDescription(rs.getString(DBC_FIELD_DESCRIZIONE_SEDE));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public String selectDescSede_byCod(String codSede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	
		String descSede;
		descSede = "";
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Codice], [DescrSede], Codice + '/' + DescrSede AS descrizione" + 
			" FROM [Common].[dbo].[MetaprocessoSediAnalitiche]"+
				  " WHERE (LEN(Codice) = 4) AND Codice = ? ";
			
			if(abilitaSystemOut)
				System.out.println("***selectDescSede_byCod = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1,codSede);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				
				while (rs.next()) {
								
					descSede = rs.getString("descrizione");
					
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				rs.close();
				pstmt.close();
		        wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		
		return descSede;
	}
	
	public ArrayList<ComboElement> selectSedi_Cod_Desc_da_Array(ArrayList<String> list_str_ToAdd) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT [Codice], [DescrSede], Codice + '/' + DescrSede AS descrizione" + 
			" FROM [Common].[dbo].[MetaprocessoSediAnalitiche]"+
				  " WHERE (LEN(Codice) = 4) AND Codice in ";
			
			String interval="(";
			if(list_str_ToAdd.size()>0){
				Iterator it = list_str_ToAdd.iterator();
				while(it.hasNext()){
					interval = interval+(it.next()).toString()+",";
				}
				
				interval = interval.substring(0,interval.length()-1);
			}
			query = query + interval + ")"+ " ORDER BY Codice";
			
			if(abilitaSystemOut)
				System.out.println("***selectSedi_Cod_Desc_da_Array = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
//			pstmt.setInt(1,listIdToAdd);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DBC_FIELD_CODICE_SEDE));
					ce.setDescription(rs.getString("descrizione"));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				rs.close();
				pstmt.close();
		        wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	

	public ArrayList<ComboElement> selectDestDaCodSede(String listIdIn, String codSedeHermes) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT  [Codice],[DescrSede]"+
				" FROM [Common].[dbo].[MetaprocessoSediAnalitiche]";
				  
			if(listIdIn!=null && !listIdIn.equals("")){
				query += " WHERE [Codice] NOT IN ('" + codSedeHermes + "') " +
						"AND [Codice] IN "+ listIdIn;
			}
			
			query += " ORDER BY [Codice]";
			
			if(abilitaSystemOut)
				System.out.println("***selectDestDaCodSede = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(DBC_FIELD_CODICE_SEDE));
					ce.setDescription(ce.getId() + "/" +
									  rs.getString(DBC_FIELD_DESCRIZIONE_SEDE));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	
	public ArrayList<ComboElement> selectListePersUtente(String matricola, String codiceSede) throws Exception
	{
		//Dato 1 o pi� codice_sedi eseguo la query su common e ritorno codici e descrizione Sede
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			

			String query = "SELECT id_ListaDistribuzione, descrizione"+
				  " FROM lista_distribuzione"+
				  " WHERE id_matricola = ?" +
				  " AND codiceSede = ?" +
				  " ORDER BY descrizione";
			
			if(abilitaSystemOut)
				System.out.println("***selectListePersUtente *** = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1,matricola);
			pstmt.setString(2,codiceSede);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString(LISTADISTUT_FIELD_ID));
					ce.setDescription(rs.getString(LISTADISTUT_FIELD_DESC));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectDestDaSedeUtente(Integer idLD, String matricola, String codiceSede) throws Exception
	{
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			

			String query = "SELECT id_sede,descrSede" +
					  " FROM lista_distribuzione" +
					  " INNER JOIN lista_distribuzione_sede ON (lista_distribuzione.id_listaDistribuzione = lista_distribuzione_sede.id_listaDistribuzione)" +
					  " WHERE lista_distribuzione.id_listaDistribuzione = ?" +
					  " AND id_matricola = ?" +
					  " AND codiceSede = ?" +
					  " ORDER BY descrSede";
			
			if(abilitaSystemOut)
				System.out.println("***selectDestDaSedeUtente = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1,idLD);
			pstmt.setString(2,matricola);
			pstmt.setString(3,codiceSede);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString("id_sede"));
					ce.setDescription(rs.getString(DESTLP_FIELD_DESC));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectDestDaLDSede(Integer idLD) throws Exception
	{
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			

			String query = "SELECT id_sede,descrSede" +
					  " FROM lista_distribuzione" +
					  " INNER JOIN lista_distribuzione_sede ON (lista_distribuzione.id_listaDistribuzione = lista_distribuzione_sede.id_listaDistribuzione)" +
					  " WHERE lista_distribuzione.id_listaDistribuzione = ?" +
					  " ORDER BY descrSede";
			
			if(abilitaSystemOut)
				System.out.println("***selectDestDaLDSede = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1,idLD);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString("id_sede"));
					ce.setDescription(rs.getString(DESTLP_FIELD_DESC));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectListePersSede(String pcodiceSede) throws Exception
	{
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			/*Visualizzo tutte
			String query = "SELECT Distinct id_matricola,autore,codiceSede "+
			" FROM lista_distribuzione" +
			" WHERE codiceSede = '" + pcodiceSede + "'";*/
			
			//Visualizza tutte con join tabella utente
			String query = "SELECT Distinct ld.id_matricola,autore,codiceSede "+
			" FROM lista_distribuzione as ld" +
			" INNER JOIN utente AS u ON ld.id_matricola = u.id_matricola" +
			" WHERE ld.codiceSede = '" + pcodiceSede + "'";
			
			if(abilitaSystemOut)
				System.out.println("***selectListePersSede = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				String matricola = "";
				String codiceSede = "";
				
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId("");
					ce.setDescription(rs.getString("autore"));
					listaCombo.add(ce);
					matricola = rs.getString("id_matricola");
					codiceSede = rs.getString("codiceSede");
					
					query = "SELECT id_listaDistribuzione, descrizione" +
						" FROM lista_distribuzione" +
						" WHERE id_matricola=? AND codiceSede = ?" +
						" ORDER BY descrizione";
					
					pstmt = wsc.getConnection().prepareStatement(query);
					
					pstmt.setString(1,matricola);
					pstmt.setString(2,codiceSede);
					
					rs2  = pstmt.executeQuery();
					if(rs2!=null){
						while (rs2.next()) {
							ce = new ComboElement();
							ce.setId(rs2.getString("id_listaDistribuzione"));
							ce.setDescription("--->"+rs2.getString("descrizione"));
							listaCombo.add(ce);
						}
					}
					
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(rs2!=null)
					rs2.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}


	public ArrayList<ComboElement> selectDD() {
		ArrayList<ComboElement> lista = new ArrayList<ComboElement>();
		ComboElement ce;
		for(int i=1;i<32;i++){
			ce = new ComboElement();
			ce.setId(new Integer(i).toString());
			ce.setDescription(ce.getId());
			lista.add(ce);
		}
		return lista;
	}
	public ArrayList<ComboElement> selectMM() {
		ArrayList<ComboElement> lista = new ArrayList<ComboElement>();
		ComboElement ce;
		for(int i=1;i<13;i++){
			ce = new ComboElement();
			ce.setId(new Integer(i).toString());
			ce.setDescription(ce.getId());
			lista.add(ce);
		}
		return lista;
	}
	public ArrayList<ComboElement> selectYY(Integer startAnno, Integer currentAnno ) {
		ArrayList<ComboElement> lista = new ArrayList<ComboElement>();
		ComboElement ce;
		
		while(!currentAnno.equals(startAnno)){
			ce = new ComboElement();
			ce.setId(currentAnno.toString());
			ce.setDescription(ce.getId());
			lista.add(ce);
			currentAnno = currentAnno -1;
		}		
		
		ce = new ComboElement();
		ce.setId(startAnno.toString());
		ce.setDescription(ce.getId());
		lista.add(ce);
		return lista;
	}
	
	
	public ArrayList<ComboElement> selectUtentiFromGruppo(Integer idGruppo,String matricola, String codiceSede) throws Exception
	{
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaCombo = null;
		ComboElement ce = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			

			String query = "SELECT DISTINCT utente.id_matricola, utente.login"+
				" FROM utente_sede_gruppo"+
				" INNER JOIN utente ON utente_sede_gruppo.id_matricola = utente.id_matricola" +
				" WHERE id_gruppo = ?" +
				" AND utente.id_matricola!= ?"+
				" AND id_sede = ?";
			
			
			System.out.println("***selectUtentiFromGruppo = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1,idGruppo);
			pstmt.setString(2, matricola);
			pstmt.setString(3, codiceSede);
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaCombo = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();				
					ce.setId(rs.getString("id_matricola"));
					ce.setDescription(rs.getString("login"));
					listaCombo.add(ce);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaCombo;
	}
	
	public ArrayList<ComboElement> selectGAListaSedi() throws Exception
	{
		//Seleziona la lista delle sedi configurate nel tool di gestione accessi
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaSedi = null;
		ComboElement ce = null;
		
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT id_sede,id_sede + '/' + descrizione AS descrizione"+
			               " FROM sede"+
			               " ORDER BY id_sede";
						   
			
			System.out.println("******** selectSedi = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			
			rs  = pstmt.executeQuery();
			if(rs!=null){
				listaSedi = new ArrayList<ComboElement>();
				
				while (rs.next()) {
					ce = new ComboElement();
					ce.setId(rs.getString("id_sede"));
					ce.setDescription(rs.getString("descrizione"));
					listaSedi.add(ce);
			    }
			}
			
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaSedi;
	}
	
	public ArrayList<ComboElement> selectGAListaGruppi(String notIn) throws Exception
	{
		//Seleziona la lista dei gruppi nel tool di gestione accessi
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaSedi = null;
		ComboElement ce = null;
		
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT id_gruppo,descrizione"+
			               " FROM gruppo";
			
			if (!notIn.equals("")){
				query+= " WHERE id_gruppo NOT IN (" + notIn + ")";
			}
			query+= " ORDER BY id_gruppo";
			
			System.out.println("******** selectGAListaGruppi = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			rs  = pstmt.executeQuery();
			if(rs!=null){
				listaSedi = new ArrayList<ComboElement>();
				
				while (rs.next()) {
					ce = new ComboElement();
					ce.setId(rs.getString("id_gruppo"));
					ce.setDescription(rs.getString("descrizione"));
					listaSedi.add(ce);
			    }
			}
			
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaSedi;
	}
	
	public ArrayList<ComboElement> selectGAListaGruppiInNotIn(String in, String notIn) throws Exception
	{
		//Seleziona la lista dei gruppi nel tool di gestione accessi
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ComboElement> listaSedi = null;
		ComboElement ce = null;
		
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT id_gruppo,descrizione"+
			               " FROM gruppo";
			
			if (!notIn.equals("") && !in.equals("")){
				
				query+= " WHERE id_gruppo NOT IN (" + notIn + ")"+
						" AND id_gruppo IN (" + in + ")";
				
			}else{
				
				if (!in.equals("")){
					
					query+= " WHERE id_gruppo IN (" + in + ")";
					
				}else if (!notIn.equals("")){
					
					query+= " WHERE id_gruppo NOT IN (" + notIn + ")";
					
				}
			}
			
			
			query+= " ORDER BY id_gruppo";
			
			System.out.println("******** selectGAListaGruppiInNotIn = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			rs  = pstmt.executeQuery();
			if(rs!=null){
				listaSedi = new ArrayList<ComboElement>();
				
				while (rs.next()) {
					ce = new ComboElement();
					ce.setId(rs.getString("id_gruppo"));
					ce.setDescription(rs.getString("descrizione"));
					listaSedi.add(ce);
			    }
			}
			
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaSedi;
	}
	
	
	public ArrayList<ComboElement> selectGAUtenteSedeGruppoFiltroSede(String filtroSede) throws Exception {
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		ComboElement ce = null;
		ArrayList<ComboElement> listaGAUtente = null;
		
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT DISTINCT usg.id_matricola, usg.id_sede, u.cognome,u.nome"+
	        " FROM utente_sede_gruppo AS usg" +
			" INNER JOIN utente AS u on u.id_matricola = usg.id_matricola" +
			" WHERE usg.id_sede = '" + filtroSede + "'";
			
			query +=" ORDER BY usg.id_sede, u.cognome";
			
			System.out.println("selectGAUtenteSedeGruppo_filtroSede :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			rs  = pstmt.executeQuery();
			
			
			if(rs!=null){
				
				listaGAUtente = new ArrayList<ComboElement>();
				while (rs.next()) {
					ce = new ComboElement();
					ce.setId(rs.getString("id_matricola"));
					ce.setDescription(rs.getString("cognome") + " " + rs.getString("nome"));
					listaGAUtente.add(ce);
				}
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaGAUtente;
	}
	

}
