package it.eustema.inps.hermes.dao;


import it.eustema.inps.hermes.dto.Gruppo;
import it.eustema.inps.hermes.dto.Sede;
import it.eustema.inps.hermes.dto.Utente;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import asconnection.connectionpool.WSConnect;
import asconnection.interfaccia.ApplicationServerDriver;




public class UtenteDAO {
	
	private static final String DBC_UTENTE_FIELD_NOME = "NOME";
	private static final String DBC_UTENTE_FIELD_COGNOME = "COGNOME";
	private static final String DBC_UTENTE_FIELD_SEDEATTUALE = "SEDEATTUALE";
	private static final String DBC_UTENTE_FIELD_EMAIL = "email";
	
	private static final String UTENTE_FIELD_NOME = "nome";
	private static final String UTENTE_FIELD_COGNOME = "cognome";
	private static final String UTENTE_FIELD_LOGIN = "login";
	private static final String UTENTE_FIELD_EMAIL = "email";
	
	private static final String GRUPPO_FIELD_ID = "id_gruppo";
	private static final String GRUPPO_FIELD_DESC = "descrizione";
	
	private boolean abilitaSystemOut = false;

	/**
	 * @param matricola
	 * @return
	 * @throws Exception
	 */
	public Utente selectUtenteCommonByMatricola(String matricola) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Utente utente = null;
		
		if (matricola == null){
			return utente;
		}
		
		
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
//			String query = "SELECT [MATRANAG]," +
//					" [COGNOME],"+
//					" [NOME],"+
//					" [SEDEATTUALE],"+
//					" [email]"+
//					" FROM [Common].[dbo].[PersonalePlusFull] as utente"+
//					" INNER JOIN [Common].[dbo].[MetaprocessoSediAnalitiche] as sede ON utente.[SEDEATTUALE] = sede.[CODICE]"+
//					" WHERE utente.[MATRANAG] = ?";
			
			String query = "SELECT [MATRANAG]," +
			" [COGNOME],"+
			" [NOME],"+
			" [SEDEATTUALE],"+
			" [email]"+
			" FROM [Common].[dbo].[PersonalePlusFull] "+
			" WHERE [MATRANAG] = ?";
			
			if(abilitaSystemOut){
			System.out.println("selectUtenteCommonByMatricola = "+query);
			System.out.println("matricola = "+matricola);
			}
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, matricola);
			
			rs  = pstmt.executeQuery();
			
			while (rs.next()) {
				utente = new Utente();
				utente.setMatricola(matricola);
				utente.setNome(rs.getString(DBC_UTENTE_FIELD_NOME).trim());
				utente.setCognome(rs.getString(DBC_UTENTE_FIELD_COGNOME).trim());
				utente.setLogin(rs.getString(DBC_UTENTE_FIELD_NOME).trim() + " " + 
								rs.getString(DBC_UTENTE_FIELD_COGNOME).trim());
//				utente.setIdSede(rs.getString(DBC_UTENTE_FIELD_SEDEATTUALE));
				utente.setEmail(rs.getString(DBC_UTENTE_FIELD_EMAIL));
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (Exception e) {
		    	
		    }
		}
		return utente;
	}
	
	public Utente selectUtenteCommonDirettore_bySede(String sede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Utente utente = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBCOMMON,this.getClass().getCanonicalName());
			
			String query = "SELECT Matricola, " +
			" ppf.MATRANAG,"+
			" ppf.email,"+
			" ppf.NOME, ppf.COGNOME"+
			" FROM [Common].[dbo].[MetaprocessoComponentiAnalitico] AS mca"+
			" INNER JOIN [Common].[dbo].[PersonalePlusFull] AS ppf"+
			" ON mca.Matricola = ppf.MATRANAG"+
			" WHERE (mca.[Codice Sede] = ?) AND (mca.MTPDepartmentChef = 1) AND (mca.descrizione LIKE 'direzione')";

			if(abilitaSystemOut)
				System.out.println("****selectUtenteCommonDirettore_bySede**** " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, sede);
			
			rs  = pstmt.executeQuery();
			
			while (rs.next()) {
				utente = new Utente();
				utente.setMatricola(rs.getString("matricola"));
				utente.setNome(rs.getString(DBC_UTENTE_FIELD_NOME).trim());
				utente.setCognome(rs.getString(DBC_UTENTE_FIELD_COGNOME).trim());
				utente.setLogin(rs.getString(DBC_UTENTE_FIELD_NOME).trim() + " " + 
								rs.getString(DBC_UTENTE_FIELD_COGNOME).trim());
//				utente.setIdSede(rs.getString(DBC_UTENTE_FIELD_SEDEATTUALE));
				utente.setEmail(rs.getString(DBC_UTENTE_FIELD_EMAIL));
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (Exception e) {
		    	
		    }
		}
		return utente;
	}

	/**
	 * @param matricola
	 * @return
	 * @throws Exception
	 */
	public Utente selectUtenteByMatricola(String matricola) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Utente utente = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
						
			String query = "   SELECT login, nome, cognome, email" +
				" FROM utente" +
				" WHERE utente.id_matricola = ?";
			
			if(abilitaSystemOut){
			System.out.println("selectUtenteByMatricola = " + query);
			System.out.println("matricola");
			}
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, matricola);
			
			rs  = pstmt.executeQuery();
			
			while (rs.next()) {
				utente = new Utente();
				utente.setMatricola(matricola);
				utente.setLogin(rs.getString(UTENTE_FIELD_LOGIN).trim());
				utente.setNome(rs.getString(UTENTE_FIELD_NOME).trim());
				utente.setCognome(rs.getString(UTENTE_FIELD_COGNOME).trim());
				if (rs.getString(UTENTE_FIELD_EMAIL) != null)
					utente.setEmail(rs.getString(UTENTE_FIELD_EMAIL).trim());
				else
					utente.setEmail("");
				
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return utente;
	}
	
	/**
	 * @param utente
	 * @return rowCount
	 * @throws Exception
	 */
	public int aggiornaUtente(Utente utente) throws Exception
	{
		int rowCount=0;
		int key = -1;
		
		WSConnect wsc = null;
		
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			key = doAggiornaUtente(wsc, utente);
			
//			Controllo se l'utente � configurato nella tabella utente_Sede_gruppo
			key = doSelectCountGruppiByMatricola_e_Sede(wsc, utente);
			
			if (key <= 0){
				//Se l'utente: matricola e id_sede 
				//non � censito nella tabella utente_sede_gruppo
				//lo inizializza con i ruoli base ComponiBozza, Leggi Inviati, Leggi Ricevuti
				Utente utenteDirettore = selectUtenteCommonDirettore_bySede(utente.getIdSede());
				
				if(utenteDirettore!=null && utenteDirettore.getMatricola().equals(utente.getMatricola()))
				{
					for (int i=2;i<13;i++){
						//doAggiornaUtenteSedeGruppo(wsc, utente, i);
						doInsertUtenteSedeGruppo(wsc, utente, i);
					}
				}else{
					for (int i=2;i<5;i++){
						//doAggiornaUtenteSedeGruppo(wsc, utente, i);
						doInsertUtenteSedeGruppo(wsc, utente, i);
					}
				}
			}
			
			wsc.commit();
			
		}
		catch( Exception eh )
		{
		   throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return rowCount;
	}
	
	public int updateUtente(Utente utente) throws Exception
	{
		
		int key = -1;
		
		WSConnect wsc = null;
		
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			key = doAggiornaUtente(wsc, utente);

			wsc.commit();
			
		}
		catch( Exception eh )
		{
		   throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return key;
	}
	
	public int insertUtente(Utente utente,boolean autocensimento) throws Exception
	{
	
		int key = -1;
		WSConnect wsc = null;
		
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());			
			//inizio transazione: wsc ha setAutoCommit(false);
			key = doInsertUtente(wsc, utente);
			
			//Controllo se l'utente � configurato nella tabella utente_Sede_gruppo
			key = doSelectCountGruppiByMatricola_e_Sede(wsc, utente);
			
			if (key <= 0 && autocensimento){
				//Se l'utente: matricola e id_sede 
				//non � censito nella tabella utente_sede_gruppo
				//lo inizializza con i ruoli base ComponiBozza, Leggi Inviati, Leggi Ricevuti
				for (int i=2;i<5;i++){
					//doAggiornaUtenteSedeGruppo(wsc, utente, i);
					doInsertUtenteSedeGruppo(wsc, utente, i);
				}
				
				//Se l'utente � un consolato inserisco anche i ruoli 5,6,7,8,9,10
				if (utente.getTipoUtente() == 3){
					for (int i=5;i<10;i++){
						doInsertUtenteSedeGruppo(wsc, utente, i);
					}
				}
				
				
				
			}
			
			
			wsc.commit();
		}
		catch( Exception eh )
		{
		   wsc.rollback();
		   throw eh ;
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return key;
	}
	
	public int doSelectCountGruppiByMatricola_e_Sede(WSConnect wsc, Utente utente) throws Exception
	{
		
		//Dato una matricola utente e l'id_Sede 
		//ritorna il numero di record trovati
		//nella tabella di configurazione utente_sede_gruppo
		ResultSet rs = null;
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "SELECT count(*) as rownumber" +
		 " FROM utente_sede_gruppo" +
		 " WHERE id_matricola = ?" +
		 " AND id_sede = ?";
		
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		pstmt.setString(1, utente.getMatricola());
		pstmt.setString(2, utente.getIdSede());
		 
		rs  = pstmt.executeQuery();
		if(rs!=null){
			
			while (rs.next()) {
				key = rs.getInt("rownumber");
			}
			
		}
		if(pstmt!=null)
			pstmt.close();
		if(rs!=null)
			rs.close();
		return key;
	}
	
	public int doInsertUtente(WSConnect wsc, Utente utente) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO utente (id_matricola,login,nome,cognome,email,tipo_utente)" +
				" VALUES(?,?,?,?,?,?)";
		pstmt =  wsc.getConnection().prepareStatement(query);
		pstmt.setString(1, utente.getMatricola());
		pstmt.setString(2, utente.getLogin());
		pstmt.setString(3, utente.getNome());
		pstmt.setString(4, utente.getCognome());
		pstmt.setString(5, utente.getEmail());
		pstmt.setInt(6, utente.getTipoUtente());
		pstmt.executeUpdate();
		
		if(pstmt!=null)
			pstmt.close();
		
		return key;
	}
	
	public int doAggiornaUtente(WSConnect wsc, Utente utente) throws Exception
	{
		
		int rowCount=0;
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "UPDATE utente" +
		" SET login = ?," +
		" nome = ?," +
		" cognome = ?," +
		" email = ?" +
		" WHERE id_matricola = ?";
		pstmt =  wsc.getConnection().prepareStatement(query);
		pstmt.setString(1, utente.getLogin());
		pstmt.setString(2, utente.getNome());
		pstmt.setString(3, utente.getCognome());
		pstmt.setString(4, utente.getEmail());
		pstmt.setString(5, utente.getMatricola());
		rowCount = pstmt.executeUpdate();
		
		if(pstmt!=null)
			pstmt.close();
		
		return key;
	}
	
	/**
	 * @param utente
	 * @return rowCount
	 * @throws Exception
	 */
	public int aggiornaUtenteSedeGruppo(Utente utente, Integer idGruppo) throws Exception
	{
		int rowCount=0;
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			rowCount = doAggiornaUtenteSedeGruppo(wsc, utente, idGruppo);
			wsc.commit();
			
		}
		catch( Exception eh )
		{
		   throw( eh );
		}finally{
			try{
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return rowCount;
	}
	
	
	public int doInsertUtenteSedeGruppo(WSConnect wsc, Utente utente, Integer idGruppo) throws Exception
	{
		int rowCount=0;
		
		PreparedStatement pstmt = null;
		 
		String query = "INSERT INTO utente_sede_gruppo (id_matricola, id_sede,id_gruppo)" +
				" values (?,?,?)";
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		pstmt.setString(1, utente.getMatricola());
		pstmt.setString(2, utente.getIdSede());
		pstmt.setInt(3, idGruppo);
		
		rowCount = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return rowCount;
	}
	
	public int doAggiornaUtenteSedeGruppo(WSConnect wsc, Utente utente, Integer idGruppo) throws Exception
	{
		int rowCount=0;
		
		PreparedStatement pstmt = null;
		 
		
		String query = "UPDATE utente_sede_gruppo" +
				" SET id_sede = ?," +
				" id_gruppo = ?" +
				" WHERE id_matricola = ?";
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		pstmt.setString(1, utente.getIdSede());
		pstmt.setInt(2, idGruppo);
		pstmt.setString(3, utente.getMatricola());
		
		rowCount = pstmt.executeUpdate();
		
		if(pstmt!=null)
			pstmt.close();
			
		return rowCount;
	}
	
	
	public ArrayList<Gruppo> selectGruppiByMatricola(String matricola, String idsede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<Gruppo> listaGruppi= null;
		Gruppo gruppo = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT gruppo.id_gruppo,gruppo.descrizione" +
				 " FROM utente_sede_gruppo" +
				 " INNER JOIN gruppo" +
				 " ON gruppo.id_gruppo = utente_sede_gruppo.id_gruppo" +
				 " WHERE utente_sede_gruppo.id_matricola = ?"+
				 " AND utente_sede_gruppo.id_sede = ?";
			
			if(abilitaSystemOut)
				System.out.println("selectGruppiByMatricola = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, matricola);
			pstmt.setString(2, idsede);
			
			rs  = pstmt.executeQuery();
			if(rs!=null){
				listaGruppi = new ArrayList<Gruppo>();
				while (rs.next()) {
					gruppo = new Gruppo();
					gruppo.setIdGruppo(rs.getInt(GRUPPO_FIELD_ID));
					gruppo.setDescrizione(rs.getString(GRUPPO_FIELD_DESC));
					
			        listaGruppi.add(gruppo);
			    }
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaGruppi;
	}
	
	public ArrayList<Gruppo> selectGruppiByMatricolaAdmin(String matricola, String idsede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<Gruppo> listaGruppi= null;
		Gruppo gruppo = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT gruppo.id_gruppo,gruppo.descrizione" +
				 " FROM utente_sede_gruppo" +
				 " INNER JOIN gruppo" +
				 " ON gruppo.id_gruppo = utente_sede_gruppo.id_gruppo" +
				 " WHERE utente_sede_gruppo.id_matricola = ?"+
				 " AND utente_sede_gruppo.id_sede = ?";
			
			
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, matricola);
			pstmt.setString(2, idsede);
			
			gruppo = new Gruppo();
			listaGruppi = new ArrayList<Gruppo>();
			
			gruppo.setIdGruppo(1);
			gruppo.setDescrizione("Amministratore");
			
	        listaGruppi.add(gruppo);
			
			rs  = pstmt.executeQuery();
			if(rs!=null){
				
				while (rs.next()) {
					gruppo = new Gruppo();
					gruppo.setIdGruppo(rs.getInt(GRUPPO_FIELD_ID));
					gruppo.setDescrizione(rs.getString(GRUPPO_FIELD_DESC));
					
			        listaGruppi.add(gruppo);
			    }
				
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaGruppi;
	}
	
	public ArrayList<Sede> selectSediUtenteByMatricola(String matricola) throws Exception
	{
		//Data una matricola prende l'elenco delle sedi configurate
		//ritorna un ArrayList di Sede
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<Sede> listaSedi= null;
		Sede sede = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT DISTINCT usg.id_sede,s.descrizione"+
	        " FROM utente_sede_gruppo AS usg"+
			" INNER JOIN sede AS s on s.id_sede = usg.id_sede"+
			" WHERE usg.id_matricola = '" + matricola + "'";
			
			if(abilitaSystemOut)
				System.out.println("******** selectSediUtenteByMatricola = " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			rs  = pstmt.executeQuery();
			if(rs!=null){
				listaSedi = new ArrayList<Sede>();
				while (rs.next()) {
					sede = new Sede();
					sede.setIdSede(rs.getString("id_sede"));
					sede.setDescriSede(rs.getString("descrizione").replaceAll("�", "'"));
					listaSedi.add(sede);
			    }
			}
			
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaSedi;
	}
	
	public String selectSedeAmministratore(String matricola) throws Exception
	{
		//Ritorna se esiste una matricola con ruolo 1-Amministratore
		//indipendentemente dalla sede
		String result = "";
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT id_sede" +
				 " FROM utente_sede_gruppo" +
				 " WHERE id_matricola = ?"+
				 " AND id_gruppo = ?";
			
			if(abilitaSystemOut)
				System.out.println("selectSedeAmministratore = "+query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, matricola);
			pstmt.setInt(2, Gruppo.GRP_AMMINISTRATORE);
			rs  = pstmt.executeQuery();
			
				while (rs.next()) {
					result = rs.getString("id_sede");
			    }
			
	
		} catch( Exception eh ) {
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public String selectAmministratoreDiSede(String matricola, String idsede) throws Exception
	{
		//Ritorna se esiste una matricola con ruolo 12-Amministratore di sede
		//indipendentemente dalla sede
		String result = "";
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT id_sede" +
				 " FROM utente_sede_gruppo" +
				 " WHERE id_matricola = ?"+
				 " AND id_sede = ?"+
				 " AND id_gruppo = ?";
			
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, matricola);
			pstmt.setString(2, idsede);
			pstmt.setInt(3, Gruppo.GRP_AMMINISTRATOREDISEDE);
			rs  = pstmt.executeQuery();
			
				while (rs.next()) {
					result = rs.getString("id_sede");
			    }
			
	
		} catch( Exception eh ) {
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
}
