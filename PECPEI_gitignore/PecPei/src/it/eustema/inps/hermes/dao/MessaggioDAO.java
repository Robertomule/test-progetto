package it.eustema.inps.hermes.dao;

import it.eustema.inps.hermes.dto.Allegato;
import it.eustema.inps.hermes.dto.ComboElement;
import it.eustema.inps.hermes.dto.Messaggio;
import it.eustema.inps.hermes.dto.MessaggioWS;
import it.eustema.inps.utility.StaticUtil;
//import it.eustema.inps.hermes.ws.BeanInput;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.UUID;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;


import asconnection.connectionpool.WSConnect;
import asconnection.interfaccia.ApplicationServerDriver;




public class MessaggioDAO {
	
	private boolean abilitaSystemOut = false;
	
	public void aggiornaThread(){
		WSConnect wsc = null;
		try{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			String queryInsert = "update logBatch set abilita = 0 where dataCreazione not in (select max(dataCreazione) from logBatch)";
			PreparedStatement pstmt = null;
			pstmt = wsc.getConnection().prepareStatement(queryInsert);
			pstmt.executeUpdate();
			if(pstmt!=null)
				pstmt.close();
			if(wsc!=null)
				wsc.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public boolean verificaThread(String x){
		WSConnect wsc = null;
		boolean result = false;
		if(ApplicationServerDriver.DBHERMES!=null){
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try{
				String queryInsert = " select * from logBatch where idBatch = ? and abilita = 1";
				pstmt = wsc.getConnection().prepareStatement(queryInsert);
				pstmt.setString(1,x);
				rs = pstmt.executeQuery();
				while(rs.next()){
					result = true;
				}
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
			}catch(Exception e){
//				e.printStackTrace();
			}
		}
		return result;
	}
	
	public String insertVerificaThread(){
		String s = UUID.randomUUID().toString();
		WSConnect wsc = null;
		try{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			String queryInsert = " insert into logBatch (  idBatch  ,abilita, dataCreazione) VALUES ( ?,?,?)";
			PreparedStatement pstmt = null;
			pstmt = wsc.getConnection().prepareStatement(queryInsert);
			pstmt.setString(1,s);
			pstmt.setInt(2,1);
			java.sql.Timestamp postedDate = new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis());
			pstmt.setTimestamp(3,postedDate);
			pstmt.executeUpdate();
			if(pstmt!=null)
				pstmt.close();
			if(wsc!=null)
				wsc.close();
			return s;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public ArrayList<Messaggio>  selectAllMessaggiRicevutiConFlagSottoscr(String data) throws Exception
	{
		//La query seguente ritorna un ArrayList<Messaggio> che contiene tutti i messaggi
		//ricevuti che non sono stati elaborati dal tread Sottoscrizioni (flagSottoscrizioni)
		//Usata da: TreadSottoscrizioni
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio msgDTO = null;
		
		ArrayList<Messaggio> listaMsgDTO = null;
		
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			String query = "";
			if(data == null){
			query = "SELECT m.id_messaggio, m.codiceSede, m.Subject, m.Body, m.FlgSottoscrizione, m.DeliveredDate, msd.SendTo" +
			" FROM messaggio AS m INNER JOIN messaggio_sedeDestinatari AS msd ON m.id_messaggio = msd.id_messaggio" +
            " WHERE(m.DeliveredDate IS NOT NULL) AND (m.NroProtocollo <> '') AND (m.flg_Stato = " + Messaggio.FLG_STATO_INVIATO + ")" + 
            " AND (m.FlgSottoscrizione <> '1' OR m.FlgSottoscrizione IS NULL)" +
            " ORDER BY m. codiceSede DESC, m.DeliveredDate DESC";
			}
			else {
			
				query = "SELECT m.id_messaggio, m.codiceSede, m.Subject, m.Body, m.FlgSottoscrizione, m.DeliveredDate, msd.SendTo" +
				" FROM messaggio AS m INNER JOIN messaggio_sedeDestinatari AS msd ON m.id_messaggio = msd.id_messaggio" +
	            " WHERE(m.DeliveredDate IS NOT NULL) AND (m.NroProtocollo <> '') AND (m.flg_Stato = " + Messaggio.FLG_STATO_INVIATO + ")" + 
	            " AND (m.FlgSottoscrizione <> '1' OR m.FlgSottoscrizione IS NULL) AND m.dataCreazione >= '" +data+ "'"+
	            " ORDER BY m. codiceSede DESC, m.DeliveredDate DESC";
			}
			if(abilitaSystemOut)
				System.out.println("selectAllMessaggiRicevutiConFlagSottoscr :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaMsgDTO = new ArrayList<Messaggio>();
				while (rs.next()) {
					msgDTO = new Messaggio();
					msgDTO.setIdMessaggio(rs.getInt("id_messaggio"));
					msgDTO.setCodiceSede(rs.getString("codiceSede"));
					msgDTO.setSendTo(rs.getString("SendTo"));
					
					//Prendo il body senza HTML
					//String plainText = StringEscapeUtils.escapeXml(rs.getString("Body"));
					msgDTO.setBody(rs.getString("Body"));
					
					msgDTO.setSubject(rs.getString("Subject"));
			        listaMsgDTO.add(msgDTO);

				}

			}
			
			wsc.commit();
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaMsgDTO;
	}
	
	public int updateMessaggioFlagSottoscr(WSConnect wsc, int idMsg) throws Exception
	{
		//La query seguente aggiorna il flag Sottoscrizioni dato un id messaggio
		//Usata da: TreadSottoscrizioni
		
		//WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int key= -1;
		
		try
		{
			//wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "UPDATE messaggio" + 
			" SET FlgSottoscrizione = '1' "+
			" WHERE (id_messaggio = " + idMsg + ")";
			
			if(abilitaSystemOut)
				System.out.println("updateMessaggioFlagSottoscr :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			key  = pstmt.executeUpdate();
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(pstmt!=null)
					pstmt.close();
		        //wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return key;
	}//END read()
	
	public Messaggio selectMessaggioById(String idMsg, boolean pubblico, String formMsg) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		System.out.println("id messaggio : +selectMessaggioById");
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
	        
			//Ricavo il messaggio considerando sia gli inviati che i ricevuti
			//(indipendentemente se protocollati o meno)
			//il cui id_messaggio coincide con il parametro
			//e la cui sede utente di sessione fa parte dei messaggi ricevuti o inviati
			String query = "SELECT m.*,convert (VARCHAR, m.PostedDate, 103) as postedDateFormatted, convert (VARCHAR, m.PostedDate, 108) as postedTimeFormatted,CONVERT(VARCHAR(10),m.rif_Data, 103) as Rif_DataFormatted,"+
					  " msd.sendTo,msd.descSedeTo,msd.riepilogoDestinatari,tp.descrizione as tipologiaDesc, cl.descrizione as classificazioneDesc";
			
					if (formMsg!= null && formMsg.equals("FirmaCongiunta")){
							query = query +  " FROM messaggio_fc AS m";
						}else{
							query = query +  " FROM messaggio AS m";
						}
					  
						query = query + " INNER JOIN messaggio_sedeDestinatari AS msD ON (m.id_messaggio = mSD.id_messaggio)" +
					  " LEFT JOIN tipologia AS tp ON (m.id_tipoMessaggio = tp.id_tipologia)"+
					  " LEFT JOIN classificazione as cl ON (cl.id_classificazione = m.id_classificazione)" + 
					  " WHERE m.id_messaggio = ?";
						
					if (formMsg!= null && formMsg.equals("FirmaCongiunta")){
						//Prende il messaggio anche in stato di bozza firma congiunta
					}else{
						//esporto solo i messaggi inviati
						query = query + " AND (m.PostedDate is not null)"+ 
					      " AND m.flg_Stato = " + Messaggio.FLG_STATO_INVIATO;
					}
					  
			
			if (pubblico){
				//aggiungo le condizioni che deve essere inviato, ricevuto e protocollato
				//percui un messaggio pubblico
				query = query + " AND m.DeliveredDate is not null"+
				" AND m.NroProtocollo != ''";
			
			}
			
			if(abilitaSystemOut)
				System.out.println("selectMessaggioById :: " + query);
			
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg);
			
			rs  = pstmt.executeQuery();
//			DateFormat df=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			
			while (rs.next()) {
				messaggio = new Messaggio();
				
				messaggio.setIdMatricola(rs.getString("id_matricola"));
				messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
				messaggio.setIdMessaggioPadre(rs.getInt("id_messaggio_padre"));
				
				messaggio.setCodiceSede(rs.getString("codiceSede"));
				messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
				
				
		        messaggio.setSubject(rs.getString("subject"));
		        messaggio.setBody(StringEscapeUtils.escapeHtml(rs.getString("body")));
		        messaggio.setFlagBodyStorico(rs.getString("flg_BodyStorico"));
		        
		        messaggio.setSendTo(rs.getString("SendTo"));
		        messaggio.setDescSedeTo(rs.getString("descSedeTo"));
		        messaggio.setRiepilogoDestinatari(rs.getString("riepilogoDestinatari"));
		        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
		        messaggio.setTipologiaDesc(rs.getString("tipologiaDesc"));
		        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
		        messaggio.setClassificazioneDesc(rs.getString("classificazioneDesc"));
//		        String s=df.format(rs.getDate("PostedDate"));
//		        messaggio.setPostedDateFormatted(s);
		        messaggio.setPostedDateFormatted(rs.getString("postedDateFormatted")+" "+rs.getString("postedTimeFormatted"));
				messaggio.setPostedDate(rs.getDate("PostedDate"));
				
		        messaggio.setNroProtocollo(rs.getString("nroProtocollo"));
		        messaggio.setSegnatura(rs.getString("segnatura"));
		        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
		        
		        messaggio.setDaInviareExInpdapEnpals(rs.getString("daInviareExInpdapEnpals"));
		        messaggio.setDaInviarePatronati(rs.getString("daInviarePatronati"));
		        
		        messaggio.setPubbProcServizio(rs.getString("pubblicazioneProceduraServizio"));
		        messaggio.setTipoProceduraServizio(rs.getString("tipoProceduraServizio"));
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        messaggio.setNomeProcServ(rs.getString("nomeProceduraServizio"));
		        
		        messaggio.setFlagStato(rs.getInt("flg_Stato"));
		        
		        messaggio.setRifNumero(rs.getString("rif_numero"));
		        messaggio.setRifData2(rs.getString("Rif_DataFormatted"));
		        messaggio.setRifSede(rs.getString("rif_sede"));
		        
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        
		        //codice sede mittente
		        messaggio.setCodiceSede(rs.getString("codiceSede"));
		        //descrizione della sede del mittente
		        messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}//END read()

	public int insertFile(File file,String fileName) throws Exception
	{
		FileInputStream fis = new FileInputStream(file);
		 
		int length = (int) file.length();
		int key = -1;
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "INSERT INTO allegati (nome_file,file_binary) VALUES(?,?)";
			pstmt =  wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, fileName);
			pstmt.setBinaryStream(2, fis, length);
			 
			pstmt.executeUpdate();
			wsc.commit();
			
		}
		catch( Exception eh )
		{
		   throw( eh );
		}finally{
			try{
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return key;
	}//END insert(object)
	
	public Allegato getFile(int key) throws Exception {

		
		String str = "SELECT FileName, Filedata FROM Attachments where IDattachment = "+key;
		WSConnect wsc = new WSConnect(ApplicationServerDriver.DBHERMES, this.getClass().getCanonicalName());
		
         Statement state = wsc.getConnection().createStatement();
         ResultSet rs = state.executeQuery(str);
         if (rs.next())
        {
        	 Allegato attachment = new Allegato();
        	 attachment.setFileName(rs.getString(1));
        	 attachment.setFileData(rs.getBytes(2));
        	 return attachment;
        	 
        }
         if(state!=null)
        	 state.close();
         if(rs!=null)
        	 rs.close();
         
         wsc.commit();
         
         if(wsc!=null)
        	 wsc.close();
		
         return null;
        
	}
	
	public int insertMessaggioFC(Messaggio msgDTO) throws Exception
	{
		//Inserimento firma congiunta
		//Questo metodo va sempre chiamato nella fase di InviaInFirmaCongiunta
		int keyMsg = -1;
		int key_associativa = -1;
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
			if (msgDTO.getIdMessaggio() == null || (msgDTO.getIdMessaggio() <= 0)){
//				 *** ricavo l'identity dell'ultimo record inserito nella tabella messagio ***
				//Non avendo id messaggio significa che ho fatto in invia in firma congiunta
				//da una bozza nuova in composizione e mai salvata
				Vector res = wsc.executeQuery("select IDENT_CURRENT('messaggio')");
				Vector row = (Vector)res.elementAt(0);
				Object temp =  row.elementAt(0);
				msgDTO.setIdMessaggio(Integer.parseInt(temp.toString()));
			}

			keyMsg = doInsertMessaggioFC(wsc, msgDTO);
			
			if (keyMsg != -1) {
				
				if(msgDTO.getElencoFileAllegati()!=null && !msgDTO.getElencoFileAllegati().isEmpty()){
					key_associativa = doInsertAllegatiFC(wsc, msgDTO.getIdMessaggio(), msgDTO.getElencoFileAllegati());
				}
				
				
				wsc.commit();
				
				if(abilitaSystemOut)
					System.out.println("****Commit eseguito****");
			}
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return keyMsg;
	}
	
	public int creaVersioneMessaggioFC(Messaggio msgDTO) throws Exception
	{
		//Inserimento firma congiunta
		//Questo metodo va sempre chiamato nella fase di InviaInFirmaCongiunta
		int keyMsg = -1;
		int key_associativa = -1;
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			keyMsg = doCreaVersioneMessaggioFC(wsc, msgDTO);
			
			//Ricavo l'identity dell'ultimo versioning appena 
			Vector res = wsc.executeQuery("select IDENT_CURRENT('messaggio_fc')");
			Vector row = (Vector)res.elementAt(0);
			Object temp =  row.elementAt(0);
			int id_msg = Integer.parseInt(temp.toString());
			
			if (keyMsg != -1) {
				
				if(msgDTO.getElencoFileAllegati()!=null && !msgDTO.getElencoFileAllegati().isEmpty()){
					key_associativa = doInsertAllegatiFC(wsc, id_msg, msgDTO.getElencoFileAllegati());
				}
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return keyMsg;
	}
	
	public int insertLogFC(int idmsg, String azione, String commento, String matricola, String login) throws Exception
	{
		//Inserimento firma congiunta
		//Questo metodo va sempre chiamato nella fase di InviaInFirmaCongiunta
		int keyMsg = -1;
		int key_associativa = -1;
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			

			keyMsg = doInsertLogFC(wsc, idmsg, azione, commento, matricola, login);
			
			if (keyMsg != -1) {
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit insert log eseguito eseguito****");
			}
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return keyMsg;
	}
	
	
	public int insertMessaggio(Messaggio msgDTO) throws Exception
	{
		int keyMsg = -1;
		int key_associativa = -1;
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			keyMsg = doInsertMessaggio(wsc, msgDTO);
			
			if (keyMsg != -1) {
				
				if(msgDTO.getElencoDestinatari()!=null && !msgDTO.getElencoDestinatari().isEmpty()){
					String[] stringArray = StaticUtil.getStringFromArrayListCE(msgDTO.getElencoDestinatari(),",");
					
					key_associativa = doInsertAssociativaMessaggioSediD(wsc,
																keyMsg,
																stringArray[0],
																stringArray[1],
																msgDTO.getRiepilogoDestinatari());
					
					if(abilitaSystemOut)
					System.out.println("****Key Associativa ::" + key_associativa + "****");
				}
				
				if(msgDTO.getElencoFileAllegati()!=null && !msgDTO.getElencoFileAllegati().isEmpty()){
					key_associativa = doInsertAllegati(wsc, keyMsg, msgDTO.getElencoFileAllegati());
				}
				
				//Firma Congiunta
				if(msgDTO.getSelectDirezioneListFC()!=null && msgDTO.getSelectDirezioneListFC().size()>=0){
					key_associativa = doInsertApprovatoriFirmaCongiunta(wsc, keyMsg, msgDTO);
				}
					
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return keyMsg;
	}
	

	public int updateMessaggio(Messaggio msgDTO) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			result = doUpdateMessaggio(wsc, msgDTO);
			
			if (result != -1) {
				
				//Per fare l'update dei destinatari cancella i dati dall'associativa
				doDeleteAssMsgSediD(wsc, msgDTO.getIdMessaggio());
				//inserisce i destinatari se sono presenti
				if(msgDTO.getElencoDestinatari()!=null && !msgDTO.getElencoDestinatari().isEmpty()){
					String[] stringArray = StaticUtil.getStringFromArrayListCE(msgDTO.getElencoDestinatari(),",");
					int key_associativa = -1;
					key_associativa = doInsertAssociativaMessaggioSediD(wsc,
														msgDTO.getIdMessaggio(),
														stringArray[0],
														stringArray[1],
														msgDTO.getRiepilogoDestinatari());
					
					if(abilitaSystemOut)
					System.out.println("****Key Associativa ::" + key_associativa + "****");
				}
				
				//Per fare l'update cancella gli allegati del messaggio
				doDeleteAllegati(wsc, msgDTO.getIdMessaggio());
				//inserisce gli allegati, se presenti
				if(msgDTO.getElencoFileAllegati()!=null && !msgDTO.getElencoFileAllegati().isEmpty()){
					result = doInsertAllegati(wsc, msgDTO.getIdMessaggio(), msgDTO.getElencoFileAllegati());
				}
				
			    //Per fare l'update cancella gli approvatori del messaggio
				doDeleteFirmaCongiunta(wsc, msgDTO.getIdMessaggio());
				if(msgDTO.getSelectDirezioneListFC()!=null && msgDTO.getSelectDirezioneListFC().size()>=0){
					result = doInsertApprovatoriFirmaCongiunta(wsc, msgDTO.getIdMessaggio(), msgDTO);
				}
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
			}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int updateMessaggio_SbloccaFC(Messaggio msgDTO, boolean esisteVersione) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			result = doUpdateMessaggio_SbloccaFC(wsc, msgDTO, esisteVersione);
			
			if (result != -1) {
				
				//Se esiste una versione allora trasferisco nuovamente anche gli allegati
				
				if (esisteVersione){
//					Per fare l'update cancella gli allegati del messaggio
					doDeleteAllegati(wsc, msgDTO.getIdMessaggio());
					//inserisce gli allegati, se presenti
					if(msgDTO.getElencoFileAllegati()!=null && !msgDTO.getElencoFileAllegati().isEmpty()){
						result = doInsertAllegati(wsc, msgDTO.getIdMessaggio(), msgDTO.getElencoFileAllegati());
					}
				}
				
				
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
			}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int updateMessaggioFC(Messaggio msgDTO) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			result = doUpdateMessaggioFC(wsc, msgDTO);
			
			if (result != -1) {
				
				//Per fare l'update cancella gli allegati del messaggio
				doDeleteAllegatiFC(wsc, msgDTO.getIdMessaggio());
				//inserisce gli allegati, se presenti
				if(msgDTO.getElencoFileAllegati()!=null && !msgDTO.getElencoFileAllegati().isEmpty()){
					result = doInsertAllegatiFC(wsc, msgDTO.getIdMessaggio(), msgDTO.getElencoFileAllegati());
				}
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
			}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int updateMessaggioFC_Autorizza(Messaggio msgDTO, boolean esisteVersione) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			result = doUpdateMessaggioFC_Autorizza(wsc, msgDTO, esisteVersione);
			
			if (result != -1) {
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
			}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int updateMessaggioFC_PrendiInCarico(String idMatricolaPresaInCarico, int idMsg) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			result = doUpdateMessaggioFC_PrendiInCarico(wsc, idMatricolaPresaInCarico, idMsg);
			
			if (result != -1) {
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
			}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int insertApprovatoreFC(Messaggio msgDTO, boolean isApprovatoreDelegato) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			result = doInsertApprovatoreFC(wsc, msgDTO, isApprovatoreDelegato);
			
			if (result != -1) {
				
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
			}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int eliminaApprovatoreFC(Messaggio msgDTO) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			doDeleteApprovatoreFC(wsc, msgDTO);
			
			wsc.commit();
				
			if(abilitaSystemOut)
			System.out.println("****Commit eseguito****");
			
			
			}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int updateApprovatoriFC(Messaggio msgDTO) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			result = doUpdateApprovatoriFC(wsc, msgDTO);
			
			if (result != -1) {
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
			}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int updatePulisciApprovatoriFC(Messaggio msgDTO) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			result = doUpdatePulisciApprovatoriFC(wsc, msgDTO);
			
			if (result != -1) {
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
			}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int updateMessaggioEditAdmin(Messaggio msgDTO) throws Exception
	{
		int result = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			result = doUpdateMessaggioEditAdmin(wsc, msgDTO);
			
			if (result != -1) {
				
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			}
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int doInsertApprovatoriFirmaCongiunta(WSConnect wsc, int key, Messaggio msgDTO) throws Exception
	{
		
		int keyFC = -1;
		PreparedStatement pstmt = null;
		
		ArrayList<ComboElement> t_elenco;
		t_elenco = msgDTO.getSelectDirezioneListFC();
		
		String query = null;
		query = "INSERT INTO approvatori_messaggio_fc (id_messaggio,matricolaDirezione,nominativoDirezione,matricolaDirettore,nominativoDirettore,sedeDettaglio)" +
		" VALUES(?,?,?,?,?,?)";
		pstmt =  wsc.getConnection().prepareStatement(query);
		for(int i=0;i<t_elenco.size();i++){
			
			if(abilitaSystemOut)
			System.out.println("****scrivo approvatore :: ****" + t_elenco.get(i).getDescription());
			
			pstmt.setInt(1, key);
			pstmt.setString(2, msgDTO.getSelectDirezioneListFC().get(i).getId());
			pstmt.setString(3, msgDTO.getSelectDirezioneListFC().get(i).getDescription());
			pstmt.setString(4, msgDTO.getSelectDirettoreListFC().get(i).getId());
			pstmt.setString(5, msgDTO.getSelectDirettoreListFC().get(i).getDescription());
			pstmt.setString(6, msgDTO.getSelectSediListFC().get(i).getDescription());
			pstmt.executeUpdate();
			
			if(abilitaSystemOut)
			System.out.println("****approvatore inserito:: ****");
		}
		
		if(pstmt!=null)
			pstmt.close();
		return keyFC;
	}
	
	public int doInsertMessaggioFC(WSConnect wsc, Messaggio msgDTO) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO messaggio_fc" +
				" (body,subject,id_tipoMessaggio,id_classificazione,flg_Stato,rif_numero,rif_data,rif_sede,daPubblicareSuInternet,daInviareExInpdapEnpals,daInviarePatronati,pubblicazioneProceduraServizio,tipoProceduraServizio,dirigenteAreaProceduraServizio,nomeProceduraServizio,id_matricola,codiceSede,descrSede,ufficioSede,PostedDate,id_matricola_approv_bozza,id_matricola_approv_messaggio,id_matricola_approv_invia,id_messaggio_padre,id_messaggio)" +
	      " SELECT body,subject,id_tipoMessaggio,id_classificazione,flg_Stato,rif_numero,rif_data,rif_sede,daPubblicareSuInternet,daInviareExInpdapEnpals,daInviarePatronati,pubblicazioneProceduraServizio,tipoProceduraServizio,dirigenteAreaProceduraServizio,nomeProceduraServizio,id_matricola,codiceSede,descrSede,ufficioSede,PostedDate,id_matricola_approv_bozza,id_matricola_approv_messaggio,id_matricola_approv_invia,id_messaggio_padre,id_messaggio " +
				" FROM messaggio"+
				" WHERE messaggio.id_messaggio = " + msgDTO.getIdMessaggio().toString();
				
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		
		key = pstmt.executeUpdate();

		return key;
	}
	
	public int doCreaVersioneMessaggioFC(WSConnect wsc, Messaggio msgDTO) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		//Copio il messaggio a firma congiunta inserendo in id_messaggio_rif il valore del padre id_messaggio
		
		String query = "INSERT INTO messaggio_fc" +
				" (id_messaggio_rif,body,subject,id_tipoMessaggio,id_classificazione,flg_Stato,rif_numero,rif_data,rif_sede,daPubblicareSuInternet,pubblicazioneProceduraServizio,tipoProceduraServizio,dirigenteAreaProceduraServizio,nomeProceduraServizio,id_matricola,codiceSede,descrSede,ufficioSede,PostedDate,id_matricola_approv_bozza,id_matricola_approv_messaggio,id_matricola_approv_invia,id_messaggio_padre)" +
	      " SELECT id_messaggio,body,subject,id_tipoMessaggio,id_classificazione,flg_Stato,rif_numero,rif_data,rif_sede,daPubblicareSuInternet,pubblicazioneProceduraServizio,tipoProceduraServizio,dirigenteAreaProceduraServizio,nomeProceduraServizio,id_matricola,codiceSede,descrSede,ufficioSede,PostedDate,id_matricola_approv_bozza,id_matricola_approv_messaggio,id_matricola_approv_invia,id_messaggio_padre " +
				" FROM messaggio"+
				" WHERE messaggio.id_messaggio = " + msgDTO.getIdMessaggio().toString();
				
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		
		key = pstmt.executeUpdate();

		return key;
	}
	

	public int doInsertMessaggio(WSConnect wsc, Messaggio msgDTO) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO messaggio" +
				" (body,subject,id_tipoMessaggio,id_classificazione,flg_Stato,rif_numero,rif_data,rif_sede,daPubblicareSuInternet,pubblicazioneProceduraServizio,tipoProceduraServizio,dirigenteAreaProceduraServizio,nomeProceduraServizio,id_matricola,codiceSede,descrSede,ufficioSede,PostedDate,id_matricola_approv_bozza,id_matricola_approv_messaggio,id_matricola_approv_invia,id_messaggio_padre,abilitaFC,inFirmaCongiunta,daInviareExInpdapEnpals,daInviarePatronati,FromUiDoc)" +
				" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		pstmt =  wsc.getConnection().prepareStatement(query);
		pstmt.setString(1, msgDTO.getBody());
		pstmt.setString(2, msgDTO.getSubject());
		pstmt.setInt(3, msgDTO.getIdTipoMessaggio());
		pstmt.setInt(4, msgDTO.getIdClassificazione());
		pstmt.setInt(5, msgDTO.getFlagStato());
		pstmt.setString(6, msgDTO.getRifNumero());
		
		java.sql.Date rifData = null;
		if(msgDTO.getRifData()!=null){
			rifData = new java.sql.Date(msgDTO.getRifData().getTime());
		}	
		pstmt.setDate(7, rifData);
		pstmt.setString(8,msgDTO.getRifSede());
		pstmt.setString(9, msgDTO.getDaPubbSuInternet());
		pstmt.setString(10, msgDTO.getPubbProcServizio());
		pstmt.setString(11, msgDTO.getTipoProceduraServizio());
		pstmt.setString(12, msgDTO.getDirigenteAreaProcServ());
		pstmt.setString(13, msgDTO.getNomeProcServ());
		
		pstmt.setString(14, msgDTO.getIdMatricola());
		pstmt.setString(15, msgDTO.getCodiceSede());
		pstmt.setString(16, msgDTO.getCodiceSedeDescr());
		pstmt.setString(17, msgDTO.getUfficioSede());
		
//		java.sql.Date postedDate = null;
//		if(msgDTO.getPostedDate()!=null){
//			postedDate = new java.sql.Date(msgDTO.getPostedDate().getTime());
//		}
//		pstmt.setDate(17, postedDate);
		java.sql.Timestamp postedDate = null;
		if(msgDTO.getPostedDate()!=null){
			postedDate = new java.sql.Timestamp(msgDTO.getPostedDate().getTime());
		}
		pstmt.setTimestamp(18, postedDate);
		
		pstmt.setString(19, msgDTO.getIdMatricolaApprovBozza());
		pstmt.setString(20, msgDTO.getIdMatricolaApprovaMsg());
		pstmt.setString(21, msgDTO.getIdMatricolaApprovaInvia());
		if(msgDTO.getIdMessaggioPadre()==null){
			pstmt.setNull(22, java.sql.Types.INTEGER);
		}else{
			pstmt.setInt(22, msgDTO.getIdMessaggioPadre());
		}
		
		pstmt.setString(23, msgDTO.getAbilitaFC());
		pstmt.setString(24, msgDTO.getInFirmaCongiunta());
		
		pstmt.setString(25, msgDTO.getDaInviareExInpdapEnpals());
		pstmt.setString(26, msgDTO.getDaInviarePatronati());
		
		if(msgDTO.getFromUidoc()==null){
			pstmt.setNull(27, java.sql.Types.VARCHAR);
		}else{
			pstmt.setString(27, msgDTO.getFromUidoc());
		}
		
		key = pstmt.executeUpdate();

		// *** ricavo l'identity ***
		Vector res = wsc.executeQuery("select IDENT_CURRENT('messaggio')");
		Vector row = (Vector)res.elementAt(0);
		Object temp =  row.elementAt(0);
		key = Integer.parseInt(temp.toString());
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doUpdateMessaggio(WSConnect wsc, Messaggio msgDTO) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "UPDATE messaggio" +
				" SET body=?,subject=?,id_tipoMessaggio=?,id_classificazione=?," +
				"flg_Stato=?,rif_numero=?,rif_data=?,rif_sede=?,daPubblicareSuInternet=?," +
				"pubblicazioneProceduraServizio=?,tipoProceduraServizio=?," +
				"dirigenteAreaProceduraServizio=?,nomeProceduraServizio=?," +
				"ufficioSede=?, PostedDate=?, id_matricola_approv_bozza=?, id_matricola_approv_messaggio= ?," +
				"id_matricola_approv_invia=?,abilitaFC=?,inFirmaCongiunta=?,daInviareExInpdapEnpals=?,daInviarePatronati=?" +
				" WHERE id_messaggio = ?";
		
		if(abilitaSystemOut)
		System.out.println("doUpdateMessaggio = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setString(1, msgDTO.getBody());
		pstmt.setString(2, msgDTO.getSubject());
		pstmt.setInt(3, msgDTO.getIdTipoMessaggio());
		pstmt.setInt(4, msgDTO.getIdClassificazione());
		pstmt.setInt(5, msgDTO.getFlagStato());
		pstmt.setString(6, msgDTO.getRifNumero());
		
		java.sql.Date rifData = null;
		if(msgDTO.getRifData()!=null){
			rifData = new java.sql.Date(msgDTO.getRifData().getTime());
		}	
		pstmt.setDate(7, rifData);
		pstmt.setString(8,msgDTO.getRifSede());
		pstmt.setString(9, msgDTO.getDaPubbSuInternet());
		pstmt.setString(10, msgDTO.getPubbProcServizio());
		pstmt.setString(11, msgDTO.getTipoProceduraServizio());
		pstmt.setString(12, msgDTO.getDirigenteAreaProcServ());
		pstmt.setString(13, msgDTO.getNomeProcServ());				
		pstmt.setString(14, msgDTO.getUfficioSede());
		
		java.sql.Timestamp postedDate = null;
		if(msgDTO.getPostedDate()!=null){
			postedDate = new java.sql.Timestamp (msgDTO.getPostedDate().getTime());
		}
		pstmt.setTimestamp(15, postedDate);
		
		pstmt.setString(16, msgDTO.getIdMatricolaApprovBozza());
		pstmt.setString(17, msgDTO.getIdMatricolaApprovaMsg());
		pstmt.setString(18, msgDTO.getIdMatricolaApprovaInvia());
		pstmt.setString(19, msgDTO.getAbilitaFC());
		
		pstmt.setString(20, msgDTO.getInFirmaCongiunta());
		pstmt.setString(21, msgDTO.getDaInviareExInpdapEnpals());
		pstmt.setString(22, msgDTO.getDaInviarePatronati());
		
		pstmt.setInt(23, msgDTO.getIdMessaggio());
		
		key = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doUpdateMessaggio_SbloccaFC(WSConnect wsc, Messaggio msgDTO, boolean esisteVersione) throws Exception
	{
		//Sblocco il messaggio originale dalla firma congiunta.
		//Ricopio in caso di esistenza delle versioni il subject ed il body prese dal messaggio a firma congiunta
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "UPDATE messaggio" +
		" SET inFirmaCongiunta=?";
		if (esisteVersione){
			query = query + ",body=?,subject=?";
		}
			
		query = query + " WHERE id_messaggio = ?";
		
		if(abilitaSystemOut)
		System.out.println("doUpdateMessaggio_SbloccaFC = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
	
		pstmt.setString(1, "");
		
		if (esisteVersione){
			pstmt.setString(2, msgDTO.getBody());
			pstmt.setString(3, msgDTO.getSubject());
			pstmt.setInt(4, msgDTO.getIdMessaggio());
		}else{
			pstmt.setInt(2, msgDTO.getIdMessaggio());
		}
		
		key = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doUpdateMessaggioFC(WSConnect wsc, Messaggio msgDTO) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "UPDATE messaggio_fc" +
				" SET body=?,subject=?,id_tipoMessaggio=?,id_classificazione=?," +
				"flg_Stato=?,rif_numero=?,rif_data=?,rif_sede=?,daPubblicareSuInternet=?," +
				"pubblicazioneProceduraServizio=?,tipoProceduraServizio=?," +
				"dirigenteAreaProceduraServizio=?,nomeProceduraServizio=?," +
				"ufficioSede=?, PostedDate=?, id_matricola_approv_bozza=?, id_matricola_approv_messaggio= ?," +
				"id_matricola_approv_invia=?,daInviareExInpdapEnpals=?,daInviarePatronati=?" +
				" WHERE id_messaggio = ?";
		
		if(abilitaSystemOut)
		System.out.println("doUpdateMessaggioFC = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setString(1, msgDTO.getBody());
		pstmt.setString(2, msgDTO.getSubject());
		pstmt.setInt(3, msgDTO.getIdTipoMessaggio());
		pstmt.setInt(4, msgDTO.getIdClassificazione());
		pstmt.setInt(5, msgDTO.getFlagStato());
		pstmt.setString(6, msgDTO.getRifNumero());
		
		java.sql.Date rifData = null;
		if(msgDTO.getRifData()!=null){
			rifData = new java.sql.Date(msgDTO.getRifData().getTime());
		}	
		pstmt.setDate(7, rifData);
		pstmt.setString(8,msgDTO.getRifSede());
		pstmt.setString(9, msgDTO.getDaPubbSuInternet());
		pstmt.setString(10, msgDTO.getPubbProcServizio());
		pstmt.setString(11, msgDTO.getTipoProceduraServizio());
		pstmt.setString(12, msgDTO.getDirigenteAreaProcServ());
		pstmt.setString(13, msgDTO.getNomeProcServ());				
		pstmt.setString(14, msgDTO.getUfficioSede());
		
		java.sql.Timestamp postedDate = null;
		if(msgDTO.getPostedDate()!=null){
			postedDate = new java.sql.Timestamp (msgDTO.getPostedDate().getTime());
		}
		pstmt.setTimestamp(15, postedDate);
		
		pstmt.setString(16, msgDTO.getIdMatricolaApprovBozza());
		pstmt.setString(17, msgDTO.getIdMatricolaApprovaMsg());
		pstmt.setString(18, msgDTO.getIdMatricolaApprovaInvia());
		pstmt.setString(19, msgDTO.getDaInviareExInpdapEnpals());
		pstmt.setString(20, msgDTO.getDaInviarePatronati());
		
		pstmt.setInt(21, msgDTO.getIdMessaggio());
		
		key = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doUpdateMessaggioFC_Autorizza(WSConnect wsc, Messaggio msgDTO, boolean esisteVersione) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "UPDATE messaggio_fc" +
		" SET flg_Autorizzata=?" +
		" WHERE id_messaggio = ?";
		
		if(abilitaSystemOut)
		System.out.println("doUpdateMessaggioFC_Autorizza = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
	
		pstmt.setInt(1, 1);
		pstmt.setInt(2, msgDTO.getIdMessaggio());
		
		key = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doUpdateMessaggioFC_PrendiInCarico(WSConnect wsc, String idMatricolaPresaInCarico, int idMsg) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "UPDATE messaggio_fc" +
		" SET id_matricola_presaInCarico=?" +
		" WHERE id_messaggio = ?";
		
		if(abilitaSystemOut)
		System.out.println("doUpdateMessaggioFC_prendiInCarico = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
	
		pstmt.setString(1,idMatricolaPresaInCarico);
		pstmt.setInt(2, idMsg);
		
		key = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	
	public int doInsertApprovatoreFC(WSConnect wsc, Messaggio msgDTO, boolean isApprovatoreDelegato) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO approvatoreFc_messaggio_fc" +
		" (id_messaggio,matricola,nominativo,matricolaDelegato,nominativoDelegato)" +
		" VALUES(?,?,?,?,?)";
		
		if(abilitaSystemOut)
		System.out.println("doInsertApprovatoreFC = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
	
		pstmt.setInt(1, msgDTO.getIdMessaggio());
		if (isApprovatoreDelegato){
			
			pstmt.setString(2,"");
			pstmt.setString(3, "");
			pstmt.setString(4,msgDTO.getIdMatricola());
			pstmt.setString(5, msgDTO.getLoginCompositore());
			
		}else{
			
			pstmt.setString(2,msgDTO.getIdMatricola());
			pstmt.setString(3, msgDTO.getLoginCompositore());
			pstmt.setString(4,"");
			pstmt.setString(5, "");
		}
		
		
		key = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doUpdateApprovatoriFC(WSConnect wsc, Messaggio msgDTO) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "UPDATE approvatori_messaggio_fc" +
		" SET approvato='1'" +
		" WHERE id_messaggio = ? AND (matricolaDirezione=? OR matricolaDirettore=?)";
		
		if(abilitaSystemOut)
		System.out.println("doUpdateApprovatoreFC = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
	
		pstmt.setInt(1, msgDTO.getIdMessaggio());
		pstmt.setString(2,msgDTO.getIdMatricola());
		pstmt.setString(3, msgDTO.getIdMatricola());
		
		key = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doUpdatePulisciApprovatoriFC(WSConnect wsc, Messaggio msgDTO) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "UPDATE approvatori_messaggio_fc" +
		" SET approvato=''" +
		" WHERE id_messaggio = ?";
		
		if(abilitaSystemOut)
		System.out.println("doUpdateApprovatoreFC = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
	
		pstmt.setInt(1, msgDTO.getIdMessaggio());
		
		key = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doUpdateMessaggioEditAdmin(WSConnect wsc, Messaggio msgDTO) throws Exception
	{
		//Esegue un update solo del body e del subject
		//metodo richiamato soltanto nel caso di edit da parte dell'amministratore di
		//un messaggio inviato e protocollato
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "UPDATE messaggio" +
				" SET body=?,subject=?,dataModifica=?"+
				" WHERE id_messaggio = ?";
		
		if(abilitaSystemOut)
		System.out.println("doUpdateMessaggio = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setString(1, msgDTO.getBody());
		pstmt.setString(2, msgDTO.getSubject());
		
		Calendar cal = Calendar.getInstance();
		java.sql.Date dataAttuale = new java.sql.Date(cal.getTimeInMillis());
		pstmt.setDate(3,dataAttuale);
		pstmt.setInt(4, msgDTO.getIdMessaggio());
		
		key = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doInsertAssociativaMessaggioSediD(WSConnect wsc, int key,String sendTo,String descSedeTo, String riepilogoDest) throws Exception
	{
	
		PreparedStatement pstmt = null;
	 
		String query = "INSERT INTO messaggio_sedeDestinatari (id_messaggio,sendTo,descSedeTo,riepilogoDestinatari)" +
		" VALUES(?,?,?,?)";
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setInt(1, key);
		pstmt.setString(2, sendTo);
		pstmt.setString(3, descSedeTo);
		pstmt.setString(4, riepilogoDest);
		pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return 1;
	}
	
	private int doInsertLogFC(WSConnect wsc, int idmsg, String azione, String commento, String matricola, String login) throws Exception {
		
		PreparedStatement pstmt = null;
			
			String query = "INSERT INTO log_messaggio_fc (id_messaggio,azione,commento,id_matricola,nominativo)" +
			" VALUES(?,?,?,?,?)";
			pstmt =  wsc.getConnection().prepareStatement(query);
			
			pstmt.setInt(1, idmsg);
			pstmt.setString(2, azione);
			pstmt.setString(3, commento);
			pstmt.setString(4, matricola);
			pstmt.setString(5, login);
			pstmt.executeUpdate();
		
		if(pstmt!=null)
			pstmt.close();
		
		return 1;
	}
	
	private int doDeleteFirmaCongiunta(WSConnect wsc, int key) throws Exception {
		
		PreparedStatement pstmt = null;
		int result;
		String query = "DELETE FROM approvatori_messaggio_fc" +
				" WHERE id_messaggio = ?";
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setInt(1, key);
		
		result = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return result;
	}
	
	private int doDeleteApprovatoreFC(WSConnect wsc, Messaggio msgDTO) throws Exception {
		
		PreparedStatement pstmt = null;
		int result;
		String query = "DELETE FROM approvatoreFc_messaggio_fc" +
				" WHERE id_messaggio = ?";
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setInt(1, msgDTO.getIdMessaggio());
		
		result = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return result;
	}
	
	private int doInsertAllegati(WSConnect wsc, int key, ArrayList<Allegato> elencoFileAllegati) throws Exception {
		
		PreparedStatement pstmt = null;
		
		Iterator it = elencoFileAllegati.iterator();
		
		while(it.hasNext()){
			Allegato allegato = (Allegato) it.next();
			
			String query = "INSERT INTO allegato (file_allegato,nome_allegato,contentType,fileSize,id_messaggio)" +
			" VALUES(?,?,?,?,?)";
			pstmt =  wsc.getConnection().prepareStatement(query);
			
			pstmt.setBytes(1, allegato.getFileData());
			pstmt.setString(2, allegato.getFileName());
			pstmt.setString(3, allegato.getContentType());
			pstmt.setInt(4, allegato.getFileSize());
			pstmt.setInt(5, key);
			pstmt.executeUpdate();
		}
		
		if(pstmt!=null)
			pstmt.close();
		
		return 1;
	}
	
	private int doInsertAllegatiFC(WSConnect wsc, int key, ArrayList<Allegato> elencoFileAllegati) throws Exception {
		
		PreparedStatement pstmt = null;
		
		Iterator it = elencoFileAllegati.iterator();
		
		while(it.hasNext()){
			Allegato allegato = (Allegato) it.next();
			
			String query = "INSERT INTO allegato_fc (file_allegato,nome_allegato,contentType,fileSize,id_messaggio)" +
			" VALUES(?,?,?,?,?)";
			pstmt =  wsc.getConnection().prepareStatement(query);
			
			pstmt.setBytes(1, allegato.getFileData());
			pstmt.setString(2, allegato.getFileName());
			pstmt.setString(3, allegato.getContentType());
			pstmt.setInt(4, allegato.getFileSize());
			pstmt.setInt(5, key);
			pstmt.executeUpdate();
		}
		
		if(pstmt!=null)
			pstmt.close();
		
		return 1;
	}
	
	private int doDeleteAssMsgSediD(WSConnect wsc, int key) throws Exception {
		
		PreparedStatement pstmt = null;
		int result;
		String query = "DELETE FROM messaggio_sedeDestinatari" +
				" WHERE id_messaggio = ?";
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setInt(1, key);
		
		result = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return result;
	}
	
	private int doDeleteAllegati(WSConnect wsc, int key) throws Exception {
		
		PreparedStatement pstmt = null;
		int result;
		String query = "DELETE FROM allegato" +
				" WHERE id_messaggio = ?";
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setInt(1, key);
		
		result = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return result;
	}
	
	private int doDeleteAllegatiFC(WSConnect wsc, int key) throws Exception {
		
		PreparedStatement pstmt = null;
		int result;
		String query = "DELETE FROM allegato_fc" +
				" WHERE id_messaggio = ?";
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setInt(1, key);
		
		result = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return result;
	}
	
	private int doDeleteMessaggio(WSConnect wsc, int key) throws Exception {
		
		PreparedStatement pstmt = null;
		int result;
		String query = "DELETE FROM messaggio" +
				" WHERE id_messaggio = ?";
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setInt(1, key);
		
		result = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return result;
	}
	
	public Messaggio selectApprovazioneFirmaCongiuntaById(String id_matricola, String codiceSede,String idMsg) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
//			"SELECT messaggio.id_messaggio,subject,body,descSedeTo,riepilogoDestinatari,id_tipoMessaggio,id_classificazione,flg_Stato,daPubblicareSuInternet,rif_numero,ufficioSede,id_matricola_approv_bozza,id_matricola_approv_messaggio" +
			String query = "SELECT *" +
					  " FROM approvatori_messaggio_fc" +
					  " WHERE id_messaggio = ?";
					  
			if(abilitaSystemOut)
			System.out.println("selectApprovazioneFirmaCongiuntaById :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg);
			rs  = pstmt.executeQuery();
			
			ArrayList<ComboElement> listaCEDirezione = new ArrayList<ComboElement>();
			ArrayList<ComboElement> listaCEDirettore = new ArrayList<ComboElement>();
			ArrayList<ComboElement> listaCESede = new ArrayList<ComboElement>();
			ArrayList<ComboElement> listaCEApprovatori = new ArrayList<ComboElement>();
			ComboElement ce1;
			ComboElement ce2;
			ComboElement ce3;
			ComboElement ce4;
			
			
			while (rs.next()) {
				
				ce1 = new ComboElement();				
				ce1.setId(rs.getString("matricolaDirezione"));
				ce1.setDescription(rs.getString("nominativoDirezione"));
				listaCEDirezione.add(ce1);
				
				ce2 = new ComboElement();
				ce2.setId(rs.getString("matricolaDirettore"));
				ce2.setDescription(rs.getString("nominativoDirettore"));
				listaCEDirettore.add(ce2);
				
				ce3 = new ComboElement();
				ce3.setId(rs.getString("sedeDettaglio").substring(0, 4));
				ce3.setDescription(rs.getString("sedeDettaglio"));
				listaCESede.add(ce3);
				
				ce4 = new ComboElement();
				ce4.setId(rs.getString("sedeDettaglio").substring(0, 4));
				ce4.setDescription(rs.getString("sedeDettaglio") + " (" + rs.getString("nominativoDirezione") + ")");
				listaCEApprovatori.add(ce4);
				
				messaggio = new Messaggio();
				messaggio.setSelectDirezioneListFC(listaCEDirezione);
				messaggio.setSelectDirettoreListFC(listaCEDirettore);
				messaggio.setSelectSediListFC(listaCESede);
				messaggio.setSelectApprovatoriFC(listaCEApprovatori);
				
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	public Messaggio selectChiNonHaApprovatoFirmaCongiunta(int idMsg) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
			//Controllo dato un idMessaggio quali approvatori hanno approvato considerando:
			//approvatori diretti (matricolaDirezione)
			//approvatori delegati (matricolaDirettore)
			String query = "SELECT * FROM approvatori_messaggio_fc AS amfc"+
			
			" WHERE (id_messaggio = ?) AND (approvato <> '1' OR approvato IS NULL)";
			
							
			if(abilitaSystemOut)
			System.out.println("selectChiNonHaApprovatoFirmaCongiunta :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, idMsg);
			rs  = pstmt.executeQuery();
			
			ArrayList<ComboElement> listaCEDirezione = new ArrayList<ComboElement>();
			
			ComboElement ce1;
			
			while (rs.next()) {
				
				ce1 = new ComboElement();				
				ce1.setId(rs.getString("matricolaDirezione"));
				ce1.setDescription(rs.getString("nominativoDirezione"));
				listaCEDirezione.add(ce1);
				
				messaggio = new Messaggio();
				messaggio.setSelectDirezioneListFC(listaCEDirezione);
				
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	
	public Messaggio selectChiHaApprovatoFirmaCongiunta(int idMsg) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT * FROM approvatoreFc_messaggio_fc" +
			
			" WHERE (id_messaggio = ?)";
			
			if(abilitaSystemOut)
			System.out.println("selectChiHaApprovatoFirmaCongiunta :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, idMsg);
			rs  = pstmt.executeQuery();
			
			ArrayList<ComboElement> listaCEDirezione = new ArrayList<ComboElement>();
			
			ComboElement ce1;
			
			String tmp_matricola;
			while (rs.next()) {
				
				ce1 = new ComboElement();		
				
				tmp_matricola = rs.getString("matricola");
				if (tmp_matricola == null || tmp_matricola.equals("")){
					ce1.setId(rs.getString("matricolaDelegato"));
					ce1.setDescription(rs.getString("nominativoDelegato"));
				}else{
					ce1.setId(rs.getString("matricola"));
					ce1.setDescription(rs.getString("nominativo"));
				}
				
				listaCEDirezione.add(ce1);
				
				messaggio = new Messaggio();
				messaggio.setSelectDirezioneListFC(listaCEDirezione);
				
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	public int selectCountChiHaApprovatoFirmaCongiunta(int idMsg) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		int result = 0;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT count(*) as count FROM approvatoreFc_messaggio_fc" +
			
			" WHERE (id_messaggio = ?)";
			
			if(abilitaSystemOut)
			System.out.println("selectChiHaApprovatoFirmaCongiunta :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, idMsg);
			rs  = pstmt.executeQuery();
			
			ArrayList<ComboElement> listaCEDirezione = new ArrayList<ComboElement>();
			
			ComboElement ce1;
			
			String tmp_matricola;
			while (rs.next()) {
				
				result = rs.getInt("count");
				
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int selectSeHoApprovatoFirmaCongiunta(int idMsg, String matricola) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int result = 0;
		try
		{
			//Cerco se la matricola fa parte degli approvatori diretti oppure come delegato
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT count(*) AS count FROM approvatoreFc_messaggio_fc" +
			
			" WHERE (id_messaggio = ?) AND (matricola = ? OR matricolaDelegato = ?)";
			
			
			if(abilitaSystemOut)
			System.out.println("selectSeHoApprovatoFirmaCongiunta :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, idMsg);
			pstmt.setString(2, matricola);
			pstmt.setString(3, matricola);
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				while (rs.next()) {
					result = rs.getInt("count");
			
				}
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int selectSePossoApprovareFirmaCongiunta(int idMsg, String matricola) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		//1 come direzione - 2 come delegato - 0 non posso approvare
		int result = 0;
		int direzione = 0;
		int direttore = 0;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT count(*) AS count FROM approvatori_messaggio_fc" +
			
			" WHERE (id_messaggio = ?) AND (matricolaDirezione = ?) AND (approvato <> '1' OR approvato IS NULL)";
			
			if(abilitaSystemOut)
			System.out.println("selectSePossoApprovareFirmaCongiunta :: Direzione " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, idMsg);
			pstmt.setString(2, matricola);
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				while (rs.next()) {
					direzione = rs.getInt("count");
					
				}
			}
			
			if (direzione == 0){
				//vede se posso approvare come delegato
				query = "SELECT count(*) AS count FROM approvatori_messaggio_fc" +
				
				" WHERE (id_messaggio = ?) AND (matricolaDirettore = ?) AND (approvato <> '1' OR approvato IS NULL)";
				
				if(abilitaSystemOut)
				System.out.println("selectSePossoApprovareFirmaCongiunta :: Direttore " + query);
				
				pstmt = wsc.getConnection().prepareStatement(query);
				pstmt.setInt(1, idMsg);
				pstmt.setString(2, matricola);
				rs  = pstmt.executeQuery();
				
				if(rs!=null){
					while (rs.next()) {
						direttore = rs.getInt("count");
						
					}
				}
				
				if (direttore > 0){
					//approvo come delegato
					result = 2;
				}
				
			}else{
				
				//approvo come pricipale direzione
				result = 1;
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public int selectComePossoApprovareFirmaCongiunta(int idMsg, String matricola) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		//1 come direzione - 2 come delegato - 0 non posso approvare
		int result = 0;
		int direzione = 0;
		int direttore = 0;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT count(*) AS count FROM approvatori_messaggio_fc" +
			
			" WHERE (id_messaggio = ?) AND (matricolaDirettore = ?)";
			
			if(abilitaSystemOut)
			System.out.println("selectComePossoApprovareFirmaCongiunta :: Direzione " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, idMsg);
			pstmt.setString(2, matricola);
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				while (rs.next()) {
					direttore = rs.getInt("count");
					
				}
			}
			
			if (direttore == 0){
				//vede se posso approvare come delegato
				query = "SELECT count(*) AS count FROM approvatori_messaggio_fc" +
				
				" WHERE (id_messaggio = ?) AND (matricolaDirezione = ?)";
				
				if(abilitaSystemOut)
				System.out.println("selectComePossoApprovareFirmaCongiunta :: Direttore " + query);
				
				pstmt = wsc.getConnection().prepareStatement(query);
				pstmt.setInt(1, idMsg);
				pstmt.setString(2, matricola);
				rs  = pstmt.executeQuery();
				
				if(rs!=null){
					while (rs.next()) {
						direzione = rs.getInt("count");
						
					}
				}
				
				if (direzione > 0){
					//approvo come delegato
					result = 2;
				}
				
			}else{
				
				//approvo come pricipale direzione
				result = 1;
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public Messaggio selectBozzaById(String id_matricola, String codiceSede,String idMsg, boolean isAdmin) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
//			"SELECT messaggio.id_messaggio,subject,body,descSedeTo,riepilogoDestinatari,id_tipoMessaggio,id_classificazione,flg_Stato,daPubblicareSuInternet,rif_numero,ufficioSede,id_matricola_approv_bozza,id_matricola_approv_messaggio" +
			String query = "SELECT messaggio.*, CONVERT(VARCHAR(10),messaggio.rif_Data, 103) as Rif_DataFormatted, messaggio_sedeDestinatari.*," +
					  " tp.descrizione as tipologiaDesc, cl.descrizione as classificazioneDesc"+
					  " FROM messaggio" +
					  " LEFT JOIN messaggio_sedeDestinatari" +
					  " ON messaggio.id_messaggio = messaggio_sedeDestinatari.id_messaggio" +
					  " LEFT JOIN tipologia AS tp ON (messaggio.id_tipoMessaggio = tp.id_tipologia)"+
					  " LEFT JOIN classificazione as cl ON (cl.id_classificazione = messaggio.id_classificazione)" + 
					  " WHERE messaggio.id_messaggio = ?"+
					  " AND flg_Stato != 2"+
						" AND messaggio.codiceSede = ?";
			
			if (!isAdmin){
				query = query + " AND (" +
				"messaggio.id_matricola= ?"+
				" OR messaggio.id_matricola_approv_bozza = ?" +
				" OR messaggio.id_matricola_approv_messaggio = ?)";
			}
			
			if(abilitaSystemOut)
			System.out.println("selectBozzaById :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg);
			pstmt.setString(2, codiceSede);
			if (!isAdmin){
				pstmt.setString(3, id_matricola);
				pstmt.setString(4, id_matricola);
				pstmt.setString(5, id_matricola);
			}
			
			rs  = pstmt.executeQuery();
		
			while (rs.next()) {
				messaggio = new Messaggio();
				messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
				messaggio.setIdMessaggioPadre(rs.getInt("id_messaggio_padre"));
				
				messaggio.setCodiceSede(rs.getString("codiceSede"));
				messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
				
		        messaggio.setSubject(StringEscapeUtils.unescapeHtml(rs.getString("subject")));
		        messaggio.setBody(rs.getString("body"));
		        
		        messaggio.setSendTo(rs.getString("SendTo"));
		        messaggio.setDescSedeTo(rs.getString("descSedeTo"));
		        messaggio.setRiepilogoDestinatari(rs.getString("riepilogoDestinatari"));
		        
		        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
		        messaggio.setTipologiaDesc(rs.getString("tipologiaDesc"));
		        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
		        messaggio.setClassificazioneDesc(rs.getString("classificazioneDesc"));
		        
		        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
		        messaggio.setDaInviareExInpdapEnpals(rs.getString("daInviareExInpdapEnpals"));
		        messaggio.setDaInviarePatronati(rs.getString("daInviarePatronati"));
		       
		        messaggio.setPubbProcServizio(rs.getString("pubblicazioneProceduraServizio"));
		        messaggio.setTipoProceduraServizio(rs.getString("tipoProceduraServizio"));
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        messaggio.setNomeProcServ(rs.getString("nomeProceduraServizio"));
				
		        messaggio.setFlagStato(rs.getInt("flg_Stato"));
		        
		        messaggio.setRifNumero(rs.getString("rif_numero"));
		        messaggio.setRifData2(rs.getString("Rif_DataFormatted"));
		        messaggio.setRifSede(rs.getString("rif_sede"));
		        
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        
		        messaggio.setIdMatricola(rs.getString("id_matricola"));
		        messaggio.setIdMatricolaApprovBozza(rs.getString("id_matricola_approv_bozza"));
		        messaggio.setIdMatricolaApprovaMsg(rs.getString("id_matricola_approv_messaggio"));
		        
		        messaggio.setAbilitaFC(rs.getString("abilitaFC"));
		        messaggio.setInFirmaCongiunta(rs.getString("inFirmaCongiunta"));
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	public int selectMessaggioFCAutorizzatoById(int idMsg) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int result = 0;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT count(id_messaggio) as count FROM messaggio_fc" +
			
			" WHERE (id_messaggio = ?) AND flg_Autorizzata = 1";
			
			if(abilitaSystemOut)
			System.out.println("selectMessaggioFCAutorizzatoById :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, idMsg);
			rs  = pstmt.executeQuery();
			
			while (rs.next()) {
				
				result = rs.getInt("count");
				
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	public ArrayList<Messaggio> selectMessaggioFC_Versioning_ById(String id_matricola, String codiceSede,String idMsg, boolean isAdmin) throws Exception
	{
		//Selezione delle versioni messaggio a firma congiunta
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		
		ArrayList<Messaggio> listaMsgDTO = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			//Seleziono il messaggio a firma congiunta controllando se sono abilitato a leggerlo
			String query = "SELECT mfc.*, convert (VARCHAR, mfc.dataCreazione, 103) as dataCreazioneFormatted, convert (VARCHAR, mfc.dataCreazione, 108) as dataCreazioneTimeFormatted, msd.*" +
					  " FROM messaggio_fc AS mfc" +
					  " LEFT JOIN messaggio_sedeDestinatari AS msd" +
					  " ON mfc.id_messaggio = msd.id_messaggio";
			
					  if(!isAdmin){
						  query = query + " INNER JOIN approvatori_messaggio_fc AS amfc"+
		                  " ON mfc.id_messaggio_rif = amfc.id_messaggio";
					  }
					
					  query = query + " WHERE mfc.id_messaggio_rif = ?";
					  
					  if(!isAdmin){

						  
						  query = query + " AND ("+
						  "(mfc.id_matricola = ?) OR (mfc.id_matricola_approv_bozza = ?) OR (mfc.id_matricola_approv_messaggio = ?)"+
						  " OR "+
				            "(amfc.matricolaDirezione = ?) OR (amfc.matricolaDirettore = ?)"+
				            
				            ")";
			            
					  }
					  
					  query = query + " ORDER BY mfc.dataCreazione DESC";
				
			if(abilitaSystemOut)
			System.out.println("selectMessaggioFC_Versioning_ById :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg);
			 if(!isAdmin){
				 	pstmt.setString(2, id_matricola);
					pstmt.setString(3, id_matricola);
					pstmt.setString(4, id_matricola);
					pstmt.setString(5, id_matricola);
					pstmt.setString(6, id_matricola);
			 }
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				
			
				listaMsgDTO = new ArrayList<Messaggio>();
				DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
		
				while (rs.next()) {
					messaggio = new Messaggio();
					messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
					messaggio.setIdMessaggioPadre(rs.getInt("id_messaggio_padre"));
					messaggio.setIdMessaggioRif(rs.getInt("id_messaggio_rif"));
					messaggio.setIdMessaggioFC(rs.getInt("id_messaggio_fc"));
					
					messaggio.setCodiceSede(rs.getString("codiceSede"));
					messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
					
			        messaggio.setSubject(rs.getString("subject"));
			        messaggio.setBody(rs.getString("body"));
			        messaggio.setSendTo(rs.getString("SendTo"));
			        messaggio.setDescSedeTo(rs.getString("descSedeTo"));
			        messaggio.setRiepilogoDestinatari(rs.getString("riepilogoDestinatari"));
			        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
			        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
			        
			        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
			        messaggio.setFlagStato(rs.getInt("flg_Stato"));
			        
			        messaggio.setRifNumero(rs.getString("rif_numero"));
			        messaggio.setUfficioSede(rs.getString("ufficioSede"));
			        
			        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
			        
			        messaggio.setIdMatricola(rs.getString("id_matricola"));
			        messaggio.setIdMatricolaApprovBozza(rs.getString("id_matricola_approv_bozza"));
			        messaggio.setIdMatricolaApprovaMsg(rs.getString("id_matricola_approv_messaggio"));
			        
			        messaggio.setIdMatricolaPresaInCarico(rs.getString("id_matricola_presaInCarico"));
			        
			        //String s=df.format(rs.getDate("dataCreazione"));
			        //messaggio.setCreationDateFormatted(s);
			        
			        messaggio.setCreationDateFormatted(rs.getString("dataCreazioneFormatted")+" - "+rs.getString("dataCreazioneTimeFormatted"));
			        messaggio.setCreationDate(rs.getDate("dataCreazione"));
			        
			        listaMsgDTO.add(messaggio);
			    }
				
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaMsgDTO;
	}
	
	public Messaggio selectMessaggioFCById(String id_matricola, String codiceSede,String idMsg, boolean isAdmin, boolean isVersione) throws Exception
	{
		//Selezione del messaggio a firma congiunta
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String Query_id_messaggio = "";
			String Query_id_messaggio_mfc = "";
			
			if (isVersione){
				 //Eseguo la query su una versione della firma congiunta, il riferimento id � il padre principale per ricavare i
				 //destinatari ed approvatori
				Query_id_messaggio = "mfc.id_messaggio_rif";
				Query_id_messaggio_mfc = "mfc.id_messaggio_fc";
				
			}else{
				Query_id_messaggio = "mfc.id_messaggio";
				Query_id_messaggio_mfc = "mfc.id_messaggio";
			}
			
			//Seleziono tutte le versioni del messaggio a firma congiunta controllando se sono abilitato a leggerlo
			String query = "SELECT mfc.*,msd.*,CONVERT(VARCHAR(10),mfc.rif_Data, 103) as Rif_DataFormatted,"+
					  "tp.descrizione as tipologiaDesc, cl.descrizione as classificazioneDesc"+
					  " FROM messaggio_fc AS mfc" +
					  
					  " LEFT JOIN messaggio_sedeDestinatari AS msd" +
					  	" ON " + Query_id_messaggio + " = msd.id_messaggio"+
					  
					  " LEFT JOIN approvatori_messaggio_fc AS amfc"+
					  	" ON " + Query_id_messaggio + " = amfc.id_messaggio"+
					  	
					  " LEFT JOIN tipologia AS tp ON (mfc.id_tipoMessaggio = tp.id_tipologia)"+
					  " LEFT JOIN classificazione as cl ON (mfc.id_classificazione = cl.id_classificazione)" +
					  	
					  " WHERE " + Query_id_messaggio_mfc + " = ?";	
			
					  if(!isAdmin){
						  
						  //Aggiungo l'AND delle matricole abilitate a leggere la FC
						  query = query + " AND ("+
						  "(mfc.id_matricola = ?) OR (mfc.id_matricola_approv_bozza = ?) OR (mfc.id_matricola_approv_messaggio = ?)"+
						  " OR "+
				            "(amfc.matricolaDirezione = ?) OR (amfc.matricolaDirettore = ?)"+
				            
				            ")";
			            
					  }
					
			if(abilitaSystemOut)
			System.out.println("selectMessaggioFCById :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg);
			 if(!isAdmin){
				 	pstmt.setString(2, id_matricola);
					pstmt.setString(3, id_matricola);
					pstmt.setString(4, id_matricola);
					pstmt.setString(5, id_matricola);
					pstmt.setString(6, id_matricola);
			 }
			
			rs  = pstmt.executeQuery();
		
			while (rs.next()) {
				messaggio = new Messaggio();
				messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
				messaggio.setIdMessaggioPadre(rs.getInt("id_messaggio_padre"));
				messaggio.setIdMessaggioRif(rs.getInt("id_messaggio_rif"));
				messaggio.setIdMessaggioFC(rs.getInt("id_messaggio_fc"));
				
				messaggio.setCodiceSede(rs.getString("codiceSede"));
				messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
				
		        messaggio.setSubject(rs.getString("subject"));
		        messaggio.setBody(rs.getString("body"));
		        messaggio.setSendTo(rs.getString("SendTo"));
		        messaggio.setDescSedeTo(rs.getString("descSedeTo"));
		        messaggio.setRiepilogoDestinatari(rs.getString("riepilogoDestinatari"));
		        
		        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
		        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
		        
		        messaggio.setTipologiaDesc(rs.getString("tipologiaDesc"));
		        messaggio.setClassificazioneDesc(rs.getString("classificazioneDesc"));
		     
		        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
		        messaggio.setDaInviareExInpdapEnpals(rs.getString("daInviareExInpdapEnpals"));
		        messaggio.setDaInviarePatronati(rs.getString("daInviarePatronati"));

		        messaggio.setPubbProcServizio(rs.getString("pubblicazioneProceduraServizio"));
		        messaggio.setTipoProceduraServizio(rs.getString("tipoProceduraServizio"));
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        messaggio.setNomeProcServ(rs.getString("nomeProceduraServizio"));
		        
		        messaggio.setFlagStato(rs.getInt("flg_Stato"));
		        
		        messaggio.setRifNumero(rs.getString("rif_numero"));
		        messaggio.setRifData2(rs.getString("Rif_DataFormatted"));
		        messaggio.setRifSede(rs.getString("rif_sede"));
		        
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        
		        messaggio.setIdMatricola(rs.getString("id_matricola"));
		        messaggio.setIdMatricolaApprovBozza(rs.getString("id_matricola_approv_bozza"));
		        messaggio.setIdMatricolaApprovaMsg(rs.getString("id_matricola_approv_messaggio"));
		        
		        messaggio.setIdMatricolaPresaInCarico(rs.getString("id_matricola_presaInCarico"));
		        messaggio.setFlagAutorizzata(rs.getInt("flg_Autorizzata"));
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	public Messaggio selectMessaggioBySegnatura(String id_matricola, String codiceSede,String idSegnatura) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try 
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
	        
			//Ricavo il messaggio considerando sia gli inviati che i ricevuti
			//(indipendentemente se protocollati o meno)
			//il cui id_messaggio coincide con il parametro
			//e la cui sede utente di sessione fa parte dei messaggi ricevuti o inviati
			String query = "SELECT m.*,convert (VARCHAR, m.PostedDate, 103) as postedDateFormatted, convert (VARCHAR, m.PostedDate, 108) as postedTimeFormatted,CONVERT(VARCHAR(10),m.rif_Data, 103) as Rif_DataFormatted,"+
					  " msd.sendTo,msd.descSedeTo,msd.riepilogoDestinatari,tp.descrizione as tipologiaDesc, cl.descrizione as classificazioneDesc,m.UIDoc"+
					  " FROM messaggio AS m"+
					  " INNER JOIN messaggio_sedeDestinatari AS msD ON (m.id_messaggio = mSD.id_messaggio)" +
					  " LEFT JOIN tipologia AS tp ON (m.id_tipoMessaggio = tp.id_tipologia)"+
					  " LEFT JOIN classificazione as cl ON (cl.id_classificazione = m.id_classificazione)" + 
					  " WHERE m.segnatura = ?"+
//					  " AND ((msD.sendTo LIKE '%" + codiceSede + "%') OR m.codiceSede ='" + codiceSede + "')"+
					  " AND (m.PostedDate is not null)"+ 
				      " AND m.flg_Stato = " + Messaggio.FLG_STATO_INVIATO;
			
			if(abilitaSystemOut)
			System.out.println("selectMessaggioById :: " + query);
			
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idSegnatura);
			
			rs  = pstmt.executeQuery();
//			DateFormat df=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			
			while (rs.next()) {
				messaggio = new Messaggio();
				
				messaggio.setIdMatricola(rs.getString("id_matricola"));
				messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
				messaggio.setIdMessaggioPadre(rs.getInt("id_messaggio_padre"));
				
				messaggio.setCodiceSede(rs.getString("codiceSede"));
				messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
				
				// *** gestione messaggio inviato/ricevuto ***
				String ir = rs.getString("sendTo");
				String cs = rs.getString("codiceSede");
				
				if (ir.contains(codiceSede) && !cs.equals(codiceSede)){
					messaggio.setFlagIR("R");
				}else if (cs.equals(codiceSede)){
					messaggio.setFlagIR("I");
					messaggio.setIdMatricolaApprovaInvia(rs.getString("id_matricola_approv_invia"));
				}
//				 *** fine gestione messaggio inviato/ricevuto
		        
		        messaggio.setSubject(StringEscapeUtils.unescapeHtml(rs.getString("subject")));

		        String flg_BodyStorico = rs.getString("flg_BodyStorico");
		        if (flg_BodyStorico != null && flg_BodyStorico.equals("1")){
		        	//messaggio importato da lotus
		        	messaggio.setBody(StringEscapeUtils.escapeHtml(rs.getString("body")).replaceAll("�","\'").replaceAll("�", "\'").replaceAll("�", "\"").replaceAll("�", "\""));
		        }else{
		        	 messaggio.setBody(rs.getString("body"));
		        }
		        messaggio.setFlagBodyStorico(flg_BodyStorico);
		        
		        messaggio.setSendTo(rs.getString("SendTo"));
		        messaggio.setDescSedeTo(rs.getString("descSedeTo"));
		        messaggio.setRiepilogoDestinatari(rs.getString("riepilogoDestinatari"));
		        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
		        messaggio.setTipologiaDesc(rs.getString("tipologiaDesc"));
		        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
		        messaggio.setClassificazioneDesc(rs.getString("classificazioneDesc"));
//		        String s=df.format(rs.getDate("PostedDate"));
//		        messaggio.setPostedDateFormatted(s);
		        messaggio.setPostedDateFormatted(rs.getString("postedDateFormatted")+" "+rs.getString("postedTimeFormatted"));
				messaggio.setPostedDate(rs.getDate("PostedDate"));
				
		        messaggio.setNroProtocollo(rs.getString("nroProtocollo"));
		        messaggio.setSegnatura(rs.getString("segnatura"));
		        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
		        
		        messaggio.setDaInviareExInpdapEnpals(rs.getString("daInviareExInpdapEnpals"));
		        messaggio.setDaInviarePatronati(rs.getString("daInviarePatronati"));
		        
		        messaggio.setPubbProcServizio(rs.getString("pubblicazioneProceduraServizio"));
		        messaggio.setTipoProceduraServizio(rs.getString("tipoProceduraServizio"));
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        messaggio.setNomeProcServ(rs.getString("nomeProceduraServizio"));
		        
		        messaggio.setFlagStato(rs.getInt("flg_Stato"));
		        
		        messaggio.setRifNumero(rs.getString("rif_numero"));
		        messaggio.setRifData2(rs.getString("Rif_DataFormatted"));
		        messaggio.setRifSede(rs.getString("rif_sede"));
		        
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        
		        //codice sede mittente
		        messaggio.setCodiceSede(rs.getString("codiceSede"));
		        //descrizione della sede del mittente
		        messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
		        
		        messaggio.setUidoc(rs.getString("UIDoc"));
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	public Messaggio selectMessaggioById(String id_matricola, String codiceSede,String idMsg) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
	        
			//Ricavo il messaggio considerando sia gli inviati che i ricevuti
			//(indipendentemente se protocollati o meno)
			//il cui id_messaggio coincide con il parametro
			//e la cui sede utente di sessione fa parte dei messaggi ricevuti o inviati
			String query = "SELECT m.*,convert (VARCHAR, m.PostedDate, 103) as postedDateFormatted, convert (VARCHAR, m.PostedDate, 108) as postedTimeFormatted,CONVERT(VARCHAR(10),m.rif_Data, 103) as Rif_DataFormatted,"+
					  " msd.sendTo,msd.descSedeTo,msd.riepilogoDestinatari,tp.descrizione as tipologiaDesc, cl.descrizione as classificazioneDesc,m.UIDoc"+
					  " FROM messaggio AS m"+
					  " INNER JOIN messaggio_sedeDestinatari AS msD ON (m.id_messaggio = mSD.id_messaggio)" +
					  " LEFT JOIN tipologia AS tp ON (m.id_tipoMessaggio = tp.id_tipologia)"+
					  " LEFT JOIN classificazione as cl ON (cl.id_classificazione = m.id_classificazione)" + 
					  " WHERE m.id_messaggio = ?"+
					  " AND ((msD.sendTo LIKE '%" + codiceSede + "%') OR m.codiceSede ='" + codiceSede + "')"+
					  " AND (m.PostedDate is not null)"+ 
				      " AND m.flg_Stato = " + Messaggio.FLG_STATO_INVIATO;
			
			if(abilitaSystemOut)
			System.out.println("selectMessaggioById :: " + query);
			
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg);
			
			rs  = pstmt.executeQuery();
//			DateFormat df=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			
			while (rs.next()) {
				messaggio = new Messaggio();
				
				messaggio.setIdMatricola(rs.getString("id_matricola"));
				messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
				messaggio.setIdMessaggioPadre(rs.getInt("id_messaggio_padre"));
				
				messaggio.setCodiceSede(rs.getString("codiceSede"));
				messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
				
				// *** gestione messaggio inviato/ricevuto ***
				String ir = rs.getString("sendTo");
				String cs = rs.getString("codiceSede");
				
				if (ir.contains(codiceSede) && !cs.equals(codiceSede)){
					messaggio.setFlagIR("R");
				}else if (cs.equals(codiceSede)){
					messaggio.setFlagIR("I");
					messaggio.setIdMatricolaApprovaInvia(rs.getString("id_matricola_approv_invia"));
				}
//				 *** fine gestione messaggio inviato/ricevuto
		        
		        messaggio.setSubject(StringEscapeUtils.escapeHtml(rs.getString("subject")));

		        String flg_BodyStorico = rs.getString("flg_BodyStorico");
		        if (flg_BodyStorico != null && flg_BodyStorico.equals("1")){
		        	//messaggio importato da lotus
		        	messaggio.setBody(StringEscapeUtils.escapeHtml(rs.getString("body")));
		        }else{
		        	 messaggio.setBody(rs.getString("body").replaceAll("�","\'").replaceAll("�", "\'").replaceAll("�", "\"").replaceAll("�", "\"").replaceAll("�", "-"));
		        }
		        messaggio.setFlagBodyStorico(flg_BodyStorico);
		        
		        messaggio.setSendTo(rs.getString("SendTo"));
		        messaggio.setDescSedeTo(rs.getString("descSedeTo"));
		        messaggio.setRiepilogoDestinatari(rs.getString("riepilogoDestinatari"));
		        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
		        messaggio.setTipologiaDesc(rs.getString("tipologiaDesc"));
		        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
		        messaggio.setClassificazioneDesc(rs.getString("classificazioneDesc"));
//		        String s=df.format(rs.getDate("PostedDate"));
//		        messaggio.setPostedDateFormatted(s);
		        messaggio.setPostedDateFormatted(rs.getString("postedDateFormatted")+" "+rs.getString("postedTimeFormatted"));
				messaggio.setPostedDate(rs.getDate("PostedDate"));
				
		        messaggio.setNroProtocollo(rs.getString("nroProtocollo"));
		        messaggio.setSegnatura(rs.getString("segnatura"));
		        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
		        
		        messaggio.setDaInviareExInpdapEnpals(rs.getString("daInviareExInpdapEnpals"));
		        messaggio.setDaInviarePatronati(rs.getString("daInviarePatronati"));
		        
		        messaggio.setPubbProcServizio(rs.getString("pubblicazioneProceduraServizio"));
		        messaggio.setTipoProceduraServizio(rs.getString("tipoProceduraServizio"));
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        messaggio.setNomeProcServ(rs.getString("nomeProceduraServizio"));
		        
		        messaggio.setFlagStato(rs.getInt("flg_Stato"));
		        
		        messaggio.setRifNumero(rs.getString("rif_numero"));
		        messaggio.setRifData2(rs.getString("Rif_DataFormatted"));
		        messaggio.setRifSede(rs.getString("rif_sede"));
		        
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        
		        //codice sede mittente
		        messaggio.setCodiceSede(rs.getString("codiceSede"));
		        //descrizione della sede del mittente
		        messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
		        
		        messaggio.setUidoc(rs.getString("UIDoc"));
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	public Messaggio selectMessaggioById_dcnhsp(String id_matricola, String codiceSede,String idMsg) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
	        
			//Ricavo il messaggio considerando sia gli inviati che i ricevuti
			//(indipendentemente se protocollati o meno)
			//il cui id_messaggio coincide con il parametro
			//e la cui sede utente di sessione fa parte dei messaggi ricevuti o inviati
			
			//Restituisco i campi destinatari e destinatari che hanno stampato il protocollo
			String query = "SELECT msT.stampatoDa,msd.sendTo,msd.descSedeTo"+
					  " FROM messaggio AS m"+
					  " INNER JOIN messaggio_sedeDestinatari AS msD ON (m.id_messaggio = mSD.id_messaggio)"+
					  " left JOIN messaggio_stampatoDa AS msT ON (m.id_messaggio = msT.id_messaggio)"+
					  " WHERE m.id_messaggio = ?"+
					  " AND ((msD.sendTo LIKE '%" + codiceSede + "%') OR m.codiceSede ='" + codiceSede + "')"+
					  " AND (m.PostedDate is not null)"+ 
				      " AND m.flg_Stato = " + Messaggio.FLG_STATO_INVIATO;
			
			if(abilitaSystemOut)
			System.out.println("selectMessaggioById_dcnhsp :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg);
			
			rs  = pstmt.executeQuery();
			
			while (rs.next()) {
				messaggio = new Messaggio();
				
				messaggio.setSendTo(rs.getString("sendTo"));
				messaggio.setStampatoDa(rs.getString("stampatoDa"));
		        //messaggio.setDescSedeTo(rs.getString("descSedeTo"));
		      
		        
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	
	public int eliminaMessaggio(Integer keyMsg) throws Exception
	{
		int result = -1;
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
			//cancella eventuali sedi destinatarie associate
			doDeleteAssMsgSediD(wsc, keyMsg);
			//cancella evenutali allegati associati:
			doDeleteAllegati(wsc, keyMsg);
			//cancella il messaggio tramite id
			result = doDeleteMessaggio(wsc,keyMsg);
				
			wsc.commit();
			
			if(abilitaSystemOut)
			System.out.println("****Commit eseguito****");
			
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	

	//aggiorna il messaggio con i dati della protocollazione
	public int updateMsgProtocollato(Messaggio msgDTO)throws Exception {
		int result = -1;
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
			
			PreparedStatement pstmt = null;
			
			String query = "UPDATE messaggio" +
					" SET segnatura=?,NroProtocollo=?,DeliveredDate=?" +
					" WHERE id_messaggio = ?";
			
			if(abilitaSystemOut)
			System.out.println("updateMsgProtocollato = "+ query);
			
			pstmt =  wsc.getConnection().prepareStatement(query);
			
			pstmt.setString(1, msgDTO.getSegnatura());
			pstmt.setString(2, msgDTO.getNroProtocollo());
			
//			java.sql.Date deliveredDate = null;
//			if(msgDTO.getDeliveredDate()!=null){
//				deliveredDate = new java.sql.Date(msgDTO.getDeliveredDate().getTime());
//			}	
//			pstmt.setDate(3, deliveredDate);
			java.sql.Timestamp deliveredDate = null;
			if(msgDTO.getDeliveredDate()!=null){
				deliveredDate = new java.sql.Timestamp(msgDTO.getDeliveredDate().getTime());
			}	
			pstmt.setTimestamp(3, deliveredDate);
			
			pstmt.setInt(4, msgDTO.getIdMessaggio());
			result = pstmt.executeUpdate();
			
			wsc.commit();
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		
		
		
		return result;
		
	}
	
	//aggiorna il messaggio con i dati della protocollazione
	public int updateMsgProtocollatoMultithread(WSConnect wsc, Messaggio msgDTO)throws Exception {
		int result = -1;
		
//		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		
		try
		{	 
//			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "UPDATE messaggio" +
					" SET segnatura=?,NroProtocollo=?,DeliveredDate=?" +
					" WHERE id_messaggio = ?";
			
			if(abilitaSystemOut)
			System.out.println("updateMsgProtocollato = "+ query);
			
			pstmt =  wsc.getConnection().prepareStatement(query);
			
			pstmt.setString(1, msgDTO.getSegnatura());
			pstmt.setString(2, msgDTO.getNroProtocollo());
			
//			java.sql.Date deliveredDate = null;
//			if(msgDTO.getDeliveredDate()!=null){
//				deliveredDate = new java.sql.Date(msgDTO.getDeliveredDate().getTime());
//			}	
//			pstmt.setDate(3, deliveredDate);
			java.sql.Timestamp deliveredDate = null;
			if(msgDTO.getDeliveredDate()!=null){
				deliveredDate = new java.sql.Timestamp(msgDTO.getDeliveredDate().getTime());
			}	
			pstmt.setTimestamp(3, deliveredDate);
			
			pstmt.setInt(4, msgDTO.getIdMessaggio());
			result = pstmt.executeUpdate();
			
			wsc.commit();
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			
			if(pstmt!=null)
				pstmt.close();

		}
		
		return result;
		
	}
	
	public int insertMessaggio_lettoDa(String idMsg, String id_matricola, String id_sede) throws Exception
	{
		int keyMsg = -1;
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			//Controllo se ho gi� loggato il messaggio letto per la matricola
			keyMsg = doSelectMessaggio_lettoDa(wsc, id_matricola, idMsg, id_sede);
			if (keyMsg == -1){
				//Inserisco il messaggio come letto Da matricola
				keyMsg = doInsertMessaggio_lettoDa(wsc, id_matricola, idMsg, id_sede);
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit Messaggio_lettoDa eseguito****");
			}

		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return keyMsg;
	}

	
	public int doInsertMessaggio_lettoDa(WSConnect wsc, String id_matricola, String idMsg, String id_sede) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO messaggio_lettoDa" +
				" (id_messaggio,id_matricola, id_sede)"+
				" VALUES(?,?,?)";
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		pstmt.setInt(1, Integer.parseInt(idMsg));
		pstmt.setString(2, id_matricola);
		pstmt.setString(3, id_sede);
		
		key = pstmt.executeUpdate();
		
		if(abilitaSystemOut)
		System.out.println("doInsertMessaggio_lettoDa = "+ query);
		
		return key;
	}
	
	public int doSelectMessaggio_lettoDa(WSConnect wsc, String id_matricola, String idMsg, String id_sede) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT * FROM messaggio_lettoDa"+
		" WHERE id_messaggio = ?" + 
		" AND id_matricola = ?" +
		" AND id_sede = ?";
		
		if(abilitaSystemOut)
		System.out.println("doSelectMessaggio_lettoDa = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setString(1, idMsg);
		pstmt.setString(2, id_matricola);
		pstmt.setString(3, id_sede);
		
		
		rs  = pstmt.executeQuery();
		while (rs.next()) {
			key = 1;
		}
		if(rs!=null)
			rs.close();
		return key;
	}
	
	
	public int insertMessaggio_inoltratoA(String idMsg, String id_matricola) throws Exception
	{
		int keyMsg = -1;
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			//Controllo se ho gi� loggato il messaggio letto per la matricola
			keyMsg = doSelectMessaggio_inoltratoA(wsc, id_matricola, idMsg);
			if (keyMsg == -1){
				//Inserisco il messaggio come letto Da matricola
				keyMsg = doInsertMessaggio_inoltratoA(wsc, id_matricola, idMsg);
				wsc.commit();
				
				if(abilitaSystemOut)
				System.out.println("****Commit Messaggio_inoltratoA eseguito****");
			}

		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return keyMsg;
	}
	
	public int doInsertMessaggio_inoltratoA(WSConnect wsc, String id_matricola, String idMsg) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO messaggio_inoltratoA" +
				" (id_messaggio,id_matricola)"+
				" VALUES(?,?)";
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		pstmt.setInt(1, Integer.parseInt(idMsg));
		pstmt.setString(2, id_matricola);
		
		key = pstmt.executeUpdate();
		
		if(abilitaSystemOut)
		System.out.println("doInsertMessaggio_inoltratoA = "+ query);
		
		if(pstmt!=null)
			pstmt.close();
		return key;
	}
	
	public int doSelectMessaggio_inoltratoA(WSConnect wsc, String id_matricola, String idMsg) throws Exception
	{
	
		int key = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT * FROM messaggio_inoltratoA"+
		" WHERE id_messaggio = ?" + 
		" AND id_matricola = ?";
		
		if(abilitaSystemOut)
		System.out.println("doSelectMessaggio_inoltratoA = "+ query);
		
		pstmt =  wsc.getConnection().prepareStatement(query);
		
		pstmt.setString(1, idMsg);
		pstmt.setString(2, id_matricola);
		
		rs  = pstmt.executeQuery();
		while (rs.next()) {
			key = 1;
		}
		if(rs!=null)
			rs.close();
		return key;
	}
	
	public int updateMessaggio_StampatoDa(Integer[] msgSelected, String codiceSede) throws Exception
	{
		int key = -1;
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			int lng = msgSelected.length;
			
			for(int i=0;i<lng;i++){
				key = doUpdateMessaggio_StampatoDa(wsc, msgSelected[i], codiceSede);
				
			}
			//Controllo se ho gi� loggato il messaggio letto per la matricola
			
			if(abilitaSystemOut)
			System.out.println("****Commit updateMessaggio_StampatoDa****");
			
			wsc.commit();

		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return key;
	}
	
	public int doUpdateMessaggio_StampatoDa(WSConnect wsc, Integer Id, String codiceSede) throws Exception
	{
		int key = -1;
		int eseguiUpdate = 1;
		ResultSet rs = null;
		
			PreparedStatement pstmt = null;
			String query = "";
			String stampatoDa = "";
			
			//Controllo se esiste gia un record che indica che il messaggio/protocollo
			//� gia stato stampato da una o pi� sede
			query = "SELECT * FROM messaggio_stampatoDa"+
					" WHERE id_messaggio = " + Id.toString();
					//" AND stampatoDa NOT LIKE '%" + codiceSede + "%'";
			
			pstmt =  wsc.getConnection().prepareStatement(query);
			rs  = pstmt.executeQuery();
			while (rs.next()) {
				//Se esiste il recordo messaggio stampato
				//e la sede non fa parte dell'elenco di stampa
				//aggiungo la sede all'elenco sedi che hanno stampato
				stampatoDa = rs.getString("stampatoDa");
				if (stampatoDa.contains(codiceSede)){
					eseguiUpdate = 0;
				} else stampatoDa += "," + codiceSede;
				
			}
			
			if (stampatoDa.equals("")){
				//inserisco il recordo perch� la sede corrente � la prima a stampare
				//il messaggio/protocollo
				query = "INSERT INTO messaggio_stampatoDa" +
				" (id_messaggio,stampatoDa)"+
				" VALUES("+ Id.toString()+",'" + codiceSede + "')";
			}else{
				//prepara l'update 
				query = "UPDATE messaggio_stampatoDa" +
				" SET stampatoDa = '" + stampatoDa + "'"+
				" WHERE id_messaggio = " + Id.toString();
			}

			if (eseguiUpdate == 1){
				
				if(abilitaSystemOut)
					System.out.println("updateMessaggio_StampatoDa = "+ query);
				
				pstmt =  wsc.getConnection().prepareStatement(query);
				key = pstmt.executeUpdate();
			}
			
			if(pstmt!=null)
				pstmt.close();
			
			if(rs!=null)
				rs.close();
			
			return key;
			
		}
	
	
	public int updateMessaggioFlagStampa_OLD(String listaIds) throws Exception
	{
		int key = -1;
		
		
		WSConnect wsc = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			PreparedStatement pstmt = null;
			
			String query = "UPDATE messaggio" +
					" SET flagStampa = '1'" +
					" WHERE id_messaggio IN " + listaIds;
			
			if(abilitaSystemOut)
			System.out.println("updateMessaggioFlagStampa = "+ query);
			
			pstmt =  wsc.getConnection().prepareStatement(query);
			key = pstmt.executeUpdate();
			
			wsc.commit();
				
			if(abilitaSystemOut)
				System.out.println("****Commit eseguito****");
			
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				wsc.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return key;
	}
	
	public Messaggio selectMessaggio_lettoDa_byId(String idMsg, String idSede) throws Exception
	{
//		Dato un idMessaggio prendo la lista delle matricole che hanno letto il meossaggi
		//e valorizza la CE temporanea nel DTO messaggio
		int keyMsg = -1;
		
		WSConnect wsc = null;
		Messaggio messaggio = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());

			String query = "SELECT * FROM messaggio_lettoDa"+
			" WHERE id_messaggio = ? AND id_sede = ?";
			
			if(abilitaSystemOut)
			System.out.println("selectMessaggio_lettoDa_byId = "+ query);
			
			pstmt =  wsc.getConnection().prepareStatement(query);
			
			pstmt.setString(1, idMsg);
			pstmt.setString(2, idSede);
			
			
			rs  = pstmt.executeQuery();
			
			ArrayList<ComboElement> listaCE = new ArrayList<ComboElement>();
			ComboElement ce;
			
			messaggio = new Messaggio();
			
			while (rs.next()) {
				ce = new ComboElement();				
				ce.setId(rs.getString("id_matricola"));
				ce.setDescription(rs.getString("id_matricola"));
				listaCE.add(ce);
			}
			
			messaggio.setElencoCETemporaneo(listaCE);

		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	public Messaggio selectMessaggio_inoltratoA_byId(String idMsg) throws Exception
	{
		//Dato un idMessaggio prendo la lista delle matricole a cui ho inoltrato
		//e valorizza la CE temporanea nel DTO messaggio
		int keyMsg = -1;
		
		WSConnect wsc = null;
		Messaggio messaggio = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT * FROM messaggio_inoltratoA"+
			" WHERE id_messaggio = ?";
			
			if(abilitaSystemOut)
			System.out.println("selectMessaggio_inoltratoA_byId = "+ query);
			
			pstmt =  wsc.getConnection().prepareStatement(query);
			
			pstmt.setString(1, idMsg);
			
			rs  = pstmt.executeQuery();
			
			ArrayList<ComboElement> listaCE = new ArrayList<ComboElement>();
			ComboElement ce;
			
			messaggio = new Messaggio();
			
			while (rs.next()) {
				ce = new ComboElement();				
				ce.setId(rs.getString("id_matricola"));
				ce.setDescription(rs.getString("id_matricola"));
				listaCE.add(ce);
			}
			
			messaggio.setElencoCETemporaneo(listaCE);

		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}
	
	/*
	public MessaggioWS[] selectStoricoMsg(BeanInput bi) throws Exception
	{
		//La query seguente ritorna un Array di Messaggio recuperati nella tabella storicoMessaggi
		//in base ai criteri di ricerca passati in input al web service
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		MessaggioWS msgDTO = null;
		
		MessaggioWS[] listaMsgDTO = null;
		
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String whereCondition= "PostedDate BETWEEN CONVERT(datetime,'" + bi.getDateFrom()+ "',21) AND CONVERT(datetime,'" + bi.getDateTo() + "',21) ";
			//CAMPI OPZIONALI 
			if(bi.getNroProtocollo()!=null && !bi.getNroProtocollo().equals("")){
				whereCondition += " AND NroProtocollo = " + bi.getNroProtocollo();
			}
			
			if(bi.getTxtInSubject()!=null && !bi.getTxtInSubject().equals("")){
				whereCondition += " AND Subject like '%" + bi.getTxtInSubject() + "%'";
			}
			
			if(bi.getTxtInBody()!=null && !bi.getTxtInBody().equals("")){
				whereCondition += " AND Body like '%" +bi.getTxtInBody() +"%'" ;
			}
			
			if(bi.getCodiceSede()!=null && !bi.getCodiceSede().equals("")){
				whereCondition += " AND SUBSTRING(Sede,0,5) = " + bi.getCodiceSede();
			}
			
			String queryCount = "SELECT count(id_messaggio) as count"+
				" FROM storico_messaggio" +
				" WHERE " + whereCondition;
			
			pstmt = wsc.getConnection().prepareStatement(queryCount);
			rs  = pstmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			//recupera dal file di configurazione il numero massimo che pu� essere
			//restituito dalla ricerca
			int maxCount = Integer.parseInt(ConfigProp.ricercaNumMax);
			if (count>maxCount){
				//se la query restituisce un numero maggiore nel numero massimo preso dal file
				//di configurazione allora viene settato quest'ultimo per dare un limite ai record
				//da restituire
				count = maxCount;
			}
				
			
			String query = "SELECT TOP " + maxCount + " id_Messaggio, UNIVERSALID, NroProtocollo,CONVERT(varchar,PostedDate,105) as dataPubblicazione,SUBSTRING(Sede,0,5) as sedeMittente,Subject,Body" +
				" FROM storico_messaggio" +
				" WHERE " + whereCondition;
				
			System.out.println("selectStoricoMsg :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				//istanzia l'array che avr� come dimensioni count calcolato in precedenza
				//(non pu� essere maggiore del numero massimo)
				listaMsgDTO = new MessaggioWS[count];
				
				while(rs.next()){
					msgDTO = new MessaggioWS();
					msgDTO.setIdMessaggio(rs.getString("id_Messaggio"));
					msgDTO.setUniversalID(rs.getString("UNIVERSALID"));
					msgDTO.setNroProtocollo(rs.getString("NroProtocollo"));
					msgDTO.setPostedDate(rs.getString("dataPubblicazione"));
					msgDTO.setSedeMittente(rs.getString("sedeMittente"));
					msgDTO.setSubject(rs.getString("Subject"));
					msgDTO.setBody(rs.getString("Body"));
					//aggiungo alla posizione corretta l'oggetto MessaggioWS
			        listaMsgDTO[rs.getRow()-1] = msgDTO;

				}
				
			}
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaMsgDTO;
	}

	*/
	public Messaggio selectModelloById(String id_matricola, String codiceSede,String idMsg) throws Exception{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
//			"SELECT messaggio.id_messaggio,subject,body,descSedeTo,riepilogoDestinatari,id_tipoMessaggio,id_classificazione,flg_Stato,daPubblicareSuInternet,rif_numero,ufficioSede,id_matricola_approv_bozza,id_matricola_approv_messaggio" +
			String query = "SELECT messaggio.*,CONVERT(VARCHAR(10),messaggio.rif_Data, 103) as Rif_DataFormatted, messaggio_sedeDestinatari.*," +
					  " tp.descrizione as tipologiaDesc, cl.descrizione as classificazioneDesc"+
					  " FROM messaggio" +
					  " LEFT JOIN messaggio_sedeDestinatari" +
					  " ON messaggio.id_messaggio = messaggio_sedeDestinatari.id_messaggio" +
					  " LEFT JOIN tipologia AS tp ON (messaggio.id_tipoMessaggio = tp.id_tipologia)"+
					  " LEFT JOIN classificazione as cl ON (cl.id_classificazione = messaggio.id_classificazione)" + 
					  " WHERE messaggio.id_messaggio = ?"+
					  " AND messaggio.codiceSede = ?"+
			          " AND (messaggio.id_matricola = ?"+
			          " OR messaggio.id_matricola_approv_bozza = ?)"+
					  " AND messaggio.flg_Stato = ?";
			
			if(abilitaSystemOut)
			System.out.println("selectBozzaById :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg);
			pstmt.setString(2, codiceSede);
			pstmt.setString(3, id_matricola);
			pstmt.setString(4, id_matricola);
			pstmt.setInt(5, Messaggio.FLG_STATO_MODELLO);
			rs  = pstmt.executeQuery();
		
			while (rs.next()) {
				messaggio = new Messaggio();
				messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
		        messaggio.setSubject(rs.getString("subject"));
		        messaggio.setBody(rs.getString("body"));
		        messaggio.setSendTo(rs.getString("SendTo"));
		        messaggio.setDescSedeTo(rs.getString("descSedeTo"));
		        
		        messaggio.setRiepilogoDestinatari(rs.getString("riepilogoDestinatari"));
		        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));

		        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
		        messaggio.setTipologiaDesc(rs.getString("tipologiaDesc"));
		        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
		        messaggio.setClassificazioneDesc(rs.getString("classificazioneDesc"));
		        
		        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
		        messaggio.setDaInviareExInpdapEnpals(rs.getString("daInviareExInpdapEnpals"));
		        messaggio.setDaInviarePatronati(rs.getString("daInviarePatronati"));
		        
		        messaggio.setPubbProcServizio(rs.getString("pubblicazioneProceduraServizio"));
		        messaggio.setTipoProceduraServizio(rs.getString("tipoProceduraServizio"));
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        messaggio.setNomeProcServ(rs.getString("nomeProceduraServizio"));
		        
		        messaggio.setFlagStato(rs.getInt("flg_Stato"));
		        
		        messaggio.setRifNumero(rs.getString("rif_numero"));
		        messaggio.setRifData2(rs.getString("Rif_DataFormatted"));
		        messaggio.setRifSede(rs.getString("rif_sede"));
		        
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        
		        messaggio.setIdMatricolaApprovBozza(rs.getString("id_matricola_approv_bozza"));
		        messaggio.setIdMatricolaApprovaMsg(rs.getString("id_matricola_approv_messaggio"));
		        
		        messaggio.setAbilitaFC(rs.getString("abilitaFC"));
		        messaggio.setInFirmaCongiunta(rs.getString("inFirmaCongiunta"));
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}

	
	public Messaggio selectMessaggioExport(String idMsg, String codiceSede) throws Exception
	{
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;
		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
//			"SELECT messaggio.id_messaggio,subject,body,descSedeTo,riepilogoDestinatari,id_tipoMessaggio,id_classificazione,flg_Stato,daPubblicareSuInternet,rif_numero,ufficioSede,id_matricola_approv_bozza,id_matricola_approv_messaggio" +
			String query = "SELECT m.*,convert (VARCHAR, m.PostedDate, 103) as postedDateFormatted,convert (VARCHAR, m.PostedDate, 108) as postedTimeFormatted,CONVERT(VARCHAR(10),m.rif_Data, 103) as Rif_DataFormatted,"+
						    " messaggio_sedeDestinatari.*,tp.descrizione as tipologiaDesc, cl.descrizione as classificazioneDesc" +
					  " FROM messaggio AS m" +
					  " LEFT JOIN messaggio_sedeDestinatari ON m.id_messaggio = messaggio_sedeDestinatari.id_messaggio" +
					  " LEFT JOIN tipologia AS tp ON (m.id_tipoMessaggio = tp.id_tipologia)"+
					  " LEFT JOIN classificazione as cl ON (cl.id_classificazione = m.id_classificazione)" +
					  
					  " WHERE m.id_messaggio = ?";
			
			if(abilitaSystemOut)
			System.out.println("selectMessaggioExport :: " + query + " :: idMsg: " + idMsg);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg);
			
			rs  = pstmt.executeQuery();
		
			while (rs.next()) {
				messaggio = new Messaggio();
				messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
				messaggio.setIdMessaggioPadre(rs.getInt("id_messaggio_padre"));
				messaggio.setCodiceSede(rs.getString("codiceSede"));
				messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
				
//				 *** gestione messaggio inviato/ricevuto ***
				String ir = rs.getString("sendTo");
				String cs = rs.getString("codiceSede");
				
				if (rs.getInt("flg_Stato") == 2){
					
					if (codiceSede != null && !codiceSede.equals("")){
						if (ir.contains(codiceSede) && !cs.equals(codiceSede)){
							messaggio.setFlagIR("R");
						}else if (cs.equals(codiceSede)){
							messaggio.setFlagIR("I");
							messaggio.setIdMatricolaApprovaInvia(rs.getString("id_matricola_approv_invia"));
						}
					}else messaggio.setFlagIR("R");
					
				}else{
					//� ancora in bozza
					messaggio.setFlagIR("B");
				}
				
				
//				 *** fine gestione messaggio inviato/ricevuto
					 messaggio.setSubject(rs.getString("subject"));
				        messaggio.setBody(rs.getString("body"));
				        messaggio.setFlagBodyStorico(rs.getString("flg_BodyStorico"));
				        
				        messaggio.setSendTo(rs.getString("SendTo"));
				        messaggio.setDescSedeTo(rs.getString("descSedeTo"));
				        messaggio.setRiepilogoDestinatari(rs.getString("riepilogoDestinatari"));
				        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
				        messaggio.setTipologiaDesc(rs.getString("tipologiaDesc"));
				        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
				        messaggio.setClassificazioneDesc(rs.getString("classificazioneDesc"));
//				        String s=df.format(rs.getDate("PostedDate"));
//				        messaggio.setPostedDateFormatted(s);
				        messaggio.setPostedDateFormatted(rs.getString("postedDateFormatted")+" "+rs.getString("postedTimeFormatted"));
						messaggio.setPostedDate(rs.getDate("PostedDate"));
						
				        messaggio.setNroProtocollo(rs.getString("nroProtocollo"));
				        messaggio.setSegnatura(rs.getString("segnatura"));
				        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
				        
				        messaggio.setPubbProcServizio(rs.getString("pubblicazioneProceduraServizio"));
				        messaggio.setTipoProceduraServizio(rs.getString("tipoProceduraServizio"));
				        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
				        messaggio.setNomeProcServ(rs.getString("nomeProceduraServizio"));
				        
				        messaggio.setFlagStato(rs.getInt("flg_Stato"));
				        
				        messaggio.setRifNumero(rs.getString("rif_numero"));
				        messaggio.setRifData2(rs.getString("Rif_DataFormatted"));
				        messaggio.setRifSede(rs.getString("rif_sede"));
				        
				        messaggio.setUfficioSede(rs.getString("ufficioSede"));
				        
				        //codice sede mittente
				        messaggio.setCodiceSede(rs.getString("codiceSede"));
				        //descrizione della sede del mittente
				        messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
		        
		        messaggio.setIdMatricolaApprovBozza(rs.getString("id_matricola_approv_bozza"));
		        messaggio.setIdMatricolaApprovaMsg(rs.getString("id_matricola_approv_messaggio"));
		        
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return messaggio;
	}


	public ArrayList<Messaggio> selectMsgDaProtocollare() throws Exception{
		
		ArrayList<Messaggio> elencoMessaggiDaProt = new ArrayList<Messaggio>();
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;

		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT * from messaggio WITH (updlock)" +
					" WHERE flg_Stato = 2" +
					" AND PostedDate is not null" +
					" AND DeliveredDate is null";
			
			if(abilitaSystemOut)
			System.out.println("selectMsgDaProtocollare :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			rs  = pstmt.executeQuery();
		
			while (rs.next()) {
				messaggio = new Messaggio();
				messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
		        messaggio.setSubject(rs.getString("subject"));
		        messaggio.setBody(rs.getString("body"));
		        messaggio.setDescSedeTo(rs.getString("descrSede"));
		        messaggio.setCodiceSede(rs.getString("codiceSede"));
		        messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
		        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
		        
		        messaggio.setSegnatura(rs.getString("segnatura"));
		        messaggio.setNroProtocollo(rs.getString("NroProtocollo"));
		        
		        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
		        messaggio.setFlagStato(rs.getInt("flg_Stato"));
		        
		        messaggio.setRifNumero(rs.getString("rif_numero"));
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        
		        messaggio.setIdMatricolaApprovBozza(rs.getString("id_matricola_approv_bozza"));
		        messaggio.setIdMatricolaApprovaMsg(rs.getString("id_matricola_approv_messaggio"));
		        messaggio.setIdMatricolaApprovaInvia(rs.getString("id_matricola_approv_invia"));
		        
		        messaggio.setElencoDestinatari(doSelectDestFormIdMsg(wsc,messaggio.getIdMessaggio()));
		        messaggio.setElencoFileAllegati(doSelectAllegatiFormIdMsg(wsc,messaggio.getIdMessaggio()));
		        messaggio.setDaInviareExInpdapEnpals(rs.getString("daInviareExInpdapEnpals"));
		        messaggio.setDaInviarePatronati(rs.getString("daInviarePatronati"));
		        
		        elencoMessaggiDaProt.add(messaggio);
		        
		    }
			
			wsc.commit();
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				
				if(pstmt!=null)
					pstmt.close();
				
				if(wsc!=null)
					wsc.close();
				
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return elencoMessaggiDaProt;
	}
	
public ArrayList<Messaggio> selectMsgDaProtocollareMultithread(WSConnect wsc) throws Exception{
		
		ArrayList<Messaggio> elencoMessaggiDaProt = new ArrayList<Messaggio>();
		
//		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Messaggio messaggio = null;

		try
		{
			
			String query = "SELECT * from messaggio WITH (updlock)" +
					" WHERE flg_Stato = 2" +
					" AND PostedDate is not null " +
					" AND DeliveredDate is null ";
			
			if(abilitaSystemOut)
			System.out.println("selectMsgDaProtocollare :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			
			rs  = pstmt.executeQuery();
		
			while (rs.next()) {
				messaggio = new Messaggio();
				messaggio.setIdMessaggio(rs.getInt("id_messaggio"));
		        messaggio.setSubject(rs.getString("subject"));
		        messaggio.setBody(rs.getString("body"));
		        messaggio.setDescSedeTo(rs.getString("descrSede"));
		        messaggio.setCodiceSede(rs.getString("codiceSede"));
		        messaggio.setCodiceSedeDescr(rs.getString("descrSede"));
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        messaggio.setIdTipoMessaggio(rs.getInt("id_tipoMessaggio"));
		        messaggio.setIdClassificazione(rs.getInt("id_classificazione"));
		        
		        messaggio.setSegnatura(rs.getString("segnatura"));
		        messaggio.setNroProtocollo(rs.getString("NroProtocollo"));
		        
		        messaggio.setDaPubbSuInternet(rs.getString("daPubblicareSuInternet"));
		        messaggio.setFlagStato(rs.getInt("flg_Stato"));
		        
		        messaggio.setRifNumero(rs.getString("rif_numero"));
		        messaggio.setUfficioSede(rs.getString("ufficioSede"));
		        
		        messaggio.setDirigenteAreaProcServ(rs.getString("dirigenteAreaProceduraServizio"));
		        
		        messaggio.setIdMatricolaApprovBozza(rs.getString("id_matricola_approv_bozza"));
		        messaggio.setIdMatricolaApprovaMsg(rs.getString("id_matricola_approv_messaggio"));
		        messaggio.setIdMatricolaApprovaInvia(rs.getString("id_matricola_approv_invia"));
		        
		        messaggio.setElencoDestinatari(doSelectDestFormIdMsg(wsc,messaggio.getIdMessaggio()));
		        messaggio.setElencoFileAllegati(doSelectAllegatiFormIdMsg(wsc,messaggio.getIdMessaggio()));
		        messaggio.setDaInviareExInpdapEnpals(rs.getString("daInviareExInpdapEnpals"));
		        messaggio.setDaInviarePatronati(rs.getString("daInviarePatronati"));
		        
		        elencoMessaggiDaProt.add(messaggio);
		        
		    }
			
			wsc.commit();
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				
				if(pstmt!=null)
					pstmt.close();
				
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return elencoMessaggiDaProt;
	}

	private ArrayList<ComboElement> doSelectDestFormIdMsg(WSConnect wsc,Integer idMsg) {
		ArrayList<ComboElement> elencoDestinatari = new ArrayList<ComboElement>();
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT sendTo, descSedeTo" +
				" FROM messaggio_sedeDestinatari" +
		" WHERE id_messaggio = ?";
		
		if(abilitaSystemOut)
		System.out.println("*** doSelectDestFormIdMsg **** = "+query);
		
		try {
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg.toString());
			rs  = pstmt.executeQuery();
			
			while (rs.next()) {
				elencoDestinatari.addAll(StaticUtil.getListaCEFromString(rs.getString("descSedeTo"), ","));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		
		
		return elencoDestinatari;
	}
	
	private ArrayList<Allegato> doSelectAllegatiFormIdMsg(WSConnect wsc,Integer idMsg) {
		ArrayList<Allegato> elencoAllegati = null;
		Allegato allegato;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "SELECT * from allegato" +
		" WHERE id_messaggio = ?";
		try {
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg.toString());
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				elencoAllegati = new ArrayList<Allegato>();
				while (rs.next()) {
					allegato = new Allegato();
					allegato.setFileName(rs.getString("nome_allegato"));
					allegato.setFileData(rs.getBytes("file_allegato"));
					allegato.setContentType(rs.getString("contentType"));
					allegato.setFileSize(rs.getInt("fileSize"));
					elencoAllegati.add(allegato);
			    }
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		
		
		return elencoAllegati;
	}
		

	public String selectRiepilogoMsg(Integer idMsg) throws Exception{
		
		String riepilogoDest = "";
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		

		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT riepilogoDestinatari"+
					" FROM messaggio_sedeDestinatari"+
					" WHERE id_messaggio = ?";
			
			if(abilitaSystemOut)
			System.out.println("selectRiepilogoMsg :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg.toString());
			rs  = pstmt.executeQuery();
		
			while (rs.next()) {
				riepilogoDest = rs.getString("riepilogoDestinatari");   
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return riepilogoDest;
	}

	//metodo che restituisce true se il messaggio � instato inviato ma non � ancora
	//stato protocollato. Altrimenti restituisce false
	public boolean isMsgDaProtocollare(Integer idMsg) throws Exception{
		
		boolean result = false;
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			String query = "SELECT * from messaggio" +
					" WHERE flg_Stato = 2" +
					" AND PostedDate is not null" +
					" AND DeliveredDate is null" +
					" AND id_messaggio = ?";
			
			if(abilitaSystemOut)
			System.out.println("isMsgDaProtocollare :: " + query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setString(1, idMsg.toString());
			
			rs  = pstmt.executeQuery();
		
			while (rs.next()) {
				result = true;
		    }
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	}
	
	//metodo che aggiorna il messaggio: flg_Stato viene messao il valore delle bozze,
	// PostedDate e id_matricola_approv_invia vengono rimessi a NULL 
	public int annullaInvio(Integer idMsg) throws Exception{
		
		int result = -1;
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
			String query = "UPDATE messaggio" +
					" SET flg_Stato=?, PostedDate=?," +
					" id_matricola_approv_invia=?" +
					" WHERE id_messaggio = ?";
			
			if(abilitaSystemOut)
			System.out.println("annullaInvio = "+ query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, Messaggio.FLG_STATO_BOZZA);
			pstmt.setNull(2, java.sql.Types.DATE);
			pstmt.setNull(3, java.sql.Types.VARCHAR);
			pstmt.setString(4, idMsg.toString());
			
			result = pstmt.executeUpdate();

			wsc.commit();
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
				
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	
	}
	
	
	//cancellazione logica
	//metodo che aggiorna il messaggio: flg_Stato viene messao il valore delle bozze,
	// PostedDate e id_matricola_approv_invia vengono rimessi a NULL 
	public int cancellaInvio(Integer idMsg) throws Exception{
		
		int result = -1;
		
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		
		try
		{	 
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			
			
			String query = "UPDATE messaggio" +
					" SET flg_Stato=?, dataCancellazione=?" +
					" WHERE id_messaggio = ?";
			
			if(abilitaSystemOut)
			System.out.println("annullaInvio = "+ query);
			
			pstmt = wsc.getConnection().prepareStatement(query);
			pstmt.setInt(1, Messaggio.FLG_STATO_CANCELLATO);
			
			Calendar cal = Calendar.getInstance();
			java.sql.Date dataAttuale = new java.sql.Date(cal.getTimeInMillis());
			pstmt.setDate(2,dataAttuale);
			
			pstmt.setString(3, idMsg.toString());
			
			result = pstmt.executeUpdate();

			wsc.commit();
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				wsc.close();
				pstmt.close();
			}
	        catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return result;
	
	}
	
	public ArrayList<ComboElement>  config_FC_Approvatore() throws Exception
	{
	
		WSConnect wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
		
		String query = "SELECT *" +
				  " FROM configurazione_fc";
				
		if(abilitaSystemOut)
		System.out.println("config_FC_Approvatore :: " + query);
		
		pstmt = wsc.getConnection().prepareStatement(query);
		rs  = pstmt.executeQuery();
		
		ArrayList<ComboElement> listace = new ArrayList<ComboElement>();
		ComboElement ce;
		
		while (rs.next()) {
			
			ce = new ComboElement();				
			ce.setId(rs.getString("codSedeAppFC"));
			ce.setDescription(rs.getString("codSedeAppFC") + "/" + rs.getString("descSedeAppFC"));
			listace.add(ce);
			
			ce = new ComboElement();	
			ce.setId(rs.getString("matricolaDirettore"));
			ce.setDescription(rs.getString("nominativoDirettore"));
			listace.add(ce);
		}
		
		return listace;
	}



}
