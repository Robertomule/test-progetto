package it.eustema.inps.hermes.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.UUID;

import asconnection.connectionpool.WSConnect;
import asconnection.interfaccia.ApplicationServerDriver;
import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.hermes.dto.MonitorTransazioneDTO;

public class MonitorTransazioneDao extends AgentBaseDAO{
	
	private static String queryInsertTransLog = "INSERT INTO monitor_transazione " +
	"(xml_request, xml_response, data_richiesta, data_risposta, tempo_transazione, esito, desc_errore, app_utente, operation,descrizione,protocollo,n_step,idMessaggio)" +
	" VALUES ( CAST(CAST(? AS VARCHAR(max)) AS XML), CAST(CAST(? AS VARCHAR(max)) AS XML), ?, ?,?,?,?,?,?,?,?,?,?)";	
	
	public int insertMonitorTransazione (MonitorTransazioneDTO transazioneDTO) throws DAOException  {
		
		System.err.println("MonitorDAO :: insertMonitorTransazione");
		
		System.err.println("MonitorDAO :: AppUtente:" + transazioneDTO.getApp_utente());
		System.err.println("MonitorDAO :: DescErrore:" + transazioneDTO.getDesc_errore());
		System.err.println("MonitorDAO :: Esito:"+transazioneDTO.isEsito());
		System.err.println("MonitorDAO :: Operation:"+transazioneDTO.getOperation());
		System.err.println("MonitorDAO :: TempoTransazione:"+transazioneDTO.getTempo_transazione());
		System.err.println("MonitorDAO :: DataRichiesta:" + transazioneDTO.getDataRichiesta());
		System.err.println("MonitorDAO :: DataRisposta:" + transazioneDTO.getDataRisposta());
	

		WSConnect wsc = null;
		int result = 0;
		
		try{
			wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
			PreparedStatement pstmt = null;
			pstmt = wsc.getConnection().prepareStatement(queryInsertTransLog);
			
			pstmt.setString(1, transazioneDTO.getXml_request());
			pstmt.setString(2, transazioneDTO.getXml_response());
			pstmt.setTimestamp(3, transazioneDTO.getDataRichiesta());
			pstmt.setTimestamp(4, transazioneDTO.getDataRisposta());
			pstmt.setInt(5, transazioneDTO.getTempo_transazione());
			pstmt.setBoolean(6, transazioneDTO.isEsito());
			pstmt.setString(7, transazioneDTO.getDesc_errore());
			pstmt.setString(8, transazioneDTO.getApp_utente());
			pstmt.setString(9, transazioneDTO.getOperation());
			pstmt.setString(10, transazioneDTO.getDescrizione());
			pstmt.setString(11, transazioneDTO.getProtocollo());
			pstmt.setInt(12, transazioneDTO.getN_step());
			pstmt.setInt(13, transazioneDTO.getIdMessaggio());
						
			result = pstmt.executeUpdate();
			
			wsc.commit();
			
			if(pstmt!=null)
				pstmt.close();
			if(wsc!=null)
				wsc.close();
			return result;
			
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
	}
}
