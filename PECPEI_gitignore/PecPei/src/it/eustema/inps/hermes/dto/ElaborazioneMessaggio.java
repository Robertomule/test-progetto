package it.eustema.inps.hermes.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class ElaborazioneMessaggio implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer idMessaggio;
	
	private Date dataElaborazione;
	
	public ElaborazioneMessaggio() {
		
	}

}
