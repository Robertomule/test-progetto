package it.eustema.inps.hermes.dto;

import java.io.InputStream;
import java.io.Serializable;

public class Allegato implements Serializable{

	private static final long serialVersionUID = 1L;
	private String fileName;
	private byte[] fileData;
	private String contentType;
	private InputStream inputStream;
	private int fileSize;
	
	
	public int getFileSize() {
		return fileSize;
	}
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public byte[] getFileData() {
		return fileData;
	}
	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	

}
