package it.eustema.inps.hermes.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class GAUtente implements Serializable{

	private static final long serialVersionUID = 1L;
	private String matricola;
	private String login;
	private String nome;
	private String cognome;
	private String email;
	private String idSede;
	private String descSede;
	private int tipoUtente;
	
	private String sede;
	
	private ArrayList<Sede> listaSedi;
	private ArrayList<Gruppo> listaGruppi;
	
	private String id_Gruppo; //campi scambio per DTO select by matricola
	private String descGruppo; //campi scambio per DTO select by matricola
	
	private String[] idGruppo;
	private ArrayList<ComboElement> comboGruppi;
	
	private String idGruppiSelezionati;
	private ArrayList<ComboElement> comboGruppiSelezionati;

	private String elencoGruppi; //campi scambio per DTO vista utente_Sede_gruppo
	
	public String getElencoGruppi() {
		return elencoGruppi;
	}

	public void setElencoGruppi(String elencoGruppi) {
		this.elencoGruppi = elencoGruppi;
	}

	public GAUtente(){
		
	}
	
	public GAUtente(GAUtente ut){
		
		this.login = ut.getLogin();
		this.matricola = ut.getMatricola();
		this.nome = ut.getNome();
		this.cognome = ut.getCognome();
		this.email = ut.getEmail();
		this.listaSedi = ut.getListaSedi();
		this.sede = ut.getSede();
	}
	
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIdSede() {
		return idSede;
	}
	public void setIdSede(String idSede) {
		this.idSede = idSede;
	}
	public ArrayList<Sede> getListaSedi() {
		return listaSedi;
	}
	public void setListaSedi(ArrayList<Sede> listaSedi) {
		this.listaSedi = listaSedi;
	}
	public ArrayList<Gruppo> getListaGruppi() {
		return listaGruppi;
	}
	public void setListaGruppi(ArrayList<Gruppo> listaGruppi) {
		this.listaGruppi = listaGruppi;
	}

	public String getDescSede() {
		return descSede;
	}

	public void setDescSede(String descSede) {
		this.descSede = descSede;
	}

	public ArrayList<ComboElement> getComboGruppiSelezionati() {
		return comboGruppiSelezionati;
	}

	public void setComboGruppiSelezionati(
			ArrayList<ComboElement> comboGruppiSelezionati) {
		this.comboGruppiSelezionati = comboGruppiSelezionati;
	}

	public String getIdGruppiSelezionati() {
		return idGruppiSelezionati;
	}

	public void setIdGruppiSelezionati(String idGruppiSelezionati) {
		this.idGruppiSelezionati = idGruppiSelezionati;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public ArrayList<ComboElement> getComboGruppi() {
		return comboGruppi;
	}

	public void setComboGruppi(ArrayList<ComboElement> comboGruppi) {
		this.comboGruppi = comboGruppi;
	}

	public String[] getIdGruppo() {
		return idGruppo;
	}

	public void setIdGruppo(String[] idGruppo) {
		this.idGruppo = idGruppo;
	}

	public String getDescGruppo() {
		return descGruppo;
	}

	public void setDescGruppo(String descGruppo) {
		this.descGruppo = descGruppo;
	}

	public String getId_Gruppo() {
		return id_Gruppo;
	}

	public void setId_Gruppo(String id_Gruppo) {
		this.id_Gruppo = id_Gruppo;
	}

	public int getTipoUtente() {
		return tipoUtente;
	}

	public void setTipoUtente(int tipoUtente) {
		this.tipoUtente = tipoUtente;
	}

	
	
	

}
