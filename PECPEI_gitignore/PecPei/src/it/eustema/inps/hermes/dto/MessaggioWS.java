package it.eustema.inps.hermes.dto;

import java.io.Serializable;

public class MessaggioWS implements Serializable {

	private static final long serialVersionUID = 1L;
	private String idMessaggio;
	private String universalID;
	private String nroProtocollo;
	private String postedDate;
	private String sedeMittente;
	private String subject;
	private String body;
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getIdMessaggio() {
		return idMessaggio;
	}
	public void setIdMessaggio(String idMessaggio) {
		this.idMessaggio = idMessaggio;
	}
	public String getNroProtocollo() {
		return nroProtocollo;
	}
	public void setNroProtocollo(String nroProtocollo) {
		this.nroProtocollo = nroProtocollo;
	}
	public String getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}
	public String getSedeMittente() {
		return sedeMittente;
	}
	public void setSedeMittente(String sedeMittente) {
		this.sedeMittente = sedeMittente;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getUniversalID() {
		return universalID;
	}
	public void setUniversalID(String universalID) {
		this.universalID = universalID;
	}
	
	
}
