package it.eustema.inps.hermes.dto;

public class StoricoAllegato {

	private int id_messaggio;
	private String UIDoc = "";
	private String UNIVERSALID = "";
	
	
	public int getId_messaggio() {
		return id_messaggio;
	}
	public void setId_messaggio(int id_messaggio) {
		this.id_messaggio = id_messaggio;
	}
	public String getUIDoc() {
		return UIDoc;
	}
	public void setUIDoc(String doc) {
		UIDoc = doc;
	}
	public String getUNIVERSALID() {
		return UNIVERSALID;
	}
	public void setUNIVERSALID(String universalid) {
		UNIVERSALID = universalid;
	}
	
}
