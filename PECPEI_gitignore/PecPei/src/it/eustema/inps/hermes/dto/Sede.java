package it.eustema.inps.hermes.dto;

import java.io.Serializable;

public class Sede implements Serializable{
	
	public Sede(){
		
	}
	
	public Sede(String idSede, String descSede){
		this.idSede = idSede;
		this.descriSede = descSede;
	}

	private static final long serialVersionUID = 1L;
	private String idSede;
	private String descriSede;
	private String tipoDirezione;
	
	public String getDescriSede() {
		return descriSede;
	}
	public void setDescriSede(String descriSede) {
		this.descriSede = descriSede;
	}
	public String getIdSede() {
		return idSede;
	}
	public void setIdSede(String idSede) {
		this.idSede = idSede;
	}
	public String getTipoDirezione() {
		return tipoDirezione;
	}
	public void setTipoDirezione(String tipoDirezione) {
		this.tipoDirezione = tipoDirezione;
	}
	
	

}
