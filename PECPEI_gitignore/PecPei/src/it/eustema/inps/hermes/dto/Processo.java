package it.eustema.inps.hermes.dto;

import java.io.Serializable;

public class Processo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private Integer idProcesso;
	private String descrizione;
	
	
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public Integer getIdProcesso() {
		return idProcesso;
	}
	public void setIdProcesso(Integer idProcesso) {
		this.idProcesso = idProcesso;
	}
	
	

}
