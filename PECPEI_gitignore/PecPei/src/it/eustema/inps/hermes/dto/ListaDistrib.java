package it.eustema.inps.hermes.dto;

import java.util.ArrayList;

public class ListaDistrib {
	
	private Integer idListaDistribuzione;
	
	
	private String descrizione;
	private String matricola;
	private String codiceSede;
	private String autore;
	
	private String loginCompositore;
	private String loginCompositoreMatricola;
	
	private String txtRiepilogo;
	
	private String idDestinatari;
	private ArrayList<ComboElement> selectDestinatari = new ArrayList<ComboElement>();
	
	private ArrayList<ComboElement> listaDestinatariSel = new ArrayList<ComboElement>();
	
	public ArrayList<ComboElement> getListaDestinatariSel() {
		return listaDestinatariSel;
	}
	public void setListaDestinatariSel(ArrayList<ComboElement> listaDestinatariSel) {
		this.listaDestinatariSel = listaDestinatariSel;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public Integer getIdListaDistribuzione() {
		return idListaDistribuzione;
	}
	public void setIdListaDistribuzione(Integer idListaDistribuzione) {
		this.idListaDistribuzione = idListaDistribuzione;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	
	public String getCodiceSede() {
		return codiceSede;
	}
	public void setCodiceSede(String codiceSede) {
		this.codiceSede = codiceSede;
	}
	public String getAutore() {
		return autore;
	}
	public void setAutore(String autore) {
		this.autore = autore;
	}
	public String getTxtRiepilogo() {
		return txtRiepilogo;
	}
	public void setTxtRiepilogo(String txtRiepilogo) {
		this.txtRiepilogo = txtRiepilogo;
	}
	public String getLoginCompositore() {
		return loginCompositore;
	}
	public void setLoginCompositore(String loginCompositore) {
		this.loginCompositore = loginCompositore;
	}
	public String getLoginCompositoreMatricola() {
		return loginCompositoreMatricola;
	}
	public void setLoginCompositoreMatricola(String loginCompositoreMatricola) {
		this.loginCompositoreMatricola = loginCompositoreMatricola;
	}
	public String getIdDestinatari() {
		return idDestinatari;
	}
	public void setIdDestinatari(String idDestinatari) {
		this.idDestinatari = idDestinatari;
	}
	public ArrayList<ComboElement> getSelectDestinatari() {
		return selectDestinatari;
	}
	public void setSelectDestinatari(ArrayList<ComboElement> selectDestinatari) {
		this.selectDestinatari = selectDestinatari;
	}

}
