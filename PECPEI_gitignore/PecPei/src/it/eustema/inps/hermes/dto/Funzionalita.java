package it.eustema.inps.hermes.dto;

import java.io.Serializable;

public class Funzionalita implements Serializable{

	private static final long serialVersionUID = 1L;
	private String idFunz;
	private String descriFunz;
	private String menuFunz;
	private String linkFunz;
	private String imgUrl;
	private String classe;
	
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getDescriFunz() {
		return descriFunz;
	}
	public void setDescriFunz(String descriFunz) {
		this.descriFunz = descriFunz;
	}
	public String getIdFunz() {
		return idFunz;
	}
	public void setIdFunz(String idFunz) {
		this.idFunz = idFunz;
	}
	public String getLinkFunz() {
		return linkFunz;
	}
	public void setLinkFunz(String linkFunz) {
		this.linkFunz = linkFunz;
	}
	public String getMenuFunz() {
		return menuFunz;
	}
	public void setMenuFunz(String menuFunz) {
		this.menuFunz = menuFunz;
	}
	
	

}
