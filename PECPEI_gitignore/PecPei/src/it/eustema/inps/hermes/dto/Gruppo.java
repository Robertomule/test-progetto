package it.eustema.inps.hermes.dto;

import java.io.Serializable;

public class Gruppo implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final int GRP_AMMINISTRATORE = 1;
	public static final int GRP_AMMINISTRATOREDISEDE = 12;
	public static final int GRP_RICHIESTA_APPROV_BOZZA = 6;
	public static final int GRP_APPROVATORE_MESSAGGIO = 5;
	
	private Integer idGruppo;
	private String descrizione;
	
//	public Gruppo(){
//		
//	}
//	public Gruppo(Integer idGruppo){
//		this.idGruppo = idGruppo;
//	}
	
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public Integer getIdGruppo() {
		return idGruppo;
	}
	public void setIdGruppo(Integer idGruppo) {
		this.idGruppo = idGruppo;
	}
	
//	//viene implementato il metodo equals 
//	//per velocizzare la ricerca dei gruppi in un arrayList
//	public boolean equals(Object obj){
//		//se � lo stesso oggetto 
//		if(obj == this){
//            return true;
//        }
//        if(obj == null || obj.getClass() != this.getClass()){
//            return false;
//        }
//        int idGruppo = ((Gruppo)obj).getIdGruppo();
//
//        //due gruppi sono uguali se hanno lo stesso id
//		if(this.idGruppo==idGruppo){
//			return true;
//		}else{
//			return false;
//		}		
//		
//	}
	

}
