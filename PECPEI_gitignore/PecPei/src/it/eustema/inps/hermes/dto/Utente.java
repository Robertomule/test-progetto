package it.eustema.inps.hermes.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class Utente implements Serializable{

	private static final long serialVersionUID = 1L;
	private String matricola;
	private String login;
	private String nome;
	private String cognome;
	private String email;
	private String idSede;
	private String descSede;
	private int tipoUtente; //1-Inps;2-Esterno;3-Consolato
	private ArrayList<Sede> listaSedi;
	private ArrayList<Gruppo> listaGruppi;
	public static final int TIPO_UTENTE_ESTERNO = 2;
	public Utente(){
		
	}
	
	public Utente(Utente ut){
		
		this.login = ut.getLogin();
		this.matricola = ut.getMatricola();
		this.nome = ut.getNome();
		this.cognome = ut.getCognome();
		this.email = ut.getEmail();
		this.listaSedi = ut.getListaSedi();
		this.tipoUtente = ut.getTipoUtente();
	}
	
	public int getTipoUtente() {
		return tipoUtente;
	}

	public void setTipoUtente(int tipoUtente) {
		this.tipoUtente = tipoUtente;
	}

	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIdSede() {
		return idSede;
	}
	public void setIdSede(String idSede) {
		this.idSede = idSede;
	}
	public ArrayList<Sede> getListaSedi() {
		return listaSedi;
	}
	public void setListaSedi(ArrayList<Sede> listaSedi) {
		this.listaSedi = listaSedi;
	}
	public ArrayList<Gruppo> getListaGruppi() {
		return listaGruppi;
	}
	public void setListaGruppi(ArrayList<Gruppo> listaGruppi) {
		this.listaGruppi = listaGruppi;
	}

	public String getDescSede() {
		return descSede;
	}

	public void setDescSede(String descSede) {
		this.descSede = descSede;
	}
	
	

}
