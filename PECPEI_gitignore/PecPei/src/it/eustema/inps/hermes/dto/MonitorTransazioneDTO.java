package it.eustema.inps.hermes.dto;

import it.eustema.inps.agentPecPei.dto.DTO;
import java.io.Serializable;
import java.sql.Timestamp;

public class MonitorTransazioneDTO implements Serializable,DTO {
	
	private static final long serialVersionUID = 1L;

	private int id_transazione;
	private String xml_request;
	private String xml_response;
	private Timestamp dataRichiesta;
	private Timestamp dataRisposta;
	private int tempo_transazione;
	private boolean esito;
	private String desc_errore;
	private String app_utente;
	private String operation;
	private String descrizione;
	private String protocollo;
	private int n_step;
	private int idMessaggio;
	
	
	public int getId_transazione() {
		return id_transazione;
	}
	public void setId_transazione(int id_transazione) {
		this.id_transazione = id_transazione;
	}
	public String getXml_request() {
		return xml_request;
	}
	public void setXml_request(String xml_request) {
		this.xml_request = xml_request;
	}
	public String getXml_response() {
		return xml_response;
	}
	public void setXml_response(String xml_response) {
		this.xml_response = xml_response;
	}
	public int getTempo_transazione() {
		return tempo_transazione;
	}
	public void setTempo_transazione(int tempo_transazione) {
		this.tempo_transazione = tempo_transazione;
	}
	public boolean isEsito() {
		return esito;
	}
	public void setEsito(boolean esito) {
		this.esito = esito;
	}
	public String getDesc_errore() {
		return desc_errore;
	}
	public void setDesc_errore(String desc_errore) {
		this.desc_errore = desc_errore;
	}
	public String getApp_utente() {
		return app_utente;
	}
	public void setApp_utente(String app_utente) {
		this.app_utente = app_utente;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getProtocollo() {
		return protocollo;
	}
	public void setProtocollo(String protocollo) {
		this.protocollo = protocollo;
	}
	public int getN_step() {
		return n_step;
	}
	public void setN_step(int n_step) {
		this.n_step = n_step;
	}
	
	public int getIdMessaggio() {
		return idMessaggio;
	}
	public void setIdMessaggio(int idMessaggio) {
		this.idMessaggio = idMessaggio;
	}
	public void setDataRichiesta(Timestamp dataRichiesta) {
		this.dataRichiesta = dataRichiesta;
	}
	public Timestamp getDataRichiesta() {
		return dataRichiesta;
	}
	public void setDataRisposta(Timestamp dataRisposta) {
		this.dataRisposta = dataRisposta;
	}
	public Timestamp getDataRisposta() {
		return dataRisposta;
	}
}
