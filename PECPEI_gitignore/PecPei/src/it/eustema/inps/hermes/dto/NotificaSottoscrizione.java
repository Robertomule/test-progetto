package it.eustema.inps.hermes.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class NotificaSottoscrizione implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer idSottoscrizione;
	private Integer idMessaggio;
	private String matricola;
	private String email;
	private String subject;
	private String body;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIdMessaggio() {
		return idMessaggio;
	}

	public void setIdMessaggio(Integer idMessaggio) {
		this.idMessaggio = idMessaggio;
	}

	public NotificaSottoscrizione() {
		
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Integer getIdSottoscrizione() {
		return idSottoscrizione;
	}

	public void setIdSottoscrizione(Integer idSottoscrizione) {
		this.idSottoscrizione = idSottoscrizione;
	}

	public String getMatricola() {
		return matricola;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
