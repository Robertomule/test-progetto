package it.eustema.inps.hermes.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;


public class Messaggio implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final int FLG_STATO_CANCELLATO = 0;
	public static final int FLG_STATO_BOZZA = 1;
	public static final int FLG_STATO_INVIATO = 2;
	public static final int FLG_STATO_BOZZA_DA_APPROV = 3;
	public static final int FLG_STATO_MSG_DA_INVIARE = 4; 
	public static final int FLG_STATO_BOZZA_DA_MODIFICARE = 5;
	public static final int FLG_STATO_MODELLO = 6;
	
	private Integer idMessaggio;
	private Integer idMessaggioRif;
	private Integer idMessaggioFC; //identity della tabella messaggio_fc (usato in allegati della versione FC)
	
	private String flagIR; // I: inviato , R: ricevuto
	private String body;
	private Date dataCreazione;
	private Date dataCancellazione;
	private String dataCancellazioneFormatted;
	
	private String idMatricola;
	private Date dataProtocollo;
	private String nroProtocollo;
	
	private String iconaIR;
	private String icona;
	
	private String viewDettaglioBozza;
	private String viewDettaglioBozzaAttore;
	private String viewDettaglioBozzaStato;
	private String viewDettaglioProtocollo;
	
	private String flagBodyStorico;
	
	private String ufficioSede;
	private String segnatura;
	private String subject;
	private Integer statoPubblicato;
	private Integer statoPubbInternet;
	private Integer idTipoMessaggio;
	private String tipologiaDesc;
	private Integer idClassificazione;
	private String classificazioneDesc;
	private Integer flagStato;
	
	private Integer flagAutorizzata; //flag autorizzazione finale firma congiunta
	
//	elencoCETemporaneo :: usato per scambi temporanei di record 1 a n
	//esempio lista delle matricole da riconvertire in nomi da common
	private ArrayList<ComboElement> elencoCETemporaneo;
	
	private ArrayList<ComboElement> elencoDestinatari;
	private ArrayList<Allegato> elencoFileAllegati;
	private String rifNumero;
	private Date rifData;
	private String rifData2;
	private String rifSede;
	private String daPubbSuInternet;
	private String pubbProcServizio;
	private String tipoProceduraServizio;
	private String dirigenteAreaProcServ;
	private String nomeProcServ;
	
	private String daInviareExInpdapEnpals;
	private String daInviarePatronati;
	
	private String flagStampa;
	
	private String loginCompositore;
	private String loginApprovatoreBozza;
	private String loginApprovatoreMessaggio;
	
	private Integer numAllegati;
	
	private String codiceSede;
	private String codiceSedeDescr;
	private String labelFromTo;
	
	private String sede;
	private String universalId;
	
	private String sendTo;
	private String descSedeTo;
	private String riepilogoDestinatari;
	
	private String stampatoDa;
	
	private Date creationDate;
	private String creationDateFormatted;
	
	private Date postedDate;
	private String postedDateFormatted;
	
	private Date deliveredDate;
	private String deliveredDateFormatted;
	private String idMatricolaApprovBozza;
	private String idMatricolaApprovaMsg;
	private String idMatricolaApprovaInvia;
	
	private Integer idMessaggioPadre;
	private Integer idMessaggioFiglio;
	
	private String abilitaFC;
	private String inFirmaCongiunta;
	private ArrayList<ComboElement> selectDirettoreListFC = new ArrayList<ComboElement>();
	private ArrayList<ComboElement> selectDirezioneListFC = new ArrayList<ComboElement>();
	private ArrayList<ComboElement> selectSediListFC = new ArrayList<ComboElement>();
	private ArrayList<ComboElement> selectApprovatoriFC = new ArrayList<ComboElement>();
	
	private String idMatricolaPresaInCarico;
	private String uidoc;
	private String fromUidoc;
	
	public String getFromUidoc() {
		return fromUidoc;
	}
	public void setFromUidoc(String fromUidoc) {
		this.fromUidoc = fromUidoc;
	}
	public String getUidoc() {
		return uidoc;
	}
	public void setUidoc(String uidoc) {
		this.uidoc = uidoc;
	}
	public String getIdMatricolaPresaInCarico() {
		return idMatricolaPresaInCarico;
	}
	public void setIdMatricolaPresaInCarico(String idMatricolaPresaInCarico) {
		this.idMatricolaPresaInCarico = idMatricolaPresaInCarico;
	}
	public String getInFirmaCongiunta() {
		return inFirmaCongiunta;
	}
	public void setInFirmaCongiunta(String inFirmaCongiunta) {
		this.inFirmaCongiunta = inFirmaCongiunta;
	}
	public ArrayList<ComboElement> getSelectApprovatoriFC() {
		return selectApprovatoriFC;
	}
	public void setSelectApprovatoriFC(ArrayList<ComboElement> selectApprovatoriFC) {
		this.selectApprovatoriFC = selectApprovatoriFC;
	}
	public String getAbilitaFC() {
		return abilitaFC;
	}
	public void setAbilitaFC(String abilitaFC) {
		this.abilitaFC = abilitaFC;
	}
	public ArrayList<ComboElement> getSelectDirettoreListFC() {
		return selectDirettoreListFC;
	}
	public void setSelectDirettoreListFC(
			ArrayList<ComboElement> selectDirettoreListFC) {
		this.selectDirettoreListFC = selectDirettoreListFC;
	}
	public ArrayList<ComboElement> getSelectDirezioneListFC() {
		return selectDirezioneListFC;
	}
	public void setSelectDirezioneListFC(
			ArrayList<ComboElement> selectDirezioneListFC) {
		this.selectDirezioneListFC = selectDirezioneListFC;
	}
	public ArrayList<ComboElement> getSelectSediListFC() {
		return selectSediListFC;
	}
	public void setSelectSediListFC(ArrayList<ComboElement> selectSediListFC) {
		this.selectSediListFC = selectSediListFC;
	}
	public Integer getIdMessaggioFiglio() {
		return idMessaggioFiglio;
	}
	public void setIdMessaggioFiglio(Integer idMessaggioFiglio) {
		this.idMessaggioFiglio = idMessaggioFiglio;
	}
	public String getIdMatricolaApprovaMsg() {
		return idMatricolaApprovaMsg;
	}
	public void setIdMatricolaApprovaMsg(String idMatricolaApprovaMsg) {
		this.idMatricolaApprovaMsg = idMatricolaApprovaMsg;
	}
	public String getIdMatricolaApprovBozza() {
		return idMatricolaApprovBozza;
	}
	public void setIdMatricolaApprovBozza(String idMatricolaApprovBozza) {
		this.idMatricolaApprovBozza = idMatricolaApprovBozza;
	}
	public String getUfficioSede() {
		return ufficioSede;
	}
	public void setUfficioSede(String ufficioSede) {
		this.ufficioSede = ufficioSede;
	}
	public Date getDelvieredDate() {
		return deliveredDate;
	}
	
	public String getPostedDateFormatted() {
		return postedDateFormatted;

	}
	public void setPostedDateFormatted(String postedDateFormatted) {
		this.postedDateFormatted = postedDateFormatted;
	}
	public String getDeliveredDateFormatted() {
		return deliveredDateFormatted;
	}
	public void setDeliveredDateFormatted(String deliveredDateFormatted) {
		this.deliveredDateFormatted = deliveredDateFormatted;
	}
	public Date getDeliveredDate() {
		return deliveredDate;
	}
	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}
	public Date getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
	public String getRiepilogoDestinatari() {
		return riepilogoDestinatari;
	}
	public void setRiepilogoDestinatari(String riepilogoDestinatari) {
		this.riepilogoDestinatari = riepilogoDestinatari;
	}
	public String getDescSedeTo() {
		return descSedeTo;
	}
	public void setDescSedeTo(String descSedeTo) {
		this.descSedeTo = descSedeTo;
	}
	public String getSendTo() {
		return sendTo;
	}
	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}
	public Messaggio(String subject, String codiceSede,String sede){
		this.subject = subject;
		this.codiceSede = codiceSede;
		this.sede = sede;
	}
	public Messaggio() {
		
	}
	public ArrayList<Messaggio> loadMessaggio(){
		ArrayList<Messaggio>  listaMessaggi = new ArrayList<Messaggio>();
		listaMessaggi.add(new Messaggio("prova1","1","sede1"));
		listaMessaggi.add(new Messaggio("prova2","2","sede2"));
		listaMessaggi.add(new Messaggio("prova3","3","sede3"));
		listaMessaggi.add(new Messaggio("prova4","4","sede4"));
		listaMessaggi.add(new Messaggio("prova5","5","sede5"));
		listaMessaggi.add(new Messaggio("prova6","6","sede6"));
		listaMessaggi.add(new Messaggio("prova7","7","sede7"));
		listaMessaggi.add(new Messaggio("prova8","8","sede8"));
		listaMessaggi.add(new Messaggio("prova9","9","sede9"));
		listaMessaggi.add(new Messaggio("prova9","10","sede10"));
		listaMessaggi.add(new Messaggio("prova10","11","sede11"));
		listaMessaggi.add(new Messaggio("prova11","12","sede12"));
		listaMessaggi.add(new Messaggio("prova12","13","sede13"));
		listaMessaggi.add(new Messaggio("prova13","14","sede14"));
		listaMessaggi.add(new Messaggio("prova14","15","sede15"));
		listaMessaggi.add(new Messaggio("prova15","16","sede16"));
		listaMessaggi.add(new Messaggio("prova16","17","sede17"));
		listaMessaggi.add(new Messaggio("prova17","18","sede18"));
		listaMessaggi.add(new Messaggio("prova18","19","sede19"));
		listaMessaggi.add(new Messaggio("prova19","20","sede20"));
		listaMessaggi.add(new Messaggio("prova20","21","sede21"));
		listaMessaggi.add(new Messaggio("prova21","22","sede22"));
		listaMessaggi.add(new Messaggio("prova22","23","sede23"));
		listaMessaggi.add(new Messaggio("prova23","24","sede24"));
		
		
		
		return listaMessaggi;
	}
	
	public String getCodiceSede() {
		return codiceSede;
	}
	public void setCodiceSede(String codiceSede) {
		this.codiceSede = codiceSede;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getUniversalId() {
		return universalId;
	}
	public void setUniversalId(String universalId) {
		this.universalId = universalId;
	}
	public Date getDataCreazione() {
		return dataCreazione;
	}
	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	public Date getDataProtocollo() {
		return dataProtocollo;
	}
	public void setDataProtocollo(Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}
	public Integer getIdClassificazione() {
		return idClassificazione;
	}
	public void setIdClassificazione(Integer idClassificazione) {
		this.idClassificazione = idClassificazione;
	}
	public String getIdMatricola() {
		return idMatricola;
	}
	public void setIdMatricola(String idMatricola) {
		this.idMatricola = idMatricola;
	}
	
	
	public Integer getIdMessaggio() {
		return idMessaggio;
	}
	public void setIdMessaggio(Integer idMessaggio) {
		this.idMessaggio = idMessaggio;
	}
	public Integer getIdTipoMessaggio() {
		return idTipoMessaggio;
	}
	public void setIdTipoMessaggio(Integer idTipoMessaggio) {
		this.idTipoMessaggio = idTipoMessaggio;
	}
	public String getSegnatura() {
		return segnatura;
	}
	public void setSegnatura(String segnatura) {
		this.segnatura = segnatura;
	}
	public Integer getStatoPubbInternet() {
		return statoPubbInternet;
	}
	public void setStatoPubbInternet(Integer statoPubbInternet) {
		this.statoPubbInternet = statoPubbInternet;
	}
	public Integer getStatoPubblicato() {
		return statoPubblicato;
	}
	public void setStatoPubblicato(Integer statoPubblicato) {
		this.statoPubblicato = statoPubblicato;
	}
	public Integer getFlagStato() {
		return flagStato;
	}
	public void setFlagStato(Integer flagStato) {
		this.flagStato = flagStato;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public ArrayList<ComboElement> getElencoDestinatari() {
		return elencoDestinatari;
	}
	public void setElencoDestinatari(ArrayList<ComboElement> elencoDestinatari) {
		this.elencoDestinatari = elencoDestinatari;
	}
	
	public ArrayList<Allegato> getElencoFileAllegati() {
		return elencoFileAllegati;
	}
	public void setElencoFileAllegati(ArrayList<Allegato> elencoFileAllegati) {
		this.elencoFileAllegati = elencoFileAllegati;
	}
	public Date getRifData() {
		return rifData;
	}
	public void setRifData(Date rifData) {
		this.rifData = rifData;
	}
	public String getRifNumero() {
		return rifNumero;
	}
	public void setRifNumero(String rifNumero) {
		this.rifNumero = rifNumero;
	}
	public String getRifSede() {
		return rifSede;
	}
	public void setRifSede(String rifSede) {
		this.rifSede = rifSede;
	}
	public String getDaPubbSuInternet() {
		return daPubbSuInternet;
	}
	public void setDaPubbSuInternet(String daPubbSuInternet) {
		this.daPubbSuInternet = daPubbSuInternet;
	}
	public String getDirigenteAreaProcServ() {
		return dirigenteAreaProcServ;
	}
	public void setDirigenteAreaProcServ(String dirigenteAreaProcServ) {
		this.dirigenteAreaProcServ = dirigenteAreaProcServ;
	}
	public String getNomeProcServ() {
		return nomeProcServ;
	}
	public void setNomeProcServ(String nomeProcServ) {
		this.nomeProcServ = nomeProcServ;
	}
	public String getPubbProcServizio() {
		return pubbProcServizio;
	}
	public void setPubbProcServizio(String pubbProcServizio) {
		this.pubbProcServizio = pubbProcServizio;
	}
	public String getTipoProceduraServizio() {
		return tipoProceduraServizio;
	}
	public void setTipoProceduraServizio(String tipoProceduraServizio) {
		this.tipoProceduraServizio = tipoProceduraServizio;
	}
	public String getLoginCompositore() {
		return loginCompositore;
	}
	public void setLoginCompositore(String loginCompositore) {
		this.loginCompositore = loginCompositore;
	}
	public Integer getNumAllegati() {
		return numAllegati;
	}
	public void setNumAllegati(Integer numAllegati) {
		this.numAllegati = numAllegati;
	}
	public String getNroProtocollo() {
		return nroProtocollo;
	}
	public void setNroProtocollo(String nroProtocollo) {
		this.nroProtocollo = nroProtocollo;
	}
	public String getViewDettaglioProtocollo() {
		return viewDettaglioProtocollo;
	}
	public void setViewDettaglioProtocollo(String viewDettaglioProtocollo) {
		this.viewDettaglioProtocollo = viewDettaglioProtocollo;
	}
	public String getLabelFromTo() {
		return labelFromTo;
	}
	public void setLabelFromTo(String labelFromTo) {
		this.labelFromTo = labelFromTo;
	}
	public String getLoginApprovatoreBozza() {
		return loginApprovatoreBozza;
	}
	public void setLoginApprovatoreBozza(String loginApprovatoreBozza) {
		this.loginApprovatoreBozza = loginApprovatoreBozza;
	}
	public String getLoginApprovatoreMessaggio() {
		return loginApprovatoreMessaggio;
	}
	public void setLoginApprovatoreMessaggio(String loginApprovatoreMessaggio) {
		this.loginApprovatoreMessaggio = loginApprovatoreMessaggio;
	}
	public String getFlagStampa() {
		return flagStampa;
	}
	public void setFlagStampa(String flagStampa) {
		this.flagStampa = flagStampa;
	}
	public String getFlagIR() {
		return flagIR;
	}
	public void setFlagIR(String flagIR) {
		this.flagIR = flagIR;
	}

	public String getStampatoDa() {
		return stampatoDa;
	}
	public void setStampatoDa(String stampatoDa) {
		this.stampatoDa = stampatoDa;
	}

	public String getCodiceSedeDescr() {
		return codiceSedeDescr;
	}
	public void setCodiceSedeDescr(String codiceSedeDescr) {
		this.codiceSedeDescr = codiceSedeDescr;
	}
	public String getClassificazioneDesc() {
		return classificazioneDesc;
	}
	public void setClassificazioneDesc(String classificazioneDesc) {
		this.classificazioneDesc = classificazioneDesc;
	}
	public String getTipologiaDesc() {
		return tipologiaDesc;
	}
	public void setTipologiaDesc(String tipologiaDesc) {
		this.tipologiaDesc = tipologiaDesc;
	}
	public String getFlagBodyStorico() {
		return flagBodyStorico;
	}
	public void setFlagBodyStorico(String flagBodyStorico) {
		this.flagBodyStorico = flagBodyStorico;
	}
	public ArrayList<ComboElement> getElencoCETemporaneo() {
		return elencoCETemporaneo;
	}
	public void setElencoCETemporaneo(ArrayList<ComboElement> elencoCETemporaneo) {
		this.elencoCETemporaneo = elencoCETemporaneo;
	}
	public Integer getIdMessaggioPadre() {
		return idMessaggioPadre;
	}
	public void setIdMessaggioPadre(Integer idMessaggioPadre) {
		this.idMessaggioPadre = idMessaggioPadre;
	}
	public String getIdMatricolaApprovaInvia() {
		return idMatricolaApprovaInvia;
	}
	public void setIdMatricolaApprovaInvia(String idMatricolaApprovaInvia) {
		this.idMatricolaApprovaInvia = idMatricolaApprovaInvia;
	}
	public String getIconaIR() {
		return iconaIR;
	}
	public void setIconaIR(String iconaIR) {
		this.iconaIR = iconaIR;
	}
	public Date getDataCancellazione() {
		return dataCancellazione;
	}
	public void setDataCancellazione(Date dataCancellazione) {
		this.dataCancellazione = dataCancellazione;
	}
	public String getDataCancellazioneFormatted() {
		return dataCancellazioneFormatted;
	}
	public void setDataCancellazioneFormatted(String dataCancellazioneFormatted) {
		this.dataCancellazioneFormatted = dataCancellazioneFormatted;
	}
	public String getIcona() {
		return icona;
	}
	public void setIcona(String icona) {
		this.icona = icona;
	}
	public Integer getIdMessaggioRif() {
		return idMessaggioRif;
	}
	public void setIdMessaggioRif(Integer idMessaggioRif) {
		this.idMessaggioRif = idMessaggioRif;
	}
	public Integer getIdMessaggioFC() {
		return idMessaggioFC;
	}
	public void setIdMessaggioFC(Integer idMessaggioFC) {
		this.idMessaggioFC = idMessaggioFC;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationDateFormatted() {
		return creationDateFormatted;
	}
	public void setCreationDateFormatted(String creationDateFormatted) {
		this.creationDateFormatted = creationDateFormatted;
	}
	public Integer getFlagAutorizzata() {
		return flagAutorizzata;
	}
	public void setFlagAutorizzata(Integer flagAutorizzata) {
		this.flagAutorizzata = flagAutorizzata;
	}
	public String getRifData2() {
		return rifData2;
	}
	public void setRifData2(String rifData2) {
		this.rifData2 = rifData2;
	}
	public String getDaInviareExInpdapEnpals() {
		return daInviareExInpdapEnpals;
	}
	public void setDaInviareExInpdapEnpals(String daInviareExInpdapEnpals) {
		this.daInviareExInpdapEnpals = daInviareExInpdapEnpals;
	}
	public String getViewDettaglioBozza() {
		return viewDettaglioBozza;
	}
	public void setViewDettaglioBozza(String viewDettaglioBozza) {
		this.viewDettaglioBozza = viewDettaglioBozza;
	}
	public String getViewDettaglioBozzaAttore() {
		return viewDettaglioBozzaAttore;
	}
	public void setViewDettaglioBozzaAttore(String viewDettaglioBozzaAttore) {
		this.viewDettaglioBozzaAttore = viewDettaglioBozzaAttore;
	}
	public String getViewDettaglioBozzaStato() {
		return viewDettaglioBozzaStato;
	}
	public void setViewDettaglioBozzaStato(String viewDettaglioBozzaStato) {
		this.viewDettaglioBozzaStato = viewDettaglioBozzaStato;
	}
	public String getDaInviarePatronati() {
		return daInviarePatronati;
	}
	public void setDaInviarePatronati(String daInviarePatronati) {
		this.daInviarePatronati = daInviarePatronati;
	}
	
	

	

}
