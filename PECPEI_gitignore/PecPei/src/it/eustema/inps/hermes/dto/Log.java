package it.eustema.inps.hermes.dto;

import java.io.Serializable;
import java.util.Date;

public class Log implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private Date dataCreazione;
	private String dataCreazioneFormatted;
	private String azione;
	private String commento;
	
	public String getDataCreazioneFormatted() {
		return dataCreazioneFormatted;
	}
	public void setDataCreazioneFormatted(String dataCreazioneFormatted) {
		this.dataCreazioneFormatted = dataCreazioneFormatted;
	}
	public String getAzione() {
		return azione;
	}
	public void setAzione(String azione) {
		this.azione = azione;
	}
	public String getCommento() {
		return commento;
	}
	public void setCommento(String commento) {
		this.commento = commento;
	}
	public Date getDataCreazione() {
		return dataCreazione;
	}
	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	
	
	
	

}
