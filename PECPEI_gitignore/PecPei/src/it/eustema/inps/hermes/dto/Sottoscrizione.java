package it.eustema.inps.hermes.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Sottoscrizione implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer idSottoscrizione;
	private Integer id_regola;
	private Integer id_sede;
	
	private String loginCompositore;
	private String loginCompositoreMatricola;
	
	private Date dataCreazione;
	private String matricola;
	private String codiceSede;
	private String descSede;
	
	private String descrizione;
	private String cercain;
	
	private ArrayList<ComboElement> selectSediSottoscrizione = new ArrayList<ComboElement>();
	
	private ArrayList<ComboElement> elencoRegole = new ArrayList<ComboElement>();
	
	public String getCercain() {
		return cercain;
	}
	public void setCercain(String cercain) {
		this.cercain = cercain;
	}
	public ArrayList<ComboElement> getElencoRegole() {
		return elencoRegole;
	}
	public void setElencoRegole(ArrayList<ComboElement> elencoRegole) {
		this.elencoRegole = elencoRegole;
	}
	public Sottoscrizione(String oggetto, String codiceSede,String sede){
		this.descrizione = oggetto;
		
	}
	public Sottoscrizione() {
		
	}
	public Date getDataCreazione() {
		return dataCreazione;
	}
	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public Integer getId_regola() {
		return id_regola;
	}
	public void setId_regola(Integer id_regola) {
		this.id_regola = id_regola;
	}
	public Integer getId_sede() {
		return id_sede;
	}
	public void setId_sede(Integer id_sede) {
		this.id_sede = id_sede;
	}
	
	
	public Integer getIdSottoscrizione() {
		return idSottoscrizione;
	}
	public void setIdSottoscrizione(Integer idSottoscrizione) {
		this.idSottoscrizione = idSottoscrizione;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	public ArrayList<ComboElement> getSelectSediSottoscrizione() {
		return selectSediSottoscrizione;
	}
	public void setSelectSediSottoscrizione(
			ArrayList<ComboElement> selectSediSottoscrizione) {
		this.selectSediSottoscrizione = selectSediSottoscrizione;
	}
	public String getCodiceSede() {
		return codiceSede;
	}
	public void setCodiceSede(String codiceSede) {
		this.codiceSede = codiceSede;
	}
	public String getDescSede() {
		return descSede;
	}
	public void setDescSede(String descSede) {
		this.descSede = descSede;
	}
	public String getLoginCompositore() {
		return loginCompositore;
	}
	public void setLoginCompositore(String loginCompositore) {
		this.loginCompositore = loginCompositore;
	}
	public String getLoginCompositoreMatricola() {
		return loginCompositoreMatricola;
	}
	public void setLoginCompositoreMatricola(String loginCompositoreMatricola) {
		this.loginCompositoreMatricola = loginCompositoreMatricola;
	}
	

}
