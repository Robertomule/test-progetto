package it.eustema.inps.hermes.dto;

import it.eustema.inps.agentPecPei.dto.DTO;

import java.io.Serializable;
import java.util.Date;

public class NotificaErroriDTO implements Serializable, DTO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3906480902787603600L;
	
	private long ID;
	private int codice;
	private String messaggio;
	private String codiceAOO;
	private String messageID;
	private Date dataCreazione;
	
	public void setID(long iD) {
		ID = iD;
	}
	public long getID() {
		return ID;
	}
	public void setCodice(int codice) {
		this.codice = codice;
	}
	public int getCodice() {
		return codice;
	}
	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}
	public String getMessaggio() {
		return messaggio;
	}
	public void setCodiceAOO(String codiceAOO) {
		this.codiceAOO = codiceAOO;
	}
	public String getCodiceAOO() {
		return codiceAOO;
	}
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}
	public String getMessageID() {
		return messageID;
	}
	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	public Date getDataCreazione() {
		return dataCreazione;
	}
}
