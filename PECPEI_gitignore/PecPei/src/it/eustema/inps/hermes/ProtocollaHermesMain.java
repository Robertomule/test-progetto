package it.eustema.inps.hermes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import it.eustema.inps.agentPecPei.dao.Agent_execution_logDAO;
import it.eustema.inps.agentPecPei.dto.AgentExecutionLogDTO;
import it.eustema.inps.agentPecPei.util.AgentStatus;
import it.eustema.inps.hermes.dao.ComboDAO;
import it.eustema.inps.hermes.dao.HermesAgent_execution_logDAO;
import it.eustema.inps.hermes.dao.MessaggioDAO;
import it.eustema.inps.hermes.dao.MonitorTransazioneDao;
import it.eustema.inps.hermes.dao.UtenteDAO;
import it.eustema.inps.hermes.dto.Allegato;
import it.eustema.inps.hermes.dto.ComboElement;
import it.eustema.inps.hermes.dto.Messaggio;
import it.eustema.inps.hermes.dto.MonitorTransazioneDTO;
import it.eustema.inps.hermes.dto.Utente;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.Posta;
import it.eustema.inps.utility.logging.LoggingConfigurator;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;

import javax.xml.rpc.ServiceException;

import asconnection.connectionpool.WSConnect;
import asconnection.interfaccia.ApplicationServerDriver;


import sun.misc.BASE64Encoder;
import wspia2013.Service1;
import wspia2013.Service1Locator;
import wspia2013.Service1Soap;

import org.apache.commons.lang.StringEscapeUtils;

public class ProtocollaHermesMain {
	
	private boolean abilitaSystemOut = true;
	private long tempoRichiesta;
	private long tempoRisposta;
	
	
	static {
        System.out.println("ScheduleMain :: esegue blocco di inizializzazione");
		//blocco eseguito una volta al caricamento della classe
        try {
			// carica i parametri dal file di cfg
			//new ConfigProp(cfgFileName);
			new ConfigProp().init();
			// inizializzazione configurazione del logging
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
		} catch (Exception e) {
			System.err.println("" + e.getMessage());
		}        
    }
	
	
	
	
	public void run() {
		
		try{
			
			MessaggioDAO msgDAO = new MessaggioDAO();
			
//			String verifcaThread = msgDAO.insertVerificaThread();
			//msgDAO.aggiornaThread();

			/*
			 * boolean abilitaThread = false;
			   if(verifcaThread!=null)
			   abilitaThread = msgDAO.verificaThread(verifcaThread);
			*/
			
//				if(abilitaSystemOut)
//					System.out.println("--- Inizio NewProtocollaThread -----Con id----"+verifcaThread);
				
				try {
					AgentExecutionLogDTO executionLogDTO = new AgentExecutionLogDTO();
					HermesAgent_execution_logDAO execLogDAO = new HermesAgent_execution_logDAO();
					executionLogDTO.setAgent_status(AgentStatus.STARTED.name());
					executionLogDTO.setEnd_time(executionLogDTO.getStart_time());												
					try{
						int executionID = execLogDAO.insertAgentExecution(executionLogDTO);
						executionLogDTO.setExecution_id(executionID);
					} catch (Exception e) {
						e.printStackTrace();
					}
						startProtocolla();
						long tempoEndAgent = System.currentTimeMillis();
						executionLogDTO.setEnd_time(new Timestamp(tempoEndAgent));
						executionLogDTO.setAgent_status(AgentStatus.COMPLETED.name());
					try{
						execLogDAO.updateAgentExecution(executionLogDTO);
					} catch (Exception e) {
						e.printStackTrace();
					}

					
				} catch (Exception e) {
					e.printStackTrace();
				}
//				if(abilitaSystemOut)
//					System.out.println("--- Fine NewProtocollaThread -----Con id----"+verifcaThread);
				
				//Ritarda l'esecuzione del thread per i minuti presi dal file di configurazione
				
			 
		} catch (Exception e) {
//			e.printStackTrace();
		}
		
		
		
		
	}
	
	//il metodo � sincronizzato per evitare che pi� thread concorrenti possano agire
	//sugli stessi oggetti
	public void startProtocolla(){
		MessaggioDAO msgDAO = new MessaggioDAO();
		
		
		WSConnect wsc = new WSConnect(ApplicationServerDriver.DBHERMES,this.getClass().getCanonicalName());
		MonitorTransazioneDao monTraDao = new MonitorTransazioneDao();
		MonitorTransazioneDTO transDto = null;
		
		try {
			ArrayList<Messaggio> elencoMsgDaProtocollare = msgDAO.selectMsgDaProtocollareMultithread(wsc);
			
			
			//per ogni messaggio...
			if(elencoMsgDaProtocollare!=null && !elencoMsgDaProtocollare.isEmpty()){
				
				for(int i=0;i<elencoMsgDaProtocollare.size();i++){
					Messaggio messaggio = elencoMsgDaProtocollare.get(i);
					transDto = new MonitorTransazioneDTO();
					
					//messaggio.setRiepilogoDestinatari(msgDAO.selectRiepilogoMsg(messaggio.getIdMessaggio()));
					
					//genera l'xml per la protocollazione
					String xml =generaXML(messaggio);
					//System.out.println("generaXML = " + xml);
					//genera l'allegato txt che contiene il riepilogo di tutti i dati inviati nel messaggio
					Allegato documentoPrimario = generaTxt(messaggio);
					//System.out.println("generaTxt = "+new String(documentoPrimario.getFileData()));
					
					tempoRichiesta = System.currentTimeMillis(); //inizio Protocollazione
					
					//istanzia le classi costruite in base al wsdl per richiamare i metodi
					//esposti nel web service wspia 
					Service1 s1 = new Service1Locator();
					Service1Soap serviziWspia;
					serviziWspia = s1.getService1Soap();
				
				
					//listaFile contiene il txt con il riepilogo del messaggio e altri eventuali allegati
					ArrayList<Allegato> listaFile = new ArrayList<Allegato>();
					//aggiunge il file txt
					listaFile.add(documentoPrimario);
					if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
						//di seguito aggiunge la lista di eventuali allegati al messaggio
						listaFile.addAll(messaggio.getElencoFileAllegati());		
					}
					
					BASE64Encoder encoder = new BASE64Encoder();
					String[] arrAt = new String[listaFile.size()];
					//scorre tutti i file
					for(int j=0;j<listaFile.size();j++){
						//chiama l'encode Buffer per trasformare l'array di byte del singolo allegato
						//in una stringa codificata in base64. La stringa viene aggiunta alla j-esima posizione
						//dell'array
						arrAt[j] = encoder.encodeBuffer(listaFile.get(j).getFileData());
					}
					
					//chiamata al wspia per la protocollazione del messaggio contenente gli allegati
					String result;
					
					result = serviziWspia.protocollaConImmagineBlob64(ConfigProp.codAppHermes,
							ConfigProp.codAMMHermes,
							ConfigProp.codAOOHermes,
							ConfigProp.codUtenteHermes,
							xml.replaceAll("&", "e"),
							arrAt);
					
					if(abilitaSystemOut)
						System.out.println("result = "+result);
					
					tempoRisposta = System.currentTimeMillis(); //fine protocollazione
					
					int start = result.indexOf("<Segnatura>") + "<Segnatura>".length() ;
					int end = result.indexOf("</Segnatura>");
					if (end > 0){
						
						//traccio la transazine di protocollazione
						transDto.setIdMessaggio(messaggio.getIdMessaggio());
						transDto.setApp_utente("pci01");
						transDto.setDataRichiesta(new Timestamp(tempoRichiesta));
						transDto.setDataRisposta(new Timestamp(tempoRisposta));
						transDto.setDesc_errore("");
						transDto.setDescrizione("Step di protocollazione messaggio Hermes");
						transDto.setEsito(true);
						transDto.setN_step(2);
						transDto.setOperation("protocollaHermes");
						transDto.setProtocollo("Hermes");
						transDto.setTempo_transazione(new Long(tempoRisposta - tempoRichiesta).intValue());
						transDto.setXml_request(xml);
						transDto.setXml_response(result);
						
						monTraDao.insertMonitorTransazione(transDto);
						//fine transazine di tracciamento protocollazione
						
						String segnatura = "";
						String nroProto = "";
						segnatura = result.substring(start, end);
						//System.out.println("**** segnatura =" + segnatura);
						//da specifica per il numero di protocollo si prendono gli ultimi sei caratteri della segnatura
						nroProto = segnatura.substring(segnatura.length()-6, segnatura.length());
						//System.out.println("**** nroProto =" + nroProto);
						
						Messaggio msgDTO = new Messaggio();
						
						msgDTO.setIdMessaggio(messaggio.getIdMessaggio());
						msgDTO.setSegnatura(segnatura);
						msgDTO.setNroProtocollo(nroProto);
						msgDTO.setDeliveredDate(new Date());
						
						tempoRichiesta = System.currentTimeMillis(); //inizio Aggiornamento del messaggio
						
//						aggiorna il messaggio con:
						// - la segnatura
						// - il numero di protocollo
						// - la delivered date
						ConfigDAO configDao = new ConfigDAO();
						int u = msgDAO.updateMsgProtocollatoMultithread(wsc, msgDTO);
						if(u<0){
							boolean riprova = true;
							int j = 0;
							int numTentativi = Integer.parseInt(configDao.getProperty("numero_massimo_tentativi_aggiornamentoProtocollo", "3"));
							int sleepTentativi = Integer.parseInt(configDao.getProperty("sleep_tentativi_aggiornamentoProtocollo", "5000"));
							while(riprova || j==numTentativi){
								try{
									u = msgDAO.updateMsgProtocollatoMultithread(wsc, msgDTO);
									riprova = false;
								}catch(Exception e){
									j++;
									Thread.sleep(sleepTentativi);
									riprova = true;
									if(j==numTentativi+1){
										riprova = false;
//										executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
//										executor.executeCommand();
									}
								}
							}
						}
						if(u>0){
							
							if(abilitaSystemOut)
							System.out.println("Aggiornato il messaggio " + msgDTO.getIdMessaggio());
							
							
							Posta posta = new Posta();
							Vector destinatari = new Vector();
							//legge dal file di configurazione se l'email del destinatario � in modalit� test: emailTest = true
							if(Boolean.parseBoolean(ConfigProp.emailTestHermes)){
								destinatari.add(ConfigProp.destinatarioTestHermes);
								
							}else{
								//recupera l'utente dalla matricola dell'approvatore della bozza
								UtenteDAO utenteDAO = new UtenteDAO();
								//prende l'email da common attraverso la matricola
								Utente approvatore = utenteDAO.selectUtenteCommonByMatricola(messaggio.getIdMatricolaApprovaInvia());
								
								//aggiunge il destinatario reale (l'email della persona selezionata per approvare la bozza)
								
								if (approvatore!=null && approvatore.getEmail() != null 
										&& !approvatore.getEmail().equals("")){
									destinatari.add(approvatore.getEmail());
									
								}else{
									//l'utente non ha email percui non invio notifica
									
									//System.out.println("TreadProtocolla :: Impossibile ricavare l'email dell'approvatore su Common per il messaggio a segnatura " + segnatura + ". La notifica di protocollazione non sar� inviata.");
								}
							}
							
							String dataProtocollo = "";
							String nrProtocollo = "";
							if (destinatari != null && destinatari.size() > 0){
								Vector destinatariCC = new Vector();
								Vector allegati = new Vector();
								nrProtocollo = segnatura.substring(23,30);
								dataProtocollo = segnatura.substring(12,22);
								String linkMsg = "http://intranet.inps.it/APP01/Servizi.NetNT/PortaleComunicazione/default.aspx?msgHermes="+nrProtocollo+"&data="+dataProtocollo.replaceAll("/","-");
								System.err.println("linkMsg: "+linkMsg);
								String oggettoProtocollato = "Il Messaggio HERMES con oggetto: [" +StringEscapeUtils.escapeHtml(messaggio.getSubject()) + "] � stato inviato con numero di protocollo [" + msgDTO.getSegnatura() + "]";
								String testoEmail = "Oggetto del Messaggio Inviato:"+
								"<br/>"+
								messaggio.getSubject()+
								"<br/><br/>"+
								"--------------------------------"+
								"<br/><br/>"+
								"Numero di Protocollo assegnato tramite il sistema del Protocollo Informatico: " + msgDTO.getSegnatura()+
								"<br/><br/><br/><br/>"+
								"Per qualunque chiarimento e/o problematica in merito contattare " + ConfigProp.emailInfoHermes + " via posta elettronica."+						
								"<br/><br/><br/><a href=\"" + linkMsg + "\">Visualizza Messaggio Hermes inviato</a>";

								//ArrayList<Allegato> listaAllegati = messaggio.getElencoFileAllegati();
								ArrayList<Allegato> listaAllegati = new ArrayList<Allegato>();
								try {
									
									posta.inviaEmailHermes(ConfigProp.serverPosta,
											ConfigProp.uidPosta,
											ConfigProp.pwdPosta,
											ConfigProp.HermesNotifica,
											destinatari,
											destinatariCC,
											oggettoProtocollato,
											testoEmail,
											listaAllegati);
									
									if(abilitaSystemOut)
							System.err.println("EMAIL APPROVATORE inviata correttamente");
									
								} catch (Exception e) {
									// TODO Blocco catch generato automaticamente
									e.printStackTrace();
								}
							}

							//Controllo se il messaggio � da pubblicare su internet
							if (messaggio.getDaPubbSuInternet() != null &&
									messaggio.getDaPubbSuInternet().equals("si") ){
								
//								Invio l'email da pubblicare su internet
								posta = new Posta();
								Vector destinatariDP = new Vector();
								//legge dal file di configurazione se l'email del destinatario � in modalit� test: emailTest = true
								if(Boolean.parseBoolean(ConfigProp.emailTestHermes)){
									//aggiunge il destinatario preso dal file di configurazione
									destinatariDP.add(ConfigProp.destinatarioTestHermes);
								}else{
									
									destinatariDP.add(ConfigProp.HermesToInternetHermes);
								}
								
								String mittente = messaggio.getCodiceSede();
								
								Vector destinatariCC = new Vector();
								Vector allegati = new Vector();
//								String linkMsgDP = ConfigProp.baseUrlEmailThreadStorico + "storicoMsg.do?guidAccess="+FreeAccess.getGuid(String.valueOf(messaggio.getIdMessaggio()),"") + "&vd=1&guidForm=1";
								nrProtocollo = segnatura.substring(23,30);
								dataProtocollo = segnatura.substring(12,22);
								String linkMsgDP = "http://intranet.inps.it/APP01/Servizi.NetNT/PortaleComunicazione/default.aspx?msgHermes="+nrProtocollo+"&data="+dataProtocollo.replaceAll("/","-");
								System.err.println("linkMsg: "+linkMsgDP);

								
								String oggettoDaPubblicare = "Messaggio da Pubblicare su Internet:  " + msgDTO.getSegnatura() + " - " + messaggio.getSubject();
								System.err.println("linkMsgDP: "+linkMsgDP);
								String testoEmailDP = "<a href=\"" + linkMsgDP + "\">Visualizza Messaggio Hermes</a><br><br> --------------------------- <br><br>";
									
								testoEmailDP += messaggio.getBody();
								
								
								ArrayList<Allegato> listaAllegati = messaggio.getElencoFileAllegati();
								try {
									
									posta.inviaEmailHermes(ConfigProp.serverPosta,
											ConfigProp.uidPosta,
											ConfigProp.pwdPosta,
											mittente,
											destinatariDP,
											destinatariCC,
											oggettoDaPubblicare,
											testoEmailDP,
											listaAllegati);
									
									if(abilitaSystemOut)
									System.err.println("EMAIL DA PUBBLICARE SU INTERNET inviata correttamente");
								} catch (Exception e) {
									// TODO Blocco catch generato automaticamente
									e.printStackTrace();
								}
								
							}
							//Controllo se il messaggio � da inviare a ex inpdap enpals
							if (messaggio.getDaInviareExInpdapEnpals() != null && messaggio.getDaInviareExInpdapEnpals().equals("si") )
							{
								posta = new Posta();
								Vector destinatariDP = new Vector();
								if(Boolean.parseBoolean(ConfigProp.emailTestHermes))
								{
									destinatariDP.add(ConfigProp.destinatarioTestHermes);
								}
								else
								{
									destinatariDP.add(ConfigProp.HermesDaInviareExInpdapHermes);
									destinatariDP.add(ConfigProp.HermesDaInviareExInpalsHermes);
								}
								//String mittente = messaggio.getCodiceSede();
								String mittente = "INPSComunica@inps.it";
								
								//String oggettoDaPubblicare = "Messaggio EX INPDAP ENPALS:  " + msgDTO.getSegnatura() + " - " + messaggio.getSubject();
								String oggettoDaPubblicare = "[ " + msgDTO.getSegnatura() + " ] - " + messaggio.getSubject();
								String testoEmailDP = "Mittente: " + messaggio.getCodiceSede() + "/" + messaggio.getCodiceSedeDescr() + "<br/><br/>"+
								messaggio.getBody() + "<br/><br/><br/>"+
								"=============================="+
								"<br/>"+
								"Messaggio di posta elettronica generato automaticamente dal sistema HERMES-INPS, si prega di non rispondere in quanto l'indirizzo mittente non � presidiato."+
								"<br/>"+
								"==============================";
								
								ArrayList<Allegato> listaAllegati = messaggio.getElencoFileAllegati();
								
								try 
								{
									posta.inviaEmailHermes(ConfigProp.serverPosta,ConfigProp.uidPosta,ConfigProp.pwdPosta,mittente,destinatariDP,new Vector(),oggettoDaPubblicare,testoEmailDP,listaAllegati);
									
									if(abilitaSystemOut)
									System.err.println("EMAIL EX INPDAP inviata correttamente");
								} catch (Exception e) {
									e.printStackTrace();
								}
								
							}
							
//							Controllo se il messaggio � da inviare a patronati
							if (messaggio.getDaInviarePatronati() != null && messaggio.getDaInviarePatronati().equals("si") )
							{
								ComboDAO comboDAO = new ComboDAO();
								
								posta = new Posta();
								Vector destinatariSendTo = new Vector();
								Vector destinatariCC = new Vector();
								Vector destinatariBCC = new Vector();
								
								if(Boolean.parseBoolean(ConfigProp.emailTestHermes))
								{
									destinatariSendTo.add(ConfigProp.destinatarioTestHermes);
								}
								else
								{
									//Prendo i patronati dalla tabella
									//destinatariDP.add(ConfigProp.HermesDaInviareExInpdap);
									//destinatariDP.add(ConfigProp.HermesDaInviareExInpals);
									
									ArrayList<ComboElement> ce_emailSendTo = comboDAO.selectIndirizziPatronati("sendTo = 1");
									ArrayList<ComboElement> ce_emailCopyTo = comboDAO.selectIndirizziPatronati("copyTo = 1");
									ArrayList<ComboElement> ce_emailBlindCopyTo = comboDAO.selectIndirizziPatronati("blindCopyTo = 1");
									
									ComboElement ce;
									Iterator it;
									it = ce_emailSendTo.iterator();
									
									if(ce_emailSendTo!=null && !ce_emailSendTo.isEmpty()){
										while (it.hasNext()){
											ce = (ComboElement)it.next();
											destinatariSendTo.add(ce.getId());		
										}
										
									}
									
									it = ce_emailCopyTo.iterator();
								
									if(ce_emailCopyTo!=null && !ce_emailCopyTo.isEmpty()){
										while (it.hasNext()){
											ce = (ComboElement)it.next();
											destinatariCC.add(ce.getId());			
										}
										
									}
									
									it = ce_emailBlindCopyTo.iterator();
									
									if(ce_emailBlindCopyTo!=null && !ce_emailBlindCopyTo.isEmpty()){
										while (it.hasNext()){
											ce = (ComboElement)it.next();
											destinatariBCC.add(ce.getId());			
										}
										
									}
								}
								
								String mittente = "INPSComunica@inps.it";
								
								String oggettoDaPubblicare = "[ " + msgDTO.getSegnatura() + " ] - " + messaggio.getSubject();
								
								String testoEmailDP = "Mittente: " + messaggio.getCodiceSede() + "/" + messaggio.getCodiceSedeDescr() + "<br/><br/>"+
								
								messaggio.getBody() + "<br/><br/><br/>"+
								"=============================="+
								"<br/>"+
								"E-mail generata automaticamente dal sistema HERMES-INPS, si prega di non rispondere in quanto l'indirizzo mittente non � presidiato."+
								"<br/>"+
								"Ai sensi e per effetti del 'Codice in materia di protezione dei dati personali' (D.Lgs 196/2003), questa e-mail e' destinata unicamente alle persone sopra indicate e le informazioni in essa contenute sono da considerarsi strettamente riservate. E' proibito leggere, copiare, usare, comunicare, diffondere, inoltrare e trattare il contenuto della presente e-mail al di fuori delle finalit� per la quale la stessa e-mail e' stata inviata. Se avete ricevuto questo messaggio per errore, siete invitati di rispedire lo stesso al mittente e di cancellare l'originale senza aprire gli eventuali allegati presenti."+
								"<br/>"+
								"==============================";
								
								ArrayList<Allegato> listaAllegati = messaggio.getElencoFileAllegati();
								
								try 
								{
									posta.inviaEmailHermes(ConfigProp.serverPosta,
											ConfigProp.uidPosta,
											ConfigProp.pwdPosta,
											mittente,
											destinatariSendTo,
											destinatariCC,
											oggettoDaPubblicare,
											testoEmailDP,
											listaAllegati);
									
									if(abilitaSystemOut)
									System.err.println("EMAIL Patronati inviata correttamente");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							
						}//Fine controllo u
						
						tempoRisposta = System.currentTimeMillis(); //fine Aggiornamento del messaggio
						
						//traccio la transazine di aggiornamento/invio messaggio
						transDto.setDataRichiesta(new Timestamp(tempoRichiesta));
						transDto.setDataRisposta(new Timestamp(tempoRisposta));
						transDto.setDesc_errore("");
						transDto.setDescrizione("Step di Aggiornamento/Invio messaggio Hermes");
						transDto.setEsito(true);
						transDto.setN_step(3);
						transDto.setOperation("aggiornaHermes");
						transDto.setTempo_transazione(new Long(tempoRisposta - tempoRichiesta).intValue());
						transDto.setXml_request("");
						transDto.setXml_response("");
						
						monTraDao.insertMonitorTransazione(transDto);
					
					}else{//Fine controllo e
						
						//traccio la transazine di protocollazione con errore
						transDto.setIdMessaggio(messaggio.getIdMessaggio());
						transDto.setApp_utente("pci01");
						transDto.setDataRichiesta(new Timestamp(tempoRichiesta));
						transDto.setDataRisposta(new Timestamp(tempoRisposta));
						transDto.setDesc_errore(result.split("DescrizioneErrore>")[1].replace("<", "").replace("/", ""));
						transDto.setDescrizione("Step di protocollazione messaggio Hermes");
						transDto.setEsito(false);
						transDto.setN_step(2);
						transDto.setOperation("protocollaHermes");
						transDto.setProtocollo("Hermes");
						transDto.setTempo_transazione(new Long(tempoRisposta - tempoRichiesta).intValue());
						transDto.setXml_request(xml);
						transDto.setXml_response(result);
						
						monTraDao.insertMonitorTransazione(transDto);
					}
					
					
				}//Fine For

			}//fine get elenco da protocollare
			if(wsc!=null)
				wsc.close();
			
		}catch (ServiceException e) {
			//traccio la transazine di aggiornamento/invio messaggio con errore
			try{
				if(transDto != null){
					transDto.setDataRichiesta(new Timestamp(tempoRichiesta));
					transDto.setDataRisposta(new Timestamp(tempoRisposta));
					transDto.setDesc_errore("ServiceException: controllare i log");
					transDto.setDescrizione("Step di Aggiornamento/Invio messaggio Hermes");
					transDto.setEsito(false);
					transDto.setN_step(3);
					transDto.setOperation("aggiornaHermes");
					transDto.setTempo_transazione(new Long(tempoRisposta - tempoRichiesta).intValue());
					transDto.setXml_request("");
					transDto.setXml_response("");
					
					monTraDao.insertMonitorTransazione(transDto);
				}
			}catch(Exception e1){
				e1.printStackTrace();
			}
			
			e.printStackTrace();
		}catch (RemoteException e) {
			//traccio la transazine di aggiornamento/invio messaggio con errore
			try{
				if(transDto != null){
					transDto.setDataRichiesta(new Timestamp(tempoRichiesta));
					transDto.setDataRisposta(new Timestamp(tempoRisposta));
					transDto.setDesc_errore("RemoteException: controllare i log");
					transDto.setDescrizione("Step di Aggiornamento/Invio messaggio Hermes");
					transDto.setEsito(false);
					transDto.setN_step(3);
					transDto.setOperation("aggiornaHermes");
					transDto.setTempo_transazione(new Long(tempoRisposta - tempoRichiesta).intValue());
					transDto.setXml_request("");
					transDto.setXml_response("");
					
					monTraDao.insertMonitorTransazione(transDto);
				}
			}catch(Exception e1){
				e1.printStackTrace();
			}
			
			e.printStackTrace();
		}catch (Exception e) {
			//traccio la transazine di aggiornamento/invio messaggio con errore
			try{
				if(transDto != null){
					transDto.setDataRichiesta(new Timestamp(tempoRichiesta));
					transDto.setDataRisposta(new Timestamp(tempoRisposta));
					transDto.setDesc_errore("Exception: controllare i log");
					transDto.setDescrizione("Step di Aggiornamento/Invio messaggio Hermes");
					transDto.setEsito(false);
					transDto.setN_step(3);
					transDto.setOperation("aggiornaHermes");
					transDto.setTempo_transazione(new Long(tempoRisposta - tempoRichiesta).intValue());
					transDto.setXml_request("");
					transDto.setXml_response("");
					
					monTraDao.insertMonitorTransazione(transDto);
				}
			}catch(Exception e1){
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try{
				if(wsc!=null)
					wsc.close();
			}catch(Exception e){}
		}
	} 
	
	//genera il contenuto del file di testo da allegare come riepilogo
	//del messaggio che si sta inviando. Restituisce un oggetto Allegato
	private Allegato generaTxt(Messaggio messaggio) {
		Allegato allegato = new Allegato();
		String codiceSede = messaggio.getCodiceSede();
		
		UtenteDAO utenteDAO = new UtenteDAO();
		String loginInviatoDa="";
		
		try {
			loginInviatoDa = utenteDAO.selectUtenteByMatricola(messaggio.getIdMatricolaApprovaInvia()).getLogin();
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
		String nominativo = "INPS\\" + codiceSede + "\\" + messaggio.getCodiceSedeDescr();
		
		if(messaggio.getUfficioSede()!=null && !messaggio.getUfficioSede().equals("")){
			nominativo += "\\" + messaggio.getUfficioSede();
		}
		nominativo += "\\" + loginInviatoDa;
		
		
		StringBuffer testoRiepilogo = new StringBuffer("HERMES-Messaggistica Ufficiale\n\n");
		testoRiepilogo.append("Mittente: INPS.").append(codiceSede).append("\n\n");
		testoRiepilogo.append(nominativo).append("\n\n");
		//testoRiepilogo.append("Destinatari: ").append(messaggio.getRiepilogoDestinatari()).append("\n\n");
		testoRiepilogo.append("Destinatari: ").append("\n\n");
		
		if(messaggio.getElencoDestinatari()!=null && !messaggio.getElencoDestinatari().isEmpty()){
			String dest = "";
			for(int i=0;i<messaggio.getElencoDestinatari().size();i++){
				dest = messaggio.getElencoDestinatari().get(i).getDescription();
				testoRiepilogo.append(dest).append("\n");
			}
		}
		testoRiepilogo.append("\n\n\n");
		testoRiepilogo.append("OGGETTO: ").append(messaggio.getSubject()).append("\n\n");
		testoRiepilogo.append("TESTO\n\n");
		testoRiepilogo.append(messaggio.getBody()).append("\n\n");
		
		if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
			testoRiepilogo.append("Sono presenti i seguenti allegati:\n\n");
			String nomeFile="";
			String oggettoDoc="";
			for(int i=0;i<messaggio.getElencoFileAllegati().size();i++){
				nomeFile = messaggio.getElencoFileAllegati().get(i).getFileName();
				oggettoDoc = nomeFile + "(" + messaggio.getElencoFileAllegati().get(i).getFileSize() + 
					" - " + nomeFile + ")";
				
				testoRiepilogo.append("[").append(oggettoDoc).append("]\n");
			}
		}
		
		
		
		allegato.setFileName("msghermes.txt");
		//converte in array di byte la stringa con i contenuti del messaggio
		allegato.setFileData((testoRiepilogo.toString()).getBytes());
		allegato.setContentType("text/html");
		
		return allegato;
	}
	//genera la stringa del file xml necessario per la chiamata al web sercice
	private String generaXML(Messaggio messaggio) {
		String codiceSede = messaggio.getCodiceSede();
		
		UtenteDAO utenteDAO = new UtenteDAO();
		String loginInviatoDa="";
		try {
			loginInviatoDa = utenteDAO.selectUtenteByMatricola(messaggio.getIdMatricolaApprovaInvia()).getLogin();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
		String nominativo = "INPS\\" + codiceSede + "\\" + messaggio.getCodiceSedeDescr();
	
		if(messaggio.getUfficioSede()!=null && !messaggio.getUfficioSede().equals("")){
			nominativo += "\\" + messaggio.getUfficioSede();
		}
		nominativo += "\\" + loginInviatoDa;
		
		String sendTo = null;
		String descSedeTo = null;
		String classifica = messaggio.getIdTipoMessaggio().toString();
		
		if(classifica==null || classifica.equals("") || classifica.equals("0")){
			//se non � stato selezionato nessun valore prende il valore di default: 010.010
			classifica = "010.010";
		}else{
			//da specifica calcola la stringa da passare nell'xml, in base alla tipologia selezionata
			classifica = "010.0"+classifica+"0";
		}
		
		String subject = messaggio.getSubject();
		//subject = subject.replaceAll("<", "&lt;");
		//subject = subject.replaceAll(">", "&gt;");
		subject = StringEscapeUtils.escapeXml(subject);
        
		
		String dataArrivo = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
		String nomeFile = null;
		String oggettoDoc = null;
		
		StringBuffer xml = new StringBuffer("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
		xml.append("<DatiInvio>");
		xml.append("<CategoriaProtocollo>U</CategoriaProtocollo>");
		xml.append("<Mittente><Tipo>M</Tipo>");
		xml.append("<Codice>").append(codiceSede).append("</Codice>");
				
		xml.append("<Nominativo>").append(nominativo).append("</Nominativo>");
		xml.append("</Mittente>");
		
		if(messaggio.getElencoDestinatari()!=null && !messaggio.getElencoDestinatari().isEmpty()){
			//aggiunge l'elenco dei destinatari
			xml.append("<ListaDestinatari>");
			for(int i=0;i<messaggio.getElencoDestinatari().size();i++){
				xml.append("<Destinatario>");
				xml.append("<Tipo>M</Tipo>");
				sendTo = messaggio.getElencoDestinatari().get(i).getId();
				descSedeTo = messaggio.getElencoDestinatari().get(i).getDescription();
				if(descSedeTo.indexOf("/")!=-1){
					descSedeTo = descSedeTo.substring(descSedeTo.indexOf("/"));
				}
				
				xml.append("<Codice>INPS.").append(sendTo).append("</Codice>");
				xml.append("<Nominativo>INPS\\").append(descSedeTo).append("</Nominativo>");
				xml.append("</Destinatario>");
			}
			xml.append("</ListaDestinatari>");
		}
		
		xml.append("<ListaDocumenti>");
		xml.append("<Documento NomeFile=\"msghermes.txt\">");
		xml.append("<Sensibile>N</Sensibile>");
		xml.append("<Riservato>N</Riservato>");
		xml.append("<FlagProtocollo>S</FlagProtocollo>");
		//classifica, se vuoto mette classifica="010.010"
		xml.append("<Classifica>").append(classifica).append("</Classifica>");
		xml.append("<Oggetto>").append(subject).append("</Oggetto>");
		xml.append("<ProtocolloMittente Data=\"\" Protocollo=\"\"/>");
		xml.append("<DataArrivo>").append(dataArrivo).append("</DataArrivo>");
		
		if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
			xml.append("<ListaAllegati>");
			for(int i=0;i<messaggio.getElencoFileAllegati().size();i++){
				nomeFile = messaggio.getElencoFileAllegati().get(i).getFileName();
				oggettoDoc = nomeFile + "(" + messaggio.getElencoFileAllegati().get(i).getFileSize() + 
					" - " + nomeFile + ")";
				xml.append("<Documento NomeFile=\"").append(nomeFile).append("\">");
				xml.append("<Oggetto>").append(oggettoDoc).append("</Oggetto>");
				xml.append("</Documento>");
			}
			xml.append("</ListaAllegati>");
		}
		
		xml.append("</Documento>");
		xml.append("</ListaDocumenti>");
		xml.append("</DatiInvio>");
		return xml.toString();
	}
	
	
	public static void main ( String args[] ){
		new ProtocollaHermesMain().run();
//		String segnatura = "INPS.HERMES.10/12/2014.0009539";
//		String nrProtocollo = segnatura.substring(23,30);
//		String dataProtocollo = segnatura.substring(12,22);
//		System.out.println(nrProtocollo);
//		System.out.println(dataProtocollo);
	}


}
