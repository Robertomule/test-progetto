package it.eustema.inps.agentPecPei.aggiornaTitolari;


import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

public class TitolarioDAO extends AgentBaseDAO{
	
	public int insertTitolarioSede(ArrayList<Titolario> listaTitolari,String idSede) throws Exception
	{
		int key = -1;
		
		
		Connection wsc = null;
		try
		{	 
			wsc =  cm.getConnection(Constants.DB_PECPEI);
			
			if(listaTitolari!=null && !listaTitolari.isEmpty()){
				//cancella tutti i record in base alla sede
				doDeleteTitolariFromSede(wsc, idSede);
				
				for(int i=0;i<listaTitolari.size();i++){
					//inserisce tutti i titolari della sede
					key = doInsertSede(wsc, listaTitolari.get(i));
				}
			}
			
			wsc.commit();
			// System.out.println("****Commit eseguito****");
				
			
			
		}
		catch( Exception eh )
		{
			if(wsc!=null){
				wsc.rollback();
			}
				
			throw( eh );
		}finally{
			try{
				wsc.close();
			}
	        catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		return key;
	}
	
	public int doInsertSede(Connection wsc, Titolario titolario) throws Exception{
		int key;
				
		PreparedStatement pstmt = null;
		
		String query = "INSERT INTO titolario_sede (codiceTitolario,Denominazione,pad1,desc2,codAOO,tipoTitolario)" +
				" VALUES(?,?,?,?,?,?)";
		pstmt =  wsc.prepareStatement(query);
		pstmt.setString(1, titolario.getCodiceTitolario());
		pstmt.setString(2, titolario.getDenominazione());
		pstmt.setString(3, titolario.getPad1());
		pstmt.setString(4, titolario.getDesc2());
		pstmt.setString(5, titolario.getCodAOO());
		pstmt.setString(6, titolario.getTipoTitolario());
		
		 
		key = pstmt.executeUpdate();
		
		if(pstmt!=null)
			pstmt.close();
		
		return key;
	}
	
	
	public int doDeleteTitolariFromSede(Connection wsc, String idSede) throws Exception {
		
		PreparedStatement pstmt = null;
		int result;
		String query = "DELETE FROM titolario_sede" +
		 " WHERE codAOO = ?";
		 
		
		// System.out.println("****doDeleteTitolariFromSede:: ****" + query);
		
		pstmt =  wsc.prepareStatement(query);
		
		pstmt.setString(1, idSede);
		
		result = pstmt.executeUpdate();
		if(pstmt!=null)
			pstmt.close();
		return result;
	}
	
	
}
