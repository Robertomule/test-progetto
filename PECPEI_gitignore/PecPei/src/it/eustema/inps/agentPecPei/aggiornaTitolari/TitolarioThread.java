package it.eustema.inps.agentPecPei.aggiornaTitolari;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.business.util.SendMailVerificaPecCommand;
import it.eustema.inps.agentPecPei.dao.SedeDAO;
import it.eustema.inps.agentPecPei.dto.AgentExecutionLogDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.AgentStatus;
import it.eustema.inps.hermes.dto.Sede;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.logging.LoggingConfigurator;
import it.inps.agentPec.verificaPec.dao.Agent_execution_logDAO;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.Callable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import wspia2013.Service1;
import wspia2013.Service1Locator;
import wspia2013.Service1Soap;

public class TitolarioThread implements Command, Callable<Object> {
	
	static {
		//blocco eseguito una volta al caricamento della classe
        try {
			// carica i parametri dal file di cfg
			//new ConfigProp(cfgFileName);
			new ConfigProp().init();
			// inizializzazione configurazione del logging
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
		} catch (Exception e) {
			System.err.println("" + e.getMessage());
		}        
    }
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "TitolarioThread";
	}

	@Override
	public Object execute(){
		try {
			ConfigDAO configdao = new ConfigDAO();
			int starter = Integer.parseInt(configdao.getProperty("oraStartTitolarioThread", "23"));
			int delay = Integer.parseInt(configdao.getProperty("delayDayTitolario", "30"));
				// System.out.println("--- Inizio verifica TitolarioThread ----");
				// System.out.println("Ora per cominciare = " + starter);
				// Il thread deve essere avviato ogni giorno all'ora decisa
				// da variabile su file di config
				GregorianCalendar GC = new GregorianCalendar();
				int ore = GC.get(Calendar.HOUR_OF_DAY);
				// System.out.println("Ora adesso = " + ore);
				// Se le ore sono pari al valore di starter avvio il thread e
				// aspetto per 24 ore.
				// In caso contrario aspetto per un'ora e ripeto il test sul
				// valore delle ore.
//				if (ore == starter) {
				if (true) {	
					CommandExecutor executorService = CommandExecutorManager.getInstance().getExecutor();
					try {
						synchronized(this){
							Agent_execution_logDAO execLogDAO = new Agent_execution_logDAO();
							AgentExecutionLogDTO executionLogDTO = new AgentExecutionLogDTO();
							executionLogDTO.setAgent_type(AgentExecutionLogDTO.type_titolario);
							int executionID = execLogDAO.insertAgentExecution(executionLogDTO);
							executionLogDTO.setExecution_id(executionID);
							
							Date data = new Date();
							Date dataUltimaEsecuzione = execLogDAO.selectAgentExecution(AgentExecutionLogDTO.type_titolario);
							long differenzaGiorni = (data.getTime()-dataUltimaEsecuzione.getTime()) / (1000 * 60 * 60 * 24);


//							if(differenzaGiorni==delay || dataUltimaEsecuzione==null){	
							if(true){
								executorService.setCommand( new SendMailVerificaPecCommand( new NotifyMailBusinessException(-0000, "**** Tread Elabora Titolario inizio :"+new Date()) ) );
								executorService.executeCommand();
								start();
								executorService.setCommand( new SendMailVerificaPecCommand( new NotifyMailBusinessException(-0000, "**** Tread Elabora Titolario terminato correttamente **** "+new Date()) ) );
								executorService.executeCommand();
							}
							
							long tempoEndAgent = System.currentTimeMillis();
							executionLogDTO.setEnd_time(new Timestamp(tempoEndAgent));
							executionLogDTO.setAgent_status(AgentStatus.COMPLETED.name());
							execLogDAO.updateAgentExecution(executionLogDTO);

						}
					} catch (Exception e) {
						executorService.setCommand( new SendMailVerificaPecCommand( new NotifyMailBusinessException(-0001, "**** Tread Elabora Titolario terminato con errore : "+e.getMessage()) ) );
						executorService.executeCommand();
					}
					// System.out.println("--- Fine verifica TitolarioThread ----");

				} else {
					// System.out.println("--- Fine verifica TitolarioThread ----");
				}
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public void start() throws Exception {
		
		// System.out.println("****tread Elabora Titolario****");
		Service1 s1 = new Service1Locator();
		Service1Soap serviziWspia;
		
		serviziWspia = s1.getService1Soap();
		String codiceSede ="0040";
		String xmlResult = "";
		SedeDAO sedeDAO = new SedeDAO();
		ArrayList<Sede> listaSedi = sedeDAO.selectSediAmm();
		if(listaSedi!=null && listaSedi.size()>0){
			//per ogni sede..
			for(int i=0;i<listaSedi.size();i++){
					codiceSede = listaSedi.get(i).getIdSede();
					//chiamata al servizio web con l'i-esima sede
					xmlResult = serviziWspia.getTitolarioPEI_PEC(ConfigProp.codAPP, ConfigProp.codAMM, codiceSede,"");
					
					// System.out.println("**** xmlResult = "+xmlResult);
					
					ArrayList<Titolario> listaTitolariToAdd = new ArrayList<Titolario>();
					//parser dell'xml - aggiorna per riferimento listaTitolariToAdd
					recuperaTitolari(xmlResult,codiceSede,listaTitolariToAdd);
					
					TitolarioDAO tDAO = new TitolarioDAO();
					
					tDAO.insertTitolarioSede(listaTitolariToAdd,codiceSede);
			}
		}
		
		// System.out.println("**** fine tread Elabora Titolario****");
	}

	
	private void recuperaTitolari(String xmlResult,String codiceSede,ArrayList<Titolario> listaTitolariToAdd) {
		//parserizza l'xml e valorizza per riferimento la lista di titolari da aggiungere
		try {
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		
		    InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(xmlResult));
		    
		    Document doc = db.parse(is);
		    NodeList voci = doc.getElementsByTagName("Voce");
		    if(voci!=null && voci.getLength()>0){
		    	String tipoTitolario;
		    	String codiceTitolario;
		    	String denominazione;
		    	String pad1;
		    	String cod2;
		    	String desc2;
		    	
		    	Titolario titolarioToAdd;
		    	for (int i = 0; i < voci.getLength(); i++) {
		    		// System.out.println("Voce i = "+i);
		    		Element voce = (Element) voci.item(i);
		    		if(voce!=null){
		    			tipoTitolario = getCharacterDataFromTag(voce.getElementsByTagName("Tipo_Titolario"));
		    			codiceTitolario =getCharacterDataFromTag(voce.getElementsByTagName("CodiceTitolario"));
//			    		// System.out.println("codiceTitolario = "+codiceTitolario);
			    		denominazione = getCharacterDataFromTag(voce.getElementsByTagName("Denominazione"));
//			    		// System.out.println("denominazione = "+denominazione);
			    		pad1 = getCharacterDataFromTag(voce.getElementsByTagName("pad1"));
//			    		// System.out.println("pad1 = "+pad1);
			    		cod2 = getCharacterDataFromTag(voce.getElementsByTagName("cod2"));
//			    		// System.out.println("cod2 = "+cod2);
			    		desc2 = getCharacterDataFromTag(voce.getElementsByTagName("desc2"));
//			    		// System.out.println("desc2 = "+desc2);
			    		
			    		titolarioToAdd = new Titolario();
			    		titolarioToAdd.setTipoTitolario(tipoTitolario);
			    		titolarioToAdd.setCodiceTitolario(codiceTitolario);
			    		titolarioToAdd.setDenominazione(denominazione);
			    		titolarioToAdd.setPad1(pad1);
			    		titolarioToAdd.setCod2(cod2);
			    		titolarioToAdd.setDesc2(desc2);
			    		titolarioToAdd.setCodAOO(codiceSede);
			    		listaTitolariToAdd.add(titolarioToAdd);
		    		}
		    		
		    	}
		    }
		    
		    
		} catch (ParserConfigurationException e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		}
		
	}
	
	public static String getCharacterDataFromTag(NodeList nl) {
		
		if(nl!=null && nl.getLength()>0){
			Element e = (Element) nl.item(0);
			if(e!=null){
				Node child = e.getFirstChild();
			    if (child instanceof CharacterData) {
			      CharacterData cd = (CharacterData) child;
			      return cd.getData();
			    }
			}
		}
		return "";
	    
	  }

	public String rightString(String fullString, String subString) {
		if (fullString.indexOf(subString) == -1) {
			return "";
		}
		else {
		return (fullString.substring(fullString.indexOf(subString) + subString.length(), fullString.length()));
		}
	}
		
		public String leftString(String fullString, String subString) {
			if (fullString.indexOf(subString) == -1) {
				return "";
			} else {
			return (fullString.substring(0, fullString.indexOf(subString)));
			}
		}

		@Override
		public Object call() throws Exception {
			// TODO Stub di metodo generato automaticamente
			return execute();
		}
		
		public static void main(String[] args) throws Exception{
			new ConfigProp().init();
			Service1 s1 = new Service1Locator();
			Service1Soap serviziWspia;
			
			serviziWspia = s1.getService1Soap();
			String codiceSede ="0040";
			String xmlResult = "";
			try{
				xmlResult = serviziWspia.getTitolarioPEI_PEC(ConfigProp.codAPP, ConfigProp.codAMM, codiceSede,"");
			}catch(Exception e){
				e.printStackTrace();
			}
		
		}
}
