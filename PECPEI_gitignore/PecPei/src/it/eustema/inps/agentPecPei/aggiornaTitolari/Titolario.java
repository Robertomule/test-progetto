package it.eustema.inps.agentPecPei.aggiornaTitolari;

import java.io.Serializable;


public class Titolario implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String description;
	
	private String tipoTitolario;
	private String codiceTitolario;
	private String denominazione;
	private String pad1;
	private String cod2;
	private String desc2;
	private String codAOO;
	
	
	public String getCodAOO() {
		return codAOO;
	}

	public void setCodAOO(String codAOO) {
		this.codAOO = codAOO;
	}

	public String getCodiceTitolario() {
		return codiceTitolario;
	}

	public void setCodiceTitolario(String codiceTitolario) {
		this.codiceTitolario = codiceTitolario;
	}

	public String getDenominazione() {
		return denominazione;
	}

	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}

	public String getDesc2() {
		return desc2;
	}

	public void setDesc2(String desc2) {
		this.desc2 = desc2;
	}

	public String getPad1() {
		return pad1;
	}

	public void setPad1(String pad1) {
		this.pad1 = pad1;
	}

	public Titolario(){
		
	}
	
	public Titolario(String id, String description){
		this.id = id;
		this.description = description;
		
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	//due titolari sono uguali se hanno lo stesso id
	public boolean equals(Object other){
		Titolario titolario = null;
		if(other instanceof Titolario){
			titolario = (Titolario)other;
		}
		return this.id.equals(titolario.getId());
	}

	public String getCod2() {
		return cod2;
	}

	public void setCod2(String cod2) {
		this.cod2 = cod2;
	}

	public String getTipoTitolario() {
		return tipoTitolario;
	}

	public void setTipoTitolario(String tipoTitolario) {
		this.tipoTitolario = tipoTitolario;
	}
	
}
