package it.eustema.inps.agentPecPei.invioNotificheMailInterop;

import it.eustema.inps.agentPecPei.exception.PecBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.manager.CommandExecutorMultiThread;
import it.eustema.inps.agentPecPei.manager.GestioneDestinatari_BusinessManager;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;

import java.util.List;

import org.apache.log4j.Logger;

public class InvioMailInterop_BusinessManager {
	
private static Logger log = Logger.getLogger(GestioneDestinatari_BusinessManager.class);
	
	public String elabora() throws PecBusinessException{
		
		log.info("InvioMailInterop_BusinessManager :: elabora");
		System.out.println("Inizio Elaborazione");
		String exitCode = "";
		
		try {
				GestioneNotificheInterDao dao = new GestioneNotificheInterDao();
				List<GestioneNotificheInterBean> listMessaggi = dao.getMessaggiInDn();
				
				if(listMessaggi.size() > 0){
					CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, listMessaggi.size());
					
					for ( GestioneNotificheInterBean element: listMessaggi ){
							executor.setCommand( new InvioMailInteropCommand(element) );
							executor.executeCommandAndLog();
					}
					
					List<Object> out = ((CommandExecutorMultiThread)executor).waitResult();
					
					log.info("Elaborati "+out.size());
					System.out.println("Elaborati "+out.size());
				}else{
					System.out.println("Nessun Messaggio da Elaborare!");
					log.info("Nessun Messaggio da Elaborare!");
				}
			
		} catch (Exception e) {
			e.printStackTrace();		
		}
		
		log.info("InvioMailInterop_BusinessManager :: Fine Elaborazione.");
		System.out.println("Fine Elaborazione.");
		
		return exitCode;
	}
	
}
