package it.eustema.inps.agentPecPei.invioNotificheMailInterop;

/**
 * 
 * @author r.stabile
 *
 */


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.apache.log4j.Logger;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.GestioneDestinatari_BusinessManager;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.Posta;

public class InvioMailInteropCommand implements Command, Callable<Object>{

	private static Logger log = Logger.getLogger(GestioneNotificheInterBean.class);
//	private MessaggioDTO messaggio = new MessaggioDTO();
	private AllegatoDTO allegato;
	private GestioneNotificheInterBean notifica;
	
	public InvioMailInteropCommand(GestioneNotificheInterBean bean){
		this.notifica = bean;
//		messaggio.setAccountPec(notificheInterBean.getAccountPec());
//		messaggio.setCodiceAOO(notificheInterBean.getCodiceAOO());
//		messaggio.setSegnatura(notificheInterBean.getSegnatura());
//		messaggio.setMessageId(notificheInterBean.getMessageId());
//		messaggio.setDataOraUltimaModificaTxt(notificheInterBean.getDataProtocol());
	}
	
	@Override
	public Object execute() throws BusinessException {
		
		//se non c'Ŕ la segnatura non notifico nulla
		if(notifica.getSegnatura() != null && !notifica.getSegnatura().equalsIgnoreCase(""))
		try{
			log.debug("InvioMailInteropCommand : Inizio Invio Mail Notifica InteroperabilitÓ");
			Vector<String> destinatari = new Vector<String>();
			
			Posta posta = new Posta();
			String mittente = String.format(ConfigProp.indirizzoMittenteInterop,notifica.getAccountPec());
			String testoOggetto = notifica.getTipoNotifica().split("-")[0].equalsIgnoreCase("accettazione")?"Conferma di Ricezione":"Notifica di Eccezione";
			destinatari.add(notifica.getAccountPec());
			
			//Se la protocollazione Ŕ avvenuta con eccezione invio l'allegato eccezione, altrimenti invio l'allegato conferma
			if(testoOggetto.equals("Notifica di Eccezione"))
				allegato = MessaggiDAO.creaAllegatoInterop("Eccezione", creaXmlAllegatoMail(notifica.getMessaggioNotifica()), notifica.getMessageId());
			else
				allegato = MessaggiDAO.creaAllegatoInterop("ConfermaRicezione", creaXmlAllegatoMail(notifica.getMessaggioNotifica()), notifica.getMessageId());
			
			String bodyMessaggio = creaBodyMessaggio(testoOggetto);
			
			posta.inviaInsertDBEmailAllegati(
							ConfigProp.serverPosta,
							ConfigProp.uidPosta,
							ConfigProp.pwdPosta,
							mittente, // Inserire il mittente
							destinatari, // Inserire il destinatario
							null, // Inserire il destinatario CC
							"Esito InteroperabilitÓ: "+testoOggetto, // Inserire Oggetto
							bodyMessaggio, // Inserire Testo da definire
							allegato,
							true);
			
			try{
				//Faccio l'update del nuovo stato NT del messaggio
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				GestioneNotificheInterDao dao = new GestioneNotificheInterDao();
				notifica.setStatoNotifica("NT");
				notifica.setBodyNotifica(bodyMessaggio);
				notifica.setDataNotifica(sdf.format(new java.util.Date()));
				dao.updateStatoNotifica(notifica);
				
//				MessaggioDTO messaggioInUscitaIns = (MessaggioDTO) SerializationUtils.clone(messaggio);
//				inserisciMailDB(messaggioInUscitaIns, allegato, destinatari, testoOggetto);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		catch(Exception e){
			new NotifyMailBusinessException(-6002, "Errore nell'invio email con interoperabilitÓ del messaggio "+notifica.getMessageId()+": " + e.getMessage());
		}
		
		log.debug("InvioMailInteropCommand : Fine Invio Mail Notifica InteroperabilitÓ");
		
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "InvioMailInteropCommand";
	}
	
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}
	
	public String creaBodyMessaggio(String oggetto){
		String newLine = "<br />";//System.getProperty("line.separator");
		StringBuilder testo = new StringBuilder("");
		
		testo.append(newLine+" Il messaggio con:"+newLine+"Oggetto: \""+oggetto+"\""+newLine+" Data: "+notifica.getDataProtocol() + newLine+" indirizzato a:");
		//testo.append(andataCapo+" e numero di protocollo \""+ notifica.getSegnatura() +"\""+ " del "+notifica.getDataProtocol());
		// da definire i destinatari
		String destinatari = notifica.getAccountPec();
		String destinatariCC = "";
		
		String indirizzi[] = notifica.getDestinatarioInteroperabilita().split(";");
		
		for(int x=0;x<indirizzi.length;x++)
			testo.append(newLine+"      "+indirizzi[x]); 
		
		if(oggetto.equalsIgnoreCase("Notifica di Eccezione")){
			testo.append(newLine+" Ŕ stato protocollato, senza InteroperabilitÓ, con numero di protocollo : "+notifica.getSegnatura());
			testo.append(" in quanto si presenta il seguente problema di InteroperabilitÓ: "+notifica.getMessaggioNotifica());

		}else{
			testo.append(newLine+"Ŕ stato protocollato con numero di protocollo: "+notifica.getSegnatura());
		}
		testo.append(newLine+newLine+"Struttura INPS: "+notifica.getCodiceAOO());
		testo.append(newLine+"E-mail generata automaticamente dal sistema, si prega di non scrivere a tale indirizzo");
		
		return testo.toString();
	}
	
	/*
	private void inserisciMailDB(MessaggioDTO messaggioInUscitaIns, AllegatoDTO allegato, List<String> destinatari, String testoOggetto)throws DAOException{
		List<DestinatarioDTO> dest = new ArrayList<DestinatarioDTO>();
		DestinatarioDTO destinatario = new DestinatarioDTO();
		destinatario.setAddr(destinatari.get(0));
		destinatario.setPhrase(destinatari.get(0));
		destinatario.setName(destinatari.get(0));
		destinatario.setType("Per");
		destinatario.setPostedDate(messaggio.getPostedDate());
		dest.add(destinatario);
		messaggioInUscitaIns.setElencoDestinatariPer(dest);
		messaggioInUscitaIns.setElencoDestinatariCc(new ArrayList<DestinatarioDTO>());
		messaggioInUscitaIns.setElencoDestinatariCcn(new ArrayList<DestinatarioDTO>());
		messaggioInUscitaIns.setSubject(testoOggetto+messaggio.getSubject());
		messaggioInUscitaIns.setBody(creaBodyMessaggio(testoOggetto));
		List<AllegatoDTO> allegati = new ArrayList<AllegatoDTO>();
		allegati.add(allegato);
		messaggioInUscitaIns.setElencoFileAllegati(allegati);
		messaggioInUscitaIns.setVerso("U");
		messaggioInUscitaIns.setMessageId(messaggioInUscitaIns.getMessageId().concat("-interop"));
		messaggioInUscitaIns.setInteroperabile(true);
		MessaggiDAO messaggiDAO = new MessaggiDAO();
		messaggiDAO.inserisciMessaggio(null, messaggioInUscitaIns );
	}
	
	public static boolean validateEmail(String email) {
        boolean isValid = false;
        try {
            InternetAddress internetAddress = new InternetAddress(email);
            internetAddress.validate();
            isValid = true;
        } catch (AddressException e) {
        	e.printStackTrace();
        }
        return isValid;
    }
	*/
	
	public String creaXmlAllegatoMail(String msg){
		try {
			
			String xml = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>"
							+"<Allegato>"
								+"<Messaggio>"+msg+"</Messaggio>"
							+"</Allegato>";
			
			return xml;
			
		}catch (Exception e) {			
	         e.printStackTrace();
	         return "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>"
						+"<Allegato>"
							+"<Messaggio>Errore in creazione allegato</Messaggio>"
						+"</Allegato>";
		}
	}
//	public String creaXmlAllegatoMail(){
//		try {
//			
//			//levo il body della mail dal file xml
//			String body = notifica.getBodyNotifica();
//			notifica.setBodyNotifica("");
//			
//			//creo un JAXBElement di tipo Gestionenotifica
//			JAXBElement<Gestionenotifica> notificaElement = new JAXBElement<Gestionenotifica>(new QName(Gestionenotifica.class.getSimpleName()), Gestionenotifica.class, notifica);
//			
//			//String writer usato per scrivere il jaxbElment XML in stringa
//			JAXBContext context = JAXBContext.newInstance(Gestionenotifica.class);
//			StringWriter writer = new StringWriter();
//			
//			//marshall: converto il jaxbElement nel formato xml e lo scrivo nel writer
//			context.createMarshaller().marshal(notificaElement, writer);
//			  
//			//Stampo l'xml contenente le informazioni della segnature			
//			System.out.println("\nMarshall completato:\n" + writer.toString() ); 
//			
//			//Rimetto il body nell'oggetto notifica
//			notifica.setBodyNotifica(body);
//			
//			return writer.toString();
//			
//		}catch (JAXBException e) {			
//	         e.printStackTrace();
//	         return "Errore";
//		}
//	}

}
