package it.eustema.inps.agentPecPei.invioNotificheMailInterop;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GestioneNotificheInterBean {
	
	private String MessageId = "";
	private String AccountPec = "";
	private String CodiceAOO = "";
	private String Segnatura = "";
	private String StatoNotifica = "";
	private String TipoNotifica = "";
	private String MessaggioNotifica = "";
	private String DataNotifica = "";
	private String BodyNotifica = "";
	private String DataProtocol = "";
	private String destinatarioInteroperabilita = "";
	private String oggetto = "";
	 
	public String getMessageId() {
		return MessageId;
	}
	
	@XmlElement
	public void setMessageId(String messageId) {
		MessageId = messageId;
	}
	public String getAccountPec() {
		return AccountPec;
	}
	
	@XmlElement
	public void setAccountPec(String accountPec) {
		AccountPec = accountPec;
	}
	public String getCodiceAOO() {
		return CodiceAOO;
	}
	
	@XmlElement
	public void setCodiceAOO(String codiceAOO) {
		CodiceAOO = codiceAOO;
	}
	public String getSegnatura() {
		return Segnatura;
	}
	
	@XmlElement
	public void setSegnatura(String segnatura) {
		Segnatura = segnatura;
	}
	public String getStatoNotifica() {
		return StatoNotifica;
	}
	
	@XmlElement
	public void setStatoNotifica(String statoNotifica) {
		StatoNotifica = statoNotifica;
	}
	public String getTipoNotifica() {
		return TipoNotifica;
	}
	
	@XmlElement
	public void setTipoNotifica(String tipoNotifica) {
		TipoNotifica = tipoNotifica;
	}
	public String getMessaggioNotifica() {
		return MessaggioNotifica;
	}
	
	@XmlElement
	public void setMessaggioNotifica(String messaggioNotifica) {
		MessaggioNotifica = messaggioNotifica;
	}
	public String getDataNotifica() {
		return DataNotifica;
	}
	
	@XmlElement
	public void setDataNotifica(String dataNotifica) {
		DataNotifica = dataNotifica;
	}
	public String getBodyNotifica() {
		return BodyNotifica;
	}
	
	@XmlElement
	public void setBodyNotifica(String bodyNotifica) {
		BodyNotifica = bodyNotifica;
	}
	
	@XmlElement
	public void setDataProtocol(String dataProtocol) {
		DataProtocol = dataProtocol;
	}

	public String getDataProtocol() {
		return DataProtocol;
	}

	public void setDestinatarioInteroperabilita(
			String destinatarioInteroperabilita) {
		this.destinatarioInteroperabilita = destinatarioInteroperabilita;
	}

	public String getDestinatarioInteroperabilita() {
		return destinatarioInteroperabilita;
	}

	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	public String getOggetto() {
		return oggetto;
	}
}
