package it.eustema.inps.agentPecPei.invioNotificheMailInterop;

import it.eustema.inps.agentPecPei.dao.AgentBaseDAO;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class GestioneNotificheInterDao extends AgentBaseDAO{
	
	private static final String SELECT_MSG_IN_DN = "select top 100 * from GestioneNotificheInter with(nolock) where StatoNotifica = 'DN' ";
	
	private static final String UPDATE_STATO_NOTIFICA = "update GestioneNotificheInter set StatoNotifica = ?, BodyNotifica = ?, DataNotifica = ? where messageId = ? and accountPec = ? and codiceAoo = ?";
	
	public List<GestioneNotificheInterBean> getMessaggiInDn(){

		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		List<GestioneNotificheInterBean> lista = new ArrayList<GestioneNotificheInterBean>();
		GestioneNotificheInterBean bean;
		ResultSet rs = null;
		
		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			
			pstmt = connPecPei.prepareStatement(SELECT_MSG_IN_DN);

			rs  = pstmt.executeQuery();	
			
			while(rs.next()) {
				
				bean = new GestioneNotificheInterBean();
				
				bean.setAccountPec(rs.getString("AccountPec"));
				bean.setBodyNotifica(rs.getString("bodyNotifica"));
				bean.setCodiceAOO(rs.getString("codiceAOO"));
				bean.setDataNotifica(rs.getString("dataNotifica"));
				bean.setMessageId(rs.getString("messageId"));
				bean.setSegnatura(rs.getString("segnatura"));
				bean.setStatoNotifica(rs.getString("statoNotifica"));
				bean.setTipoNotifica(rs.getString("tipoNotifica"));
				bean.setMessaggioNotifica(rs.getString("messaggioNotifica"));
				bean.setDataProtocol(rs.getString("DataProtocol"));
				bean.setDestinatarioInteroperabilita(rs.getString("Destinatario"));
				bean.setOggetto(rs.getString("oggetto"));
				
				lista.add(bean);
		    }	
			
		} catch (Exception e) {
			e.printStackTrace();

		}finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		return lista;
	}
	
	public int updateStatoNotifica(GestioneNotificheInterBean bean){
		int esito = -1;
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		
		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			
			pstmt = connPecPei.prepareStatement(UPDATE_STATO_NOTIFICA);
			pstmt.setString(1, bean.getStatoNotifica());
			pstmt.setString(2, bean.getBodyNotifica());
			pstmt.setString(3, bean.getDataNotifica());
			pstmt.setString(4, bean.getMessageId());
			pstmt.setString(5, bean.getAccountPec());
			pstmt.setString(6, bean.getCodiceAOO());

			esito  = pstmt.executeUpdate();		
			
		} catch (Exception e) {
			e.printStackTrace();

		}finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();

		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		return esito;
	}

}
