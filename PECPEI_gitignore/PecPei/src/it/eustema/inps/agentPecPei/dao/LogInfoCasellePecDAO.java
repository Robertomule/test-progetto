package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.LogInfoCasellePecDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LogInfoCasellePecDAO extends AgentBaseDAO
{
	private final static String INS_LOG_INFO_CASELLA = "INSERT INTO LogInfoCasellePec (codiceSede, accountPEC, limite, utilizzato, libero)" +											
	" VALUES(?,?,?,?,?)";
	
	public long insertLogInfo(LogInfoCasellePecDTO logInfoDTO) throws DAOException 
	{				
		long idNuovLogInfo = -1;

		if(logInfoDTO != null){
			Connection connPecPei = null;
			try {
				connPecPei = cm.getConnection(Constants.DB_PECPEI);    
				connPecPei.setAutoCommit(false);
      		
				idNuovLogInfo = doInsertLogInfo(connPecPei, logInfoDTO);									

				if(idNuovLogInfo != -1)
				{
					connPecPei.commit();
				}
			}
			catch(DAOException ex){
				ex.printStackTrace();
				
				if(connPecPei!=null)
					try {
						connPecPei.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}	
				throw ex;
			}
			catch(Exception eh){
				eh.printStackTrace();
				
				if(connPecPei!=null)
					try {
						connPecPei.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}				
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB");
			}			
			finally{
				try {
					if(connPecPei != null)
						connPecPei.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}		
		return idNuovLogInfo;
	
	}
	
	
	private long doInsertLogInfo(Connection conn, LogInfoCasellePecDTO logInfoDTO) throws DAOException 
	{		
		long idLogInfo = -1;
		PreparedStatement pstmt = null;
		ResultSet generatedKeys = null;
		try{
				pstmt = conn.prepareStatement(INS_LOG_INFO_CASELLA, Statement.RETURN_GENERATED_KEYS);			
				pstmt.setString(1, logInfoDTO.getCodiceSede());
				pstmt.setString(2, logInfoDTO.getAccountPEC());
				pstmt.setLong(3, logInfoDTO.getLimite());
				pstmt.setLong(4, logInfoDTO.getUtilizzato());
				pstmt.setLong(5, logInfoDTO.getLibero());
				
				pstmt.executeUpdate();
				generatedKeys = pstmt.getGeneratedKeys();
				if (generatedKeys.next()) {
					idLogInfo = generatedKeys.getLong(1);
				} else {
					throw new SQLException("Impossibile ottenere la chiava generata per il log casella");
				}			

				pstmt.close();
				if(generatedKeys != null) 
					try {
						generatedKeys.close();
					} catch (Exception e){}
			}
			catch(SQLException ex){
				System.err.println("Errore durante inserimento in LogInfoCasellePec: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB" );
			}
			catch(Exception ex){
				System.err.println("Errore durante inserimento in LogInfoCasellePec: " + ex.getMessage());
				ex.printStackTrace();
				throw new DAOException(ex.getMessage());
			}
			
			return idLogInfo;
		}
	
}
