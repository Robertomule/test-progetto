package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DestinatariTestDao extends AgentBaseDAO{
	
	private static Log log = LogFactory.getLog("it.eustema.inps.agentPecPei.dao.MessaggiDAO");
	
	private static final String SELECT_DESTINATARI_TEST = "select AccountLoginName from AccountsPEC where gestioneNuovoAgent = 1 AND PECTest = 2";
	
	public List<String> getDestinatariTest() throws DAOException{
		
		log.debug("Carico Destinatari Test :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<String> destTest = new ArrayList<String>();

		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_DESTINATARI_TEST);
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				destTest.add(rs.getString(1));
		    }
		}catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("Carico Destinatari Test :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}finally{
			
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		log.debug("Carico Destinatari Test :: END, trovati: " + destTest.size() +" destinatari di test" );		
		return destTest;
	}

}
