package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.IndicePADTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.agentPecPei.util.CrittografiaPasswordPEC;
import it.eustema.inps.agentPecPei.util.TripleDES;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AccountPecDAO extends AgentBaseDAO {
	
	private static Log log = LogFactory.getLog("it.eustema.inps.agentPecPei.dao.AccountPecDAO");
	
	private static final String SELECT_ACCOUNT_PEC_QRY = "SELECT AOOINPS, AOOArea, AOORegione, AccountLoginName, PasswordAccountBatch, AOO from AccountsPEC with(nolock) where AccountLoginName = ? and gestioneNuovoAgent = 0 order by DtUltimaRicezione asc";
	private static final String SELECT_ACCOUNT_PEC_QRY_TEST = "SELECT AOOINPS, AOOArea, AOORegione, AccountLoginName, PasswordAccountBatch, AOO from AccountsPEC with(nolock) where AccountLoginName = ? and PECTest = 2 order by DtUltimaRicezione asc";
	private static final String SELECT_ALL_ACCOUNT_PEC_QRY = "SELECT AOOINPS,AOOArea,AOORegione,AccountLoginName, PasswordAccountBatch, AOO from AccountsPEC with(nolock) where PECAttiva = 1 and AOO = ? and gestioneNuovoAgent = 0  order by DtUltimaRicezione asc";
	private static final String SELECT_ALL_ACCOUNT_PEC_QRY_TEST = "SELECT AOOINPS,AOOArea,AOORegione,AccountLoginName, PasswordAccountBatch, AOO from AccountsPEC with(nolock) where PECAttiva = 1 and AOO = ? and gestioneNuovoAgent = 1 and PECTest = 2  order by DtUltimaRicezione asc";
	private static final String SELECT_COD_AOO_FROM_ACCOUNT_PEC = "select distinct AOO from accountsPEC with(nolock) where gestioneNuovoAgent = 0 and idBatch = ? order by AOO asc";
	private static final String SELECT_COD_AOO_FROM_ACCOUNT_PEC_NO_ID_BATCH = "select distinct AOO from accountsPEC with(nolock) where gestioneNuovoAgent = 0 order by AOO asc";
	private static final String SELECT_COD_AOO_FROM_ACCOUNT_PEC_TEST = "select distinct AOO from accountsPEC with(nolock) where gestioneNuovoAgent = 1 and PECTest = 2";
	private static final String UPDATE_INDICE_PA_DATA_ULTIMA_RICEZIOPNE = " UPDATE  [PECPEI].[dbo].[accountsPEC] set DtUltimaRicezione = ? where AccountLoginName = ? ";
	private static final String DATA_ULTIMA_RICEZIOPNE = " select dtUltimaRicezione from AccountsPEC where AccountLoginName = ? and PecSoppressa = 0 ";
	private static final String DISABILITA_CASELLA = " UPDATE AccountsPEC SET PecAttiva = 0 where AccountLoginName = ? ";
		 

public AccountPecDTO selectAccountPec( String accountPec ) throws DAOException{
		
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AccountPecDTO account = null;
		TripleDES crittografia = new TripleDES();
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_ACCOUNT_PEC_QRY);
			pstmt.setString(1, accountPec );
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				account = new AccountPecDTO();
				account.setDescrizioneSede(rs.getString(1));
				account.setUfficioSede(rs.getString(2));
				account.setRegioneSede(rs.getString(3));
				account.setUserName(rs.getString(4));
				account.setPassword(crittografia.decrypt(rs.getString(5)));
				account.setCodAOO(rs.getString(6));
		    }
		}
		catch( Exception eh )
		{
			throw new DAOException("Errore recupero dati Account PEC");
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		
		return account;

	}

public AccountPecDTO selectAccountPecTest( String accountPec ) throws DAOException{
	
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	AccountPecDTO account = null;
	TripleDES crittografia = new TripleDES();
	try
	{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(SELECT_ACCOUNT_PEC_QRY_TEST);
		pstmt.setString(1, accountPec );
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			account = new AccountPecDTO();
			account.setDescrizioneSede(rs.getString(1));
			account.setUfficioSede(rs.getString(2));
			account.setRegioneSede(rs.getString(3));
			account.setUserName(rs.getString(4));
			account.setPassword(crittografia.decrypt(rs.getString(5)));
			account.setCodAOO(rs.getString(6));
	    }
	}
	catch( Exception eh )
	{
		throw new DAOException("Errore recupero dati Account PEC");
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	
	return account;

}

public List<AccountPecDTO> selectAllAccountPecAttivi( String codAOO ) throws DAOException{

	System.out.println("AccountPecDAO :: selectAllAccountPecAttivi :: Controllo gli account attivi");

	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	AccountPecDTO account = null;
	List<AccountPecDTO> out = new ArrayList<AccountPecDTO>();
	TripleDES crittografia  = new TripleDES();
	String passw = "";
	try
	{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		System.out.println("AccountPecDAO :: seleziono tutti gli account PEC :: Eseguo la query " + SELECT_ALL_ACCOUNT_PEC_QRY);
		pstmt = connPecPei.prepareStatement(SELECT_ALL_ACCOUNT_PEC_QRY);
		pstmt.setString(1, codAOO ); 
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			System.out.println("AccountPecDAO :: selectAllAccountPecAttivi :: Creo l'account");
			account = new AccountPecDTO();
			account.setDescrizioneSede(rs.getString(1));
			account.setUfficioSede(rs.getString(2));
			account.setRegioneSede(rs.getString(3));
			account.setUserName(rs.getString(4));
			passw = rs.getString(5);
			account.setPassword(crittografia.decrypt(passw));
			account.setCodAOO(rs.getString(6));
			out.add( account );
	    }

	}
	catch( Exception eh )
	{
		/*
		StackTraceElement[] e = eh.getStackTrace();
		for(StackTraceElement err:e){
			log.error(err.toString());
		}*/
//		log.error("Errore recupero dati degli Account PEC:  "+passw+""+eh.getMessage());
//		throw new DAOException("Errore recupero dati degli Account PEC: "+passw+""+eh.getMessage());
	}
	finally{
		System.out.println("AccountPecDao :: selectAllAccountPecAttivi() :: Provo la connessione con le utenze");
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(rs!=null)
				rs.close();
			if(pstmt!=null){
				System.out.println("AccountPecDAO :: selectAllAccountPecAttivi() :: Sono nell'if(pstmt!=null) contenuto nel try-catch");
				pstmt.close();
			}
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	
	return out;

} 

public List<AccountPecDTO> selectAllAccountPecAttiviTest( String codAOO ) throws DAOException{
	
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	AccountPecDTO account = null;
	List<AccountPecDTO> out = new ArrayList<AccountPecDTO>();
	TripleDES crittografia  = new TripleDES();
	String passw = "";
	try
	{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(SELECT_ALL_ACCOUNT_PEC_QRY_TEST);
		pstmt.setString(1, codAOO ); 
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			account = new AccountPecDTO();
			account.setDescrizioneSede(rs.getString(1));
			account.setUfficioSede(rs.getString(2));
			account.setRegioneSede(rs.getString(3));
			account.setUserName(rs.getString(4));
			passw = rs.getString(5);
			account.setPassword(crittografia.decrypt(passw));
			account.setCodAOO(rs.getString(6));
			out.add( account );
	    }

	}
	catch( Exception eh )
	{
		/*
		StackTraceElement[] e = eh.getStackTrace();
		for(StackTraceElement err:e){
			log.error(err.toString());
		}*/
//		log.error("Errore recupero dati degli Account PEC:  "+passw+""+eh.getMessage());
//		throw new DAOException("Errore recupero dati degli Account PEC: "+passw+""+eh.getMessage());
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	
	return out;

} 
 
public int updateDataUltimaRicezione( String accountName ) throws DAOException{
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	int ret;
	try
	{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(UPDATE_INDICE_PA_DATA_ULTIMA_RICEZIOPNE);
		pstmt.setTimestamp(1, new Timestamp(new Date().getTime()));
		pstmt.setString(2, accountName );
		ret  = pstmt.executeUpdate();
		
	}catch( Exception eh ){
		throw new DAOException("Errore recupero dati Account PEC");
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(pstmt!=null)
				pstmt.close();
		}catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	return ret;
}

public Long getDataUltimaRicezione(String acc)throws DAOException{
	
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	long esito = 0L;
	
	try
	{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(DATA_ULTIMA_RICEZIOPNE);
		pstmt.setString(1, acc );
		rs = pstmt.executeQuery();
		
		if(rs.next())
			esito = rs.getTimestamp(1).getTime();
		
	}catch( Exception eh ){
		throw new DAOException("Errore recupero dati Account PEC");
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(pstmt!=null)
				pstmt.close();
			if(rs != null)
				rs.close();
		}catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	return esito;
}

public int disabilitaCasella(String acc)throws DAOException{
	
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	int esito = 0;
	
	try
	{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(DISABILITA_CASELLA);
		pstmt.setString(1, acc );
		esito = pstmt.executeUpdate();
		
	}catch( Exception eh ){
		throw new DAOException("Errore recupero dati Account PEC");
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(pstmt!=null)
				pstmt.close();
		}catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	return esito;
}

public List<String> selectAllAOO(int idBatch) throws DAOException{
	
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	List<String> out = new ArrayList<String>();
	try
	{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(SELECT_COD_AOO_FROM_ACCOUNT_PEC);
		pstmt.setInt(1, idBatch);
		
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			out.add( rs.getString(1) );
	    }
	}
	catch( Exception eh )
	{
		throw new DAOException("Errore recupero dati da ACCOUNTSPEC");
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	return out;
}

public List<String> selectAllAOONoIdBatch() throws DAOException{
	
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	List<String> out = new ArrayList<String>();
	try{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(SELECT_COD_AOO_FROM_ACCOUNT_PEC_NO_ID_BATCH);
		
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			out.add( rs.getString(1) );
	    }
	}catch( Exception eh ){
		throw new DAOException("Errore recupero dati da ACCOUNTSPEC");
	}finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	return out;
}

public List<String> selectAllAOOTest() throws DAOException{
	
	Connection connPecPei = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	List<String> out = new ArrayList<String>();
	try
	{
		connPecPei = cm.getConnection(Constants.DB_PECPEI);
		pstmt = connPecPei.prepareStatement(SELECT_COD_AOO_FROM_ACCOUNT_PEC_TEST);
		rs  = pstmt.executeQuery();			
		while (rs.next()) {
			out.add( rs.getString(1) );
	    }
	}
	catch( Exception eh )
	{
		throw new DAOException("Errore recupero dati da ACCOUNTSPEC");
	}
	finally{
		try {
			if(connPecPei!=null)
				connPecPei.close();
			if(rs!=null)
				rs.close();
			if(pstmt!=null)
				pstmt.close();
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
	return out;

} 
}
