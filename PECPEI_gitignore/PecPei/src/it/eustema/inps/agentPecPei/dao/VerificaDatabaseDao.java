package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.business.util.SendMailCommandNewProto;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.manager.GestionePEC_BusinessManager;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

public class VerificaDatabaseDao extends AgentBaseDAO{
	
	private static Logger log = Logger.getLogger(GestionePEC_BusinessManager.class);
	
	private static String FREE_SPACE_PECPEI = "select sum(convert(decimal(12,2),round((a.size-fileproperty(a.name,'SpaceUsed'))/128.000,2))) as FreeSpaceMB"
		+ " from dbo.sysfiles a where filename like '%mdf' or filename like '%ndf'";	
	
	Connection con;
	
	public boolean getFreeSpace(){
		PreparedStatement pstmt = null;
		ResultSet rs;
		Double freeSpace = 0d;
		
		try{
			con = cm.getConnection(Constants.DB_PECPEI);
			pstmt = con.prepareStatement(FREE_SPACE_PECPEI);
			rs = pstmt.executeQuery();
			
			while(rs.next())
				freeSpace = rs.getDouble(1);
			
		}catch( Exception e ){
			e.printStackTrace();
			try{
				log.info("Problemi al database di PecPei. Switch sul backup " +  e.getMessage());
		    	CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		    	executor.setCommand( new SendMailCommandNewProto( new NotifyMailBusinessException( -2000, "Problemi al database di PecPei. Switch sul backup " +  e.getMessage())));
				executor.executeCommand();
	    	}catch(Exception ex){
	    		ex.printStackTrace();
	    	}finally{
	    		freeSpace = 0d;
	    	}
		}finally{
			try {
				if(con!=null)
					con.close();
				if(pstmt!=null)
					pstmt.close();
			}catch (Exception e) {
		    	e.printStackTrace();		    	
		    }
		}
		
		if(freeSpace > 0)
			return false;
		else{
			try{
				log.info("Spazio insufficiente sul db di PecPei, utilizzo il backup");
		    	CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		    	executor.setCommand( new SendMailCommandNewProto( new NotifyMailBusinessException( -2000, "Spazio insufficiente sul db di PecPei, utilizzo il backup")));
				executor.executeCommand();
	    	}catch(Exception ex){
	    		ex.printStackTrace();
	    	}
	    	
	    	return true;
		}
	}
}
