package it.eustema.inps.agentPecPei.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import it.eustema.inps.agentPecPei.dto.SedeDTO;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.hermes.dto.Sede;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SedeDAO extends AgentBaseDAO{

	private static Log log = LogFactory.getLog("it.eustema.inps.agentPecPei.dao.SedeDAO");
	
	private static String SEDI_NUOVO_PROTOCOLLO = "select distinct(codiceAOO) from sede with (nolock) where isNewProto = 1";
	
	private static final String SELECT_SEDE_FROM_AOO = 
		" select top 1 msa.Codice, msa.DescrSede, msa.DIRETTOREDISPLAY, msa.Regione, msa.Provincia, msa.CAP, msa.Citta, mca.Matricola, mca.DataUltimaVariazione "+
		" from Common.dbo.MetaprocessoSediAnalitiche msa, Common.dbo.MetaprocessoComponentiAnalitico mca "+
		" where msa.Codice = ? and " +
		" mca.MTPDepartmentChef = 1  and " +
		" mca.descrizione = 'Direzione' and "+ 
		" msa.Codice4 = mca.[Codice Sede] "+
		" order by mca.DataUltimaVariazione desc ";

	
	public ArrayList<Sede> selectSediAmm() throws Exception {
		Connection wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<Sede> listaSedi= null;
		Sede sede = null;
		try
		{
			wsc =  cm.getConnection(Constants.DB_PECPEI);
			
			String query = "SELECT DISTINCT CodiceAOO,descrizione,codRiferimento,Regione"+
						   " FROM sede"+
						   " ORDER BY CodiceAOO,descrizione";
			// System.out.println("******** selectSediAmm = " + query);
			pstmt = wsc.prepareStatement(query);
			
			rs  = pstmt.executeQuery();
			if(rs!=null){
				listaSedi = new ArrayList<Sede>();
				while (rs.next()) {
					sede = new Sede();
					sede.setIdSede(rs.getString("CodiceAOO"));
					sede.setDescriSede(rs.getString("CodiceAOO") + "/" +rs.getString("descrizione"));
//					sede.setCodRiferimento(rs.getString("codRiferimento"));
//					sede.setProfiloSede(Profilo.AMMINISTRATORE);
//					sede.setNomeRegione(rs.getString("Regione"));
					listaSedi.add(sede);
			    }
			}
			
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaSedi;
	}
	public SedeDTO selezioneSedeFromAOO(String codiceSede){ 
		SedeDTO sede = new SedeDTO();
		log.debug("selezioneSedeFromAOO :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			connPecPei = cm.getConnection(Constants.DB_COMMON);
			pstmt = connPecPei.prepareStatement(SELECT_SEDE_FROM_AOO);
			pstmt.setString(1, codiceSede);
			rs  = pstmt.executeQuery();			
			if(rs.next()) {
				sede = new SedeDTO();
				sede.setCodice(rs.getString(1).trim());
				sede.setDescrSede(rs.getString(2).trim());
				sede.setDirettore(rs.getString(3).trim());
				sede.setRegione(rs.getString(4).trim());
				sede.setProvincia(rs.getString(5).trim());
				sede.setCap(rs.getString(6).trim());
				sede.setCitta(rs.getString(7).trim());
				sede.setMatricolaDirettore(rs.getString(8).trim());
			}
		}catch(Exception e){
			log.error("Errore nel recupero della sede : "+e.getMessage());
		}finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
			    }catch (Exception e) {
			    	e.printStackTrace();
			    }
		}
		return sede;
	}
	
	public ArrayList<String> selectSediNewProto() throws Exception {
		Connection wsc = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<String> listaSedi= null;

		try
		{
			wsc =  cm.getConnection(Constants.DB_PECPEI);

			pstmt = wsc.prepareStatement(SEDI_NUOVO_PROTOCOLLO);
			
			rs  = pstmt.executeQuery();
			
			if(rs!=null){
				listaSedi = new ArrayList<String>();
				while (rs.next()) {
					
					listaSedi.add(rs.getString(1).trim());
			    }
			}
			
	
		}
		catch( Exception eh )
		{
			throw eh;
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(wsc!=null)
					wsc.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}
		return listaSedi;
	}
	
}
