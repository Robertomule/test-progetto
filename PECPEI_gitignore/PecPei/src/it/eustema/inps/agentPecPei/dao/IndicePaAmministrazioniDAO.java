package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.IndiciPaAmministrazioniTmpDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.DBConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class IndicePaAmministrazioniDAO{

	private static final String INSERT_INDICE_PA_Amministrazioni_QRY = "insert into dbo.IndiciPaAmministrazioniTmp (cod_amm,des_amm,Comune,nome_resp,cogn_resp,Cap,Provincia,Regione," +
																		"sito_istituzionale,Indirizzo,titolo_resp,tipologia_istat,tipologia_amm,acronimo,cf_validato,Cf,mail1,tipo_mail1," +
																		"mail2,tipo_mail2,mail3,tipo_mail3,mail4,tipo_mail4,mail5,tipo_mail5,url_facebook,url_twitter,url_googleplus,url_youtube,liv_accessibili) " +
																		"values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
															
	private static final String DELETE_TABLE_QRY = "DELETE FROM dbo.IndiciPaAmministrazioniTmp";
	
	public int insertIndiciAmministrazioni(List<IndiciPaAmministrazioniTmpDTO> lista ) throws DAOException{
		
		PreparedStatement pstmt = null;
		int result=0;
		Connection conn = null;
		ResultSet rs = null;
		
		try{
			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
			
			conn.setAutoCommit(false);
			pstmt =  conn.prepareStatement(INSERT_INDICE_PA_Amministrazioni_QRY);
			
			for(IndiciPaAmministrazioniTmpDTO dto: lista){
				pstmt.setString(1, dto.getCod_amm());
				pstmt.setString(2, dto.getDes_amm());
				pstmt.setString(3, dto.getComune());
				pstmt.setString(4, dto.getNome_resp());
				pstmt.setString(5, dto.getCogn_resp());
				pstmt.setString(6, dto.getCap());
				pstmt.setString(7, dto.getProvincia());
				pstmt.setString(8, dto.getRegione());
				pstmt.setString(9, dto.getSito_istituzionale());
				pstmt.setString(10, dto.getIndirizzo());
				pstmt.setString(11, dto.getTitolo_resp());
				pstmt.setString(12, dto.getTipologia_istat());
				pstmt.setString(13, dto.getTipologia_amm());
				pstmt.setString(14, dto.getAcronimo());
				pstmt.setString(15, dto.getCf_validato());
				pstmt.setString(16, dto.getCf());
				pstmt.setString(17, dto.getMail1());
				pstmt.setString(18, dto.getTipo_mail1());
				pstmt.setString(19, dto.getMail2());
				pstmt.setString(20, dto.getTipo_mail2());
				pstmt.setString(21, dto.getMail3());
				pstmt.setString(22, dto.getTipo_mail3());
				pstmt.setString(23, dto.getMail4());
				pstmt.setString(24, dto.getTipo_mail4());
				pstmt.setString(25, dto.getMail5());
				pstmt.setString(26, dto.getTipo_mail5());
				pstmt.setString(27, dto.getUrl_facebook());
				pstmt.setString(28, dto.getUrl_twitter());
				pstmt.setString(29, dto.getUrl_googleplus());
				pstmt.setString(30, dto.getUrl_youtube());
				pstmt.setString(31, dto.getLiv_accessibili());
				
				result += pstmt.executeUpdate();
			}
			
			conn.commit();
			
		}catch(Exception e){
			e.printStackTrace();
			result = 0;
			if (conn != null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
		}finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				throw new DAOException( e.getMessage() );
			}
		}
		
		return result;
	}
	
	public void deleteDataFromTable() throws DAOException{
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		
		try{
			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
			
			pstmt =  conn.prepareStatement(DELETE_TABLE_QRY);
			pstmt.executeUpdate();
			
		}catch(Exception e){
			e.printStackTrace();
			if (conn != null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
		}finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				throw new DAOException( e.getMessage() );
			}
		}
	}
}
