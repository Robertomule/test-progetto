package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.LogIndiciPaDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class LogIndiciPaDao extends AgentBaseDAO{
	
	
	private static final String SELECT_TOP_FIVE = "select TOP(5) * from LogEsecuzioniIndiciPa";
	private static final String INSERT_LOG_ROW = "insert into LogEsecuzioniIndiciPa (DataInizio,DataFine,TotaleIndLav) values (?,?,?) ";
	
	public List<LogIndiciPaDTO> getTopFive() throws DAOException{
		
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<LogIndiciPaDTO> logIndPaList = new ArrayList<LogIndiciPaDTO>();
		LogIndiciPaDTO logTmp;
		
		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_TOP_FIVE);
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				logTmp = new LogIndiciPaDTO();
				
				logTmp.setId(rs.getInt(1));
				logTmp.setDataInizio(rs.getTimestamp(2));
				logTmp.setDataFine(rs.getTimestamp(3));
				logTmp.setTotaleIndLav(rs.getInt(4));
				
				logIndPaList.add(logTmp);
		    }
		}catch( Exception eh )
		{
			eh.printStackTrace();
			throw new DAOException(eh.getMessage());
		}finally{
			
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		return logIndPaList;
	}
	
	public int insertLogEsecuzione( LogIndiciPaDTO logExec) throws DAOException{
		
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int i=0;
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(INSERT_LOG_ROW);
			pstmt.setTimestamp(1, logExec.getDataInizio());
			pstmt.setTimestamp(2, logExec.getDataFine());
			pstmt.setInt(3, logExec.getTotaleIndLav());
			
			i  = pstmt.executeUpdate();
			
		}
		catch( Exception eh )
		{
			throw new DAOException("Errore insert dati Account PEC: "+eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();

		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		return i;
	}

}
