package it.eustema.inps.agentPecPei.dao;


import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.DBConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AllegatiDAO {

	private static Log log = LogFactory.getLog(AllegatiDAO.class);

	public ArrayList<AllegatoDTO> selectAllegatiWS( String messageID ) throws DAOException{


		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<AllegatoDTO> listaAllegati = new ArrayList<AllegatoDTO>();
		AllegatoDTO allegato = new AllegatoDTO();
		Connection conn = null;
		try {

			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

			String query = "SELECT NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,idDocStore" 
				+ " FROM View_Allegati with(nolock)"
				+ " WHERE MessageID = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, messageID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				allegato = new AllegatoDTO();
				allegato.setFileName(rs.getString("NomeAllegato"));
				allegato.setTipoAllegato(rs.getString("TipoAllegato"));
				allegato.setFileSize(rs.getInt("sizeAllegato"));
				allegato.setFileData(rs.getBytes("Allegato"));
				allegato.setContentType(rs.getString("contentType"));
				allegato.setTestoAllegato(rs.getString("TestoAllegato"));
				allegato.setIdDocStore(rs.getString("idDocStore"));

				if ( allegato.getFileSize() == 0 && allegato.getFileData() != null )
					allegato.setFileSize( allegato.getFileData().length );

				listaAllegati.add(allegato);
				//				if(DebugConstants.ENABLE_DEBUG_CONSTANTS){
				//					FileOutputStream fo = new FileOutputStream("C:\\testPdf\\"+ allegato.getFileName()); 
				//					fo.write(allegato.getFileData());
				//					fo.close();
				//				}
			}

		} catch (Exception eh) {
			throw new DAOException( eh.getMessage() );
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();

			} catch (Exception e) {
				throw new DAOException( e.getMessage() );
			}
		}
		return listaAllegati;
	}

	public int insertAllegato(MessaggioDTO messageId, AllegatoDTO allegato)throws Exception {

		PreparedStatement pstmt = null;
		int result=0;
		Connection conn = null;
		try {

			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

			//			Con mese anno
			//			String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,PostedDate,meseAnno)" +
			//			" VALUES(?,?,?,?,?,?,?,?,?)";
			if(!verificaPresenzaAllegato(messageId.getMessageId(), allegato)){

				String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,PostedDate)" +
				" VALUES(?,?,?,?,?,?,?,?)";

				pstmt =  conn.prepareStatement(query);

				pstmt.setString(1, messageId.getMessageId());
				pstmt.setString(2, allegato.getFileName());
				pstmt.setString(3, allegato.getTipoAllegato());
				pstmt.setInt(4, allegato.getFileSize());
				pstmt.setBytes(5, allegato.getFileData());
				pstmt.setString(6, allegato.getContentType());
				if(allegato.getTestoAllegato()!=null) {
					pstmt.setString(7, allegato.getTestoAllegato());
				}else{
					pstmt.setNull(7,java.sql.Types.VARCHAR);
				}
				if(messageId.getPostedDate()!=null) {
					pstmt.setTimestamp(8, allegato.getPostedDate());
				}else{
					pstmt.setNull(8,java.sql.Types.TIMESTAMP);
				}
				//				Con mese Anno
				//				if(allegato.getMeseAnno()!=null){
				//					pstmt.setString(9, allegato.getMeseAnno());
				//				}else{
				//					pstmt.setNull(9, java.sql.Types.VARCHAR);
				//				}
				result = pstmt.executeUpdate();
			}
		}catch(Exception e){
			log.error("errore : insertAllegato", e);
		}finally{
			if(pstmt!=null)
				pstmt.close();
			if (conn != null)
				conn.close();
			if (pstmt != null)
				pstmt.close();

		}		

		return result;
	}

	public boolean updateMetadatiAllegato(String messageId, AllegatoDTO allegato)throws Exception {

		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {

			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

			if(messageId != null && !messageId.equals("") && allegato != null){

				String query =  "UPDATE Allegati " +
								"SET Allegato = ? " +
								"WHERE MessageID = ? " +
								"AND NomeAllegato = ?";

				pstmt =  conn.prepareStatement(query);

				pstmt.setBytes(1, allegato.getFileData());
				pstmt.setString(2, messageId);
				pstmt.setString(3, allegato.getFileName());

				pstmt.executeUpdate();

				result = true;

			} else 
				throw new Exception("I campi necessari per l'aggiornamento dei metadati dell'allagato: MessageID, NomeAllegato ed i suoi metadati sono incompleti o errati!");
		}catch(Exception e){
			log.error("Errore: Aggiornamento Metadati Allagato " +
					"( MessageID: "+ messageId + " | NomeAllegato: " + allegato.getFileName() + " )", e);
		}finally{
			if(pstmt!=null)
				pstmt.close();
			if (conn != null)
				conn.close();
			if (pstmt != null)
				pstmt.close();	
		}		

		return result;
	}

	public int doInsertAllegati(Connection conn, String messageId, String codAOO, List<AllegatoDTO> elencoFileAllegati)
	throws Exception {

		PreparedStatement pstmt = null;
		int result=0;
		Iterator<AllegatoDTO> it = elencoFileAllegati.iterator();

		while(it.hasNext()){
			AllegatoDTO allegato = it.next();

			//			Con mese anno
			//			String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,PostedDate,meseAnno)" +
			//			" VALUES(?,?,?,?,?,?,?,?,?)";
			if(!verificaPresenzaAllegato(messageId, allegato)){
				String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,PostedDate)" +
				" VALUES(?,?,?,?,?,?,?,?)";

				pstmt =  conn.prepareStatement(query);

				pstmt.setString(1, messageId);
				pstmt.setString(2, allegato.getFileName());
				pstmt.setString(3, allegato.getTipoAllegato());
				pstmt.setInt(4, allegato.getFileSize());
				pstmt.setBytes(5, allegato.getFileData());
				pstmt.setString(6, allegato.getContentType());
				if(allegato.getTestoAllegato()!=null) {
					pstmt.setString(7, allegato.getTestoAllegato());
				}else{
					pstmt.setNull(7,java.sql.Types.VARCHAR);
				}
				if(allegato.getPostedDate()!=null) {
					pstmt.setTimestamp(8, allegato.getPostedDate());
				}else{
					pstmt.setNull(8,java.sql.Types.TIMESTAMP);
				}
				//				Con Mese Anno
				//				if(allegato.getMeseAnno()!=null){
				//					pstmt.setString(9, allegato.getMeseAnno());
				//				}else{
				//					pstmt.setNull(9, java.sql.Types.VARCHAR);
				//				}
				result = pstmt.executeUpdate();
			}
		}

		if(pstmt!=null)
			pstmt.close();

		return result;
	}

	private boolean verificaPresenzaAllegato(String messageId, AllegatoDTO allegato){
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		boolean trovato = false;
		try {

			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

			String querySelect = "SELECT NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato" 
				+ " FROM View_Allegati with(nolock)"
				+ " WHERE MessageID = ? and UPPER(NomeAllegato) = UPPER(?) ";
			pstmt =  conn.prepareStatement(querySelect);
			pstmt.setString(1, messageId);
			pstmt.setString(2, allegato.getFileName());
			rs = pstmt.executeQuery();

			//			return rs.next();

		}catch(Exception e){
			log.error("errore : verificaPresenzaAllegato", e);
		}finally{
			try{
				if(rs!=null){
					trovato = rs.next();
					rs.close();
				}else
					trovato = false;
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){

			}
		}
		return trovato;
	}

	public String selectAllegatiName(Connection connection, String messageID) throws Exception {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String result = "";

		try {

			String query = "SELECT NomeAllegato" +
			" FROM View_Allegati with(nolock) " +
			" WHERE MessageID = ? AND " +
			" TipoAllegato not like 'P7S'" +
			" AND TipoAllegato not like 'EML'" +
			" AND NomeAllegato not like '%daticert.xml%'";

			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, messageID);

			rs  = pstmt.executeQuery();
			if(rs!=null){
				while (rs.next()) {
					result += rs.getString("NomeAllegato") + ", ";
				}
				// condizione necessaria nel caso la query non restituisca alcun record ma il rs � != null
				if(!result.equals(""))
					result = result.substring(0,result.length()-2);
			}
		} catch (Exception eh) {
			throw eh;
		} 
		if (rs != null)
			rs.close();
		if (pstmt != null)
			pstmt.close();

		return result;
	}

	public int addIdDocumentStore(String idDoc,String msgId, String nome){
		PreparedStatement pstmt = null;
		Connection conn = null;
		int result = 0;

		try {

			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

			String query = "update allegati set idDocStore = ? where messageId = ? and nomeAllegato = ? ";
			pstmt =  conn.prepareStatement(query);
			pstmt.setString(1, idDoc);
			pstmt.setString(2, msgId);
			pstmt.setString(3, nome);

			result = pstmt.executeUpdate();

		}catch(Exception e){
			log.error("errore in inserimento Id Document Store per allegato "+ nome +" del messaggio "+result, e);
		}finally{
			try{
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){

			}
		}
		return result;
	}
	
	public AllegatoDTO getAllegatoMessaggio(String messageID, String nomeAllegato){

		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		AllegatoDTO allegato = null;

		try {

			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

			String query =  "SELECT MessageID, NomeAllegato, TipoAllegato, TestoAllegato, sizeAllegato, Allegato, contentType, postedDate, idDocStore " +
							"FROM Allegati WITH(NOLOCK) " +
							"WHERE MessageID = ? AND NomeAllegato = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, messageID);
			pstmt.setString(2, nomeAllegato);

			rs  = pstmt.executeQuery();

			if(rs!=null){
				while (rs.next()) {
					allegato = new AllegatoDTO();
					allegato.setFileData(rs.getBytes("Allegato"));
					allegato.setFileSize(rs.getInt("sizeAllegato"));
					allegato.setMessageID(rs.getString("MessageID"));
					allegato.setIdDocStore(rs.getString("idDocStore"));
					allegato.setFileName(rs.getString("NomeAllegato"));
					allegato.setContentType(rs.getString("contentType"));
					allegato.setPostedDate(rs.getTimestamp("postedDate"));
					allegato.setTipoAllegato(rs.getString("TipoAllegato"));
					allegato.setTestoAllegato(rs.getString("TestoAllegato"));
				}
			}

		}catch(Exception e){
			log.error("Impossibile recuperare l'allegato " + nomeAllegato + " per il messaggio con MessageID: " + messageID, e);
		}finally{
			try{
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){

			}
		}
		return allegato;
	}

	public List<AllegatoDTO> getAllegatiMessaggioPrincipale(String msgId){

		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		List<AllegatoDTO> result = new ArrayList<AllegatoDTO>();

		try {

			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

			String query =  "SELECT MessageID, NomeAllegato, TipoAllegato, TestoAllegato, sizeAllegato, Allegato, contentType, postedDate, idDocStore " +
							"FROM Allegati WITH(NOLOCK) " +
							"WHERE MsgID = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, msgId);

			rs  = pstmt.executeQuery();

			if(rs!=null){
				while (rs.next()) {
					AllegatoDTO allegato = new AllegatoDTO();
					allegato.setFileData(rs.getBytes("Allegato"));
					allegato.setFileSize(rs.getInt("sizeAllegato"));
					allegato.setMessageID(rs.getString("MessageID"));
					allegato.setIdDocStore(rs.getString("idDocStore"));
					allegato.setFileName(rs.getString("NomeAllegato"));
					allegato.setContentType(rs.getString("contentType"));
					allegato.setPostedDate(rs.getTimestamp("postedDate"));
					allegato.setTipoAllegato(rs.getString("TipoAllegato"));
					allegato.setTestoAllegato(rs.getString("TestoAllegato"));

					result.add(allegato);
				}
			}

		}catch(Exception e){
			log.error("Impossibile recuperare gli allegati per il messaggio con MessageID: " + msgId, e);
		}finally{
			try{
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){

			}
		}
		return result;
	}	
	
	public List<AllegatoDTO> refreshAllegatiMessaggio(String messageID){

		ResultSet rs = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		List<AllegatoDTO> result = new ArrayList<AllegatoDTO>();

		try {

			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

			String query =  "SELECT MessageID, NomeAllegato, TipoAllegato, TestoAllegato, sizeAllegato, Allegato, contentType, postedDate, idDocStore " +
							"FROM Allegati WITH(NOLOCK) " +
							"WHERE MessageID = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, messageID);

			rs  = pstmt.executeQuery();

			if(rs!=null){
				while (rs.next()) {
					AllegatoDTO allegato = new AllegatoDTO();
					allegato.setFileData(rs.getBytes("Allegato"));
					allegato.setFileSize(rs.getInt("sizeAllegato"));
					allegato.setMessageID(rs.getString("MessageID"));
					allegato.setIdDocStore(rs.getString("idDocStore"));
					allegato.setFileName(rs.getString("NomeAllegato"));
					allegato.setContentType(rs.getString("contentType"));
					allegato.setPostedDate(rs.getTimestamp("postedDate"));
					allegato.setTipoAllegato(rs.getString("TipoAllegato"));
					allegato.setTestoAllegato(rs.getString("TestoAllegato"));

					result.add(allegato);
				}
			}

		}catch(Exception e){
			log.error("Impossibile recuperare gli allegati per il messaggio con MessageID: " + messageID, e);
		}finally{
			try{
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){

			}
		}
		return result;
	}

}
