package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.IndicePADTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class IndicePADAO extends AgentBaseDAO {
	
	private static final String SELECT_INDICE_PA_QRY = "SELECT " +
														    " [CodAMM] ,[CodO] ,[CodOU] ,[description] ,[DesO] ,[DesOU] ,[Form] ,[ISLdap] ,[l],[mailResp] "+
														    " ,[nomeResp] ,[cognomeResp] ,[O] ,[OU] ,[provincia] ,[regione] ,[mail] ,[TipoAmm],[TipoStruttura] "+
														    " ,[AOO] ,[AOORef] ,[codAOO] ,[DesAOO] ,[dominioPEC] " +
														    " FROM IndicePA with(nolock) "+
														    " WHERE mail = ?"
														    ;
	
	
	private static final String INSERT_INDICE_PA_QRY = "INSERT INTO [PECPEI].[dbo].[IndicePA]" +
														    " ([CodAMM] ,[CodO] ,[CodOU] ,[description] ,[DesO] ,[DesOU] ,[Form] ,[ISLdap] ,[l],[mailResp] "+
														    " ,[nomeResp] ,[cognomeResp] ,[O] ,[OU] ,[provincia] ,[regione] ,[mail] ,[TipoAmm],[TipoStruttura] "+
														    " ,[AOO] ,[AOORef] ,[codAOO] ,[DesAOO] ,[dominioPEC] ,[CodiceFiscale] ,[CodiceUnivocoUO])  VALUES ( "+
														    " ?,?,?,?,?,?,?,?,?,?, "+
														    " ?,?,?,?,?,?,?,?,?, "+
														    " ?,?,?,?,?,?,? )";
	
	public static final String SELECT_PEC_INTEROP2013 = "select mail from AbilitazioniInteropEmail with(nolock) where interop2013 = 1 and mail = ?";
	
	public static final String INSERT_PEC_INTEROP2013 = "insert into AbilitazioniInteropEmail (mail ,descrizioneCasella ,dominioPEC ,interop2013 ,dataAttivazione ,dataDisattivazione ,dataAggiornamento, MessageID) VALUES ( ?, '','',1,getdate(),null,getdate(),?)";
	
	public static final String UPDATE_PEC_INTEROP2013 = "update AbilitazioniInteropEmail set interop2013 = 1, messageID = ? where mail = ?";
	
	public int updateEmailInterop2013( MessaggioDTO messaggio) throws DAOException{
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int i=0;
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(UPDATE_PEC_INTEROP2013);
			pstmt.setString(1, messaggio.getMessageId());
			pstmt.setString(2, messaggio.getReplyTo());
			i  = pstmt.executeUpdate();	
		}
		catch( Exception eh )
		{
			throw new DAOException("Errore update dati Account PEC: "+eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();

		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		return i;
	}
	public int insertEmailInterop2013( MessaggioDTO messaggio) throws DAOException{
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int i=0;
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(INSERT_PEC_INTEROP2013);
			pstmt.setString(1, messaggio.getReplyTo() );
			pstmt.setString(2, messaggio.getMessageId() );
			i  = pstmt.executeUpdate();
			
		}
		catch( Exception eh )
		{
			throw new DAOException("Errore insert dati Account PEC: "+eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();

		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		return i;
	}
	public int selectEmailInterop2013( String mail) throws DAOException{
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		IndicePADTO indicePA = null;
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_PEC_INTEROP2013);
			pstmt.setString(1, mail );
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				indicePA = new IndicePADTO();
				indicePA.setMail(rs.getString("mail"));
		    }
		}
		catch( Exception eh )
		{
			throw new DAOException("Errore recupero dati Account PEC: "+eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();

		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		return indicePA==null?0:1;
	}
	
	public IndicePADTO selectEmailIndicePA( String mail ) throws DAOException{
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		IndicePADTO indicePA = null;
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_INDICE_PA_QRY);
			pstmt.setString(1, mail );
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				indicePA = new IndicePADTO();
				indicePA.setMail(rs.getString("mail"));
				indicePA.setNomeresp(rs.getString("nomeResp"));
				indicePA.setCognomeresp(rs.getString("cognomeResp"));
				indicePA.setCodamm(rs.getString("codamm"));
				indicePA.setCodaoo(rs.getString("codaoo"));
		    }
		}
		catch( Exception eh )
		{
			throw new DAOException("Errore recupero dati Account PEC: "+eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();

		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		return indicePA;
	}
}
