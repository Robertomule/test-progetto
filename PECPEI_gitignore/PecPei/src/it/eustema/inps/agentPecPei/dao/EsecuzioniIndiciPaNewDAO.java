package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.EsecuzioniIndiciPaNew;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.DBConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.log4j.Logger;

public class EsecuzioniIndiciPaNewDAO{
	
	private static Logger log = Logger.getLogger(EsecuzioniIndiciPaNewDAO.class);
	private static final String INSERT_ESECUZIONE_QRY = "insert into dbo.EsecuzioniIndiciPaNew (inizioEsecuzione,fineEsecuzione,tempoEsecuzione,ultimoFileLavorato,errore,totLavorati) " +
																		"values (?,?,?,?,?,?)";
	
	private static final String ULTIMA_ESECUZIONE_QRY = "select top(1) fineesecuzione from EsecuzioniIndiciPaNew where fineesecuzione is not null order by id desc";
															
															
	public int insertEsecuzione(EsecuzioniIndiciPaNew esecuzione ) throws DAOException{
		
		PreparedStatement pstmt = null;
		int result=0;
		Connection conn = null;
		ResultSet rs = null;
		
		try{
			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
			
			pstmt =  conn.prepareStatement(INSERT_ESECUZIONE_QRY);
			
			pstmt.setTimestamp(1, esecuzione.getInizioEsecuzione());
			pstmt.setTimestamp(2, esecuzione.getFineEsecuzione());
			pstmt.setString(3, esecuzione.getTempoEsecuzione());
			pstmt.setBytes(4, esecuzione.getUltimoFileLavorato()!=null?esecuzione.getUltimoFileLavorato():null);
			pstmt.setString(5, esecuzione.getErrore()!=null?esecuzione.getErrore():"");
			pstmt.setInt(6, esecuzione.getTotLavorati());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e){
			e.printStackTrace();
			if (conn != null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
		}finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				log.error("Indici PA --> ERRORE::64 metodo insertEsecuzione "+e.getMessage());
				throw new DAOException( e.getMessage() );
			}
		}
		
		return result;
	}
	
	public Timestamp getUltimaEsecuzione(){
		Connection conn = null;
		Timestamp timestamp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		System.out.println("Carico l'ultima esecuzione dal database");
		try{
			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
			pstmt =  conn.prepareStatement(ULTIMA_ESECUZIONE_QRY);
			rs = pstmt.executeQuery();
			
			while(rs.next())
				timestamp = rs.getTimestamp(1);
			
		}catch(Exception e){
			System.out.println("Errore in recuper TimeStamp ultima esecuzione: "+e.getMessage());	
			e.printStackTrace();
			if (conn != null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					System.out.println("Errore in recuper TimeStamp ultima esecuzione: "+e.getMessage());
					log.error("Indici PA --> ERRORE::92 metodo getUltimaEsecuzione "+e1.getMessage());
					e1.printStackTrace();
				}
		}finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				System.out.println("Errore in recuper TimeStamp ultima esecuzione: "+e.getMessage());
				log.error("Indici PA --> ERRORE::105 metodo getUltimaEsecuzione "+e.getMessage());
				e.printStackTrace();
			}
		}
		
		System.out.println("TimeStamp ultima esecuzione: "+timestamp);	
		return timestamp;
	}
}
