package it.eustema.inps.agentPecPei.dao;

//import it.eustema.inps.agentPecPei.dto.AgentExecutionParamDTO;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SedeDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.gestioneDestinatari.GestDatiCert;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.DBConnectionManager;
import it.eustema.inps.utility.StaticUtil;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class MessaggiDAO extends AgentBaseDAO{ 

	ConfigDAO configDao = new ConfigDAO();

	private static Log log = LogFactory.getLog("it.eustema.inps.agentPecPei.dao.MessaggiDAO");

	private static final String SELECT_MSG_PEI_USCENTI_QRY = "select m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura , m.nomeUfficio, " +
	" m.tipoMessaggio,m.tipologia,d.Addr,d.Type_per_cc, m.subject, m.body, m.verso, m.classifica , m.confermaRicezione, m.nomeSede, " +
	" m.FromPhrase,m.FromAddr, m.Sensibile, m.Riservato, d.Name, m.desMitt "+
	" from messaggi m with(nolock) " +
	" LEFT JOIN destinatari d ON m.MessageID = d.MessageID " +
	" where m.verso = 'U' and m.X_Trasporto = 'messaggio-pei' and m.Stato = 'DI' " +
																	" and m.messageId = '<04105655ABE54E9FC5C4D520520C4780>' and m.codiceAoo = '7201'"+
	" order by m.PostedDate asc";

	private static final String SELECT_MSG_PEI_USCENTI_MESSAGEID_QRY = "select m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura , m.nomeUfficio, " +
	" m.tipoMessaggio,m.tipologia,d.Addr,d.Type_per_cc, m.subject, m.body, m.verso, m.classifica , m.confermaRicezione, m.nomeSede, " +
	" m.FromPhrase,m.FromAddr, m.Sensibile, m.Riservato, d.Name, m.desMitt "+
	" from messaggi m with(nolock) " + 
	" LEFT JOIN destinatari d ON m.MessageID = d.MessageID " +
	//	" where m.messageId = '<40774323F316341A8649E5DA8FA17123>' and m.codiceAoo = '0064'"+ 
	" where m.verso = 'U' and m.X_Trasporto = 'messaggio-pei' and m.Stato = 'IN' and m.messageId = ? " +
	" order by m.PostedDate asc";


	private static final String SELECT_MSG_PEI_DA_PROTOCOLLARE_QRY = "select m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura, " +
	" m.tipoMessaggio,m.tipologia,m.subject, m.body, m.verso, m.classifica, " +
	" m.Sensibile, m.Riservato"+
	" from messaggi m with(nolock) " + 
	" where m.X_Trasporto = 'messaggio-pei' and m.Stato = 'DP' and m.messageId = '<04105655ABE54E9FC5C4D520520C4780>'" +
	" order by m.MessageID";

	private static final String SELECT_MSG_PEI_DA_PROTOCOLLARE_QRY_MESSAGEID = "select m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura, " +
	" m.tipoMessaggio,m.tipologia,m.subject, m.body, m.verso, m.classifica, " +
	" m.Sensibile, m.Riservato"+
	" from messaggi m with(nolock) " + 
	" where m.X_Trasporto = 'messaggio-pei' and m.Stato = 'DP' and m.messageID = ? " +
	" order by m.MessageID";

	private static final String SELECT_MSG_PEI_USCENTI_QRY2 = " select distinct m.MessageID, m.CodiceAOO, m.UiDoc, m.verso, m.nomeSede, m.PostedDate, m.Subject, m.Body, m.Stato, m.Riservato,"
		+ "m.Sensibile, m.size, m.IPAddress, m.nomeRegione, m.classifica, m.desClassifica, m.nomeUfficio, m.accountPEC, m.FromAddr, m.FromPhrase,"
		+ "m.AutoreCompositore, m.AutoreUltimaModifica, m.AutoreMittente,"
		+ "m.confermaRicezione, m.AddrConfermaRicezione, m.dominioPEC, m.documentoPrincipale, m.messaggio, m.tipologia, m.X_trasporto,"
		+ "m.MsgID, m.tipoMessaggio, m.ReplyTo, m.isReply, m.inReplyTo, m.inReplyToSegnatura, m.inReplyToSubject, m.SegnaturaRif, m.SegnaturaRoot, m.CodiceAMM,"
		+ "m.CodiceAOOMitt, m.CodiceAMMMitt, m.X_TipoRicevuta, m.BodyHtml, m.isSPAMorCCN, m.tipoMitt, m.desMitt, m.DirettoreAOO, m.DirettoreAOOMitt, m.substato,"
		+ "m.X_Ricevuta, m.DeliveredDate, d.Addr,d.Type_per_cc,d.Name from messaggi m with(nolock) " +
		" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
		" where m.verso = 'U' and m.X_Trasporto = 'messaggio-pei' and m.Stato = 'DI' and m.MessageID = ? " +
		" order by m.MessageID";



	private static final String SELECT_MSG_PEC_ENTRANTI_DA_RIPROTOCCOLARE_QRY = "select m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura, " +
	" m.tipoMessaggio,m.tipologia,d.Addr,d.Name,d.Type_per_cc, m.subject, m.body, m.verso, m.classifica, m.idPrenotazione, canale " +
	" from messaggi m with(nolock)" + 
	" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
	" where m.verso = 'E' and m.X_Trasporto = 'posta-certificata' and m.Stato = 'DP' and m.CodiceAOO = ?  and m.isSPAMorCCN = 0 " +
	" order by m.PostedDate desc";

	private static final String SELECT_MSG_PEC_USCENTI_QRY = "select m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura, " +
	" m.tipoMessaggio,m.tipologia,d.Addr,d.Name,d.Type_per_cc, m.subject, m.body, m.verso, m.classifica, m.X_TipoRicevuta, m.canale " +
	" from messaggi m with(nolock)" + 
	" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
	" where m.verso = 'U' and m.X_Trasporto = 'posta-certificata' and m.Stato in ('DI','BZ') and m.CodiceAOO = ?  " +
	" order by m.MessageID";

	private static final String SELECT_MSG = "select count(m.MessageID) tot_messaggio"+
	" from messaggi m with(nolock)" + 
	" where m.MessageID = ?";	
	private static final String SELECT_DEST = "select count(m.MessageID) tot_messaggio"+
	" from destinatari m with(nolock)" + 
	" where m.MessageID = ?";	
	private static final String SELECT_ALLEGATI = "select count(m.MessageID) tot_messaggio"+
	" from destinatari m with(nolock)" + 
	" where m.MessageID = ?";

	private static final String SELECT_MSG_PEC_USCENTI_QRY2 = "select m.MessageID, m.CodiceAOO, m.UiDoc, m.verso, m.nomeSede, m.PostedDate, m.Subject, m.Body, m.Stato, m.canale, m.Riservato, "
		+ "m.Sensibile, m.size, m.IPAddress, m.nomeRegione, m.classifica, m.desClassifica, m.nomeUfficio, m.accountPEC, m.FromAddr, m.FromPhrase, "
		+ "m.AutoreCompositore, m.AutoreUltimaModifica, m.AutoreMittente, m.PostedDate, m.Segnatura, "
		+ "m.confermaRicezione, m.AddrConfermaRicezione, m.dominioPEC, m.documentoPrincipale, m.messaggio, m.tipologia, m.X_trasporto, "
		+ "m.MsgID, m.tipoMessaggio, m.ReplyTo, m.isReply, m.inReplyTo, m.inReplyToSegnatura, m.inReplyToSubject, m.SegnaturaRif, m.SegnaturaRoot, m.CodiceAMM, "
		+ "m.CodiceAOOMitt, m.CodiceAMMMitt, m.X_TipoRicevuta, m.BodyHtml, m.isSPAMorCCN, m.tipoMitt, m.desMitt, m.DirettoreAOO, m.DirettoreAOOMitt, m.substato, "
		+ "m.X_Ricevuta, m.DeliveredDate, d.Addr,d.Name,d.Type_per_cc, d.phrase from messaggi m with(nolock)" +
		" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
		" where m.verso = 'U' and m.X_Trasporto = 'posta-certificata' and m.Stato = 'DI' and m.MessageID = ? " +
		" order by m.MessageID";

	private static final String SELECT_MSG_PEC_ENTRANTI_QRY_DA_PROTOCOLLARE = "select m.MessageID, m.CodiceAOO, m.UiDoc, m.verso, m.nomeSede, m.PostedDate, m.Subject, m.Body, m.Stato, m.Riservato,"
		+ "m.Sensibile, m.size, m.IPAddress, m.nomeRegione, m.classifica, m.desClassifica, m.nomeUfficio, m.accountPEC, m.FromAddr, m.FromPhrase, m.idPrenotazione, "
		+ "m.AutoreCompositore, m.AutoreUltimaModifica, m.AutoreMittente,"
		+ "m.confermaRicezione, m.AddrConfermaRicezione, m.dominioPEC, m.documentoPrincipale, m.messaggio, m.tipologia, m.X_trasporto,"
		+ "m.MsgID, m.tipoMessaggio, m.ReplyTo, m.isReply, m.inReplyTo, m.inReplyToSegnatura, m.inReplyToSubject, m.SegnaturaRif, m.SegnaturaRoot, m.CodiceAMM,"
		+ "m.CodiceAOOMitt, m.CodiceAMMMitt, m.X_TipoRicevuta, m.BodyHtml, m.isSPAMorCCN, m.tipoMitt, m.desMitt, m.DirettoreAOO, m.DirettoreAOOMitt, m.substato,"
		+ "m.X_Ricevuta, m.DeliveredDate, d.Addr,d.Name,d.Type_per_cc, d.phrase from messaggi m with(nolock)"  +
		" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
		" where m.verso = 'E' and m.X_Trasporto = 'posta-certificata' and m.Stato = 'DP' and m.MessageID = ? " +
		" order by m.MessageID";

	private static final String SELECT_COD_AOO_FROM_MESSAGGI = "select distinct m.CodiceAOO from messaggi m with(nolock), AccountsPEC a with(nolock) " +
	"														where m.CodiceAOO = a.AOO and a.gestioneNuovoAgent = 0 and idBatch = ? order by CodiceAOO asc";

	private static final String SELECT_COD_AOO_FROM_MESSAGGI_TEST = "select distinct m.CodiceAOO from messaggi m with(nolock), AccountsPEC a with(nolock) where m.CodiceAOO = ? and a.gestioneNuovoAgent = 1 and PECTest = 2 order by CodiceAOO asc";


	private static final String UPDATE_MSG_INVIATO_QRY = "update messaggi SET Stato=?, Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?, Subject=?" +
	" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";

	private static final String UPDATE_MSG_USCENTE_PROTOCOLLATO_QRY = "update messaggi SET Segnatura=? " +
	" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";


	private static final String UPDATE_MSG_RICEVUTO_QRY = "update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=? " +
	" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=? and Stato='DP'";

	private static final String UPDATE_MSG_SCARICATO_QRY = "update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=? " +
	" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=? and Stato='DP'";


	private static final String UPDATE_SUBSTATO_MESSAGGIO_QRY = "update messaggi SET substato='DN' WHERE MessageID=? AND CodiceAOO=? AND accountPEC=? and Stato='RI' and substato != 'NT'";

	private static final String UPDATE_MSG_RICEVUTO_DOCPRIN_QRY = "update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, documentoPrincipale=?" +
	" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";

	private static final String UPDATE_MSG_PRENOTATO_QRY = "update messaggi SET DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?, documentoPrincipale=?, idPrenotazione = ?" +
	" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";

	private static final String UPDATE_MSG_PRENOTATO_QRY_1 = "update messaggi SET DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?, idPrenotazione = ?" +
	" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";


	private static final String INS_MSG_ENTRANTE_QRY =  "INSERT INTO messaggi"
		+ " (MessageID, CodiceAOO, UiDoc, verso, nomeSede, PostedDate, Subject, Body, Stato, Riservato,"
		+ "Sensibile, size, IPAddress, nomeRegione, classifica, desClassifica, nomeUfficio, accountPEC, FromAddr, FromPhrase,"
		+ "AutoreCompositore, AutoreUltimaModifica, AutoreMittente,"
		+ "confermaRicezione, AddrConfermaRicezione, dominioPEC, documentoPrincipale, messaggio, tipologia, X_trasporto,"
		+ "MsgID, tipoMessaggio, ReplyTo, isReply, inReplyTo, inReplyToSegnatura, inReplyToSubject, SegnaturaRif, SegnaturaRoot, CodiceAMM,"
		+ "CodiceAOOMitt, CodiceAMMMitt, X_TipoRicevuta, BodyHtml, isSPAMorCCN, tipoMitt, desMitt, DirettoreAOO, DirettoreAOOMitt, substato,"
		+ "X_Ricevuta, DeliveredDate, segnatura, PostacertEml_Body, PostacertEml_Subject, SignIssuerName,SignSUbjectName , isSigned, isSignedVer, IdCartella," 
		+ " StatoLettura,SignValidFromDate, SignValidToDate,headers ,PostacertEml_Headers, rawContent,DatiCertificazione, PostacertEml_PostedDate, PostacertEml_From , SegnaturaMitt, canale)"
		+ " VALUES(?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,"
		+ "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,"
		+ "?,?,?,?,?,?,?,?,?,?,"+ "?,?,?,?,?,?,?,?,?,?,"
		+ "?,?,?,?,?,?,?,?,?,?,?)";
	private static final String INS_MSG_ENTRANTE_QRY_INTEROP =  "INSERT INTO messaggiInteroperabilita"
		+ " (MessageID, CodiceAOO, UiDoc, verso, nomeSede, PostedDate, Subject, Body, Stato, Riservato,"
		+ "Sensibile, size, IPAddress, nomeRegione, classifica, desClassifica, nomeUfficio, accountPEC, FromAddr, FromPhrase,"
		+ "AutoreCompositore, AutoreUltimaModifica, AutoreMittente,"
		+ "confermaRicezione, AddrConfermaRicezione, dominioPEC, documentoPrincipale, messaggio, tipologia, X_trasporto,"
		+ "MsgID, tipoMessaggio, ReplyTo, isReply, inReplyTo, inReplyToSegnatura, inReplyToSubject, SegnaturaRif, SegnaturaRoot, CodiceAMM,"
		+ "CodiceAOOMitt, CodiceAMMMitt, X_TipoRicevuta, BodyHtml, isSPAMorCCN, tipoMitt, desMitt, DirettoreAOO, DirettoreAOOMitt, substato,"
		+ "X_Ricevuta, DeliveredDate, segnatura, PostacertEml_Body, PostacertEml_Subject, SignIssuerName,SignSUbjectName , isSigned, isSignedVer, IdCartella," 
		+ " StatoLettura,SignValidFromDate, SignValidToDate,headers ,PostacertEml_Headers, rawContent,DatiCertificazione, PostacertEml_PostedDate, PostacertEml_From , SegnaturaMitt, canale)"
		+ " VALUES(?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,"
		+ "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,"
		+ "?,?,?,?,?,?,?,?,?,?,"+ "?,?,?,?,?,?,?,?,?,?,"
		+ "?,?,?,?,?,?,?,?,?,?,?)";
	private static final String SELECT_MSG_Sens_Riser = "select m.MessageID , m.Riservato, m.Sensibile"+
	" from messaggi m with(nolock) " + 
	" where m.MessageID = ? ";

	private static final String SELECT_SEGNATURA = "select m.segnatura from messaggi m with (nolock) where m.MessageID = ? ";

	private static final String SELECT_SEGNATURA_PEI = "select m.segnatura from messaggi m with (nolock) where m.MessageID = ? and accountPec = ? and CodiceAOO = ? ";

	private static final String SELECT_DATICERT = "select top 100 m.messageId, m.posteddate,  a.nomeallegato, a.allegato "
		+"from messaggi m with(nolock), allegati a with(nolock) "
		+"where m.posteddate >= getdate()-1  and tipomessaggio = ? and  m.messageID = a.messageID and a.nomeallegato = ? "
		+"and m.messageID not in (select messageid from dbo.GestioneDestinatari) "
		+"order by m.posteddate desc";

	private static final String INSERT_DESTINATARIO = "insert into GestioneDestinatari (MessageID , PostedDate, DataLavorazione, Indirizzo, Tipo ) values (?,?,?,?,?) ";

	private static final String SELECT_INDIRIZZO = "select count(indirizzo) from GestioneDestinatari with(nolock) where indirizzo = ?";

	private static final String UPDATE_MSG_RICEVUTO_DOCPRIN_QRY_TEST = "update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?, documentoPrincipale=?" +
	" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";

	private static final String UPDATE_MSG_RICEVUTO_QRY_TEST = "update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?" +
	" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=? and Stato='TDP'";

	private static final String SELECT_MSG_PEC_ENTRANTI_QRY_DA_PROTOCOLLARE_TEST = "select m.MessageID, m.CodiceAOO, m.UiDoc, m.verso, m.nomeSede, m.PostedDate, m.Subject, m.Body, m.Stato, m.Riservato,"
		+ "m.Sensibile, m.size, m.IPAddress, m.nomeRegione, m.classifica, m.desClassifica, m.nomeUfficio, m.accountPEC, m.FromAddr, m.FromPhrase,"
		+ "m.AutoreCompositore, m.AutoreUltimaModifica, m.AutoreMittente,"
		+ "m.confermaRicezione, m.AddrConfermaRicezione, m.dominioPEC, m.documentoPrincipale, m.messaggio, m.tipologia, m.X_trasporto,"
		+ "m.MsgID, m.tipoMessaggio, m.ReplyTo, m.isReply, m.inReplyTo, m.inReplyToSegnatura, m.inReplyToSubject, m.SegnaturaRif, m.SegnaturaRoot, m.CodiceAMM,"
		+ "m.CodiceAOOMitt, m.CodiceAMMMitt, m.X_TipoRicevuta, m.BodyHtml, m.isSPAMorCCN, m.tipoMitt, m.desMitt, m.DirettoreAOO, m.DirettoreAOOMitt, m.substato,"
		+ "m.X_Ricevuta, m.DeliveredDate, d.Addr,d.Name,d.Type_per_cc, d.phrase from messaggi m with(nolock)"  +
		" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
		" where m.verso = 'E' and m.X_Trasporto = 'posta-certificata' and m.Stato = 'TDP' and m.MessageID = ? " +
		" order by m.MessageID";

	private static final String SELECT_MSG_PEC_ENTRANTI_DA_RIPROTOCCOLARE_QRY_TEST = "select m.MessageID,m.CodiceAOO,m.accountPEC,m.CodiceAOOMitt,m.StatoLettura, m.Stato, m.Segnatura, " +
	" m.tipoMessaggio,m.tipologia,d.Addr,d.Name,d.Type_per_cc, m.subject, m.body, m.verso, m.classifica " +
	" from messaggi m with(nolock)" + 
	" LEFT JOIN destinatari d with(nolock) ON m.MessageID = d.MessageID " +																
	" where m.verso = 'E' and m.X_Trasporto = 'posta-certificata' and m.Stato = 'TDP' and m.CodiceAOO = ?  and m.isSPAMorCCN = 0 " +
	" order by m.PostedDate desc";

	private static final String GET_MSG_SGD_IN_USCITA = "select distinct(messageId) from messaggi with(nolock) where verso = 'U' and msgId = ? and canale = ?";

	private static final String GET_SEGNATURA_MESSAGGIO_PRINCIPALE = "SELECT Segnatura FROM Messaggi WITH(NOLOCK) WHERE MessageID = ?";
	
	//public int aggiornaMsgInviatiRicevuti(MessaggioDTO msgUscente, List<MessaggioDTO> elencoMsgEntranti) throws DAOException{
	public int aggiornaMsgInviatiRicevuti(Map<MessaggioDTO,List<MessaggioDTO>> messaggiUscentiEntrantiMap) throws DAOException{

		Connection connPecPei = null;		
		int ret = 0;
		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			log.debug("aggiornaMsgInviatiRicevuti :: usa la connection: " + connPecPei);
			connPecPei.setAutoCommit(false);
		}
		catch(Exception e){
			e.printStackTrace();
			System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
		}
		try{
			Set<MessaggioDTO> messaggiUscentiChiavi = messaggiUscentiEntrantiMap.keySet();
			for(MessaggioDTO msgUscente: messaggiUscentiChiavi){

				aggiornaMsgUscenteInviato(connPecPei, msgUscente);
				List<MessaggioDTO> listaMsgEntranti = messaggiUscentiEntrantiMap.get(msgUscente); 
				if(msgUscente.getTipologia().equalsIgnoreCase("messaggio-pei")){
					inserisciMsgEntrantiPEI(connPecPei, msgUscente, listaMsgEntranti);
				}
				else
					inserisciMsgEntranti(connPecPei, msgUscente, listaMsgEntranti);
			}											
			connPecPei.commit(); // solo per test, poi @ TODO decommentare la commit
			log.debug("aggiornaMsgUscenteInviato: Commit eseguito");
			//connPecPei.rollback();	// solo per test elaborazione senza aggiornamento DB ... poi cancellare		
		}
		catch(Exception eh){			   
			if(connPecPei!=null){
				try {
					log.error("aggiornaMsgUscenteInviato: esegue rollback");					
					connPecPei.rollback();
				} catch (SQLException sqlex) {
					sqlex.printStackTrace();
				}	
			}
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ret;

	}
	//public int aggiornaMsgInviatiRicevuti(MessaggioDTO msgUscente, List<MessaggioDTO> elencoMsgEntranti) throws DAOException{
	public int aggiornaMsgInviatiRicevuti(MessaggioDTO messaggiUscente, MessaggioDTO messaggiEntrate, boolean aggMessaggioUscente) throws DAOException{

		Connection connPecPei = null;		
		int ret = 0;
		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			log.debug("aggiornaMsgInviatiRicevuti :: usa la connection: " + connPecPei);
			connPecPei.setAutoCommit(false);
		}
		catch(Exception e){
			e.printStackTrace();
			System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
		}
		try{

			if(aggMessaggioUscente)
				aggiornaMsgUscenteInviato(connPecPei, messaggiUscente);
			else
				inserisciMsgEntrantiPEI(connPecPei, messaggiUscente, messaggiEntrate);

			connPecPei.commit(); // solo per test, poi @ TODO decommentare la commit

			log.debug("aggiornaMsgUscenteInviato: Commit eseguito");

			//connPecPei.rollback();	// solo per test elaborazione senza aggiornamento DB ... poi cancellare		
		}
		catch(Exception eh){			   
			if(connPecPei!=null){
				try {
					log.error("aggiornaMsgUscenteInviato: esegue rollback");					
					connPecPei.rollback();
				} catch (SQLException sqlex) {
					sqlex.printStackTrace();
				}	
			}
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ret;

	}

	public int aggiornaMsgRicevutoDaRiprotocollare(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{
		boolean internal = false;
		if ( connPecPei == null ){
			internal = true;
			try{
				connPecPei = cm.getConnection(Constants.DB_PECPEI);
				log.debug("aggiornaMsgRicevutoDaRiprotocollare :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}

		log.debug("aggiornaMsgRicevutoDaRiprotocollare START");

		int ret = 0;
		PreparedStatement pstmt = null;

		try	{
			if(msgUscente.getDocumentoPrincipale()!=null && !msgUscente.getDocumentoPrincipale().equals(""))
			{
				//				"update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?, documentoPrincipale=?" +
				//				" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";
				pstmt = connPecPei.prepareStatement(UPDATE_MSG_RICEVUTO_DOCPRIN_QRY);
				// segnatura
				if(msgUscente.getSegnatura()!=null)
					pstmt.setString(1, msgUscente.getSegnatura());
				else
					pstmt.setNull(1, java.sql.Types.VARCHAR);
				pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
				//				pstmt.setString(4, msgUscente.getSubstato());
				pstmt.setString(4, msgUscente.getDocumentoPrincipale());
				pstmt.setString(5, msgUscente.getMessageId());
				pstmt.setString(6, msgUscente.getCodiceAOO());
				pstmt.setString(7, msgUscente.getAccountPec());
			}else{			
				//				"update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?" +
				//				" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";

				pstmt = connPecPei.prepareStatement(UPDATE_MSG_RICEVUTO_QRY);
				// segnatura
				if(msgUscente.getSegnatura()!=null)
					pstmt.setString(1, msgUscente.getSegnatura());
				else
					pstmt.setNull(1, java.sql.Types.VARCHAR);
				pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
				//					pstmt.setString(4, msgUscente.getSubstato());
				pstmt.setString(4, msgUscente.getMessageId());
				pstmt.setString(5, msgUscente.getCodiceAOO());
				pstmt.setString(6, msgUscente.getAccountPec());
			}
			boolean riprova = true;
			int i = 0;
			int numTentativi = Integer.parseInt(configDao.getProperty("numero_massimo_tentativi_aggiornamentoProtocollo", "3"));
			int sleepTentativi = Integer.parseInt(configDao.getProperty("sleep_tentativi_aggiornamentoProtocollo", "5000"));

			while(riprova || i==numTentativi){
				try{
					ret = pstmt.executeUpdate();
					riprova = false;
				}catch(Exception e){
					i++;
					Thread.sleep(sleepTentativi);
					riprova = true;
					if(i==numTentativi+1){
						riprova = false;
						//						executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
						//						executor.executeCommand();
					}
				}
			}

			//Aggiorno il substato in DN solo se non � gi� in NT
			pstmt = connPecPei.prepareStatement(UPDATE_SUBSTATO_MESSAGGIO_QRY);
			pstmt.setString(1, msgUscente.getMessageId());
			pstmt.setString(2, msgUscente.getCodiceAOO());
			pstmt.setString(3, msgUscente.getAccountPec());

			riprova = true;
			i = 0;

			while(riprova || i==numTentativi){
				try{
					ret = pstmt.executeUpdate();
					riprova = false;
				}catch(Exception e){
					i++;
					Thread.sleep(sleepTentativi);
					riprova = true;
					if(i==numTentativi+1){
						riprova = false;
					}
				}
			}

			if ( internal )
				connPecPei.commit();
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgRicevutoDaRiprotocollare Exception: " + eh.getMessage());		
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if( internal && connPecPei!=null)
					connPecPei.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("aggiornaMsgRicevutoDaRiprotocollare END");		
		return ret;		
	}
	public int aggiornaMsgUscenteInviato(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{
		boolean internal = false;
		if ( connPecPei == null ){
			internal = true;
			try{
				connPecPei = cm.getConnection(Constants.DB_PECPEI);
				log.debug("aggiornaMsgUscenteInviato :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}

		log.debug("aggiornaMsgUscenteInviato START");

		int ret = 0;
		PreparedStatement pstmt = null;
		try	{
			pstmt = connPecPei.prepareStatement(UPDATE_MSG_INVIATO_QRY);

			// segnatura
			pstmt.setString(1, msgUscente.getStato());
			if(msgUscente.getSegnatura()!=null)
				pstmt.setString(2, msgUscente.getSegnatura());
			else
				pstmt.setNull(2, java.sql.Types.VARCHAR);
			pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
			pstmt.setTimestamp(4, new Timestamp((new java.util.Date()).getTime()));
			pstmt.setString(5, msgUscente.getSubstato());
			pstmt.setString(6, msgUscente.getSubject());
			pstmt.setString(7, msgUscente.getMessageId());
			pstmt.setString(8, msgUscente.getCodiceAOO());
			pstmt.setString(9, msgUscente.getAccountPec());

			boolean riprova = true;

			int i = 0;
			int numTentativi = Integer.parseInt(configDao.getProperty("numero_massimo_tentativi_aggiornamentoProtocollo", "3"));
			int sleepTentativi = Integer.parseInt(configDao.getProperty("sleep_tentativi_aggiornamentoProtocollo", "5000"));
			while(riprova || i==numTentativi){
				try{
					ret = pstmt.executeUpdate();
					riprova = false;
				}catch(Exception e){
					i++;
					Thread.sleep(sleepTentativi);
					riprova = true;
					if(i==numTentativi+1){
						riprova = false;
						//						executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
						//						executor.executeCommand();
					}
				}
			}
			doInsertAllegati(connPecPei, msgUscente.getMessageId(), msgUscente.getCodiceAOO(), msgUscente.getElencoFileAllegati());

			if ( internal )
				connPecPei.commit();
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgUscenteInviato Exception: " + eh.getMessage());
			//System.err.println("MessaggiDAO :: aggiornaMsgUscenteInviato Exception: " + eh.getMessage());
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if( internal && connPecPei!=null)
					connPecPei.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("aggiornaMsgUscenteInviato END");		
		return ret;		
	}



	public int aggiornaMsgUscenteProtocollato(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{
		boolean internal = false;
		if ( connPecPei == null ){
			internal = true;
			try{
				connPecPei = cm.getConnection(Constants.DB_PECPEI);
				log.debug("aggiornaMsgUscenteProtocollato :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}

		log.debug("aggiornaMsgUscenteProtocollato START");

		int ret = 0;
		PreparedStatement pstmt = null;
		try	{
			pstmt = connPecPei.prepareStatement(UPDATE_MSG_USCENTE_PROTOCOLLATO_QRY);

			// segnatura


			pstmt.setString(1, msgUscente.getSegnatura());
			pstmt.setString(2, msgUscente.getMessageId());
			pstmt.setString(3, msgUscente.getCodiceAOO());
			pstmt.setString(4, msgUscente.getAccountPec());

			ret = pstmt.executeUpdate();

			if ( internal )
				connPecPei.commit();
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgUscenteProtocollato Exception: " + eh.getMessage());
			//System.err.println("MessaggiDAO :: aggiornaMsgUscenteInviato Exception: " + eh.getMessage());
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if( internal && connPecPei!=null)
					connPecPei.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("aggiornaMsgUscenteProtocollato END");		
		return ret;		
	}

	public boolean verificaMessaggioEntrante(MessaggioDTO msgEntrante){
		PreparedStatement pstmtNew = null;
		Connection connPecPei = null;
		boolean messaggioPresente = false;
		ResultSet rs = null;
		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);	
			pstmtNew =  connPecPei.prepareStatement("select messageId from Messaggi with(nolock) " +
					" where messageId = ? and" +
					" accountPec = ? and" +
			" codiceAOO = ? ");
			pstmtNew.setString(1, msgEntrante.getMessageId());
			pstmtNew.setString(2, msgEntrante.getAccountPec());
			pstmtNew.setString(3, msgEntrante.getCodiceAOO());
			rs = pstmtNew.executeQuery();
			if(rs.next()){
				messaggioPresente=true;
			}
		}catch(Exception e){
			log.error("errore verificaMessaggioEntrante :: usa la connection: " + connPecPei+ e.getMessage());
		}finally{
			try{
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmtNew!=null)
					pstmtNew.close();
				if(rs!=null)
					rs.close();
			}catch(Exception e){}

		}
		return messaggioPresente;
	}
	public int aggiornaMsgEntranteRicevuto(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{
		boolean internal = false;
		if ( connPecPei == null ){
			internal = true;
			try{
				connPecPei = cm.getConnection(Constants.DB_PECPEI);
				log.debug("aggiornaMsgUscenteInviato :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}

		log.debug("aggiornaMsgEntranteRicevuto START");
		int ret = 0;
		PreparedStatement pstmt = null;
		try	{
			pstmt = connPecPei.prepareStatement(UPDATE_MSG_SCARICATO_QRY);

			// segnatura
			if(msgUscente.getSegnatura()!=null)
				pstmt.setString(1, msgUscente.getSegnatura());
			else
				pstmt.setNull(1, java.sql.Types.VARCHAR);
			pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
			pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
			pstmt.setString(4, msgUscente.getSubstato());
			pstmt.setString(5, msgUscente.getMessageId());
			pstmt.setString(6, msgUscente.getCodiceAOO());
			pstmt.setString(7, msgUscente.getAccountPec());

			int i = 0;
			int numTentativi = Integer.parseInt(configDao.getProperty("numero_massimo_tentativi_aggiornamentoProtocollo", "3"));
			int sleepTentativi = Integer.parseInt(configDao.getProperty("sleep_tentativi_aggiornamentoProtocollo", "5000"));
			boolean riprova = true;
			while(riprova || i==numTentativi){
				try{
					ret = pstmt.executeUpdate();
					riprova = false;
				}catch(Exception e){
					i++;
					Thread.sleep(sleepTentativi);
					riprova = true;
					if(i==numTentativi+1){
						riprova = false;
						//						executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
						//						executor.executeCommand();
					}
				}
			}
			//			ret = pstmt.executeUpdate();

			if ( internal )
				connPecPei.commit();
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgEntranteRicevuto Exception: " + eh.getMessage());
			//System.err.println("MessaggiDAO :: aggiornaMsgUscenteInviato Exception: " + eh.getMessage());
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if( internal && connPecPei!=null)
					connPecPei.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("aggiornaMsgUscenteInviato END");		
		return ret;		
	}

	private int inserisciMsgEntrantiPEI(Connection connPecPei, MessaggioDTO messaggioUscente, List<MessaggioDTO> elencoMsgEntranti) throws Exception{

		log.debug("inserisciMsgEntranti START");
		int rowCount = -1;						
		for(MessaggioDTO msgDTO: elencoMsgEntranti){
			try{	
				msgDTO.setSegnaturaMitt(messaggioUscente.getSegnatura());
				inserisciMessaggio(connPecPei, msgDTO);
				if(messaggioUscente.getConfermaRicezione().equalsIgnoreCase("si")){
					MessaggioDTO ricevuta = selezionaDettaglioPerRicevutaPEI(connPecPei, messaggioUscente, msgDTO.getSegnatura(), msgDTO.getCodiceAOO());
					inserisciMessaggio(connPecPei, ricevuta);
				}
			}
			catch(Exception ex){
				ex.printStackTrace();
				log.error("inserisciMsgEntranti Exception: " + ex.getMessage());
				//System.err.println("MessaggiDAO :: inserisciMsgEntranti Exception: " + ex.getMessage());
				throw ex;
			}
		} // for su elencoMsgEntranti

		log.debug("inserisciMsgEntranti END");				
		return rowCount;

	} //inserisciMsgEntranti

	private int inserisciMsgEntrantiPEI(Connection connPecPei, MessaggioDTO messaggioUscente, MessaggioDTO msgDTO) throws Exception{

		log.debug("inserisciMsgEntranti START");
		int rowCount = -1;						
		//		for(MessaggioDTO msgDTO: elencoMsgEntranti){
		try{	
			msgDTO.setSegnaturaMitt(messaggioUscente.getSegnatura());
			inserisciMessaggio(connPecPei, msgDTO);
			if(messaggioUscente.getConfermaRicezione().equalsIgnoreCase("si")){
				MessaggioDTO ricevuta = selezionaDettaglioPerRicevutaPEI(connPecPei, messaggioUscente, msgDTO.getSegnatura(), msgDTO.getCodiceAOO());
				inserisciMessaggio(connPecPei, ricevuta);
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error("inserisciMsgEntranti Exception: " + ex.getMessage());
			//System.err.println("MessaggiDAO :: inserisciMsgEntranti Exception: " + ex.getMessage());
			throw ex;
		}
		//		} // for su elencoMsgEntranti

		log.debug("inserisciMsgEntranti END");				
		return rowCount;

	} //inserisciMsgEntranti
	private int inserisciMsgEntranti(Connection connPecPei, MessaggioDTO messaggioUscente, List<MessaggioDTO> elencoMsgEntranti) throws Exception{

		log.debug("inserisciMsgEntranti START");
		int rowCount = -1;

		for(MessaggioDTO msgDTO: elencoMsgEntranti){
			try{	
				inserisciMessaggio(connPecPei, msgDTO);
				doInsertDestinatari(connPecPei, msgDTO.getMessageId(), msgDTO.getElencoDestinatariPer(), msgDTO);
				doInsertDestinatari(connPecPei, msgDTO.getMessageId(), msgDTO.getElencoDestinatariCc(), msgDTO);
				doInsertAllegati(connPecPei, msgDTO.getMessageId(), msgDTO.getCodiceAOO(), msgDTO.getElencoFileAllegati() );
			}catch(Exception ex){
				ex.printStackTrace();
				log.error("inserisciMsgEntranti Exception: " + ex.getMessage());
				//System.err.println("MessaggiDAO :: inserisciMsgEntranti Exception: " + ex.getMessage());
				throw ex;
			}
		} // for su elencoMsgEntranti

		log.debug("inserisciMsgEntranti END");				
		return rowCount;

	} //inserisciMsgEntranti

	public String selezionaMessaggioInUscitaSGD(String msgId){
		log.debug("selezionaMessaggioInUscitaSGD :: START");
		log.info("verifico se il msgId "+msgId+" � un messaggio SGD");

		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String messageId = "";
		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(GET_MSG_SGD_IN_USCITA);
			pstmt.setString(1, msgId);
			pstmt.setString(2, ConfigProp.canaleSgd);

			rs  = pstmt.executeQuery();		

			if(rs != null){
				rs.next();
				messageId = rs.getString(1);
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		if(messageId.length() > 0)
			log.info("Il msgId "+msgId+" � un messaggio SGD");
		else
			log.info("Il msgId "+msgId+" NON � un messaggio SGD");

		log.debug("selezionaMessaggioInUscitaSGD :: END");

		return messageId;
	}

	public List<MessaggioDTO> selezionaMessaggiDaProtocollarePEI() throws DAOException{

		System.out.println("$ - Sono nel metodo");
		log.debug("selezionaMessaggiDaProtocollarePEI :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
		try
		{
			System.out.println("Faccio la query.");
			//			DBConnectionManager cm = new DBConnectionManager();
			//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//			connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEI_DA_PROTOCOLLARE_QRY);
			rs  = pstmt.executeQuery();

			listaDestinatariPer = new ArrayList<DestinatarioDTO>();
			while (rs.next()) {
				String msgID = rs.getString("MessageID");
				System.out.println("MsgID da potocollare: " + msgID);
				//				if(!msgID.equals(currentMsgID)){
				//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
				// reset
				messaggioPeiDTO = new MessaggioDTO();
				destDTO = new DestinatarioDTO();

				listaDestinatariCC = new ArrayList<DestinatarioDTO>();

				messaggioPeiDTO.setMessageId(msgID);
				messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
				messaggioPeiDTO.setCodiceAOO(rs.getString("CodiceAOO"));
				messaggioPeiDTO.setSubject(rs.getString("subject"));
				messaggioPeiDTO.setBody(rs.getString("body"));
				messaggioPeiDTO.setVerso(rs.getString("verso"));
				messaggioPeiDTO.setClassifica(rs.getString("classifica"));
				messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
				messaggioPeiDTO.setSensibile(rs.getString("Sensibile"));
				messaggioPeiDTO.setRiservato(rs.getString("Riservato"));
				messaggioPeiDTO.setTipoMessaggio("messaggio-pei");

				//messaggioPeiDTO.setDominoPec("dominioPEC");

				System.out.println(messaggioPeiDTO);
				List<DestinatarioDTO> listaDest  = getDestinatariPEI(connPecPei, msgID);

				for(DestinatarioDTO dest:listaDest){	
					destDTO.setAddr(dest.getAddr());
					destDTO.setType(dest.getType());
					destDTO.setName(dest.getName());
					//int slashIdx = addr.indexOf("/");
					String codiceAOO_dest = dest.getAddr().substring(0, 4);

					System.out.println("##CODICEAOO destinatario da protocollare:  " + codiceAOO_dest);

					log.debug("selezionaMessaggiDaProtocollarePEI :: codiceAOO_dest=" + codiceAOO_dest);						
					destDTO.setCodiceAOO(codiceAOO_dest);
					if(destDTO.getType().equals("per")) {
						System.out.println("##DESTINATARIO:  " + destDTO.getAddr());
						listaDestinatariPer.add(destDTO);
					}
					else
						listaDestinatariCC.add(destDTO);
				}

				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);
				//				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
				listaMessaggiInUscita.add(messaggioPeiDTO);

				System.out.println("MessaggioPEI DTO : " + messaggioPeiDTO);
			} //while

			for (MessaggioDTO o : listaMessaggiInUscita) {
				System.out.println("Lista Destinatari 'USCITA'" + o.getCodiceAOO());
			}
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiDaProtocollarePEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();

			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("selezionaMessaggiDaProtocollarePEI :: END, numero messaggi PEI in da Protocollare=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita;
	}
	public List<MessaggioDTO> selezionaMessaggiInUscitaPEI() throws DAOException{

		log.debug("selezionaMessaggiInUscitaPEI :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			System.out.println("Seleziono i metodi in uscita PEI -> faccio la query per MSGID");
			System.out.println("Il messageID e': <04105655ABE54E9FC5C4D520520C4780>");
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEI_USCENTI_QRY);
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {

				String msgID = rs.getString("MessageID");
				System.out.println("$ - MessageID " + msgID);
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();

					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setCodiceAOO(rs.getString("CodiceAOO"));
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
					messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
					messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
					messaggioPeiDTO.setNomeSede(rs.getString("nomeSede"));
					messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
					messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));
					messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
					messaggioPeiDTO.setSensibile(rs.getString("Sensibile"));
					messaggioPeiDTO.setRiservato(rs.getString("Riservato"));
					messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
					messaggioPeiDTO.setSegnatura(rs.getString("segnatura"));
					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));
						destDTO.setName(rs.getString("Name"));
						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));	
						destDTO.setName(rs.getString("Name"));
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);

						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);

				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita;
	}

	public MessaggioDTO selezionaDettaglioMessaggioInUscitaPEI( String messageID ) throws DAOException{

		log.debug("selezionaMessaggiInUscitaPEI :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();

		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEI_USCENTI_QRY2);
			pstmt.setString( 1, messageID );
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {

				String msgID = rs.getString("MessageID");
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();

					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setCodiceAOO( rs.getString("CodiceAOO") );
					messaggioPeiDTO.setUiDoc( rs.getString("UiDoc") );
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setNomeSede( rs.getString("nomeSede") );
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setStato( rs.getString( "Stato" ) );
					messaggioPeiDTO.setRiservato( rs.getString("Riservato") );
					messaggioPeiDTO.setSensibile( rs.getString("Sensibile") );
					messaggioPeiDTO.setSize(rs.getInt("size"));
					messaggioPeiDTO.setIpAddress(rs.getString("IPAddress"));
					messaggioPeiDTO.setNomeRegione(rs.getString("nomeRegione"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setDesClassifica(rs.getString("desClassifica"));
					messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
					messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));					
					messaggioPeiDTO.setAutoreCompositore(rs.getString("AutoreCompositore"));
					messaggioPeiDTO.setAutoreUltimaModifica(rs.getString("AutoreUltimaModifica"));
					messaggioPeiDTO.setAutoreMittente(rs.getString("AutoreMittente"));
					messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
					messaggioPeiDTO.setAddrConfermaRicezione(rs.getString("AddrConfermaRicezione"));
					messaggioPeiDTO.setDominoPec(rs.getString("dominioPEC"));
					messaggioPeiDTO.setDocumentoPrincipale(rs.getString("documentoPrincipale"));
					messaggioPeiDTO.setMessaggio(rs.getBytes("messaggio"));
					messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
					messaggioPeiDTO.setXTrasporto(rs.getString("X_trasporto"));
					messaggioPeiDTO.setMsgId(rs.getString("MsgID"));
					messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
					messaggioPeiDTO.setReplyTo(rs.getString("ReplyTo"));
					messaggioPeiDTO.setIsReply(rs.getString("isReply"));
					messaggioPeiDTO.setInReplyTo(rs.getString("inReplyTo"));
					messaggioPeiDTO.setInReplyToSegnatura(rs.getString("inReplyToSegnatura"));
					messaggioPeiDTO.setInReplyToSubject(rs.getString("inReplyToSubject"));
					messaggioPeiDTO.setSegnaturaRif(rs.getString("SegnaturaRif"));
					messaggioPeiDTO.setSegnaturaRoot(rs.getString("SegnaturaRoot"));
					messaggioPeiDTO.setCodiceAMM(rs.getString("CodiceAMM"));
					messaggioPeiDTO.setCodiceAOOMitt(rs.getString("CodiceAOOMitt"));
					messaggioPeiDTO.setCodiceAMMMitt(rs.getString("CodiceAMMMitt"));
					messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
					messaggioPeiDTO.setBodyHtml(rs.getString("BodyHtml"));
					messaggioPeiDTO.setIsSPAMorCCN(rs.getInt("isSPAMorCCN"));
					messaggioPeiDTO.setTipoMitt(rs.getString("tipoMitt"));
					messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
					messaggioPeiDTO.setDirettoreAOO(rs.getString("DirettoreAOO"));
					messaggioPeiDTO.setDirettoreAOOMitt(rs.getString("DirettoreAOOMitt"));
					messaggioPeiDTO.setSubstato(rs.getString("substato"));
					messaggioPeiDTO.setRicevuta(rs.getString("X_Ricevuta"));

					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));
						destDTO.setName(rs.getString("name"));
						//						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}

					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));	
						destDTO.setName(rs.getString("name"));
						//						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);

						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}

				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);

				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita.get(0);
	}

	public MessaggioDTO selezionaDettaglioPerRicevutaPEI( Connection connPecPei, MessaggioDTO messaggioUscente, String segnaturaDest, String codAOOMit ) throws DAOException{

		log.debug("selezionaDettaglioPerRicevutaPEI :: START");
		//	PreparedStatement pstmt = null;
		//	ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();

		try
		{
			SedeDTO sedeMittente = new SedeDAO().selezioneSedeFromAOO(codAOOMit);
			//		connPecPei = cm.getConnection(Constants.DB_PECPEI);
			//		pstmt = connPecPei.prepareStatement(SELECT_MSG_PEI_USCENTI_QRY2);
			//		pstmt.setString( 1, messageID );
			//		rs  = pstmt.executeQuery();			
			Timestamp dataInserimento = new Timestamp(new java.util.Date().getTime());
			//		while (rs.next()) {
			String msgID = messaggioUscente.getMessageId();
			messaggioPeiDTO = new MessaggioDTO();
			destDTO = new DestinatarioDTO();
			listaDestinatariPer = new ArrayList<DestinatarioDTO>();
			listaDestinatariCC = new ArrayList<DestinatarioDTO>();
			//			messaggioPeiDTO = messaggioUscente;
			messaggioPeiDTO = (MessaggioDTO) SerializationUtils.clone(messaggioUscente);
			String messageIdRicevuta = StaticUtil.generateMessageIdRicevutaPei(msgID,sedeMittente.getCodice());
			messaggioPeiDTO.setMessageId(messageIdRicevuta);
			messaggioPeiDTO.setMsgId(msgID);
			//			messaggioPeiDTO.setCodiceAOO( rs.getString("CodiceAOO") );
			messaggioPeiDTO.setUiDoc(messageIdRicevuta);
			messaggioPeiDTO.setVerso("E");
			messaggioPeiDTO.setNomeSede("");
			messaggioPeiDTO.setSubject("RICEVUTA di PROTOCOLLAZIONE PEI: "+messaggioUscente.getSubject());
			//			messaggioPeiDTO.setBody(rs.getString("body"));
			messaggioPeiDTO.setStato("RI");
			messaggioPeiDTO.setRiservato( "N" );
			messaggioPeiDTO.setSensibile( "N" );
			messaggioPeiDTO.setSize(0);
			//			messaggioPeiDTO.setIpAddress(rs.getString("IPAddress"));
			messaggioPeiDTO.setNomeRegione("");
			messaggioPeiDTO.setClassifica("");
			messaggioPeiDTO.setDesClassifica("");
			//			messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
			//			messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
			//			messaggioPeiDTO.setFromAddr(messaggioPeiDTO.getFromAddr());
			//			messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));					
			messaggioPeiDTO.setAutoreCompositore("");
			messaggioPeiDTO.setAutoreUltimaModifica("");
			messaggioPeiDTO.setAutoreMittente("");
			messaggioPeiDTO.setConfermaRicezione("no");
			messaggioPeiDTO.setAddrConfermaRicezione("");
			messaggioPeiDTO.setDominoPec("@inps.it");
			messaggioPeiDTO.setDocumentoPrincipale("");
			messaggioPeiDTO.setMessaggio("".getBytes());
			messaggioPeiDTO.setTipologia("X-Ricevuta");
			//			messaggioPeiDTO.setXTrasporto(rs.getString("X_trasporto"));
			//			messaggioPeiDTO.setMsgId(rs.getString("messageId"));
			messaggioPeiDTO.setTipoMessaggio("ricezione-protocollazione-pei");
			//			messaggioPeiDTO.setReplyTo(rs.getString("ReplyTo"));
			messaggioPeiDTO.setIsReply("1");
			messaggioPeiDTO.setInReplyTo(messaggioPeiDTO.getMsgId());
			//			messaggioPeiDTO.setInReplyToSegnatura(rs.getString("Segnatura"));
			//			messaggioPeiDTO.setInReplyToSubject(rs.getString("Subject"));
			messaggioPeiDTO.setSegnaturaRif(segnaturaDest);
			messaggioPeiDTO.setSegnaturaRoot(segnaturaDest);
			messaggioPeiDTO.setCodiceAMM("INPS");
			messaggioPeiDTO.setCodiceAOOMitt(codAOOMit);
			messaggioPeiDTO.setCodiceAMMMitt("INPS");
			messaggioPeiDTO.setSegnaturaMitt(segnaturaDest);
			messaggioPeiDTO.setTipoRicevuta("no");
			messaggioPeiDTO.setBodyHtml("");
			messaggioPeiDTO.setIsSPAMorCCN(0);
			messaggioPeiDTO.setTipoMitt("M");
			messaggioPeiDTO.setDesMitt(sedeMittente.getCodice()+"__/"+sedeMittente.getRegione());
			//			messaggioPeiDTO.setDirettoreAOO(rs.getString("DirettoreAOO"));
			messaggioPeiDTO.setDirettoreAOOMitt(sedeMittente.getMatricolaDirettore()+" "+sedeMittente.getDirettore()+"/"+sedeMittente.getCodice()+"/"+sedeMittente.getProvincia()+"INPS/IT");
			//			messaggioPeiDTO.setSubstato(rs.getString("substato"));
			messaggioPeiDTO.setRicevuta("ricevuta-pei");
			messaggioPeiDTO.setPostedDate(dataInserimento);
			messaggioPeiDTO.setDeliveredDate(dataInserimento);
			messaggioPeiDTO.setDataOraUltimaModifica(dataInserimento);
			messaggioPeiDTO.setBody(StaticUtil.preparaBodyPerRicevutaPEI(messaggioUscente,messaggioPeiDTO));
			//			String addr = rs.getString("Addr");
			//			if(addr != null){	
			//				destDTO.setAddr(addr);
			//				destDTO.setType(rs.getString("Type_per_cc"));
			//				String codiceAOO_dest = addr.substring(0, 4);
			//				log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
			//				destDTO.setCodiceAOO(codiceAOO_dest);
			//				if(destDTO.getType().equals("per"))
			//					listaDestinatariPer.add(destDTO);
			//				else
			//					listaDestinatariCC.add(destDTO);
			//			}
			//			currentMsgID = msgID;
			//			messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
			//			messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);
			//			if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
			//				listaMessaggiInUscita.add(messaggioPeiDTO);
			//	    }
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			//		try {
			//			if(pstmt!=null)
			//				pstmt.close();
			//			if(rs!=null)
			//				rs.close();
			//
			//	    }catch (Exception e) {
			//	    	e.printStackTrace();
			//	    }
		}
		log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
		return messaggioPeiDTO;
	}

	/*public MessaggioDTO selezionaDettaglioMessaggioInUscitaPEI( String msgID ) throws DAOException{

		log.debug("selezionaDettaglioMessaggioInUscitaPEI :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<AllegatoDTO> listaAllegati = null;

		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEI_USCENTI_QRY2);
			pstmt.setString( 1, msgID );
			rs  = pstmt.executeQuery();			

			while (rs.next()) {



					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();

					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setCodiceAOO( rs.getString("CodiceAOO") );
					messaggioPeiDTO.setUiDoc( rs.getString("UiDoc") );
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setNomeSede( rs.getString("nomeSede") );
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setStato( rs.getString( "Stato" ) );
					messaggioPeiDTO.setRiservato( rs.getString("Riservato") );
					messaggioPeiDTO.setSensibile( rs.getString("Sensibile") );
					messaggioPeiDTO.setSize(rs.getInt("size"));
					messaggioPeiDTO.setIpAddress(rs.getString("IPAddress"));
					messaggioPeiDTO.setNomeRegione(rs.getString("nomeRegione"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setDesClassifica(rs.getString("desClassifica"));
					messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
					messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));					
					messaggioPeiDTO.setAutoreCompositore(rs.getString("AutoreCompositore"));
					messaggioPeiDTO.setAutoreUltimaModifica(rs.getString("AutoreUltimaModifica"));
					messaggioPeiDTO.setAutoreMittente(rs.getString("AutoreMittente"));
					messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
					messaggioPeiDTO.setAddrConfermaRicezione(rs.getString("AddrConfermaRicezione"));
					messaggioPeiDTO.setDominoPec(rs.getString("dominioPEC"));
					messaggioPeiDTO.setDocumentoPrincipale(rs.getString("documentoPrincipale"));
					messaggioPeiDTO.setMessaggio(rs.getBytes("messaggio"));
					messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
					messaggioPeiDTO.setXTrasporto(rs.getString("X_trasporto"));
					messaggioPeiDTO.setMsgId(rs.getString("MsgID"));
					messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
					messaggioPeiDTO.setReplyTo(rs.getString("ReplyTo"));
					messaggioPeiDTO.setIsReply(rs.getString("isReply"));
					messaggioPeiDTO.setInReplyTo(rs.getString("inReplyTo"));
					messaggioPeiDTO.setInReplyToSegnatura(rs.getString("inReplyToSegnatura"));
					messaggioPeiDTO.setInReplyToSubject(rs.getString("inReplyToSubject"));
					messaggioPeiDTO.setSegnaturaRif(rs.getString("SegnaturaRif"));
					messaggioPeiDTO.setSegnaturaRoot(rs.getString("SegnaturaRoot"));
					messaggioPeiDTO.setCodiceAMM(rs.getString("CodiceAMM"));
					messaggioPeiDTO.setCodiceAOOMitt(rs.getString("CodiceAOOMitt"));
					messaggioPeiDTO.setCodiceAMMMitt(rs.getString("CodiceAMMMitt"));
					messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
					messaggioPeiDTO.setBodyHtml(rs.getString("BodyHtml"));
					messaggioPeiDTO.setIsSPAMorCCN(rs.getInt("isSPAMorCCN"));
					messaggioPeiDTO.setTipoMitt(rs.getString("tipoMitt"));
					messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
					messaggioPeiDTO.setDirettoreAOO(rs.getString("DirettoreAOO"));
					messaggioPeiDTO.setDirettoreAOOMitt(rs.getString("DirettoreAOOMitt"));
					messaggioPeiDTO.setSubstato(rs.getString("substato"));
					messaggioPeiDTO.setRicevuta(rs.getString("X_Ricevuta"));

					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));

						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaDettaglioMessaggioInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}



				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);

				return messaggioPeiDTO;

		    } //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaDettaglioMessaggioInUscitaPEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}

		log.debug("selezionaMessaggiInUscitaPEI :: END ");		
		return null;
	}

	 */
	public List<DestinatarioDTO> getDestinatariPEI(Connection conn, String messageID) throws Exception {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<DestinatarioDTO> destinatari = new ArrayList<DestinatarioDTO>();
		DestinatarioDTO destinatario ;
		String query = "select MessageID, Addr, Phrase, Type_per_cc, [Name], postedDate from destinatari where MessageID = ?";

		pstmt = conn.prepareStatement(query);
		pstmt.setString(1, messageID);
		rs = pstmt.executeQuery();

		while(rs.next()){
			destinatario = new DestinatarioDTO();
			destinatario.setAddr(rs.getString("Addr"));
			destinatario.setMessageID(messageID);
			destinatario.setName(rs.getString("Name"));
			destinatario.setPhrase(rs.getString("Phrase"));
			destinatario.setType(rs.getString("Type_per_cc"));
			destinatari.add(destinatario);
		}

		return destinatari;
	}


	public int insertDestinatario(String[] valori){
		Connection connPecPei = null;
		PreparedStatement pstmt = null;

		int row = 0;

		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(INSERT_DESTINATARIO);

			if(!valori[0].equals("") && valori[0] != null)
				pstmt.setString(1, valori[0]);
			else
				throw new Exception("Errore in insertDestinatario della classe MessaggiDAO nel recupero del valore MessageID");

			if(!valori[1].equals("") && valori[1] != null)
				pstmt.setString(2, valori[1]);
			else
				pstmt.setNull(2, 0);

			if(!valori[2].equals("") && valori[2] != null)
				pstmt.setString(3, valori[2]);
			else
				pstmt.setNull(3, 0);

			if(!valori[3].equals("") && valori[3] != null)
				pstmt.setString(4, valori[3]);
			else
				pstmt.setNull(4, 0);

			if(!valori[4].equals("") && valori[4] != null)
				pstmt.setString(5, valori[4]);
			else
				pstmt.setNull(5, 0);

			//prima di fare la insert verifico se l'indirizzo � gia stato elaborato

			if(!indirizzoGiaInserito(connPecPei,valori[3])){
				row = pstmt.executeUpdate();
				//				connPecPei.commit();
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				if(connPecPei!=null)
					pstmt.close();
				connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return row;
	}

	public boolean indirizzoGiaInserito(Connection conn, String indirizzo){
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean esiste = false;

		try{
			pstmt = conn.prepareStatement(SELECT_INDIRIZZO);
			pstmt.setString(1, indirizzo);
			rs = pstmt.executeQuery();

			rs.next();
			System.out.println("Trovati "+rs.getInt(1)+" indirizzi "+indirizzo+" in fase di inserimento");
			if(rs.getInt(1) > 0)
				esiste = true;

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				pstmt.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return esiste;
	}


	public List<GestDatiCert> selectDatiCert(){

		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<GestDatiCert> listDatiCert = new ArrayList<GestDatiCert>();
		GestDatiCert datiCert = null;

		try{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_DATICERT);
			pstmt.setString(1, "accettazione");
			pstmt.setString(2, "daticert.xml");

			rs = pstmt.executeQuery();

			while(rs.next()){
				datiCert = new GestDatiCert();
				datiCert.setMessageId(rs.getString(1));
				datiCert.setPosteddate(rs.getString(2));
				datiCert.setNomeallegato(rs.getString(3));
				datiCert.setAllegato(rs.getBytes(4));

				listDatiCert.add(datiCert);
			}


		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				if(connPecPei!=null)
					pstmt.close();
				connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return listDatiCert;
	}

	public int doInsertDestinatari(Connection conn, String messageID, List<DestinatarioDTO> destinatari, MessaggioDTO msg) throws Exception {
		if ( destinatari == null )
			return -1;
		int key = -1;

		for ( DestinatarioDTO destinatario: destinatari ){
			PreparedStatement pstmt = null;

			String querySelect = "SELECT * FROM DESTINATARI WHERE MessageID = ? AND Addr=? AND Name=? ";
			pstmt = conn.prepareStatement(querySelect);
			pstmt.setString(1, messageID);
			if (destinatario.getAddr() != null)
				pstmt.setString(2, destinatario.getAddr());
			else
				pstmt.setNull(2, java.sql.Types.VARCHAR);
			if (destinatario.getName() != null)
				pstmt.setString(3, destinatario.getName());
			else {
				// in caso di PEC il Name non � valorizzato
				if (destinatario.getAddr() != null) {
					// viene valorizzato con l'addr
					pstmt.setString(3, destinatario.getAddr());
				} else {
					// se non � valorizzato neanche l'addr si mette x perch� Name �
					// chiave!!!
					pstmt.setString(3, "x");
				}
			}
			ResultSet rs = pstmt.executeQuery();
			if(!rs.next()){
				String query = "INSERT INTO destinatari"
					+ " (MessageID,Addr,Phrase,Type_per_cc,Name, postedDate)"
					+ " VALUES(?,?,?,?,?,?)";
				pstmt = conn.prepareStatement(query);

				pstmt.setString(1, messageID);

				if (destinatario.getAddr() != null&&destinatario.getAddr().length()>4)
					pstmt.setString(2, destinatario.getAddr());
				else
					pstmt.setNull(2, java.sql.Types.VARCHAR);

				String phrase = destinatario.getPhrase();
				if(phrase!=null&&phrase.length()>=200)
					phrase = phrase.substring(phrase.length()-200);

				if (destinatario.getPhrase() != null)
					pstmt.setString(3, phrase);
				else
					pstmt.setNull(3, java.sql.Types.VARCHAR);

				if (destinatario.getType() != null)
					pstmt.setString(4, destinatario.getType());
				else
					pstmt.setNull(4, java.sql.Types.VARCHAR);

				String name = destinatario.getName();
				if(name.length()>=200)
					name = phrase.substring(50);

				if (destinatario.getName() != null)
					pstmt.setString(5, name);
				else {
					// in caso di PEC il Name non � valorizzato
					if (destinatario.getAddr() != null) {
						// viene valorizzato con l'addr
						pstmt.setString(5, destinatario.getAddr());
					} else {
						// se non � valorizzato neanche l'addr si mette x perch� Name �
						// chiave!!!
						pstmt.setString(5, "x");
					}
				}
				if (msg.getPostedDate() != null)
					pstmt.setTimestamp(6, new Timestamp(msg.getPostedDate().getTime()));
				else
					pstmt.setNull(6, java.sql.Types.TIMESTAMP);

				key = pstmt.executeUpdate();
			}
			pstmt.close();
		}

		return key;

	}

	public int doInsertAllegato(Connection connPecPei, String messageId, String codAOO, AllegatoDTO allegato) throws Exception {

		PreparedStatement pstmt = null;
		int result=0;
		boolean internal = false;
		boolean error = false;
		StringBuilder errors = new StringBuilder("");

		try{			
			if ( connPecPei == null ){
				internal = true;
				connPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
			}
			// Con Mese Anno
			//			String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,postedDate,meseAnno )" +
			//			" VALUES(?,?,?,?,?,?,?,?,?)";

			String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,postedDate)" +
			" VALUES(?,?,?,?,?,?,?,?)";

			pstmt =  connPecPei.prepareStatement(query);

			pstmt.setString(1, messageId);
			pstmt.setString(2, allegato.getFileName());
			pstmt.setString(3, allegato.getTipoAllegato());
			pstmt.setInt(4, allegato.getFileSize());
			pstmt.setBytes(5, allegato.getFileData());
			pstmt.setString(6, allegato.getContentType());
			if(allegato.getTestoAllegato()!=null) {
				pstmt.setString(7, allegato.getTestoAllegato());
			}else{
				pstmt.setNull(7,java.sql.Types.VARCHAR);
			}
			pstmt.setTimestamp(8, allegato.getPostedDate());
			//Con Mese Anno			
			/*		
			if(allegato.getMeseAnno()!=null){
				pstmt.setString(9, allegato.getMeseAnno());
			}else{
				pstmt.setNull(9, java.sql.Types.VARCHAR);
			}
			 */
			result = pstmt.executeUpdate();

		}catch(Exception e){
			error = true;
			errors.append( e.getMessage() );
			errors.append("\n");
		}finally{
			if(internal){
				if(connPecPei!=null)
					connPecPei.close();
			}
			if(pstmt!=null)
				pstmt.close();
			if (error){
				throw new BusinessException(-6060, "Errori nell'inserimento allegati. Dettaglio:\n" + errors.toString());
			}
		}

		return result;
	}

	public int doInsertAllegati(Connection conn, String messageId, String codAOO, List<AllegatoDTO> elencoFileAllegati) throws Exception {

		PreparedStatement pstmt = null;
		int result=0;
		Iterator<AllegatoDTO> it = elencoFileAllegati.iterator();
		StringBuilder errors = new StringBuilder();
		ResultSet rs = null;
		boolean error = false;

		while(it.hasNext()){
			try{
				AllegatoDTO allegato = it.next();
				//						Con mese anno
				//						String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,PostedDate,meseAnno)" +
				//						" VALUES(?,?,?,?,?,?,?,?,?)";
				String verificaAllegati = "select NomeAllegato from View_Allegati with(nolock) where messageId = ? and NomeAllegato = ?";
				pstmt =  conn.prepareStatement(verificaAllegati);
				pstmt.setString(1, messageId);
				String nomeFile = allegato.getFileName();
				if(nomeFile.length()>=255) nomeFile=nomeFile.substring(15);
				pstmt.setString(2, nomeFile);
				rs = pstmt.executeQuery();
				if(!rs.next()){

					String query = "INSERT INTO allegati (MessageId,NomeAllegato,TipoAllegato,sizeAllegato,Allegato,contentType,TestoAllegato,PostedDate)" +
					" VALUES(?,?,?,?,?,?,?,?)";
					pstmt =  conn.prepareStatement(query);

					pstmt.setString(1, messageId);
					nomeFile = allegato.getFileName();
					if(nomeFile.length()>=255) nomeFile=nomeFile.substring(nomeFile.length()-255);
					pstmt.setString(2, nomeFile);
					pstmt.setString(3, allegato.getTipoAllegato().length()>=7?"":allegato.getTipoAllegato());
					pstmt.setInt(4, allegato.getFileSize());
					pstmt.setBytes(5, allegato.getFileData());

					if ( allegato.getContentType() != null && allegato.getContentType().indexOf( ";" ) != -1){
						String contentType = allegato.getContentType();
						contentType = allegato.getContentType().substring(0,allegato.getContentType().indexOf(";"));
						if(contentType.length()>=250)
							contentType = contentType.substring(0,250);
						pstmt.setString(6, contentType);
					}else 
						pstmt.setString(6, "");
					if(allegato.getTestoAllegato()!=null) {
						pstmt.setString(7, allegato.getTestoAllegato());
					}else{
						pstmt.setNull(7,java.sql.Types.VARCHAR);
					}
					if(allegato.getPostedDate()!=null) {
						pstmt.setTimestamp(8, allegato.getPostedDate());
					}else{
						pstmt.setNull(8,java.sql.Types.TIMESTAMP);
					}
					/*
							if(allegato.getMeseAnno()!=null){
								pstmt.setString(9, allegato.getMeseAnno());
							}else{
								pstmt.setNull(9, java.sql.Types.VARCHAR);
							}
					 */
					pstmt.setQueryTimeout(300);
					result = pstmt.executeUpdate();
				}
			} catch (Exception e){
				error = true;
				errors.append( e.getMessage() );
				errors.append("\n");
			}
		}

		if(pstmt!=null)
			pstmt.close();


		if (error){
			throw new BusinessException(-6060, "Errori nell'inserimento allegati. Dettaglio:\n" + errors.toString());
		}

		return result;
	}


	public int inserisciMessaggio(Connection connPecPei, MessaggioDTO msgDTO) throws DAOException{

		log.debug("inserisciMessaggio START");
		int rowCount = -1;
		PreparedStatement pstmt = null;
		boolean internal = false;
		ResultSet rs = null;

		try{			
			if ( connPecPei == null ){
				internal = true;
				connPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
			}

			if(!msgDTO.getTipologia().equalsIgnoreCase("messaggio-pei")){
				pstmt =  connPecPei.prepareStatement("select messageId from MessaggiBlob with(nolock) where messageId = ?");
				pstmt.setString(1, msgDTO.getMessageId());
				rs = pstmt.executeQuery(); 

				if(!rs.next()){
					try{
						pstmt =  connPecPei.prepareStatement("insert into MessaggiBlob ( Messageid ,postedDate ,rawContent ,messaggio ,stato ,[size]) VALUES ( ? ,?,?,?,?,?)");
						pstmt.setString(1, msgDTO.getMessageId());
						pstmt.setTimestamp(2, new Timestamp(msgDTO.getPostedDate().getTime()));
						pstmt.setString(3, msgDTO.getRawContent());
						pstmt.setBytes(4,  creaMessageTxt(msgDTO).getFileData());
						pstmt.setString(5, "DE");
						pstmt.setInt(6, msgDTO.getSize());
						pstmt.executeUpdate();
					}catch(Exception e){
						log.error("inserisciMsgEntrantiBlob Exception: " + e.getMessage()+"sul messaggio  : "+msgDTO.getMessageId()+ "codice sede "+msgDTO.getCodiceAOO());
						throw new Exception("inserisciMsgEntrantiBlob Exception: " + e.getMessage()+"sul messaggio  : "+msgDTO.getMessageId()+ "codice sede "+msgDTO.getCodiceAOO());
					}
				}
			}
			connPecPei.setAutoCommit(false);


			if(!msgDTO.getTipologia().equalsIgnoreCase("messaggio-pei")){

				if(verificaDestinatari(msgDTO.getMessageId())<=0){

					if(msgDTO.getElencoDestinatariPer().size()<=0) throw new Exception("Errore nella gestione dei destinatari");

					if(msgDTO.getElencoFileAllegati().size()<=0) throw new Exception("Errore nella gestione degli allegati");

					doInsertDestinatari(connPecPei, msgDTO.getMessageId(), msgDTO.getElencoDestinatariPer(), msgDTO);
					doInsertDestinatari(connPecPei, msgDTO.getMessageId(), msgDTO.getElencoDestinatariCc(),msgDTO);
					if(!msgDTO.getTipoMessaggio().equalsIgnoreCase("ricezione-protocollazione-pei") && !Constants.AUTORE_COMPOSITORE_PST.equals( msgDTO.getAutoreCompositore() ) )
						doInsertAllegati(connPecPei, msgDTO.getMessageId(), msgDTO.getCodiceAOO(), msgDTO.getElencoFileAllegati() );
				}
			}
			ConfigDAO configDao = new ConfigDAO();
			if(msgDTO.isInteroperabile()){
				if(Boolean.parseBoolean(configDao.getProperty("ABILITA_INS_MSG_ENTRANTE_QRY_INTEROP", "true"))){
					pstmt =  connPecPei.prepareStatement(INS_MSG_ENTRANTE_QRY_INTEROP);
				}else{
					pstmt =  connPecPei.prepareStatement(INS_MSG_ENTRANTE_QRY);
				}
			}else
				pstmt =  connPecPei.prepareStatement(INS_MSG_ENTRANTE_QRY);

			pstmt.setString(1, msgDTO.getMessageId());

			pstmt.setString(2, msgDTO.getCodiceAOO());

			pstmt.setString(3, msgDTO.getUiDoc());

			if(msgDTO.getVerso()!=null)
				pstmt.setString(4, msgDTO.getVerso());
			else
				pstmt.setNull(4, java.sql.Types.VARCHAR);

			if(msgDTO.getNomeSede()!=null)
				pstmt.setString(5, msgDTO.getNomeSede());
			else
				pstmt.setNull(5, java.sql.Types.VARCHAR);

			java.sql.Timestamp postedDate = null;

			if(msgDTO.getStato()!=null && (msgDTO.getStato().equals(Constants.STATO_MSG_DAINVIARE) || msgDTO.getStato().equals(Constants.STATO_MSG_RICEVUTO))){
				if(msgDTO.getPostedDate()!= null){
					postedDate = new java.sql.Timestamp(msgDTO.getPostedDate().getTime());
				}
				pstmt.setTimestamp(6, postedDate);
			}else{
				if(msgDTO.getPostedDate()!=null)
					pstmt.setTimestamp(6,  new java.sql.Timestamp(msgDTO.getPostedDate().getTime()));
				else
					pstmt.setTimestamp(6,  new java.sql.Timestamp(new java.util.Date().getTime()));
			}

			if(msgDTO.getSubject()!=null)
				pstmt.setString(7, msgDTO.getSubject().replaceAll("it.eustema.inps.agentPecPei.dto.SegnaturaDTO@", ""));
			else
				pstmt.setNull(7, java.sql.Types.VARCHAR);

			if(msgDTO.getBody()!=null)
				pstmt.setString(8, msgDTO.getBody().replaceAll("it.eustema.inps.agentPecPei.dto.SegnaturaDTO@", ""));
			else
				pstmt.setNull(8, java.sql.Types.VARCHAR);

			if((msgDTO.getStato()!=null && msgDTO.getRicevuta() != null && !msgDTO.getRicevuta().equals("")) || msgDTO.getSubject().startsWith("ANOMALIA")){
				pstmt.setString(9, Constants.STATO_RICEVUTO);
			}else if(msgDTO.getStato()!=null){
				pstmt.setString(9, msgDTO.getStato());
			}else{
				pstmt.setNull(9, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getRiservato()!=null)
				pstmt.setString(10, msgDTO.getRiservato());
			else
				pstmt.setNull(10, java.sql.Types.VARCHAR);

			if(msgDTO.getSensibile()!=null){
				pstmt.setString(11, msgDTO.getSensibile());
			}else{
				pstmt.setNull(11, java.sql.Types.VARCHAR);
			}

			pstmt.setInt(12, msgDTO.getSize());

			pstmt.setString(13, "Batch PecPei");

			if(msgDTO.getNomeRegione()!=null)
				pstmt.setString(14, msgDTO.getNomeRegione());
			else
				pstmt.setNull(14, java.sql.Types.VARCHAR);

			//id titolario � obbligatorio
			pstmt.setString(15, msgDTO.getClassifica());
			//descrizione titolario � obbligatorio
			pstmt.setString(16, msgDTO.getDesClassifica());

			if(msgDTO.getNomeUfficio()!=null){
				pstmt.setString(17, msgDTO.getNomeUfficio());
			}else{
				pstmt.setNull(17, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getAccountPec()!=null){
				pstmt.setString(18, msgDTO.getAccountPec());
			}else{
				pstmt.setNull(18, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getFromAddr()!=null){
				pstmt.setString(19, msgDTO.getFromAddr());
			}else{
				pstmt.setNull(19, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getFromPhrase()!=null){
				pstmt.setString(20, msgDTO.getFromPhrase());
			}else{
				pstmt.setNull(20, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getAutoreCompositore()!=null){
				pstmt.setString(21,msgDTO.getAutoreCompositore());
			}else{
				pstmt.setNull(21, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getAutoreUltimaModifica()!=null){
				pstmt.setString(22, msgDTO.getAutoreUltimaModifica());
			}else{
				pstmt.setNull(22, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getAutoreMittente()!=null){
				pstmt.setString(23, msgDTO.getAutoreMittente());
			}else{
				pstmt.setNull(23, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getConfermaRicezione()!=null){
				pstmt.setString(24, msgDTO.getConfermaRicezione());
			}else{
				pstmt.setNull(24, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getAddrConfermaRicezione()!=null){
				pstmt.setString(25, msgDTO.getAddrConfermaRicezione());
			}else{
				pstmt.setNull(25, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getDominoPec()!=null){
				pstmt.setString(26, msgDTO.getDominoPec());
			}else{
				pstmt.setNull(26, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getDocumentoPrincipale()!=null){
				pstmt.setString(27, msgDTO.getDocumentoPrincipale());
			}else{
				pstmt.setNull(27, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getMessaggio()!=null){
				pstmt.setBytes(28, msgDTO.getMessaggio());
			}else{
				pstmt.setNull(28, java.sql.Types.VARBINARY);
			}

			if(msgDTO.getTipologia()!=null){
				pstmt.setString(29, msgDTO.getTipologia());
			}else{
				pstmt.setNull(29, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getXTrasporto()!=null){
				pstmt.setString(30, msgDTO.getXTrasporto());
			}else{
				pstmt.setNull(30, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getMsgId()!=null){
				if(msgDTO.getMsgId().length()>=300)
					pstmt.setString(31, msgDTO.getMessageId());
				else
					pstmt.setString(31, msgDTO.getMsgId());
			}else{
				pstmt.setString(31, msgDTO.getMessageId());
			}

			if(msgDTO.getTipoMessaggio()!=null){
				pstmt.setString(32, msgDTO.getTipoMessaggio());
			}else{
				pstmt.setNull(32, java.sql.Types.VARCHAR);
			}

			//ReplyTo se PEC = FromAddr
			if(msgDTO.getReplyTo()!=null){
				pstmt.setString(33, msgDTO.getReplyTo());
			}else{
				pstmt.setNull(33, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getIsReply()!=null){
				pstmt.setString(34, msgDTO.getIsReply());
			}else{
				pstmt.setNull(34, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getInReplyTo()!=null){
				pstmt.setString(35, msgDTO.getInReplyTo());
			}else{
				pstmt.setNull(35, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getInReplyToSegnatura()!=null){
				pstmt.setString(36, msgDTO.getInReplyToSegnatura());
			}else{
				pstmt.setNull(36, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getInReplyToSubject()!=null){
				pstmt.setString(37, msgDTO.getInReplyToSubject());
			}else{
				pstmt.setNull(37, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getSegnaturaRif()!=null){
				pstmt.setString(38, msgDTO.getSegnaturaRif());
			}else{
				pstmt.setNull(38, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getSegnaturaRoot()!=null){
				pstmt.setString(39, msgDTO.getSegnaturaRoot());
			}else{
				pstmt.setNull(39, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getCodiceAMM()!=null){
				pstmt.setString(40,msgDTO.getCodiceAMM());
			}else{
				pstmt.setNull(40, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getCodiceAOOMitt()!=null){
				pstmt.setString(41,msgDTO.getCodiceAOOMitt());
			}else{
				pstmt.setNull(41, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getCodiceAMMMitt()!=null){
				pstmt.setString(42,msgDTO.getCodiceAMMMitt());
			}else{
				pstmt.setNull(42, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getTipoRicevuta()!=null){
				pstmt.setString(43,msgDTO.getTipoRicevuta());
			}else{
				pstmt.setNull(43, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getBodyHtml()!=null){
				pstmt.setString(44,msgDTO.getBodyHtml().replaceAll("it.eustema.inps.agentPecPei.dto.SegnaturaDTO@", ""));
			}else{
				pstmt.setNull(44, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getIsSPAMorCCN()!=null){
				pstmt.setInt(45,msgDTO.getIsSPAMorCCN());
			}else{
				pstmt.setNull(45, java.sql.Types.INTEGER);
			}

			if(msgDTO.getTipoMitt()!=null){
				pstmt.setString(46,msgDTO.getTipoMitt());
			}else{
				pstmt.setNull(46, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getDesMitt()!=null){
				pstmt.setString(47,msgDTO.getDesMitt());
			}else{
				pstmt.setNull(47, java.sql.Types.VARCHAR);
			}

			SedeDAO sede = new SedeDAO();
			SedeDTO sedeDTO = sede.selezioneSedeFromAOO(msgDTO.getCodiceAOO());
			String direttoreSede = "";
			if(sedeDTO!=null)
				direttoreSede = String.format("%s %s/%s/%s/%s/%s",sedeDTO.getMatricolaDirettore(), sedeDTO.getDirettore(), sedeDTO.getCodice(),sedeDTO.getProvincia(),"INPS","IT");

			if(msgDTO.getDirettoreAOO()!=null){
				pstmt.setString(48,msgDTO.getDirettoreAOO());
			}else{
				pstmt.setString(48,direttoreSede);
			}

			if(msgDTO.getDirettoreAOOMitt()!=null){
				pstmt.setString(49,msgDTO.getDirettoreAOOMitt());
			}else{
				pstmt.setNull(49, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getSubstato()!=null){
				pstmt.setString(50,msgDTO.getSubstato());
			}else{
				pstmt.setNull(50, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getRicevuta()!=null){
				pstmt.setString(51,msgDTO.getRicevuta());
			}else{
				pstmt.setNull(51, java.sql.Types.VARCHAR);
			}

			if(msgDTO.getStato()!=null && msgDTO.getStato().equals(Constants.STATO_MSG_RICEVUTO)){
				java.sql.Timestamp deliveredDate = null;
				if(msgDTO.getPostedDate()!=null&&msgDTO.getDeliveredDate()!=null){
					deliveredDate = new java.sql.Timestamp(msgDTO.getDeliveredDate().getTime());
				}
				pstmt.setTimestamp(52, deliveredDate);
			}else{
				pstmt.setNull(52, java.sql.Types.TIMESTAMP);
			}
			// Per le ricevute 
			if((msgDTO.getRicevuta()!=null && (msgDTO.getRicevuta().equals(Constants.nonAccettazione) || msgDTO.getRicevuta().equals(Constants.erroreConsegna)) && msgDTO.getIsSPAMorCCN().equals(0) )
					||
					(msgDTO.getSubject()!=null && msgDTO.getSubject().startsWith(Constants.anomaliaMessaggio) && msgDTO.getIsSPAMorCCN().equals(0))
			){
				java.sql.Timestamp deliveredDate = new java.sql.Timestamp(new java.util.Date().getTime());
				pstmt.setTimestamp(52, deliveredDate);
				// Direttore AOO
				pstmt.setString(48,direttoreSede);
				// Direttore AOO Mitt
				pstmt.setNull(49,java.sql.Types.VARCHAR);
				// SubStato
				pstmt.setString(50,Constants.SUBSTATO_INVIO_NOTIFICA);
			}else if(msgDTO.getRicevuta()!=null && (!msgDTO.getRicevuta().equals(Constants.nonAccettazione)|| msgDTO.getIsSPAMorCCN().equals(1))){
				java.sql.Timestamp deliveredDate = new java.sql.Timestamp(new java.util.Date().getTime());
				pstmt.setTimestamp(52, deliveredDate);
				// Direttore AOO
				pstmt.setString(48,direttoreSede);
				// Direttore AOO Mitt
				//					pstmt.setString(49,Constants.SUBSTATO_INVIO_NOTIFICA);
				// SubStato
				pstmt.setString(50,"A");
			}

			pstmt.setString( 53 , msgDTO.getSegnatura() );

			if ( msgDTO.getPostaCertEMLBody() != null )
				pstmt.setString( 54, msgDTO.getPostaCertEMLBody().replaceAll("it.eustema.inps.agentPecPei.dto.SegnaturaDTO@", "") );
			else
				pstmt.setNull(54, java.sql.Types.VARCHAR);

			if ( msgDTO.getPostaCertEMLSubject() != null )
				pstmt.setString( 55, msgDTO.getPostaCertEMLSubject().replaceAll("it.eustema.inps.agentPecPei.dto.SegnaturaDTO@", "") );
			else
				pstmt.setNull(55, java.sql.Types.VARCHAR);

			if ( msgDTO.getSignIssuerName() != null )
				pstmt.setString( 56, msgDTO.getSignIssuerName() );
			else
				pstmt.setNull(56, java.sql.Types.VARCHAR);

			if ( msgDTO.getSignSubjectName() != null )
				pstmt.setString( 57, msgDTO.getSignSubjectName() );
			else
				pstmt.setNull(57, java.sql.Types.VARCHAR);

			if ( msgDTO.getIsSigned() != null )
				pstmt.setString( 58, msgDTO.getIsSigned());
			else
				pstmt.setNull(58, java.sql.Types.VARCHAR);

			if ( msgDTO.getIsSignedVer() != null )
				pstmt.setString( 59, msgDTO.getIsSignedVer());
			else
				pstmt.setNull(59, java.sql.Types.VARCHAR);

			pstmt.setInt( 60, msgDTO.getIdCartella());

			if ( msgDTO.getStatoLettura() != null )
				pstmt.setString( 61, msgDTO.getStatoLettura());
			else
				pstmt.setNull(61, java.sql.Types.VARCHAR);

			if(msgDTO.getSignValidFromDate()!=null){
				pstmt.setTimestamp(62, new java.sql.Timestamp(msgDTO.getSignValidFromDate().getTime()));
			}else{
				pstmt.setNull(62, java.sql.Types.TIMESTAMP);
			}

			if(msgDTO.getSignValidToDate()!=null){
				pstmt.setTimestamp(63, new java.sql.Timestamp(msgDTO.getSignValidToDate().getTime()));
			}else{
				pstmt.setNull(63, java.sql.Types.TIMESTAMP);
			}

			if ( msgDTO.getHeaders() != null )
				pstmt.setString( 64, msgDTO.getHeaders().toString());
			else
				pstmt.setNull(64, java.sql.Types.VARCHAR);

			if ( msgDTO.getPemlHeaders() != null )
				pstmt.setString( 65, msgDTO.getPemlHeaders().toString());
			else
				pstmt.setNull(65, java.sql.Types.VARCHAR);

			//				if ( msgDTO.getRawContent() != null )
			//					pstmt.setString( 66, msgDTO.getRawContent());
			//				else
			pstmt.setNull(66, java.sql.Types.VARCHAR);

			if ( msgDTO.getDatiSignature() != null )
				pstmt.setString( 67, msgDTO.getDatiSignature());
			else
				pstmt.setNull(67, java.sql.Types.VARCHAR);

			if ( msgDTO.getPemlPostedDate() != null )
				pstmt.setTimestamp( 68, new Timestamp( msgDTO.getPemlPostedDate().getTime() ));
			else
				pstmt.setNull(68, java.sql.Types.VARCHAR);


			if ( msgDTO.getPemlFrom() != null )
				pstmt.setString( 69, msgDTO.getPemlFrom());
			else
				pstmt.setNull(69, java.sql.Types.VARCHAR);

			if ( msgDTO.getSegnaturaMitt() != null )
				pstmt.setString( 70, msgDTO.getSegnaturaMitt());
			else
				pstmt.setNull(70, java.sql.Types.VARCHAR);

			if ( msgDTO.getCanale() != null )
				pstmt.setString( 71, msgDTO.getCanale());
			else
				pstmt.setNull(71, java.sql.Types.VARCHAR);

			rowCount = pstmt.executeUpdate();

			pstmt.close();	

			/*if(!msgDTO.getTipologia().equalsIgnoreCase("messaggio-pei")){
					if(verificaDestinatari(msgDTO.getMessageId())<=0){
						doInsertDestinatari(connPecPei, msgDTO.getMessageId(), msgDTO.getElencoDestinatariPer(), msgDTO);
						doInsertDestinatari(connPecPei, msgDTO.getMessageId(), msgDTO.getElencoDestinatariCc(),msgDTO);
						if(!msgDTO.getTipoMessaggio().equalsIgnoreCase("ricezione-protocollazione-pei") && !Constants.AUTORE_COMPOSITORE_PST.equals( msgDTO.getAutoreCompositore() ) )
							doInsertAllegati(connPecPei, msgDTO.getMessageId(), msgDTO.getCodiceAOO(), msgDTO.getElencoFileAllegati() );
						}
				}*/

		}
		catch(Exception ex){
			if ( internal ){
				try {
					connPecPei.rollback();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			log.error("inserisciMsgEntranti Exception: " + ex.getMessage()+"sul messaggio  : "+msgDTO.getMessageId()+ "codice sede "+msgDTO.getCodiceAOO());
			//System.err.println("MessaggiDAO :: inserisciMsgEntranti Exception: " + ex.getMessage());
			throw new DAOException( ex.getMessage()+"sul messaggio  : "+msgDTO.getMessageId()+ "codice sede "+msgDTO.getCodiceAOO());
		} finally{
			if ( internal ){
				try {
					connPecPei.commit();	
					connPecPei.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		

			}
			try{
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

			}catch(Exception e){
				log.error("inserisci messaggio :"+e.getMessage());
			}

		}

		log.debug("inserisciMessaggio END");				
		return rowCount;

	} //inserisciMsgEntranti


	public List<MessaggioDTO> selezionaMessaggiInUscitaPEC( String codAOO ) throws DAOException{

		System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEC() :: START");
		log.debug("selezionaMessaggiInUscitaPEC :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEC :: Eseguo la query " + SELECT_MSG_PEC_USCENTI_QRY);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_USCENTI_QRY);
			pstmt.setString( 1, codAOO );
			//			pstmt.setString( 2, accountPec );
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: Creo il messaggio");
			while (rs.next()) {

				String msgID = rs.getString("MessageID");

				if(!msgID.equals(currentMsgID)){
					System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: Messaggio NUOVO, MessageID=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();

					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setCodiceAOO(rs.getString("CodiceAOO"));
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
					messaggioPeiDTO.setStato(rs.getString("stato"));
					messaggioPeiDTO.setCanale(rs.getString("canale"));

					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));

						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						//						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
					currentMsgID = msgID;
				}				
				else{
					System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: STESSO Messaggio (Altro destinatario), MessageID=" + msgID);
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));					
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: destinatario=" + destDTO.getAddr());
						System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);
						//						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);

						if(destDTO.getType().equals("per")){
							System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: Riempie la listaDestinatariPer ('per')");
							listaDestinatariPer.add(destDTO);
						}
						else{
							System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: Riempie la listaDestinatariCC");
							listaDestinatariCC.add(destDTO);
						}
					}
				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);

				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEC :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("selezionaMessaggiInUscitaPEC :: END, numero messaggi PEC in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita;
	}

	public int verificaAllegati( String messageId ) throws DAOException{

		log.debug("verificaMessaggio :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int msgID = 0;
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_ALLEGATI);
			pstmt.setString( 1, messageId );
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				msgID = rs.getInt("tot_messaggio");
			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("verificaMessaggio :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("verificaMessaggio :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
		return msgID;
	}
	public int verificaDestinatari( String messageId ) throws DAOException{

		log.debug("verificaMessaggio :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int msgID = 0;
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_DEST);
			pstmt.setString( 1, messageId );
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				msgID = rs.getInt("tot_messaggio");
			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("verificaMessaggio :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("verificaMessaggio :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
		return msgID;
	}
	public int verificaMessaggio( String messageId ) throws DAOException{

		log.debug("verificaMessaggio :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int msgID = 0;
		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG);
			pstmt.setString( 1, messageId );
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				msgID = rs.getInt("tot_messaggio");
			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("verificaMessaggio :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("verificaMessaggio :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
		return msgID;
	}
	public MessaggioDTO verificaMessaggioSensOrRiserv( String messageId) throws DAOException{

		log.debug("verificaMessaggioSensOrRiserv :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int msgID = 0;
		MessaggioDTO messaggioPeiDTO = null;

		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_Sens_Riser);
			pstmt.setString( 1, messageId );
			//		pstmt.setString( 2, acoountPEC );
			//		pstmt.setString( 3, codiceAOO );
			rs  = pstmt.executeQuery();			
			while (rs.next()) {
				messaggioPeiDTO = new MessaggioDTO();
				messaggioPeiDTO.setSensibile(rs.getString("Sensibile"));
				messaggioPeiDTO.setRiservato(rs.getString("Riservato"));
			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("verificaMessaggioSensOrRiserv :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("verificaMessaggioSensOrRiserv :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
		return messaggioPeiDTO;
	}
	public List<MessaggioDTO> selezionaMessaggiInUscitaPECDaRiprotocollare( String codAOO ) throws DAOException{

		log.debug("selezionaMessaggiInUscitaPEC :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//		connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_ENTRANTI_DA_RIPROTOCCOLARE_QRY);
			pstmt.setString( 1, codAOO );
			//		pstmt.setString( 2, accountPec );
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {

				String msgID = rs.getString("MessageID");
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();

					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setCodiceAOO(rs.getString("CodiceAOO"));
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setIdPrenotazione(rs.getString("idPrenotazione")!=null?rs.getString("idPrenotazione"):"");

					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));

						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));					
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);

						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);

				messaggioPeiDTO.setCanale(rs.getString("canale"));

				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEC :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//		throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("selezionaMessaggiInUscitaPEC :: END, numero messaggi PEC in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita;
	}

	public MessaggioDTO selezionaDettaglioMessaggioInUscitaPEC( String messageID ) throws DAOException{

		log.debug("selezionaMessaggiInUscitaPEI :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();

		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//		connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_USCENTI_QRY2);
			pstmt.setString( 1, messageID );
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {

				String msgID = rs.getString("MessageID");
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();

					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setPostedDate( rs.getTimestamp("PostedDate") );
					messaggioPeiDTO.setCodiceAOO( rs.getString("CodiceAOO") );
					messaggioPeiDTO.setUiDoc( rs.getString("UiDoc") );
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setNomeSede( rs.getString("nomeSede") );
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setStato( rs.getString( "Stato" ) );
					messaggioPeiDTO.setCanale(rs.getString("canale"));
					messaggioPeiDTO.setRiservato( rs.getString("Riservato") );
					messaggioPeiDTO.setSensibile( rs.getString("Sensibile") );
					messaggioPeiDTO.setSize(rs.getInt("size"));
					messaggioPeiDTO.setIpAddress(rs.getString("IPAddress"));
					messaggioPeiDTO.setNomeRegione(rs.getString("nomeRegione"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setDesClassifica(rs.getString("desClassifica"));
					messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
					messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));					
					messaggioPeiDTO.setAutoreCompositore(rs.getString("AutoreCompositore"));
					messaggioPeiDTO.setAutoreUltimaModifica(rs.getString("AutoreUltimaModifica"));
					messaggioPeiDTO.setAutoreMittente(rs.getString("AutoreMittente"));
					messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
					messaggioPeiDTO.setAddrConfermaRicezione(rs.getString("AddrConfermaRicezione"));
					messaggioPeiDTO.setDominoPec(rs.getString("dominioPEC"));
					messaggioPeiDTO.setDocumentoPrincipale(rs.getString("documentoPrincipale"));
					messaggioPeiDTO.setMessaggio(rs.getBytes("messaggio"));
					messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
					messaggioPeiDTO.setXTrasporto(rs.getString("X_trasporto"));
					messaggioPeiDTO.setMsgId(rs.getString("MsgID"));
					messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
					messaggioPeiDTO.setReplyTo(rs.getString("ReplyTo"));
					messaggioPeiDTO.setIsReply(rs.getString("isReply"));
					messaggioPeiDTO.setInReplyTo(rs.getString("inReplyTo"));
					messaggioPeiDTO.setInReplyToSegnatura(rs.getString("inReplyToSegnatura"));
					messaggioPeiDTO.setInReplyToSubject(rs.getString("inReplyToSubject"));
					messaggioPeiDTO.setSegnaturaRif(rs.getString("SegnaturaRif"));
					messaggioPeiDTO.setSegnaturaRoot(rs.getString("SegnaturaRoot"));
					messaggioPeiDTO.setSegnatura(rs.getString("Segnatura"));
					messaggioPeiDTO.setCodiceAMM(rs.getString("CodiceAMM"));
					messaggioPeiDTO.setCodiceAOOMitt(rs.getString("CodiceAOOMitt"));
					messaggioPeiDTO.setCodiceAMMMitt(rs.getString("CodiceAMMMitt"));
					messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
					messaggioPeiDTO.setBodyHtml(rs.getString("BodyHtml"));
					messaggioPeiDTO.setIsSPAMorCCN(rs.getInt("isSPAMorCCN"));
					messaggioPeiDTO.setTipoMitt(rs.getString("tipoMitt"));
					messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
					messaggioPeiDTO.setDirettoreAOO(rs.getString("DirettoreAOO"));
					messaggioPeiDTO.setDirettoreAOOMitt(rs.getString("DirettoreAOOMitt"));
					messaggioPeiDTO.setSubstato(rs.getString("substato"));
					messaggioPeiDTO.setRicevuta(rs.getString("X_Ricevuta"));

					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));
						destDTO.setName(rs.getString("name"));
						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}

					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));	
						destDTO.setName(rs.getString("name"));
						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);

						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}

				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);

				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita.get(0);
	}
	public MessaggioDTO selezionaDettaglioMessaggioInUscitaPECDaProtocollare( String messageID ) throws DAOException{

		log.debug("selezionaMessaggiInUscitaPEI :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();

		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//		connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_ENTRANTI_QRY_DA_PROTOCOLLARE);
			pstmt.setString( 1, messageID );
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {

				String msgID = rs.getString("MessageID");
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();

					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setCodiceAOO( rs.getString("CodiceAOO") );
					messaggioPeiDTO.setUiDoc( rs.getString("UiDoc") );
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setNomeSede( rs.getString("nomeSede") );
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setStato( rs.getString( "Stato" ) );
					messaggioPeiDTO.setRiservato( rs.getString("Riservato") );
					messaggioPeiDTO.setSensibile( rs.getString("Sensibile") );
					messaggioPeiDTO.setSize(rs.getInt("size"));
					messaggioPeiDTO.setIpAddress(rs.getString("IPAddress"));
					messaggioPeiDTO.setNomeRegione(rs.getString("nomeRegione"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setDesClassifica(rs.getString("desClassifica"));
					messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
					messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));					
					messaggioPeiDTO.setAutoreCompositore(rs.getString("AutoreCompositore"));
					messaggioPeiDTO.setAutoreUltimaModifica(rs.getString("AutoreUltimaModifica"));
					messaggioPeiDTO.setAutoreMittente(rs.getString("AutoreMittente"));
					messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
					messaggioPeiDTO.setAddrConfermaRicezione(rs.getString("AddrConfermaRicezione"));
					messaggioPeiDTO.setDominoPec(rs.getString("dominioPEC"));
					messaggioPeiDTO.setDocumentoPrincipale(rs.getString("documentoPrincipale"));
					messaggioPeiDTO.setMessaggio(rs.getBytes("messaggio"));
					messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
					messaggioPeiDTO.setXTrasporto(rs.getString("X_trasporto"));
					messaggioPeiDTO.setMsgId(rs.getString("MsgID"));
					messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
					messaggioPeiDTO.setReplyTo(rs.getString("ReplyTo"));
					messaggioPeiDTO.setIsReply(rs.getString("isReply"));
					messaggioPeiDTO.setInReplyTo(rs.getString("inReplyTo"));
					messaggioPeiDTO.setInReplyToSegnatura(rs.getString("inReplyToSegnatura"));
					messaggioPeiDTO.setInReplyToSubject(rs.getString("inReplyToSubject"));
					messaggioPeiDTO.setSegnaturaRif(rs.getString("SegnaturaRif"));
					messaggioPeiDTO.setSegnaturaRoot(rs.getString("SegnaturaRoot"));
					messaggioPeiDTO.setCodiceAMM(rs.getString("CodiceAMM"));
					messaggioPeiDTO.setCodiceAOOMitt(rs.getString("CodiceAOOMitt"));
					messaggioPeiDTO.setCodiceAMMMitt(rs.getString("CodiceAMMMitt"));
					messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
					messaggioPeiDTO.setBodyHtml(rs.getString("BodyHtml"));
					messaggioPeiDTO.setIsSPAMorCCN(rs.getInt("isSPAMorCCN"));
					messaggioPeiDTO.setTipoMitt(rs.getString("tipoMitt"));
					messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
					messaggioPeiDTO.setDirettoreAOO(rs.getString("DirettoreAOO"));
					messaggioPeiDTO.setDirettoreAOOMitt(rs.getString("DirettoreAOOMitt"));
					messaggioPeiDTO.setSubstato(rs.getString("substato"));
					messaggioPeiDTO.setRicevuta(rs.getString("X_Ricevuta"));
					messaggioPeiDTO.setIdPrenotazione(rs.getString("idPrenotazione")!=null?rs.getString("idPrenotazione"):"");

					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));
						destDTO.setName(rs.getString("name"));
						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}

					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));	
						destDTO.setName(rs.getString("name"));
						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);

						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}

				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);

				pstmt =  connPecPei.prepareStatement("select rawContent from MessaggiBlob with(nolock) where messageId = ?");
				pstmt.setString(1, messaggioPeiDTO.getMessageId());
				rs = pstmt.executeQuery(); 
				if(rs.next()){
					messaggioPeiDTO.setRawContent(rs.getString("rawContent"));
				}

				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita.get(0);
	}
	public List<String> selectAllAOO(int idBatch) throws DAOException{

		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<String> out = new ArrayList<String>();

		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_COD_AOO_FROM_MESSAGGI);
			pstmt.setInt(1, idBatch);

			rs  = pstmt.executeQuery();			
			while (rs.next()) {

				out.add( rs.getString(1) );

			}

		}
		catch( Exception eh )
		{
			throw new DAOException("Errore recupero dati da MESSAGGI");
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}


		return out;

	}

	public AllegatoDTO creaMessageTxt(MessaggioDTO messaggio){

		AllegatoDTO allegato = new AllegatoDTO();
		try{
			//			File temp = File.createTempFile("TestoDelMessaggio", ".txt");
			//			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			StringBuilder str = new StringBuilder();
			if ("messaggio-pei".equalsIgnoreCase( messaggio.getTipologia() )){
				if(messaggio.getVerso()!=null && messaggio.getVerso().equals("U"))
					str.append("\r\n\nmessaggio pei in uscita");
			}else if(messaggio.getVerso()!=null && messaggio.getVerso().equals("U"))
				str.append("\r\n\nposta certificata in uscita");
			else 
				str.append("\r\n\nposta certificata in entrata");

			str.append("\r\n\nSede "+messaggio.getCodiceAOO());
			str.append("\r\n\nCompositore: "+messaggio.getAutoreCompositore());
			str.append("\r\n\nMittente: "+ messaggio.getAutoreMittente());
			str.append("\r\n\nSegnatura :" + messaggio.getSegnatura() );
			str.append("\r\n\nSegnatura padre:");
			str.append("\r\n\nSegnatura origine:");
			str.append("\r\n\nSegnatura mittente:");
			str.append("\r\n\nTitolario: "+messaggio.getClassifica()+" "+messaggio.getDesClassifica());
			str.append("\r\n\nRiservato: "+messaggio.getRiservato());
			str.append("\r\n\nSensibile: "+messaggio.getSensibile());
			str.append("\r\n\nTipo Ricevuta: "+messaggio.getTipoRicevuta());
			str.append("\r\n\nConferma ricezione: "+messaggio.getConfermaRicezione()+" "+messaggio.getAddrConfermaRicezione());
			str.append("\r\n\n-------------------------");
			str.append("\r\n\nRisposta: "+messaggio.getIsReply());
			str.append("\r\n\nMessage-ID: "+messaggio.getMessageId());
			str.append("\r\n\nDa: "+messaggio.getFromAddr());
			str.append("\r\n\nPosted date: "+messaggio.getPostedDate());
			str.append("\r\n\nPer ");
			for(DestinatarioDTO a : messaggio.getElencoDestinatariPer()){
				str.append("\r\n\n"+a.getAddr());
			}
			str.append("\r\n\nCc ");
			for(DestinatarioDTO a : messaggio.getElencoDestinatariCc()){
				str.append("\r\n\n"+a.getAddr());
			}
			str.append("\r\n\nAllegati ");
			if(messaggio.getElencoFileAllegati().size()>0){
				for(AllegatoDTO a : messaggio.getElencoFileAllegati()){
					if(!a.getFileName().equalsIgnoreCase("segnatura.xml")&&!a.getFileName().equalsIgnoreCase("testodelmessaggio.txt")){				
						str.append("\r\n\n"+a.getFileName().replaceAll("&", "e"));
					}
				}
			}else{
				str.append("\r\n\nnon presenti");
			}
			str.append("\r\n\nOggetto: "+messaggio.getSubject());
			str.append("\r\n\n-------------------------");
			str.append("\r\n\n"+messaggio.getBody());
			//			bw.write(str.toString());
			//			bw.close();

			allegato.setContentType("txt");
			allegato.setFileData(str.toString().getBytes());
			allegato.setFileName("TestoDelMessaggio.txt");
			allegato.setFileSize(str.length());
			allegato.setMessageID(messaggio.getMessageId());
			if(messaggio.getPostedDate()!=null){
				allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
				SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
				String meseAnno = sfd.format(messaggio.getPostedDate());
				allegato.setMeseAnno(meseAnno);
				//				allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
			}
			allegato.setTestoAllegato(str.toString());
			allegato.setTipoAllegato("TXT");

			//    	    temp.delete();

		}catch(Exception e){
			log.error("Errore nella creazione del TestoDelMessaggio.txt per il messaggio "+messaggio.getMessageId(),e);
		}
		return allegato;
	}
	public AllegatoDTO creaMessagePecCompleto(MessaggioDTO messaggio){

		AllegatoDTO allegato = new AllegatoDTO();
		try{
			//			File temp = File.createTempFile("postacert", ".eml");

			//			File temp = new File("C:\\testPdf\\messaggiBonificati\\postacert.eml");
			//			FileInputStream fo = new FileInputStream(temp);
			//			IOUtils.toByteArray(fo)
			if(messaggio.getRawContent()==null){
				for(AllegatoDTO a:messaggio.getElencoFileAllegati()){
					if(a.getFileName()!=null&&a.getFileName().equalsIgnoreCase("postacert.eml"))
						messaggio.setRawContent(new String(a.getFileData()));
				}
			}

			allegato.setContentType("eml");
			allegato.setFileData(messaggio.getRawContent().getBytes());
			allegato.setFileName("postacert.eml");
			allegato.setFileSize(messaggio.getRawContent().getBytes().length);
			allegato.setMessageID(messaggio.getMessageId());
			if(messaggio.getPostedDate()!=null){
				allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
				SimpleDateFormat sfd = new SimpleDateFormat("MM/yyyy");
				String meseAnno = sfd.format(messaggio.getPostedDate());
				allegato.setMeseAnno(meseAnno);
				//				allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
			}
			allegato.setTestoAllegato(null);
			allegato.setTipoAllegato("EML");

			//			File temp = new File("C:\\testPdf\\messaggiBonificati\\postacert.eml");
			//			FileOutputStream fo = new FileOutputStream(temp);
			//			fo.write(allegato.getFileData());
			//			fo.flush();
			//			fo.close();

		}catch(Exception e){
			log.error("Errore nella creazione del TestoDelMessaggio.txt per il messaggio "+messaggio.getMessageId(),e);
		}
		return allegato;
	}
	public static AllegatoDTO creaAllegatoInterop(String nomeFile, String contenuto, String messageId){

		AllegatoDTO allegato = new AllegatoDTO();
		try{
			//			File temp = File.createTempFile(nomeFile, ".xml");
			//			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			StringBuilder str = new StringBuilder(contenuto);
			allegato.setContentType("text/xml");
			allegato.setFileData(str.toString().getBytes());
			allegato.setFileName(nomeFile+".xml");
			allegato.setFileSize(str.length());
			allegato.setTestoAllegato(str.toString());
			allegato.setTipoAllegato("xml");


		}catch(Exception e){
			log.error("Errore nella creazione del RisultatoXml per interoperabilit� per il messaggio "+messageId);
		}
		return allegato;
	}

	public static AllegatoDTO creaAllegatoInterop2013(String nomeFile, String contenuto, String messageId){

		AllegatoDTO allegato = new AllegatoDTO();
		try{
			//			File temp = File.createTempFile(nomeFile, ".xml");
			//			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			StringBuilder str = new StringBuilder(contenuto);
			allegato.setContentType("text/xml");
			allegato.setFileData(str.toString().getBytes());
			allegato.setFileName(nomeFile+".xml");
			allegato.setFileSize(str.length());
			allegato.setTestoAllegato(str.toString());
			allegato.setTipoAllegato("xml");


		}catch(Exception e){
			log.error("Errore nella creazione del RisultatoXml per interoperabilit� per il messaggio "+messageId);
		}
		return allegato;
	}
	public byte[] estraiASCIItoHTML(byte ascii) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException, Exception{

		String query = "select [Html Entity] FROM ConversionTable where [ASCII] = ?";
		Connection conn = cm.getConnection(Constants.DB_PECPEI);

		//		per chiamate locali
		//		String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		//	    Class.forName(driver).newInstance();
		//	    String urlDB = "jdbc:sqlserver://SQLINPS04.inps\\SQLINPS04;databaseName=PECPEI";
		//	    Connection conn=DriverManager.getConnection(urlDB, "PECPEI","aB7-9Hk$pg");
		//

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String valoreHtml=null;
		try
		{
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, ascii+256);
			rs  = pstmt.executeQuery();
			boolean trovato = false;
			while (rs.next()) {
				valoreHtml = rs.getString("Html Entity");
				trovato = true;
			}
			if(!trovato){
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, "99999999");
				rs  = pstmt.executeQuery();
				while (rs.next()) {
					valoreHtml = rs.getString("Html Entity");
					trovato = true;
				}
			}
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
					conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return valoreHtml.getBytes();

	}
	public byte[] estraiASCIItoCarattere(byte ascii) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException, Exception{

		String query = "select [Carattere] FROM ConversionTable where [ASCII] = ?";
		Connection conn = cm.getConnection(Constants.DB_PECPEI);

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String valoreHtml=null;
		try
		{
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, ascii+256);
			rs  = pstmt.executeQuery();
			boolean trovato = false;
			while (rs.next()) {
				valoreHtml = rs.getString("Carattere");
				trovato = true;
			}
			if(!trovato){
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1, "99999999");
				rs  = pstmt.executeQuery();
				while (rs.next()) {
					valoreHtml = rs.getString("Carattere");
					trovato = true;
				}
			}
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
		}finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
					conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return valoreHtml.getBytes();

	}

	public String leggiSegnatura( String messageId) throws DAOException{

		log.debug("leggiSegnatura :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int msgID = 0;
		String out = null;

		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_SEGNATURA);
			pstmt.setString( 1, messageId );
			//			pstmt.setString( 2, acoountPEC );
			//			pstmt.setString( 3, codiceAOO );
			rs  = pstmt.executeQuery();			
			if (rs.next()) {

				out = rs.getString("Segnatura");

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("leggiSegnatura :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("leggiSegnatura :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
		return out;
	}

	public String leggiSegnaturaPEI( String messageId, String accountPEC, String codiceAOO) throws DAOException{

		log.debug("leggiSegnatura :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int msgID = 0;
		String out = null;

		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_SEGNATURA_PEI);
			pstmt.setString( 1, messageId );
			pstmt.setString( 2, accountPEC );
			pstmt.setString( 3, codiceAOO );
			rs  = pstmt.executeQuery();			
			if (rs.next()) {

				out = rs.getString("Segnatura");

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("leggiSegnatura :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(pstmt!=null)
					pstmt.close();
				if(rs!=null)
					rs.close();

			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("leggiSegnatura :: END, numero messaggi messaggi con messageId = "+messageId+" : " + msgID);		
		return out;
	}

	public MessaggioDTO selezionaDettaglioMessaggioInUscitaPECDaProtocollareTest( String messageID ) throws DAOException{

		log.debug("selezionaMessaggiInUscitaPEI :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();

		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_ENTRANTI_QRY_DA_PROTOCOLLARE_TEST);
			pstmt.setString( 1, messageID );
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {

				String msgID = rs.getString("MessageID");
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();

					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setCodiceAOO( rs.getString("CodiceAOO") );
					messaggioPeiDTO.setUiDoc( rs.getString("UiDoc") );
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setNomeSede( rs.getString("nomeSede") );
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setStato( rs.getString( "Stato" ) );
					messaggioPeiDTO.setRiservato( rs.getString("Riservato") );
					messaggioPeiDTO.setSensibile( rs.getString("Sensibile") );
					messaggioPeiDTO.setSize(rs.getInt("size"));
					messaggioPeiDTO.setIpAddress(rs.getString("IPAddress"));
					messaggioPeiDTO.setNomeRegione(rs.getString("nomeRegione"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));
					messaggioPeiDTO.setDesClassifica(rs.getString("desClassifica"));
					messaggioPeiDTO.setNomeUfficio(rs.getString("nomeUfficio"));
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setFromAddr(rs.getString("FromAddr"));
					messaggioPeiDTO.setFromPhrase(rs.getString("FromPhrase"));					
					messaggioPeiDTO.setAutoreCompositore(rs.getString("AutoreCompositore"));
					messaggioPeiDTO.setAutoreUltimaModifica(rs.getString("AutoreUltimaModifica"));
					messaggioPeiDTO.setAutoreMittente(rs.getString("AutoreMittente"));
					messaggioPeiDTO.setConfermaRicezione(rs.getString("confermaRicezione"));
					messaggioPeiDTO.setAddrConfermaRicezione(rs.getString("AddrConfermaRicezione"));
					messaggioPeiDTO.setDominoPec(rs.getString("dominioPEC"));
					messaggioPeiDTO.setDocumentoPrincipale(rs.getString("documentoPrincipale"));
					messaggioPeiDTO.setMessaggio(rs.getBytes("messaggio"));
					messaggioPeiDTO.setTipologia(rs.getString("tipologia"));
					messaggioPeiDTO.setXTrasporto(rs.getString("X_trasporto"));
					messaggioPeiDTO.setMsgId(rs.getString("MsgID"));
					messaggioPeiDTO.setTipoMessaggio(rs.getString("tipoMessaggio"));
					messaggioPeiDTO.setReplyTo(rs.getString("ReplyTo"));
					messaggioPeiDTO.setIsReply(rs.getString("isReply"));
					messaggioPeiDTO.setInReplyTo(rs.getString("inReplyTo"));
					messaggioPeiDTO.setInReplyToSegnatura(rs.getString("inReplyToSegnatura"));
					messaggioPeiDTO.setInReplyToSubject(rs.getString("inReplyToSubject"));
					messaggioPeiDTO.setSegnaturaRif(rs.getString("SegnaturaRif"));
					messaggioPeiDTO.setSegnaturaRoot(rs.getString("SegnaturaRoot"));
					messaggioPeiDTO.setCodiceAMM(rs.getString("CodiceAMM"));
					messaggioPeiDTO.setCodiceAOOMitt(rs.getString("CodiceAOOMitt"));
					messaggioPeiDTO.setCodiceAMMMitt(rs.getString("CodiceAMMMitt"));
					messaggioPeiDTO.setTipoRicevuta(rs.getString("X_TipoRicevuta"));
					messaggioPeiDTO.setBodyHtml(rs.getString("BodyHtml"));
					messaggioPeiDTO.setIsSPAMorCCN(rs.getInt("isSPAMorCCN"));
					messaggioPeiDTO.setTipoMitt(rs.getString("tipoMitt"));
					messaggioPeiDTO.setDesMitt(rs.getString("desMitt"));
					messaggioPeiDTO.setDirettoreAOO(rs.getString("DirettoreAOO"));
					messaggioPeiDTO.setDirettoreAOOMitt(rs.getString("DirettoreAOOMitt"));
					messaggioPeiDTO.setSubstato(rs.getString("substato"));
					messaggioPeiDTO.setRicevuta(rs.getString("X_Ricevuta"));

					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));
						destDTO.setName(rs.getString("name"));
						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}

					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));	
						destDTO.setName(rs.getString("name"));
						destDTO.setPhrase(rs.getString("phrase"));
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);

						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}

				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);

				pstmt =  connPecPei.prepareStatement("select rawContent from MessaggiBlob with(nolock) where messageId = ?");
				pstmt.setString(1, messaggioPeiDTO.getMessageId());
				rs = pstmt.executeQuery(); 
				if(rs.next()){
					messaggioPeiDTO.setRawContent(rs.getString("rawContent"));
				}

				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("selezionaMessaggiInUscitaPEI :: END, numero messaggi PEI in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita.get(0);
	}

	public List<String> selectAllAOOTest() throws DAOException{

		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<String> out = new ArrayList<String>();

		try
		{
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_COD_AOO_FROM_MESSAGGI_TEST);
			pstmt.setString(1, ConfigProp.aooDiTest);

			rs  = pstmt.executeQuery();			
			while (rs.next()) {

				out.add( rs.getString(1) );

			}

		}
		catch( Exception eh )
		{
			throw new DAOException("Errore recupero dati da MESSAGGI");
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}


		return out;

	}

	public List<MessaggioDTO> selezionaMessaggiInUscitaPECDaRiprotocollareTest( String codAOO ) throws DAOException{

		log.debug("selezionaMessaggiInUscitaPEC :: START");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MessaggioDTO messaggioPeiDTO = null;
		DestinatarioDTO destDTO = null;
		List<DestinatarioDTO> listaDestinatariPer = null;
		List<DestinatarioDTO> listaDestinatariCC = null;
		List<MessaggioDTO> listaMessaggiInUscita = new ArrayList<MessaggioDTO>();
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//		connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//connPecPei = cm.getConnection_PecPei(ConfigProp.usrPecPei, ConfigProp.pwdPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_MSG_PEC_ENTRANTI_DA_RIPROTOCCOLARE_QRY_TEST);
			pstmt.setString( 1, codAOO );
			//		pstmt.setString( 2, accountPec );
			rs  = pstmt.executeQuery();			
			String currentMsgID = null;
			while (rs.next()) {

				String msgID = rs.getString("MessageID");
				if(!msgID.equals(currentMsgID)){
					//System.out.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: nuovo messaggio id=" + msgID);
					// reset
					messaggioPeiDTO = new MessaggioDTO();
					destDTO = new DestinatarioDTO();
					listaDestinatariPer = new ArrayList<DestinatarioDTO>();
					listaDestinatariCC = new ArrayList<DestinatarioDTO>();

					messaggioPeiDTO.setMessageId(msgID);
					messaggioPeiDTO.setAccountPec(rs.getString("accountPEC"));
					messaggioPeiDTO.setCodiceAOO(rs.getString("CodiceAOO"));
					messaggioPeiDTO.setSubject(rs.getString("subject"));
					messaggioPeiDTO.setBody(rs.getString("body"));
					messaggioPeiDTO.setVerso(rs.getString("verso"));
					messaggioPeiDTO.setClassifica(rs.getString("classifica"));

					String addr = rs.getString("Addr");
					if(addr != null){	
						destDTO.setAddr(addr);
						destDTO.setType(rs.getString("Type_per_cc"));

						//int slashIdx = addr.indexOf("/");
						String codiceAOO_dest = addr.substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);
						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
					currentMsgID = msgID;
				}				
				else{
					// caso di stesso msg, altro destinatario
					if(rs.getString("Addr") != null){	
						destDTO = new DestinatarioDTO();
						destDTO.setAddr(rs.getString("Addr"));
						destDTO.setType(rs.getString("Type_per_cc"));					
						//int slashIdx =  destDTO.getAddr().indexOf("/");
						String codiceAOO_dest = destDTO.getAddr().substring(0, 4);
						log.debug("selezionaMessaggiInUscitaPEI :: codiceAOO_dest=" + codiceAOO_dest);						
						destDTO.setCodiceAOO(codiceAOO_dest);

						if(destDTO.getType().equals("per"))
							listaDestinatariPer.add(destDTO);
						else
							listaDestinatariCC.add(destDTO);
					}
				}
				messaggioPeiDTO.setElencoDestinatariCc(listaDestinatariCC);
				messaggioPeiDTO.setElencoDestinatariPer(listaDestinatariPer);

				if(!listaMessaggiInUscita.contains(messaggioPeiDTO))
					listaMessaggiInUscita.add(messaggioPeiDTO);

			} //while	
		}
		catch( Exception eh )
		{
			eh.printStackTrace();
			log.error("selezionaMessaggiInUscitaPEC :: exception=" + eh.getMessage());
			//System.err.println("MessaggiDAO :: selezionaMessaggiInUscitaPEI :: exception=" + eh.getMessage());
			//		throw new DAOException(eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("selezionaMessaggiInUscitaPEC :: END, numero messaggi PEC in uscita=" + listaMessaggiInUscita.size());		
		return listaMessaggiInUscita;
	}

	public int aggiornaMsgRicevutoDaRiprotocollareTest(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{
		boolean internal = false;
		if ( connPecPei == null ){
			internal = true;
			try{
				connPecPei = cm.getConnection(Constants.DB_PECPEI);
				log.debug("aggiornaMsgRicevutoDaRiprotocollare :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}

		log.debug("aggiornaMsgRicevutoDaRiprotocollare START");

		int ret = 0;
		PreparedStatement pstmt = null;

		try	{
			if(msgUscente.getDocumentoPrincipale()!=null && !msgUscente.getDocumentoPrincipale().equals(""))
			{
				//			"update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?, documentoPrincipale=?" +
				//			" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";
				pstmt = connPecPei.prepareStatement(UPDATE_MSG_RICEVUTO_DOCPRIN_QRY_TEST);
				// segnatura
				if(msgUscente.getSegnatura()!=null)
					pstmt.setString(1, msgUscente.getSegnatura());
				else
					pstmt.setNull(1, java.sql.Types.VARCHAR);
				pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setString(4, msgUscente.getSubstato());
				pstmt.setString(5, msgUscente.getDocumentoPrincipale());
				pstmt.setString(6, msgUscente.getMessageId());
				pstmt.setString(7, msgUscente.getCodiceAOO());
				pstmt.setString(8, msgUscente.getAccountPec());
			}else{			
				//			"update messaggi SET Stato='RI', Segnatura=?, DeliveredDate=?, DataOraUltimaModifica=?, SubStato=?" +
				//			" WHERE MessageID=? AND CodiceAOO=? AND accountPEC=?";
				pstmt = connPecPei.prepareStatement(UPDATE_MSG_RICEVUTO_QRY_TEST);
				// segnatura
				if(msgUscente.getSegnatura()!=null)
					pstmt.setString(1, msgUscente.getSegnatura());
				else
					pstmt.setNull(1, java.sql.Types.VARCHAR);
				pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setString(4, msgUscente.getSubstato());
				pstmt.setString(5, msgUscente.getMessageId());
				pstmt.setString(6, msgUscente.getCodiceAOO());
				pstmt.setString(7, msgUscente.getAccountPec());
			}
			boolean riprova = true;
			int i = 0;
			int numTentativi = Integer.parseInt(configDao.getProperty("numero_massimo_tentativi_aggiornamentoProtocollo", "3"));
			int sleepTentativi = Integer.parseInt(configDao.getProperty("sleep_tentativi_aggiornamentoProtocollo", "5000"));

			while(riprova || i==numTentativi){
				try{
					ret = pstmt.executeUpdate();
					riprova = false;
				}catch(Exception e){
					i++;
					Thread.sleep(sleepTentativi);
					riprova = true;
					if(i==numTentativi+1){
						riprova = false;
						//					executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
						//					executor.executeCommand();
					}
				}
			}

			if ( internal )
				connPecPei.commit();
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgRicevutoDaRiprotocollare Exception: " + eh.getMessage());		
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if( internal && connPecPei!=null)
					connPecPei.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("aggiornaMsgRicevutoDaRiprotocollare END");		
		return ret;		
	}

	public int aggiornaMsgRicevutoDaRiprotocollare2016(Connection connPecPei, MessaggioDTO msgUscente) throws DAOException{

		if ( connPecPei == null ){
			try{
				connPecPei = cm.getConnection(Constants.DB_PECPEI);
				log.debug("aggiornaMsgRicevutoDaRiprotocollare :: usa la connection: " + connPecPei);
				connPecPei.setAutoCommit(false);
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
				throw new DAOException("Impossibile aggiornare l'archivio dei messaggi: " + e.getMessage());
			}
		}

		log.debug("aggiornaMsgRicevutoDaRiprotocollare START");

		int ret = 0;
		PreparedStatement pstmt = null;

		try	{
			if(msgUscente.getDocumentoPrincipale()!=null && !msgUscente.getDocumentoPrincipale().equals(""))
			{
				pstmt = connPecPei.prepareStatement(UPDATE_MSG_PRENOTATO_QRY);
				pstmt.setTimestamp(1, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setString(3, msgUscente.getSubstato());
				pstmt.setString(4, msgUscente.getDocumentoPrincipale());
				pstmt.setString(5, msgUscente.getIdPrenotazione());
				pstmt.setString(6, msgUscente.getMessageId());
				pstmt.setString(7, msgUscente.getCodiceAOO());
				pstmt.setString(8, msgUscente.getAccountPec());
			}else{			
				pstmt = connPecPei.prepareStatement(UPDATE_MSG_PRENOTATO_QRY_1);

				pstmt.setTimestamp(1, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setTimestamp(2, new Timestamp((new java.util.Date()).getTime()));
				pstmt.setString(3, msgUscente.getSubstato());
				pstmt.setString(4, msgUscente.getIdPrenotazione());
				pstmt.setString(5, msgUscente.getMessageId());
				pstmt.setString(6, msgUscente.getCodiceAOO());
				pstmt.setString(7, msgUscente.getAccountPec());
			}


			try{
				ret = pstmt.executeUpdate();
				connPecPei.commit();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		catch(Exception eh){
			eh.printStackTrace();
			log.error("aggiornaMsgRicevutoDaRiprotocollare Exception: " + eh.getMessage());		
			throw new DAOException( eh.getMessage() );
		} finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.debug("aggiornaMsgRicevutoDaRiprotocollare END");		
		return ret;		
	}

	public String getSegnaturaMessaggioPrincipale(String msgID) throws DAOException{

		ResultSet resultSet = null;
		Connection connectionPecPei = null;
		PreparedStatement preparedStatement = null;
		
		String segnaturaMessaggioPrincipale = null;

		try
		{
			connectionPecPei = cm.getConnection(Constants.DB_PECPEI);
			
			preparedStatement = connectionPecPei.prepareStatement(GET_SEGNATURA_MESSAGGIO_PRINCIPALE);
			preparedStatement.setString(1, msgID);

			resultSet = preparedStatement.executeQuery();		
			
			if(resultSet.next()) {
				segnaturaMessaggioPrincipale = resultSet.getString(1);
			}

		}
		catch( Exception eh )
		{
			throw new DAOException("Errore nel recupero della segnatura del messaggio principale");
		}
		finally{
			try {
				if(connectionPecPei!=null)
					connectionPecPei.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return segnaturaMessaggioPrincipale;

	}

}