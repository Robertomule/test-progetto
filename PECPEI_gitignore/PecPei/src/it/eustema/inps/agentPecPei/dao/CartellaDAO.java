package it.eustema.inps.agentPecPei.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.agentPecPei.dto.CartellaDTO;
import it.eustema.inps.agentPecPei.exception.CartellaNotEmptyException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;


public class CartellaDAO extends AgentBaseDAO{

	private final static String INS_CARTELLA_QRY = "INSERT INTO Cartelle (IdPadre, Valido, Descrizione, AccountPEC, fullpathfolder)" +											
	" VALUES(?,?,?,?,?)";
	
	/*
	private final static String UPD_CARTELLA_QRY = "UPDATE Cartelle" +
	" SET IdPadre=?, Valido=?, Descrizione=?, AccountPEC=?" +
	" WHERE IdCartella=?";
	*/
	
	private final static String UPD_RENAME_CARTELLA_QRY = "UPDATE Cartelle SET Descrizione=? WHERE IdCartella=?";	
	
	private final static String DEL_LOGICO_CARTELLA_QRY = "UPDATE Cartelle SET Valido =" + MyConstants.CARTELLA_ELIMINATA +  
		" WHERE IdCartella=?";
	
	/*
	private final static String SEL_CARTELLE_QRY = "SELECT * FROM Cartelle WHERE Valido=" + MyConstants.CARTELLA_VALIDA; 
	
	private final static String SEL_CARTELLE_BY_ACCOUNT_QRY = "SELECT * FROM Cartelle" +
		" WHERE Valido=" +MyConstants.CARTELLA_VALIDA+ " AND AccountPEC=?";

	private final static String SEL_CARTELLE_FIGLIE_BY_ACCOUNT_QRY = "SELECT * FROM Cartelle" +
		" WHERE Valido=" +MyConstants.CARTELLA_VALIDA + 
		" AND IdPadre=? AND AccountPEC=?";
	*/	
	
	private final static String SEL_CARTELLE_FIGLIE_QRY = "SELECT * FROM Cartelle" + 
		" WHERE Valido=" +MyConstants.CARTELLA_VALIDA+ " AND IdPadre=?";
	
	private final static String SEL_CARTELLE_ROOT_BY_ACCOUNT_PEC_QRY = "Select * FROM Cartelle" + 
		" WHERE Valido=" +MyConstants.CARTELLA_VALIDA +	" AND IdPadre IS NULL AND AccountPEC=?";	
	
	private final static String COUNT_CARTELLE_FIGLIE_QRY = "Select count(*) from Cartelle " + 
		" WHERE Valido=" +MyConstants.CARTELLA_VALIDA+ " AND IdPadre=?";	

	
	private static String SEL_ID_CARTELLA_QRY = "Select IdCartella FROM Cartelle" +
//			  " WHERE Valido=" +MyConstants.CARTELLA_VALIDA + 
//			  " AND FullPathFolder=? AND AccountPEC=? ";
			  " WHERE FullPathFolder=? AND AccountPEC=? ";
	
	private final static String CHECK_ID_CARTELLA_QRY = "Select count(*) FROM Cartelle" +
	  " WHERE Valido=" +MyConstants.CARTELLA_VALIDA + " AND IdCartella=?";	

	private final static String SELECT_ACCOUNT_PEC_QRY = "Select AccountPEC FROM Cartelle" +
	  " WHERE Valido=" +MyConstants.CARTELLA_VALIDA + " AND IdCartella=?";
	
	private static String CHECK_CARTELLA_UNICA_QRY = "SELECT distinct IdCartella FROM Cartelle " +
		" WHERE   AccountPEC=? AND fullpathfolder=? ";
	
	
	public void verificaTabelle(Connection connPecPei){

		String query = "SELECT TABLE_SCHEMA + '.' + TABLE_NAME AS Tabelle "+
							" FROM INFORMATION_SCHEMA.TABLES "+
								" WHERE TABLE_TYPE = 'BASE TABLE'";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = connPecPei.prepareStatement(query);
			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()){					
					System.out.println(rs.getString("Tabelle"));					
				}
			}
		}catch(Exception e ){
			e.printStackTrace();
		}
	}
	
	/**
	 * Restituisce l'identificativo della cartella corrispondente ai criteri passati in input,
	 * altrimenti restituisce il valore -1 se non trova alcuna cartella.
	 * @param cartDTO oggetto dto contenente i parametri di ricerca
	 * @return
	 * @throws DAOException
	 */
	public int getIdCartellaPadre(CartellaDTO cartDTO) throws DAOException  {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int idCartella = -1;
		Connection connPecPei = null;
		
		try {
			String query = SEL_ID_CARTELLA_QRY;
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
    		if(cartDTO.getIdPadre()!=null)    			
    			query += "AND IdPadre = ?";    		
			pstmt = connPecPei.prepareStatement(query);
			pstmt.setString(1, cartDTO.getFullPathFolder());
			pstmt.setString(2, cartDTO.getAccount());

			if(cartDTO.getIdPadre()!=null)
				pstmt.setInt(3, cartDTO.getIdPadre());
			rs = pstmt.executeQuery();
			
			if (rs != null) {
				if(rs.next()){					
					idCartella = rs.getInt("IdCartella");					
				} 
			}
		} 
	    catch (SQLException sqlException) {
	    	sqlException.printStackTrace();
	        throw new DAOException("Errore durante accesso al DB");
	    }				
		catch (Exception eh) {
			eh.printStackTrace();
	        throw new DAOException("Errore durante accesso al DB");
		}
		finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (connPecPei != null)
					connPecPei.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return idCartella;
	}
	
	
	/**
	 * Controlla l'esistena nel DB della cartella specificata in input; nel caso positivo restituisce il relativo accountPEC 
	 * @param idCartellaToCheck identificativo della cartella che si vuole controllare
	 * @return l'accountPEC della cartella specificata in input se esistente, altrimenti null
	 * @throws DAOException se si verifica un problema durante l'accesso al DB
	 */
	public String getAccountPECCartella(int idCartellaToCheck) throws DAOException{
		
		String accountPEC = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection connPecPei = null;
		try {
			
			connPecPei = cm.getConnection(Constants.DB_PECPEI);    		
			pstmt = connPecPei.prepareStatement(SELECT_ACCOUNT_PEC_QRY);						
			pstmt.setInt(1, idCartellaToCheck);
			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()){	
					accountPEC = rs.getString(1);					
				}
			}
		} 
	    catch (SQLException sqlException) {
	    	sqlException.printStackTrace();
	        throw new DAOException("Errore durante accesso al DB");
	    }				
		catch (Exception eh) {
			eh.printStackTrace();
	        throw new DAOException("Errore durante accesso al DB");
		}
		finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (connPecPei != null)
					connPecPei.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
				
		return accountPEC;
		
	}//getAccountPECCartella
	
	
	public int isCartellaPresente(String accountPEC, Integer idPadre, String descCartella) throws DAOException{
		
		int isPresente = 0;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		//System.out.println("CartellaDAO :: isCartellaPresente - idPadre:" + idPadre);
		Connection connPecPei = null;
		try {
			connPecPei = cm.getConnection(Constants.DB_PECPEI);    	
			if(idPadre != null && idPadre !=0)
				CHECK_CARTELLA_UNICA_QRY += " AND IdPadre=?";
			pstmt = connPecPei.prepareStatement(CHECK_CARTELLA_UNICA_QRY);
			pstmt.setString(1, accountPEC);
			pstmt.setString(2, descCartella );
			if(idPadre != null && idPadre !=0)
				pstmt.setInt(3, idPadre);			
			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()){					
//					if(rs.getInt(1) > 0)
						isPresente = rs.getInt(1);
				}
			}
		} 
	    catch (SQLException sqle) {
	    	sqle.printStackTrace();
	        throw new DAOException("Errore durante accesso al DB");
	    }				
		catch (Exception eh) {
			eh.printStackTrace();
	        throw new DAOException("Errore durante accesso al DB");
		}
		finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (connPecPei != null)
					connPecPei.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		
		return isPresente;
	}
	
	/**
	 * 
	 * @param idCartellaToCheck
	 * @return
	 * @throws DAOException
	 */
	public boolean esisteIdCartella(int idCartellaToCheck) throws DAOException{
		
		boolean esiste = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		//System.out.println("CartellaDAO :: esisteIdCartella :: idCartellaToCheck: " + idCartellaToCheck);
		Connection connPecPei = null;
		try {
			
			connPecPei = cm.getConnection(Constants.DB_PECPEI);    		
			pstmt = connPecPei.prepareStatement(CHECK_ID_CARTELLA_QRY);						
			pstmt.setInt(1, idCartellaToCheck);
			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()){					
					if(rs.getInt(1) > 0)
						esiste = true;
				}
			}
		} 
	    catch (SQLException sqlException) {
	    	sqlException.printStackTrace();
	        throw new DAOException("Errore durante accesso al DB");
	    }				
		catch (Exception eh) {
			eh.printStackTrace();
	        throw new DAOException("Errore durante accesso al DB");
		}
		finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (connPecPei != null)
					connPecPei.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
				
		return esiste;
		
	}//esisteIdCartella
	

	public int rinominaCartella(CartellaDTO cartDTO) throws DAOException {
		
		int rowCount=0;
		//System.out.println("CartellaDAO :: rinominaCartella");	
		Connection connPecPei = null;
		try {
			connPecPei = cm.getConnection(Constants.DB_PECPEI);    		
			PreparedStatement pstmt = null;						
			pstmt =  connPecPei.prepareStatement(UPD_RENAME_CARTELLA_QRY);						
			pstmt.setString(1, cartDTO.getDescrizione());
			pstmt.setInt(2, cartDTO.getIdCartella());
			rowCount = pstmt.executeUpdate();
			if(pstmt!=null)
				pstmt.close();			
		}		
	    catch (SQLException sqlException) {
	    	sqlException.printStackTrace();
	        throw new DAOException("Errore durante accesso al DB");
	    }				
		catch (Exception eh) {
			eh.printStackTrace();
	        throw new DAOException("Errore durante l'operazione di rinomina cartella");
		}
		finally{
			try{
				if(connPecPei!=null)
					connPecPei.close();
			}
	        catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		return rowCount;
		
	} //rinominaCartella	
	
	
	public boolean checkFigli(int idCartella) throws DAOException{
		
		boolean hasFigli = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int count = -1;
		Connection connPecPei = null;
		try {
			connPecPei = cm.getConnection(Constants.DB_PECPEI);    
			pstmt =  connPecPei.prepareStatement(COUNT_CARTELLE_FIGLIE_QRY);			
			pstmt.setInt(1, idCartella);
			rs  = pstmt.executeQuery();			
			if(rs != null){
				while (rs.next()) {
					count = rs.getInt(1);					
				}
			}
			hasFigli = count > 0 ? true : false;
			pstmt.close();			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (SQLException e) {
		    	e.printStackTrace();
		    }
		}		
				
		return hasFigli;
		
	}//checkFigli
	
	
	/**
	 * Nel caso la cartella specificata sia vuota, la cancella logicamente, impostando a 0 il valore del campo 'Valido';
	 * altrimenti lancia una CartellaNotEmptyException contenente un messaggio d'errore.
	 * @param idCartella identificativo della cartella che si vuole eliminare
	 * @param cron oggetto contenente le info da memorizzare nella tab cronologia
	 * @return
	 * @throws DAOException se si verificano errori durante l'accesso al DB
	 * @throws CartellaNotEmptyException se la cartella non pu� essere eliminata perch� non vuota
	 */
	//public int deleteLogicoCartella(int idCartella, ActivityDTO logActivity) throws DAOException, CartellaNotEmptyException {
/*
	public int deleteLogicoCartella(int idCartella) throws DAOException, CartellaNotEmptyException {
		
		int rowCount = -1;
		
		String msg = null;
		//System.out.println("CartellaDAO :: deleteLogicoCartella : idCartella="+idCartella);	
		try{			
			boolean hasCartelleFiglie = checkFigli(idCartella);			
			if(!hasCartelleFiglie){
				// controlla la presenza di messaggi nella cartella da cancellare
				int numMsg = new MessaggiDAO().selectCountMessaggiByCartella(idCartella);
				if(numMsg == 0){
					// caso di cartella vuota e senza msg: esegue il delete logico
					ConnectionManager cm = new ConnectionManager();
		    		conn = cm.getConnection(ConfigProp.jndiPecPei);					
					conn.setAutoCommit(false);	
					rowCount = doDeleteLogico(conn, idCartella);
					if (rowCount != 0) {
						if(cron != null){
							//logActivity.setIdCartella(idCartella);
							cron.setMessageID(String.valueOf(idCartella));
							//memorizza l'operazione di eliminazione logica nella tab cronologia
							 //new ActivityDAO().doInsertLogActivity(conn, logActivity);
//							new VerticaleCronologiaDAO().doInsertCronologia(conn, cron, true);
						}					
						conn.commit();
						//System.out.println("CartellaDAO :: deleteLogicoCartella :: commit eseguito");						
					}
				}
				else{
					// caso di cartella contenente msg					
					msg = "Impossibile cancellare la cartella specificata: spostare tutti i messaggi in essa contenuti";					
					throw new CartellaNotEmptyException(msg);
				}				
			}
			else{
				// caso di cartella con figli
				msg = "Impossibile cancellare la cartella specificata: eliminare le sue cartelle figlie";
				throw new CartellaNotEmptyException(msg);
			}
		}
		catch(CartellaNotEmptyException ex){
			throw ex;
		}
		catch(Exception eh){
		   System.err.println("CartellaDAO :: deleteLogicoCartella :: errore " + eh.getMessage());
		   if(conn!=null){
				try {
					conn.rollback();
				} catch (SQLException sqlex) {
					sqlex.printStackTrace();
				}	
		   }
		   throw new DAOException("Errore durante la cancellazione logica della cartella", eh);
		}
		finally{
			try{
				if(conn!=null)
					conn.close();
			}
	        catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		return rowCount;
		
	} //deleteLogicoCartella
*/	
	
	private int doDeleteLogico(Connection conn, int idCartella) throws Exception{
		
		int rowCount = -1;
		PreparedStatement pstmt = null;
		try{			
			pstmt =  conn.prepareStatement(DEL_LOGICO_CARTELLA_QRY);			
			pstmt.setInt(1, idCartella);
			rowCount = pstmt.executeUpdate();						
			pstmt.close();			
		}
		catch(Exception ex){
			System.err.println("Errore durante delete logico nella tab Cartelle: " + ex.getMessage());
			ex.printStackTrace();
			throw ex;
		}
		return rowCount;		
		
	}//doDeleteLogico
	
	
	/**
	* Inserisce un record nella tab Cartelle e un record di tracciamento
	* dell'attivita' nella tab LogActivity_Cartelle.
	* @param cartDTO l'oggetto rappresentante la cartella da inserire sul DB
	* @param cron l'oggetto rappresentante il record da inserire nella tabella cronologia
	* @return
	* @throws Exception se si verifica un errore durante l'accesso al DB
	*/
	//public int insertCartella(CartellaDTO cartDTO, ActivityDTO logActivity) throws DAOException {
	public int insertCartella(CartellaDTO cartDTO) throws DAOException {			
	
		//System.out.println("CartellaDAO :: insertCartella");
		
		int idNuovaCartella = -1;
		int result = -1;
		if(cartDTO != null){
			Connection connPecPei = null;
			try {
				connPecPei = cm.getConnection(Constants.DB_PECPEI);    
				connPecPei.setAutoCommit(false);
      		
        		idNuovaCartella = doInsertCartella(connPecPei, cartDTO);									
//				if(cron != null){
//					cron.setMessageID(String.valueOf(idNuovaCartella));
//					cron.setDettaglioAzione(cron.getDettaglioAzione() + " con id:" + idNuovaCartella);
//					result = new VerticaleCronologiaDAO().doInsertCronologia(conn, cron, true);
//					//result = new ActivityDAO().doInsertLogActivity(conn, logActivity);
//				}
				if(idNuovaCartella != -1){
					connPecPei.commit();
					//System.out.println("CartellaDAO :: insertCartella :: commit eseguito");
				}
			}
			catch(DAOException ex){
				ex.printStackTrace();
				//System.err.println("CartellaDAO :: insertCartella :: errore " + ex.getMessage());				
				if(connPecPei!=null)
					try {
						connPecPei.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}	
				throw ex;
			}
			catch(Exception eh){
				eh.printStackTrace();
				//System.err.println("CartellaDAO :: insertCartella :: errore " + eh.getMessage());				
				if(connPecPei!=null)
					try {
						connPecPei.rollback();
					} catch (SQLException e) {
						e.printStackTrace();
					}				
				throw new DAOException("ERRORE DURANTE ACCESSO AL DB");
			}			
			finally{
				try {
					if(connPecPei != null)
						connPecPei.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}		
		return idNuovaCartella;
	
	} //insertCartella
	
	
	private int doInsertCartella(Connection conn, CartellaDTO cartDTO) throws DAOException {
	
	int idCartella = -1;
	PreparedStatement pstmt = null;
	ResultSet generatedKeys = null;
	try{
			pstmt = conn.prepareStatement(INS_CARTELLA_QRY, Statement.RETURN_GENERATED_KEYS);			
			if(cartDTO.getIdPadre() == null)
				pstmt.setNull(1, java.sql.Types.INTEGER);
			else	
				pstmt.setInt(1, cartDTO.getIdPadre());
			pstmt.setInt(2, cartDTO.isValida()? 1 : 0);
			pstmt.setString(3, cartDTO.getDescrizione());			
			/*if(cartDTO.getAccount() == null || cartDTO.getAccount().equals(""))
				pstmt.setString(4, ConfigProp.accountPEC);
			else*/
			pstmt.setString(4, cartDTO.getAccount());
			pstmt.setString(5, cartDTO.getFullPathFolder());
			pstmt.executeUpdate();
			generatedKeys = pstmt.getGeneratedKeys();
			if (generatedKeys.next()) {
				idCartella = generatedKeys.getInt(1);
			} else {
				throw new SQLException("Impossibile ottenere la chiava generata per la cartella");
			}			
			//System.out.println("Inserimento in Cartelle effettuato correttamente: idCartella:" + idCartella);
			pstmt.close();
			if(generatedKeys != null) 
				try {
					generatedKeys.close();
				} catch (Exception e){}
		}
		catch(SQLException ex){
			System.err.println("Errore durante inserimento in Cartelle: " + ex.getMessage());
			ex.printStackTrace();
			throw new DAOException("ERRORE DURANTE ACCESSO AL DB" );
		}
		catch(Exception ex){
			System.err.println("Errore durante inserimento in Cartelle: " + ex.getMessage());
			ex.printStackTrace();
			throw new DAOException(ex.getMessage());
		}
		
		return idCartella;
	}
	
	
	/**
	 * Seleziona dal DB tutte le cartelle figlie di quella specificata in input 
	 * @param idPadre identificativo della cartella padre
	 * @return una lista di oggetti CartellaDTO rappresentanti le cartelle figlie di quella specificata
	 * @throws DAOException se si verifica un problema durante l'accesso al DB
	 */
	public List<CartellaDTO> selectCartelleFiglieById(int idPadre) throws DAOException{
		
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		CartellaDTO cart = null;
		List<CartellaDTO> elencoCartelleOut = null;
		System.out.println("CartellaDAO :: selectCartelleFiglieById :: idPadre=" + idPadre);
		Connection connPecPei = null;
		try {
			connPecPei = cm.getConnection(Constants.DB_PECPEI);    
			
			pstmt = connPecPei.prepareStatement(SEL_CARTELLE_FIGLIE_QRY);
			pstmt.setInt(1, idPadre);
			rs = pstmt.executeQuery();
			if (rs != null) {
				elencoCartelleOut = new ArrayList<CartellaDTO>();
				while (rs.next()) {
					cart = new CartellaDTO();
					cart.setIdCartella(rs.getInt("IdCartella"));
					cart.setIdPadre(rs.getInt("IdPadre"));
					cart.setDescrizione(rs.getString("Descrizione"));
					cart.setAccount(rs.getString("AccountPEC"));
					cart.setValida(true);
					elencoCartelleOut.add(cart);
				}
			}
		} 
		catch (Exception eh) {
			eh.printStackTrace();
			throw new DAOException("ERRORE DURANTE ACCESSO AL DB");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (connPecPei != null)
					connPecPei.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return elencoCartelleOut;
		
	}//selectCartelleFiglieById
	
	
	public ArrayList<CartellaDTO> selectRootsByAccountPEC(String accountPEC) throws DAOException{
		
		System.out.println("CartellaDAO :: selectRootsByAccountPEC");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<CartellaDTO> elencoCartelleRoot = new ArrayList<CartellaDTO>();		
		Connection connPecPei = null;
		try {
			connPecPei = cm.getConnection(Constants.DB_PECPEI);    
			pstmt = connPecPei.prepareStatement(SEL_CARTELLE_ROOT_BY_ACCOUNT_PEC_QRY);			
			pstmt.setString(1, accountPEC);
			rs = pstmt.executeQuery();
			if (rs != null) {
				CartellaDTO nodoCorrente = null;
				int idNodoCorrente = -1;
				while (rs.next()) {
					
					nodoCorrente = new CartellaDTO();
					idNodoCorrente = rs.getInt("IdCartella");
					nodoCorrente.setIdCartella(idNodoCorrente);					
					nodoCorrente.setDescrizione(rs.getString("Descrizione"));					
					nodoCorrente.setValida(true);
					// aggiunge alla lista
					elencoCartelleRoot.add(nodoCorrente);
					
					/*
					ArrayList<Cartella> elencoNodiFigli = this.selectCartelleFiglieRecursive(idNodoCorrente, accountPEC);
					if(elencoNodiFigli!=null && !elencoNodiFigli.isEmpty()){
						//System.out.println("CartellaDAO :: selectCartelleFiglieRecursive :: il nodo corrente: "+idNodoCorrente+" ha " +elencoNodiFigli.size()+" figli." );
						nodoCorrente.setListaCartelleFiglie(elencoNodiFigli);
					}*/
					
				} //while
			}
		} 
		catch (Exception eh) {
			eh.printStackTrace();
			throw new DAOException("ERRORE DURANTE ACCESSO AL DB");
			//throw eh;
		}
		finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (connPecPei != null)
					connPecPei.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	

		return elencoCartelleRoot;
		
	} //selectRootsByAccountPEC	
}
