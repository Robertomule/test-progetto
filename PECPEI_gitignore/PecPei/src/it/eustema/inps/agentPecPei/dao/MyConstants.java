package it.eustema.inps.agentPecPei.dao;

public class MyConstants {
	
	public static final String wsGetElencoMessaggiOut = "wsGetElencoMessaggiOut";
	public static final String result = "result";
	public static final String errorDescription = "errorDescription";
	
	public static final String intestazioni = "intestazioni";
	public static final String intestazione = "intestazione";
	
	public static final String messageID = "messageID";
	public static final String verso = "verso";
	public static final String stato = "stato";
	public static final String riservato = "riservato";
	public static final String sensibile = "sensibile";
	public static final String deliveredDate = "deliveredDate";
	public static final String codiceAOO = "codiceAOO";
	public static final String accountPEC = "accountPEC";
	public static final String subject = "subject";
	public static final String fromAddr = "fromAddr";
	public static final String replyTo = "replyTo";
	public static final String tipoMessaggio = "tipoMessaggio";
	public static final String tipologia = "tipologia";
	public static final String nomeUfficio = "nomeUfficio";
	public static final String fromPhrase = "fromPhrase";
	public static final String codiceAOOMitt = "codiceAOOMitt";
	
	
	public static final String msgID = "msgID";
	public static final String segnatura = "segnatura";
	public static final String statoLettura = "statoLettura";
	public static final String numAllegati = "numAllegati";
	public static final String nomiAllegati = "nomiAllegati";
	
	public static final String destinatari = "destinatari";
	public static final String inA = "inA";
	
	public static final String addr = "addr";
	public static final String sede = "sede";
	//public static final String nomeUfficio = "nomeUfficio";
	public static final String nomeDirigente = "nomeDirigente";
	
	public static final String inCC = "inCC";
	// aggiunto il 16-10-13
	public static final String idCartella = "idCartella";
	// DETTAGLIO
	
	public static final String wsGetDettagliMessaggioOut = "wsGetDettagliMessaggioOut";
	public static final String wsGetAllegatoOut = "wsGetAllegatoOut";
	public static final String wsGetDettaglioOut = "wsGetDettaglioOut";
	public static final String wsGetMessageId = "wsGetMessageId";
	public static final String wsGestioneMessaggio = "wsGestioneMessaggio";
	
	
	public static final String uiDoc = "uiDoc";
	public static final String dominioPEC = "dominioPEC";
	public static final String nomeSede = "nomeSede";
	public static final String nomeRegione = "nomeRegione";
	

	public static final String postedDate = "postedDate";
	public static final String xTipoRicevuta = "xTipoRicevuta";
	public static final String classifica = "classifica";
	public static final String desClassifica = "desClassifica";
	public static final String documentoPrincipale = "documentoPrincipale";
	public static final String isReply = "isReply";
	public static final String inReplyTo = "inReplyTo";
	public static final String body = "body";
	public static final String inReplyToSegnatura = "inReplyToSegnatura";
	public static final String inReplyToSubject = "inReplyToSubject";
	public static final String headers = "headers";
	public static final String xTrasporto = "xTrasporto";
	public static final String xRicevuta = "xRicevuta";
	public static final String segnaturaRif = "segnaturaRif";
	public static final String segnaturaRoot = "segnaturaRoot";
	public static final String isSigned = "isSigned";
	public static final String isSignedVer = "isSignedVer";
	public static final String datiCertificazione = "datiCertificazione";
	public static final String signIssuerName = "signIssuerName";
	public static final String signSubjectName = "signSubjectName";
	public static final String signValidFromDate = "signValidFromDate";
	public static final String signValidToDate = "signValidToDate";
	public static final String postaCertEmlPostedDate = "postaCertEmlPostedDate";
	public static final String postaCertEmlSubject = "postaCertEmlSubject";
	public static final String postaCertEmlFrom = "postaCertEmlFrom";
	public static final String postaCertEmlBody = "postaCertEmlBody";
	public static final String postaCertEmlIsSigned = "postaCertEmlIsSigned";
	public static final String postaCertEmlIsSignedVer = "postaCertEmlIsSignedVer";
	public static final String postaCertEmlDatiSignature = "postaCertEmlDatiSignature";
	public static final String postaCertEmlSignatureIssuerName = "postaCertEmlSignatureIssuerName";
	public static final String postaCertEmlSignSubjectName = "postaCertEmlSignSubjectName";
	public static final String postaCertEmlSignValidFromDate = "postaCertEmlSignValidFromDate";
	public static final String postaCertEmlSignValidToDate = "postaCertEmlSignValidToDate";
	public static final String size = "size";
	public static final String ipAddress = "ipAddress";
	public static final String autoreCompositore = "autoreCompositore";
	public static final String autoreUltimaModifica = "autoreUltimaModifica";
	public static final String dataOraUltimaModifica = "dataOraUltimaModifica";
	public static final String confermaRicezione = "confermaRicezione";
	
	public static final String autoreMittente = "autoreMittente";
	
	public static final String addrConfermaRicezione = "addrConfermaRicezione";
	public static final String bodyHtml = "bodyHtml";
	public static final String postaCertEmlHeaders = "postaCertEmlHeaders";
	public static final String codiceAMM = "codiceAMM";
	public static final String codiceAMMMitt = "codiceAMMMitt";
	public static final String segnaturaMitt = "segnaturaMitt";
	public static final String isSPAMorCCN = "isSPAMorCCN";
	public static final String tipoMitt = "tipoMitt";
	public static final String desMitt = "desMitt";
	public static final String direttoreAOO = "direttoreAOO";
	public static final String direttoreAOOMitt = "direttoreAOOMitt";
	public static final String substato = "substato";
	
	
	public static final String fileName = "fileName";
	public static final String contentType = "contentType";
	public static final String fileSize = "fileSize";
	public static final String testoAllegato = "testoAllegato";
	public static final String tipoAllegato = "tipoAllegato";
	
	
	public static final String codiceApp = "codiceApp";
	public static final String idSede = "idSede";
	public static final String tipoPecPei = "tipoPecPei";
	public static final String utente = "utente";
	
	public static final String matricola = "matricola";
	public static final String isDirigente = "isDirigente";
	public static final String isResponsabile = "isResponsabile";
	
	public static final String delegheSede = "delegheSede";
	public static final String delegaSede = "delegaSede";
	public static final String permessi = "permessi";
	
	
	public static final String delegheArea = "delegheArea";
	public static final String delegaArea = "delegaArea";
	
	public static final String delegheMessaggi = "delegheMessaggi";
	public static final String delegaMessaggio = "delegaMessaggio";
	
	
	public static final String wsGetPrivilegiOut = "wsGetPrivilegiOut";
	public static final String wsCreaCasellaOut = "wsCreaCasellaOut";
	public static final String wsSetPrivilegiOut = "wsSetPrivilegiOut";
	
	public static String PIC_RESOURCES_FILE_NAME = "it.eustema.inps.pecpei.ApplicationResources";
	
	// gestione cartelle
	public static final int CARTELLA_ELIMINATA = 0;	
	public static final int CARTELLA_VALIDA = 1;
	
	public static final String UTF8_CHARSET = "UTF-8";
	
	public static final String OGGETTO_CARTELLA = "cartella";
	
	public static final String OGGETTO_MESSAGGIO = "messaggio";
	
	public static final String AZIONE_CREAZIONE_CARTELLA = "creazione_cartella";
	
	public static final String AZIONE_CANCELLAZIONE_CARTELLA = "cancellazione_cartella";
	
	public static final String AZIONE_SPOSTAMENTO_CARTELLA = "spostamento_cartella";
	
	public static final String AZIONE_SPOSTAMENTO_MSG = "spostamento_messaggio";
	
	public static final String AZIONE_CARICAMENTO_MSG = "caricamento_messaggio";
	
	public static final int ID_SISTEMA_PIC = 1;
	
	public static final String TIPO_MSG_PEC = "1";
	
	public static final String TIPO_MSG_PEI = "2";
	
	public static final String TIPO_MSG_PECPEI = "3";
	
	// costanti per la composizione della query nella ricerca dello storico messaggi
	public static final int RICERCA_IN_OGGETTO = 0;
	public static final int RICERCA_IN_MITTENTE = 1;
	public static final int RICERCA_IN_SEGNATURA = 2;
	public static final int RICERCA_IN_TESTO = 3;
	public static final int RICERCA_IN_X = 4;
	public static final int RICERCA_IN_OGG_TESTO = 5;
	public static final int RICERCA_IN_TUTTI = -1;
	
	// costanti utilizzate per la memorizzazione nella tabella del monitoraggio chiamate SOAP
	public static final String ELENCO_MESSAGGI_OPERATION = "getElencoMessaggi";
	public static final String ELENCO_ALL_MESSAGGI_OPERATION = "getAllElencoMessaggi";
	public static final String DETTAGLIO_MESSAGGIO_OPERATION = "getDettaglioMessaggio";
	public static final String ALLEGATO_OPERATION = "getAllegato";
	public static final String INVIO_MSG_OPERATION = "invioMessaggio";
	public static final String GET_MSG_ID_OPERATION = "getMessageID";
	public static final String GET_PRIVILEGI_OPERATION = "getPrivilegi";
	public static final String CREA_CASELLA_OPERATION = "creaCasella";
	public static final String SET_PRIVILEGI_OPERATION = "wsSetPrivilegi";
	public static final String CREA_CARTELLA_OPERATION = "creaCartella";
	public static final String DEL_CARTELLA_OPERATION = "eliminaCartella";
	public static final String RENAME_CARTELLA_OPERATION = "rinominaCartella";
	public static final String ID_CARTELLA_OPERATION = "getIdCartella";
	public static final String SPOSTA_MESSAGGI_OPERATION = "spostaMessaggi";
	public static final String CARTELLE_ROOT_OPERATION = "getElencoCartelleRoot";
	public static final String CARTELLE_FIGLIE_OPERATION = "getCartelleFiglie";
	public static final String STORICO_MESSAGGI_OPERATION = "getMessaggiStorico";
	
	public static final short ESITO_OK = (short)0;	
	public static final short ESITO_KO = (short)1;		

}
