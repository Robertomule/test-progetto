package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.LogIndiciPaDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;

public class OperazioniSupportoDAO extends AgentBaseDAO {

	private static final String INSERT_OPERAZIONE_SUPPORTO = "INSERT INTO OperazioniSupporto(dataSupporto, datum, field, messageId, newDatum, utenteSupporto) VALUES (?, ?, ?, ?, ?, ?)";
	
	public void doInsertOperazioniSupporto(String dato, String datoBonificato, String field, String messageId) throws DAOException {

		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(INSERT_OPERAZIONE_SUPPORTO);
			
			pstmt.setTimestamp(1, new Timestamp(new Date().getTime()));
			pstmt.setString(2, dato);
			pstmt.setString(3, field);
			pstmt.setString(4, messageId);
			pstmt.setString(5, datoBonificato);
			pstmt.setString(6, "Batch PecPei");
			
			pstmt.executeUpdate();
			
		} catch( Exception eh ) {
			throw new DAOException("Errore insert dati Account PEC: " + eh.getMessage());
		} finally {
			try {
				if(connPecPei!=null)
					connPecPei.close();
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}