package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.EsitoNotificaSgdDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.DBConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EsitiNotificheSgdDAO extends AgentBaseDAO{

	private static Log log = LogFactory.getLog(AllegatiDAO.class);
	
	private final String QUERY_INSERT = "INSERT INTO EsitiNotificheSGD(Metodo, AccountPec, MessageID, MsgID, Stato, Esito, DescrizioneErrore) " +
										"VALUES(?, ?, ?, ?, ?, ?, ?)";
	
	public void insertEsito(EsitoNotificaSgdDTO esitoNotificaSgd)throws Exception {

		PreparedStatement preparedStatement = null;
		Connection connection = null;
		try {

			connection = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

			preparedStatement =  connection.prepareStatement(QUERY_INSERT);

			preparedStatement.setString(1, esitoNotificaSgd.getMetodo());
			preparedStatement.setString(2, esitoNotificaSgd.getAccountPec());
			preparedStatement.setString(3, esitoNotificaSgd.getMessageID());
			preparedStatement.setString(4, esitoNotificaSgd.getMsgID());
			preparedStatement.setString(5, esitoNotificaSgd.getStato());
			preparedStatement.setString(6, esitoNotificaSgd.getEsito().equalsIgnoreCase("OK")?"OK":"KO");
			preparedStatement.setString(7, esitoNotificaSgd.getDescrizioneErrore());
			
			preparedStatement.executeUpdate();
			
		}catch(Exception e) {
			System.err.println("Errore durante l'inserimento di un record nella tabella: EsitiNotificheSGD" +
					"\nMotivo Errore: " + e.getMessage());
			log.error("Errore durante l'inserimento di un record nella tabella: EsitiNotificheSGD" +
					"\nMotivo Errore: ", e);
		}finally {
			if(preparedStatement!=null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
			if (preparedStatement != null)
				preparedStatement.close();
		}		

	}
}
