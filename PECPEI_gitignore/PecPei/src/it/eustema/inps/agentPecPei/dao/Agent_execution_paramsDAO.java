package it.eustema.inps.agentPecPei.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import it.eustema.inps.agentPecPei.dto.AgentExecutionParamDTO;
import it.eustema.inps.agentPecPei.dto.AgentExecutionParamsDTO;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
//import it.eustema.inps.utility.DBConnectionManager;

public class Agent_execution_paramsDAO extends AgentBaseDAO{

	private static final String INS_AGENT_EXEC_PARAM_QRY = "INSERT INTO Agent_execution_params " + 
										" (execution_id, param_name, param_value) " + 
										" VALUES (?,?,?)";
	
	private int doInsertAgentExecParam(Connection connPecPei, long execID, AgentExecutionParamDTO paramDTO) throws Exception{
	
		int rowCount = -1;
		PreparedStatement pstmt = null;
		try{			
			pstmt =  connPecPei.prepareStatement(INS_AGENT_EXEC_PARAM_QRY);			
			pstmt.setLong(1, execID);
			pstmt.setString(2, paramDTO.getParamName());
			pstmt.setString(3, paramDTO.getParamValue());
			rowCount = pstmt.executeUpdate();						
			pstmt.close();			
		}
		catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}
		return rowCount;
		
	}
	
	public int insertAgentExecutionParams(AgentExecutionParamsDTO paramsDTO)throws DAOException {
		
		//System.out.println("Agent_execution_paramsDAO :: insertAgentExecutionParams START");
		int ret = -1;				
		Connection connPecPei = null;		
		try
		{
			//DBConnectionManager cm = new DBConnectionManager();
			//connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			connPecPei.setAutoCommit(false);
			List<AgentExecutionParamDTO> listaExecParams = paramsDTO.getAgentExecutionParams();
			for(AgentExecutionParamDTO execParam: listaExecParams){
				doInsertAgentExecParam(connPecPei, paramsDTO.getExecution_id(), execParam);
			}
			connPecPei.commit();
			//System.out.println("Agent_execution_paramsDAO :: insertAgentExecutionParams :: commit eseguita");
			
		}
		catch( Exception eh )
		{
		   System.out.println("Agent_execution_paramsDAO :: insertAgentExecutionParams Exception: " + eh.getMessage());			   
		   if(connPecPei!=null){
				try {
					connPecPei.rollback();
				} catch (SQLException sqlex) {
					sqlex.printStackTrace();
				}	
		   }
		   throw new DAOException("Errore durante l'inserimento su Agent_execution_params: " + eh.getMessage());
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		
		//System.out.println("Agent_execution_paramsDAO :: insertAgentExecutionParams END");
		return ret;

	}
}
