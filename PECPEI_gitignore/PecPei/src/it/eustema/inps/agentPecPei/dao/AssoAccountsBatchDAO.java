package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.DBConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author rstabile
 *
 */

public class AssoAccountsBatchDAO {
	
	private static Log log = LogFactory.getLog(AssoAccountsBatchDAO.class);
	private static final String GET_BATCH_BY_ID = "select a.flgAttivo_assoAccountsBatch from assoAccountsBatch a where a.key_assoAccountsBatch = ?";
	
	public boolean isBatchAttivo(int idBatch){
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		boolean isAttivo = false;
		
		try {
			
			conn = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
			
			pstmt =  conn.prepareStatement(GET_BATCH_BY_ID);
			pstmt.setInt(1, idBatch);
			rs = pstmt.executeQuery();
			
		}catch(Exception e){
			log.error("errore : verificaPresenzaAllegato", e);
		}finally{
			try{
				if(rs!=null){
					rs.next();
					isAttivo = rs.getBoolean(1); 
					rs.close();
				}else
					isAttivo = false;
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null){
					conn.close();
				}
			}catch(Exception e){
				
			}
		}
		return isAttivo;
	}
}