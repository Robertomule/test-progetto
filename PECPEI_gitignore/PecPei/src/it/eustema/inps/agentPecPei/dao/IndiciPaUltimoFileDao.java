package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.DBConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author rstabile
 *
 */

public class IndiciPaUltimoFileDao {

	public byte[] getUltimoFileDaLavorare(){
	PreparedStatement pstmt = null;
	Connection conn = null;
	ResultSet rs = null;
	byte[] file = null;
	
	try {

		conn = DBConnectionManager.getInstance().getConnection(Constants.DB_VERIFICAPEC);

		String querySelect = "select top(1) fileBlob from indiciPaUltimoFile order by id desc";
		pstmt =  conn.prepareStatement(querySelect);
		rs = pstmt.executeQuery();
		
		if(rs.next()) {
			file = rs.getBytes(1);
		}

	}catch(Exception e){
	}finally {
		try {
			if(conn!=null) conn.close();
			if(rs!=null) rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		return file;
	}
}


