package it.eustema.inps.agentPecPei.dao;

import it.eustema.inps.agentPecPei.dto.AgentExecutionParamDTO;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Agent_cfg_paramsDAO extends AgentBaseDAO{

	private static final String SELECT_CFG_AGENT_PARAMS_QRY = "SELECT p.param_desc, p.param_value, p.pattern_valid " +
								" FROM Agent_cfg_params p with(nolock)";
	
	public List<AgentExecutionParamDTO> selectAgentCfgParams() throws Exception{
		
		System.out.println("Agent_cfg_paramsDAO :: selectAgentCfgParams() :: Esecuzione del metodo");
		Connection connPecPei = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AgentExecutionParamDTO execParamsDTO = null;
		List<AgentExecutionParamDTO> listaCfgParams = new ArrayList<AgentExecutionParamDTO>();
		try
		{
//			connPecPei = cm.getConnection(ConfigProp.jndiPecPei);
			//System.out.println("$ - QUERY nella tabella Agent_cfg_params");
			System.out.println("Agent_cfg_paramsDAO :: Esecuzione della query " + SELECT_CFG_AGENT_PARAMS_QRY);
			connPecPei = cm.getConnection(Constants.DB_PECPEI);
			pstmt = connPecPei.prepareStatement(SELECT_CFG_AGENT_PARAMS_QRY);

			rs  = pstmt.executeQuery();

			while (rs.next()) {
				execParamsDTO = new AgentExecutionParamDTO();
				execParamsDTO.setParamName(rs.getString(1));
				execParamsDTO.setParamValue(rs.getString(2));
				execParamsDTO.setPatternValidazione(rs.getString(3));

				listaCfgParams.add(execParamsDTO);
		    }
			//System.out.println("# - Totale dei parametri caricati "+listaCfgParams.size());
			//System.out.println("Agent_cfg_paramsDAO :: selectAgentCfgParams :: Parametri caricati "+listaCfgParams.size());
	
		}
		catch( Exception eh )
		{
			System.out.println("Agent_cfg_paramsDAO :: selectAgentCfgParams :: exception=" + eh.getMessage());
			throw eh;
		}
		finally{
			try {
				if(connPecPei!=null)
					connPecPei.close();
		    }catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
		System.out.println("Agent_cfg_paramsDAO :: selectAgentCfgParams :: Fine del metodo");
		
		return listaCfgParams;

	} 
}
