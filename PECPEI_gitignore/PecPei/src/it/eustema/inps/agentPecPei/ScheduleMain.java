package it.eustema.inps.agentPecPei;

import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dao.Agent_cfg_paramsDAO;
import it.eustema.inps.agentPecPei.dao.Agent_execution_logDAO;
import it.eustema.inps.agentPecPei.dao.Agent_execution_paramsDAO;
import it.eustema.inps.agentPecPei.dto.AgentExecutionLogDTO;
import it.eustema.inps.agentPecPei.dto.AgentExecutionParamDTO;
import it.eustema.inps.agentPecPei.dto.AgentExecutionParamsDTO;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;

import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.StaticUtil;
import it.eustema.inps.utility.logging.LoggingConfigurator;

import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.AgentStatus;
import it.eustema.inps.agentPecPei.util.AgentType;
import it.eustema.inps.agentPecPei.util.Constants;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ScheduleMain {


	static {
        System.out.println("ScheduleMain :: esegue blocco di inizializzazione");
		//blocco eseguito una volta al caricamento della classe
        try {
			// carica i parametri dal file di cfg
			//new ConfigProp(cfgFileName);
			new ConfigProp().init();
			// inizializzazione configurazione del logging
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
		} catch (Exception e) {
			System.err.println("" + e.getMessage());
		}        
    }
	
	private static Log log = LogFactory.getLog("it.eustema.inps.agentPecPei.ScheduleMain");
//	private static int idBatch = -1;
	
    public static void main(String args []) throws Exception { 
    	
    	log.info("main :: START");
    	if(args.length > 0)
    		log.info("main :: Batch in avvio :: " + args[0]); 
    	
    	System.out.println("main :: Eseguo Conversione idBatch in Intero");
    	log.info("main :: Eseguo Conversione idBatch in Intero");
    	
    	/**ATTENZIONE: Da Utilizzare solo per creazioine jar locale**/
    	/*System.out.println("##################################################################################################");
    	System.out.println("\nATTENZIONE: Prima di avviare l'esecuzione accertarsi di selezionare un batch che non sia gi? schedulato in console");
    	String workingDir = System.getProperty("user.dir");
 	   	System.out.println("\nWorking directory : " + workingDir+"\n");
 	   	//String libraryPath = workingDir.replace("\\","\\\\")+"\\\\authdll\\\\";
 	   	System.setProperty("java.library.path", workingDir+"\\authdll\\");
 	   	System.out.println("Directory library path impostata: " + System.getProperty("java.library.path")+"\n");
 	   	System.out.println("##################################################################################################");
    	Scanner reader = new Scanner(System.in);
    	System.out.print("Inserisci il numero dell'istanza batch da avviare (-1 per terminare) --> ");
    	int idBatch = reader.nextInt();
    	if(idBatch==-1)
    		return;*/
    	/********************************************************/
    	
    	//int idBatch = Integer.parseInt(args[0]);
		int idBatch = 1;

		System.out.println("main :: idBatch "+idBatch);
    	log.info("main :: Esito Conversione idBatch "+idBatch);
    	
    	//Faccio il set dei parametri del certificato per accedere al servizio
		System.setProperty("javax.net.ssl.keyStore", ConfigProp.keyStore);
		System.setProperty("javax.net.ssl.keyStorePassword", ConfigProp.keyStorePassword);
		System.setProperty("javax.net.ssl.trustStore", ConfigProp.keyStore);
		System.setProperty("javax.net.ssl.trustStorePassword", ConfigProp.keyStorePassword);
		//26/01/2015 Nuove modifica NotificaErrori
    	System.setProperty("NotificaErrori.DbName", Constants.DB_PECPEI);
    	CommandExecutor executorService = CommandExecutorManager.getInstance().getExecutor();
    	try {
        	Agent_cfg_paramsDAO paramsDAO = new Agent_cfg_paramsDAO();
        	Agent_execution_logDAO execLogDAO = new Agent_execution_logDAO();
        	Agent_execution_paramsDAO execParamsDAO = new Agent_execution_paramsDAO();
        	int executionID = -1;
        	List<AgentExecutionParamDTO> listaExecParams = null;

    		AgentExecutionLogDTO executionLogDTO = new AgentExecutionLogDTO();
    		executionLogDTO.setIdBatch(idBatch);
    		// legge dal DB i parametri di configurazione del JOB
			System.out.println("main :: paramsDAO.selectAgentCfgParams() :: Lettura dei parametri di configurazione presenti nel DB");

			listaExecParams = paramsDAO.selectAgentCfgParams();
			System.out.println("main :: Lista dei record ottenuti dalla query");
			int parameters = 0;
			for (AgentExecutionParamDTO lista: listaExecParams) {
				parameters ++;
				System.out.println( parameters + ". " + "Nome: " + lista.getParamName() + ", Valore: " + lista.getParamValue());
			}

			Map<String, String> paramsMap = StaticUtil.convertParamsListToMap(listaExecParams);
			// Legge la password per la cifratura\decifratura Pec
			
			ConfigProp.passwordPec = paramsMap.get("passwordPec");
			ConfigProp.passwordIVPec = paramsMap.get("passwordIVPec");
			ConfigProp.insBodyPart = paramsMap.get("ins_bodyPart");
			
			String abiltazioneAgentValue = paramsMap.get("abilitato");
			if(abiltazioneAgentValue.equals(Constants.AGENT_DISABLED)){
				// caso di esecuzione dell'agent disabiliata
				executionLogDTO.setAgent_status(AgentStatus.DISABLED.name());
				executionLogDTO.setEnd_time(executionLogDTO.getStart_time());												
			}				
			
			// inserisce un record nella tab Agent_execution_log e ricava l'identificativo del job
			System.out.println("main :: execLogDAO.insertAgentExecution() :: Entro all'interno della classe Agent_execution_logDAO");
			executionID = execLogDAO.insertAgentExecution(executionLogDTO);
			executionLogDTO.setExecution_id(executionID);

			//System.out.println("$ - IdBatch  " + executionLogDTO.getIdBatch());
			
			if(executionLogDTO.getAgent_status().equals(AgentStatus.STARTED.name())){
				System.out.println("main :: START :: Agent Abilitato");
				// caso di agent abilitato
				log.info("main :: caso di agent abilitato");				
				String agentTypeStr = paramsMap.get("agent_type");

				System.out.println("main :: paramsMap.get(\"agent_type\") :: Valore corrispondente all'agent_type presente nel DB e': " + agentTypeStr);
				// i values iniziano da indice 0

				//System.out.println("VALUES Agent Type:" + AgentType.values()[0] + " " + AgentType.values()[1] + " " + AgentType.values()[2]);
				AgentType agentType = AgentType.values()[Integer.parseInt(agentTypeStr) -1];

				System.out.println("main :: AgentType.values()[Integer.parseInt(agentTypeStr) -1] :: Valore finale: " + agentType);

				log.info("main :: agentTypeStr=" + agentTypeStr);				
				// inserisce un record nella tab dei parametri di esecuzione del job per ognuno dei parametri di cfg
				AgentExecutionParamsDTO paramsDTO = new AgentExecutionParamsDTO();
				//System.out.println("$ - Agent_execution_paramsDAO -> Setto i parametri (executionID, listaExecParams, paramsDTO)");
				paramsDTO.setExecution_id(executionID);
				paramsDTO.setAgentExecutionParams(listaExecParams);
				execParamsDAO.insertAgentExecutionParams(paramsDTO);
				
				ExecutorService executor;
				Set<Future<String>> futureSet = new HashSet<Future<String>>(); 
				
				Callable<String> agentPEC;
				Callable<String> agentPEI;
				Callable<String> agentHERMES;
				Callable<String> agentDestinatari;
				Future<String> futurePEC;
				Future<String> futurePEI;
				Future<String> futureHERMES;

				log.info("main :: agentTypeId=" + agentType.getTypeId());
				System.out.println("main :: agentTypeId=" + agentType.getTypeId());

				System.out.println("main :: Entro nello switch");

				switch(agentType){
						
					case PEC:
						System.out.println("main :: switch() :: Caso Agent PEC");
						log.info("main :: caso agent PEC");
						executor = Executors.newFixedThreadPool(1);
						System.out.println("main :: switch(PEC) :: Creo un oggetto di tipo AgentCallable(PEC, idBatch)");
						agentPEC = new AgentCallable(AgentType.PEC,idBatch);
						futurePEC = executor.submit(agentPEC);
						futureSet.add(futurePEC);
						break;
					
					case PEI:
						System.out.println("main :: switch() :: Caso Agent PEI");
						log.info("main :: caso agent PEI");
						executor = Executors.newFixedThreadPool(1);
						System.out.println("main :: switch(PEI) :: Creo un oggetto di tipo AgentCallable(PEI, -1)");
						agentPEI = new AgentCallable(AgentType.PEI,-1);

						System.out.println("Entro nell'Agent Callable");
						agentPEI = new AgentCallable(AgentType.PEI,idBatch);
						futurePEI = executor.submit(agentPEI);
						futureSet.add(futurePEI);
						break;
						
					case PEC_PEI:
						log.info("main :: caso agent PEC e PEI");						
						executor = Executors.newFixedThreadPool(1);
						agentPEC = new AgentCallable(AgentType.PEC,idBatch);
						futurePEC = executor.submit(agentPEC);
						futureSet.add(futurePEC);
						break;
						
					case HERMES:
						log.info("main :: caso agent HERMES");						
//						executor = Executors.newFixedThreadPool(1);						
//						agentHERMES = new AgentCallable(AgentType.HERMES);
//						futureHERMES = executor.submit(agentHERMES);
//						futureSet.add(futureHERMES);
						break;						
						
					case PEC_PEI_HERMES:
						log.info("main :: caso agent PEC PEI HERMES");						
//						executor = Executors.newFixedThreadPool(3);
//						agentPEC = new AgentCallable(AgentType.PEC);
//						futurePEC = executor.submit(agentPEC);
//						futureSet.add(futurePEC);
//						agentPEI = new AgentCallable(AgentType.PEI);
//						futurePEI = executor.submit(agentPEI);
//						futureSet.add(futurePEI);
//						agentHERMES = new AgentCallable(AgentType.HERMES);
//						futureHERMES = executor.submit(agentHERMES);
//						futureSet.add(futureHERMES);
						break;
				}
								
				boolean stepsTerminated = true; 
				for (Future<String> retCode : futureSet) {
					log.debug("main :: retCode="+retCode.get());					
					if(!retCode.isDone()){
						// 
						stepsTerminated = false;
					}
				}
				log.debug("main :: stepsTerminated="+stepsTerminated);				
				if(stepsTerminated){				
					long tempoEndAgent = System.currentTimeMillis();
					executionLogDTO.setEnd_time(new Timestamp(tempoEndAgent));
					executionLogDTO.setAgent_status(AgentStatus.COMPLETED.name());
					execLogDAO.updateAgentExecution(executionLogDTO);
										
				}
				
			}			

			//************** Elaborazione Batch MailCommander **************************+
//			new MailCommander().elab();
			//**************************************************************************+
								
								
							
			
		} catch (Exception e) {
			executorService.setCommand( new SendMailCommand( new NotifyMailBusinessException( -9999, "Errore caricamento parametri di configurazione: " + e.getMessage() ) ) );
			executorService.executeCommand();
			log.info("main :: ERRORE:: " +e.getMessage()); 
			log.error("main :: ERRORE:: " +e.getMessage()); 
		}
		
		log.info("main :: END"); 
		System.exit( 0 );
		//26/01/2015 Nuove modifica NotificaErrori
		System.setProperty("NotificaErrori.DbName", "");
    } //main

}
