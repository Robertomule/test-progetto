package it.eustema.inps.agentPecPei.business.pec.documentale;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import wspia2013.InteropUscitaResponse2013;
import generated.vo.wspia.inperoperabilita.uscita.DatiInvio;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;

	/**
	 * Classe Command che si occupa di costruire un'istanza della classe SegnaturaDTO
	 * che contiene i dati risultanti dal parsing della stringa segnatura XML in input
	 * @author ALEANZI
	 *
	 */
	public class BuildSegnaturaInterDTOCommand implements Command {
		private String xml; 
		private InteropUscitaResponse2013 result2013;
		
		public BuildSegnaturaInterDTOCommand( String xml ){
			this.xml = xml;
		}
		public BuildSegnaturaInterDTOCommand( InteropUscitaResponse2013 result2013 ){
			this.result2013 = result2013;
		}
		@Override
		public Object execute() throws BusinessException {
			
			// TODO Auto-generated method stub
//			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SimpleDateFormat dt = new SimpleDateFormat("dd/mm/yyyy"); 
			SimpleDateFormat dt1 = new SimpleDateFormat("yyyyy-mm-dd");
			
		        /*SAXParser saxParser = saxParserFactory.newSAXParser();
		        SegnaturaParserHandler handler = new SegnaturaParserHandler();
		        saxParser.parse( new ByteArrayInputStream( xml.getBytes("UTF-8") ), handler );
		        SegnaturaDTO out = handler.getSegnaturaDTO();*/
				SegnaturaDTO out = null;
		        
		        // setup object mapper using the AppConfig class
		        JAXBContext context;
				try {
					
					if(xml!=null&&!xml.equalsIgnoreCase("")){
						context = JAXBContext.newInstance(generated.vo.wspia.inperoperabilita.segnatura.Segnatura.class);
						if ( xml.indexOf("CodiceErrore") == -1 ){
						 // parse the XML and return an instance of the AppConfig class
							generated.vo.wspia.inperoperabilita.segnatura.Segnatura seg = (generated.vo.wspia.inperoperabilita.segnatura.Segnatura) context.createUnmarshaller().unmarshal(new ByteArrayInputStream( xml.getBytes("UTF-8") ));
							out = new SegnaturaDTO();
							out.setStatus( SegnaturaDTO.OK );
							Date dataSegnatura = dt1.parse(seg.getIntestazione().getIdentificatore().getDataRegistrazione().toString());
							String segnatura = seg.getIntestazione().getIdentificatore().getCodiceAmministrazione()+"."+
												seg.getIntestazione().getIdentificatore().getCodiceAOO()+"."+
												dt.format(dataSegnatura)+"."+
												seg.getIntestazione().getIdentificatore().getNumeroRegistrazione();
							out.setSegnatura(segnatura);
						} else {
							context = JAXBContext.newInstance(generated.vo.wspia.errore.ServizioProtocollo.class);
							 // parse the XML and return an instance of the AppConfig class
					        generated.vo.wspia.errore.ServizioProtocollo sp = (generated.vo.wspia.errore.ServizioProtocollo) context.createUnmarshaller().unmarshal(new ByteArrayInputStream( xml.getBytes("UTF-8") ));
					        out = new SegnaturaDTO();
					        out.setStatus( SegnaturaDTO.KO );
					        out.setCodiceErrore(sp.getCodiceErrore());
					        out.setDescErrore(sp.getDescrizioneErrore());
						} 
					}else{
						context = JAXBContext.newInstance(it.inps.www.interop2013.Segnatura.class);
						if(result2013!=null && result2013.getCodice().getValue().equalsIgnoreCase("OK")){
							out = new SegnaturaDTO();
							out.setStatus( SegnaturaDTO.OK );
							it.inps.www.interop2013.Segnatura seg = (it.inps.www.interop2013.Segnatura) context.createUnmarshaller().unmarshal(new ByteArrayInputStream( result2013.getSegnaturaXML().getBytes("UTF-8") ));
							Date dataSegnatura = dt1.parse(seg.getIntestazione().getIdentificatore().getDataRegistrazione().toString());
							String segnatura = seg.getIntestazione().getIdentificatore().getCodiceAmministrazione()+"."+seg.getIntestazione().getIdentificatore().getCodiceAOO()+"."+dt.format(dataSegnatura)+"."+seg.getIntestazione().getIdentificatore().getNumeroRegistrazione();
							out.setSegnatura(segnatura);
						}else{
							out = new SegnaturaDTO();
					        out.setStatus( SegnaturaDTO.KO );
					        out.setCodiceErrore(result2013.getCodice().getValue());
					        out.setDescErrore(result2013.getDescrizione());
						}
					}
				} catch (JAXBException e) {
						//throw new BusinessException(-50000, e.getMessage()  );
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					//throw new BusinessException(-50000, e.getMessage()  );
				} catch (ParseException e) {
					// TODO Blocco catch generato automaticamente
				} 
		        
		        if ( out == null ){
		        	out = new SegnaturaDTO();
		        	out.setStatus(SegnaturaDTO.KO);
		        	out.setCodiceErrore( "-1" );
		        	out.setDescErrore("Errore non censito dal servizio del protocollo");
		        }
		        return out;
		}
		
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return "xml -> SegnaturaDTO";
		}
		
		
		
	}