package it.eustema.inps.agentPecPei.business.pec.mail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommand;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommandNew;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioUscitaCommand;
import it.eustema.inps.agentPecPei.business.pec.mail.InviaPecCommand;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;

public class ReadAndElabMessaggiPecDaProtocollareTest implements Command, Callable<Object> {

	/**
	 * 
	 * @author rstabile
	 *
	 */
	private String codAOO;
	
	public ReadAndElabMessaggiPecDaProtocollareTest( String codAOO){
		this.codAOO = codAOO;
	}
	
	@Override
	public Object execute() throws BusinessException {
		
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		
		//Recupero di tutti i messaggi in uscita
		MessaggiDAO messaggiDAO = new MessaggiDAO();
		AllegatiDAO allegatiDAO = new AllegatiDAO();
		List<MessaggioDTO> listaMessaggiInUscita = null;
		try {
			listaMessaggiInUscita = messaggiDAO.selezionaMessaggiInUscitaPECDaRiprotocollareTest( codAOO );//Qui prendo solo i casi di test con stato TDP
		} catch (DAOException e) {
			throw new NotifyMailBusinessException( -1000, "Errore nel recupero dei messaggi in entrata da riprotocollare dal DB. Dettaglio Errore: " + e.getMessage() );
		}
		
		for ( MessaggioDTO messaggioInUscita: listaMessaggiInUscita ){

			try {
				//Costruzione del messaggio in uscita con Destinatari ed allegati
				messaggioInUscita = messaggiDAO.selezionaDettaglioMessaggioInUscitaPECDaProtocollareTest( messaggioInUscita.getMessageId() );
				messaggioInUscita.setElencoFileAllegati( allegatiDAO.selectAllegatiWS( messaggioInUscita.getMessageId() ) );
				
				//Protocollazione fittizia
				SegnaturaDTO segnatura = new SegnaturaDTO();
				//INPS.dd/MM/yyyy.XXXXXXX
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy.HH:mm:ss");
				segnatura.setSegnatura("INPS."+sdf.format(new Date()));
				
				//Valorizzazione dello stato e della segnatura
				messaggioInUscita.setSegnatura( segnatura.getSegnatura() );
				messaggioInUscita.setStato( Constants.STATO_MSG_RICEVUTO );
				messaggioInUscita.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
				
				messaggiDAO.aggiornaMsgRicevutoDaRiprotocollareTest( null, messaggioInUscita );
				allegatiDAO.insertAllegato(messaggioInUscita,messaggiDAO.creaMessageTxt(messaggioInUscita));
				
				
			} catch (DAOException e) {
				executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
				executor.executeCommand();
			} catch (BusinessException be){
				//Si prosegue con il prossimo messaggio
			} catch (Exception e) {
				executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
				executor.executeCommand();
			} 
		}
		
		return new Integer(1);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "InviaMessaggiPECInUscita";
	}

	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}
}
