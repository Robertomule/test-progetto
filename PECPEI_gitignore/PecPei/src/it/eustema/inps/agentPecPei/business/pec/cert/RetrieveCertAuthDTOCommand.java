package it.eustema.inps.agentPecPei.business.pec.cert;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.util.CollectionStore;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommand;
import it.eustema.inps.agentPecPei.dto.CertAuthDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;

/**
 * Command che si occupa di costruire un'istanza della classe CertAuthDTO a partire
 * da un array di byte contentente un certificato. L'oggetto in output
 * conterr� tutti i dati del certificato in input
 * 
 * @author ALEANZI
 *
 */
public class RetrieveCertAuthDTOCommand implements Command {
	
	private byte[] cert;
	private static Logger log = Logger.getLogger(RetrieveCertAuthDTOCommand.class);
	
	public RetrieveCertAuthDTOCommand( byte[] cert ){
		this.cert = cert;
	}
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		
		
			//f = new FileInputStream( new File("C:\\LISTACER_20140102.zip.p7m") );
			//f = new FileInputStream( new File("C:\\smime.p7s") );
			//byte[] in = new byte[f.available()];
			//f.read( in );
			
		 CMSSignedData cms = null;
			try {
				cms = new CMSSignedData( cert );
			} catch (CMSException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}          
			   /*if(cms.getSignedContent() == null) {
			     //Error!!!
			         return null;
			  }*/
			  CertAuthDTO out = new CertAuthDTO();                
			  try {
				//cms.getSignedContent().write(out);
				if(cms!=null){  
					CollectionStore cs = (CollectionStore)cms.getCertificates();
					for ( X509CertificateHolder cert : (Collection<X509CertificateHolder>) cs.getMatches( null ) ){
						
						//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
						if(cert.toASN1Structure().getIssuer()!=null)
							out.setIssuerName(cert.toASN1Structure().getIssuer().toString());
						
						if(cert.toASN1Structure().getSubject()!=null)
							out.setSubjectName(cert.toASN1Structure().getSubject().toString());
						
						//System.out.println( "Fine validit� " + sdf.format(  cert.toASN1Structure().getEndDate().getDate() ) );
						out.setSignValidFrom( new Timestamp( cert.toASN1Structure().getStartDate().getDate().getTime() ));
						out.setSignValidTo( new Timestamp( cert.toASN1Structure().getEndDate().getDate().getTime() ));
						out.setSigned("1");
						out.setSignedVer("1");
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error("Errore per RetrieveCertAuthDTOCommand : "+e.getMessage(), e);
			}
//			} catch (CMSException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}          
//			  return out.toByteArray();
				return out;
			} 
	
	
		  
	
	public static void main( String[] args ){
		
			
			try {
				//FileInputStream f = new FileInputStream( new File("C:\\LISTACER_20140102.zip.p7m") );
				FileInputStream f = new FileInputStream( new File("C:\\smime.p7s") );
				byte[] in = new byte[f.available()];
				f.read( in );
				new RetrieveCertAuthDTOCommand( in ).execute();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Recupero certificato PEC";
	}

}
