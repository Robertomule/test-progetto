package it.eustema.inps.agentPecPei.business;

import it.eustema.inps.agentPecPei.dto.InputDTO;
import it.eustema.inps.agentPecPei.dto.OutputDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;

/**
 * Interfaccia Pattern Command
 * @author ALEANZI
 *
 */
public interface Command {
	public Object execute() throws BusinessException;
	public String getName();
}
