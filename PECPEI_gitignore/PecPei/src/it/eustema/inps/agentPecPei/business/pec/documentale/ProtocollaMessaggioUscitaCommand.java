package it.eustema.inps.agentPecPei.business.pec.documentale;


import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPElement;

import org.apache.axis.message.SOAPHeaderElement;
import org.apache.commons.lang3.StringEscapeUtils;

import sun.misc.BASE64Encoder;

import wspia2013.InteropUscitaResponse2013;
import wspia2013.Service1;
import wspia2013.Service1Locator;
import wspia2013.Service1Soap;
import wspia2013.Service1SoapStub;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.StaticUtil;

/**
 * Classe Command che si occupa di protocollare in uscita il messaggio in input
 * e restituire un'istanza della SegnaturaDTO 
 * @author ALEANZI
 * 
 */
public class ProtocollaMessaggioUscitaCommand implements Command {

	private MessaggioDTO messaggio;
	
	public ProtocollaMessaggioUscitaCommand( MessaggioDTO messaggio ){
		this.messaggio = messaggio;
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		boolean interoperabilita = false;
		
		AllegatiDAO allegatoDao = new AllegatiDAO();
		MessaggiDAO messaggioDao = new MessaggiDAO();
		//genera l'allegato txt che contiene il riepilogo di tutti i dati inviati nel messaggio
		AllegatoDTO documentoPrimario = messaggioDao.creaMessageTxt(messaggio);
		
		//istanzia le classi costruite in base al wsdl per richiamare i metodi
		//esposti nel web service wspia 
		Service1 s1 = new Service1Locator();
		Service1Soap serviziWspia ;
		try{
			serviziWspia = s1.getService1Soap();
		}catch(ServiceException se){
			throw new NotifyMailBusinessException(-6002, "Errore nella chiamata al  il servizio Protocollazione Dettaglio Errore: " + se.getMessage());
		}
		
		//listaFile contiene il txt con il riepilogo del messaggio e altri eventuali allegati
		ArrayList<AllegatoDTO> listaFile = new ArrayList<AllegatoDTO>();
		
		//aggiunge il file txt
		listaFile.add(documentoPrimario);
		if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
			//di seguito aggiunge la lista di eventuali allegati al messaggio
			listaFile.addAll(messaggio.getElencoFileAllegati());		
		}
		BASE64Encoder encoder = new BASE64Encoder();
		String[] aos = new String[listaFile.size()];
		
		//scorre tutti i file
		for(int j=0;j<listaFile.size();j++){
			//chiama l'encode Buffer per trasformare l'array di byte del singolo allegato
			//in una stringa codificata in base64. La stringa viene aggiunta alla j-esima posizione
			//dell'array
			aos[j] = encoder.encodeBuffer(listaFile.get(j).getFileData());
		}
		
		//genera l'xml per la protocollazione
		String xml =generaXML(messaggio, null);
		
		//chiamata al wspia per la protocollazione del messaggio contenente gli allegati
		String result = "";
		InteropUscitaResponse2013 result2013 = null;
		int nTry = 0;
		while ( nTry < ConfigProp.numeroTentativiProtocollazione  ) {
			try {
				nTry++;
				serviziWspia = s1.getService1Soap();
				CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
				
				if ("messaggio-pei".equalsIgnoreCase( messaggio.getTipologia() )){
					result = serviziWspia.protocollaConImmagineBlob64(ConfigProp.codAPP, ConfigProp.codAMM, messaggio.getCodiceAOO(), ConfigProp.codiceUtente, xml.replaceAll("&", "e"), aos );
				}else{
					try{
						// vecchio metodo
//						result = serviziWspia.interopUscitaBlob64(ConfigProp.codAPP, ConfigProp.codAMM,  messaggio.getCodiceAOO(),  ConfigProp.codiceUtente, messaggio.getAccountPec()+ConfigProp.dominioPEC ,xml , aos);
						
						// nuovo metodo
						Service1SoapStub proxy = new Service1SoapStub();
						SOAPHeaderElement header = new SOAPHeaderElement("http://www.inps.it", "Header");
						SOAPElement nodeName = header.addChildElement("AppName");
						nodeName.addTextNode(ConfigProp.codAPP);
						SOAPElement nodeKey = header.addChildElement("UserId");
						nodeKey.addTextNode(ConfigProp.codiceUtente);
						proxy.setHeader(header);
						result2013 = proxy.interopUscita2013(ConfigProp.codAMM,  messaggio.getCodiceAOO(),  messaggio.getAccountPec()+ConfigProp.dominioPEC ,xml , aos);
						
					}catch(Exception e){
						executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -5677, "Errore nella interopUscitaBlob64 del messaggio [" + messaggio.getMessageId() + "] in uscita dal DB. Dettaglio Errore: codice: "+e.getMessage())));
					}finally{
						if(result.equals("") && result2013==null){
							executor.setCommand( new BuildSegnaturaInterDTOCommand(result));
							SegnaturaDTO seg = (SegnaturaDTO)executor.executeCommand();
							if(!seg.isOK()){
								result = serviziWspia.protocollaConImmagineBlob64(ConfigProp.codAPP, ConfigProp.codAMM, messaggio.getCodiceAOO(), ConfigProp.codiceUtente, xml.replaceAll("&", "e"), aos );
							}
						}else interoperabilita = true;
					}
				}
				
				if(interoperabilita){
					if(result.equalsIgnoreCase(""))
						executor.setCommand( new BuildSegnaturaInterDTOCommand(result2013));
					else
						executor.setCommand( new BuildSegnaturaInterDTOCommand(result));
				}else
					executor.setCommand( new BuildSegnaturaDTOCommand( result ) );
				
				SegnaturaDTO seg = (SegnaturaDTO)executor.executeCommand();
				
				if ( !seg.isOK() && ConfigProp.codiceErroreClassificaInesistente.equals( seg.getCodiceErrore() )){
					if ( !"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
						xml = generaXML(messaggio,ConfigProp.classificaDefaultPEC);
					} else {
						xml = generaXML(messaggio,ConfigProp.classificaDefaultPEI);	
					}
				}
				/**** Caricamento allegati Base 64 ****/
				if(interoperabilita && seg.isOK()){
//					String[] allegatiB64 = new String[messaggio.getElencoFileAllegati().size()];
//					int ia = 0;
//					for(AllegatoDTO a:messaggio.getElencoFileAllegati()){
//						allegatiB64[ia]=encoder.encode(a.getFileData());
//						ia++;
//					}
					AllegatoDTO allegato = new AllegatoDTO();
					allegato.setContentType("xml");
					allegato.setFileData(result2013.getSegnaturaXML().toString().getBytes());
					allegato.setFileName("Segnatura.xml");
					allegato.setFileSize(result2013.getSegnaturaXML().toString().length());
					allegato.setMessageID(messaggio.getMessageId());
					allegato.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
					if(!result.equals(""))
						allegato.setTestoAllegato(result.toString());
					else
						allegato.setTestoAllegato(result2013.getSegnaturaXML());
					allegato.setTipoAllegato("XML");
					allegatoDao.insertAllegato(messaggio,allegato);
					messaggio.getElencoFileAllegati().add(allegato);
//					executor = CommandExecutorManager.getInstance().getExecutor();
//					executor.setCommand( new BuildSegnaturaDTOCommand( result ) );
//					seg = (SegnaturaDTO)executor.executeCommand();
				}

				seg.setXmlInput( xml );
				return seg;
				
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				if ( nTry >= ConfigProp.numeroTentativiProtocollazione )
					throw new NotifyMailBusinessException(-6002, "Errore nella chiamata al  il servizio protocollaConImmagineBlob64 Dettaglio Errore: " + e.getMessage());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				if ( nTry >= ConfigProp.numeroTentativiProtocollazione )
					throw new NotifyMailBusinessException(-6002, "Errore nella chiamata al  il servizio protocollaConImmagineBlob64 Dettaglio Errore: " + e.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				if ( nTry >= ConfigProp.numeroTentativiProtocollazione )
					throw new NotifyMailBusinessException(-6002, "Errore nella chiamata al  il servizio protocollaConImmagineBlob64 Dettaglio Errore: " + e.getMessage());
			}
			
		}
		
		return null;
		
	}
	
	
	private String generaXML(MessaggioDTO messaggio, String classificaForce) {
		
		String codiceSede = messaggio.getCodiceAOO();
		String nominativo = "INPS\\" + codiceSede + "\\" + messaggio.getNomeSede();
	
		if(messaggio.getNomeUfficio()!=null && !messaggio.getNomeUfficio().equals("")){
			nominativo += "\\" + messaggio.getNomeUfficio().replaceAll("&", "&amp;").
									replaceAll("/","-").
									replace(":"," ").
									replaceAll("�", "a").
									replaceAll("�", "u").
									replaceAll("�", "e").
									replaceAll("�","i");
		}
		if(messaggio.getFromPhrase()!=null && !messaggio.getFromPhrase().equals("")){
			nominativo += "\\" + messaggio.getFromPhrase().replaceAll("&", "&amp;").
									replaceAll("/","-").
									replace(":"," ").
									replaceAll("�", "a").
									replaceAll("�", "u").
									replaceAll("�", "e").
									replaceAll("�","i");
		}
		
		String sendTo = null;
		String descSedeTo = null;
		String classifica = classificaForce != null ? classificaForce :  messaggio.getClassifica();
		
		if(classifica==null || classifica.equals("") || classifica.equals("0")){
			//se non � stato selezionato nessun valore prende il valore di default: 800.010 o 800.020
			if (!"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() ))
				classifica = ConfigProp.classificaDefaultPEC;
			else 
				classifica = ConfigProp.classificaDefaultPEI;
		}else{
			//da specifica calcola la stringa da passare nell'xml, in base alla tipologia selezionata
			//classifica = "810.0"+classifica+"0";
		}
		
		String subject = StaticUtil.removeAllSpecialLecter( messaggio.getSubject() );
		subject = StringEscapeUtils.escapeXml(subject);
		
		String dataArrivo = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
		String nomeFile = null;
		String oggettoDoc = null;
		
		StringBuffer xml = new StringBuffer("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
		xml.append("<DatiInvio>");
		xml.append("<CategoriaProtocollo>U</CategoriaProtocollo>");
		xml.append("<Mittente><Tipo>M</Tipo>");
		xml.append("<Codice>").append(codiceSede).append("</Codice>");
		xml.append("<Nominativo>").append(nominativo).append("</Nominativo>");
		xml.append("</Mittente>");
		
		List<DestinatarioDTO> destinatari = new ArrayList<DestinatarioDTO>();
		if ( messaggio.getElencoDestinatariPer() != null ){
			destinatari.addAll( messaggio.getElencoDestinatariPer() );
		}
		xml.append("<ListaDestinatari>");
		
		if(!destinatari.isEmpty()){
			//aggiunge l'elenco dei destinatari
			
			for(int i=0;i<destinatari.size();i++){
				
				if(messaggio.getConfermaRicezione().equalsIgnoreCase("Si nella casella PEC") || messaggio.getConfermaRicezione().equalsIgnoreCase("Si nella mia casella"))
					xml.append("<Destinatario confermaRicezione=\"si\">");
				else
					xml.append("<Destinatario confermaRicezione=\""+messaggio.getConfermaRicezione()+"\">");
				
				xml.append("<Tipo>M</Tipo>");
				sendTo = destinatari.get(i).getCodiceAOO();//Verificare
				descSedeTo = destinatari.get(i).getAddr();
				if ( descSedeTo == null ) 
					descSedeTo = "";
				if(descSedeTo.indexOf("/")!=-1){
					descSedeTo = descSedeTo.substring(descSedeTo.indexOf("/"));
				}
				if ( !"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
					if ( destinatari.get(i).getPhrase() != null &&
							!destinatari.get(i).getPhrase().trim().isEmpty()
							&& destinatari.get(i).getPhrase().indexOf("@") == -1
							&& destinatari.get(i).getPhrase().indexOf("\"") == -1
							&& destinatari.get(i).getPhrase().indexOf("<") == -1
							)
						descSedeTo = destinatari.get(i).getPhrase().trim();
				} 
				
				if(descSedeTo.indexOf("@")!=-1){
					descSedeTo = descSedeTo.substring(0, descSedeTo.indexOf("@"));
				}
				if ( !"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
					xml.append("<Nominativo>").append(StaticUtil.removeAllSpecialLecter( destinatari.get(i).getAddr()) ).append("</Nominativo>");
					xml.append("<EMail>").append( StaticUtil.removeAllSpecialLecter(destinatari.get(i).getAddr()) ).append("</EMail>");
					xml.append("<Codice>INPS."+sendTo+"</Codice>");
				}else{
					xml.append("<Nominativo>").append(descSedeTo).append("</Nominativo>");
					xml.append("<Codice>INPS."+sendTo+"</Codice>");
				}
				// TODO Controllare se email letta correttamente
//				xml.append("<EMail>").append(destinatari.get(i).getAddr()).append("</EMail>");
				xml.append("</Destinatario>");
			}
		}
			
			destinatari = new ArrayList<DestinatarioDTO>();
			if ( messaggio.getElencoDestinatariCc() != null ){
				destinatari.addAll( messaggio.getElencoDestinatariCc() );
			}
			
			if(!destinatari.isEmpty()){
				//aggiunge l'elenco dei destinatari
				
				for(int i=0;i<destinatari.size();i++){
					if(messaggio.getConfermaRicezione().equalsIgnoreCase("Si nella casella PEC") || messaggio.getConfermaRicezione().equalsIgnoreCase("Si nella mia casella"))
						xml.append("<Destinatario confermaRicezione=\"Si\" TipoInvio=\"CC\">");
					else
						xml.append("<Destinatario confermaRicezione=\""+messaggio.getConfermaRicezione()+"\" TipoInvio=\"CC\">");

					xml.append("<Tipo>M</Tipo>");
					sendTo = destinatari.get(i).getCodiceAOO();//Verificare
					descSedeTo = destinatari.get(i).getName();
					if ( descSedeTo == null ) 
						descSedeTo = "";
					if(descSedeTo.indexOf("/")!=-1){
						descSedeTo = descSedeTo.substring(descSedeTo.indexOf("/"));
					}
					
					xml.append("<Codice>INPS.").append(sendTo).append("</Codice>");
					xml.append("<Nominativo>INPS\\").append(descSedeTo).append("</Nominativo>");
					// TODO Controllare se email letta correttamente
					xml.append("<EMail>").append(destinatari.get(i).getAddr()).append("</EMail>");
					xml.append("</Destinatario>");
				}
		}
		xml.append("</ListaDestinatari>");
		xml.append("<ListaDocumenti>");
		
		if ("posta-certificata".equalsIgnoreCase( messaggio.getTipoMessaggio() ))
			xml.append("<Documento NomeFile=\""+StaticUtil.subString( messaggio.getDocumentoPrincipale(), 247 ).replaceAll("&", "e").replaceAll("�", "a").replaceAll("�", "e").replaceAll("�", "e").replaceAll("�", "o").replaceAll(":", "")+"\">");
		else 
			xml.append("<Documento NomeFile=\"TestoDelMessaggio.txt\">");
		
		
		xml.append("<Sensibile>"+messaggio.getSensibile()+"</Sensibile>");
		xml.append("<Riservato>"+messaggio.getRiservato()+"</Riservato>");
		xml.append("<FlagProtocollo>S</FlagProtocollo>");
		//classifica, se vuoto mette classifica="010.010"
		xml.append("<Classifica>").append(classifica).append("</Classifica>");
		xml.append("<Oggetto>").append(subject).append("</Oggetto>");
		xml.append("<ProtocolloMittente Data=\"\" Protocollo=\"\"/>");
		xml.append("<DataArrivo>").append(dataArrivo).append("</DataArrivo>");
//		xml.append("<ListaSoggettiAfferenti>").append("</ListaSoggettiAfferenti>");
		if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
			xml.append("<ListaAllegati>");
			for(int i=0;i<messaggio.getElencoFileAllegati().size();i++){
				nomeFile = StaticUtil.removeAllSpecialChar( StaticUtil.subString( messaggio.getElencoFileAllegati().get(i).getFileName().replaceAll(":", ""), 247 ));
				oggettoDoc = nomeFile + "(" + messaggio.getElencoFileAllegati().get(i).getFileSize() + 
					" - " + nomeFile + ")";
				xml.append("<Documento NomeFile=\"").append(nomeFile).append("\">");
				xml.append("<Oggetto>").append(oggettoDoc).append("</Oggetto>");
				xml.append("</Documento>");
			}
			xml.append("</ListaAllegati>");
		}/*else{
			xml.append("<ListaAllegati>");
			nomeFile = "TestoDelMessaggio.txt";
			oggettoDoc = "TestoDelMessaggio.txt";
			xml.append("<Documento NomeFile=\"").append(nomeFile).append("\">");
			xml.append("<Oggetto>").append(oggettoDoc).append("</Oggetto>");
			xml.append("</Documento>");
			xml.append("</ListaAllegati>");
		}*/
		xml.append("</Documento>");
		xml.append("</ListaDocumenti>");
		xml.append("</DatiInvio>");
		return xml.toString();
	}
	
	
	
	
	//genera il contenuto del file di testo da allegare come riepilogo
	//del messaggio che si sta inviando. Restituisce un oggetto Allegato
	private AllegatoDTO generaTxt(MessaggioDTO messaggio) {
		AllegatoDTO allegato = new AllegatoDTO();
		String codiceSede = messaggio.getCodiceAOO();
		
		String nominativo = "INPS\\" + codiceSede + "\\" + messaggio.getNomeSede();
		
		if(messaggio.getNomeUfficio()!=null && !messaggio.getNomeUfficio().equals("")){
			nominativo += "\\" + messaggio.getNomeUfficio();
		}
		nominativo += "\\" + messaggio.getFromPhrase();
		
		
		StringBuffer testoRiepilogo = new StringBuffer("HERMES-Messaggistica Ufficiale\n\n");
		testoRiepilogo.append("Mittente: INPS.").append(codiceSede).append("\n\n");
		testoRiepilogo.append(nominativo).append("\n\n");
		//testoRiepilogo.append("Destinatari: ").append(messaggio.getRiepilogoDestinatari()).append("\n\n");
		testoRiepilogo.append("Destinatari: ").append("\n\n");
		
		
		List<DestinatarioDTO> destinatari = new ArrayList<DestinatarioDTO>();
		if ( messaggio.getElencoDestinatariPer() != null ){
			destinatari.addAll( messaggio.getElencoDestinatariPer() );
		}
		if ( messaggio.getElencoDestinatariCc() != null ){
			destinatari.addAll( messaggio.getElencoDestinatariCc() );
		}
		
		if(!destinatari.isEmpty()){
			String dest = "";
			for(int i=0;i<destinatari.size();i++){
				dest = destinatari.get(i).getName();
				testoRiepilogo.append(dest).append("\n");
			}
		}
		testoRiepilogo.append("\n\n\n");
		testoRiepilogo.append("OGGETTO: ").append(messaggio.getSubject()).append("\n\n");
		testoRiepilogo.append("TESTO\n\n");
		testoRiepilogo.append(messaggio.getBody()).append("\n\n");
		
		if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
			testoRiepilogo.append("Sono presenti i seguenti allegati:\n\n");
			String nomeFile="";
			String oggettoDoc="";
			for(int i=0;i<messaggio.getElencoFileAllegati().size();i++){
				nomeFile = StaticUtil.subString( messaggio.getElencoFileAllegati().get(i).getFileName().replaceAll(":", ""), 247 );
				oggettoDoc = nomeFile + "(" + messaggio.getElencoFileAllegati().get(i).getFileSize() + 
					" - " + nomeFile + ")";
				
				testoRiepilogo.append("[").append(oggettoDoc).append("]\n");
			}
		}
		
		
		
		allegato.setFileName("msghermes.txt");
		//converte in array di byte la stringa con i contenuti del messaggio
		allegato.setFileData((testoRiepilogo.toString()).getBytes());
		
		allegato.setContentType("text/html");
		
		return allegato;
	}
	
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "WSPIA Messaggio in Uscita";
	}
	
	

}
