package it.eustema.inps.agentPecPei.business.util;

import java.util.StringTokenizer;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;

/**
 * Recupera l'indirizzo pec per valorizzare la colonna ReplyTO, dal campo FROM.
 * @author ALEANZI
 *
 */
public class RetrieveReplyToFromFromCommand implements Command {

	private String from;
	
	public RetrieveReplyToFromFromCommand( String from ){
		this.from = from;
	}
	
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		if ( from.indexOf("@") != -1 ){
			StringTokenizer tok = new StringTokenizer( from, "@" );
			return tok.nextToken();
		}
		return "";
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Recupero del campo ReplyTO";
	}

}
