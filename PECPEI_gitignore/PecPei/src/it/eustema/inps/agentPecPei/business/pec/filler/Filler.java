package it.eustema.inps.agentPecPei.business.pec.filler;

import it.eustema.inps.agentPecPei.exception.BusinessException;

/**
 * Filler generico. Un filler si occupa di valorizzare i campi di un oggetto.
 * @author ALEANZI
 *
 */
public interface Filler {
	public void fill() throws BusinessException ;
}
