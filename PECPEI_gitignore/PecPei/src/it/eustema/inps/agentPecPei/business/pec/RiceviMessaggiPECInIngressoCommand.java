package it.eustema.inps.agentPecPei.business.pec;

import java.util.List;
import java.util.concurrent.Callable;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.mail.ReadAndElabMessageCommand;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;

public class RiceviMessaggiPECInIngressoCommand implements Command, Callable<Object> {

	private String codAOO;
	
	public RiceviMessaggiPECInIngressoCommand( String codAOO ){
		this.codAOO = codAOO;
	}
	@Override
	public Object execute() throws BusinessException {
//		VerificaDatabaseDao vd = new VerificaDatabaseDao();
//		ConfigProp.switchDbPecPei = vd.getFreeSpace();
		System.out.println("RiceviMessaggiPECInIngressoCommand :: execute() :: Recupero tutti gli account");
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		List<AccountPecDTO> listaAccountPEC = null;
		//Lettura di tutti gli account PEC attivi e non temporaneamente disabilitati
		AccountPecDAO accountPecDAO = new AccountPecDAO();
		try {
			System.out.println("InviaMessaggiPECInUscitaCommand :: try() :: Sono nel try-catch per recuperare gli account PEC Attivi");
			listaAccountPEC = accountPecDAO.selectAllAccountPecAttivi( codAOO );
		} catch (DAOException e) {
			throw new NotifyMailBusinessException( -1001, "Errore nel recupero di tutti gli account PEC da DB. DettaglioErrore: " + e.getMessage() );
		} 
		
		for ( AccountPecDTO accountPEC : listaAccountPEC ){
			try {
				System.out.println("RiceviMessaggiPECInIngressoCommand :: try() :: Sono nel try-catch per scorrere la lista di tutti gli account PEC");
				System.out.println("RiceviMessaggiPECInIngressoCommand :: executor.setCommand( new ReadAndElabMessageCommand( accountPEC ) :: Creo l'oggetto di tipo ReadAndElabMessageCommand");
				executor.setCommand( new ReadAndElabMessageCommand( accountPEC ) );
				executor.executeCommandAndLog();
			} catch ( BusinessException e){
				//Qualsiasi Errore, si passa all'elaborazione dell'account successivo
			}catch (Exception e) {
				throw new NotifyMailBusinessException( -1001, "Errore nel recupero di tutti gli account PEC da DB. DettaglioErrore: " + e.getMessage() );
//				if(e.getMessage().toLowerCase().contains("disk space".toLowerCase()) && i==0){
//				ConfigProp.switchDbPecPei = true;
//				continue;
//			}
			}
		}

		return new Integer(1);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "RiceviMessaggiPECInIngresso";
	}
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
