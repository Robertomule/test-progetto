package it.eustema.inps.agentPecPei.business.pec.filler;

import it.eustema.inps.agentPecPei.business.util.RetrieveUiDocFromMessageIDCommand;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;

import com.pff.PSTMessage;

/**
 * Filler di MessaggioDTO. Valorizza tutti i campi del messaggioDTO che devono essere 
 * valorizzati a partire da un HEADER di un messaggio letto da PST
 * @author ALEANZI
 *
 */
public class PSTMessaggioDTOHeaderFiller extends
		AbstractMessaggioDTOFiller {

	private PSTMessage mail;
	
	
	public void setPSTMessage( PSTMessage mail ){
		this.mail = mail;
	}
	
	@Override
	public void fill() throws BusinessException {
		// TODO Auto-generated method stub
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		
		//HEADERS
		getMessaggioDTO().setHeaders( mail.getTransportMessageHeaders() );
		
		//MESSAGEID
		getMessaggioDTO().setMessageId( mail.getInternetMessageId() );
		
		//UIDOC
		executor.setCommand( new RetrieveUiDocFromMessageIDCommand( mail.getInternetMessageId() ) );
		getMessaggioDTO().setUiDoc( (String)executor.executeCommand() );
		
		//FROMADDR
		getMessaggioDTO().setFromAddr(mail.getSenderEmailAddress().trim());
		
		
	}

}
