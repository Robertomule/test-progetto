package it.eustema.inps.agentPecPei.business.pec.pst;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileRoller {

	private PrintWriter writer = null;
	
	public FileRoller( String path, String suffisso  ){
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy   hh.mm");
		
		try {
			writer = new PrintWriter(path + sdf.format( new Date() ) + "_" + suffisso, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void roll(  String text){
		writer.println( text );
	}
	
	public void stopRoll(){
		
		writer.close();
	}
}
