package it.eustema.inps.agentPecPei.business.pec.documentale;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import wspia2013.InteropEntrataResponse2013;

public class BuildSegnaturaInterConfermaDTOCommand implements Command {
	private String xml; 
	private InteropEntrataResponse2013 interopEntrata2013;
	
	public BuildSegnaturaInterConfermaDTOCommand( String xml ){
		this.xml = xml;
	}
	public BuildSegnaturaInterConfermaDTOCommand( InteropEntrataResponse2013 interopEntrata2013 ){
		this.interopEntrata2013 = interopEntrata2013;
	}
	
	@Override
	public Object execute() throws BusinessException {
		
		// TODO Auto-generated method stub
//		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		SimpleDateFormat dt = new SimpleDateFormat("dd/mm/yyyy"); 
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyy-mm-dd");
		
	        /*SAXParser saxParser = saxParserFactory.newSAXParser();
	        SegnaturaParserHandler handler = new SegnaturaParserHandler();
	        saxParser.parse( new ByteArrayInputStream( xml.getBytes("UTF-8") ), handler );
	        SegnaturaDTO out = handler.getSegnaturaDTO();*/
			SegnaturaDTO out = null;
	        
	        // setup object mapper using the AppConfig class
	        JAXBContext context;
			try {
				 if(xml!=null && !xml.equalsIgnoreCase("")){
					if ( xml.indexOf("CodiceErrore") == -1 ){
						context = JAXBContext.newInstance(generated.vo.wspia.inperoperabilita.conferma.ConfermaRicezione.class);
					 // parse the XML and return an instance of the AppConfig class
						generated.vo.wspia.inperoperabilita.conferma.ConfermaRicezione conf = (generated.vo.wspia.inperoperabilita.conferma.ConfermaRicezione) context.createUnmarshaller().unmarshal(new ByteArrayInputStream( xml.getBytes("UTF-8") ));
						out = new SegnaturaDTO();
						out.setStatus( SegnaturaDTO.OK );
						Date dataSegnatura = dt1.parse(conf.getIdentificatore().getDataRegistrazione().toString());
						String segnatura = conf.getIdentificatore().getCodiceAmministrazione()+"."+
											conf.getIdentificatore().getCodiceAOO()+"."+
											dt.format(dataSegnatura)+"."+
											conf.getIdentificatore().getNumeroRegistrazione();
						out.setSegnatura(segnatura);
					} else {
						context = JAXBContext.newInstance(generated.vo.wspia.errore.ServizioProtocollo.class);
						 // parse the XML and return an instance of the AppConfig class
				        generated.vo.wspia.errore.ServizioProtocollo sp = (generated.vo.wspia.errore.ServizioProtocollo) context.createUnmarshaller().unmarshal(new ByteArrayInputStream( xml.getBytes("UTF-8") ));
				        out = new SegnaturaDTO();
				        out.setStatus( SegnaturaDTO.KO );
				        out.setCodiceErrore(sp.getCodiceErrore());
				        out.setDescErrore(sp.getDescrizioneErrore());
					}
				}else{
						if(interopEntrata2013.getCodice().getValue().equalsIgnoreCase("OK")){
							context = JAXBContext.newInstance(generated.vo.wspia.inperoperabilita.conferma.ConfermaRicezione.class);
						 // parse the XML and return an instance of the AppConfig class
							generated.vo.wspia.inperoperabilita.conferma.ConfermaRicezione conf = (generated.vo.wspia.inperoperabilita.conferma.ConfermaRicezione) context.createUnmarshaller().unmarshal(new ByteArrayInputStream( interopEntrata2013.getConfermaRicezione().getBytes("UTF-8") ));
							out = new SegnaturaDTO();
							out.setStatus( SegnaturaDTO.OK );
							Date dataSegnatura = dt1.parse(conf.getIdentificatore().getDataRegistrazione().toString());
							String segnatura = conf.getIdentificatore().getCodiceAmministrazione()+"."+
												conf.getIdentificatore().getCodiceAOO()+"."+
												dt.format(dataSegnatura)+"."+
												conf.getIdentificatore().getNumeroRegistrazione();
							out.setSegnatura(segnatura);
						} else {
							 // parse the XML and return an instance of the AppConfig class
					        out = new SegnaturaDTO();
					        out.setStatus( SegnaturaDTO.KO );
					        out.setCodiceErrore(interopEntrata2013.getCodice().getValue());
					        out.setDescErrore(interopEntrata2013.getNotificaEccezione());
					}
				}
			} catch (JAXBException e) {
					//throw new BusinessException(-50000, e.getMessage()  );
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				//throw new BusinessException(-50000, e.getMessage()  );
			} catch (ParseException e) {
				// TODO Blocco catch generato automaticamente
			} 
	        
	        if ( out == null ){
	        	out = new SegnaturaDTO();
	        	out.setStatus(SegnaturaDTO.KO);
	        	out.setCodiceErrore( "-1" );
	        	out.setDescErrore("Errore non censito dal servizio del protocollo");
	        }
	        return out;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "xml -> SegnaturaDTO";
	}
}
