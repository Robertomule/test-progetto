package it.eustema.inps.agentPecPei.business.pec.mail;

import java.io.Reader;
import java.io.StringReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import org.apache.log4j.Logger;
import org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Enumeration.EnStatoConsegnaRichiestaPECInUscita;
import org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Enumeration.EnStatoInvioRichiestaPECInUscita;
import org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Responses.RegistraStatoConsegnaPECInUscitaResponse;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.tempuri.DNA_BASICHTTP_BindingStub;

import wsPciClient.org.tempuri.WsOUTComunicaRispostePEC;
import wsPciClient.org.tempuri.WsPCItoSGDSoapStub;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.builder.MessageMessaggioDTOBuilder;
import it.eustema.inps.agentPecPei.business.pec.documentale.DocumentStore;
import it.eustema.inps.agentPecPei.business.pec.documentale.EsitoStoreAllegati;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommand;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommandNew;
import it.eustema.inps.agentPecPei.business.util.SendMailCasella;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dao.EsitiNotificheSgdDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.EsitoNotificaSgdDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.invioNotificheMailInterop.GestioneNotificheInterBean;
import it.eustema.inps.agentPecPei.invioNotificheMailInterop.InvioMailInteropCommand;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;
import it.eustema.inps.agentPecPei.notifichePciSGD.InvioNotificaSGDCallable;
import it.eustema.inps.agentPecPei.notifichePciSGD.ProtocolloPciToSgdSingleton;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.agentPecPei.util.CrittografiaPasswordPEC;
import it.eustema.inps.agentPecPei.util.DebugConstants;
import it.eustema.inps.agentPecPei.util.TripleDES;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.logging.LoggingConfigurator;
import it.inps.soa.WS00732.IWSProtocolloProxy;
import it.inps.soa.WS00732.data.DocumentoPEC;
import it.inps.soa.WS00732.data.RegistraStatoConsegnaPECInUscitaRequest;
import it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaRequest;
import it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse;

public class ReadAndElabMessageCommand implements Command{

	private AccountPecDTO accountPecDTO;
	private static Logger log = Logger.getLogger(ReadAndElabMessageCommand.class);
	private Future<Object> resultSGD = null; //utilizzato per la notifica SGD
	//private WsPCItoSGDSoapStub stub;
	private WsOUTComunicaRispostePEC outRispostaPec; 
	
	public ReadAndElabMessageCommand( AccountPecDTO accountPecDTO ){
		this.accountPecDTO = accountPecDTO;
		//stub = ProtocolloPciToSgdSingleton.getInstance().proxy.getProxyHelper();
	}
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		System.out.println("ReadAndElabMessageCommand :: execute() :: Leggo ed Elaboro il messaggio");
		TripleDES u = new TripleDES();
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		
		MessaggiDAO messaggiDAO = new MessaggiDAO();
		AccountPecDAO accountPecDao = new AccountPecDAO();
		EsitiNotificheSgdDAO esitiNotificheSgdDAO = new EsitiNotificheSgdDAO();
		
		IWSProtocolloProxy wsProtocollo = new IWSProtocolloProxy();
		DNA_BASICHTTP_BindingStub proxyProtocollo = wsProtocollo.getProxyHelper();//Chiamata con datapower
		//DNA_BASICHTTP_BindingStub proxyProtocollo = wsProtocollo.getProxyHelperNoDataPower(); //Chiamata no datapower
		
		Properties props = new Properties();
	    props.put("mail.host", accountPecDTO.getHostPop() );
	    props.put("mail.store.protocol", accountPecDTO.getStoreProtocolPop() );
	    props.put("mail.pop3s.port", accountPecDTO.getPortPop() );
	    props.put("mail.pop3.ssl.trust",accountPecDTO.getSslTrustPop() ); // or "*"
//		Gestione IMAPS
//	    props.put("mail.imaps.port", accountPecDTO.getPortPop() );
//	    props.put("mail.imaps.ssl.trust",accountPecDTO.getSslTrustPop() ); // or "*"
	    Session session = Session.getInstance(props);
//	    session.setDebug(true);
	    
        Store store;
		try {
			store = session.getStore();
			
			if ( DebugConstants.ENABLE_DEBUG_CONSTANTS )
				try{
					System.out.println("llll");
					store.connect( DebugConstants.ACCOUNT_PEC_USER_NAME, u.decrypt(DebugConstants.ACCOUNT_PEC_USER_PASSWORD));
				}catch(Exception e){
					executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6051, "Errore generico nella decifratura della password. Dettaglio Errore: " + e.getMessage() ) ) );
		    		executor.executeCommand();
				}
			else{
				System.out.println("ReadAndElabMessageCommand :: execute() :: Mi trovo all'interno dell'else contenuto nel try-catch ");
//				if(accountPecDTO.getUserName().equals("direzione.provinciale.messina"))
				if(ConfigProp.indirizziPerTest.contains( accountPecDTO.getUserName()))
					store.connect( accountPecDTO.getUserName()+ConfigProp.dominioPECTest, accountPecDTO.getPassword() );
				else
					store.connect( accountPecDTO.getUserName()+ConfigProp.dominioPEC, accountPecDTO.getPassword() );
				}
//		Gestione Anomalie
//		int messageCount = 0;//inbox.getMessageCount();
//		Folder inbox =null;
//		if(accountPecDTO.getUserName().equals("direzione.provinciale.messina")){
//			Folder[] f = store.getFolder("INBOX").list();
//			inbox = store.getFolder("inbox");
//			if(accountPecDTO.getUserName().equals("direzione.provinciale.messina")){
//				for(Folder s:f){
//					System.out.println("Nome: "+s.getName());
//					if(s.getName().equals("Bozze"))
//						inbox = s;
//				}
//			    inbox.open(Folder.READ_WRITE); // Folder.READ_ONLY
//			}
//			messageCount = inbox.getMessageCount();
//		}else{	
//			messageCount = 0;
//		}

		Folder inbox = store.getFolder("inbox");
		inbox.open(Folder.READ_WRITE); // Folder.READ_ONLY
		int messageCount = inbox.getMessageCount();
		
	    for ( int i=1; i<=messageCount; i++ ){
	    	MessaggioDTO messaggioInEntrata = new MessaggioDTO();
	    	try{
		    	Message message = inbox.getMessage(i);
//		    	String messageId = MessageMessaggioDTOBuilder.readHeaders("Message-ID", message.getAllHeaders()) == null ? null : MessageMessaggioDTOBuilder.readHeaders("Message-ID",message.getAllHeaders()).get(0);
		    	System.out.println("ReadAndElabMessageCommand :: execute() :: Creo un oggetto di tipo 'MessageMessaggioDTOBuilder'");
		    	executor.setCommand( new MessageMessaggioDTOBuilder( message, accountPecDTO, Constants.VERSO_MSG_ENTRANTE ) );
		    	messaggioInEntrata = (MessaggioDTO) executor.executeCommand();
		    	
		    	if ( "E".equalsIgnoreCase( messaggioInEntrata.getVerso() ) && "dpl.salerno@mailcert.lavoro.gov.it".equalsIgnoreCase( messaggioInEntrata.getReplyTo() )){
		    		for ( AllegatoDTO a: messaggioInEntrata.getElencoFileAllegati() ){
		    			if ( "postacert.eml".equalsIgnoreCase( a.getFileName() ) ){
		    				a.setFileName("allegato.eml");
		    				log.info("Messaggio pec in Entrata Dalla DPL.SALERNO ******* : "+messaggioInEntrata.getMessageId()+" account "+messaggioInEntrata.getAccountPec()+" sulla sede "+messaggioInEntrata.getCodiceAOO() + " RINOMINATO ALLEGATO POSTACERT.EML IN ALLEGATO.EML");
		    				break;
		    			}
		    		}
		    	}

		    	//Verifica Correttezza messaggio
		    	executor.setCommand( new CheckCorrettezzaMessaggioCommand( messaggioInEntrata ) );
		    	CheckCorrettezzaMessaggioCommand.Out checkOut = (CheckCorrettezzaMessaggioCommand.Out) executor.executeCommand();
		    	log.info("Messaggio pec in Entrata : "+messaggioInEntrata.getMessageId()+" account "+messaggioInEntrata.getAccountPec()+" sulla sede "+messaggioInEntrata.getCodiceAOO());
		    	//Inserimento del messaggio su DB
//				messaggiDAO.doInsertDestinatari(connPecPei, messaggioInEntrata.getMessageId(), messaggioInEntrata.getElencoDestinatariPer());
//				messaggiDAO.doInsertDestinatari(connPecPei, messaggioInEntrata.getMessageId(), messaggioInEntrata.getElencoDestinatariCc());
//				messaggiDAO.doInsertAllegati(connPecPei, messaggioInEntrata.getMessageId(), messaggioInEntrata.getCodiceAOO(), messaggioInEntrata.getElencoFileAllegati() );

				SegnaturaDTO segnatura = null;
				//Valorizzazione dello stato e della segnatura
				
				if(!messaggiDAO.verificaMessaggioEntrante(messaggioInEntrata)){
					
					messaggiDAO.inserisciMessaggio(null, messaggioInEntrata );
					
					//Se � un messaggio di test lo metto nello stato TDP e passo al messaggio successivo
			    	if(ConfigProp.indirizziPerTest.contains(messaggioInEntrata.getAccountPec())){
			    		//Rimozione del messaggio dalla casella
				        message.setFlag(Flags.Flag.SEEN, true);
				        message.setFlag(Flags.Flag.DELETED, true);
			    		continue;
			    	}

			    	
					if(messaggioInEntrata.isInteroperabile()){
						
					}else{
						
						if ( checkOut == CheckCorrettezzaMessaggioCommand.Out.OK ){
				    	
							//Protocollazione in entrata
							try{
								//verifico se utilizzare il vecchio o il nuovo protocollo in base alle aoo abilitate
								if(messaggioInEntrata.getTipoMessaggio().equalsIgnoreCase("posta-certificata") && ConfigProp.listaAooAbilitate.contains(messaggioInEntrata.getCodiceAOO()))
									executor.setCommand( new ProtocollaMessaggioEntrataCommandNew( messaggioInEntrata ) ); //nuovo metodo di protocollazione
								else
									executor.setCommand( new ProtocollaMessaggioEntrataCommand( messaggioInEntrata ) );
								
								//Se si tratta di aoo abilitata al nuovo protocollo passo al prossimo messagio
								if(messaggioInEntrata.getTipoMessaggio().equalsIgnoreCase("posta-certificata") && ConfigProp.listaAooAbilitate.contains(messaggioInEntrata.getCodiceAOO())){
									executor.executeCommandAndLog();
									continue;
								}
								
								segnatura = (SegnaturaDTO)executor.executeCommandAndLog();
								
								if ( !segnatura.isOK() ){
									executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -5000, "Errore nella protocollazione del messaggio [" + messaggioInEntrata.getMessageId() + "] in entrata. Dettaglio Errore: codice: " + segnatura.getCodiceErrore() + "   descrizione : " + segnatura.getDescErrore()  + System.getProperty("line.separator") + "XML inviato: " +System.getProperty("line.separator") + System.getProperty("line.separator") + segnatura.getXmlInput() ) ) );
									executor.executeCommand();
								} else {
									
									messaggioInEntrata.setStato( Constants.STATO_MSG_RICEVUTO );
									messaggioInEntrata.setSegnatura( segnatura.getSegnatura() );
									messaggioInEntrata.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
									if(segnatura.getDocumentoPrincipale()!=null && !segnatura.getDocumentoPrincipale().equals(""))
										messaggioInEntrata.setDocumentoPrincipale(segnatura.getDocumentoPrincipale());
									else
										messaggioInEntrata.setDocumentoPrincipale("");
									
									messaggiDAO.aggiornaMsgEntranteRicevuto( null, messaggioInEntrata );
									
									/*if(messaggioInEntrata.getCanale() != null && messaggioInEntrata.getCanale().equalsIgnoreCase(ConfigProp.canaleSgd)){
										InvioNotificaSGDCallable invioNotifica = new InvioNotificaSGDCallable();
										ExecutorService executorService = Executors.newSingleThreadExecutor();
										resultSGD = executorService.submit(invioNotifica);
										
										//segnalo a PCI che la PEC SGD pervenuta � in carico a SGD
										String messageIdPecInUscita = messaggiDAO.selezionaMessaggioInUscitaSGD(messaggioInEntrata.getMsgId());
										outRispostaPec = stub.wsComunicaRispostePEC(messageIdPecInUscita,messaggioInEntrata.getMessageId(),messaggioInEntrata.getCodiceAOO(),messaggioInEntrata.getAccountPec(),messaggioInEntrata.getTipoMessaggio());
										if(outRispostaPec.getIEsito() == 0)
											log.error("Errore in notifica messaggio correlato SGD a PCI :"+outRispostaPec.getSDescrizione());
										else
											log.info("Messaggio correlato SGD notificato a PCI: "+outRispostaPec.getSDescrizione());
									}*/
									
								}
							} catch ( BusinessException be){
								executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -5000, "Errore nella protocollazione del messaggio [" + messaggioInEntrata.getMessageId() + "] in entrata. Dettaglio Errore: codice: " + be.getMessage()  )));
								executor.executeCommand();
								throw new BusinessException( -6500, "Errore nella validazione del messaggio [" + messaggioInEntrata.getMessageId() + "] in entrata. Dettaglio Errore: codice: " + checkOut.getCodice() + "   descrizione : " + checkOut.getDesc() ) ;
	
							}
						} else {
							if ( checkOut != CheckCorrettezzaMessaggioCommand.Out.CAMPO_TIPO_MESSAGGIO_RICEVUTA ){
								executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6500, "Errore nella validazione del messaggio [" + messaggioInEntrata.getMessageId() + "] in entrata. Dettaglio Errore: codice: " + checkOut.getCodice() + "   descrizione : " + checkOut.getDesc() ) ) );
								executor.executeCommand();
								throw new BusinessException( -6500, "Errore nella validazione del messaggio [" + messaggioInEntrata.getMessageId() + "] in entrata. Dettaglio Errore: codice: " + checkOut.getCodice() + "   descrizione : " + checkOut.getDesc() ) ;
							}	
						}
					}
				}		
				
				// Aggiunto per la gestione SGD - Notifiche Invio e Ricezione
				if(messaggioInEntrata.getCanale() != null && messaggioInEntrata.getCanale().equalsIgnoreCase(ConfigProp.canaleSgd)){
					
					String stato = null;
					
					String [] statiCheck = {
						"ACCETTAZIONE", 
						"Notifica di non accettazione", 
						"MANCATA CONSEGNA", 
						"CONSEGNA", 
						"AVVISO DI MANCATA CONSEGNA", 
						"Conferma di Ricezione", 
						"Notifica di Eccezione"
					};
					
					String [] statiValue = {
						"Accettazione", 
						"NonAccettazione", 
						"MancataConsegna", 
						"AvvenutaConsegna", 
						"AvvisoMancataConsegna",
						"ConfermaRicezione", 
						"NotificaEccezione"
					};
					
					for(int index = 0; index < statiCheck.length; index++){
						if(messaggioInEntrata.getSubject().contains(statiCheck[index])){
							stato = statiValue[index];
							break;
						}
					}
					
					//Se � una notifica
					if(stato != null){
						
						// Verifico se la notifica sia una di Invio o di Consegna
						if(stato.equalsIgnoreCase("Accettazione") || stato.equalsIgnoreCase("NonAccettazione")){
							
							RegistraStatoInvioPECInUscitaRequest request = new RegistraStatoInvioPECInUscitaRequest();
							
							// Imposto come MessageID quello del padre
							request.setMessageID(messaggioInEntrata.getMsgId());
							request.setStato(stato.equalsIgnoreCase("Accettazione") 
											? EnStatoInvioRichiestaPECInUscita.Accettazione 
											: EnStatoInvioRichiestaPECInUscita.NonAccettazione);
							
							// Converto gli allegati in DocumentiPEC impostando come stato quello della request
							DocumentStore documentStore = new DocumentStore();
							EsitoStoreAllegati esito = documentStore.memorizzaAllegati(messaggioInEntrata.getElencoFileAllegati(), messaggioInEntrata.getMessageId(), messaggiDAO.getSegnaturaMessaggioPrincipale(messaggioInEntrata.getMsgId()));
							
							// Se sono tutti memorizzati (Quindi con IDDocStore popolato continuo
							if(esito.isAllAllegatiMemorizzati()){

								AllegatiDAO allegatiDAO = new AllegatiDAO();
								
								// Refresho gli allegati - caricando cos� gli idDocumentStore
								messaggioInEntrata.setElencoFileAllegati(allegatiDAO.refreshAllegatiMessaggio(messaggioInEntrata.getMessageId()));
								
								// Essendo una ricevuta l'elenco dei file allegati non pu� essere vuoto
								if(messaggioInEntrata.getElencoFileAllegati().size() == 0)
									continue;
								
								String[] documentIDes = new String[messaggioInEntrata.getElencoFileAllegati().size()];
								
								for(int index = 0; index < messaggioInEntrata.getElencoFileAllegati().size(); index++){
									documentIDes[index] = messaggioInEntrata.getElencoFileAllegati().get(index).getIdDocStore();
								}
								
								request.setDocumentIDes(documentIDes);
								
								RegistraStatoInvioPECInUscitaResponse response = proxyProtocollo.registraStatoInvioPECInUscita(request);
								
								EsitoNotificaSgdDTO esitoNotificaSgd = new EsitoNotificaSgdDTO();
								esitoNotificaSgd.setMessageID(request.getMessageID());
								esitoNotificaSgd.setMetodo("RegistraStatoInvioPECInUscita");
								esitoNotificaSgd.setStato(request.getStato().toString());
								esitoNotificaSgd.setEsito(response.getCodice().toString());
								esitoNotificaSgd.setDescrizioneErrore(response.getDescrizione());
								
								esitiNotificheSgdDAO.insertEsito(esitoNotificaSgd);
								
							} else { // Altrimenti vado avanti
								continue;
							}
							
						} else {
							
							RegistraStatoConsegnaPECInUscitaRequest request = new RegistraStatoConsegnaPECInUscitaRequest();
							
							// Imposto come MessageID quello del padre
							request.setMessageID(messaggioInEntrata.getMsgId());
							
							// Vado a leggere il destinatario da daticert per la notifica
							AllegatoDTO daticert = null;
							for(AllegatoDTO allegato: messaggioInEntrata.getElencoFileAllegati())
								if(allegato.getFileName().equals("daticert.xml")){
									daticert = allegato;
									break;
								}
							
							// Imposto come AccountPec quello del destinatario letto da daticert
							SAXBuilder builder = new SAXBuilder();
							Reader in = new StringReader(daticert.getTestoAllegato());
						    Document document = builder.build(in);
						    Element intestazione = document.getRootElement().getChild("intestazione");
						    
							request.setAccountPEC(intestazione.getChild("destinatari").getText());
							
							if(stato.equalsIgnoreCase("MancataConsegna"))
								request.setStato(EnStatoConsegnaRichiestaPECInUscita.MancataConsegna);
							else if(stato.equalsIgnoreCase("AvvenutaConsegna"))
								request.setStato(EnStatoConsegnaRichiestaPECInUscita.AvvenutaConsegna);
							else if(stato.equalsIgnoreCase("AvvisoMancataConsegna"))
								request.setStato(EnStatoConsegnaRichiestaPECInUscita.AvvisoMancataConsegna);
							else if(stato.equalsIgnoreCase("ConfermaRicezione"))
								request.setStato(EnStatoConsegnaRichiestaPECInUscita.ConfermaRicezione);
							else if(stato.equalsIgnoreCase("NotificaEccezione"))
								request.setStato(EnStatoConsegnaRichiestaPECInUscita.NotificaEccezione);	
							
							// Converto gli allegati in DocumentiPEC impostando come stato quello della request
							DocumentStore documentStore = new DocumentStore();
							EsitoStoreAllegati esito = documentStore.memorizzaAllegati(messaggioInEntrata.getElencoFileAllegati(), messaggioInEntrata.getMessageId(), messaggiDAO.getSegnaturaMessaggioPrincipale(messaggioInEntrata.getMsgId()));
							
							// Se sono tutti memorizzati (Quindi con IDDocStore popolato continuo
							if(esito.isAllAllegatiMemorizzati()){

								AllegatiDAO allegatiDAO = new AllegatiDAO();
								
								// Refresho gli allegati - caricando cos� gli idDocumentStore
								messaggioInEntrata.setElencoFileAllegati(allegatiDAO.refreshAllegatiMessaggio(messaggioInEntrata.getMessageId()));
								
								List<String> allegatiTmp = new ArrayList<String>();
								for(AllegatoDTO allegato: messaggioInEntrata.getElencoFileAllegati()){
									if(allegato.getFileName().equalsIgnoreCase("daticert.xml") || allegato.getFileName().equalsIgnoreCase("smime.p7s"))
										allegatiTmp.add(allegato.getIdDocStore());
								}
								
								String[] documentIDes = new String[allegatiTmp.size()];
								documentIDes = allegatiTmp.toArray(documentIDes);
								
								request.setDocumentIDes(documentIDes);

								RegistraStatoConsegnaPECInUscitaResponse response = proxyProtocollo.registraStatoConsegnaPECInUscita(request);

								EsitoNotificaSgdDTO esitoNotificaSgd = new EsitoNotificaSgdDTO();
								esitoNotificaSgd.setMessageID(messaggioInEntrata.getMessageId());
								esitoNotificaSgd.setAccountPec(request.getAccountPEC());
								esitoNotificaSgd.setMsgID(request.getMessageID());
								esitoNotificaSgd.setMetodo("RegistraStatoConsegnaPECInUscita");
								esitoNotificaSgd.setStato(request.getStato().toString());
								esitoNotificaSgd.setEsito(response.getCodice().toString());
								esitoNotificaSgd.setDescrizioneErrore(response.getDescrizione());
								
								esitiNotificheSgdDAO.insertEsito(esitoNotificaSgd);
								
							} else { // Altrimenti vado avanti
								continue;
							}
							
						}
					}
				}
				
				//Rimozione del messaggio dalla casella
		        message.setFlag(Flags.Flag.SEEN, true);
		        message.setFlag(Flags.Flag.DELETED, true);
		       
		        
		        //attendo esito notifica SGD
				if(resultSGD != null && messaggioInEntrata.getCanale() != null && messaggioInEntrata.getCanale().equalsIgnoreCase(ConfigProp.canaleSgd)){
					Object o = resultSGD.get();
					System.out.println("Esito notifica SGD per messageID "+messaggioInEntrata.getMessageId() + ": "+o);
					log.info("Esito notifica SGD: "+messaggioInEntrata.getMessageId() + ": "+o);
				}
				
	    	} catch ( BusinessException e ){
				executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6051, "Errore nell'nserimento su DB->Messaggi. Dettaglio Errore: " + e.getMessage() ) ) );
	    		executor.executeCommand();
	    	} catch (DAOException e) {
				if(messaggioInEntrata.getSubject()!=null && !messaggioInEntrata.getSubject().contains("****SPAM****")){
					
		    		executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6051, "Errore nell'nserimento su DB->Messaggi. Dettaglio Errore: " + e.getMessage() ) ) );
		    		executor.executeCommand();
		    		
//					if(e.getMessage().toLowerCase().contains("disk space".toLowerCase()) && i==0){
//						ConfigProp.switchDbPecPei = true;
//						continue;
//					}
	    		}
	    	} catch (Exception e) {
	    		e.printStackTrace();
				executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6051, "Errore generico nell'nserimento su DB->Messaggi. Dettaglio Errore: " + e.getMessage() ) ) );
	    		executor.executeCommand();
			}
	    }
	    
	    try{
	    	accountPecDao.updateDataUltimaRicezione(accountPecDTO.getUserName());
	    }catch (Exception e) {
			executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -6051, "Impossibile aggiornare la data ultima rcezione : " + e.getMessage() ) ) );
    		executor.executeCommand();
		}
	    
//	    if(accountPecDTO.getUserName().equals("direzione.provinciale.messina")){
		    inbox.close(true);
		    store.close();
		    
//	    }		
			} catch (NoSuchProviderException e) {
				throw new NotifyMailBusinessException(-2010, "Errore nella connesisone con l'account PEC[ " + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				throw new NotifyMailBusinessException(-2020, "Errore nella connesisone con l'utenza [" + accountPecDTO.getUserName() + "] nell'account PEC[ " + accountPecDTO.getHostSmtp() + "] Dettaglio Errore: " + e.getMessage());
			}
			
		    verificaLetturaCasella(accountPecDTO.getUserName(),accountPecDao);
//			if(verificaLetturaCasella(accountPecDTO.getUserName(),accountPecDao))
//				return null;

		    
	        return null;
	}
	
	
	private boolean verificaLetturaCasella(String acc,AccountPecDAO dao){
		
		try{
			Date oraCorrente = new Date();
			int ritardo = (int) ((oraCorrente.getTime()-dao.getDataUltimaRicezione(acc))/60000);
			
			if(ConfigProp.ritardoLettura <= ritardo){
				log.info("Ritardo Lettura Casella: La casella "+acc+" non viene letta da "+ritardo+" minuti. La disabilito.");
				System.out.println("Ritardo Lettura Casella: La casella "+acc+" non viene letta da "+ritardo+" minuti. La disabilito.");
				
				if(dao.disabilitaCasella(acc)==1){
					log.info("Ritardo Lettura Casella: La casella "+acc+" � stata disabilitata.");
					System.out.println("Ritardo Lettura Casella: La casella "+acc+" � stata disabilitata.");
					
					//invio mail
					CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.NORMAL);
					executor.setCommand( new SendMailCasella(acc));
					executor.executeCommandAndLog();
		    		
				}else{
					log.info("Ritardo Lettura Casella: Errore nel disabilitare la casella "+acc);
					System.out.println("Ritardo Lettura Casella: Errore nel disabilitare la casella "+acc);
				}
					
				return true;
			}
			
		}catch(Exception e){
			
		}
				
		return false;
	}
	
	public static void main( String[] args ){
		try {
			
			new ConfigProp().init();
				
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
			AccountPecDTO acc = new AccountPecDTO();
			acc.setUserName( "utente03" );
			acc.setCodAOO("0040");
			new ReadAndElabMessageCommand( acc ).execute();
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Read Message MAIL";
	}

}
