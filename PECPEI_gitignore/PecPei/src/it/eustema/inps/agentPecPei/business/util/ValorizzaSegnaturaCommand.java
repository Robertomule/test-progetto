package it.eustema.inps.agentPecPei.business.util;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;

public class ValorizzaSegnaturaCommand implements Command {

	private MessaggioDTO messaggio;
	private String segnatura;
	
	public ValorizzaSegnaturaCommand( MessaggioDTO messaggio, String segnatura ){
		this.messaggio = messaggio;
		this.segnatura = segnatura;	
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		
		messaggio.setSegnatura( segnatura );
		messaggio.setSegnaturaMitt("");
		messaggio.setSegnaturaRif("");
		
		//Valido solo per i messaggi in uscita. Per i messaggi in entrata, nella fase di 
		//protocollazione, molti campi non sono stati ancora valorizzati
		if ( "U".equalsIgnoreCase( messaggio.getVerso() ) && ( messaggio.getSegnaturaRoot() == null || "".equalsIgnoreCase(messaggio.getSegnaturaRoot().trim() ))){
			messaggio.setSegnaturaRoot( segnatura );	
		}
		
		
		return null;
	}
	
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Valorizzazione della segnatura";
	}

}
