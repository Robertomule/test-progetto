package it.eustema.inps.agentPecPei.business.util;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

import it.inps.www.interop2013.Destinazione;
import it.inps.www.interop2013.PerConoscenza;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.Posta;

public class SendMailInterop2013Command implements Command {

		private static Logger log = Logger.getLogger(SendMailCommand.class);
		
		private NotifyMailBusinessException exception;
		private AllegatoDTO allegato;
		private MessaggioDTO messaggio;
		private it.inps.www.interop2013.Segnatura segInterop2013;
		private SegnaturaDTO seg;
		
		public SendMailInterop2013Command( NotifyMailBusinessException exception, AllegatoDTO allegato, MessaggioDTO messaggio,it.inps.www.interop2013.Segnatura segInterop2013, SegnaturaDTO seg ){
			this.allegato = allegato;
			this.exception = exception;
			this.messaggio = messaggio;
			this.segInterop2013 = segInterop2013;
			this.seg = seg;
		}
		
		//26/01/2015 Nuove modifica NotificaErrori
		/*
		@Override
		public Object execute() throws BusinessException 
		{
			log.debug("notificaErroreByDB");
			try{
				
				Vector<String> destinatari = new Vector<String>();
				
				String destinatarioIterop = segInterop2013.getIntestazione().getOrigine().getIndirizzoTelematico().getValue();
				
				if(destinatarioIterop==null || (destinatarioIterop!=null && destinatarioIterop.equals("")))
					destinatarioIterop= segInterop2013.getIntestazione().getRisposta().getIndirizzoTelematico().getValue();
			
				if(!validateEmail(destinatarioIterop)){
					log.debug("Segnatura contiene mittente errato :"+destinatarioIterop+" sostituito con "+messaggio.getReplyTo());
					destinatarioIterop = messaggio.getReplyTo();
				}
				
				destinatari.add(destinatarioIterop);
				
				String testoOggetto = allegato.getFileName().equalsIgnoreCase("eccezione.xml")?"Notifica di Eccezione":"Conferma di Ricezione"; 
				
				try{
					MessaggioDTO messaggioInUscitaIns = (MessaggioDTO) SerializationUtils.clone(messaggio);
					inserisciMailDB(messaggioInUscitaIns, allegato, destinatari, testoOggetto);
				}
				catch(Exception e)
				{
					log.error("Errore durante l'inserimento Mail Interop2013 nel DB : " + e.getMessage());
				}
				
				InserisciNotificaErrori();
								
			}
			catch(Exception e)
			{
				log.error("Notifica errori fallita: " + e.getMessage() + "; segInterop2013 = " + segInterop2013 + ";");
			}
			
			return null;
		}
		*/
		//26/01/2015 Nuove modifica NotificaErrori
		
		public Object execute() throws BusinessException {
			
				log.debug("notificaErroreByMail : SendMailInteropCommand");
				try{
					Vector<String> destinatari = new Vector<String>();
//					public void inviaEmailAllegati(String serverPosta, String uid,
//							String password, String mittente, Vector<String> destinatari,
//							Vector<String> destinatariCC, String oggetto, String testoEmail,
//							AllegatoDTO allegati, boolean isHtml)
					
					String destinatarioIterop = segInterop2013.getIntestazione().getOrigine().getIndirizzoTelematico().getValue();
					
					if(destinatarioIterop==null || (destinatarioIterop!=null && destinatarioIterop.equals("")))
						destinatarioIterop= segInterop2013.getIntestazione().getRisposta().getIndirizzoTelematico().getValue();
				
					if(!validateEmail(destinatarioIterop)){
						log.debug("Segnatura contiene mittente errato :"+destinatarioIterop+" sostituito con "+messaggio.getReplyTo());
						destinatarioIterop = messaggio.getReplyTo();
					}
					
					destinatari.add(destinatarioIterop);
					
					Posta posta = new Posta();
					String mittente = String.format(ConfigProp.indirizzoMittenteInterop,messaggio.getAccountPec());
					String testoOggetto = allegato.getFileName().equalsIgnoreCase("eccezione.xml")?"Notifica di Eccezione":"Conferma di Ricezione"; 
//					if(!destinatarioIterop.contains(messaggio.getAccountPec())){
					posta.inviaInsertDBEmailAllegati(
									ConfigProp.serverPosta,
									ConfigProp.uidPosta,
									ConfigProp.pwdPosta,
									mittente, // Inserire il mittente
									destinatari, // Inserire il destinatario
									null, // Inserire il destinatario CC
									testoOggetto+" : "+messaggio.getSubject(), // Inserire Oggetto
									creaBodyMessaggio(testoOggetto), // Inserire Testo da definire
									allegato,
									true);
//					}
					try{
						MessaggioDTO messaggioInUscitaIns = (MessaggioDTO) SerializationUtils.clone(messaggio);
						inserisciMailDB(messaggioInUscitaIns, allegato, destinatari, testoOggetto);
					}catch(Exception e){}
				}
				catch(Exception e){
					new NotifyMailBusinessException(-6002, "Errore nell'invio email con interoperabilitÓ del messaggio "+messaggio.getMessageId()+": " + e.getMessage());
				}
				
				return null;
			} 
		

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return "SendMailInteropCommand";
		}
		
		private void inserisciMailDB(MessaggioDTO messaggioInUscitaIns, AllegatoDTO allegato, List<String> destinatari, String testoOggetto)throws DAOException{
			List<DestinatarioDTO> dest = new ArrayList<DestinatarioDTO>();
			DestinatarioDTO destinatario = new DestinatarioDTO();
			destinatario.setAddr(destinatari.get(0));
			destinatario.setPhrase(destinatari.get(0));
			destinatario.setName(destinatari.get(0));
			destinatario.setType("Per");
			destinatario.setPostedDate(messaggio.getPostedDate());
			dest.add(destinatario);
			messaggioInUscitaIns.setElencoDestinatariPer(dest);
			messaggioInUscitaIns.setElencoDestinatariCc(new ArrayList<DestinatarioDTO>());
			messaggioInUscitaIns.setElencoDestinatariCcn(new ArrayList<DestinatarioDTO>());
			messaggioInUscitaIns.setSubject(testoOggetto+messaggio.getSubject());
			messaggioInUscitaIns.setBody(creaBodyMessaggio(testoOggetto));
			List<AllegatoDTO> allegati = new ArrayList<AllegatoDTO>();
			allegati.add(allegato);
			messaggioInUscitaIns.setElencoFileAllegati(allegati);
			messaggioInUscitaIns.setVerso("U");
			messaggioInUscitaIns.setMessageId(messaggioInUscitaIns.getMessageId().concat("-interop"));
			messaggioInUscitaIns.setInteroperabile(true);
			MessaggiDAO messaggiDAO = new MessaggiDAO();
			messaggiDAO.inserisciMessaggio(null, messaggioInUscitaIns );
		}
		
		public static boolean validateEmail(String email) {
	        boolean isValid = false;
	        try {
	            //
	            // Create InternetAddress object and validated the supplied
	            // address which is this case is an email address.
	            //
	            InternetAddress internetAddress = new InternetAddress(email);
	            internetAddress.validate();
	            isValid = true;
	        } catch (AddressException e) {
//	            e.printStackTrace();
	        }
	        return isValid;
	    }
		public String creaBodyMessaggio(String nomeFile){
			String andataCapo = "\n";
			StringBuilder testo = new StringBuilder("");
			testo.append(andataCapo+" Il messaggio con oggetto: \""+messaggio.getSubject()+"\"");
			testo.append(andataCapo+" e numero di protocollo\""+segInterop2013.getIntestazione().getIdentificatore().getNumeroRegistrazione()+"\""+ " del "+segInterop2013.getIntestazione().getIdentificatore().getDataRegistrazione());
			// da definire i destinatari
			String destinatari = "";
			String destinatariCC = "";
			
			for(Destinazione s:segInterop2013.getIntestazione().getDestinazione()){
				if(s.getIndirizzoTelematico()!=null)
					destinatari = s.getIndirizzoTelematico().getValue()+";"+destinatari;
			}
			for(PerConoscenza s:segInterop2013.getIntestazione().getPerConoscenza()){
				destinatariCC = destinatariCC+";"+s.getIndirizzoTelematico().getValue();
			}
			testo.append(andataCapo+" indirizzato a: "+destinatari.substring(0, destinatari.length()-1)); 
			
			if(nomeFile.equalsIgnoreCase("Notifica di Eccezione")){
				testo.append(andataCapo+"Ŕ stato smistato al destinatario "+messaggio.getAccountPec()); // da definire
				if(seg.getSegnatura()!=null&&!seg.getSegnatura().equals("")){
					testo.append(andataCapo+"in quanto si presenta il seguente problema di InteroperabilitÓ "+exception.getMessage()); // da definire OK
					testo.append(andataCapo+"il messaggio Ŕ stato protocollato, senza InteroperabilitÓ, con numero di protocollo : "+seg.getSegnatura()); // da definire OK
				}else{
					testo.append(andataCapo+"in quanto si presenta il seguente problema di InteroperabilitÓ "+exception.getMessage()); // da definire OK
				}
			}else{
				testo.append(andataCapo+"Ŕ stato protocollato con numero protocollo: "+seg.getSegnatura());
			}
			testo.append(andataCapo+"Struttura INPS: "+messaggio.getCodiceAOO());
			testo.append(andataCapo+"Indirizzo PEC destinatario: "+messaggio.getAccountPec());
			testo.append(andataCapo+"E-mail generata automaticamente dal sistema, si prega di non scrivere a tale indirizzo");
			return testo.toString();
		}
		
		private void InserisciNotificaErrori() throws DAOException
		{
			String dbName = System.getProperty("NotificaErrori.DbName");
			
			Object notifica = null;
			
			Object notificaDAO = null;
			
			long resultID = -1;
			
			if(dbName != null)					
			{
				if(dbName == Constants.DB_PECPEI)
				{
					notifica = new it.eustema.inps.agentPecPei.dto.NotificaErroriDTO();
					
					((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setCodice(exception.getErrorCode());
					
					((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setMessaggio(exception.getMessage());
					
					if(this.messaggio != null)
					{
						((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setAccountPEC(this.messaggio.getAccountPec());
						((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setCodiceAOO(this.messaggio.getCodiceAOO());
						((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setMessageID(this.messaggio.getMessageId());
					}
					
					notificaDAO = new it.eustema.inps.agentPecPei.dao.NotificaErroriDAO();
					
					resultID = ((it.eustema.inps.agentPecPei.dao.NotificaErroriDAO) notificaDAO).insertNotifica((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica);
					
				}else if(dbName == Constants.DB_VERIFICAPEC)
				{
					notifica = new it.inps.agentPec.verificaPec.dto.NotificaErroriDTO();
					
					((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica).setCodice(exception.getErrorCode());
					
					((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica).setMessaggio(exception.getMessage());
					
					if(this.messaggio != null)
					{
						((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica).setMessageID(this.messaggio.getMessageId());
					}
					
					notificaDAO = new it.inps.agentPec.verificaPec.dao.NotificaErroriDAO();
					
					resultID = ((it.inps.agentPec.verificaPec.dao.NotificaErroriDAO) notificaDAO).insertNotifica((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica);

				}
				else if(dbName == Constants.DB_HERMES)
				{
					notifica = new it.eustema.inps.hermes.dto.NotificaErroriDTO();
					
					((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setCodice(exception.getErrorCode());
					
					((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setMessaggio(exception.getMessage());
					
					if(this.messaggio != null)
					{
						((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setCodiceAOO(this.messaggio.getCodiceAOO());
						((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setMessageID(this.messaggio.getMessageId());
					}
					
					notificaDAO = new it.eustema.inps.hermes.dao.NotificaErroriDAO();
					
					resultID = ((it.eustema.inps.hermes.dao.NotificaErroriDAO) notificaDAO).insertNotifica((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica);

				}
				else
					log.info("Notifica errori fallita: valore proprietÓ NotificaErrori.DbName non riconosciuto!");
			}
			else
				log.info("Notifica errori fallita: proprietÓ NotificaErrori.DbName non trovata!");
			
			if(resultID < 0)
				log.info("Notifica errori fallita: inserimento in " + dbName + ".NotificaErrori fallito! Codice Errore: " + this.exception.getErrorCode() + "; Messaggio: " + this.exception.getMessage() + "");

		}
	}
