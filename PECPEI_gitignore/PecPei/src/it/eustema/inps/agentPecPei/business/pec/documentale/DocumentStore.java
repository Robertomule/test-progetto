package it.eustema.inps.agentPecPei.business.pec.documentale;


import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaRequest;
import it.inps.soa.DocumentStore.data.ArchiviaDocumentoRequest;
import it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse;
import it.inps.soa.DocumentStore.data.DocumentType;
import it.inps.soa.DocumentStore.data.Metadati;
import it.inps.soa.WS00541.IStoreAttachmentDocument;
import it.inps.soa.WS00541.IStoreAttachmentDocumentProxy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import wsPciClient.org.tempuri.DNA_BASICHTTP_BindingStub;

/**
 * 
 * @author r.stabile
 *
 */

public class DocumentStore{

	private static Logger log = Logger.getLogger(ProtocollaMessaggioEntrataCommandNew.class);

	private Map<String, String> allegatiMapTmp;
	//private List<String> allegatiNoRelatedDataTmp;
	private EsitoStoreAllegati esitoStore;
	private IStoreAttachmentDocumentProxy proxy;
	private DNA_BASICHTTP_BindingStub stub;
	private int totAllegati;

	public DocumentStore(){
		proxy = new IStoreAttachmentDocumentProxy();
		stub = proxy.getProxyHelper(); //Chiamata con datapower
		//stub = proxy.getProxyHelperNoDataPower(); //Chiamata senza datapower

		//allegatiNoRelatedDataTmp = new ArrayList<String>();
		allegatiMapTmp = new HashMap<String, String>();
		esitoStore = new EsitoStoreAllegati();
		totAllegati = 0;
	}

	public EsitoStoreAllegati memorizzaAllegati(List<AllegatoDTO> allegati, String messageId, String segnatura){

		try{
			//Salvo gli allegati nel document store e gli id nella tabella allegati di pecpei
			if(allegati.size() > 0){

				/* inizio archivio documento */
				log.info("Inizio archiviazione documenti per " + messageId);

				//Se l'allegato non ha un id del document store non � stato salvato
				for(AllegatoDTO allegato: allegati){
					if(allegato.getIdDocStore() == null || allegato.getIdDocStore().equals("")){
						if(!allegato.getFileName().equalsIgnoreCase("TestoDelMessaggio.txt")){
							byte[] body = allegato.getFileData();

							Metadati metadati = new Metadati();
							String[] ct = allegato.getContentType().split(";");

							if(ct[0].equalsIgnoreCase("txt"))
								metadati.setContentType("text/plain");
							else
								if(ct[0].equalsIgnoreCase("xml"))
									metadati.setContentType("text/xml");
								else
									metadati.setContentType(ct[0]);

							//aggiungo il content-type se mancante
							if(ct == null || ct[0].equalsIgnoreCase("")){
								if(allegato.getTipoAllegato() != null && !allegato.getTipoAllegato().equals(""))
									metadati.setContentType("application/"+allegato.getTipoAllegato());
								else
									metadati.setContentType("application/octet-stream");

								System.out.println("content-type mancante. Eseguito calcolo: " + metadati.getContentType());
								log.info("content-type mancante. Eseguito calcolo: " + metadati.getContentType());
							}

							metadati.setOriginalFileName(allegato.getFileName());

							ArchiviaDocumentoPerSegnaturaRequest archiviaRequest = new ArchiviaDocumentoPerSegnaturaRequest();
							archiviaRequest.setDocumento(body);
							archiviaRequest.setMetadati(metadati);
							archiviaRequest.setSegnatura(segnatura);
							archiviaRequest.setDocumentType(DocumentType.Correlato);

							ArchiviaDocumentoResponse archiviaResponse = stub.archiviaDocumentoPerSegnatura(archiviaRequest);

							//Se esito diverso da ok esco. Alla prossima elaborazione gli allegati non memorizzati verranno salvati.
							if(!archiviaResponse.getEsito().getValue().equalsIgnoreCase("OK")){
								log.info("Esito chiamata archiviaDocumento: " + archiviaResponse.getDocumentId() + " --> " + allegato.getFileName() +" "+ archiviaResponse.getEsito() + " - " + archiviaResponse.getDescrizione()!=null?archiviaResponse.getDescrizione():"");
								System.out.println("Esito chiamata archiviaDocumento: " + archiviaResponse.getDocumentId() + " --> " +  allegato.getFileName() +" "+ archiviaResponse.getEsito() + " - " + archiviaResponse.getDescrizione());

								esitoStore.setAllAllegatiMemorizzati(false);
								esitoStore.setAllegatiMap(allegatiMapTmp);
								esitoStore.setInteroperabile(false);
								return esitoStore;
							}else
								allegatiMapTmp.put(archiviaResponse.getDocumentId(), allegato.getFileName());

							//se � presente l'allegato segnatura.xml vuol dire che si tratta di un messaggio interoperabile 
							if(allegato.getFileName().equalsIgnoreCase("segnatura.xml")){
								esitoStore.setSegnaturaXml(new String(body));
								esitoStore.setInteroperabile(true);
							}

							//memorizzo l'id  restituido dal document store nella tabella allegati di pecpei
							AllegatiDAO allegatiDao = new AllegatiDAO();
							allegatiDao.addIdDocumentStore(archiviaResponse.getDocumentId(),messageId,allegato.getFileName());

							totAllegati++;

							log.info("Esito chiamata archiviaDocumento: " + archiviaResponse.getDocumentId() + " --> " + allegato.getFileName() +" "+ archiviaResponse.getEsito() + " - " + archiviaResponse.getDescrizione()!=null?archiviaResponse.getDescrizione():"");
							System.out.println("Esito chiamata archiviaDocumento: " + archiviaResponse.getDocumentId() + " --> " +  allegato.getFileName() +" "+ archiviaResponse.getEsito() + " - " + archiviaResponse.getDescrizione());
						} else { // Se l'allegato in lettura � il testo del messaggio aumento il contatore senza per� archiviarlo
							totAllegati++;
							log.info("Allegato: TestoDelMessaggio.txt --> Non archivio");
							System.out.println("Allegato: TestoDelMessaggio.txt --> Non archivio");
						}
					} else {
						//se � presente l'allegato segnatura.xml vuol dire che si tratta di un messaggio interoperabile 
						if(allegato.getFileName().equalsIgnoreCase("segnatura.xml")){
							esitoStore.setSegnaturaXml(new String(allegato.getFileData()));
							esitoStore.setInteroperabile(true);
						}
						//popolo la lista degli allegati
						allegatiMapTmp.put(allegato.getIdDocStore(), allegato.getFileName());

						totAllegati++;
						log.info("Esito chiamata archiviaDocumento: " + allegato.getFileName() +" Gi� presente nel Document Store");
						System.out.println("Esito chiamata archiviaDocumento: " + allegato.getFileName() +" Gi� presente nel Document Store");
					}
				}

				//Tutti gli allegati sono stati memorizzati
				if(totAllegati == allegati.size())
					esitoStore.setAllAllegatiMemorizzati(true);
				else
					esitoStore.setAllAllegatiMemorizzati(false);

				esitoStore.setAllegatiMap(allegatiMapTmp);

				log.info("Fine archiviazione documenti per " +messageId);
				return esitoStore;

			}else{
				esitoStore.setAllAllegatiMemorizzati(true);
				esitoStore.setAllegatiMap(allegatiMapTmp);
				esitoStore.setInteroperabile(false);
				return esitoStore;
			}
			/* fine archiviazione documenti */

		}catch(Exception e){
			e.printStackTrace();
			esitoStore.setAllAllegatiMemorizzati(false);
			esitoStore.setAllegatiMap(allegatiMapTmp);
			esitoStore.setInteroperabile(false);
			log.error("Eccezione in memorizzazione allegati nel document store per: "+messageId +" per errore: "+e.getMessage());
			System.out.println("Eccezione in memorizzazione allegati nel document store per: "+messageId +" per errore: "+e.getMessage());
			return esitoStore;
		}
	}

	public EsitoStoreAllegati memorizzaAllegato(AllegatoDTO allegato, String messageId, String segnatura){
		
		try{
			//Salvo gli allegati nel document store e gli id nella tabella allegati di pecpei
			if(allegato != null){

				// Archio il documento
				log.info("Inizio archiviazione documento " + allegato.getFileName() + " per il messaggio con MessageID: " + messageId);

				if(allegato.getIdDocStore() == null || allegato.getIdDocStore().equals("")){
					if(!allegato.getFileName().equalsIgnoreCase("TestoDelMessaggio.txt")){
						byte[] body = allegato.getFileData();

						Metadati metadati = new Metadati();
						String[] ct = allegato.getContentType().split(";");

						if(ct[0].equalsIgnoreCase("txt"))
							metadati.setContentType("text/plain");
						else
							if(ct[0].equalsIgnoreCase("xml"))
								metadati.setContentType("text/xml");
							else
								metadati.setContentType(ct[0]);

						//aggiungo il content-type se mancante
						if(ct == null || ct[0].equalsIgnoreCase("")){
							if(allegato.getTipoAllegato() != null && !allegato.getTipoAllegato().equals(""))
								metadati.setContentType("application/"+allegato.getTipoAllegato());
							else
								metadati.setContentType("application/octet-stream");

							System.out.println("content-type mancante. Eseguito calcolo: " + metadati.getContentType());
							log.info("content-type mancante. Eseguito calcolo: " + metadati.getContentType());
						}

						metadati.setOriginalFileName(allegato.getFileName());

						ArchiviaDocumentoPerSegnaturaRequest archiviaRequest = new ArchiviaDocumentoPerSegnaturaRequest();
						archiviaRequest.setDocumento(body);
						archiviaRequest.setMetadati(metadati);
						archiviaRequest.setSegnatura(segnatura);
						archiviaRequest.setDocumentType(DocumentType.Correlato);
						
						ArchiviaDocumentoResponse archiviaResponse = stub.archiviaDocumentoPerSegnatura(archiviaRequest);

						//Se esito diverso da ok esco. Alla prossima elaborazione gli allegati non memorizzati verranno salvati.
						if(!archiviaResponse.getEsito().getValue().equalsIgnoreCase("OK")){
							log.info("Esito chiamata archiviaDocumento: " + archiviaResponse.getDocumentId() + " --> " + allegato.getFileName() +" "+ archiviaResponse.getEsito() + " - " + archiviaResponse.getDescrizione()!=null?archiviaResponse.getDescrizione():"");
							System.out.println("Esito chiamata archiviaDocumento: " + archiviaResponse.getDocumentId() + " --> " +  allegato.getFileName() +" "+ archiviaResponse.getEsito() + " - " + archiviaResponse.getDescrizione());

							esitoStore.setAllAllegatiMemorizzati(false);
							esitoStore.setAllegatiMap(allegatiMapTmp);
							esitoStore.setInteroperabile(false);
							return esitoStore;
						} else {
							allegatiMapTmp.put(archiviaResponse.getDocumentId(), allegato.getFileName());
							esitoStore.setAllAllegatiMemorizzati(true); // Se � stato memorizzato
						}
						//se � presente l'allegato segnatura.xml vuol dire che si tratta di un messaggio interoperabile 
						if(allegato.getFileName().equalsIgnoreCase("segnatura.xml")){
							esitoStore.setSegnaturaXml(new String(body));
							esitoStore.setInteroperabile(true);
						}

						//memorizzo l'id  restituido dal document store nella tabella allegati di pecpei
						AllegatiDAO allegatiDao = new AllegatiDAO();
						allegatiDao.addIdDocumentStore(archiviaResponse.getDocumentId(),messageId,allegato.getFileName());

						log.info("Esito chiamata archiviaDocumento: " + archiviaResponse.getDocumentId() + " --> " + allegato.getFileName() +" "+ archiviaResponse.getEsito() + " - " + archiviaResponse.getDescrizione()!=null?archiviaResponse.getDescrizione():"");
						System.out.println("Esito chiamata archiviaDocumento: " + archiviaResponse.getDocumentId() + " --> " +  allegato.getFileName() +" "+ archiviaResponse.getEsito() + " - " + archiviaResponse.getDescrizione());
					} else { // Se l'allegato in lettura � il testo del messaggio aumento il contatore senza per� archiviarlo
						totAllegati++;
						log.info("Allegato: TestoDelMessaggio.txt --> Non archivio");
						System.out.println("Allegato: TestoDelMessaggio.txt --> Non archivio");
					}
				} else {
					//se � presente l'allegato segnatura.xml vuol dire che si tratta di un messaggio interoperabile 
					if(allegato.getFileName().equalsIgnoreCase("segnatura.xml")){
						esitoStore.setSegnaturaXml(new String(allegato.getFileData()));
						esitoStore.setInteroperabile(true);
					}
					//popolo la lista degli allegati
					allegatiMapTmp.put(allegato.getIdDocStore(), allegato.getFileName());

					log.info("Esito chiamata archiviaDocumento: " + allegato.getFileName() +" Gi� presente nel Document Store");
					System.out.println("Esito chiamata archiviaDocumento: " + allegato.getFileName() +" Gi� presente nel Document Store");
				}

				esitoStore.setAllegatiMap(allegatiMapTmp);

				log.info("Fine archiviazione documenti per " +messageId);
				return esitoStore;

			} else {
				esitoStore.setAllAllegatiMemorizzati(true);
				esitoStore.setAllegatiMap(allegatiMapTmp);
				esitoStore.setInteroperabile(false);
				return esitoStore;
			}
			// Fine Archiviazione

		} catch(Exception e){
			
			e.printStackTrace();
			esitoStore.setAllAllegatiMemorizzati(false);
			esitoStore.setAllegatiMap(allegatiMapTmp);
			esitoStore.setInteroperabile(false);
			log.error("Eccezione in memorizzazione allegati nel document store per: "+messageId +" per errore: "+e.getMessage());
			System.out.println("Eccezione in memorizzazione allegati nel document store per: "+messageId +" per errore: "+e.getMessage());
			return esitoStore;
		}
	}
}






