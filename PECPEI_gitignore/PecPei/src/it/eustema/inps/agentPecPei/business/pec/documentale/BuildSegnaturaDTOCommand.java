package it.eustema.inps.agentPecPei.business.pec.documentale;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;



import generated.vo.wspia.ServizioProtocollo;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;

/**
 * Classe Command che si occupa di costruire un'istanza della classe SegnaturaDTO
 * che contiene i dati risultanti dal parsing della stringa segnatura XML in input
 * @author ALEANZI
 *
 */
public class BuildSegnaturaDTOCommand implements Command {
	private String xml; 
	
	
	public BuildSegnaturaDTOCommand( String xml ){
		this.xml = xml;
	}
	
	@Override
	public Object execute() throws BusinessException {
		
		// TODO Auto-generated method stub
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
	   
	        /*SAXParser saxParser = saxParserFactory.newSAXParser();
	        SegnaturaParserHandler handler = new SegnaturaParserHandler();
	        saxParser.parse( new ByteArrayInputStream( xml.getBytes("UTF-8") ), handler );
	        SegnaturaDTO out = handler.getSegnaturaDTO();*/
	    	SegnaturaDTO out = null;
	        
	        // setup object mapper using the AppConfig class
	        JAXBContext context;
			try {
				
				if ( xml.indexOf("CodiceErrore") == -1 ){
					context = JAXBContext.newInstance(ServizioProtocollo.class);
				 // parse the XML and return an instance of the AppConfig class
					ServizioProtocollo seg = (ServizioProtocollo) context.createUnmarshaller().unmarshal(new ByteArrayInputStream( xml.getBytes("UTF-8") ));
					out = new SegnaturaDTO();
					out.setStatus( SegnaturaDTO.OK );
					out.setSegnatura( seg.getSegnatura() );
				} else {
					context = JAXBContext.newInstance(generated.vo.wspia.errore.ServizioProtocollo.class);
					 // parse the XML and return an instance of the AppConfig class
			        generated.vo.wspia.errore.ServizioProtocollo sp = (generated.vo.wspia.errore.ServizioProtocollo) context.createUnmarshaller().unmarshal(new ByteArrayInputStream( xml.getBytes("UTF-8") ));
			        out = new SegnaturaDTO();
			        out.setStatus( SegnaturaDTO.KO );
			        out.setCodiceErrore(sp.getCodiceErrore());
			        out.setDescErrore(sp.getDescrizioneErrore());
				}
			} catch (JAXBException e) {
					//throw new BusinessException(-50000, e.getMessage()  );
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				//throw new BusinessException(-50000, e.getMessage()  );
			} 
	        
	        if ( out == null ){
	        	out = new SegnaturaDTO();
	        	out.setStatus(SegnaturaDTO.KO);
	        	out.setCodiceErrore( "-1" );
	        	out.setDescErrore("Errore non censito dal servizio del protocollo");
	        }
	        return out;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "xml -> SegnaturaDTO";
	}
	
	
	
}
