package it.eustema.inps.agentPecPei.business.pec.filler;

import it.eustema.inps.agentPecPei.dto.MessaggioDTO;

/**
 * Filler per MessaggioDTO generico. Utilizzata per implementare il metodo setMessaggioDTO()
 * In questo modo le classi concrete si possono occupare solamente di implementare in metodo fill()
 * @author ALEANZI
 *
 */
public abstract class AbstractMessaggioDTOFiller implements Filler {

	private MessaggioDTO messaggioDTO;
	
	
	public void setMessaggioDTO(MessaggioDTO messaggioDTO) {
		// TODO Auto-generated method stub
		this.messaggioDTO = messaggioDTO;

	}
	
	protected MessaggioDTO getMessaggioDTO(){
		return this.messaggioDTO;
	}


}
