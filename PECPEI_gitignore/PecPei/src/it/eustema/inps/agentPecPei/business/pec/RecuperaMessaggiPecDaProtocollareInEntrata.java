package it.eustema.inps.agentPecPei.business.pec;

import java.util.List;
import java.util.concurrent.Callable;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.mail.ReadAndElabMessaggiPecDaProtocollare;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;

public class RecuperaMessaggiPecDaProtocollareInEntrata implements Command, Callable<Object> {
	
		private String codAOO;
		
		public RecuperaMessaggiPecDaProtocollareInEntrata( String codAOO ){
			this.codAOO = codAOO;
		}
		@Override
		public Object execute() throws BusinessException {
			CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
			List<AccountPecDTO> listaAccountPEC = null;
			//Lettura di tutti gli account PEC attivi e non temporaneamente disabilitati
			AccountPecDAO accountPecDAO = new AccountPecDAO();
			try {
				listaAccountPEC = accountPecDAO.selectAllAccountPecAttivi( codAOO );
			} catch (DAOException e) {
				throw new NotifyMailBusinessException( -1001, "Errore nel recupero di tutti gli account PEC da DB. DettaglioErrore: " + e.getMessage() );
			} 
			
			for ( AccountPecDTO accountPEC : listaAccountPEC ){
				try {
					executor.setCommand( new ReadAndElabMessaggiPecDaProtocollare( accountPEC.getCodAOO() ) );
					executor.executeCommandAndLog();
				} catch ( BusinessException e){
					//Qualsiasi Errore, si passa all'elaborazione dell'account successivo
				}
			}

			return new Integer(1);
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return "RecuperaMessaggiPecDaProtocollareInEntrata";
		}
		@Override
		public Object call() throws Exception {
			// TODO Auto-generated method stub
			return execute();
		}


}
