package it.eustema.inps.agentPecPei.business.pec.pst;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Vector;

import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.pff.PSTAttachment;
import com.pff.PSTException;
import com.pff.PSTFile;
import com.pff.PSTFolder;
import com.pff.PSTMessage;
//import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;



import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.builder.MessageMessaggioDTOBuilder;
import it.eustema.inps.agentPecPei.business.pec.builder.PSTAttachmentAllegatoDTOBuilder;
import it.eustema.inps.agentPecPei.business.pec.builder.PSTMessaggioDTOBuilder;
import it.eustema.inps.agentPecPei.business.pec.cert.RetrieveCertAuthDTOCommand;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dao.CartellaDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.CartellaDTO;
import it.eustema.inps.agentPecPei.dto.CertAuthDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorImpl;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.DBConnectionManager;
import it.eustema.inps.utility.logging.LoggingConfigurator;

public class ReadPST implements Command {
	private static Logger log = Logger.getLogger(ReadPST.class);
	private CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
	private MessaggiDAO messaggiDAO = new MessaggiDAO();
	int depth = -1;
	Connection connPecPei = null;
	private String codAOO;
	private String accountPEC;
	private String nomeFile;
	private String fileDaNonCaricare = "Attivit�;Livello superiore file di dati di Outlook;Contact Search;Freebusy Data;ItemProcSearch;IPM_COMMON_VIEWS;IPM_VIEWS;SPAM Search Folder 2;Cerca nella cartella radice;Elaborazione posta verificata;Ricerca Da fare;Promemoria;GPooledSearchFolder;Impostazioni azioni rapide;Posta eliminata;Calendario;Contatti;Diario;Note;Attivit;Bozze;Feed RSS;Impostazioni azione conversazione;Impostazioni azioni rapide;Posta indesiderata;Contatti suggeriti;Impostazioni azione conversazione;Posta in uscita;Newsfeed;Cancellate WEB";
	private String startWith = "MS-OLK-FGPooledSearchFolder";
	
	private FileRoller outRoller = null;
	private FileRoller errorRoller = null;
	
	
	static {
        System.out.println("ScheduleMain :: esegue blocco di inizializzazione");
		//blocco eseguito una volta al caricamento della classe
        try {
			// carica i parametri dal file di cfg
			//new ConfigProp(cfgFileName);
			new ConfigProp().init();
			// inizializzazione configurazione del logging
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
		} catch (Exception e) {
			System.err.println("" + e.getMessage());
		}        
    }

	public ReadPST( String nomeFile, String codAOO, String accountPEC ){
		try {
			connPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.codAOO = codAOO;
		this.nomeFile = nomeFile;
		this.accountPEC = accountPEC;
		
		outRoller = new FileRoller("D:\\ESECUZIONE_AGENT\\", codAOO+"_OUT.txt");
		errorRoller = new FileRoller("D:\\ESECUZIONE_AGENT\\", codAOO+"_ERROR.txt");
	}
	
	
	
	public static void main(String[] args){
//		String from = "\"Per conto di: cch39867@pec.carabinieri.it\" <posta-certificata@cert.actalis.it>";
//		System.out.println(from.substring(0,from.lastIndexOf("\"")).replaceAll("<", "").replaceAll(">", "").replaceAll("\"", "").replaceAll("Per conto di:", "").trim());
		try {
			CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
			executor.setCommand( new ReadPST( args[0], args[1], args[2] ) );
			executor.executeCommand();
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		try {
            PSTFile pstFile = new PSTFile( nomeFile );
            log.debug( "Elaborazione file: " + nomeFile );
            log.debug( "codAOO: " + codAOO );
            CartellaDAO cartellaDao = new CartellaDAO();
			cartellaDao.verificaTabelle(connPecPei);
			System.out.println(pstFile.getMessageStore().getDisplayName());
			processFolder(pstFile.getRootFolder(),pstFile.getRootFolder());
			connPecPei.commit();

//			BufferedReader br = null;
//			try {
//	 
//				String sCurrentLine;
//	 
//				br = new BufferedReader(new FileReader("C:\\Users\\mcarpentieri\\Documents\\Documenti Agent - Migrazione PecPei\\BodyAnomali.txt"));
//	 
//				while ((sCurrentLine = br.readLine()) != null) {
//					
//					String queryEml = "select rawContent FROM MessaggiBlob where Messageid = ?";
//					PreparedStatement pstmt = null;
//					ResultSet rs=null;
//					Connection conPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
//					pstmt = conPecPei.prepareStatement(queryEml);
//					pstmt.setString(1, sCurrentLine);
//					rs = pstmt.executeQuery();
//
//					if(rs!=null){
//						
//						while(rs.next()){
//							
//							try{
//								String contenuto = rs.getString("rawContent");
////								System.out.println("contenuto: "+contenuto);
//								File f = new File("C:\\testPdf\\messaggiBonificati\\"+sCurrentLine.replaceAll("<", "").replaceAll(">", "")+"postacert.eml");
//								FileOutputStream fo = new FileOutputStream(f);
//								fo.write(contenuto.getBytes());
//								fo.flush();
//								fo.close();
//								System.out.println("messaggio elaborato : "+sCurrentLine);
//								String queryMessaggioPostedDate = "select PostedDate, CodiceAOO, accountPEC FROM messaggi where MessageID = ?";
//			    				pstmt = conPecPei.prepareStatement(queryMessaggioPostedDate);
//								pstmt.setString(1, sCurrentLine);
//								rs = pstmt.executeQuery();
//								Timestamp postedDate = null;
//								String codiceAOO = "";
//								String accountPec = "";
//								if(rs!=null){
//									while(rs.next()){
//										postedDate = rs.getTimestamp("postedDate");
//										codiceAOO = rs.getString("CodiceAOO");
//										accountPec = rs.getString("AccountPec");
//					    				ByteArrayInputStream bo = new ByteArrayInputStream(contenuto.getBytes());
//					    				MimeBodyPart m = new MimeBodyPart(bo);
//					    				MessaggioDTO messaggio = new MessaggioDTO();
//					    				PSTMessaggioDTOBuilder builder = new PSTMessaggioDTOBuilder();
//					    				ArrayList<AllegatoDTO> allegati = new ArrayList<AllegatoDTO>();
//					    				builder.elabPostaCertEML(messaggio, m,allegati);
//			    				
//					    				for(AllegatoDTO allegato : allegati){
//						    				String updateAllegati = "update allegati set Allegato = ? where messageId = ? and NomeAllegato = ? and ";
//						    				pstmt = conPecPei.prepareStatement(updateAllegati);
//											pstmt.setBytes(1, allegato.getFileData());
//											pstmt.setString(2, sCurrentLine);
//											pstmt.setString(3, allegato.getFileName());
//											allegato.setPostedDate(postedDate);
//											int i = pstmt.executeUpdate();
//											if(i==0){
//												messaggiDAO.doInsertAllegato(conPecPei, sCurrentLine, updateAllegati, allegato);
//												System.out.println("Allegato inserito message Id "+ sCurrentLine + "Nome File "+allegato.getFileName());
//											}else{
//												System.out.println("Allegato aggiorato message Id "+ sCurrentLine + "Nome File "+allegato.getFileName());
//											}
//					    				}

/*			    				
					    				conPecPei.setAutoCommit(false);
					    				String updateBody = "update messaggi set body = ?, substato=? where messageId = ? and accountPEC = ? and codiceAOO = ?";
					    				pstmt = conPecPei.prepareStatement(updateBody);
					    				
//					    				System.out.println("messaggio.getBody() : "+messaggio.getBody());
//					    				System.out.println("messaggio.getPostaCertEMLBody(): "+messaggio.getPostaCertEMLBody());
					    				
					    				if(messaggio.getPostaCertEMLBody()!=null)
					    					pstmt.setString(1, messaggio.getPostaCertEMLBody());
					    				else
					    					pstmt.setString(1, messaggio.getBody());
					    				
					    				pstmt.setString(2, "NT");
					    				pstmt.setString(3, sCurrentLine);
					    				pstmt.setString(4, accountPec);
					    				pstmt.setString(5, codiceAOO);
										int r = pstmt.executeUpdate();
										if(r>1){
											System.out.println("Trovato pi� di un message Id con il valore non corretto "+sCurrentLine);
											conPecPei.rollback();
										}else{
											System.out.println("Body Del messaggio Aggiornato "+sCurrentLine);
											conPecPei.commit();
										}
										conPecPei.setAutoCommit(true);
*/
			
//									}
//								}
//								
//		    				}catch(Exception e){
//		    					e.printStackTrace();
//		    				}
//						}
//					}
//					if(conPecPei!=null) conPecPei.close();
//					if(rs!=null) rs.close();
//					if(pstmt!=null) pstmt.close();
//				}
	 
//			} catch (IOException e) {
//				e.printStackTrace();
//			} finally {
//				try {
//					if (br != null)br.close();
//				} catch (IOException ex) {
//					ex.printStackTrace();
//				}
//			}

    } catch (Exception err) {
            err.printStackTrace();
    }
    
    outRoller.stopRoll();
    errorRoller.stopRoll();
		return null;

	}
	
	public void processFolder(PSTFolder childFolder, PSTFolder parentFolder) throws PSTException, java.io.IOException, BusinessException {
		depth++;
		// 	the root folder doesn't have a display name
		if (depth > 0) {
			//printDepth();
			//System.out.println(folder.getDisplayName());
		}
		
		// go through the folders...
		if (childFolder!=null && childFolder.hasSubfolders()) {
			Vector<PSTFolder> childFolders = childFolder.getSubFolders();
		    for (PSTFolder figlioFolder : childFolders) {
		    	processFolder(figlioFolder, childFolder);
		    }
		}
		Integer idCartellaMsg = null;
        int idcartella = 0;
    	idCartellaMsg=1;
    	
    	// and now the emails for this folder
		if (!fileDaNonCaricare.contains(childFolder.getDisplayName()) && !childFolder.getDisplayName().startsWith(startWith)) {
			
			log.debug("Elaborazione Cartella:[" + childFolder.getDisplayName() + "]");

			depth++;
			
		    CartellaDTO car = new CartellaDTO();
            car.setAccount(accountPEC);
            car.setValida(true);
            CartellaDAO cartellaDao = new CartellaDAO();
            String currentFolderPath;
            String parentFolderPath;
            try{
                if(childFolder.equals("Livello superiore file di dati di Outlook")){
                	currentFolderPath = childFolder.getDisplayName().replaceAll("Livello superiore file di dati di Outlook", "");
                	car.setFullPathFolder(currentFolderPath);
                	car.setDescrizione(currentFolderPath);
                	car.setIdPadre(null);
                }else{
                	CartellaDTO cartellaPadre = new CartellaDTO();
                	parentFolderPath = parentFolder.getDisplayName();
                	cartellaPadre.setFullPathFolder(parentFolderPath);
                	cartellaPadre.setDescrizione(parentFolderPath);
                	cartellaPadre.setAccount(accountPEC);
                	
                	int cartPadre = cartellaDao.getIdCartellaPadre(cartellaPadre);
                	
                	if(cartPadre<0 && !parentFolderPath.equals("Livello superiore file di dati di Outlook"))
                		cartPadre = cartellaDao.insertCartella(cartellaPadre);
                	
                	car.setIdPadre(cartPadre);
                	car.setDescrizione(childFolder.getDisplayName().replaceAll("Livello superiore file di dati di Outlook", ""));
                	currentFolderPath = (parentFolderPath + "." + childFolder.getDisplayName().replaceAll("Livello superiore file di dati di Outlook", "")).replaceAll("Livello superiore file di dati di Outlook.", "");	                   
                	car.setFullPathFolder(currentFolderPath);
                }
                
               /* if ( car.getDescrizione().equals("archivio posta inviata") ) {
                	System.out.println("OK");
                } else return;
                */
                
                idcartella = cartellaDao.isCartellaPresente(accountPEC, null, car.getFullPathFolder());	
                if(idcartella==0){
                	if(!fileDaNonCaricare.contains(currentFolderPath)){
                		if(!currentFolderPath.contains("Attivit�"))
                			idCartellaMsg = cartellaDao.insertCartella(car);
                		else
                			idCartellaMsg = -1;
                	}
                }
            }catch(Exception e){
            	e.printStackTrace();
            }
           
           if(idcartella>0) idCartellaMsg = idcartella;
           
           PSTMessage email = (PSTMessage)childFolder.getNextChild();
           AccountPecDTO accountPecDTO = null;
	       while (email != null) {
	    	   
			    	/*try {
						accountPecDTO = new AccountPecDAO().selectAccountPec( accountPEC );
					} catch (DAOException e1) {
						throw new NotifyMailBusinessException( -4011, "Account PEC [" +accountPEC +"] errore lettura su DB" );
					}
					
					if ( accountPecDTO == null ){
						throw new NotifyMailBusinessException( -4010, "Account PEC [" +accountPEC +"] non esistente su DB" );
					}*/
	    	   
	    	   
	    	   		MessaggioDTO messaggio = null;
	    	   		
	    	   		
		            //printDepth();
		            try {
		            	messaggio = (MessaggioDTO)new PSTMessaggioDTOBuilder( email, codAOO, "", accountPEC ).execute();
		            	if ( "<0d177c238cf35e78ecc633058541ba0b.squirrel@webmail.spcoop.postacert.it>".equals( messaggio.getMessageId() ) ){
		            		System.out.println("trovato");
		            	}
		            	messaggio.setAccountPec(accountPEC);
		            	messaggio.setCodiceAOO(codAOO);
		            	messaggio.setAutoreCompositore(Constants.AUTORE_COMPOSITORE_PST);
		            	messaggio.setAutoreUltimaModifica(Constants.AUTORE_COMPOSITORE_PST);
		            	messaggio.setAutoreMittente(Constants.AUTORE_COMPOSITORE_PST);
		            	messaggio.setIdCartella(idCartellaMsg);
						messaggiDAO.inserisciMessaggio(connPecPei, messaggio);
						try{
							messaggiDAO.doInsertAllegati(connPecPei, messaggio.getMessageId(), messaggio.getCodiceAOO(), messaggio.getElencoFileAllegati() );
							log.debug("Messaggio [" + messaggio.getMessageId() + "] Caricato correttamente" );
							outRoller.roll( "Messaggio [" + messaggio.getMessageId() + "] Caricato correttamente"  );
						} catch ( Exception e ){
							outRoller.roll( "**** Messaggio [" + messaggio.getMessageId() + "] non � stato caricato correttamente: Errore: " + e.getMessage() );
						}
						//messaggiDAO.doInsertDestinatari(connPecPei, messaggio.getMessageId(), messaggio.getElencoDestinatariPer());
						
						
					} catch (Exception e) {
						if ( messaggio != null && messaggio.getMessageId() != null )
							log.error("Messaggio [" + messaggio.getMessageId() + "] Non caricato. Dettaglio Errore: " + e.getMessage() );
						else 
							log.error("Messaggio non caricato. Dettaglio Errore: " + e.getMessage() );
						
						if ( messaggio != null )
							errorRoller.roll( "Il messaggio [" + messaggio.getMessageId() + "] non � caricato: Errore:" + e.getMessage() );
						else 
							errorRoller.roll( "Il messaggio non � caricato: Errore:" + e.getMessage() );
							
					}
						//System.out.println("Email: "+email.getSubject() + " Num Attac. " + email.getNumberOfAttachments() );
			            /*for ( int i=0; i<email.getNumberOfAttachments(); i++ ){
			            	elabAttach( email.getAttachment( i ) );
			            }*/
			            email = (PSTMessage)childFolder.getNextChild();
			    }
		    depth--;
		}
		depth--;
	}
	
	private void elaboraMessaggi(PSTMessage email, PSTFolder childFolder){
		  try{ 
			  while (email != null) {
		    	MessaggioDTO messaggio = (MessaggioDTO)new PSTMessaggioDTOBuilder( email, codAOO, "", accountPEC ).execute();
		    	
		    	if ( email.hasAttachments() ){
		    		
		    		PSTAttachment att = null;
		    		for ( int i = 0 ; i<email.getNumberOfAttachments(); i++){
		    			executor.setCommand( new PSTAttachmentAllegatoDTOBuilder(email.getAttachment( i ), messaggio) );
		    			AllegatoDTO allegato = (AllegatoDTO)executor.executeCommand();
		    			messaggio.getElencoFileAllegati().add( allegato );
		    		}
		    	}
		    	
		            //printDepth();
		            try {
		            	messaggio.setAccountPec(accountPEC);
		            	messaggio.setCodiceAOO(codAOO);
		            	messaggio.setAutoreCompositore("provaPOPexENTI");
		//				messaggiDAO.inserisciMessaggio(connPecPei, messaggio);
						messaggiDAO.doInsertAllegati(connPecPei, messaggio.getMessageId(), codAOO, messaggio.getElencoFileAllegati() );
		//				messaggiDAO.doInsertDestinatari(connPecPei, messaggio.getMessageId(), messaggio.getElencoDestinatariPer());
						 log.debug("Messaggio [" + messaggio.getMessageId() + "] Caricato correttamente" );
					} catch (Exception e) {
						log.error("Messaggio [" + messaggio.getMessageId() + "] Non caricato. Dettaglio Errore: " + e.getMessage() );
					}
					//System.out.println("Email: "+email.getSubject() + " Num Attac. " + email.getNumberOfAttachments() );
		            /*for ( int i=0; i<email.getNumberOfAttachments(); i++ ){
		            	elabAttach( email.getAttachment( i ) );
		            }*/
		            email = (PSTMessage)childFolder.getNextChild();
		    }
			  
		  }catch(Exception e ){
			  e.printStackTrace();
		  }
		  
		}
	
	private void elabAttach( PSTAttachment att ){
		try {
			if ( att.getFilename().indexOf(".p7") == -1)
				return;
			
	        InputStream attachmentStream = att.getFileInputStream();
	        // both long and short filenames can be used for attachments
	        String filename = att.getLongFilename();
	        if (filename.isEmpty()) {
	                filename = att.getFilename();
	        }
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        // 8176 is the block size used internally and should give the best performance
	        int bufferSize = 8176;
	        byte[] buffer = new byte[bufferSize];
	        int count = attachmentStream.read(buffer);
	        while (count == bufferSize) {
	                out.write(buffer);
	                count = attachmentStream.read(buffer);
	        }
	        byte[] endBuffer = new byte[count];
	        System.arraycopy(buffer, 0, endBuffer, 0, count);
	        out.write(endBuffer);
	        
	        attachmentStream.close();

			new RetrieveCertAuthDTOCommand( out.toByteArray() ).execute();
			out.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PSTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Lettura file PST";
	}


}
