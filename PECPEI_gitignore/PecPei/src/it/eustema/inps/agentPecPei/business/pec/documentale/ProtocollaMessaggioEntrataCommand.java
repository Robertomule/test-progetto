package it.eustema.inps.agentPecPei.business.pec.documentale;


import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPElement;

import org.apache.axis.message.SOAPHeaderElement;
import org.apache.axis.utils.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

import wspia2013.InteropEntrataResponse2013;
import wspia2013.Service1;
import wspia2013.Service1Locator;
import wspia2013.Service1Soap;
import wspia2013.Service1SoapStub;
import wspia.interoperabilita.BasicHttpBinding_IService1Stub;
import wspia.interoperabilita.jaxws.BasicHttpBinding_IService1Proxy;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.business.util.SendMailInterop2013Command;
import it.eustema.inps.agentPecPei.business.util.SendMailInteropCommand;
import it.eustema.inps.agentPecPei.dao.IndicePADAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.IndicePADTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.StaticUtil;
import org.apache.commons.io.*;

import sun.misc.BASE64Encoder;
/**
 * Classe Command che si occupa di protocollare in uscita il messaggio in input
 * e restituire un'istanza della SegnaturaDTO 
 * @author ALEANZI
 *
 */
public class ProtocollaMessaggioEntrataCommand implements Command {

	private MessaggioDTO messaggio;
	
	private static Logger log = Logger.getLogger(ProtocollaMessaggioEntrataCommand.class);
	
	public ProtocollaMessaggioEntrataCommand( MessaggioDTO messaggio ){
		this.messaggio = messaggio;
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub

		System.out.println("##############MESSAGGIOOO: " + messaggio);
		//genera l'xml per la protocollazione
		String xmlInter = "";
		System.out.println("\nEntro nel 'ProtocollaMessaggioEntrataCommand'");
		//System.out.println("generaXML = " + xml);
		//genera l'allegato txt che contiene il riepilogo di tutti i dati inviati nel messaggio
		MessaggiDAO messaggioDao = new MessaggiDAO();
		AllegatoDTO documentoPrimario = messaggioDao.creaMessageTxt(messaggio);
		//System.out.println("generaTxt = "+new String(documentoPrimario.getFileData()));
		AllegatoDTO allegatoPecDaProtocollare = new AllegatoDTO();
		if (!"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
			allegatoPecDaProtocollare = messaggioDao.creaMessagePecCompleto(messaggio);
		}
		
		//istanzia le classi costruite in base al wsdl per richiamare i metodi
		//esposti nel web service wspia 
		Service1 s1 = new Service1Locator();
		Service1Soap serviziWspia ;
		try{
			serviziWspia = s1.getService1Soap();
		}catch(Exception se){
			throw new NotifyMailBusinessException(-6002, "Errore nella chiamata al  il servizio Protocollazione Dettaglio Errore: " + se.getMessage());
		}
		//listaFile contiene il txt con il riepilogo del messaggio e altri eventuali allegati
		ArrayList<AllegatoDTO> listaFile = new ArrayList<AllegatoDTO>();
		//aggiunge il file txt
		listaFile.add(documentoPrimario);
		if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
			//di seguito aggiunge la lista di eventuali allegati al messaggio
			listaFile.addAll(messaggio.getElencoFileAllegati());		
		}
		
		BASE64Encoder encoder = new BASE64Encoder();
		String[] aosSegnatura = new String[listaFile.size()];
		//scorre tutti i file
		for(int j=0;j<listaFile.size();j++){
//			chiama l'encode Buffer per trasformare l'array di byte del singolo allegato
//			in una stringa codificata in base64. La stringa viene aggiunta alla j-esima posizione
//			dell'array
//			File da non passare per interoperabilit�
//			smime.p7s 
//			Segnatura.xml 
//			postacert.eml 
//			daticert.xml
			
			if(	!listaFile.get(j).getFileName().equalsIgnoreCase(ConfigProp.escludiInteropSmime) 
				&& !listaFile.get(j).getFileName().contains(ConfigProp.escludiInteropSmime) 
				&& !listaFile.get(j).getFileName().equalsIgnoreCase(ConfigProp.escludiInteropSegnatura) 
				&& !listaFile.get(j).getFileName().equalsIgnoreCase(ConfigProp.escludiInteropPostaCert) 
				&& !listaFile.get(j).getFileName().contains(ConfigProp.escludiInteropPostaCert) 
				&& !listaFile.get(j).getFileName().equalsIgnoreCase(ConfigProp.escludiInteropDatiCert) 
				&& !listaFile.get(j).getFileName().contains(ConfigProp.escludiInteropDatiCert) 
				&& !listaFile.get(j).getFileName().equalsIgnoreCase(ConfigProp.escludiInteropTestoDelMessaggio)
				)
			  {
				aosSegnatura[j] = encoder.encodeBuffer((listaFile.get(j).getFileData()));
			}
		}
		String xml = generaXML(messaggio, null, allegatoPecDaProtocollare, documentoPrimario);
		String xmlForzato = "";
		if("messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() ))
			xmlForzato = generaXML(messaggio, "800.010", allegatoPecDaProtocollare, documentoPrimario);
		
		String[] aos = new String[listaFile.size()];
		if ("messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
			int i = 0;
			//scorre tutti i file
			for(int j=0;j<listaFile.size();j++){
				//chiama l'encode Buffer per trasformare l'array di byte del singolo allegato
				//in una stringa codificata in base64. La stringa viene aggiunta alla j-esima posizione
				//dell'array
//				if(listaFile.get(j).getFileName().contains(ConfigProp.escludiInteropPostaCert) ){
					aos[i] = encoder.encodeBuffer((listaFile.get(j).getFileData()));
					i++;
//				}
			}
		}else{
//			Per la protocollazione senza interoperabilit�
			aos = new String[2];
			aos[0] = encoder.encodeBuffer(documentoPrimario.getFileData());
			aos[1] = encoder.encodeBuffer(allegatoPecDaProtocollare.getFileData());
		}
		
		//chiamata al wspia per la protocollazione del messaggio contenente gli allegati
		String result = "";
		InteropEntrataResponse2013 interopEntrata2013 = null;
		int nTry = 0;
		while ( nTry < ConfigProp.numeroTentativiProtocollazione  ) {
			try {
				nTry++;
				serviziWspia = s1.getService1Soap();
				boolean protocollaConInteroperabilita = false;
				
				messaggio.getElencoFileAllegati().add(documentoPrimario);
				System.out.println("Sono nel WHILE per i tentativi di protocollazione");
				for(AllegatoDTO allegato: messaggio.getElencoFileAllegati()){
					if(allegato.getFileName().equalsIgnoreCase("segnatura.xml")){
						protocollaConInteroperabilita = true;
						System.out.println("\n###########Creo XML\n");
						xmlInter = StaticUtil.decodicaCaratteriNonStampabiliCarattere(new String(allegato.getFileData()).replaceAll("\n", ""));
					}
				}
				
				CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
				SegnaturaDTO seg = new SegnaturaDTO();
				seg.setStatus(SegnaturaDTO.KO);
				
				boolean errorInterop = false;
				xml = StaticUtil.decodicaCaratteriNonStampabiliCarattere(xml);
				ByteArrayInputStream aProtoSemplice = new ByteArrayInputStream(xml.getBytes());
				Reader readerSemplice = new InputStreamReader(aProtoSemplice, "ISO-8859-1");
//				ByteArrayInputStream a = new ByteArrayInputStream(xmlInter.getBytes());

				System.out.println("###MESSAGGIO" + messaggio);
				messaggio.setDominoPec("inps.it");
				System.out.println("###Dominio PEC" + messaggio.getDominoPec());
				boolean verifica = messaggio.getDominoPec().contains(ConfigProp.dominioPEC.replace("@", ""));
				System.out.println("###Verifica" + verifica);

				if(!protocollaConInteroperabilita || messaggio.getDominoPec().contains(ConfigProp.dominioPEC.replace("@", ""))){
					System.out.println("IF");
					result = serviziWspia.protocollaConImmagineBlob64(ConfigProp.codAPP, ConfigProp.codAMM, messaggio.getCodiceAOO(), ConfigProp.codiceUtente, IOUtils.toString(readerSemplice), aos );
						executor.setCommand( new BuildSegnaturaDTOCommand( result ) );
						seg = (SegnaturaDTO)executor.executeCommand();
						if (!seg.isOK() && "messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
							aProtoSemplice = new ByteArrayInputStream(xmlForzato.getBytes());
							readerSemplice = new InputStreamReader(aProtoSemplice, "ISO-8859-1");
							result = serviziWspia.protocollaConImmagineBlob64(ConfigProp.codAPP, ConfigProp.codAMM, messaggio.getCodiceAOO(), ConfigProp.codiceUtente, IOUtils.toString(readerSemplice), aos );
							executor.setCommand( new BuildSegnaturaDTOCommand( result ) );
							seg = (SegnaturaDTO)executor.executeCommand();
						}
				}else{
					System.out.println("Entro qui...");
					JAXBContext contextInterop = JAXBContext.newInstance(generated.vo.wspia.inperoperabilita.segnatura.Segnatura.class);
					generated.vo.wspia.inperoperabilita.segnatura.Segnatura segInterop = new generated.vo.wspia.inperoperabilita.segnatura.Segnatura ();
					
					JAXBContext contextInterop2013 = JAXBContext.newInstance(it.inps.www.interop2013.Segnatura.class);
					it.inps.www.interop2013.Segnatura segInterop2013 = new it.inps.www.interop2013.Segnatura ();
					
					xmlInter = StaticUtil.decodicaCaratteriNonStampabiliCarattere(
							xmlInter.replaceAll("<!DOCTYPE Segnatura SYSTEM \"Segnatura.dtd\">", "").
										replaceAll("xmlns=\"http://www.digitPa.gov.it/protocollo/\"", ""));
					
					// Per la gestione dei caratte speciali
					ByteArrayInputStream aInter = new ByteArrayInputStream(xmlInter.getBytes());
					ByteArrayInputStream aInterProto = new ByteArrayInputStream(xmlInter.getBytes());
					ByteArrayInputStream aInter2013 = new ByteArrayInputStream(xmlInter.getBytes());
					ByteArrayInputStream aInterProto2013 = new ByteArrayInputStream(xmlInter.getBytes());
					Reader reader = new InputStreamReader(aInter, "ISO-8859-1");
					Reader protoReader = new InputStreamReader(aInterProto, "ISO-8859-1");
					Reader reader2013 = new InputStreamReader(aInter2013, "ISO-8859-1");
					Reader readerIntra2013 = new InputStreamReader(aInterProto2013, "ISO-8859-1");
					IndicePADAO indicePAdao = new IndicePADAO();
					
					boolean abilitaInterop2013 = false;
					
					if(indicePAdao.selectEmailInterop2013(messaggio.getReplyTo())>0) abilitaInterop2013 = true;
					try{
						if(!abilitaInterop2013){
							try{
								segInterop = (generated.vo.wspia.inperoperabilita.segnatura.Segnatura) contextInterop.createUnmarshaller().unmarshal(reader);
								result = serviziWspia.interopEntrataBlob64(ConfigProp.codAPP, ConfigProp.codAMM,  messaggio.getCodiceAOO(),  ConfigProp.codiceUtente, IOUtils.toString(protoReader), aosSegnatura);
								executor.setCommand( new BuildSegnaturaInterConfermaDTOCommand( result ) );
								seg = (SegnaturaDTO)executor.executeCommand();
							}catch(Exception e){
								log.error("Errore conversione per vecchia interoperabilit� : "+messaggio.getMessageId(), e);
							}
//							Verifico se la vecchia interoperabilit� ha funzionato
//							nel caso negativo ri-provo con la nuova
							if (!seg.isOK()){
								try{
									segInterop2013 = (it.inps.www.interop2013.Segnatura) contextInterop2013.createUnmarshaller().unmarshal(reader2013);
									Service1SoapStub proxy = new Service1SoapStub();
									SOAPHeaderElement header = new SOAPHeaderElement("http://www.inps.it", "Header");
									SOAPElement nodeName = header.addChildElement("AppName");
									nodeName.addTextNode(ConfigProp.codAPP);
									SOAPElement nodeKey = header.addChildElement("UserId");
									nodeKey.addTextNode(ConfigProp.codiceUtente);
									proxy.setHeader(header);
									interopEntrata2013 = proxy.interopEntrata2013(ConfigProp.codAMM,  messaggio.getCodiceAOO(),  IOUtils.toString(readerIntra2013), aosSegnatura);
									executor.setCommand( new BuildSegnaturaInterConfermaDTOCommand( interopEntrata2013 ) );
									seg = (SegnaturaDTO)executor.executeCommand();
									abilitaInterop2013 = true;
//									Se la nuova interoperabilit� ha avuto esito positivo allora aggiorno l'indirizzo pec delle mail con la nuova interoperabilit�
									if(seg.isOK()){
										if(indicePAdao.updateEmailInterop2013(messaggio)==0){
											indicePAdao.insertEmailInterop2013(messaggio);
										}
									}
								}catch(Exception e){
									log.error("Errore conversione per nuova interoperabilit� : "+messaggio.getMessageId(), e);
								}
							}
						}else{
							try{
								segInterop2013 = (it.inps.www.interop2013.Segnatura) contextInterop2013.createUnmarshaller().unmarshal(reader);
								Service1SoapStub proxy = new Service1SoapStub();
								SOAPHeaderElement header = new SOAPHeaderElement("http://www.inps.it", "Header");
								SOAPElement nodeName = header.addChildElement("AppName");
								nodeName.addTextNode(ConfigProp.codAPP);
								SOAPElement nodeKey = header.addChildElement("UserId");
								nodeKey.addTextNode(ConfigProp.codiceUtente);
								proxy.setHeader(header);
								interopEntrata2013 = proxy.interopEntrata2013(ConfigProp.codAMM,  messaggio.getCodiceAOO(),  IOUtils.toString(protoReader), aosSegnatura);
								executor.setCommand( new BuildSegnaturaInterConfermaDTOCommand( interopEntrata2013 ) );
								seg = (SegnaturaDTO)executor.executeCommand();
							}catch(Exception e){
								log.error("Errore conversione per nuova interoperabilit� : "+messaggio.getMessageId(), e);
							}
						}
						
						if(segInterop.getDescrizione()!=null && segInterop.getDescrizione().getDocumento()!=null)
							seg.setDocumentoPrincipale(segInterop.getDescrizione().getDocumento().getNome());

						if(segInterop2013.getDescrizione()!=null && segInterop2013.getDescrizione().getDocumento()!=null)
							seg.setDocumentoPrincipale(segInterop2013.getDescrizione().getDocumento().getNome());

						
						// Verifico se la protocollazione con iteroperabilit� non � stata eseguita correttemente
						if (!seg.isOK()){
							String descErrorInterop = "";
							JAXBContext contextException = JAXBContext.newInstance(it.inps.www.interop2013.eccezione.NotificaEccezione.class);
							it.inps.www.interop2013.eccezione.NotificaEccezione eccezione = new it.inps.www.interop2013.eccezione.NotificaEccezione();
							eccezione.setMotivo("File Segnatura.xml non conforme.");
							if(!abilitaInterop2013){
								descErrorInterop = seg.getDescErrore();
							}else{
								try{
									descErrorInterop = seg.getDescErrore();
									if(descErrorInterop !=null && descErrorInterop.getBytes()!=null){
										ByteArrayInputStream aInterProtoExce = new ByteArrayInputStream(descErrorInterop.getBytes());
										Reader readerExc = new InputStreamReader(aInterProtoExce, "ISO-8859-1");
										eccezione = (it.inps.www.interop2013.eccezione.NotificaEccezione) contextException.createUnmarshaller().unmarshal(readerExc);
									}else{
										eccezione.setMotivo("File Segnatura.xml non conforme.");
									}
								}catch(Exception e){
									log.error("Errore conversione per eccezione interoperabilit� : "+messaggio.getMessageId(), e);
								}
							}
							// Provo a protocolare con la protocollazione con immagine
							result = serviziWspia.protocollaConImmagineBlob64(ConfigProp.codAPP, ConfigProp.codAMM, messaggio.getCodiceAOO(), ConfigProp.codiceUtente, IOUtils.toString(readerSemplice), aos );
							executor.setCommand( new BuildSegnaturaDTOCommand( result ) );
							seg = (SegnaturaDTO)executor.executeCommand();
							executor.executeCommand();
							// Se la protocollazione con interoperabilit� non � andata a buon fine allora invio 
							// l'email con Eccezione se il mittente ha richiesto una conferma di ricezione 
//							if(seg.isOK()){
//								Se la protocollazione con immagine � stata eseguita correttamente 
//								invio il numero di protocollazione e l'eccezione dell'interoperabilit�
							if(seg.isOK()){
								if(!abilitaInterop2013)
									executor.setCommand(new SendMailInteropCommand( new NotifyMailBusinessException( -6002,  descErrorInterop), MessaggiDAO.creaAllegatoInterop("Eccezione", descErrorInterop, messaggio.getMessageId()), messaggio, segInterop, seg));
								else
									executor.setCommand(new SendMailInterop2013Command( new NotifyMailBusinessException( -6002, eccezione.getMotivo()), MessaggiDAO.creaAllegatoInterop2013("Eccezione", interopEntrata2013.getNotificaEccezione(), messaggio.getMessageId()), messaggio, segInterop2013, seg));
							}
							executor.executeCommand();
//							}else{
//								executor.setCommand(new SendMailInteropCommand( new NotifyMailBusinessException( -6002, descErrorInterop), MessaggiDAO.creaAllegatoInterop("Eccezione", result, messaggio.getMessageId()), messaggio, segInterop, seg));
//								executor.executeCommand();
//							}
						}else{
							// Se la protocollazione con interoperabilit�  � andata a buon fine allora invio
							// l'email con il ConfermaRicezione
							try{
								if(!abilitaInterop2013)
									executor.setCommand(new SendMailInteropCommand( new NotifyMailBusinessException( -6002, ""), MessaggiDAO.creaAllegatoInterop("ConfermaRicezione", result, messaggio.getMessageId()), messaggio, segInterop, seg));
								else
									executor.setCommand(new SendMailInterop2013Command( new NotifyMailBusinessException( -6002, ""), MessaggiDAO.creaAllegatoInterop("ConfermaRicezione", interopEntrata2013.getConfermaRicezione(), messaggio.getMessageId()), messaggio, segInterop2013, seg));
								
								executor.executeCommand();
								
							}catch(Exception eMailInterop){
								log.error("Errore per SendMailInteropCommand : "+eMailInterop.getMessage(), eMailInterop);
							}

						}
					}catch(Exception e){
						result = serviziWspia.protocollaConImmagineBlob64(ConfigProp.codAPP, ConfigProp.codAMM, messaggio.getCodiceAOO(), ConfigProp.codiceUtente, StaticUtil.decodicaCaratteriNonStampabili(xml.replaceAll("&", "e")), aos );
						executor.setCommand( new BuildSegnaturaDTOCommand( result ) );
						seg = (SegnaturaDTO)executor.executeCommand();
						if(!abilitaInterop2013){
							try{
								executor.setCommand(new SendMailInteropCommand( new NotifyMailBusinessException( -6002, e.getMessage()), MessaggiDAO.creaAllegatoInterop("Eccezione", result, messaggio.getMessageId()),messaggio,segInterop, seg));
								executor.executeCommand();
							}catch(Exception eMailInterop){
								log.error("Errore per SendMailInteropCommand : "+eMailInterop.getMessage(), eMailInterop);
							}
						}else{
							try{
								executor.setCommand(new SendMailInterop2013Command( new NotifyMailBusinessException( -6002, e.getMessage()), MessaggiDAO.creaAllegatoInterop("Eccezione", result, messaggio.getMessageId()),messaggio,segInterop2013, seg));
								executor.executeCommand();
							}catch(Exception eMailInterop){
								log.error("Errore per SendMailInteropCommand : "+eMailInterop.getMessage(), eMailInterop);
							}
						}
							
						

					}finally{
					}
				}
				if ( !seg.isOK() && ConfigProp.codiceErroreClassificaInesistente.equals( seg.getCodiceErrore() )){
					if ( !"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
						xml = generaXML(messaggio,ConfigProp.classificaDefaultPEC, allegatoPecDaProtocollare, documentoPrimario);	
					} else {
						xml = generaXML(messaggio,ConfigProp.classificaDefaultPEI, null, null);
					}
				}
				
				seg.setXmlInput( xml );
				
				return seg;
				
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				if ( nTry >= ConfigProp.numeroTentativiProtocollazione )
					throw new NotifyMailBusinessException(-6002, "Errore nella chiamata al  il servizio protocollaConImmagineBlob64 Dettaglio Errore: " + e.getMessage());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
					if ( nTry >= ConfigProp.numeroTentativiProtocollazione )
				throw new NotifyMailBusinessException(-6002, "Errore nella chiamata al  il servizio protocollaConImmagineBlob64 Dettaglio Errore: " + e.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				if ( nTry >= ConfigProp.numeroTentativiProtocollazione )
					throw new NotifyMailBusinessException(-6002, "Errore nella chiamata al  il servizio protocollaConImmagineBlob64 Dettaglio Errore: " + e.getMessage());
			}

			
		}
		
		return null;
		
	}
	
	
	private String generaXML(MessaggioDTO messaggio, String classificaForce, AllegatoDTO pecOriginaleDaProtocollare, AllegatoDTO documentoPrincipale) {
		
		String codiceSede = messaggio.getCodiceAOO(); 
		String nominativo = "INPS\\" + codiceSede + "\\" ;
		
		if(messaggio.getNomeSede()!=null && !messaggio.getNomeSede().equals("")){
			nominativo += messaggio.getNomeSede().replaceAll("&", "&amp;").
			replaceAll("/","-").
			replace(":"," ").
			replaceAll("�", "a").
			replaceAll("�", "u").
			replaceAll("�","i");
		}
		IndicePADAO indicePADao = new IndicePADAO();
		IndicePADTO indice = new IndicePADTO();
		
		if(messaggio.getNomeUfficio()!=null && !messaggio.getNomeUfficio().equals("")){
			nominativo += "\\" + messaggio.getNomeUfficio().replaceAll("&", "&amp;").
			replaceAll("/","-").
			replace(":"," ").
			replaceAll("�", "a").
			replaceAll("�", "u").
			replaceAll("�","i");
		}
		
		if ( "messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
					if(messaggio.getFromPhrase()!=null && !messaggio.getFromPhrase().equals("")){
						nominativo += "\\" + messaggio.getFromPhrase().replaceAll("&", "&amp;").
							replaceAll("/","-").
							replace(":"," ").
							replaceAll("�", "a").
							replaceAll("�", "u").
							replaceAll("�","i");
					}
		}
		String sendTo = null;
		String descSedeTo = null;
		String classifica = classificaForce != null ? classificaForce :  messaggio.getClassifica();
		
		if(classifica==null || classifica.equals("") || classifica.equals("0")){
			//se non � stato selezionato nessun valore prende il valore di default: 010.010
			if ( !"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() ))
				classifica = ConfigProp.classificaDefaultPEC;
			else 
				classifica = ConfigProp.classificaDefaultPEI;
		}else{
			//da specifica calcola la stringa da passare nell'xml, in base alla tipologia selezionata
			//classifica = "010.0"+classifica+"0";
		}
		
		String subject = messaggio.getSubject();
		//subject = subject.replaceAll("<", "&lt;");
		//subject = subject.replaceAll(">", "&gt;");
		subject = StringEscapeUtils.escapeXml(subject);

		String dataArrivo = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
		String nomeFile = null;
		String oggettoDoc = null;
		
		StringBuffer xml = new StringBuffer("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
		xml.append("<DatiInvio>");
		xml.append("<CategoriaProtocollo>E</CategoriaProtocollo>");
		try{
			if ( !"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
				indice = indicePADao.selectEmailIndicePA(messaggio.getReplyTo());
				if(indice!=null){
					xml.append("<Mittente><Tipo>M</Tipo>");
				}else{
					xml.append("<Mittente><Tipo>F</Tipo>");
				}
			}else 
				xml.append("<Mittente><Tipo>M</Tipo>");

		}catch(Exception e){
			xml.append("<Mittente><Tipo>A</Tipo>");
		}
//		xml.append("<Codice>").append(codiceSede).append("</Codice>");
				
		xml.append("<Nominativo>").append(messaggio.getReplyTo()).append("</Nominativo>");
		xml.append("</Mittente>");
		
		String confermaRicezione = messaggio.getConfermaRicezione()==null?"No":messaggio.getConfermaRicezione();
		xml.append("<ListaDestinatari>");
		
		List<DestinatarioDTO> destinatariPer = new ArrayList<DestinatarioDTO>();
		if ( messaggio.getElencoDestinatariPer() != null ){
			destinatariPer.addAll( messaggio.getElencoDestinatariPer() );
		}
		for(int i=0;i<destinatariPer.size();i++){
			if(!destinatariPer.get(i).getAddr().contains(messaggio.getAccountPec())){
				xml.append("<Destinatario confermaRicezione=\""+confermaRicezione+"\" TipoInvio=\"AA\">");
				try{
					if ( !"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
						indice = indicePADao.selectEmailIndicePA(messaggio.getReplyTo());
						if(indice!=null){
							xml.append("<Tipo>M</Tipo>");
						}else{
							xml.append("<Tipo>F</Tipo>");
						}
					}else xml.append("<Tipo>M</Tipo>");
				}catch(Exception e){
					
				}
				sendTo = destinatariPer.get(i).getCodiceAOO(); //messaggio.getCodiceAOO();//Verificare
				descSedeTo = destinatariPer.get(i).getName();
				if ( descSedeTo == null ) 
					descSedeTo = "";
				if(descSedeTo.indexOf("/")!=-1){
					descSedeTo = descSedeTo.substring(descSedeTo.indexOf("/"));
				}
				xml.append("<Codice>INPS.").append(sendTo.replaceAll("&", " ")).append("</Codice>");
				xml.append("<Nominativo>INPS\\").append(descSedeTo.replaceAll("&", " ")).append("</Nominativo>");
				// TODO Controllare se email letta correttamente
				xml.append("<EMail>").append(destinatariPer.get(i).getAddr()).append("</EMail>");
				xml.append("</Destinatario>");
			}
		}
	
		List<DestinatarioDTO> destinatariCc = new ArrayList<DestinatarioDTO>();
//		destinatari = new ArrayList<DestinatarioDTO>();
		if ( messaggio.getElencoDestinatariCc() != null ){
			destinatariCc.addAll( messaggio.getElencoDestinatariCc() );
		}
		
		if(!destinatariCc.isEmpty()){
			//aggiunge l'elenco dei destinatari
			
			for(int i=0;i<destinatariCc.size();i++){
				if(!destinatariCc.get(i).getAddr().contains(messaggio.getAccountPec())){
					xml.append("<Destinatario confermaRicezione=\""+confermaRicezione+"\" TipoInvio=\"Cc\">");
					
					try{
						if ( !"messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
							indice = indicePADao.selectEmailIndicePA(messaggio.getReplyTo());
							if(indice!=null){
								xml.append("<Tipo>M</Tipo>");
							}else{
								xml.append("<Tipo>F</Tipo>");
							}
						}else xml.append("<Tipo>M</Tipo>");
					}catch(Exception e){
						
					}
					sendTo = destinatariCc.get(i).getCodiceAOO();//Verificare
					descSedeTo = destinatariCc.get(i).getName();
					if ( descSedeTo == null ) 
						descSedeTo = "";
					if(descSedeTo.indexOf("/")!=-1){
						descSedeTo = descSedeTo.substring(descSedeTo.indexOf("/"));
					}
					xml.append("<Codice>INPS.").append(sendTo.replaceAll("&", " ")).append("</Codice>");
					xml.append("<Nominativo>INPS\\").append(descSedeTo.replaceAll("&", " ")).append("</Nominativo>");
					// TODO Controllare se email letta correttamente
					xml.append("<EMail>").append(destinatariCc.get(i).getAddr()).append("</EMail>");
					xml.append("</Destinatario>");
				}
			}
		}
		xml.append("</ListaDestinatari>");
		
		xml.append("<ListaDocumenti>");
		
		if ("messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() ))
			xml.append("<Documento NomeFile=\"TestoDelMessaggio.txt\">");
		else
			xml.append("<Documento NomeFile=\"TestoDelMessaggio.txt\">");	
		
		xml.append("<Sensibile>"+messaggio.getSensibile()+"</Sensibile>");
		xml.append("<Riservato>"+messaggio.getRiservato()+"</Riservato>");
		xml.append("<FlagProtocollo>S</FlagProtocollo>");
		xml.append("<Classifica>").append(classifica).append("</Classifica>");
		xml.append("<Oggetto>").append(subject.replaceAll("&", "e").
				replaceAll("/","-").
				replace(":"," ").
				replaceAll("�", "a").
				replaceAll("�", "u").
				replaceAll("�", "e").
				replaceAll("�","i")).append("</Oggetto>");
		xml.append("<ProtocolloMittente Data=\"\" Protocollo=\"\"/>");
		xml.append("<DataArrivo>").append(dataArrivo).append("</DataArrivo>");
		xml.append("<ListaSoggettiAfferenti>").append("</ListaSoggettiAfferenti>");
		xml.append("<ListaAllegati>");
		
		if ("messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() )){
			if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
				for(int i=0;i<messaggio.getElencoFileAllegati().size();i++){
					nomeFile = StaticUtil.removeAllSpecialChar( StaticUtil.subString( messaggio.getElencoFileAllegati().get(i).getFileName().replaceAll(":", ""), 247 ));
					oggettoDoc = nomeFile + "(" + messaggio.getElencoFileAllegati().get(i).getFileSize() + 
						" - " + nomeFile + ")";
					xml.append("<Documento NomeFile=\"").append(nomeFile).append("\">");
					xml.append("<Oggetto>").append(oggettoDoc).append("</Oggetto>");
					xml.append("</Documento>");
				}
//				for(int i=0;i<messaggio.getElencoFileAllegati().size();i++){
//					nomeFile = messaggio.getElencoFileAllegati().get(i).getFileName();
//						oggettoDoc = nomeFile + "(" + messaggio.getElencoFileAllegati().get(i).getFileSize() + 
//							" - " + nomeFile + ")";
//						if(oggettoDoc.length()>215)
//							oggettoDoc = nomeFile + "(" + messaggio.getElencoFileAllegati().get(i).getFileSize()+ ")";
//						xml.append("<Documento NomeFile=\"").append(
//								nomeFile.replaceAll("&", "e").
//								replaceAll("/","-").
//								replace(":"," ").
//								replaceAll("�", "a").
//								replaceAll("�", "u").
//								replaceAll("�","i").
//								replaceAll("�","e")
//								).append("\">");
//						xml.append("<Oggetto>").append(oggettoDoc.replaceAll("&", "e")).append("</Oggetto>");
//						xml.append("</Documento>");
//					}
			}			
		}else{
			// Nel caso di pec in entrata sar� protocollato come alleggato 
			// esclusivamente il messaggio pec originale
			nomeFile = ConfigProp.escludiInteropPostaCert;
			oggettoDoc = nomeFile + "(" + pecOriginaleDaProtocollare.getFileSize() + " - " + nomeFile + ")";
			if(oggettoDoc.length()>215)
				oggettoDoc = nomeFile + "(" + pecOriginaleDaProtocollare.getFileSize()+ ")";
				
			xml.append("<Documento NomeFile=\"").append(ConfigProp.escludiInteropPostaCert).append("\">");
			xml.append("<Oggetto>").append(oggettoDoc.replaceAll("&", "e")).append("</Oggetto>");
			xml.append("</Documento>");
		}
		xml.append("</ListaAllegati>");
		xml.append("</Documento>");
		xml.append("</ListaDocumenti>");
		xml.append("</DatiInvio>");
		return xml.toString();
	}
	
	private String generaXMLInterop(MessaggioDTO messaggio, generated.vo.wspia.inperoperabilita.segnatura.Segnatura segnaturaInterop) {
		
		String codiceSede = messaggio.getCodiceAOO();
		String nominativo = "INPS\\" + codiceSede + "\\" + messaggio.getNomeSede();
	
		if(messaggio.getNomeUfficio()!=null && !messaggio.getNomeUfficio().equals("")){
			nominativo += "\\" + messaggio.getNomeUfficio();
		}
		
		if ( "messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio() ))
			nominativo += "\\" + messaggio.getFromPhrase();
		
		String sendTo = null;
		String descSedeTo = null;
		String classifica = ConfigProp.classificaDefaultPEI;
		String subject = messaggio.getSubject();
		subject = StringEscapeUtils.escapeXml(subject);
        
		
		String dataArrivo = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
		String nomeFile = null;
		String oggettoDoc = null;
		
		StringBuffer xml = new StringBuffer("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
		xml.append("<DatiInvio>");
		xml.append("<CategoriaProtocollo>E</CategoriaProtocollo>");
		xml.append("<Mittente><Tipo>M</Tipo>");
		xml.append("<Codice>").append(codiceSede).append("</Codice>");
				
		xml.append("<Nominativo>").append(nominativo).append("</Nominativo>");
		xml.append("</Mittente>");
		
		List<DestinatarioDTO> destinatari = new ArrayList<DestinatarioDTO>();
		if ( messaggio.getElencoDestinatariPer() != null ){
			destinatari.addAll( messaggio.getElencoDestinatariPer() );
		}
		xml.append("<ListaDestinatari>");
		destinatari = new ArrayList<DestinatarioDTO>();
		if ( messaggio.getElencoDestinatariCc() != null ){
			destinatari.addAll( messaggio.getElencoDestinatariCc() );
		}
		
		if(!destinatari.isEmpty()){
			//aggiunge l'elenco dei destinatari
			
			for(int i=0;i<destinatari.size();i++){
				xml.append("<Destinatario confermaRicezione=\"no\" TipoInvio=\"CC\">");
				xml.append("<Tipo>M</Tipo>");
				sendTo = destinatari.get(i).getCodiceAOO();//Verificare
				descSedeTo = destinatari.get(i).getName();
				if ( descSedeTo == null ) 
					descSedeTo = "";
				if(descSedeTo.indexOf("/")!=-1){
					descSedeTo = descSedeTo.substring(descSedeTo.indexOf("/"));
				}
				
				xml.append("<Codice>INPS.").append(sendTo).append("</Codice>");
				xml.append("<Nominativo>INPS\\").append(descSedeTo).append("</Nominativo>");
				// TODO Controllare se email letta correttamente
				xml.append("<EMail>").append(destinatari.get(i).getAddr()).append("</EMail>");
				xml.append("</Destinatario>");
			}
		}
		xml.append("</ListaDestinatari>");
		xml.append("<ListaDocumenti>");
		
		if ("posta-certificata".equalsIgnoreCase( messaggio.getTipoMessaggio() ))
			xml.append("<Documento NomeFile=\"TestoDelMessaggio.txt\">");
		else 
			xml.append("<Documento NomeFile=\"TestoDelMessaggio.txt\">");
		
		xml.append("<Sensibile>"+messaggio.getSensibile()+"</Sensibile>");
		xml.append("<Riservato>"+messaggio.getRiservato()+"</Riservato>");
		xml.append("<FlagProtocollo>S</FlagProtocollo>");
		//classifica, se vuoto mette classifica="010.010"
		xml.append("<Classifica>").append(classifica).append("</Classifica>");
		xml.append("<Oggetto>").append(subject).append("</Oggetto>");
		xml.append("<ProtocolloMittente Data=\""+segnaturaInterop.getIntestazione().getIdentificatore().getDataRegistrazione()+"\" Protocollo=\""+segnaturaInterop.getIntestazione().getIdentificatore().getNumeroRegistrazione()+"\"/>");
		xml.append("<DataArrivo>").append(dataArrivo).append("</DataArrivo>");
		xml.append("<ListaSoggettiAfferenti>").append("</ListaSoggettiAfferenti>");
		xml.append("<ListaAllegati>");
		if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
			for(int i=0;i<messaggio.getElencoFileAllegati().size();i++){
				nomeFile = messaggio.getElencoFileAllegati().get(i).getFileName();
				oggettoDoc = nomeFile + "(" + messaggio.getElencoFileAllegati().get(i).getFileSize() + 
					" - " + nomeFile + ")";
				xml.append("<Documento NomeFile=\"").append(nomeFile).append("\">");
				xml.append("<Oggetto>").append(oggettoDoc).append("</Oggetto>");
				xml.append("</Documento>");
			}
		}
		xml.append("</ListaAllegati>");
		xml.append("</Documento>");
		xml.append("</ListaDocumenti>");
		xml.append("</DatiInvio>");
		return xml.toString();
	}
	
	
	//genera il contenuto del file di testo da allegare come riepilogo
	//del messaggio che si sta inviando. Restituisce un oggetto Allegato
	private AllegatoDTO generaTxt(MessaggioDTO messaggio) {
		AllegatoDTO allegato = new AllegatoDTO();
		String codiceSede = messaggio.getCodiceAOO();
		
		String nominativo = "INPS\\" + codiceSede + "\\" + messaggio.getNomeSede();
		
		if(messaggio.getNomeUfficio()!=null && !messaggio.getNomeUfficio().equals("")){
			nominativo += "\\" + messaggio.getNomeUfficio();
		}
		nominativo += "\\" + messaggio.getFromPhrase();
		
		
		StringBuffer testoRiepilogo = new StringBuffer("HERMES-Messaggistica Ufficiale\n\n");
		testoRiepilogo.append("Mittente: INPS.").append(codiceSede).append("\n\n");
		testoRiepilogo.append(nominativo).append("\n\n");
		//testoRiepilogo.append("Destinatari: ").append(messaggio.getRiepilogoDestinatari()).append("\n\n");
		testoRiepilogo.append("Destinatari: ").append("\n\n");
		
		
		List<DestinatarioDTO> destinatari = new ArrayList<DestinatarioDTO>();
		if ( messaggio.getElencoDestinatariPer() != null ){
			destinatari.addAll( messaggio.getElencoDestinatariPer() );
		}
		if ( messaggio.getElencoDestinatariCc() != null ){
			destinatari.addAll( messaggio.getElencoDestinatariCc() );
		}
		
		if(!destinatari.isEmpty()){
			String dest = "";
			for(int i=0;i<destinatari.size();i++){
				dest = destinatari.get(i).getName();
				testoRiepilogo.append(dest).append("\n");
			}
		}
		testoRiepilogo.append("\n\n\n");
		testoRiepilogo.append("OGGETTO: ").append(messaggio.getSubject()).append("\n\n");
		testoRiepilogo.append("TESTO\n\n");
		testoRiepilogo.append(messaggio.getBody()).append("\n\n");
		
		if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty()){
			testoRiepilogo.append("Sono presenti i seguenti allegati:\n\n");
			String nomeFile="";
			String oggettoDoc="";
			for(int i=0;i<messaggio.getElencoFileAllegati().size();i++){
				nomeFile = messaggio.getElencoFileAllegati().get(i).getFileName();
				oggettoDoc = nomeFile + "(" + messaggio.getElencoFileAllegati().get(i).getFileSize() + 
					" - " + nomeFile + ")";
				
				testoRiepilogo.append("[").append(oggettoDoc).append("]\n");
			}
		}
		allegato.setFileName("msghermes.txt");
		//converte in array di byte la stringa con i contenuti del messaggio
		allegato.setFileData((testoRiepilogo.toString()).getBytes());
		
		allegato.setContentType("text/html");
		
		return allegato;
	}
	
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "WSPIA Messaggio in Entrata";
	}
	
	

}
