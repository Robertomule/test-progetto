package it.eustema.inps.agentPecPei.business.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;

public class RetrieveUiDocFromMessageIDCommand implements Command {
	private String input;
	
	public RetrieveUiDocFromMessageIDCommand( String input ){
		this.input = input;
	}
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		 String out = "";
		 try{
			 out = input.substring(1, input.lastIndexOf("@") );
		 }catch(Exception e){
			 out = input;
		 }
		byte[] bytesOfMessage;
		try {
			bytesOfMessage = out.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);
			final StringBuilder builder = new StringBuilder();

			for(byte b : thedigest) {
		        builder.append(String.format("%02x", b));
		    }
			out = builder.toString();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		return out;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Recupero UIDOC dal Message ID";
	}

}
