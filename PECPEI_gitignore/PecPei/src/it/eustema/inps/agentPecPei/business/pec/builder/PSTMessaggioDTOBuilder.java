package it.eustema.inps.agentPecPei.business.pec.builder;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.commons.lang3.StringUtils;

import com.pff.PSTAttachment;
import com.pff.PSTMessage;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.cert.RetrieveCertAuthDTOCommand;
import it.eustema.inps.agentPecPei.business.pec.filler.PSTMessaggioDTOHeaderFiller;
import it.eustema.inps.agentPecPei.business.pec.pst.RetrieveCertAuthDTOFromPSTCommand;
import it.eustema.inps.agentPecPei.business.util.RetrieveReplyToFromFromCommand;
import it.eustema.inps.agentPecPei.business.util.RetrieveUiDocFromMessageIDCommand;
import it.eustema.inps.agentPecPei.dao.IndicePADAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.CertAuthDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.IndicePADTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;

/**
 * Classe che si occupa di costruire il MessaggioDTO, da salvare sul DB, a partire da un oggetto
 * mail di tipo PSTMessage in entrata
 * @author ALEANZI
 *
 */
public class PSTMessaggioDTOBuilder implements Command {
	
	/* Oggetto mail contenente tutti i dati della PEC ricevuta
	 * il formato � PSTMessage
	*/
	public PSTMessaggioDTOBuilder(){}
	
	private PSTMessage mail;
	private String codiceAOO;
	private String verso;
	private String accountPecPilota;
	
	public PSTMessaggioDTOBuilder( PSTMessage mail, String codiceAOO, String verso, String accountPecPilota ){
		this.mail = mail;
		this.codiceAOO = codiceAOO;
		this.verso = verso;
		this.accountPecPilota = accountPecPilota;
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		//Creazione dell'oggetto MessaggioDTO
		MessaggioDTO messaggio = new MessaggioDTO();
		messaggio.setElencoFileAllegati( new ArrayList<AllegatoDTO>() );
		messaggio.setElencoDestinatariPer( new ArrayList<DestinatarioDTO>() );
		
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		String from = readHeaders("From:", mail);
		
		
		try{
		
		//1. UIDOC
		executor.setCommand( new RetrieveUiDocFromMessageIDCommand( mail.getInternetMessageId() ) );
		messaggio.setUiDoc( (String)executor.executeCommand() );
		
		if ( messaggio.getUiDoc().equals("fd05292e55b614d1a45e4f05d7af85b7") ){
			System.out.println("ASDASDASD");
		}
		
		 //2. ACCOUNT PEC
        executor.setCommand( new RetrieveReplyToFromFromCommand( from.trim() ) );
        messaggio.setAccountPec( (String)executor.executeCommand() );
		
		//3. DOMINIO PEC
		String dominioPec = from.substring(from.lastIndexOf("@")+1,from.length()).replaceAll("\"","").replaceAll(">","").trim();
		if(dominioPec==null||dominioPec.equals(""))
			dominioPec = from;
		messaggio.setDominoPec(dominioPec.substring(dominioPec.lastIndexOf("@")+1,dominioPec.length()).replaceAll("\"",""));
		
		//4. VERSO
//		messaggio.setVerso( verso );
		messaggio.setVerso(from.toUpperCase().contains(accountPecPilota.toUpperCase())?"U":"E");
		
		//5. COD AOO
		messaggio.setCodiceAOO( codiceAOO );
		
		//6. MESSAGEID
		messaggio.setMessageId( mail.getInternetMessageId() );
		
		//11. FROMADDR
		messaggio.setFromAddr( mail.getSenderEmailAddress().trim() );
		
		//12. FROMPHRASE
        messaggio.setFromPhrase( from.trim() );
        messaggio.setPemlFrom( from.trim() );
        
        //13. REPLYTO
        //String replyTo = from;
        String replyTo =null;
        if ( messaggio.getFromPhrase().indexOf("\"Per conto di: ") != -1){
        	replyTo = messaggio.getFromPhrase().substring(0, messaggio.getFromPhrase().indexOf("<", 0));
        } else {
        	replyTo = messaggio.getFromAddr();	
        }
        
        
        if ( replyTo.lastIndexOf("\"") != -1) 
        	replyTo = replyTo.substring(0,from.lastIndexOf("\"")); 
        //executor.setCommand( new RetrieveReplyToFromFromCommand( replyTo.replaceAll("<", "").replaceAll(">", "").replaceAll("\"", "").replaceAll("Per conto di:", "").trim() ));
        //messaggio.setReplyTo( (String)executor.executeCommand() );
        messaggio.setReplyTo( replyTo.replaceAll("<", "").replaceAll(">", "").replaceAll("\"", "").replaceAll("Per conto di:", "").trim() );
        //14. POSTEDDATE E DELIVEREDDATE
		messaggio.setPostedDate( mail.getMessageDeliveryTime() );
		messaggio.setDeliveredDate( mail.getMessageDeliveryTime() );
		messaggio.setPemlPostedDate( mail.getMessageDeliveryTime() );
	
		
		
		//16. SUBJECT
		messaggio.setSubject( mail.getSubject() );
		messaggio.setPemlSubject( mail.getSubject() );
		messaggio.setPostaCertEMLSubject( mail.getSubject() );
		
		//17. XTIPORICEVUTA
		String tipoRicevuta = readHeaders("X-TipoRicevuta:", mail)+readHeaders("X-Tiporicevuta:", mail);
        if( tipoRicevuta != null && !tipoRicevuta.isEmpty() )
        	messaggio.setTipoRicevuta(tipoRicevuta.trim());
        else
        	messaggio.setTipoRicevuta("breve");
		
        //18. RISERVATO
        messaggio.setRiservato("N");
        
        //19. SENSIBILE
        messaggio.setSensibile("N");
        
        //20. CLASSIFICA
        messaggio.setClassifica("800.020");
        
        //21. DES CLASSIFICA
        messaggio.setDesClassifica("Posta Elettronica Certificata");
		
        //22. BODY
        messaggio.setBody( mail.getBody() );
        
        //23. DOCUMENTO PRINCIPALE
        //TODO nome documento principale

        
		//24. ISREPLY
		messaggio.setIsReply("0");
		
		//25.INREPLYTO
		String inReplyTo="";
        if(from!=null && from.contains("\""))
        	inReplyTo = from.substring(from.lastIndexOf("\""), from.length()).replaceAll("<", "").replaceAll(">", "").replaceAll("\"", "").trim();
        else
        	inReplyTo =from.trim();
        
        messaggio.setInReplyTo(inReplyTo);
		
		//28. HEADERS
		messaggio.setHeaders( mail.getTransportMessageHeaders() );
		
		//31. XRICEVUTA
		String xricevuta = readHeaders("X-Ricevuta", mail );
        if( xricevuta != null && !xricevuta.isEmpty() )
        	messaggio.setRicevuta(xricevuta.replaceAll(": ", "").trim());
		
		//29. TIPOLOGIA
		if(messaggio.getRicevuta()!=null){
			messaggio.setTipologia("X-Ricevuta");
			messaggio.setTipoMessaggio(messaggio.getRicevuta());
			messaggio.setTipoMitt("A");
		}else{
			if(messaggio.getXTrasporto()!=null&&messaggio.getXTrasporto().equals("errore")){
				messaggio.setTipologia("X-Ricevuta");
				messaggio.setRicevuta("errore-consegna");
				messaggio.setTipoMessaggio("errore-consegna");
				messaggio.setTipoMitt("A");
			}else{
				messaggio.setTipologia(Constants.TIPOLOGIADB_XTRASPORTO);
				messaggio.setTipoMessaggio(messaggio.getXTrasporto());
				IndicePADAO indicePADao = new IndicePADAO();
				IndicePADTO indice = indicePADao.selectEmailIndicePA(messaggio.getReplyTo());
				if(indice!=null)
					messaggio.setTipoMitt("M");
				else
					messaggio.setTipoMitt("F");
				}
		}
		
		
		//30. XTRASPORTO
		String xtrasporto = readHeaders("X-Trasporto:", mail);
		
        if( xtrasporto != null && !xtrasporto.isEmpty() )
        	messaggio.setXTrasporto(xtrasporto.trim());
        
        
        
        //32. TIPO MESSAGGIO
		// messaggio.setTipoMessaggio("posta-certificata");
        messaggio.setTipoMessaggio(readHeaders("X-Trasporto:", mail)==null?"posta-certificata":readHeaders("X-Trasporto:", mail).trim());
        try{
        if ( mail.hasAttachments() ){
    		PSTAttachment att = null;
    		for ( int i = 0 ; i<mail.getNumberOfAttachments(); i++){
    			executor.setCommand( new PSTAttachmentAllegatoDTOBuilder(mail.getAttachment( i ), messaggio) );
    			AllegatoDTO allegato = (AllegatoDTO)executor.executeCommand();
    			if(allegato.getFileName()!=null && !allegato.getFileName().equals(""))
    				messaggio.getElencoFileAllegati().add( allegato );
    			else{
    				try{
	    				ByteArrayInputStream bo = new ByteArrayInputStream(allegato.getFileData());
	    				MimeBodyPart m = new MimeBodyPart(bo);
//	    				Multipart mp = new MimeMultipart(bo.toString());
	    				elabPostaCertEML(messaggio, m,messaggio.getElencoFileAllegati());
    				}catch(Exception e){}
    				
    			}
    		}
        }
        }catch(Exception e){}
    	CertAuthDTO certAuthDTO = null;
    		
		
    	if ( "E".equalsIgnoreCase( messaggio.getVerso() )) {
	    	for (AllegatoDTO allegato : messaggio.getElencoFileAllegati()) {
				if (allegato.getTipoAllegato().indexOf("p7") != -1) {
					executor.setCommand(new RetrieveCertAuthDTOCommand(allegato
							.getFileData()));
					certAuthDTO = (CertAuthDTO) executor.executeCommand();
					//break;
				} 
				
				if ( "DATICERT.XML".equalsIgnoreCase( allegato.getFileName() ) ){
					messaggio.setDatiSignature(new String(allegato.getFileData()));
				}
				
			}
    	}
    	
		// 41. SignValidFromDate
		if (certAuthDTO != null) {
			messaggio.setSignValidFromDate(certAuthDTO.getSignValidFrom());
			messaggio.setSignIssuerName(certAuthDTO.getIssuerName());
			messaggio.setSignSubjectName(certAuthDTO.getSubjectName());
			messaggio.setIsSigned(certAuthDTO.getSigned());
			messaggio.setIsSignedVer(certAuthDTO.getSignedVer());
		}
		// 42. SignValidToDate
		if (certAuthDTO != null) {
			messaggio.setSignValidToDate(certAuthDTO.getSignValidTo());
		}

		// 52. PostacertEml_SignValidFromDate
		if (certAuthDTO != null) {
			messaggio.setPemlSignValidFromDate(certAuthDTO
					.getSignValidFrom());
		}

		// 53. PostacertEml_SignValidToDate
		if (certAuthDTO != null) {
			messaggio.setPemlSignValidToDate(certAuthDTO.getSignValidTo());
		}

        //54. STATO
		messaggio.setStato(from.contains(dominioPec)?Constants.STATO_RICEVUTO:Constants.STATO_INVIATO);
		
		//73. DESMITT
		messaggio.setDesMitt(dominioPec.substring(0,dominioPec.lastIndexOf("@")+1).replaceAll("@","").replaceAll("Per conto di:", "").replaceAll("\"", "").trim());

		
		if(mail.isRead())
			messaggio.setStatoLettura("Letto");
		
		messaggio.setIsSPAMorCCN(0);
		
		//Valorizzazione di tutti i campi ottenibili dall'Header
		PSTMessaggioDTOHeaderFiller headerFiller = new PSTMessaggioDTOHeaderFiller();
		headerFiller.setMessaggioDTO( messaggio );
		headerFiller.setPSTMessage( mail );
		headerFiller.fill();
		
		//Destinatari
		/*List<String> listaDestinatari = readHeaders("To", mail,0);
		for ( String s: listaDestinatari ){
			DestinatarioDTO dest = new DestinatarioDTO();
			dest.setAddr(s);
			dest.setMessageID( messaggio.getMessageId() );
			dest.setType("Per");
			messaggio.getElencoDestinatariPer().add( dest );
		
		}*/
		
		
		
		boolean	flagCCn = false;

		// Destinatari
		List<String> listaDestinatari = readHeaders("To", mail,0);
		if(listaDestinatari!=null && !listaDestinatari.isEmpty()){
			StringTokenizer stdest = new StringTokenizer(listaDestinatari.get(0), ";|,");
			while(stdest.hasMoreElements()){
				String s = (String)stdest.nextElement(); 
				String addr = s.indexOf("<") == -1 ? s : s.substring(s
						.indexOf("<") + 1, s.length() - 1);
				String name = s.indexOf("<") != -1 ? s.substring(0, s
						.indexOf("<")) : "";
				DestinatarioDTO dest = new DestinatarioDTO();
				if((accountPecPilota+ConfigProp.dominioPEC).equals(addr))
					flagCCn = true;
				dest.setAddr(addr.replaceAll(": ", ""));
				dest.setName(name.replaceAll(": ", "").replaceAll(":", ""));
				dest.setPhrase(s.replaceAll(": ", ""));
				dest.setMessageID(messaggio.getMessageId());
				dest.setType("Per");
				messaggio.getElencoDestinatariPer().add(dest);
			}
		}
		List<String> listaDestinatariCC = readHeaders("Cc",mail, 0);
		if(listaDestinatariCC!=null && !listaDestinatariCC.isEmpty()){
			StringTokenizer stdest = new StringTokenizer(listaDestinatariCC.get(0), ";|,");
			while(stdest.hasMoreElements()){
				String s = (String)stdest.nextElement(); 
				String addr = s.indexOf("<") == -1 ? s : s.substring(s
						.indexOf("<") + 1, s.length() - 1);
				String name = s.indexOf("<") != -1 ? s.substring(0, s
						.indexOf("<")) : "";
				DestinatarioDTO dest = new DestinatarioDTO();
				if((accountPecPilota+ConfigProp.dominioPEC).equals(addr))
					flagCCn = true;
				
				dest.setAddr(addr);
				dest.setName(name);
				dest.setPhrase(s);
				dest.setMessageID(messaggio.getMessageId());
				dest.setType("cc");
				messaggio.getElencoDestinatariCc().add(dest);
			}
		}
		
		
		boolean ccn = true;
		if ( "E".equalsIgnoreCase( messaggio.getVerso() ) ){
			
		}
		
		
		if( (messaggio.getSubject()!=null & (messaggio.getSubject().contains("****SPAM****"))) || (!flagCCn && "E".equalsIgnoreCase(messaggio.getVerso() )))
			messaggio.setIsSPAMorCCN(1);
		else
			messaggio.setIsSPAMorCCN(0);
		
		
		
		
		
		
		
		} catch ( Exception e ){
			throw new BusinessException( -7878, "Errore nel messaggio: \n OGGETTO: " + mail.getSubject() + "\n" + "DATA: " + mail.getMessageDeliveryTime() + "\n" + "Dettaglio errore: " + e.getLocalizedMessage() + "\n\n\n");
		}
		
		return messaggio;
	}
	
	
	
//	private List<AllegatoDTO> getAllegati(MessaggioDTO messaggioDTO)
//	throws BusinessException {
//		List<AllegatoDTO> out = new ArrayList<AllegatoDTO>();
//		AllegatoDTO allegato = null;
//		try {
//			Multipart multipart = (Multipart) mail.getContent();
//			addMultipartAttach(multipart, messaggioDTO, out);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (MessagingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//			
//		return out;
//	}
	private void addMultipartAttach(Multipart multipart,
			MessaggioDTO messaggioDTO, List<AllegatoDTO> listaAllegati)
			throws NotifyMailBusinessException {
		InputStream is = null;

		AllegatoDTO allegato = null;

		try {

			for (int i = 0; i < multipart.getCount(); i++) {
				Part bodyPart = multipart.getBodyPart(i);
				if (!Part.ATTACHMENT
						.equalsIgnoreCase(bodyPart.getDisposition())
						&& !StringUtils.isNotBlank(bodyPart.getFileName())) {

					Object mm = bodyPart.getContent();
					if (mm instanceof Multipart)
						addMultipartAttach((Multipart) mm, messaggioDTO,
								listaAllegati);
					else if (mm instanceof String) {
						messaggioDTO.setBodyHtml((String) mm);
						messaggioDTO.setBody((String) mm);
					}

					continue;
				}

				is = bodyPart.getInputStream();
				ByteArrayOutputStream aos = new ByteArrayOutputStream();
				byte[] buf = new byte[4096];
				int bytesRead;

				while ((bytesRead = is.read(buf)) != -1) {
					aos.write(buf, 0, bytesRead);
				}

				allegato = new AllegatoDTO();
				allegato.setContentType(bodyPart.getContentType());
				allegato.setFileName(bodyPart.getFileName() != null ? MimeUtility.decodeText(bodyPart.getFileName()): "");
				allegato.setFileData(aos.toByteArray());
				allegato.setFileSize(allegato.getFileData().length);
				allegato.setMessageID(messaggioDTO.getMessageId());
				allegato.setTipoAllegato(allegato.getFileName()
						.lastIndexOf(".") != -1 ? allegato.getFileName()
						.substring(allegato.getFileName().lastIndexOf(".") + 1)
						: "");

				aos.close();
				is.close();
				listaAllegati.add(allegato);

				if ("postacert.eml".equalsIgnoreCase(allegato.getFileName())) {

//					File f = new File("C:\\testPdf\\postacert.txt");
//					FileOutputStream fo = new FileOutputStream(f);
//					fo.write(allegato.getFileData());
//					fo.flush();
//					fo.close();

					elabPostaCertEML(messaggioDTO, bodyPart, listaAllegati);
				}
			}
		} catch (IOException e) {
			throw new NotifyMailBusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio: ["
							+ messaggioDTO.getMessageId()
							+ "] Dettaglio Errore: " + e.getMessage());
		} catch (MessagingException e) {
			throw new NotifyMailBusinessException(-8000,
					"Errore nel recupero degli allegati del messaggio: ["
							+ messaggioDTO.getMessageId()
							+ "] Dettaglio Errore: " + e.getMessage());
		}

	}

	public void elabPostaCertEML(MessaggioDTO messaggio, Part bodyPart,
			List<AllegatoDTO> listaAllegati) throws NotifyMailBusinessException {
		try {
			Object content = bodyPart.getContent();
			if (content instanceof MimeMessage) {
				content = ((MimeMessage) content).getContent();
			}

			if (content instanceof String) {
				messaggio.setPostaCertEMLBody((String) content);
			} else if (content instanceof Multipart) {
				for (int i = 0; i < ((Multipart) content).getCount(); i++) {
					Part p = ((Multipart) content).getBodyPart(i);
					if (p.getContent() instanceof String) {
						messaggio.setBodyHtml((String) (String) p.getContent());
						messaggio.setPostaCertEMLBody((String) p.getContent());
					} else if (p.getContent() instanceof Multipart) {
						addMultipartAttach((Multipart) p.getContent(),
								messaggio, listaAllegati);
					} else if (p instanceof Part) {
						InputStream is = null;
						AllegatoDTO allegato = null;
						if (!Part.ATTACHMENT.equalsIgnoreCase(p.getDisposition()) && !StringUtils.isNotBlank(p.getFileName())) {
							Object mm = p.getContent();
							if (mm instanceof Multipart)
								addMultipartAttach((Multipart) mm, messaggio,
										listaAllegati);
							else if (mm instanceof String) {
								messaggio.setBody((String)mm);
							}

							continue;
						}

						is = p.getInputStream();
						ByteArrayOutputStream aos = new ByteArrayOutputStream();
						byte[] buf = new byte[4096];
						int bytesRead;

						while ((bytesRead = is.read(buf)) != -1) {
							aos.write(buf, 0, bytesRead);
						}

						allegato = new AllegatoDTO();
						allegato.setContentType(p.getContentType());
						
						allegato.setFileName(p.getFileName() != null ? MimeUtility.decodeText(p.getFileName()) : "");
						allegato.setFileData(aos.toByteArray());
						allegato.setFileSize(allegato.getFileData().length);
						allegato.setMessageID(messaggio.getMessageId());
						allegato
								.setTipoAllegato(allegato.getFileName()
										.lastIndexOf(".") != -1 ? allegato
										.getFileName().substring(
												allegato.getFileName()
														.lastIndexOf(".") + 1)
										: "");

						aos.close();
						is.close();
						listaAllegati.add(allegato);

//						if ("postacert.eml".equalsIgnoreCase(allegato.getFileName())) {
//							File f = new File("C:\\testPdf\\postacert.txt");
//							FileOutputStream fo = new FileOutputStream(f);
//							fo.write(allegato.getFileData());
//							fo.flush();
//							fo.close();
//							elabPostaCertEML(messaggio, p, listaAllegati);
//						}
					}
					// test
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void handleMultipart(Multipart multipart)
			throws MessagingException, IOException {
		for (int i = 0, n = multipart.getCount(); i < n; i++) {
			handlePart(multipart.getBodyPart(i));
		}
	}

	public static void handlePart(Part part) throws MessagingException,
			IOException {
		String disposition = part.getDisposition();
		String contentType = part.getContentType();
		if (disposition == null) { // When just body
			System.out.println("Null: " + contentType);
			// Check if plain
			if ((contentType.length() >= 10)
					&& (contentType.toLowerCase().substring(0, 10).equals("text/plain"))) {
				part.writeTo(System.out);
			} else { // Don't think this will happen
				System.out.println("Other body: " + contentType);
//				part.writeTo(System.out);
				System.out.println("Other body fileName: " + part.getFileName());
			}
		} else if (disposition.equalsIgnoreCase(Part.ATTACHMENT)) {
			System.out.println("Attachment: " + part.getFileName() + " : "+ contentType);
			saveFile(part.getFileName(), part.getInputStream());
		} else if (disposition.equalsIgnoreCase(Part.INLINE)) {
			System.out.println("Inline: " + part.getFileName() + " : "+ contentType);
			saveFile(part.getFileName(), part.getInputStream());
		} else { // Should never happen
			System.out.println("Other: " + disposition);
		}
	}

	public static void saveFile(String filename, InputStream input)
			throws IOException {
		if (filename == null) {
			filename = File.createTempFile("xx", ".out").getName();
		}
		// Do no overwrite existing file
		File file = new File(filename);
		for (int i = 0; file.exists(); i++) {
			file = new File(filename + i);
		}
		FileOutputStream fos = new FileOutputStream(file);
		BufferedOutputStream bos = new BufferedOutputStream(fos);

		BufferedInputStream bis = new BufferedInputStream(input);
		int aByte;
		while ((aByte = bis.read()) != -1) {
			bos.write(aByte);
		}
		bos.flush();
		bos.close();
		bis.close();
	}

	public static String readHeaders(String pattern , PSTMessage message){
        ArrayList<String> listaParamentri = new ArrayList<String>();
        String result="";
        if(message.getTransportMessageHeaders()!=null){
	        BufferedReader reader = new BufferedReader(new StringReader(message.getTransportMessageHeaders()));
	        String s ="";
	        try {
				while ((s = reader.readLine()) != null) {
					if(s != null && s.startsWith(pattern, 0)){
						result = s.replaceAll(pattern, "");
					}
				  }
			} catch (IOException e) {
				// TODO Blocco catch generato automaticamente
				e.printStackTrace();
			}
		}
        return result;
	}
	
	public static String readHeadersTipoMessaggio(PSTMessage message){
	        ArrayList<String> listaParamentri = new ArrayList<String>();
	        String result="";
	        if(message.getTransportMessageHeaders()!=null){
		        BufferedReader reader = new BufferedReader(new StringReader(message.getTransportMessageHeaders()));
		        String s ="";
		        try {
					while ((s = reader.readLine()) != null) {
						if(s != null && s.startsWith("accettazione", 0)){
							result = s.replaceAll("accettazione", "");
						}
					  }
				} catch (IOException e) {
					// TODO Blocco catch generato automaticamente
					e.printStackTrace();
				}
			}
	        return result;
	}
	
	public static ArrayList<String> readHeaders(String pattern , PSTMessage message, int i){
	        ArrayList<String> listaParamentri = new ArrayList<String>();
	    	BufferedReader reader = new BufferedReader(new StringReader(message.getTransportMessageHeaders()));
	        String s ="";
	        try {
				while ((s = reader.readLine()) != null) {
					if(s != null && s.startsWith(pattern, 0)){
						listaParamentri.add(s.replaceAll(pattern, ""));
						for(int k=0;k<i;k++){
							listaParamentri.add(reader.readLine());
						}
					}
				  }
			} catch (IOException e) {
				// TODO Blocco catch generato automaticamente
				e.printStackTrace();
			}
	        return listaParamentri;
	 }
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "ELABORAZIONE MESSAGGIO PST";
	}

}
