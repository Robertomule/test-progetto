package it.eustema.inps.agentPecPei.business.pec;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;


import org.apache.log4j.Logger;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.mail.InviaPecCommandTest;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;

/**
 * @author rstabile
 *
 */
public class InviaMessaggiPECInUscitaCommandTest implements Command, Callable<Object> {

	private String codAOO;
	private static Logger log = Logger.getLogger(InviaMessaggiPECInUscitaCommandTest.class);
	
	public InviaMessaggiPECInUscitaCommandTest( String codAOO ){
		this.codAOO = codAOO;
		
	}
	
	@Override
	public Object execute() throws BusinessException {
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		
		//Recupero di tutti i messaggi in uscita
		MessaggiDAO messaggiDAO = new MessaggiDAO();
		AllegatiDAO allegatiDAO = new AllegatiDAO();
		List<MessaggioDTO> listaMessaggiInUscita = null;
		try {
			listaMessaggiInUscita = messaggiDAO.selezionaMessaggiInUscitaPEC( codAOO );
		} catch (DAOException e) {
			throw new NotifyMailBusinessException( -1000, "Errore nel recupero dei messaggi in uscita dal DB. Dettaglio Errore: " + e.getMessage() );
		}
		
		for ( MessaggioDTO messaggioInUscita: listaMessaggiInUscita ){
			
			//se l'account pec � di test eseguo il codice
			if(ConfigProp.indirizziPerTest.contains(messaggioInUscita.getAccountPec())){
				try {
					log.info("Invio mail da account di test: "+messaggioInUscita.getAccountPec());
					System.out.println("Invio mail da account di test: "+messaggioInUscita.getAccountPec());
					
					//Costruzione del messaggio in uscita con Destinatari ed allegati
					messaggioInUscita = messaggiDAO.selezionaDettaglioMessaggioInUscitaPEC( messaggioInUscita.getMessageId() );
					messaggioInUscita.setElencoFileAllegati( allegatiDAO.selectAllegatiWS( messaggioInUscita.getMessageId() ) );
					
					//Setto la lista dei destinatari Per di test (Presi dal file di configurazione)
					messaggioInUscita.setElencoDestinatariPer(creaListaDestinatariPer(messaggioInUscita));
					//Setto la lista dei destinatari cc di test (Presi dal file di configurazione)
					messaggioInUscita.setElencoDestinatariCc(creaListaDestinatariCc(messaggioInUscita));
					
					log.info("Messaggio in uscita : "+messaggioInUscita.getMessageId()+" con account : "+messaggioInUscita.getAccountPec());
					
					//Faccio il set della segnatura di test
					SegnaturaDTO segnatura = new SegnaturaDTO();
					//INPS.dd/MM/yyyy.XXXXXXX
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy.HH:mm:ss");
					segnatura.setSegnatura("INPS."+sdf.format(new Date()));
					
					//Valorizzazione dello stato e della segnatura
					messaggioInUscita.setSegnatura( segnatura.getSegnatura() );
					messaggiDAO.aggiornaMsgUscenteProtocollato( null, messaggioInUscita );
					
					messaggioInUscita.setStato( Constants.STATO_MSG_INVIATO );
					messaggioInUscita.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
					messaggioInUscita.setSubject(messaggioInUscita.getSubject()+" ["+segnatura.getSegnatura()+"]");
					
					boolean presenzaTestoDelMessaggio = false;
					AllegatoDTO testodelmessaggio = messaggiDAO.creaMessageTxt(messaggioInUscita);
					List<AllegatoDTO> allegati = messaggioInUscita.getElencoFileAllegati();
					for(int j=0;j<allegati.size();j++){
						if(allegati.get(j).getFileName().equalsIgnoreCase("testodelmessaggio.txt"))
							presenzaTestoDelMessaggio = true;
					}
					if(!presenzaTestoDelMessaggio){
						messaggioInUscita.getElencoFileAllegati().add(testodelmessaggio);
						allegatiDAO.insertAllegato(messaggioInUscita,testodelmessaggio);
					}
	
					//Invio della PEC
					executor.setCommand( new InviaPecCommandTest( messaggioInUscita ) );
					executor.executeCommandAndLogNew(messaggioInUscita);
//					executor.executeCommandAndLog();
					
					messaggiDAO.aggiornaMsgUscenteInviato( null, messaggioInUscita );
	
					
				} catch (DAOException e) {
					executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio [" + messaggioInUscita.getMessageId() + "] in uscita dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
					executor.executeCommand();
				} catch (BusinessException be){
					//Si prosegue con il prossimo messaggio
					be.printStackTrace();
				} catch (Exception e) {
					executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio [" + messaggioInUscita.getMessageId() + "] in uscita dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
					executor.executeCommand();
				} 
			}
		}
		
		return new Integer(1);
	}
	
	private List<DestinatarioDTO> creaListaDestinatariPer(MessaggioDTO msg){
		//Lista Destinatari accettati che diventer� la lista dei destinatari del messaggio
		List<DestinatarioDTO> listaDestinatariPer = new ArrayList<DestinatarioDTO>();
		//Destinatari del messaggio originale
		List<DestinatarioDTO> elencoPer = msg.getElencoDestinatariPer();
		DestinatarioDTO destTmp;
		
		for(DestinatarioDTO dest: elencoPer){
			String destPer = dest.getAddr();  
			
			//utilizzo indirizzi di test creata all'interno del try che contiene gli indirizzi che si possono utilizzare
			if(ConfigProp.indirizziPerTest.contains(destPer.split("@")[0])){
				destTmp = new DestinatarioDTO();
				destTmp.setAddr(destPer);
				destTmp.setCodiceAOO(this.codAOO);
				destTmp.setName(destPer.split("@")[0]);
				destTmp.setPhrase("<"+destPer.split("@")[0]+">"+ " "+destPer);
				destTmp.setType("per");
				destTmp.setMessageID(msg.getMessageId());
				
				listaDestinatariPer.add(destTmp);
			}
		}
		
		//se nessun destinatario � valido inserisco l'account pec di invio al fine di poter inviare ugualmente la pec
		if(listaDestinatariPer.size() == 0){
			String account = msg.getAccountPec();
			
			destTmp = new DestinatarioDTO();
			destTmp.setAddr(account);
			destTmp.setCodiceAOO(this.codAOO);
			destTmp.setName(account.split("@")[0]);
			destTmp.setPhrase("<"+account.split("@")[0]+">"+ " "+account);
			destTmp.setType("per");
			destTmp.setMessageID(msg.getMessageId());
			
			listaDestinatariPer.add(destTmp);
		}
		
		return listaDestinatariPer;
	}
	
	private List<DestinatarioDTO> creaListaDestinatariCc(MessaggioDTO msg){
		//Lista Destinatari accettati che diventer� la lista dei destinatari del messaggio
		List<DestinatarioDTO> listaDestinatariCc = new ArrayList<DestinatarioDTO>();
		//Destinatari del messaggio originale
		List<DestinatarioDTO> elencoCc = msg.getElencoDestinatariCc();
		DestinatarioDTO destTmp;
		
		for(DestinatarioDTO dest: elencoCc){
			String destCc = dest.getAddr();  
			
			//utilizzo indirizzi di test creata all'interno del try che contiene gli indirizzi che si possono utilizzare
			if(ConfigProp.indirizziPerTest.contains(destCc.split("@")[0])){
				destTmp = new DestinatarioDTO();
				destTmp.setAddr(destCc);
				destTmp.setCodiceAOO(this.codAOO);
				destTmp.setName(destCc.split("@")[0]);
				destTmp.setPhrase("<"+destCc.split("@")[0]+">"+ " "+destCc);
				destTmp.setType("per");
				destTmp.setMessageID(msg.getMessageId());
				
				listaDestinatariCc.add(destTmp);
			}
		}
		
		return listaDestinatariCc;
	}
	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "InviaMessaggiPECInUscita";
	}

	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
