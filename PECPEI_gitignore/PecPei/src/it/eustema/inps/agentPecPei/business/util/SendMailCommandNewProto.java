package it.eustema.inps.agentPecPei.business.util;

import java.util.Calendar;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutorImpl;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.Posta;

public class SendMailCommandNewProto implements Command {
	private static Logger log = Logger.getLogger(SendMailCommand.class);
	
	private NotifyMailBusinessException exception;
	private MessaggioDTO messaggio;
	
	public SendMailCommandNewProto( NotifyMailBusinessException exception ){
		this.exception = exception;
	}
	
	public SendMailCommandNewProto( NotifyMailBusinessException exception, MessaggioDTO msgDTO ){
		this.messaggio = msgDTO;
		this.exception = exception;
	}
	
	@Override
	public Object execute() throws BusinessException {
		
		
		if ( Calendar.getInstance().get(Calendar.MINUTE) > 10) return null;
		 
		 
			//log.debug("notificaErroreByMail");
			//26/01/2015 Nuove modifica NotificaErrori
			log.debug("notificaErroreByDB");
			try{
				Vector<String> destinatari = new Vector<String>();
				String destPer = ConfigProp.emailDestDettaglioEccezione;
				String[] destinatariPostaPer = destPer.split(",");
				String destinatario;
				if(destinatariPostaPer.length > 0){
					for(int d=0; d<destinatariPostaPer.length; d++){
						if(destinatariPostaPer[d].trim().contains("<"))
							destinatario = destinatariPostaPer[d].trim().substring(destinatariPostaPer[d].trim().indexOf("<"),destinatariPostaPer[d].trim().indexOf(">")).replaceAll("<", "").replaceAll(">", "");
						else
							destinatario = destinatariPostaPer[d].trim();

						destinatari.add(destinatario);
					}
				}
				Posta posta = new Posta();
				posta.inviaEmail(ConfigProp.serverPosta,
								ConfigProp.uidPosta,
								ConfigProp.pwdPosta,
								ConfigProp.emailMittDettaglioEccezione,
								destinatari,
								null,
								"Errore nella Procedura PECPEI - Codice Errore: " + exception.getErrorCode() ,
								exception.getMessage(),
								true);
				
				InserisciNotificaErrori();
								
			}
			catch(Exception e)
			{
				log.error("Notifica errori fallita: " + e.getMessage());
				System.out.println("Notifica errori fallita: " + e.getMessage());
			}
			
			return null;
		} 
	
	//26/01/2015 Nuove modifica NotificaErrori
	private void InserisciNotificaErrori() throws DAOException
	{
		String dbName = System.getProperty("NotificaErrori.DbName");
		
		Object notifica = null;
		
		Object notificaDAO = null;
		
		long resultID = -1;
		
		if(dbName != null)					
		{
			if(dbName == Constants.DB_PECPEI)
			{
				notifica = new it.eustema.inps.agentPecPei.dto.NotificaErroriDTO();
				
				((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setCodice(exception.getErrorCode());
				
				((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setMessaggio(exception.getMessage());
				
				if(this.messaggio != null)
				{
					((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setAccountPEC(this.messaggio.getAccountPec());
					((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setCodiceAOO(this.messaggio.getCodiceAOO());
					((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica).setMessageID(this.messaggio.getMessageId());
				}
				
				notificaDAO = new it.eustema.inps.agentPecPei.dao.NotificaErroriDAO();
				
				resultID = ((it.eustema.inps.agentPecPei.dao.NotificaErroriDAO) notificaDAO).insertNotifica((it.eustema.inps.agentPecPei.dto.NotificaErroriDTO) notifica);
				
			}else if(dbName == Constants.DB_VERIFICAPEC)
			{
				notifica = new it.inps.agentPec.verificaPec.dto.NotificaErroriDTO();
				
				((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica).setCodice(exception.getErrorCode());
				
				((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica).setMessaggio(exception.getMessage());
				
				if(this.messaggio != null)
				{
					((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica).setMessageID(this.messaggio.getMessageId());
				}
				
				notificaDAO = new it.inps.agentPec.verificaPec.dao.NotificaErroriDAO();
				
				resultID = ((it.inps.agentPec.verificaPec.dao.NotificaErroriDAO) notificaDAO).insertNotifica((it.inps.agentPec.verificaPec.dto.NotificaErroriDTO) notifica);

			}
			else if(dbName == Constants.DB_HERMES)
			{
				notifica = new it.eustema.inps.hermes.dto.NotificaErroriDTO();
				
				((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setCodice(exception.getErrorCode());
				
				((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setMessaggio(exception.getMessage());
				
				if(this.messaggio != null)
				{
					((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setCodiceAOO(this.messaggio.getCodiceAOO());
					((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica).setMessageID(this.messaggio.getMessageId());
				}
				
				notificaDAO = new it.eustema.inps.hermes.dao.NotificaErroriDAO();
				
				resultID = ((it.eustema.inps.hermes.dao.NotificaErroriDAO) notificaDAO).insertNotifica((it.eustema.inps.hermes.dto.NotificaErroriDTO) notifica);

			}
			else
				log.info("Notifica errori fallita: valore proprietÓ NotificaErrori.DbName non riconosciuto!");
		}
		else
			log.info("Notifica errori fallita: proprietÓ NotificaErrori.DbName non trovata!");
		
		if(resultID < 0)
			log.info("Notifica errori fallita: inserimento in " + dbName + ".NotificaErrori fallito! Codice Errore: " + this.exception.getErrorCode() + "; Messaggio: " + this.exception.getMessage() + "");

	}
	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "SendMailCommand";
	}

}
