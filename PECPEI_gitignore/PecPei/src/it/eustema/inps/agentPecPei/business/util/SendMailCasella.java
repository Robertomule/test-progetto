package it.eustema.inps.agentPecPei.business.util;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.Posta;

import java.util.Vector;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

public class SendMailCasella implements Command, Callable<Object>{

	private static Logger log = Logger.getLogger(SendMailCasella.class);
	private String nomeCasella = "";
	
	public SendMailCasella(String nome){
		this.nomeCasella = nome;
	}
	
	@Override
	public Object execute() throws BusinessException {
		
		try{
			log.debug("InvioMailPerCasellaDisabilitata : Inizio Invio Mail Notifica");
			Vector<String> destinatari = new Vector<String>();
			
			Posta posta = new Posta();
			String mittente = String.format("Notifica Dal Batch PecPei <noreply.batch@pecpei.it>","noreply.batch@pecpei.it");
			String testoOggetto = "Casella "+nomeCasella+" Disabilitata";
			
			//Creao allegato
			AllegatoDTO allegato = new AllegatoDTO();
			String txt = "1. Dalla console di monitoraggio accedere al menu PecPei --> Caselle\n"
							+"2. Prendere il codice Sede della casella bloccata\n"
							+"3. Spostarsi nel menu PecPei --> GestioneAccountPec e impostare il filtri di ricerca\n"
							+"4. Dopo aver estratto la casella interessata cliccare su modifica e prenedere la password\n"
							+"5. Da Browser aprire la casella alla url https://www.telecompost.it/webmail/login.jsp con account Pec e la password del punto 4\n"
							+"6. Verificare la presenza di eventuali vecchi messaggi letti e non eliminati. In questo caso spostarli nella cartella Deleted Items\n"
							+"7. Ripetere i punti 3 e 4 e abilitare nuovamente la casella.";
			
			allegato.setContentType("text/xml");
			allegato.setFileData(txt.getBytes());
			allegato.setFileName("Istruzioni.txt");
			allegato.setFileSize(txt.length());
			allegato.setTestoAllegato(txt);
			allegato.setTipoAllegato("txt");
			
			for(String s:ConfigProp.sendTo.split(";"))
				destinatari.add(s);
			
			String bodyMessaggio = "Attenzione: la casella <b>"+nomeCasella+"</b> � stata disabilitata a causa di un ritardo di lettura che potrebbe rallentare il batch di PecPei. <br />"
									+"Verificare eventuali messaggi letti e non eliminati all'interno della casella. <br />"
									+"Per i parametri di accesso consultare il menu PecPei del monitoraggio Batch. <br><br>"
									+"Messaggio automatico, non rispondere.";
			
			posta.inviaInsertDBEmailAllegati(
							ConfigProp.serverPosta,
							ConfigProp.uidPosta,
							ConfigProp.pwdPosta,
							mittente, // Inserire il mittente
							destinatari, // Inserire il destinatario
							null, // Inserire il destinatario CC
							testoOggetto, // Inserire Oggetto
							bodyMessaggio, // Inserire Testo da definire
							allegato,
							true);
			
		}
		catch(Exception e){
			new NotifyMailBusinessException(-6002, "Errore nell'invio email di disabilitazione casella " + e.getMessage());
		}
		
		log.debug("InvioMailPerCasellaDisabilitata: Fine Invio Mail Notifica");
		
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "SendMailCasella";
	}
	
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}
}
