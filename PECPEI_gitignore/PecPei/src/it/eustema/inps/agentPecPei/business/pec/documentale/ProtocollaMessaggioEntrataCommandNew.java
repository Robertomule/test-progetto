package it.eustema.inps.agentPecPei.business.pec.documentale;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.util.SendMailCommandNewProto;
import it.eustema.inps.agentPecPei.dao.IndicePADAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.IndicePADTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.StaticUtil;
import it.inps.soa.WS00732.IWSProtocolloProxy;
import it.inps.soa.WS00732.data.Allegato;
import it.inps.soa.WS00732.data.Classifica;
import it.inps.soa.WS00732.data.DocumentoElettronico;
import it.inps.soa.WS00732.data.DocumentoPrincipale;
import it.inps.soa.WS00732.data.EnEsito;
import it.inps.soa.WS00732.data.EnTipoSoggetto;
import it.inps.soa.WS00732.data.Mittente;
import it.inps.soa.WS00732.data.ProtocollaResponse;
import it.inps.soa.WS00732.data.ProtocolloEntrataRequest;
import it.inps.soa.WS00732.data.RelatedDataItem;
import it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataRequest;
import it.inps.soa.WS00732.data.RichiestaInteroperabilitaInEntrataResponse;
import it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataRequest;
import it.inps.soa.WS00732.data.RichiestaProtocollazioneInEntrataResponse;
import it.inps.www.interop2013.Segnatura;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

/**
 * 
 * @author r.stabile
 *
 */

public class ProtocollaMessaggioEntrataCommandNew implements Command {

	private MessaggioDTO messaggio;
	private Map<String, String> allegatiMap;
	private List<String> allegatiNoRelatedData;
	private String xmlInter;
	private String xmlSemplice;
	private String docPrincipale;
	private int idPrenotazione;
	private EnEsito esito;
	private List<AllegatoDTO> allegati;
	private EsitoStoreAllegati esa;
	private org.tempuri.DNA_BASICHTTP_BindingStub prx = null;
	//Utilizzato per l'esito dell'interoperabilitÓ
	String result = "";
	
	//Variabili per manipolare xml della segnatura
	private SAXBuilder builder;
	private Document doc;
	private Element rootElement;
	
	private static Logger log = Logger.getLogger(ProtocollaMessaggioEntrataCommandNew.class);
	
	public ProtocollaMessaggioEntrataCommandNew( MessaggioDTO messaggio ){
		this.messaggio = messaggio;
		this.builder = new SAXBuilder();
		this.doc = new Document();
		this.rootElement = null;
	}
	
	@Override
	public Object execute() throws BusinessException {
		
    	//Faccio il set dei parametri del certificato per accedere al servizio
//		System.setProperty("javax.net.ssl.keyStore", ConfigProp.keyStore);
//		System.setProperty("javax.net.ssl.keyStorePassword", ConfigProp.keyStorePassword);
//		System.setProperty("javax.net.ssl.trustStore", ConfigProp.keyStore);
//		System.setProperty("javax.net.ssl.trustStorePassword", ConfigProp.keyStorePassword);
		
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		allegati = new ArrayList<AllegatoDTO>();
		allegatiNoRelatedData = new ArrayList<String>();
		
		//Se idPrenotazione Ŕ giÓ valorizzato vuol dire che Ŕ stata giÓ effettutata una richiesta di interoperabilitÓ
		if(messaggio.getIdPrenotazione() == null || messaggio.getIdPrenotazione().equals("")){
			
			try{
				
				if(messaggio.getElencoFileAllegati()!=null && !messaggio.getElencoFileAllegati().isEmpty())
					allegati = messaggio.getElencoFileAllegati();
				
				//Salvo gli allegati nel document store e gli id nella tabella allegati di pecpei
				DocumentStore ds = new DocumentStore();
				// esa = ds.memorizzaAllegati(allegati,messaggio.getMessageId());
				
				//se non sono stati memorizzati tutti gli allegati non richiedo la protocollazione ed esco
				if(!esa.isAllAllegatiMemorizzati())
					return "";
					
				try{
					IWSProtocolloProxy proxy = new IWSProtocolloProxy();
					prx = proxy.getProxyHelperNoDataPower();//Chiamata senza datapower
					//prx = proxy.getProxyHelper();//Chiamata con datapower
					 
				}catch(Exception se){
					throw new NotifyMailBusinessException(-6002, "Errore nella chiamata al il servizio Protocollazione Dettaglio Errore: " + se.getMessage());
				}
			
				//Faccio la chiamata di interoperabilitÓ
				if(esa.isInteroperabile()){
						
					log.info("Protocollo con interoperabilitÓ");
					System.out.println("Protocollo con interoperabilitÓ");
					//Genero xml da passare per la richiesta di protocollazione con interoperabilitÓ
					//xmlInter = xmlParte1 + xmlParte2; //generaXMLInterop();
					
					//se c'Ŕ errore nel generare l'xml la chiamata va in errore e verrÓ fatta la richiesta di protocollazione all'esecuzione successiva
					xmlInter = generaXmlInterop(esa);
					
					//Oggetto per la richiesta di interoperabilitÓ
					RichiestaInteroperabilitaInEntrataRequest request = new RichiestaInteroperabilitaInEntrataRequest();
					request.setData(xmlInter);
					request.setMessageID(messaggio.getMessageId());
					request.setRegisterCode(messaggio.getCodiceAOO());
					request.setRecipient(getMittenteInterop(xmlInter)); //IndirizzoTelematico
						
					//Creo l'array RelatedDataItem con il nome e l'id degli allegati memorizzati 
					//tranne quelli inseriti dentro l'xml di richiesta interoperabilitÓ e il documento principale
					it.inps.soa.WS00732.data.RelatedDataItem item;
					it.inps.soa.WS00732.data.RelatedDataItem[]items;
					List<RelatedDataItem> listItem = new ArrayList<RelatedDataItem>();
					Iterator<Map.Entry<String, String>> iterator = esa.getAllegatiMap().entrySet().iterator();
						
					//Creo i RelatedDataItem senza il documento principale e gli allegati
					while(iterator.hasNext()){
						Map.Entry<String, String> allegato = iterator.next();
						
						if(!esa.getAllegatiNoRelatedData().contains(allegato.getValue())){
							item = new RelatedDataItem();
							item.setDocumentID(allegato.getKey());
							item.setName(allegato.getValue());
							
							listItem.add(item);
						}
					}
						
					//creo l'array di RelatedItem
					if(listItem.size() > 0){
						items = new RelatedDataItem[listItem.size()];
						for(int x = 0;x<listItem.size();x++)
							items[x] = listItem.get(x);
						
						request.setRelatedData(items);
					}else
						request.setRelatedData(new RelatedDataItem[0]);
						
					//chiamo il servizio
					RichiestaInteroperabilitaInEntrataResponse response = prx.richiestaInteroperabilitaInEntrata(request);
					esito = response.getCodice();
						
					if(response.getRequestId() != null)
						idPrenotazione = response.getRequestId();
						
					result = esito.getValue();
						
					log.info("Esito interoperabilitÓ "+messaggio.getMessageId() +" "+ esito.getValue() + " " +response.getDescrizione());
					System.out.println("Esito interoperabilitÓ "+messaggio.getMessageId() +" "+ esito.getValue() + " " + response.getDescrizione());
						
					//Aggiorno il messaggio
					try{
						
						if(esito.getValue().equalsIgnoreCase("OK")){
							
							//messaggio.setStato( Constants.STATO_MSG_RICEVUTO ); //lo stato viene inserito dal Ws di PecPei
							//messaggio.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA); //lo stato viene inserito dal Ws di PecPei
							messaggio.setIdPrenotazione(String.valueOf(idPrenotazione));
							messaggio.setDocumentoPrincipale(docPrincipale);
							MessaggiDAO messaggiDAO = new MessaggiDAO();
							messaggiDAO.aggiornaMsgRicevutoDaRiprotocollare2016( null, messaggio );
							
							log.info("Codice prenotazione "+idPrenotazione+" inserito per messaggio "+messaggio.getMessageId());
							System.out.println("Codice prenotazione "+idPrenotazione+" inserito per messaggio "+messaggio.getMessageId());
						}
						
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}catch(Exception e){
				result = "KO";
				executor.setCommand( new SendMailCommandNewProto( new NotifyMailBusinessException( -2000, "Errore nella protocollazione del messaggio [" + messaggio.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
				executor.executeCommand();
				log.error("Errore in protocollazione di InteroperabiliÓ " + e.getMessage());
				e.printStackTrace();
			}

			if(!esa.isInteroperabile()){
				//se non Ŕ interoperabile faccio la protocollazione semplice
				try{
					log.info("Non Ŕ possibile la protocollazione con interoperabilitÓ. Faccio la semplice");
					System.out.println("Non Ŕ possibile la protocollazione con interoperabilitÓ. Faccio la semplice");
					
					ProtocolloEntrataRequest request = new ProtocolloEntrataRequest();
					Iterator<Map.Entry<String, String>> iterator = esa.getAllegatiMap().entrySet().iterator();
					Allegato allegato;
					Mittente mittente;
					EnTipoSoggetto tipoSoggetto; 
					DocumentoElettronico docElettronico;
					DocumentoPrincipale docPrincipale;
					Classifica classifica;
					
					Allegato[] allegatiArray = new Allegato[allegatiMap.size()-1]; //devo togliore il documento principale che non va negli allegati
					int x=0;
					//Creo l'array di allegati
					while(iterator.hasNext()){
						Map.Entry<String, String> allegatoMap = iterator.next();
						
						//Metto come documento principale il postacert.eml
						if(allegatoMap.getValue().equalsIgnoreCase("postacert.eml")){
							docPrincipale = new DocumentoPrincipale();
							docElettronico = new DocumentoElettronico();
							classifica = new Classifica();
							
							classifica.setCodice("1");
							docElettronico.setDocumentID(allegatoMap.getKey());
							docPrincipale.setDocumentoElettronico(docElettronico);
							docPrincipale.setOggetto(allegatoMap.getValue());
							docPrincipale.setClassificazioni(new Classifica[]{classifica});
							
							request.setDocumento(docPrincipale);
						}else{
							allegato = new Allegato();
	
							docElettronico = new DocumentoElettronico();
							docElettronico.setDocumentID(allegatoMap.getKey());
							
							allegato.setAutocertificazione(false);
							allegato.setDocumentoElettronico(docElettronico);
							allegato.setOggetto(allegatoMap.getValue());
							
							allegatiArray[x] = allegato;
							x++;
						}
						
					}
					
					//Creo il mittente
					mittente = new Mittente();
					tipoSoggetto = new EnTipoSoggetto("M");
					
					mittente.setNominativo(messaggio.getAccountPec());
					mittente.setEmail(messaggio.getAccountPec() + "" +ConfigProp.dominioPEC);
					mittente.setCodice(messaggio.getMessageId());
					mittente.setTipoSoggetto(tipoSoggetto);
					
					request.setMittente(mittente);
					request.setAllegati(allegatiArray);
	
					//chiamo il servizio
					ProtocollaResponse response = prx.protocollaInEntrata(messaggio.getCodiceAOO(),request);
					esito = response.getCodice();
					//result = esito.getValue();
					
					log.info("Esito interoperabilitÓ semplice MessageId: "+messaggio.getMessageId() +" "+ esito.getValue() + " " + response.getDescrizione()!=null?response.getDescrizione():"");
					System.out.println("Esito interoperabilitÓ semplice "+messaggio.getMessageId() +" "+ esito.getValue() + " " + response.getDescrizione());
					
					if(esito.getValue().equalsIgnoreCase("OK")){
						//Valorizzazione dello stato e della segnatura
						messaggio.setSegnatura( response.getSegnatura() );
						messaggio.setStato( Constants.STATO_MSG_RICEVUTO );
						messaggio.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
						
						MessaggiDAO messaggiDAO = new MessaggiDAO();
						messaggiDAO.aggiornaMsgRicevutoDaRiprotocollare( null, messaggio );
					}else{
						executor.setCommand(new SendMailCommandNewProto( new NotifyMailBusinessException( -6002,  "Errore nella protocollazione del messaggio " +messaggio.getMessageId()+ " Errore: " +response.getDescrizione())));
						executor.executeCommand();
					}
					
				}catch(Exception e){
					executor.setCommand( new SendMailCommandNewProto( new NotifyMailBusinessException( -2000, "Errore nella protocollazione del messaggio [" + messaggio.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
					executor.executeCommand();
					log.error("Errore in protocollazione " + e.getMessage());
					e.printStackTrace();
				}
			}
			
			return "";
		}else if(messaggio.getIdPrenotazione().split(":")[0].equalsIgnoreCase("KO")){ //Se la interoperabilitÓ Ŕ andata in KO faccio la richiesta di protocollazione
			//se l'esito non Ŕ ok faccio la richiesta di protocollazione
				log.error("Errore in interoperabilitÓ faccio la richiesta di protocollazione per messaggio " +messaggio.getMessageId());
				System.out.println("Errore in interoperabilitÓ faccio la richiesta di protocollazione " +messaggio.getMessageId());
				
				try{
					
					allegati = messaggio.getElencoFileAllegati();
					allegatiMap = new HashMap<String, String>();
					allegatiNoRelatedData = new ArrayList<String>();
					
					for(AllegatoDTO allegato: allegati)
						allegatiMap.put(allegato.getIdDocStore(), allegato.getFileName());
					
//					if("messaggio-pei".equalsIgnoreCase( messaggio.getTipoMessaggio()))
//						xmlSemplice = generaXmlProtSemplice(messaggio, "800.010", allegatiMap);
//					else
//						xmlSemplice = generaXmlProtSemplice(messaggio, null, allegatiMap);
					
					allegatiNoRelatedData.add(messaggio.getDocumentoPrincipale());
					
					RichiestaProtocollazioneInEntrataRequest request = new RichiestaProtocollazioneInEntrataRequest();
					request.setData(xmlSemplice);
					request.setMessageID(messaggio.getMessageId());
					request.setRegisterCode(messaggio.getCodiceAOO());
					request.setRecipient(messaggio.getAccountPec()); //destinataio
					
					//Creo l'array RelatedDataItem con il nome e l'id degli allegati memorizzati 
					//tranne quelli inseriti dentro l'xml di richiesta interoperabilitÓ
					RelatedDataItem item;
					RelatedDataItem[] items;
					List<RelatedDataItem> listItem = new ArrayList<RelatedDataItem>();
					Iterator<Map.Entry<String, String>> iterator = allegatiMap.entrySet().iterator();
					
					//Creo i RelatedDataItem senza il documento principale
					while(iterator.hasNext()){
						Map.Entry<String, String> allegato = iterator.next();
						
						if(!allegatiNoRelatedData.contains(allegato.getValue())){
							item = new RelatedDataItem();
							item.setDocumentID(allegato.getKey());
							item.setName(allegato.getValue());
							
							listItem.add(item);
						}
					}
					
					//verifico che ci siano elementi nella lista per creare l'array di RelatedDataItem
					if(listItem.size() > 0){
						items = new RelatedDataItem[listItem.size()];
						for(int x = 0;x<listItem.size();x++)
							items[x] = listItem.get(x);
						
						request.setRelatedData(items);
					}else
						request.setRelatedData(new RelatedDataItem[0]);
					
					//chiamo il servizio
					RichiestaProtocollazioneInEntrataResponse response = prx.richiestaProtocollazioneInEntrata(request);
					esito = response.getCodice();
					
					if(response.getRequestId() != null)
						idPrenotazione = response.getRequestId();
					
					result = esito.getValue();
					
					if(!esito.getValue().equalsIgnoreCase("OK")){
						executor.setCommand(new SendMailCommandNewProto( new NotifyMailBusinessException( -6002,  "Errore nella protocollazione del messaggio " +messaggio.getMessageId()+ " Errore: " +response.getDescrizione())));
						executor.executeCommand();
					}
					
					log.info("Esito irichiesta di protocollazione "+messaggio.getMessageId() +" "+ esito.getValue() + " " + response.getDescrizione()!=null?response.getDescrizione():"");
					System.out.println("Esito richiesta di protocollazione "+messaggio.getMessageId() +" "+ esito.getValue() + " " + response.getDescrizione());
					
					//Aggiorno il messaggio
					try{
						if(esito.getValue().equalsIgnoreCase("OK")){
							
//							messaggio.setStato( Constants.STATO_MSG_RICEVUTO );
//							messaggio.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
							messaggio.setIdPrenotazione(String.valueOf(idPrenotazione));
							MessaggiDAO messaggiDAO = new MessaggiDAO();
							messaggiDAO.aggiornaMsgRicevutoDaRiprotocollare2016( null, messaggio );
							
							log.info("Codice prenotazione "+idPrenotazione+" inserito per messaggio "+messaggio.getMessageId());
							System.out.println("Codice prenotazione "+idPrenotazione+" inserito per messaggio "+messaggio.getMessageId());
						
						}
						
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}catch(Exception e){
					executor.setCommand( new SendMailCommandNewProto( new NotifyMailBusinessException( -2000, "Errore nella protocollazione del messaggio [" + messaggio.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
					executor.executeCommand();
					log.error("Errore in protocollazione Semplice" + e.getMessage());
					e.printStackTrace();
				}
				
			}//fine if di richiesta di protocollazione
		
			return String.valueOf(-1);
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "WSPIA Messaggio in Entrata";
	}
	
	private String generaXmlInterop(EsitoStoreAllegati esa){
		
		try{
			
			String xml = esa.getSegnaturaXml();
			
			try{
				xml = xml.replace("<!DOCTYPE Segnatura SYSTEM \"Segnatura.dtd\">", "");
				xml = xml.replaceAll("xmlns=\"http://www.digitPa.gov.it/protocollo/\"", "");
				xml = xml.replaceAll("´╗┐", "");
				xml = StaticUtil.decodicaCaratteriNonStampabiliCarattere(xml);
			}catch(Exception e){
				
			}
			
			//estraggo dalla segnatura il documento principale
			doc = builder.build(new StringReader(xml)); 
			rootElement = doc.getRootElement();
			docPrincipale = rootElement.getChild("Descrizione").getChild("Documento").getAttributeValue("nome");
			
			//inserisco l'id del documento principale e l'id degli allegati nella segnatura.xml
			Iterator<Map.Entry<String, String>> iterator = esa.getAllegatiMap().entrySet().iterator();
			List<Element> allegatiSegnatura = null;
			
			if(rootElement.getChild("Descrizione").getChild("Allegati") != null)
				allegatiSegnatura = rootElement.getChild("Descrizione").getChild("Allegati").getChildren();
			
			while(iterator.hasNext()){
				Map.Entry<String, String> allegato = iterator.next();
					
				//documento principale
				if(docPrincipale.equalsIgnoreCase(allegato.getValue())){
					//xmlParte2 = "<Documento id=\"" + allegato.getKey() + "\" " + xmlParte2.substring(xmlParte2.indexOf("nome=\""+docPrincipale+"\""),xmlParte2.length());
					if(allegato.getKey() != null){
						rootElement.getChild("Descrizione").getChild("Documento").setAttribute("id",allegato.getKey());
						allegatiNoRelatedData.add(allegato.getValue());
					}
					//break;
				}else{ //allegati
					if(allegatiSegnatura != null && allegatiSegnatura.size() > 0)
						for(Element e: allegatiSegnatura){
							if(e.getAttribute("nome").getValue().equalsIgnoreCase(allegato.getValue())){
								e.setAttribute("id", allegato.getKey());
								allegatiNoRelatedData.add(allegato.getValue());
							}
						}
					}
			}
			
			esa.setAllegatiNoRelatedData(allegatiNoRelatedData);
			
		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
		
		
		return new XMLOutputter().outputString(doc);
		
		/*		
			//Divido l'xml in 2 parti e salvo il documento principale, dopo aver aggiunto l'id del documento principale lo ricompongo
			xmlParte1 = xml.substring(0, xml.indexOf("<Descrizione>")+13);
			xmlParte2 = xml.substring(xml.indexOf("<Descrizione>")+13, xml.length());
			docPrincipale = xmlParte2.split("nome=\"")[1].split("\"")[0];
		*/	
		
	}
	
	private String getMittenteInterop(String xml){
		String mittente = "";
		
		try{
			
			Element indirizzoTelematico;
			doc = builder.build(new StringReader(xml)); 
			rootElement = doc.getRootElement();
			
			if((indirizzoTelematico = rootElement.getChild("Intestazione").getChild("Origine").getChild("IndirizzoTelematico")) != null)
				mittente = indirizzoTelematico.getValue();
			else if((indirizzoTelematico = rootElement.getChild("Intestazione").getChild("Risposta").getChild("IndirizzoTelematico")) != null)
				mittente = indirizzoTelematico.getValue();
			
			if(!validateEmail(mittente)){
				log.debug("Segnatura contiene mittente errato :"+mittente+" sostituito con "+messaggio.getReplyTo());
				mittente = messaggio.getReplyTo();
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return mittente;
	}
	
	private static boolean validateEmail(String email) {
        boolean isValid = false;
        try {
            //
            // Create InternetAddress object and validated the supplied
            // address which is this case is an email address.
            //
            InternetAddress internetAddress = new InternetAddress(email);
            internetAddress.validate();
            isValid = true;
        } catch (AddressException e) {
        	
        }
        return isValid;
    }
}