package it.eustema.inps.agentPecPei.business.pec.mail;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.util.DebugConstants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.Posta;

/**
 * @author rstabile
 *
 */
public class InviaPecCommandTest implements Command {

	private MessaggioDTO messaggio;
	
	public InviaPecCommandTest( MessaggioDTO messaggio ){
		this.messaggio = messaggio;
	}
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		try {
			AccountPecDTO accountPEC = new AccountPecDAO().selectAccountPecTest( messaggio.getAccountPec() );
			
			if ( accountPEC == null ){
				throw new NotifyMailBusinessException( -4010, "Account PEC [" +messaggio.getAccountPec() +"] non esistente su DB" );
			}
			
			if ( DebugConstants.ENABLE_DEBUG_CONSTANTS ){
				accountPEC.setUserName( DebugConstants.ACCOUNT_PEC_USER_NAME );
				accountPEC.setPassword( DebugConstants.ACCOUNT_PEC_USER_PASSWORD );
			}
			Posta posta = new Posta();
			posta.inviaEmailDiTest(accountPEC, messaggio.getElencoDestinatariPer(), messaggio.getElencoDestinatariCc(), messaggio.getSubject(), messaggio.getBodyHtml(), messaggio.getElencoFileAllegati(), messaggio.getMessageId(), messaggio );
			
		} catch ( DAOException de ){
			throw new NotifyMailBusinessException( -4010, "Errore nel recupero ddati ell'account PEC [" +messaggio.getAccountPec() +"] Dettaglio Errore: "+ de.getMessage() );
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new NotifyMailBusinessException(-4000, "Errore nell'invio AccountPEC all'account: [" + messaggio.getAccountPec() +"] Dettaglio Errore: " + e.getMessage());
		}
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "InviaPecCommand";
	}

}
