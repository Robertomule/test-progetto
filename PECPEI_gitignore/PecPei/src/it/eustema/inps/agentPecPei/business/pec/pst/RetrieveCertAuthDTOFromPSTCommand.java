package it.eustema.inps.agentPecPei.business.pec.pst;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.pff.PSTAttachment;
import com.pff.PSTException;
import com.pff.PSTMessage;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.cert.RetrieveCertAuthDTOCommand;
import it.eustema.inps.agentPecPei.dto.CertAuthDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;

/**
 * Classe Command che si occupa di generare un'istanza della classe CertAuthDTO contenente 
 * le informazioni estratte dal certificato presente in uno degli allegati del PSTMessage
 * passato in input.
 * 
 * Se non viene trovato alcun certificato, allora restituisce [null]
 * @author ALEANZI
 *
 */
public class RetrieveCertAuthDTOFromPSTCommand implements Command {

	private PSTMessage mail = null;
	
	public RetrieveCertAuthDTOFromPSTCommand( PSTMessage mail ){
		this.mail = mail;
	}
	@Override
	public Object execute() throws BusinessException {
		CertAuthDTO out = null;
		for ( int i=0; i<mail.getNumberOfAttachments(); i++ ){
        	try {
				if  ( mail.getAttachment( i ).getFilename().indexOf(".p7") != -1 ){
					out = elabAttach( mail.getAttachment( i ) );
				}
			} catch (PSTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
		return out;
	}
	
	private CertAuthDTO elabAttach( PSTAttachment att ){
		 CertAuthDTO certOut = null;
		try {
			
	        InputStream attachmentStream = att.getFileInputStream();
	        // both long and short filenames can be used for attachments
	        String filename = att.getLongFilename();
	        if (filename.isEmpty()) {
	                filename = att.getFilename();
	        }
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        // 8176 is the block size used internally and should give the best performance
	        int bufferSize = 8176;
	        byte[] buffer = new byte[bufferSize];
	        int count = attachmentStream.read(buffer);
	        while (count == bufferSize) {
	                out.write(buffer);
	                count = attachmentStream.read(buffer);
	        }
	        byte[] endBuffer = new byte[count];
	        System.arraycopy(buffer, 0, endBuffer, 0, count);
	        out.write(endBuffer);
	        
	        attachmentStream.close();
	        CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
	        executor.setCommand( new RetrieveCertAuthDTOCommand( out.toByteArray() ) );
	        certOut = (CertAuthDTO) executor.executeCommand();
			out.close();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PSTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return certOut;
	}
	
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "PST -> CerthAuthDTO";
	}

}
