package it.eustema.inps.agentPecPei.business.pec;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.documentale.DocumentStore;
import it.eustema.inps.agentPecPei.business.pec.documentale.EsitoStoreAllegati;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioUscitaCommand;
import it.eustema.inps.agentPecPei.business.pec.mail.InviaPecCommand;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dao.EsitiNotificheSgdDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.EsitoNotificaSgdDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.notifichePciSGD.InvioNotificaSGDCallable;
import it.eustema.inps.agentPecPei.notifichePciSGD.ProtocolloPciToSgdSingleton;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.inps.soa.DocumentStore.data.GetAllegatoRequest;
import it.inps.soa.DocumentStore.data.GetAllegatoResponse;
import it.inps.soa.DocumentStore.data.GetDocumentoRequest;
import it.inps.soa.DocumentStore.data.GetDocumentoResponse;
import it.inps.soa.DocumentStore.data.RecuperaMetadatiRequest;
import it.inps.soa.DocumentStore.data.RecuperaMetadatiResponse;
import it.inps.soa.WS00541.IStoreAttachmentDocumentProxy;
import it.inps.soa.WS00732.IWSProtocolloProxy;
import it.inps.soa.WS00732.data.DocumentoPEC;
import it.inps.soa.WS00732.data.ProtocollaPECInUscitaResponse;
import it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaRequest;
import it.inps.soa.WS00732.data.RegistraStatoInvioPECInUscitaResponse;

import org.tempuri.DNA_BASICHTTP_BindingStub;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.datacontract.schemas._2004._07.WSProtocolloService_DataContract_Enumeration.EnStatoInvioRichiestaPECInUscita;

import wsPciClient.org.tempuri.WsOUTVerificaStatoApprovazione;
import wsPciClient.org.tempuri.WsPCItoSGDSoapStub;

public class InviaMessaggiPECInUscitaCommand implements Command, Callable<Object> {

	private String codAOO;
	private static Logger log = Logger.getLogger(InviaMessaggiPECInUscitaCommand.class);
	private Future<Object> resultSGD = null; //utilizzato per la notifica SGD
	private WsPCItoSGDSoapStub stub;
	private WsOUTVerificaStatoApprovazione esitoApprovazione;
	private EsitoStoreAllegati esa;

	public InviaMessaggiPECInUscitaCommand( String codAOO ){
		this.codAOO = codAOO;
		stub = ProtocolloPciToSgdSingleton.getInstance().proxy.getProxyHelper();
		//stub = ProtocolloPciToSgdSingleton.getInstance().proxy.getProxyHelperNoDataPower(); // no datapower
	}

	@Override
	public Object execute() throws BusinessException {
		System.out.println("InviaMessaggiPECInUscitaCommand :: execute :: Recupero tutti i messaggi in uscita");
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		//Recupero di tutti i messaggi in uscita
		MessaggiDAO messaggiDAO = new MessaggiDAO();
		AllegatiDAO allegatiDAO = new AllegatiDAO();
		EsitiNotificheSgdDAO esitiNotificheSgdDAO = new EsitiNotificheSgdDAO();

		List<MessaggioDTO> listaMessaggiInUscita = null;

		IStoreAttachmentDocumentProxy attachmentStore = new IStoreAttachmentDocumentProxy();
		wsPciClient.org.tempuri.DNA_BASICHTTP_BindingStub proxyAttachmentStore = attachmentStore.getProxyHelper();
		//wsPciClient.org.tempuri.DNA_BASICHTTP_BindingStub proxyAttachmentStore = attachmentStore.getProxyHelperNoDataPower(); // Chiamata no datapower

		IWSProtocolloProxy wsProtocollo = new IWSProtocolloProxy();
		DNA_BASICHTTP_BindingStub proxyProtocollo = wsProtocollo.getProxyHelper(); //Chiamata con datapower
		//DNA_BASICHTTP_BindingStub proxyProtocollo = wsProtocollo.getProxyHelperNoDataPower();//Chiamata no datapower

		try {
			System.out.println("InviaMessaggiPECInUscitaCommand :: try() :: Sono nel try-catch per recuperare i messaggi");
			listaMessaggiInUscita = messaggiDAO.selezionaMessaggiInUscitaPEC( codAOO );
		} catch (DAOException e) {
			throw new NotifyMailBusinessException( -1000, "Errore nel recupero dei messaggi in uscita dal DB. Dettaglio Errore: " + e.getMessage() );
		}

		System.out.println("InviaMessaggiPECInUscitaCommand :: execute() :: Scorro la lista dei messaggi in uscita");
		for ( MessaggioDTO messaggioInUscita: listaMessaggiInUscita ){
			//se non � un account di test eseguo il codice
			if(!ConfigProp.indirizziPerTest.contains(messaggioInUscita.getAccountPec())){
				System.out.println("InviaMessaggiPECInUscitaCommand :: execute() :: Se " + messaggioInUscita.getAccountPec() + " non e' un account di test eseguo il codice a seguire");
				//in caso di messaggio nello stato BZ verifico se c'� l'approvazione per l'invio dal ws di PCI
				if(messaggioInUscita.getStato().equalsIgnoreCase(Constants.STATO_BOZZA)){
					try {
						log.info("Verifico approvazione per messaggio SGD "+messaggioInUscita.getMessageId());

						esitoApprovazione = stub.wsVerificaStatoApprovazione(messaggioInUscita.getMessageId(),messaggioInUscita.getCodiceAOO(),messaggioInUscita.getAccountPec());
						if(esitoApprovazione.getIEsito() == 1){
							log.info("Esito approvazione positivo "+esitoApprovazione.getSDescrizione());
							messaggioInUscita.setStato(Constants.STATO_MSG_DAINVIARE);
						}else{
							log.info("Esito approvazione negativo "+esitoApprovazione.getSDescrizione());
							continue;
						}

					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						log.error("Errore in verifica approvazione per il messaggio SGD "+messaggioInUscita.getMessageId()+": " + e.getMessage());
						continue;
					}
				}

				try {
					log.info("Invio mail da account NON di test: "+messaggioInUscita.getAccountPec() + " da canale " + messaggioInUscita.getCanale());
					System.out.println("Invio mail da account NON di test: "+messaggioInUscita.getAccountPec() + " da canale " + messaggioInUscita.getCanale());

					//Costruzione del messaggio in uscita con Destinatari ed allegati
					messaggioInUscita = messaggiDAO.selezionaDettaglioMessaggioInUscitaPEC( messaggioInUscita.getMessageId() );
					messaggioInUscita.setElencoFileAllegati( allegatiDAO.selectAllegatiWS( messaggioInUscita.getMessageId() ) );

					// Aggiunto per la gestione messaggi SGD
					//Se il canale � SGD vado a pololare i campi idDocStore sul DB
					if(messaggioInUscita.getCanale() != null && messaggioInUscita.getCanale().equalsIgnoreCase("SGD")){

						boolean allDatiAllegatiCaricati = true;

						for (AllegatoDTO allegatoInLettura: messaggioInUscita.getElencoFileAllegati()){
							if(!allegatoInLettura.getFileName().equalsIgnoreCase("TestoDelMessaggio.txt")){ // Verifico che non sia il testo del messaggio
								if(allegatoInLettura.getIdDocStore() != null && !allegatoInLettura.getIdDocStore().equals("")){ // Se c'� l'idDocumentStore
									if(allegatoInLettura.getFileData() == null || allegatoInLettura.getFileData().length == 0){ // Se non � gi� stato caricato

										GetDocumentoResponse response = proxyAttachmentStore.getDocumento(new GetDocumentoRequest(allegatoInLettura.getIdDocStore()));

										if(response.getEsito().getValue().equalsIgnoreCase("OK") && response.getBody() != null 
												&& response.getBody().length > 0){ // Se � andato a buon fine

											allegatoInLettura.setFileData(response.getBody());
											boolean esito = allegatiDAO.updateMetadatiAllegato(messaggioInUscita.getMessageId(), allegatoInLettura);

											if(!esito){ // Se l'aggiornamento non � andato a buon fine salto il messaggio
												allDatiAllegatiCaricati = false;
												log.error("Errore durante l'aggiornamento dell'allegato: " + allegatoInLettura.getFileName());
											}

										} else { // Se il recupero dei dati non � andato a buon fine
											allDatiAllegatiCaricati = false;
											log.error("Impossibile trovare il Documento con idDocumentStore: " + allegatoInLettura.getIdDocStore());
										}
									}
								} else { // Se l'idDocumentStore non � valorizzato
									// Se il file che sto leggendo � la segnatura e non presenta idDocumentStore significa che nel ciclo precedente il batch � riuscito
									// a caricarla sul DB ma nel momento in cui ha cercato di caricarla sul DocumentStore � andato in errore, per questo motivo riprovo
									if(allegatoInLettura.getFileName().equalsIgnoreCase("Segnatura.xml")){
										DocumentStore documentStore = new DocumentStore();
										EsitoStoreAllegati esito = documentStore.memorizzaAllegato(allegatoInLettura, messaggioInUscita.getMessageId(), messaggioInUscita.getSegnatura());

										if(!esito.isAllAllegatiMemorizzati()) // Se anche a questo ciclo non sono riuscito a caricare la segnatura salto il messaggio
											allDatiAllegatiCaricati = false;

										// Prendo nuovamente l'allegato cos� da avere il campo idDocumentStore popolato
										AllegatoDTO allegatoTmp = allegatiDAO.getAllegatoMessaggio(messaggioInUscita.getMessageId(), allegatoInLettura.getFileName());

										// A questo punto rimuovo il vecchio allegato della segnatura e lo sostituisco con quella nuova con idDocumentStore popolato
										messaggioInUscita.getElencoFileAllegati().remove(allegatoInLettura);
										messaggioInUscita.getElencoFileAllegati().add(allegatoTmp);
									} else 
										allDatiAllegatiCaricati = false;
								}
							}
						}

						if(!allDatiAllegatiCaricati) continue;
					}

					log.info("Messaggio in uscita : "+messaggioInUscita.getMessageId()+" con account : "+messaggioInUscita.getAccountPec());
					SegnaturaDTO segnatura = new SegnaturaDTO();
					//Se il messaggio non � stato gi� Protocollato in uscita
					if(messaggioInUscita.getSegnatura()!=null && !messaggioInUscita.getSegnatura().equals("")) {
						segnatura.setSegnatura(messaggioInUscita.getSegnatura());
						segnatura.setStatus(0);
					} else {
						// Protocollo SGD
						if(messaggioInUscita.getCanale() != null 
								&& messaggioInUscita.getCanale().equalsIgnoreCase(ConfigProp.canaleSgd)) {
							ProtocollaPECInUscitaResponse response = proxyProtocollo.protocollaPECInUscita(messaggioInUscita.getMessageId());

							if(response != null){
								segnatura = new SegnaturaDTO();
								segnatura.setStatus(response.getCodice().getValue().equalsIgnoreCase("OK")?0:1);

								if(segnatura.isOK()) {
									// Setto la segnatura
									segnatura.setSegnatura(response.getDescrizione());
									
									// Valorizzazione dello stato e della segnatura
									messaggioInUscita.setSegnatura( segnatura.getSegnatura() );
									messaggiDAO.aggiornaMsgUscenteProtocollato( null, messaggioInUscita );
									
									// Se la segnatura ottenuta � valida creo l'allegato
									AllegatoDTO segnaturaXML = new AllegatoDTO();
									segnaturaXML.setContentType("xml");
									segnaturaXML.setFileData(response.getSegnatura().getBytes());
									segnaturaXML.setFileName("Segnatura.xml");
									segnaturaXML.setFileSize(response.getSegnatura().length());
									segnaturaXML.setMessageID(messaggioInUscita.getMessageId());
									segnaturaXML.setPostedDate(new Timestamp(messaggioInUscita.getPostedDate().getTime()));
									segnaturaXML.setTestoAllegato(response.getSegnatura());
									segnaturaXML.setTipoAllegato("XML");

									// La carico sul DB
									allegatiDAO.insertAllegato(messaggioInUscita, segnaturaXML);

									// La carico sul DocumentStore
									DocumentStore documentStore = new DocumentStore();
									EsitoStoreAllegati esito = documentStore.memorizzaAllegato(segnaturaXML, messaggioInUscita.getMessageId(), response.getDescrizione());

									// Aggiorno il valore della segnatura aggiungendogli anche l'idDocStore
									segnaturaXML = allegatiDAO.getAllegatoMessaggio(messaggioInUscita.getMessageId(), segnaturaXML.getFileName());

									// Se l'esito dell'archiviazione � negativo e quindi il file non � stato caricato sul DocumentStore non invio il messaggio
									if(segnaturaXML.getIdDocStore() == null || segnaturaXML.getIdDocStore().equalsIgnoreCase(""))
										continue;

									// Aggiungo la segnatura al mio messaggio
									messaggioInUscita.getElencoFileAllegati().add(segnaturaXML);

								} else
									segnatura.setCodiceErrore(response.getDescrizione());
							} else {
								continue;
							}
						} else {
							executor.setCommand( new ProtocollaMessaggioUscitaCommand(messaggioInUscita));
							segnatura = (SegnaturaDTO)executor.executeCommandAndLogNew(messaggioInUscita);
							//segnatura = (SegnaturaDTO)executor.executeCommandAndLog();
						}
					}
					//Se il messaggio non � stato protocollato, allora si prosegue con l'elaborazione
					//del messaggio successivo
					if (!segnatura.isOK()) {
						executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -5000, "Errore nella protocollazione del messaggio [" + messaggioInUscita.getMessageId() + "] in uscita dal DB. Dettaglio Errore: codice: " + segnatura.getCodiceErrore() + "   descrizione : " + segnatura.getDescErrore() + System.getProperty("line.separator") + "XML inviato: " +System.getProperty("line.separator") + System.getProperty("line.separator") + segnatura.getXmlInput() ) ) );
						executor.executeCommand();
						continue;
					} else {
						//Valorizzazione dello stato e della segnatura
						messaggioInUscita.setSegnatura( segnatura.getSegnatura() );
						messaggiDAO.aggiornaMsgUscenteProtocollato( null, messaggioInUscita );

						messaggioInUscita.setStato( Constants.STATO_MSG_INVIATO );
						messaggioInUscita.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
						messaggioInUscita.setSubject(messaggioInUscita.getSubject()+" ["+segnatura.getSegnatura()+"]");

						boolean presenzaTestoDelMessaggio = false;
						AllegatoDTO testodelmessaggio = messaggiDAO.creaMessageTxt(messaggioInUscita);
						List<AllegatoDTO> allegati = messaggioInUscita.getElencoFileAllegati();

						for(int j=0;j<allegati.size();j++){
							if(allegati.get(j).getFileName().equalsIgnoreCase("testodelmessaggio.txt"))
								presenzaTestoDelMessaggio = true;
						}

						if(!presenzaTestoDelMessaggio) {
							messaggioInUscita.getElencoFileAllegati().add(testodelmessaggio);
							allegatiDAO.insertAllegato(messaggioInUscita,testodelmessaggio);
						}

						//Invio della PEC
						executor.setCommand( new InviaPecCommand( messaggioInUscita ) );
						executor.executeCommandAndLogNew(messaggioInUscita);
						//						executor.executeCommandAndLog();

						messaggiDAO.aggiornaMsgUscenteInviato( null, messaggioInUscita );

						// Aggiunto per la gestione messaggi SGD - Notifico
						if(messaggioInUscita.getCanale() != null && messaggioInUscita.getCanale().equalsIgnoreCase(ConfigProp.canaleSgd)) {
							RegistraStatoInvioPECInUscitaRequest request = new RegistraStatoInvioPECInUscitaRequest();

							request.setMessageID(messaggioInUscita.getMessageId());

							if(messaggioInUscita.getStato().equalsIgnoreCase("IN"))
								request.setStato(EnStatoInvioRichiestaPECInUscita.Inviata);
							else
								request.setStato(EnStatoInvioRichiestaPECInUscita.NonInviata);
							
							String[] idDocumentStoreSegnatura = new String[1];
							for(AllegatoDTO allegatoInLettura : messaggioInUscita.getElencoFileAllegati())
								if(allegatoInLettura.getFileName().equalsIgnoreCase("Segnatura.xml")){
									idDocumentStoreSegnatura[0] = allegatoInLettura.getIdDocStore();
									break;
								}
							
							request.setDocumentIDes(idDocumentStoreSegnatura);

							RegistraStatoInvioPECInUscitaResponse response = proxyProtocollo.registraStatoInvioPECInUscita(request);

							if(response.getCodice().getValue().equalsIgnoreCase("OK"))
								log.info("Esito RegistraStatoInvioPECInUscitaRequest: " + response.getCodice().getValue());
							else
								log.error("ERRORE durante RegistraStatoInvioPECInUscitaRequest per la PEC " + request.getMessageID() + ":" +
										"\nEsito: " + response.getCodice().getValue() +
										"\nDescrizione: " + response.getDescrizione());

							EsitoNotificaSgdDTO esitoNotificaSgd = new EsitoNotificaSgdDTO();
							esitoNotificaSgd.setMessageID(request.getMessageID());
							esitoNotificaSgd.setMetodo("RegistraStatoInvioPECInUscita");
							esitoNotificaSgd.setStato(request.getStato().toString());
							esitoNotificaSgd.setEsito(response.getCodice().toString());
							esitoNotificaSgd.setDescrizioneErrore(response.getDescrizione());

							esitiNotificheSgdDAO.insertEsito(esitoNotificaSgd);
						}
					}

				} catch (DAOException e) {
					executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio [" + messaggioInUscita.getMessageId() + "] in uscita dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
					executor.executeCommand();
					e.printStackTrace();
				} catch (BusinessException be){
					be.printStackTrace();
					//Si prosegue con il prossimo messaggio
				} catch (Exception e) {
					e.printStackTrace(); // Da togliere
					executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio [" + messaggioInUscita.getMessageId() + "] in uscita dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
					executor.executeCommand();
				}
			}
		}

		return new Integer(1);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "InviaMessaggiPECInUscita";
	}

	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}

}
