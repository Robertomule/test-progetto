package it.eustema.inps.agentPecPei.business.pec.builder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.pff.PSTAttachment;
import com.pff.PSTException;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;

public class PSTAttachmentAllegatoDTOBuilder implements Command {

	private PSTAttachment att;
	private MessaggioDTO messaggio;
	
	public PSTAttachmentAllegatoDTOBuilder( PSTAttachment att, MessaggioDTO messaggio ){
		this.att = att;
		this.messaggio = messaggio;
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		AllegatoDTO out = new AllegatoDTO();
		out.setMessageID( messaggio.getMessageId() );
		try {
			byte[] fileData = new byte[att.getFilesize()];
			att.getFileInputStream().read( fileData );
			out.setFileData(fileData);
			out.setFileSize( att.getFilesize() );
		} catch (PSTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		out.setFileName( att.getFilename() );
		out.setTestoAllegato("");
		out.setTipoAllegato( att.getFilename().lastIndexOf(".") != -1 ? att.getFilename().substring( att.getFilename().lastIndexOf(".")+1 ) : "");
//		try{
//			File s = new File("C:\\demoMigrazione\\test.txt");
//			FileOutputStream fo = new FileOutputStream(s);
//			fo.write(out.getFileData());
//			fo.close();
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		return out;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "PST Attach -> AllegatoDTO";
	}

}
