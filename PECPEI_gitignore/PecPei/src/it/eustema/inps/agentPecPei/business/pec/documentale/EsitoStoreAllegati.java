package it.eustema.inps.agentPecPei.business.pec.documentale;

import java.util.List;
import java.util.Map;

public class EsitoStoreAllegati {

	private Map<String, String> allegatiMap;
	private List<String> allegatiNoRelatedData;
	private boolean isInteroperabile = false;
	private boolean allAllegatiMemorizzati = false;
	private String segnaturaXml;
	
	public Map<String, String> getAllegatiMap() {
		return allegatiMap;
	}
	public void setAllegatiMap(Map<String, String> allegatiMap) {
		this.allegatiMap = allegatiMap;
	}
	public List<String> getAllegatiNoRelatedData() {
		return allegatiNoRelatedData;
	}
	public void setAllegatiNoRelatedData(List<String> allegatiNoRelatedData) {
		this.allegatiNoRelatedData = allegatiNoRelatedData;
	}
	public boolean isInteroperabile() {
		return isInteroperabile;
	}
	public void setInteroperabile(boolean isInteroperabile) {
		this.isInteroperabile = isInteroperabile;
	}
	public void setAllAllegatiMemorizzati(boolean allAllegatiMemorizzati) {
		this.allAllegatiMemorizzati = allAllegatiMemorizzati;
	}
	public boolean isAllAllegatiMemorizzati() {
		return allAllegatiMemorizzati;
	}
	public void setSegnaturaXml(String segnaturaXml) {
		this.segnaturaXml = segnaturaXml;
	}
	public String getSegnaturaXml() {
		return segnaturaXml;
	}
}
