package it.eustema.inps.agentPecPei.business.pec.mail;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.utility.ConfigProp;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;

public class CheckCorrettezzaMessaggioCommand implements Command {

	private MessaggioDTO messaggio;
	private ConfigDAO configDao ;
	public CheckCorrettezzaMessaggioCommand( MessaggioDTO messaggio ){
		this.messaggio = messaggio;
	}
	
	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
//		if ( "errore".equalsIgnoreCase( messaggio.getTipoMessaggio() ) )
//			return Out.CAMPO_TIPO_MESSAGGIO_ERRORE;
		configDao = new ConfigDAO();
		if(messaggio.getReplyTo().contains( configDao.getProperty("spam", "raccomandata@pec.tnotice.email")))
			return Out.SPAM;
		
		if ( "".equals( messaggio.getTipoMessaggio() ) )
			return Out.CAMPO_TIPO_MESSAGGIO_VUOTO;
		
		if ( !"posta-certificata".equalsIgnoreCase( messaggio.getTipoMessaggio() ) )
			return Out.CAMPO_TIPO_MESSAGGIO_RICEVUTA;
		
//		if ( !checkDatiCert() )
//			return Out.DATI_CERT_ERRATO;
		
		if ( messaggio.getElencoDestinatariCc().isEmpty() && messaggio.getElencoDestinatariPer().isEmpty() )
			return Out.NESSUN_DESTINATARIO;
			
		return Out.OK;
	}
	
	
	private boolean checkDatiCert(){
		byte[] daticert = null;
		for ( AllegatoDTO allegato: messaggio.getElencoFileAllegati() ){
			if ( "DATICERT.XML".equalsIgnoreCase( allegato.getFileName() ) ){
				daticert =  allegato.getFileData();
				String s = new String (allegato.getFileData());
				daticert = s.getBytes();
				break;
			}
		}
		
		if ( daticert == null )
			return false;
		
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(false);
		factory.setNamespaceAware(true);

		SchemaFactory schemaFactory = 
		    SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

		try {
			factory.setSchema(schemaFactory.newSchema(
			    new Source[] {new StreamSource(ConfigProp.daticertXSD)}));
			SAXParser parser = factory.newSAXParser();

			XMLReader reader = parser.getXMLReader();
			reader.setErrorHandler( new ErrorHandler(){

				@Override
				public void error(SAXParseException arg0) throws SAXException {
					// TODO Auto-generated method stub
					throw new SAXException();
				}

				@Override
				public void fatalError(SAXParseException arg0)
						throws SAXException {
					// TODO Auto-generated method stub
					throw new SAXException();
				}

				@Override
				public void warning(SAXParseException arg0) throws SAXException {
					// TODO Auto-generated method stub
					
				}} );
			reader.parse(new InputSource( new ByteArrayInputStream( daticert ) ));
			
			
		} catch (SAXException e) {
			return false;
		} catch (ParserConfigurationException e) {
			return false;
		} catch (IOException e) {
			return false;
		}

		
		
		//Check validit� 
		return true;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "CheckCorrettezzaMessaggio";
	}
	
	
	public enum Out {
		OK(0,"OK"), 
		SPAM(-6001,"Messaggio contrassegnato come SPAM"),
		CAMPO_TIPO_MESSAGGIO_VUOTO(-6002,"Il campo tipo messaggio � vuoto"),
		CAMPO_TIPO_MESSAGGIO_ERRORE(-6003, "Il campo tipo messaggio � errore"),
		CAMPO_TIPO_MESSAGGIO_RICEVUTA(-6004, "Il campo tipo messaggio � ricevuta"),
		NESSUN_DESTINATARIO(-6005,"Nessun destinatario"),
		DATI_CERT_ERRATO(-6006,"L'allegato DATICERT.XML � errato");
		
		
		private int codice;
		private String desc;
		
		private Out(int codice, String desc){
			this.codice = codice;
			this.desc =desc;
		}

		public int getCodice() {
			return codice;
		}

		public String getDesc() {
			return desc;
		}
	}

}


