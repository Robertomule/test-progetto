package it.eustema.inps.agentPecPei.business.pec.mail;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.InviaMessaggiPECInUscitaCommand;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommand;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommandNew;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioUscitaCommand;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.notifichePciSGD.InvioNotificaSGDCallable;
import it.eustema.inps.agentPecPei.notifichePciSGD.ProtocolloPciToSgdSingleton;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import wsPciClient.org.tempuri.WsOUTComunicaRispostePEC;
import wsPciClient.org.tempuri.WsPCItoSGDSoapStub;

public class ReadAndElabMessaggiPecDaProtocollare implements Command, Callable<Object> {

	/**
	 * Classe Command che si occupa di Inviare i messaggi PEC in uscita
	 * @author ALEANZI
	 *
	 */
	private String codAOO;
	private static Logger log = Logger.getLogger(ReadAndElabMessaggiPecDaProtocollare.class);
	private Future<Object> resultSGD = null; //utilizzato per la notifica SGD
	private WsPCItoSGDSoapStub stub;
	private WsOUTComunicaRispostePEC outRispostaPec; 
	
	public ReadAndElabMessaggiPecDaProtocollare( String codAOO){
		this.codAOO = codAOO;
		stub = ProtocolloPciToSgdSingleton.getInstance().proxy.getProxyHelper();
	}
	
	@Override
	public Object execute() throws BusinessException {
		
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		
		//Recupero di tutti i messaggi in uscita
		MessaggiDAO messaggiDAO = new MessaggiDAO();
		AllegatiDAO allegatiDAO = new AllegatiDAO();
		List<MessaggioDTO> listaMessaggiInUscita = null;
		try {
			listaMessaggiInUscita = messaggiDAO.selezionaMessaggiInUscitaPECDaRiprotocollare( codAOO );
		} catch (DAOException e) {
			throw new NotifyMailBusinessException( -1000, "Errore nel recupero dei messaggi in entrata da riprotocollare dal DB. Dettaglio Errore: " + e.getMessage() );
		}
		
		for ( MessaggioDTO messaggioInUscita: listaMessaggiInUscita ){
			try {
				//Costruzione del messaggio in uscita con Destinatari ed allegati
				messaggioInUscita = messaggiDAO.selezionaDettaglioMessaggioInUscitaPECDaProtocollare( messaggioInUscita.getMessageId() );
				messaggioInUscita.setElencoFileAllegati( allegatiDAO.selectAllegatiWS( messaggioInUscita.getMessageId() ) );
				
				//Protocollazione in uscita
				if(messaggioInUscita.getVerso().equals("E")){
					//verifico se utilizzare il vecchio o il nuovo protocollo in base alle aoo abilitate
					if(messaggioInUscita.getTipoMessaggio().equalsIgnoreCase("posta-certificata") && ConfigProp.listaAooAbilitate.contains(messaggioInUscita.getCodiceAOO()))
						executor.setCommand( new ProtocollaMessaggioEntrataCommandNew( messaggioInUscita ) ); //nuovo metodo di protocollazione
					else
						executor.setCommand( new ProtocollaMessaggioEntrataCommand( messaggioInUscita ) );
				}else{
					executor.setCommand( new ProtocollaMessaggioUscitaCommand( messaggioInUscita ) );
				}
				
				//Se si tratta di aoo abilitata al nuovo protocollo passo al prossimo messagio
				if(messaggioInUscita.getTipoMessaggio().equalsIgnoreCase("posta-certificata") && ConfigProp.listaAooAbilitate.contains(messaggioInUscita.getCodiceAOO())){
					executor.executeCommandAndLog();
					continue;
				}
				
				SegnaturaDTO segnatura = (SegnaturaDTO)executor.executeCommandAndLog();
				
				//Se il messaggio non � stato protocollato, allora si prosegue con l'elaborazione
				//del messaggio successivo
				if ( !segnatura.isOK() ){
					executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -5000, "Errore nella protocollazione del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: codice: " + segnatura.getCodiceErrore() + "   descrizione : " + segnatura.getDescErrore() + System.getProperty("line.separator") + "XML inviato: " +System.getProperty("line.separator") + System.getProperty("line.separator") + segnatura.getXmlInput() ) ) );
					executor.executeCommand();
					continue;
				}
				
				//Valorizzazione dello stato e della segnatura
				messaggioInUscita.setSegnatura( segnatura.getSegnatura() );
				messaggioInUscita.setStato( Constants.STATO_MSG_RICEVUTO );
				messaggioInUscita.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
				
				messaggiDAO.aggiornaMsgRicevutoDaRiprotocollare( null, messaggioInUscita );
				allegatiDAO.insertAllegato(messaggioInUscita,messaggiDAO.creaMessageTxt(messaggioInUscita));
				
				//Se il messaggio � da canale SGD ed � stato protocollato invio la notifica asinrona
				if(messaggioInUscita.getCanale() != null && messaggioInUscita.getCanale().equalsIgnoreCase(ConfigProp.canaleSgd)){
					InvioNotificaSGDCallable invioNotifica = new InvioNotificaSGDCallable();
					ExecutorService executorService = Executors.newSingleThreadExecutor();
					resultSGD = executorService.submit(invioNotifica);
					
					//segnalo a PCI che la PEC SGD pervenuta � in carico a SGD
					String messageIdPecInUscita = messaggiDAO.selezionaMessaggioInUscitaSGD(messaggioInUscita.getMsgId());
					outRispostaPec = stub.wsComunicaRispostePEC(messageIdPecInUscita,messaggioInUscita.getMessageId(),messaggioInUscita.getCodiceAOO(),messaggioInUscita.getAccountPec(),messaggioInUscita.getTipoMessaggio());
					if(outRispostaPec.getIEsito() == 0)
						log.error("Errore in notifica messaggio correlato SGD a PCI :"+outRispostaPec.getSDescrizione());
					else
						log.info("Messaggio correlato SGD notificato a PCI: "+outRispostaPec.getSDescrizione());
				}
				
				//Se � stato inviato un messaggio SGD invio la notifica
				if(messaggioInUscita.getCanale() != null && messaggioInUscita.getCanale().equalsIgnoreCase(ConfigProp.canaleSgd)){
					InvioNotificaSGDCallable invioNotifica = new InvioNotificaSGDCallable();
					ExecutorService executorService = Executors.newSingleThreadExecutor();
					resultSGD = executorService.submit(invioNotifica);
					
					//attendo esito notifica SGD
					Object o = resultSGD.get();
					System.out.println("Esito notifica SGD per messageID "+messaggioInUscita.getMessageId() + ": "+o);
					log.info("Esito notifica SGD: "+messaggioInUscita.getMessageId() + ": "+o);
				}
				
			} catch (DAOException e) {
				executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
				executor.executeCommand();
				
//				if(e.getMessage().toLowerCase().contains("disk space".toLowerCase()) && i==0){
//				ConfigProp.switchDbPecPei = true;
//				continue;
//			}
				
			} catch (BusinessException be){
				//Si prosegue con il prossimo messaggio
			} catch (Exception e) {
				executor.setCommand( new SendMailCommand( new NotifyMailBusinessException( -2000, "Errore nel recupero del messaggio da riprotocollare [" + messaggioInUscita.getMessageId() + "] in entrata dal DB. Dettaglio Errore: " + e.getMessage() ) ) );
				executor.executeCommand();
			} 
		}
		
		return new Integer(1);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "InviaMessaggiPECInUscita";
	}

	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}
}
