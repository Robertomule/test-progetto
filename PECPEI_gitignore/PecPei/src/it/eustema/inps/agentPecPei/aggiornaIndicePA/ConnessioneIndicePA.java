package it.eustema.inps.agentPecPei.aggiornaIndicePA;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.business.util.SendMailVerificaPecCommand;
import it.eustema.inps.agentPecPei.dao.LogIndiciPaDao;
import it.eustema.inps.agentPecPei.dto.AgentExecutionLogDTO;
import it.eustema.inps.agentPecPei.dto.LogIndiciPaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.util.AgentStatus;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.logging.LoggingConfigurator;
import it.inps.agentPec.verificaPec.dao.Agent_execution_logDAO;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.concurrent.Callable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;


public class ConnessioneIndicePA implements Command, Callable<Object> {

	private static Logger log = Logger.getLogger(ConnessioneIndicePA.class);
	public static String base = "c=it";

	static {
		//blocco eseguito una volta al caricamento della classe
        try {
			// carica i parametri dal file di cfg
			//new ConfigProp(cfgFileName);
			new ConfigProp().init();
			// inizializzazione configurazione del logging
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
		} catch (Exception e) {
			log.info("Indici PA --> " + e.getMessage());
			System.out.println("Indici PA --> " + e.getMessage());
			System.err.println("" + e.getMessage());
		}        
    }
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "ConnessioneIndicePA";
	}

	
	@Override
	public Object execute(){
		ConfigDAO configdao = new ConfigDAO();
		try {
			int starter = Integer.parseInt(configdao.getProperty("oraStartIndicePA", "23"));

			int delayDay = Integer.parseInt(configdao.getProperty("delayDayIndicePA", "23"));
			
			log.info("Indici PA --> starter = " +starter + " delayDay = " +delayDay);
			System.out.println("Indici PA --> starter = " +starter + " delayDay = " +delayDay);

			// System.out.println("--- Inizio verifica TitolarioThread ----");
			// System.out.println("Ora per cominciare = " + starter);
			// Il thread deve essere avviato ogni giorno all'ora decisa
			// da variabile su file di config
			GregorianCalendar GC = new GregorianCalendar();
			int ore = GC.get(Calendar.HOUR_OF_DAY);
			// System.out.println("Ora adesso = " + ore);
			// Se le ore sono pari al valore di starter avvio il thread e
			// aspetto per 24 ore.
			// In caso contrario aspetto per un'ora e ripeto il test sul
			// valore delle ore.
			if (ore == starter) {
				try {
					log.info("Indici PA --> ore == starter avvio il thread");
					System.out.println("Indici PA --> ore == starter avvio il thread");
					synchronized(this){
						Agent_execution_logDAO execLogDAO = new Agent_execution_logDAO();
						AgentExecutionLogDTO executionLogDTO = new AgentExecutionLogDTO();
						executionLogDTO.setAgent_type(AgentExecutionLogDTO.type_indicepa);
						int executionID = execLogDAO.insertAgentExecution(executionLogDTO);
						executionLogDTO.setExecution_id(executionID);
						
						Date data = new Date();
						Date dataUltimaEsecuzione = execLogDAO.selectAgentExecution(AgentExecutionLogDTO.type_indicepa);
						long differenzaGiorni = (data.getTime()-dataUltimaEsecuzione.getTime()) / (1000 * 60 * 60 * 24);
						if(differenzaGiorni==delayDay || dataUltimaEsecuzione==null){
							log.info("Indici PA --> Lancio il metodo Start");
							System.out.println("Indici PA --> Lancio il metodo Strart");
							start();
						}else{
							log.info("Indici PA --> Strart non lanciato: differenzaGiorni="+differenzaGiorni+" e delayDay="+delayDay);
							System.out.println("Indici PA --> Strart non lanciato: differenzaGiorni="+differenzaGiorni+" e delayDay="+delayDay);
						}
						
						long tempoEndAgent = System.currentTimeMillis();
						executionLogDTO.setEnd_time(new Timestamp(tempoEndAgent));
						executionLogDTO.setAgent_status(AgentStatus.COMPLETED.name());
						execLogDAO.updateAgentExecution(executionLogDTO);

					}
				} catch (Exception e) {
					log.info("Indici PA --> " + e.getMessage());
					System.out.println("Indici PA --> " + e.getMessage());
					e.printStackTrace();
				}
				System.out.println("--- Fine  sincronizzazione indici PA ----");

			} else {
				log.info("Indici PA --> ore != starter. Termino esecuzione");
				System.out.println("Indici PA --> ore != starter. Termino esecuzione");
			}
				
		} catch (Exception e) {
			log.info("Indici PA --> eccezione in execute: "+e.getMessage());
			System.out.println("Indici PA --> eccezione in execute: "+e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	public void start() throws BusinessException {
		// TODO Stub di metodo generato automaticamente
		// set properties for our connection and provider
		log.info("Indici PA --> sono nel metodo Start()");
		System.out.println("Indici PA --> sono nel metodo Start()");
		
		ConfigDAO configdao = new ConfigDAO();
		LogIndiciPaDao logEsecuzione = new LogIndiciPaDao();
		LogIndiciPaDTO logIndice = new LogIndiciPaDTO();
		CommandExecutor executorService = CommandExecutorManager.getInstance().getExecutor();
		
		Properties properties = new Properties();
		properties.put( Context.INITIAL_CONTEXT_FACTORY, ConfigProp.INITIAL_CONTEXT_FACTORY );
		properties.put( Context.PROVIDER_URL, ConfigProp.PROVIDER_URL);
		properties.put( Context.REFERRAL, "ignore" );

		// set properties for authentication
		properties.put( Context.SECURITY_PRINCIPAL, ConfigProp.SECURITY_PRINCIPAL );
		properties.put( Context.SECURITY_CREDENTIALS, ConfigProp.SECURITY_CREDENTIALS );
		Connection con = null;
		
		int totaliElaborati = 0;
		
		try {
				String driver = ConfigProp.jdbcDriver;
		        Class.forName(driver).newInstance();
		        
		        logIndice.setDataInizio(new java.sql.Timestamp(new Date().getTime()));
				logIndice.setTotaleIndLav(totaliElaborati);
				
//	        Locale
//	        String urlDB = "jdbc:sqlserver://127.0.0.1:21810;databaseName=PECPEI";
//	        con=DriverManager.getConnection(urlDB, "PECPEI","password01");
//	        Sviluppo
//	        String urlDB = "jdbc:sqlserver://SQLINPSSVIL06.INPS:2059;databaseName=PECPEI";
//	        con=DriverManager.getConnection(urlDB, "PECPEI","jar3-hK.m2");
//	        String urlDB = ConfigProp.dbPecPeiConnectionUrl;
//	        log.info("Inizializzazione Database: "+new Date()+"url :"+urlDB);
		        con=DriverManager.getConnection(ConfigProp.dbPecPeiConnectionUrl, ConfigProp.usrPecPei,ConfigProp.pwdPecPei);
		        
		        log.info("Indici PA --> Creazione oggetto InitialDirContext");
				System.out.println("Indici PA --> Creazione oggetto InitialDirContext");
				
				InitialDirContext context = new InitialDirContext( properties );
				
				log.info("Indici PA --> Oggetto InitialDirContext creato");
				System.out.println("Indici PA --> Oggetto InitialDirContext creato");
			// Create the search controls
				log.info("Indici PA --> Creazione oggetto SearchControls");
				System.out.println("Indici PA --> Creazione oggetto SearchControls");
				
				SearchControls searchCtls = new SearchControls();
				
				log.info("Indici PA --> Oggetto SearchControls creato");
				System.out.println("Indici PA --> Oggetto SearchControls creato");
			// Specify the search scope
				searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
//			searchCtls.setReturningAttributes(new String[]{"street","codiceFiscaleAmm"});
			// specify the LDAP search filter, just users
				String searchFilter = "(&(objectClass=*))";
			// Specify the attributes to return
				NamingEnumeration<SearchResult> answer = context.search( base, searchFilter, searchCtls );
				System.out.println("Indici PA --> Data Inizio Aggiornamento Indici PA"+new Date());
				
				StringBuilder sb = new StringBuilder("");
				sb.append("Indici PA -->  Data Inizio "+new Date()+"\n");
				deleteALLIndicePA(con);
				sb.append("Indici PA -->  Eliminazione di tutti gli account "+new Date()+"\n");
				while(answer.hasMoreElements()){
						SearchResult s = answer.nextElement();
						String nomeLivello = s.getName();
						Indicepa indice0 = new Indicepa();
						
						if(s.getName()!=null&&!s.getName().equals("")&&!s.getName().contains("Uff_eFatturaPA")){
		//					System.out.println("s: "+s.getName()+","+base);
							Attributes a = s.getAttributes();
							indice0.setCodamm(s.getName().substring(s.getName().indexOf("=")+1, s.getName().length()));
							valorizzaIndicePA(indice0,a);
							try{
								insertIndicePA(indice0,con);
							}catch(Exception e){
								log.error("Indici PA --> Inserimento fallito 1 : "+s.getName()+","+base);
							}
						}
						if(nomeLivello!=null&&!nomeLivello.equals("")&&!nomeLivello.contains("Uff_eFatturaPA")){
							try{
		//						System.out.println("Data Inzio "+new Date()+" per codice "+nomeLivello);
								scorriAlberaturaIndicePA( nomeLivello,  context, con);
		//						System.out.println("Data Fine "+new Date()+" per codice "+nomeLivello);
		
							}catch(NamingException e){
								log.error("Indici PA --> Errore caricamento LDAP IndicePA Naming: ",e);
							}
							totaliElaborati++;
							if(totaliElaborati%100==0){
								con.commit();
								log.info("Indici PA -->  Data Fine "+new Date());
								log.info("Indici PA -->  totaliElaborati: "+totaliElaborati);
		//						break;
							}
						}
					}		// Per i restanti indirizzi
					con.commit();
					
					sb.append("Indici PA --> Data Fine Totale"+new Date()+"\n");
					sb.append("Indici PA --> totaliElaborati Totale: "+totaliElaborati+"\n");
					sb.append("Indici PA --> Inizio Trasferimento IndicePA "+new Date()+"\n");
					if(Boolean.parseBoolean(configdao.getProperty("abilitaEsecuzioneStoreIndicePA", "false"))){
				        con.setAutoCommit(false) ;
				        CallableStatement cs = null;
				        cs = con.prepareCall("{call [dbo].[caricaIndicePA]}");
				        cs.execute() ;
				        con.commit();
				        sb.append("Indici PA --> Fine Trasferimento IndicePA "+new Date()+"\n");
					}
			        
					log.error("Indici PA --> Caricamento indicePA completato: " + sb.toString());
					System.out.println("Indici PA --> Caricamento indicePA completato: " + sb.toString());
					
			        executorService.setCommand( new SendMailVerificaPecCommand( new NotifyMailBusinessException( 2000, "Caricamento indicePA completato alle: "+new Date() ) ) );
					executorService.executeCommand();
			} catch (Exception e) {
			// TODO Blocco catch generato automaticamente
				e.printStackTrace();
				log.error("Errore Update Indici PA --> " + e.getMessage()+ " " + e.getStackTrace()[0]);
				System.out.println("Errore Update Indici PA --> " + e.getMessage() + " " + e.getStackTrace()[0]);
		        executorService.setCommand( new SendMailVerificaPecCommand( new NotifyMailBusinessException( 2000, "Caricamento indicePA completato con Errore alle: "+new Date()+ " -> "+e.getMessage() ) ) );
				executorService.executeCommand();
			}finally{
				if(con!=null)
					try {
						con.close();
						logIndice.setDataFine(new java.sql.Timestamp(new Date().getTime()));
						logIndice.setTotaleIndLav(totaliElaborati);
						logEsecuzione.insertLogEsecuzione(logIndice); //Esito esecuzione 
					} catch (SQLException e) {
						// TODO Blocco catch generato automaticamente
						log.error("Indici PA --> Messaggio errore Aggiornamento IndiciPA:"+e.getMessage());
						log.error("Indici PA --> Messaggio errore Aggiornamento IndiciPA:"+e.getMessage());
						System.out.println("Indici PA --> Messaggio errore Aggiornamento IndiciPA:"+e.getMessage());
					} catch (DAOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
	}
	
	public static NamingEnumeration<SearchResult> scorriAlberaturaIndicePA(String nomeLivello, InitialDirContext context, Connection con) throws Exception{
		NamingEnumeration<SearchResult> risultati = null;
		try{
			SearchControls searchCtls = new SearchControls();
			searchCtls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
			String searchFilter = "(&(objectClass=*))";
			risultati = context.search( nomeLivello+","+base, searchFilter, searchCtls );
			while(risultati.hasMoreElements()){
				Indicepa indice0 = new Indicepa();
				SearchResult s = risultati.nextElement();
//				System.out.println("s.isRelative(): "+s.isRelative());
				if(s.getName()!=null&&!s.getName().contains("Uff_eFatturaPA")){
//					System.out.println("s: "+s.getName()+","+base);
					Attributes a = s.getAttributes();
					indice0.setCodamm(s.getName().substring(s.getName().indexOf("=")+1, s.getName().length()));
					valorizzaIndicePA(indice0,a);
					try{
						insertIndicePA(indice0,con);
					}catch(Exception e){
						System.err.println("Inserimento fallito 1 : "+s.getName()+","+base);
					}
				}
				if(!s.getName().equals("")&&!s.getName().contains("Uff_eFatturaPA")){
					nomeLivello = s.getName()+","+nomeLivello;
					scorriAlberaturaIndicePA( nomeLivello,  context, con);
				}
			}
		}catch(NamingException ne){
//			log.error("Errore connessione ldap",ne);
		}
		return risultati;
	}
	
	public static void valorizzaIndicePA(Indicepa indicepa, Attributes attribute){
		indicepa.setCodou(setValore(attribute,"codiceUO"));
		indicepa.setDescription(setValore(attribute,"description"));
		indicepa.setAoo(setValore(attribute,"aoo"));
		indicepa.setAooref(setValore(attribute,"aooRef"));
		indicepa.setCognomeresp(setValore(attribute,"cognomeResp"));
		
/*		indicepa.setDesaoo(setValore(attribute,"description"));
		indicepa.setDeso(setValore(attribute,"description"));
		indicepa.setDesou(setValore(attribute,"description"));
		indicepa.setCodamm(setValore(attribute,"description"));
		indicepa.setCodaoo(setValore(attribute,"description"));
*/
		indicepa.setDominiopec(setValore(attribute,"dominioPEC"));
		indicepa.setCodiceUnivocoUO(setValore(attribute,"CodiceUnivocoUO"));
		indicepa.setForm(setValore(attribute,"structuralObjectClass"));
		indicepa.setIsldap("1");
		indicepa.setL(setValore(attribute,"l"));
		indicepa.setMail(setValore(attribute,"mail"));
		indicepa.setMailresp(setValore(attribute,"mailResp"));
		indicepa.setNomeresp(setValore(attribute,"nomeResp"));
		indicepa.setO(setValore(attribute,"o"));
		indicepa.setOu(setValore(attribute,"ou"));
		indicepa.setProvincia(setValore(attribute,"provincia"));
		indicepa.setRegione(setValore(attribute,"regione"));
		indicepa.setTipoamm(setValore(attribute,"tipoAmm"));
		indicepa.setTipostruttura(setValore(attribute,"description"));
		indicepa.setCodiceFiscale(setValore(attribute,"codiceFiscaleAmm"));
	}
	
	public static String setValore(Attributes attribute, String valore){
		String result = null;
		javax.naming.directory.Attribute a = attribute.get(valore);
		try {
			if(a!=null && a.get()!=null)
				result= a.get().toString();
		} catch (Exception e) {
			// TODO Blocco catch generato automaticamente
			log.error("Errore connessione ldap",e);
		}
		return result;
	}
	
	public static boolean insertIndicePA(Indicepa indicepa, Connection con) throws SQLException{
		boolean msgOk = true;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
//		Connection con = null;
		try{
			con.setAutoCommit(false);
			if(indicepa.getMail() != null && !indicepa.getMail().contains("postacert.inps.gov.it")){
				boolean abilitaInserimento = true;
				String querySelect = ConfigProp.selectEmailIndicePA; //" select mail from IndicePA where mail = ? ";
				pstmt = con.prepareStatement(querySelect);
				pstmt.setString(1,indicepa.getMail());
				rs = pstmt.executeQuery();
				if(rs.next()){
					abilitaInserimento = false;
				}
//				,[CodiceFiscale] ,[CodiceUnivocoUO] Da fare
				if(abilitaInserimento){
			        String queryInsert = ConfigProp.insertIndicePA; 
			        /*	
			           "INSERT INTO [PECPEI].[dbo].[IndicePA]" +
			           " ([CodAMM] ,[CodO] ,[CodOU] ,[description] ,[DesO] ,[DesOU] ,[Form] ,[ISLdap] ,[l],[mailResp] "+
			           " ,[nomeResp] ,[cognomeResp] ,[O] ,[OU] ,[provincia] ,[regione] ,[mail] ,[TipoAmm],[TipoStruttura] "+
			           " ,[AOO] ,[AOORef] ,[codAOO] ,[DesAOO] ,[dominioPEC] )  VALUES ( "+
			           " ?,?,?,?,?,?,?,?,?,?, "+
			           " ?,?,?,?,?,?,?,?,?, "+
			           " ?,?,?,?,? )";
			        */
			        pstmt = con.prepareStatement(queryInsert);
			        pstmt.setString(1, indicepa.getCodamm());
			        pstmt.setString(2, indicepa.getCodo());
			        pstmt.setString(3, indicepa.getCodou());
			        pstmt.setString(4, indicepa.getDescription());
			        pstmt.setString(5, indicepa.getDeso());
			        pstmt.setString(6, indicepa.getDesou());
			        pstmt.setString(7, indicepa.getForm());
			        pstmt.setString(8, indicepa.getIsldap());
			        pstmt.setString(9, indicepa.getL());
			        pstmt.setString(10, indicepa.getMailresp());
			        pstmt.setString(11, indicepa.getNomeresp());
			        pstmt.setString(12, indicepa.getCognomeresp());
			        pstmt.setString(13, indicepa.getO());
			        pstmt.setString(14, indicepa.getOu());
			        pstmt.setString(15, indicepa.getProvincia());
			        pstmt.setString(16, indicepa.getRegione());
			        pstmt.setString(17, indicepa.getMail());
			        pstmt.setString(18, indicepa.getTipoamm());
			        pstmt.setString(19, indicepa.getTipostruttura());
			        pstmt.setString(20, indicepa.getAoo());
			        pstmt.setString(21, indicepa.getAooref());
			        pstmt.setString(22, indicepa.getCodaoo());
			        pstmt.setString(23, indicepa.getDesaoo());
			        pstmt.setString(24, indicepa.getDominiopec());
//			        pstmt.setString(25, indicepa.getCodiceFiscale());
//			        pstmt.setString(26, indicepa.getCodiceUnivocoUO());
		        
			        int i = pstmt.executeUpdate();
				}
			}
		}catch(Exception e){
			log.error("Errore connessione database",e);
		}finally{
			if(pstmt!=null)
				 pstmt.close();
			if(rs!=null)
				 rs.close();
			
		}
		return msgOk;
	}
	
	public static void deleteALLIndicePA(Connection con) throws SQLException{
		String deleteIndicePA = ConfigProp.deleteIndicePA;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			pstmt = con.prepareStatement(deleteIndicePA);
			pstmt.executeUpdate();
		}catch(Exception e){
			log.error("Errore connessione database",e);
		}finally{
			if(pstmt!=null)
				 pstmt.close();
			if(rs!=null)
				 rs.close();
		}
	}


	@Override
	public Object call() throws Exception {
		// TODO Stub di metodo generato automaticamente
		return execute();
	}
}
