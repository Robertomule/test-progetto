package it.eustema.inps.agentPecPei.aggiornaIndicePA;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dao.EsecuzioniIndiciPaNewDAO;
import it.eustema.inps.agentPecPei.dao.IndicePaAmministrazioniDAO;
import it.eustema.inps.agentPecPei.dao.IndiciPaUltimoFileDao;
import it.eustema.inps.agentPecPei.dto.EsecuzioniIndiciPaNew;
import it.eustema.inps.agentPecPei.dto.IndiciPaAmministrazioniTmpDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.logging.LoggingConfigurator;
import it.inps.agentPec.verificaPec.dao.ConfigDAO;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;


public class AggiornaIndicePaFile implements Command, Callable<Object> {

	private static Logger log = Logger.getLogger(AggiornaIndicePaFile.class);
	public static String base = "c=it";
	private ConfigDAO configdao;
	EsecuzioniIndiciPaNewDAO execDao = new EsecuzioniIndiciPaNewDAO();
	
	static {
		//blocco eseguito una volta al caricamento della classe
        try {
			// carica i parametri dal file di cfg
			//new ConfigProp(cfgFileName);
			new ConfigProp().init();
			// inizializzazione configurazione del logging
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
		} catch (Exception e) {
			log.error("Aggiorna Indici PA da File --> " + e.getMessage());
			System.out.println("Aggiorna Indici PA da File --> " + e.getMessage());
		}        
    }
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "AggiornaIndicePaFile";
	}

	
	@Override
	public Object execute(){
			int starter = 0;
			int delayDay = 0;
			boolean flgStart = false;
			
			try{
				configdao = new ConfigDAO();
				starter = Integer.parseInt(configdao.getProperty("indiciPaOraStart", "23"));
				delayDay = Integer.parseInt(configdao.getProperty("indiciPaDelayGiorni", "15"));
				
				log.error("Aggiorna Indici PA da File --> starter = " +starter + " delayDay = " +delayDay);
				System.out.println("Aggiorna Indici PA da File --> starter = " +starter + " delayDay = " +delayDay);
				
				//verifico se avviare l'esecuzione				
				if(starter == Calendar.getInstance().get(Calendar.HOUR_OF_DAY) && delayDay <= calcoloDelay(execDao.getUltimaEsecuzione()))
					flgStart = true;
				
			}catch(Exception e){
				e.printStackTrace();
				log.error("Aggiorna Indici PA da File --> Errore in caricamento starter e delay:"+e.getMessage()+" --> starter = " +starter + " delayDay = " +delayDay);
				System.out.println("Aggiorna Indici PA da File --> Errore in caricamento starter e delay:"+e.getMessage()+" --> starter = " +starter + " delayDay = " +delayDay);
			}
			
			
			if(flgStart){	
				try{
					log.error("Aggiorna Indici PA da File --> Avvio il thread");
					System.out.println("Aggiorna Indici PA da File --> Avvio il thread");
					
					start(Integer.parseInt(configdao.getProperty("indiciPaInsertSimultanee", "100")));
				}catch(Exception e){
					log.error("ERRORE: Aggiorna Indici PA da FilE --> " + e.getMessage());
					System.out.println("ERRORE: Aggiorna Indici PA da FilE --> " + e.getMessage());
					e.printStackTrace();
				}
				
				System.out.println("Aggiorna Indici PA da FilE --> Fine  sincronizzazione indici PA");

			}else {
				log.error("Aggiorna Indici PA da FilE --> Non ci sono le condizione per avviare l'esecuzione. Termino!");
				System.out.println("Aggiorna Indici PA da FilE --> Non ci sono le condizione per avviare l'esecuzione. Termino!");
			}
			
		return null;
	}
	
	public void start(int numeroInsertSimultanee) throws BusinessException {

		EsecuzioniIndiciPaNew esecuzione = new EsecuzioniIndiciPaNew();
		InputStream is = null;
		
		//utilizziamo il log di errore perch� gli altri sono stati disabilitati per motivi prestazionali
		log.error("Aggiorna Indici PA da FilE --> sono nel metodo Start()");
//		System.out.println("Aggiorna Indici PA da FilE --> sono nel metodo Start()");
		int contatore = 0,righeInserite = 0;
		
		try{
			
			IndicePaAmministrazioniDAO indiciDao = new IndicePaAmministrazioniDAO();
			String remoteUrl = configdao.getProperty("urlFileIndiciPa", "");
			esecuzione.setInizioEsecuzione(new java.sql.Timestamp(new Date().getTime()));
			
				try{
//					TrustManager[] trustAllCerts = new TrustManager[]{
//				    new X509TrustManager() {
//				        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//				            return null;
//				        }
//				        public void checkClientTrusted(
//				            java.security.cert.X509Certificate[] certs, String authType) {
//				        }
//				        public void checkServerTrusted(
//				            java.security.cert.X509Certificate[] certs, String authType) {
//				        }
//				    }
//				};
//
//				try {
//				    SSLContext sc = SSLContext.getInstance("SSL");
//				    sc.init(null, trustAllCerts, new java.security.SecureRandom());
//				    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
					
//					HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("https://www.indicepa.gov.it").openConnection();
//	                httpURLConnection.setReadTimeout(1000000);
//	                httpURLConnection.setConnectTimeout(1500000);
//	                httpURLConnection.setDoOutput(true);
//	                httpURLConnection.setRequestProperty("Accept", "application/json, text/plain, */*");
//	                httpURLConnection.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
//	                httpURLConnection.setRequestProperty("Accept-Language", "it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3");
//	                httpURLConnection.setRequestProperty("Connection", "keep-alive");
//	                httpURLConnection.setRequestProperty("Host", "www.indicepa.gov.it");
//	                httpURLConnection.setRequestProperty("Proxy-Authorization","Negotiate YIIHxAYGKwYBBQUCoIIHuDCCB7SgMDAuBgkqhkiC9xIBAgIGCSqGSIb3EgECAgYKKwYBBAGCNwICHgYKKwYBBAGCNwICCqKCB34Eggd6YIIHdgYJKoZIhvcSAQICAQBuggdlMIIHYaADAgEFoQMCAQ6iBwMFACAAAACjggXsYYIF6DCCBeSgAwIBBaEOGwxSSVNPUlNFLklOUFOiKDAmoAMCAQKhHzAdGwRIVFRQGxVwcm94eS13cy5yaXNvcnNlLmlucHOjggWhMIIFnaADAgEXoQMCAQOiggWPBIIFi7F6nR0evIb56zFPElZ9eLpWCw9WDIkAt5fQaMNNGUPMeHY");
//	                httpURLConnection.setRequestProperty("UserAgent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0");
//	                httpURLConnection.setRequestProperty("GET", "/public-services/opendata-read-service.php?dstype=FS&filename=amministrazioni.txt HTTP/1.1");
//	                httpURLConnection.setRequestMethod("GET");
//	                
//	                OutputStream outputStream = httpURLConnection.getOutputStream();
//	                outputStream.close();
					
//					URL url = null;
//					URLConnection con = null;
//					int i;
//					url = new URL("https://www.indicepa.gov.it/public-services/opendata-read-service.php?dstype=FS&filename=amministrazioni.txt");
//					con = url.openConnection();
//					File file = new File("C:\\certificato\\Address.txt");
//					BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
//					BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file.getName()));
//					while ((i = bis.read()) != -1) {
//						bos.write(i);
//					}
//					bos.flush();
//					bis.close();
					
//					URL url = new URL(remoteUrl);
//					URLConnection connection = url.openConnection();
					
					//Leggo la risposta
//					InputStream is = connection.getInputStream();
					
					IndiciPaUltimoFileDao fileDao = new IndiciPaUltimoFileDao();
					byte[] file = fileDao.getUltimoFileDaLavorare();
					is = new ByteArrayInputStream(file);
					
				}catch (Exception e) {
				   e.printStackTrace();
				}			


                String line = null;

//				InputStream is = connection.getInputStream();
//				StringWriter writer = new StringWriter();
//				IOUtils.copy(is, writer);
//				String stringFile = writer.toString();
                
                //***inserisco il file nell'oggetto di log****************//
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[1024];
                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                esecuzione.setUltimoFileLavorato(buffer.toByteArray());
                //***********************************************************//
                
                //inserisco i dati del file dentro la tabella IndiciPaAmministrazioniTmpDTO
                //questa tabella verr� svuotata da una procedura sql server che popola la tabella principale IndiciPaAmministrazioniDTO
                boolean primariga = true;
                String[] datiArray;
                List<IndiciPaAmministrazioniTmpDTO> indiciList = new ArrayList<IndiciPaAmministrazioniTmpDTO>();
                IndiciPaAmministrazioniTmpDTO indiceTmp;
                
                is = new ByteArrayInputStream(buffer.toByteArray()); //Creo nuovamente InputStream
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                
                //svuoto la tabella temporanea prima di effettuare i nuovi inserimenti (Dovrebbe essere gi� vuota in quanto viene svuotata dalla procedura sql)
                indiciDao.deleteDataFromTable();
                
                while ((line = reader.readLine()) != null) {
                	//non lavoro la prima riga perch� � quella di header
                	if(!primariga){
                		//creo una stringa json manualmente che poi trasformo in un oggetto java
                		datiArray = line.split("\t");
                		indiceTmp = new IndiciPaAmministrazioniTmpDTO();
                		
                		indiceTmp.setCod_amm(datiArray[0]!=null?datiArray[0]:"");
                		indiceTmp.setDes_amm(datiArray[1]!=null?datiArray[1]:"");
                		indiceTmp.setComune(datiArray[2]!=null?datiArray[2]:"");
                		indiceTmp.setNome_resp(datiArray[3]!=null?datiArray[3]:"");
                		indiceTmp.setCogn_resp(datiArray[4]!=null?datiArray[4]:"");
                		indiceTmp.setCap(datiArray[5]!=null?datiArray[5]:"");
                		indiceTmp.setProvincia(datiArray[6]!=null?datiArray[6]:"");
                		indiceTmp.setRegione(datiArray[7]!=null?datiArray[7]:"");
                		indiceTmp.setSito_istituzionale(datiArray[8]!=null?datiArray[8]:"");
                		indiceTmp.setIndirizzo(datiArray[9]!=null?datiArray[9]:"");
                		indiceTmp.setTitolo_resp(datiArray[10]!=null?datiArray[10]:"");
                		indiceTmp.setTipologia_istat(datiArray[11]!=null?datiArray[11]:"");
                		indiceTmp.setTipologia_amm(datiArray[12]!=null?datiArray[12]:"");
                		indiceTmp.setAcronimo(datiArray[13]!=null?datiArray[13]:"");
                		indiceTmp.setCf_validato(datiArray[14]!=null?datiArray[14]:"");
                		indiceTmp.setCf(datiArray[15]!=null?datiArray[15]:"");
                		indiceTmp.setMail1(datiArray[16]!=null?datiArray[16]:"");
                		indiceTmp.setTipo_mail1(datiArray[17]!=null?datiArray[17]:"");
                		indiceTmp.setMail2(datiArray[18]!=null?datiArray[18]:"");
                		indiceTmp.setTipo_mail2(datiArray[19]!=null?datiArray[19]:"");
                		indiceTmp.setMail3(datiArray[20]!=null?datiArray[20]:"");
                		indiceTmp.setTipo_mail3(datiArray[21]!=null?datiArray[21]:"");
                		indiceTmp.setMail4(datiArray[22]!=null?datiArray[22]:"");
                		indiceTmp.setTipo_mail4(datiArray[23]!=null?datiArray[23]:"");
                		indiceTmp.setMail5(datiArray[24]!=null?datiArray[24]:"");
                		indiceTmp.setTipo_mail5(datiArray[25]!=null?datiArray[25]:"");
                		indiceTmp.setUrl_facebook(datiArray[26]!=null?datiArray[26]:"");
                		indiceTmp.setUrl_twitter(datiArray[27]!=null?datiArray[27]:"");
                		indiceTmp.setUrl_googleplus(datiArray[28]!=null?datiArray[28]:"");
                		indiceTmp.setUrl_youtube(datiArray[29]!=null?datiArray[29]:"");
                		indiceTmp.setLiv_accessibili(datiArray[30]!=null?datiArray[30]:"");
                		
                		indiciList.add(indiceTmp);
                		
                		if(contatore++ >= numeroInsertSimultanee){
                			//faccio le insert e svuoto la lista
                			righeInserite += indiciDao.insertIndiciAmministrazioni(indiciList);
                			indiciList = new ArrayList<IndiciPaAmministrazioniTmpDTO>();
                			contatore = 0;
                		}
                		
                	}else{
                		primariga = false;
                	}
                }
				
                //inserisco gli indici non ancora inseriti perch� la lista ha dimensione < numeroInsertSimultanee
                righeInserite += indiciDao.insertIndiciAmministrazioni(indiciList);
                
                esecuzione.setTotLavorati(righeInserite);
                
				log.error("Aggiorna Indici PA da FilE --> metodo Start() terminato");
//				System.out.println("Aggiorna Indici PA da FilE --> metodo Start() terminato");
				
		}catch(Exception e){
			e.printStackTrace();
			log.error("Aggiorna Indici PA da FilE --> Errore: "+e.getMessage()+" terminato");
//			System.out.println("Aggiorna Indici PA da FilE --> Errore: "+e.getMessage()+" terminato");
			esecuzione.setErrore(e.getMessage());
		}finally{
			try {
				esecuzione.setFineEsecuzione(new java.sql.Timestamp(new Date().getTime()));
                esecuzione.setTempoEsecuzione(""+((esecuzione.getFineEsecuzione()!=null?esecuzione.getFineEsecuzione().getTime():0)-esecuzione.getInizioEsecuzione().getTime()));
				execDao.insertEsecuzione(esecuzione);
			} catch (DAOException e) {
				log.error("Aggiorna Indici PA da FilE::260 --> DAOException260: "+e.getMessage());
				e.printStackTrace();
			}
		}
	}

	@Override
	public Object call() throws Exception {
		// TODO Stub di metodo generato automaticamente
		return execute();
	}
	
	private int calcoloDelay(Timestamp timestamp){
		int dayDiff = 0;
		try{
			Calendar last = Calendar.getInstance();
			last.setTimeInMillis( timestamp.getTime() );
			
			long diff = Calendar.getInstance().getTimeInMillis() - last.getTimeInMillis();
			dayDiff = (int) (diff / (1000 * 60 * 60 * 24));
			System.out.println("Aggiorna Indici PA da FilE:: esito calcolo delay giorni: "+dayDiff);
		}catch(Exception e){
			System.out.println("Aggiorna Indici PA da FilE:: Errore in calcoloDelay: "+e.getMessage());
		}
		return dayDiff;
	}
}
