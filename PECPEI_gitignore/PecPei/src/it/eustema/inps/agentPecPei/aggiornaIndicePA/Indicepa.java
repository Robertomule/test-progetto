package it.eustema.inps.agentPecPei.aggiornaIndicePA;

import java.io.Serializable;

public class Indicepa implements Serializable {
	private int id;

	private String codamm;

	private String codo;

	private String codou;

	private String description;

	private String deso;

	private String desou;

	private String form;

	private String isldap;

	private String l;

	private String mailresp;

	private String nomeresp;

	private String cognomeresp;

	private String o;

	private String ou;

	private String provincia;

	private String regione;

	private String mail;

	private String tipoamm;

	private String tipostruttura;

	private String aoo;

	private String aooref;

	private String codaoo;

	private String desaoo;

	private String dominiopec;

	private String codiceFiscale;
	
	private String codiceUnivocoUO;
	

	public String getCodiceUnivocoUO() {
		return codiceUnivocoUO;
	}

	public void setCodiceUnivocoUO(String codiceUnivocoUO) {
		this.codiceUnivocoUO = codiceUnivocoUO;
	}
	
	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	private static final long serialVersionUID = 1L;

	public Indicepa() {
		super();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodamm() {
		return this.codamm;
	}

	public void setCodamm(String codamm) {
		this.codamm = codamm;
	}

	public String getCodo() {
		return this.codo;
	}

	public void setCodo(String codo) {
		this.codo = codo;
	}

	public String getCodou() {
		return this.codou;
	}

	public void setCodou(String codou) {
		this.codou = codou;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeso() {
		return this.deso;
	}

	public void setDeso(String deso) {
		this.deso = deso;
	}

	public String getDesou() {
		return this.desou;
	}

	public void setDesou(String desou) {
		this.desou = desou;
	}

	public String getForm() {
		return this.form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getIsldap() {
		return this.isldap;
	}

	public void setIsldap(String isldap) {
		this.isldap = isldap;
	}

	public String getL() {
		return this.l;
	}

	public void setL(String l) {
		this.l = l;
	}

	public String getMailresp() {
		return this.mailresp;
	}

	public void setMailresp(String mailresp) {
		this.mailresp = mailresp;
	}

	public String getNomeresp() {
		return this.nomeresp;
	}

	public void setNomeresp(String nomeresp) {
		this.nomeresp = nomeresp;
	}

	public String getCognomeresp() {
		return this.cognomeresp;
	}

	public void setCognomeresp(String cognomeresp) {
		this.cognomeresp = cognomeresp;
	}

	public String getO() {
		return this.o;
	}

	public void setO(String o) {
		this.o = o;
	}

	public String getOu() {
		return this.ou;
	}

	public void setOu(String ou) {
		this.ou = ou;
	}

	public String getProvincia() {
		return this.provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getRegione() {
		return this.regione;
	}

	public void setRegione(String regione) {
		this.regione = regione;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTipoamm() {
		return this.tipoamm;
	}

	public void setTipoamm(String tipoamm) {
		this.tipoamm = tipoamm;
	}

	public String getTipostruttura() {
		return this.tipostruttura;
	}

	public void setTipostruttura(String tipostruttura) {
		this.tipostruttura = tipostruttura;
	}

	public String getAoo() {
		return this.aoo;
	}

	public void setAoo(String aoo) {
		this.aoo = aoo;
	}

	public String getAooref() {
		return this.aooref;
	}

	public void setAooref(String aooref) {
		this.aooref = aooref;
	}

	public String getCodaoo() {
		return this.codaoo;
	}

	public void setCodaoo(String codaoo) {
		this.codaoo = codaoo;
	}

	public String getDesaoo() {
		return this.desaoo;
	}

	public void setDesaoo(String desaoo) {
		this.desaoo = desaoo;
	}

	public String getDominiopec() {
		return this.dominiopec;
	}

	public void setDominiopec(String dominiopec) {
		this.dominiopec = dominiopec;
	}

	/* (non Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/* (non Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Indicepa)) {
			return false;
		}
		Indicepa other = (Indicepa) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
