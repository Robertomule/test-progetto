package it.eustema.inps.agentPecPei.exception;

public class PecBusinessException extends Exception {

	private static final long serialVersionUID = 3971919902147120405L;

	public PecBusinessException(String msg){
		super(msg);
	}
}
