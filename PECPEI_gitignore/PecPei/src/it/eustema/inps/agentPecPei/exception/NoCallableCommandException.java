package it.eustema.inps.agentPecPei.exception;

/**
 * Exception Rilanciata da un CommandExecutorMultiThread quando gli viene assegnato un Command con Callable
 * @author ALEANZI
 *
 */
public class NoCallableCommandException extends BusinessException {

	public NoCallableCommandException(int errorCode, String message) {
		super(errorCode, message);
		// TODO Auto-generated constructor stub
	}
	
	public NoCallableCommandException(int errorCode, String message,
			String forward) {
		super(errorCode, message, forward);
		// TODO Auto-generated constructor stub
	}

}
