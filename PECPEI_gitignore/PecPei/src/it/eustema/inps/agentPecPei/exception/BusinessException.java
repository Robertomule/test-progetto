package it.eustema.inps.agentPecPei.exception;

/**
 * Business Exception
 * @author ALEANZI
 *
 */
public class BusinessException extends Exception{
	private int errorCode;
	private String message;
	private String forward;
	
	
	
	public BusinessException(int errorCode, String message){
		this.errorCode = errorCode;
		this.message = message;
	}
	
	public BusinessException(int errorCode, String message, String forward){
		this.errorCode = errorCode;
		this.message = message;
		this.forward = forward;
	}
	
	
	public int getErrorCode() {
		return errorCode;
	}
	public String getMessage() {
		return message;
	}

	public String getForward() {
		return forward;
	}
	
	
}
