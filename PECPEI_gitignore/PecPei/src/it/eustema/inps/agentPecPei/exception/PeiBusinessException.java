package it.eustema.inps.agentPecPei.exception;

public class PeiBusinessException extends Exception {

	private static final long serialVersionUID = 7794078316811898742L;

	public PeiBusinessException(String msg){
		super(msg);
	}
}
