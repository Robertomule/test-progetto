package it.eustema.inps.agentPecPei.exception;

public class HermesBusinessException extends Exception {

	private static final long serialVersionUID = -4278789685266080863L;

	public HermesBusinessException(String msg){
		super(msg);
	}
}
