package it.eustema.inps.agentPecPei.exception;

public class CartellaNotEmptyException extends Exception {

	private static final long serialVersionUID = 6182320727265820687L;
	
	public CartellaNotEmptyException(String msg){
		super(msg);
	}

}
