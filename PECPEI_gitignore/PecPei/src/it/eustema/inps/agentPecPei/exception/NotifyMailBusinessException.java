package it.eustema.inps.agentPecPei.exception;

/**
 * Business Exception che comprende una notifica dell'errore per eMail
 * @author ALEANZI
 *
 */
public class NotifyMailBusinessException extends BusinessException {

	public NotifyMailBusinessException(int errorCode, String message) {
		super(errorCode, message);
		// TODO Auto-generated constructor stub
	}

}
