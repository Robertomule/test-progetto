package it.eustema.inps.agentPecPei.exception;

public class DAOException extends Exception{


	private static final long serialVersionUID = 6461041214160506246L;

	public DAOException(String msg){
		super(msg);
	}
}
