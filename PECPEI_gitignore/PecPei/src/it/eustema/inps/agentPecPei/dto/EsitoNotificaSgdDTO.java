package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;
import java.sql.Date;

public class EsitoNotificaSgdDTO implements Serializable, DTO {
	
	private static final long serialVersionUID = -3807106920712331448L;
	
	private int id;
	private String metodo;
	private String accountPec;
	private String messageID;
	private String msgID;
	private String stato;
	private String esito;
	private String descrizioneErrore;
	private Date DataEsecuzione;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getMetodo() {
		return metodo;
	}
	
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	
	public String getAccountPec() {
		return accountPec;
	}
	
	public void setAccountPec(String accountPec) {
		this.accountPec = accountPec;
	}
	
	public String getMessageID() {
		return messageID;
	}
	
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}
	
	public String getMsgID() {
		return msgID;
	}
	
	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}
	
	public String getStato() {
		return stato;
	}
	
	public void setStato(String stato) {
		this.stato = stato;
	}
	
	public String getEsito() {
		return esito;
	}
	
	public void setEsito(String esito) {
		this.esito = esito;
	}
	
	public String getDescrizioneErrore() {
		return descrizioneErrore;
	}
	
	public void setDescrizioneErrore(String descrizioneErrore) {
		this.descrizioneErrore = descrizioneErrore;
	}
	
	public Date getDataEsecuzione() {
		return DataEsecuzione;
	}
	
	public void setDataEsecuzione(Date dataEsecuzione) {
		DataEsecuzione = dataEsecuzione;
	}
	
}
