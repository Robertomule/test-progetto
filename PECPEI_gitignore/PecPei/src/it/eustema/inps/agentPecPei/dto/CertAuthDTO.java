package it.eustema.inps.agentPecPei.dto;

import java.sql.Timestamp;

public class CertAuthDTO {
	private Timestamp signValidFrom;
	private Timestamp signValidTo;
	private String subjectName;
	private String issuerName;
	private String signed;
	private String signedVer;
	
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public Timestamp getSignValidFrom() {
		return signValidFrom;
	}
	public void setSignValidFrom(Timestamp signValidFrom) {
		this.signValidFrom = signValidFrom;
	}
	public Timestamp getSignValidTo() {
		return signValidTo;
	}
	public void setSignValidTo(Timestamp signValidTo) {
		this.signValidTo = signValidTo;
	}
	public String getSigned() {
		return signed;
	}
	public void setSigned(String signed) {
		this.signed = signed;
	}
	public String getSignedVer() {
		return signedVer;
	}
	public void setSignedVer(String signedVer) {
		this.signedVer = signedVer;
	}

	
	
}
