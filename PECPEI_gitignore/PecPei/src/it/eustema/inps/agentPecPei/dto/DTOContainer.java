package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DTOContainer implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Map<String, DTO> map = new HashMap<String, DTO>();
	
	public DTO getDTO( String name ){
		return map.get( name );
	}
	
	public void putDTO( String name, DTO dto ){
		map.put( name, dto );
	}
	
	public void putAll( DTOContainer container ){
		
		map.putAll( container.map );
	}
}
