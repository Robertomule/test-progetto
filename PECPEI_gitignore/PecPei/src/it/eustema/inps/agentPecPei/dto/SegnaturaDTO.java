package it.eustema.inps.agentPecPei.dto;



public class SegnaturaDTO implements DTO {
	public static final int OK = 0;
	public static final int KO = 1;
	private int status;
	
	private String codiceErrore;
	private String descErrore;
	
	private String segnatura;
	private String documentoPrincipale;
	private String xmlInput;
	
	
	
	
	
	public String getDocumentoPrincipale() {
		return documentoPrincipale;
	}
	public void setDocumentoPrincipale(String documentoPrincipale) {
		this.documentoPrincipale = documentoPrincipale;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCodiceErrore() {
		return codiceErrore;
	}
	public void setCodiceErrore(String codiceErrore) {
		this.codiceErrore = codiceErrore;
	}
	public String getDescErrore() {
		return descErrore;
	}
	public void setDescErrore(String descErrore) {
		this.descErrore = descErrore;
	}
	
	public boolean isOK(){
		return status == OK;
	}
	public String getSegnatura() {
		return segnatura;
	}
	public void setSegnatura(String segnatura) {
		this.segnatura = segnatura;
	}
	public String getXmlInput() {
		return xmlInput;
	}
	public void setXmlInput(String xmlInput) {
		this.xmlInput = xmlInput;
	}
	
	
	
	
}
