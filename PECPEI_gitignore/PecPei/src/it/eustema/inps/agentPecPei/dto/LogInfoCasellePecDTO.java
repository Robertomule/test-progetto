package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;
import java.util.Date;

public class LogInfoCasellePecDTO implements Serializable, DTO
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int infoID;

	private String codiceSede;
	
	private String accountPEC;
	
	private long limite;
	
	private long utilizzato;
	
	private long libero;

	private Date dataVerifica;

	public void setInfoID(int infoID) {
		this.infoID = infoID;
	}

	public int getInfoID() {
		return infoID;
	}

	public void setCodiceSede(String codiceSede) {
		this.codiceSede = codiceSede;
	}

	public String getCodiceSede() {
		return codiceSede;
	}

	public void setAccountPEC(String accountPEC) {
		this.accountPEC = accountPEC;
	}

	public String getAccountPEC() {
		return accountPEC;
	}

	public void setLimite(long limite) {
		this.limite = limite;
	}

	public long getLimite() {
		return limite;
	}

	public void setUtilizzato(long utilizzato) {
		this.utilizzato = utilizzato;
	}

	public long getUtilizzato() {
		return utilizzato;
	}

	public void setLibero(long libero) {
		this.libero = libero;
	}

	public long getLibero() {
		return libero;
	}

	public void setDataVerifica(Date dataVerifica) {
		this.dataVerifica = dataVerifica;
	}

	public Date getDataVerifica() {
		return dataVerifica;
	}

}
