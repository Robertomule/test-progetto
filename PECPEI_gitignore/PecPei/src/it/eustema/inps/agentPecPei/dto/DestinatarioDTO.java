package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;
import java.util.Date;

public class DestinatarioDTO implements Serializable, DTO {

	private static final long serialVersionUID = -5953999345643107829L;
	private String messageID;
	private String codiceAOO;
	private String addr;
	private String phrase;
	private String type;
	private String name;
	private Date postedDate;
	
	public Date getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
	public String getMessageID() {
		return messageID;
	}
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}
	public String getCodiceAOO() {
		return codiceAOO;
	}
	public void setCodiceAOO(String codiceAOO) {
		this.codiceAOO = codiceAOO;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getPhrase() {
		return phrase;
	}
	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
