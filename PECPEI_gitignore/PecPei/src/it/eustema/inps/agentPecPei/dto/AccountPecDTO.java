package it.eustema.inps.agentPecPei.dto;

import it.eustema.inps.utility.ConfigProp;

public class AccountPecDTO implements DTO {

	private String userName;
	private String password;
	private String codAOO;
	
	private String descrizioneSede;
	private String regioneSede;
	private String ufficioSede;
	
	private String hostPop;
	private String storeProtocolPop;
	private String portPop;
	private String sslTrustPop;
	
	private String hostSmtp;
	private String portSmtp;
	
	public AccountPecDTO(String userName, String password){
		this.userName = userName;
		this.password = password;
	}
	public AccountPecDTO(){
		this.hostPop = ConfigProp.hostPopAccountPEC;
		this.storeProtocolPop = ConfigProp.storePopProtocolAccountPEC;
		this.portPop = ConfigProp.portPopAccountPEC;
		this.sslTrustPop = ConfigProp.sslPopTrustAccountPEC;
		this.hostSmtp = ConfigProp.hostSmtpAccountPEC;
		this.portSmtp = ConfigProp.portSmtpAccountPEC;
	}

	public String getDescrizioneSede() {
		return descrizioneSede;
	}

	public void setDescrizioneSede(String descrizioneSede) {
		this.descrizioneSede = descrizioneSede;
	}

	public String getRegioneSede() {
		return regioneSede;
	}

	public void setRegioneSede(String regioneSede) {
		this.regioneSede = regioneSede;
	}

	public String getUfficioSede() {
		return ufficioSede;
	}

	public void setUfficioSede(String ufficioSede) {
		this.ufficioSede = ufficioSede;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHostPop() {
		return hostPop;
	}

	public void setHostPop(String host) {
		this.hostPop = host;
	}

	public String getStoreProtocolPop() {
		return storeProtocolPop;
	}

	public void setStoreProtocolPop(String storeProtocol) {
		this.storeProtocolPop = storeProtocol;
	}

	public String getPortPop() {
		return portPop;
	}

	public void setPortPop(String port) {
		this.portPop = port;
	}

	public String getSslTrustPop() {
		return sslTrustPop;
	}

	public void setSslTrustPop(String sslTrust) {
		this.sslTrustPop = sslTrust;
	}

	public String getCodAOO() {
		return codAOO;
	}

	public void setCodAOO(String codAOO) {
		this.codAOO = codAOO;
	}

	public String getHostSmtp() {
		return hostSmtp;
	}

	public void setHostSmtp(String hostSmtp) {
		this.hostSmtp = hostSmtp;
	}

	public String getPortSmtp() {
		return portSmtp;
	}

	public void setPortSmtp(String portSmtp) {
		this.portSmtp = portSmtp;
	}
	
	
	
}
