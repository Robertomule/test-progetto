package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;

public class AgentExecutionParamDTO implements Serializable, DTO {

	private static final long serialVersionUID = -6408016149578190953L;
	private String paramName;
	private String paramValue;
	private String patternValidazione;
	

	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getParamValue() {
		return paramValue;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public String getPatternValidazione() {
		return patternValidazione;
	}
	public void setPatternValidazione(String patternValidazione) {
		this.patternValidazione = patternValidazione;
	}

}
