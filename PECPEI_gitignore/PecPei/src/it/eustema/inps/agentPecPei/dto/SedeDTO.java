package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;

public class SedeDTO implements Serializable, DTO {
	
	private static final long serialVersionUID = -5980524146275159445L;
	
	private String codice;
	private String descrSede;
	private String direttore;
	private String regione;
	private String provincia;
	private String cap;
	private String citta;
	private String matricolaDirettore;
	
	public String getMatricolaDirettore() {
		return matricolaDirettore;
	}
	public void setMatricolaDirettore(String matricolaDirettore) {
		this.matricolaDirettore = matricolaDirettore;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getDescrSede() {
		return descrSede;
	}
	public void setDescrSede(String descrSede) {
		this.descrSede = descrSede;
	}
	public String getDirettore() {
		return direttore;
	}
	public void setDirettore(String direttore) {
		this.direttore = direttore;
	}
	public String getRegione() {
		return regione;
	}
	public void setRegione(String regione) {
		this.regione = regione;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	
	
	
	
}


