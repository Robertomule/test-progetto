package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;
import java.util.List;

public class AgentExecutionParamsDTO implements Serializable, DTO{

	private static final long serialVersionUID = -5890123868955939410L;
	private long execution_id;
	private List<AgentExecutionParamDTO> agentExecutionParams;
	
	public long getExecution_id() {
		return execution_id;
	}
	public void setExecution_id(long execution_id) {
		this.execution_id = execution_id;
	}
	public List<AgentExecutionParamDTO> getAgentExecutionParams() {
		return agentExecutionParams;
	}
	public void setAgentExecutionParams(
			List<AgentExecutionParamDTO> agentExecutionParams) {
		this.agentExecutionParams = agentExecutionParams;
	}

	
}
