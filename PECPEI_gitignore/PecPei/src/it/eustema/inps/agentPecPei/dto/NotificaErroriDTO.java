package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;
import java.util.Date;

public class NotificaErroriDTO implements Serializable, DTO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5898689533085886502L;
	
	private long ID;
	private int codice;
	private String messaggio;
	private String accountPEC;
	private String codiceAOO;
	private String messageID;
	private Date dataCreazione;
	
	public void setID(long iD) {
		ID = iD;
	}
	public long getID() {
		return ID;
	}
	public void setCodice(int codice) {
		this.codice = codice;
	}
	public int getCodice() {
		return codice;
	}
	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}
	public String getMessaggio() {
		return messaggio;
	}
	public void setAccountPEC(String accountPEC) {
		this.accountPEC = accountPEC;
	}
	public String getAccountPEC() {
		return accountPEC;
	}
	public void setCodiceAOO(String codiceAOO) {
		this.codiceAOO = codiceAOO;
	}
	public String getCodiceAOO() {
		return codiceAOO;
	}
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}
	public String getMessageID() {
		return messageID;
	}
	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	public Date getDataCreazione() {
		return dataCreazione;
	}
}
