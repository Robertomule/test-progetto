package it.eustema.inps.agentPecPei.dto;

import it.eustema.inps.agentPecPei.util.AgentStatus;

import java.io.Serializable;
import java.sql.Timestamp;

public class AgentExecutionLogDTO implements Serializable, DTO{


	private static final long serialVersionUID = 2515497480839966571L;
	private long execution_id;
	private Timestamp start_time;
	private Timestamp end_time;
	private String agent_status;
	private String error_desc;
	private int agent_type;
	private int idBatch;
	
	public static final int type_verificapec = 1;
	public static final int type_titolario = 2;
	public static final int type_indicepa = 3;

	
	public AgentExecutionLogDTO(){
		this.setAgent_status(AgentStatus.STARTED.name());
		this.setStart_time(new Timestamp(System.currentTimeMillis()));
	}
	
	
	public long getExecution_id() {
		return execution_id;
	}
	public void setExecution_id(long execution_id) {
		this.execution_id = execution_id;
	}
	public Timestamp getStart_time() {
		return start_time;
	}
	public void setStart_time(Timestamp start_time) {
		this.start_time = start_time;
	}
	public Timestamp getEnd_time() {
		return end_time;
	}
	public void setEnd_time(Timestamp end_time) {
		this.end_time = end_time;
	}
	public String getAgent_status() {
		return agent_status;
	}
	public void setAgent_status(String agent_status) {
		this.agent_status = agent_status;
	}
	public String getError_desc() {
		return error_desc;
	}
	public void setError_desc(String error_desc) {
		this.error_desc = error_desc;
	}
	public int getAgent_type() {
		return agent_type;
	}
	public void setAgent_type(int agent_type) {
		this.agent_type = agent_type;
	}


	public void setIdBatch(int idBatch) {
		this.idBatch = idBatch;
	}


	public int getIdBatch() {
		return idBatch;
	}
	
}
