package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessaggioDTO implements Serializable, DTO {

	private static final long serialVersionUID = -5980524146275159445L;
	private String uiDoc;
	private String accountPec;
	private String dominoPec;
	private String verso;
	private String codiceAOO;
	private String messageId;
	private String normalMID;
	private String nomeSede;
	private String nomeRegione;
	private String nomeUfficio;
	private String msgId;
	private String fromAddr;
	private String fromPhrase;
	private String replyTo;
	private Date postedDate;
	private Date deliveredDate;
	private String subject;
	private String tipoRicevuta;
	private String riservato;
	private String sensibile;
	private String classifica;
	private String desClassifica;
	private String body;
	private String documentoPrincipale;
	private String isReply;
	private String inReplyTo;
	private String inReplyToSegnatura;
	private String inReplyToSubject;
	private String headers;
	private String tipologia;
	private String xTrasporto;
	
	//equivale sul DB al campo X_Ricevuta
	private String ricevuta;
	
	private String tipoMessaggio;
	private String segnatura;
	
	private String segnaturaRif;
	private String segnaturaRoot;
	private String isSigned;
	private String isSignedVer;
	private String datiSignature;
	private String signIssuerName;
	private String signSubjectName;
	private Date signValidFromDate;
	private Date signValidToDate;
	
	private String stato;
	private String statoLettura;
	private int size;
	private byte[] messaggio;
	private String ipAddress;
	private String autoreCompositore;
	private String autoreUltimaModifica;
	private String autoreMittente;
	private Date dataOraUltimaModifica;
	private String dataOraUltimaModificaTxt;
	private String confermaRicezione;
	private String addrConfermaRicezione;
	
	private String codiceAMM;
	private String codiceAOOMitt;
	private String codiceAMMMitt;
	private String bodyHtml;
	private String deliveredDateTxt;
	
	private Integer isSPAMorCCN;
	
	private Date pemlPostedDate;
	private String pemlPostedDateTxt;
	private String pemlSubject;
	private String pemlFrom;
	private String pemlBody;
	private String pemlIsSigned;
	private String pemlIsSignedVer;
	private String pemlDatiSignature;
	private String pemlSignIssuerName;
	private String pemlSignSubjectName;
	private Date pemlSignValidFromDate;
	private String pemlSignValidFromDateTxt;
	private Date pemlSignValidToDate;
	private String pemlSignValidToDateTxt;
	private String pemlHeaders;
	
	private String tipoMitt;
	private String desMitt;
	
	private String direttoreAOO;
	private String direttoreAOOMitt;
	private String substato;
	private String segnaturaMitt;
	
	private String nomiAllegati;
	
	
	private List<DestinatarioDTO> elencoDestinatariPer = new ArrayList<DestinatarioDTO>();
	private List<DestinatarioDTO> elencoDestinatariCc  = new ArrayList<DestinatarioDTO>();
	private List<DestinatarioDTO> elencoDestinatariCcn = new ArrayList<DestinatarioDTO>();
	private List<AllegatoDTO> elencoFileAllegati;
	
	private Integer numAllegati;
	
	private String destPerPreview;
	private String allegatiPreview;
	
	private String postaCertEMLBody;
	private String postaCertEMLSubject;
	
	private SegnaturaDTO segnaturaIdentificatore;
	private MessaggioDTO messaggioIdentificatore;
	
	private String rawContent;
	
	private String idPrenotazione;
	
	private int idCartella;
	private boolean isInteroperabile;
	private String canale;
	
	
	public List<DestinatarioDTO> getElencoDestinatariCcn() {
		return elencoDestinatariCcn;
	}
	public void setElencoDestinatariCcn(List<DestinatarioDTO> elencoDestinatariCcn) {
		this.elencoDestinatariCcn = elencoDestinatariCcn;
	}
	public String getRawContent() {
		return rawContent;
	}
	public void setRawContent(String rawContent) {
		this.rawContent = rawContent;
	}
	public int getIdCartella() {
		return idCartella;
	}
	public void setIdCartella(int idCartella) {
		this.idCartella = idCartella;
	}
	public String getAllegatiPreview() {
		return allegatiPreview;
	}
	public void setAllegatiPreview(String allegatiPreview) {
		this.allegatiPreview = allegatiPreview;
	}
	public String getDestPerPreview() {
		return destPerPreview;
	}
	public void setDestPerPreview(String destPerPreview) {
		this.destPerPreview = destPerPreview;
	}
	public Integer getNumAllegati() {
		return numAllegati;
	}
	public void setNumAllegati(Integer numAllegati) {
		this.numAllegati = numAllegati;
	}
	public List<AllegatoDTO> getElencoFileAllegati() {
		return elencoFileAllegati;
	}
	public void setElencoFileAllegati(List<AllegatoDTO> elencoFileAllegati) {
		this.elencoFileAllegati = elencoFileAllegati;
	}
	public String getAccountPec() {
		return accountPec;
	}
	public void setAccountPec(String accountPec) {
		this.accountPec = accountPec;
	}
	public String getAutoreCompositore() {
		return autoreCompositore;
	}
	public void setAutoreCompositore(String autoreCompositore) {
		this.autoreCompositore = autoreCompositore;
	}
	public String getAutoreMittente() {
		return autoreMittente;
	}
	public void setAutoreMittente(String autoreMittente) {
		this.autoreMittente = autoreMittente;
	}
	public String getAutoreUltimaModifica() {
		return autoreUltimaModifica;
	}
	public void setAutoreUltimaModifica(String autoreUltimaModifica) {
		this.autoreUltimaModifica = autoreUltimaModifica;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getClassifica() {
		return classifica;
	}
	public void setClassifica(String classifica) {
		this.classifica = classifica;
	}
	public String getCodiceAOO() {
		return codiceAOO;
	}
	public void setCodiceAOO(String codiceAOO) {
		this.codiceAOO = codiceAOO;
	}
	
	public Date getDataOraUltimaModifica() {
		return dataOraUltimaModifica;
	}
	public void setDataOraUltimaModifica(Date dataOraUltimaModifica) {
		this.dataOraUltimaModifica = dataOraUltimaModifica;
	}
	public String getDatiSignature() {
		return datiSignature;
	}
	public void setDatiSignature(String datiSignature) {
		this.datiSignature = datiSignature;
	}
	public Date getDeliveredDate() {
		return deliveredDate;
	}
	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}
	public String getDesClassifica() {
		return desClassifica;
	}
	public void setDesClassifica(String desClassifica) {
		this.desClassifica = desClassifica;
	}
	public String getDocumentoPrincipale() {
		return documentoPrincipale;
	}
	public void setDocumentoPrincipale(String documentoPrincipale) {
		this.documentoPrincipale = documentoPrincipale;
	}
	public String getDominoPec() {
		return dominoPec;
	}
	public void setDominoPec(String dominoPec) {
		this.dominoPec = dominoPec;
	}
	public String getFromAddr() {
		return fromAddr;
	}
	public void setFromAddr(String fromAddr) {
		this.fromAddr = fromAddr;
	}
	public String getFromPhrase() {
		return fromPhrase;
	}
	public void setFromPhrase(String FromPhrase) {
		this.fromPhrase = FromPhrase;
	}
	public String getHeaders() {
		return headers;
	}
	public void setHeaders(String headers) {
		this.headers = headers;
	}
	public String getInReplyTo() {
		return inReplyTo;
	}
	public void setInReplyTo(String inReplyTo) {
		this.inReplyTo = inReplyTo;
	}
	public String getInReplyToSegnatura() {
		return inReplyToSegnatura;
	}
	public void setInReplyToSegnatura(String inReplyToSegnatura) {
		this.inReplyToSegnatura = inReplyToSegnatura;
	}
	public String getInReplyToSubject() {
		return inReplyToSubject;
	}
	public void setInReplyToSubject(String inReplyToSubject) {
		this.inReplyToSubject = inReplyToSubject;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getIsReply() {
		return isReply;
	}
	public void setIsReply(String isReply) {
		this.isReply = isReply;
	}
	public String getIsSigned() {
		return isSigned;
	}
	public void setIsSigned(String isSigned) {
		this.isSigned = isSigned;
	}
	public String getIsSignedVer() {
		return isSignedVer;
	}
	public void setIsSignedVer(String isSignedVer) {
		this.isSignedVer = isSignedVer;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public byte[] getMessaggio() {
		return messaggio;
	}
	public void setMessaggio(byte[] messaggio) {
		this.messaggio = messaggio;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getNomeRegione() {
		return nomeRegione;
	}
	public void setNomeRegione(String nomeRegione) {
		this.nomeRegione = nomeRegione;
	}
	public String getNomeSede() {
		return nomeSede;
	}
	public void setNomeSede(String nomeSede) {
		this.nomeSede = nomeSede;
	}
	public String getNomeUfficio() {
		return nomeUfficio;
	}
	public void setNomeUfficio(String nomeUfficio) {
		this.nomeUfficio = nomeUfficio;
	}
	public Date getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
	public String getReplyTo() {
		return replyTo;
	}
	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}
	public String getRiservato() {
		return riservato;
	}
	public void setRiservato(String riservato) {
		this.riservato = riservato;
	}
	public String getSegnatura() {
		return segnatura;
	}
	public void setSegnatura(String segnatura) {
		this.segnatura = segnatura;
	}
	public String getSegnaturaRif() {
		return segnaturaRif;
	}
	public void setSegnaturaRif(String segnaturaRif) {
		this.segnaturaRif = segnaturaRif;
	}
	public String getSegnaturaRoot() {
		return segnaturaRoot;
	}
	public void setSegnaturaRoot(String segnaturaRoot) {
		this.segnaturaRoot = segnaturaRoot;
	}
	public String getSensibile() {
		return sensibile;
	}
	public void setSensibile(String sensibile) {
		this.sensibile = sensibile;
	}
	public String getSignIssuerName() {
		return signIssuerName;
	}
	public void setSignIssuerName(String signIssuerName) {
		this.signIssuerName = signIssuerName;
	}
	public String getSignSubjectName() {
		return signSubjectName;
	}
	public void setSignSubjectName(String signSubjectName) {
		this.signSubjectName = signSubjectName;
	}
	public Date getSignValidFromDate() {
		return signValidFromDate;
	}
	public void setSignValidFromDate(Date signValidFromDate) {
		this.signValidFromDate = signValidFromDate;
	}
	public Date getSignValidToDate() {
		return signValidToDate;
	}
	public void setSignValidToDate(Date signValidToDate) {
		this.signValidToDate = signValidToDate;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	public String getStatoLettura() {
		return statoLettura;
	}
	public void setStatoLettura(String statoLettura) {
		this.statoLettura = statoLettura;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTipologia() {
		return tipologia;
	}
	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}
	public String getTipoMessaggio() {
		return tipoMessaggio;
	}
	public void setTipoMessaggio(String tipoMessaggio) {
		this.tipoMessaggio = tipoMessaggio;
	}
	public String getUiDoc() {
		return uiDoc;
	}
	public void setUiDoc(String uiDoc) {
		this.uiDoc = uiDoc;
	}
	public String getVerso() {
		return verso;
	}
	public void setVerso(String verso) {
		this.verso = verso;
	}
	
	
	public String getTipoRicevuta() {
		return tipoRicevuta;
	}
	public void setTipoRicevuta(String tipoRicevuta) {
		this.tipoRicevuta = tipoRicevuta;
	}
	public String getXTrasporto() {
		return xTrasporto;
	}
	public void setXTrasporto(String trasporto) {
		xTrasporto = trasporto;
	}
	public List<DestinatarioDTO> getElencoDestinatariCc() {
		return elencoDestinatariCc;
	}
	public void setElencoDestinatariCc(List<DestinatarioDTO> elencoDestinatariCc) {
		this.elencoDestinatariCc = elencoDestinatariCc;
	}
	public List<DestinatarioDTO> getElencoDestinatariPer() {
		return elencoDestinatariPer;
	}
	public void setElencoDestinatariPer(List<DestinatarioDTO> elencoDestinatariPer) {
		this.elencoDestinatariPer = elencoDestinatariPer;
	}
	public String getAddrConfermaRicezione() {
		return addrConfermaRicezione;
	}
	public void setAddrConfermaRicezione(String addrConfermaRicezione) {
		this.addrConfermaRicezione = addrConfermaRicezione;
	}
	public String getConfermaRicezione() {
		return confermaRicezione;
	}
	public void setConfermaRicezione(String confermaRicezione) {
		this.confermaRicezione = confermaRicezione;
	}
	
	public String getCodiceAMM() {
		return codiceAMM;
	}
	public void setCodiceAMM(String codiceAMM) {
		this.codiceAMM = codiceAMM;
	}
	public String getCodiceAMMMitt() {
		return codiceAMMMitt;
	}
	public void setCodiceAMMMitt(String codiceAMMMitt) {
		this.codiceAMMMitt = codiceAMMMitt;
	}
	public String getCodiceAOOMitt() {
		return codiceAOOMitt;
	}
	public void setCodiceAOOMitt(String codiceAOOMitt) {
		this.codiceAOOMitt = codiceAOOMitt;
	}
	public String getBodyHtml() {
		return bodyHtml;
	}
	public void setBodyHtml(String bodyHtml) {
		this.bodyHtml = bodyHtml;
	}
	public Integer getIsSPAMorCCN() {
		return isSPAMorCCN;
	}
	public void setIsSPAMorCCN(Integer isSPAMorCCN) {
		this.isSPAMorCCN = isSPAMorCCN;
	}
	public String getDeliveredDateTxt() {
		return deliveredDateTxt;
	}
	public void setDeliveredDateTxt(String deliveredDateTxt) {
		this.deliveredDateTxt = deliveredDateTxt;
	}
	public String getPemlBody() {
		return pemlBody;
	}
	public void setPemlBody(String pemlBody) {
		this.pemlBody = pemlBody;
	}
	public String getPemlDatiSignature() {
		return pemlDatiSignature;
	}
	public void setPemlDatiSignature(String pemlDatiSignature) {
		this.pemlDatiSignature = pemlDatiSignature;
	}
	public String getPemlFrom() {
		return pemlFrom;
	}
	public void setPemlFrom(String pemlFrom) {
		this.pemlFrom = pemlFrom;
	}
	public String getPemlIsSigned() {
		return pemlIsSigned;
	}
	public void setPemlIsSigned(String pemlIsSigned) {
		this.pemlIsSigned = pemlIsSigned;
	}
	public String getPemlIsSignedVer() {
		return pemlIsSignedVer;
	}
	public void setPemlIsSignedVer(String pemlIsSignedVer) {
		this.pemlIsSignedVer = pemlIsSignedVer;
	}
	public Date getPemlPostedDate() {
		return pemlPostedDate;
	}
	public void setPemlPostedDate(Date pemlPostedDate) {
		this.pemlPostedDate = pemlPostedDate;
	}
	public String getPemlSignIssuerName() {
		return pemlSignIssuerName;
	}
	public void setPemlSignIssuerName(String pemlSignIssuerName) {
		this.pemlSignIssuerName = pemlSignIssuerName;
	}
	public String getPemlSignSubjectName() {
		return pemlSignSubjectName;
	}
	public void setPemlSignSubjectName(String pemlSignSubjectName) {
		this.pemlSignSubjectName = pemlSignSubjectName;
	}
	public Date getPemlSignValidFromDate() {
		return pemlSignValidFromDate;
	}
	public void setPemlSignValidFromDate(Date pemlSignValidFromDate) {
		this.pemlSignValidFromDate = pemlSignValidFromDate;
	}
	public Date getPemlSignValidToDate() {
		return pemlSignValidToDate;
	}
	public void setPemlSignValidToDate(Date pemlSignValidToDate) {
		this.pemlSignValidToDate = pemlSignValidToDate;
	}
	public String getPemlSubject() {
		return pemlSubject;
	}
	public void setPemlSubject(String pemlSubject) {
		this.pemlSubject = pemlSubject;
	}
	public String getPemlPostedDateTxt() {
		return pemlPostedDateTxt;
	}
	public void setPemlPostedDateTxt(String pemlPostedDateTxt) {
		this.pemlPostedDateTxt = pemlPostedDateTxt;
	}
	public String getPemlSignValidFromDateTxt() {
		return pemlSignValidFromDateTxt;
	}
	public void setPemlSignValidFromDateTxt(String pemlSignValidFromDateTxt) {
		this.pemlSignValidFromDateTxt = pemlSignValidFromDateTxt;
	}
	public String getPemlSignValidToDateTxt() {
		return pemlSignValidToDateTxt;
	}
	public void setPemlSignValidToDateTxt(String pemlSignValidToDateTxt) {
		this.pemlSignValidToDateTxt = pemlSignValidToDateTxt;
	}
	public String getPemlHeaders() {
		return pemlHeaders;
	}
	public void setPemlHeaders(String pemlHeaders) {
		this.pemlHeaders = pemlHeaders;
	}
	public String getDesMitt() {
		return desMitt;
	}
	public void setDesMitt(String desMitt) {
		this.desMitt = desMitt;
	}
	public String getTipoMitt() {
		return tipoMitt;
	}
	public void setTipoMitt(String tipoMitt) {
		this.tipoMitt = tipoMitt;
	}
	public String getDirettoreAOO() {
		return direttoreAOO;
	}
	public void setDirettoreAOO(String direttoreAOO) {
		this.direttoreAOO = direttoreAOO;
	}
	public String getDirettoreAOOMitt() {
		return direttoreAOOMitt;
	}
	public void setDirettoreAOOMitt(String direttoreAOOMitt) {
		this.direttoreAOOMitt = direttoreAOOMitt;
	}
	public String getSubstato() {
		return substato;
	}
	public void setSubstato(String substato) {
		this.substato = substato;
	}
	public String getNomiAllegati() {
		return nomiAllegati;
	}
	public void setNomiAllegati(String nomiAllegati) {
		this.nomiAllegati = nomiAllegati;
	}
	public String getDataOraUltimaModificaTxt() {
		return dataOraUltimaModificaTxt;
	}
	public void setDataOraUltimaModificaTxt(String dataOraUltimaModificaTxt) {
		this.dataOraUltimaModificaTxt = dataOraUltimaModificaTxt;
	}
	public String getRicevuta() {
		return ricevuta;
	}
	public void setRicevuta(String ricevuta) {
		this.ricevuta = ricevuta;
	}
	public String getNormalMID() {
		return normalMID;
	}
	public void setNormalMID(String normalMID) {
		this.normalMID = normalMID;
	}
	public String getSegnaturaMitt() {
		return segnaturaMitt;
	}
	public void setSegnaturaMitt(String segnaturaMitt) {
		this.segnaturaMitt = segnaturaMitt;
	}
	public SegnaturaDTO getSegnaturaIdentificatore() {
		return segnaturaIdentificatore;
	}
	public void setSegnaturaIdentificatore(SegnaturaDTO segnaturaIdentificatore) {
		this.segnaturaIdentificatore = segnaturaIdentificatore;
	}
	public MessaggioDTO getMessaggioIdentificatore() {
		return messaggioIdentificatore;
	}
	public void setMessaggioIdentificatore(MessaggioDTO messaggioIdentificatore) {
		this.messaggioIdentificatore = messaggioIdentificatore;
	}
	public String getPostaCertEMLBody() {
		return postaCertEMLBody;
	}
	public void setPostaCertEMLBody(String postaCertEMLBody) {
		this.postaCertEMLBody = postaCertEMLBody;
	}
	public String getPostaCertEMLSubject() {
		return postaCertEMLSubject;
	}
	public void setPostaCertEMLSubject(String postaCertEMLSubject) {
		this.postaCertEMLSubject = postaCertEMLSubject;
	}
	public boolean isInteroperabile() {
		return isInteroperabile;
	}
	public void setInteroperabile(boolean isInteroperabile) {
		this.isInteroperabile = isInteroperabile;
	}
	public void setIdPrenotazione(String idPrenotazione) {
		this.idPrenotazione = idPrenotazione;
	}
	public String getIdPrenotazione() {
		return idPrenotazione;
	}
	public void setCanale(String canale) {
		this.canale = canale;
	}
	public String getCanale() {
		return canale;
	}
	
	

}
