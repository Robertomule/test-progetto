package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;
import java.util.List;

public class CartellaDTO implements Serializable{

	
	private static final long serialVersionUID = 6451236885004513196L;
	private Integer idCartella;
	private Integer idPadre;
	private boolean valida;
	private String descrizione;
	private String account;
	private String FullPathFolder;
	
	public String getFullPathFolder() {
		return FullPathFolder;
	}
	public void setFullPathFolder(String fullPathFolder) {
		FullPathFolder = fullPathFolder;
	}
	private List<CartellaDTO> listaCartelleFiglie;
	
	
	
	public Integer getIdCartella() {
		return idCartella;
	}
	public void setIdCartella(Integer idCartella) {
		this.idCartella = idCartella;
	}
	public Integer getIdPadre() {
		return idPadre;
	}
	public void setIdPadre(Integer idPadre) {
		this.idPadre = idPadre;
	}
	public boolean isValida() {
		return valida;
	}
	public void setValida(boolean valida) {
		this.valida = valida;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public List<CartellaDTO> getListaCartelleFiglie() {
		return listaCartelleFiglie;
	}
	public void setListaCartelleFiglie(List<CartellaDTO> listaCartelleFiglie) {
		this.listaCartelleFiglie = listaCartelleFiglie;
	}
	
	
}
