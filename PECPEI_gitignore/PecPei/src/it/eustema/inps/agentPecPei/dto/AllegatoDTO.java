package it.eustema.inps.agentPecPei.dto;

import java.io.InputStream;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class AllegatoDTO implements Serializable, DTO {

	private static final long serialVersionUID = -3807106920712331448L;
	private String messageID;
	private String fileName;
	private byte[] fileData;
	private String contentType;
	private String tipoAllegato;
	private InputStream inputStream;
	private int fileSize;
	private String testoAllegato;
	private Timestamp postedDate;
	private String meseAnno;
	private String idDocStore;
	
	
	public String getMeseAnno() {
		return meseAnno;
	}
	public void setMeseAnno(String meseAnno) {
		this.meseAnno = meseAnno;
	}
	public Timestamp getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(Timestamp postedDate) {
		this.postedDate = postedDate;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getFileData() {
		return fileData;
	}
	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getTipoAllegato() {
		return tipoAllegato;
	}
	public void setTipoAllegato(String tipoAllegato) {
		this.tipoAllegato = tipoAllegato;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public int getFileSize() {
		return fileSize;
	}
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}
	public String getTestoAllegato() {
		return testoAllegato;
	}
	public void setTestoAllegato(String testoAllegato) {
		this.testoAllegato = testoAllegato;
	}
	public String getMessageID() {
		return messageID;
	}
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}
	public void setIdDocStore(String idDocStore) {
		this.idDocStore = idDocStore;
	}
	public String getIdDocStore() {
		return idDocStore;
	}
	
	
}
