package it.eustema.inps.agentPecPei.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class LogIndiciPaDTO implements Serializable, DTO {
	
	private static final long serialVersionUID = -5980524146275159445L;
	
	private int id;
	private Timestamp dataInizio;
	private Timestamp dataFine;
	private int totaleIndLav;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getDataInizio() {
		return dataInizio;
	}
	public void setDataInizio(Timestamp dataInizio) {
		this.dataInizio = dataInizio;
	}
	public Timestamp getDataFine() {
		return dataFine;
	}
	public void setDataFine(Timestamp dataFine) {
		this.dataFine = dataFine;
	}
	public int getTotaleIndLav() {
		return totaleIndLav;
	}
	public void setTotaleIndLav(int totaleIndLav) {
		this.totaleIndLav = totaleIndLav;
	}
}


