package it.eustema.inps.agentPecPei.dto;

import java.sql.Timestamp;

/**
 * 
 * @author rstabile
 *
 */

public class EsecuzioniIndiciPaNew {

	
	private int id;
	private Timestamp inizioEsecuzione;
	private byte[] ultimoFileLavorato;
	private String errore;
	private String tempoEsecuzione;
	private Timestamp fineEsecuzione;
	private int totLavorati;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getInizioEsecuzione() {
		return inizioEsecuzione;
	}
	public void setInizioEsecuzione(Timestamp inizioEsecuzione) {
		this.inizioEsecuzione = inizioEsecuzione;
	}
	public byte[] getUltimoFileLavorato() {
		return ultimoFileLavorato;
	}
	public void setUltimoFileLavorato(byte[] ultimoFileLavorato) {
		this.ultimoFileLavorato = ultimoFileLavorato;
	}
	public String getTempoEsecuzione() {
		return tempoEsecuzione;
	}
	public void setTempoEsecuzione(String tempoEsecuzione) {
		this.tempoEsecuzione = tempoEsecuzione;
	}
	public Timestamp getFineEsecuzione() {
		return fineEsecuzione;
	}
	public void setFineEsecuzione(Timestamp fineEsecuzione) {
		this.fineEsecuzione = fineEsecuzione;
	}
	public int getTotLavorati() {
		return totLavorati;
	}
	public void setTotLavorati(int totLavorati) {
		this.totLavorati = totLavorati;
	}
	public void setErrore(String errore) {
		this.errore = errore;
	}
	public String getErrore() {
		return errore;
	}
	
}


