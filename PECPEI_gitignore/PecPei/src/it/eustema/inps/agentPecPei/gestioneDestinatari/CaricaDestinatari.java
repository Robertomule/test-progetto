package it.eustema.inps.agentPecPei.gestioneDestinatari;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.xml.bind.JAXBContext;
import java.util.Date;
import org.apache.log4j.Logger;

import generated.Destinatari;
import generated.Postacert;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.exception.BusinessException;

public class CaricaDestinatari implements Command{
	private GestDatiCert datiCert;
	static Logger log = Logger.getLogger(CaricaDestinatariCommand.class);
	
	public CaricaDestinatari( GestDatiCert datiCert ){
		this.datiCert = datiCert;
	}

	@Override
	public Object execute() throws BusinessException {
		
		Postacert postacert = null;
		int insertEffettuate = 0;
		String[] valori = new String[5];
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		MessaggiDAO messaggioDao = new MessaggiDAO();

		try{
			JAXBContext contextIntestazione = JAXBContext.newInstance(Postacert.class);
			ByteArrayInputStream bis = new ByteArrayInputStream(datiCert.getAllegato());
			Reader reader = new InputStreamReader(bis, "ISO-8859-1");
			postacert = (Postacert) contextIntestazione.createUnmarshaller().unmarshal(reader);
			List<Destinatari> destinatari = postacert.getIntestazione().getDestinatari();
			
			for(int x=0;x<destinatari.size();x++){
				Date date = new Date();
				valori[0] = datiCert.getMessageId();
				valori[1] = datiCert.getPosteddate();
				valori[2] = sdf.format(date);
				valori[3] = destinatari.get(x).getContent();
				valori[4] = destinatari.get(x).getTipo();
				
				messaggioDao.insertDestinatario(valori);
				insertEffettuate++;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "CaricaDestinatari";
	}

}
