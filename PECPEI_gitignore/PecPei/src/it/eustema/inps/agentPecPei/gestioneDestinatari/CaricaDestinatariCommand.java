package it.eustema.inps.agentPecPei.gestioneDestinatari;

import java.util.concurrent.Callable;
import org.apache.log4j.Logger;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;

/**
 * 
 * @author r.stabile
 *
 */
public class CaricaDestinatariCommand implements Command, Callable<Object>{

	private GestDatiCert datiCert;
	
	public CaricaDestinatariCommand( GestDatiCert datiCert ){
		this.datiCert = datiCert;
	}
	
	@Override
	public Object execute() throws BusinessException {
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		
		try{
			
			//inserisco nel DB i dati
			executor.setCommand( new CaricaDestinatari( datiCert ) );
			executor.executeCommandAndLog();

		}catch(Exception e){
			e.printStackTrace();
		}
		
		return 0;
	}	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "CaricaDestinatariCommand";
	}

	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}
}
