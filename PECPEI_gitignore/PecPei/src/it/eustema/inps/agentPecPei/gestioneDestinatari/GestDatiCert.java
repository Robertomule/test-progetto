package it.eustema.inps.agentPecPei.gestioneDestinatari;

/**
 * 
 * @author r.stabile
 *
 */

public class GestDatiCert{
	
	String messageId ="";
	String posteddate = "";
	String nomeallegato = "";
	byte[] allegato = null;
	
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String idmessageId) {
		this.messageId = idmessageId;
	}
	public String getPosteddate() {
		return posteddate;
	}
	public void setPosteddate(String posteddate) {
		this.posteddate = posteddate;
	}
	public String getNomeallegato() {
		return nomeallegato;
	}
	public void setNomeallegato(String nomeallegato) {
		this.nomeallegato = nomeallegato;
	}
	public byte[] getAllegato() {
		return allegato;
	}
	public void setAllegato(byte[] allegato) {
		this.allegato = allegato;
	}

	
}
