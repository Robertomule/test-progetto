package it.eustema.inps.agentPecPei.notifichePciSGD;

import wsPciClient.org.tempuri.WsPCItoSGDSoapProxy;

/**
 * 
 * @author rstabile
 *
 */

public class ProtocolloPciToSgdSingleton {
	
	private static ProtocolloPciToSgdSingleton instance = null;
	public static WsPCItoSGDSoapProxy proxy;
	
	private ProtocolloPciToSgdSingleton(){}
	 
	public static synchronized ProtocolloPciToSgdSingleton getInstance( ) {
	 
		if(instance == null){
			instance = new ProtocolloPciToSgdSingleton();
			instance.init();
			return instance;
		}
		
      return instance;
	}
	 
	 private void init(){
		 try{
			 proxy = new WsPCItoSGDSoapProxy();
			 
		 }catch(Exception e){
			 
		 }
	 }
}