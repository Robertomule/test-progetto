package it.eustema.inps.agentPecPei.pei;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommand;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioUscitaCommand;
import it.eustema.inps.agentPecPei.business.pec.mail.InviaPecCommand;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.business.util.ValorizzaSegnaturaCommand;
import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.agentPecPei.manager.GestionePEI_BusinessManager;
import it.eustema.inps.agentPecPei.util.Constants;

public class RichiestaProtocolloMessaggiPEI implements Command, Callable<Object> {

	/**
	 * Classe Command che si occupa di Inviare i messaggi PEC in uscita
	 * @author ALEANZI
	 *
	 */
	private MessaggioDTO messaggioDaProtocollare;
	private MessaggioDTO messaggioInUscitaPEI;
	
	public RichiestaProtocolloMessaggiPEI( MessaggioDTO messaggioInUscitaPEI, MessaggioDTO messaggioDaProtocollare){
		this.messaggioDaProtocollare = messaggioDaProtocollare;
		this.messaggioInUscitaPEI = messaggioInUscitaPEI;
	}
	
	@Override
	public Object execute() throws BusinessException {
		
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		
		Map<String,String> msgIdSegnaturaMap = new ConcurrentHashMap<String,String>();

		MessaggiDAO messDao = new MessaggiDAO();

//		for(MessaggioDTO messaggioDaProtocollare: messaggiDaProtocollare){						
			try{
				SegnaturaDTO segnatura = new SegnaturaDTO();
				
				String segnaturaPei = messDao.leggiSegnaturaPEI(messaggioDaProtocollare.getMessageId(),  messaggioDaProtocollare.getAccountPec(), messaggioDaProtocollare.getCodiceAOO());
				
				if(segnaturaPei!=null&&!segnaturaPei.equalsIgnoreCase("")){
					segnatura.setSegnatura(segnaturaPei);
					segnatura.setStatus(SegnaturaDTO.OK);
				}else
					segnatura = GestionePEI_BusinessManager.richiestaProtocolloMessaggio(messaggioDaProtocollare);
				
				if ( segnatura.isOK() ){
					executor.setCommand( new ValorizzaSegnaturaCommand( messaggioDaProtocollare, segnatura.getSegnatura() ) );
					executor.executeCommand();
				//Dopo che la protocollazione ha dato esito positivo, viene impostato lo stato
				// a RI: Messaggio Ricevuto
					if(segnaturaPei==null){
						messaggioDaProtocollare.setStato(Constants.STATO_MSG_RICEVUTO);
						messaggioDaProtocollare.setPostedDate( new Date() );
						messaggioDaProtocollare.setDeliveredDate( messaggioDaProtocollare.getPostedDate() );
						messaggioDaProtocollare.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
					}
				}else{
					messaggioDaProtocollare.setStato(Constants.STATO_MSG_DA_PROTOCOLLARE);
//					messaggioDaProtocollare.setPostedDate( new Date() );
					messaggioDaProtocollare.setDeliveredDate( messaggioDaProtocollare.getPostedDate() );
//					messaggioDaProtocollare.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
				}	
				if(segnaturaPei==null){
					messDao.aggiornaMsgInviatiRicevuti(messaggioInUscitaPEI, messaggioDaProtocollare, false);
				}
//			msgIdSegnaturaMap.put(messaggioDaProtocollare.getMessageId(),segnatura.getSegnatura() );	
//			} else {
			}catch(DAOException e){
				throw new BusinessException(-70001, "Errore durante il processo di protocollazione in entrata del messaggio: " + messaggioDaProtocollare.getMessageId() );
			}

//		}
//		messDao.aggiornaMsgInviatiRicevuti(messaggioInUscitaPEI, null, true);
		return msgIdSegnaturaMap;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "InviaMessaggiPECInUscita";
	}

	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return execute();
	}
}
