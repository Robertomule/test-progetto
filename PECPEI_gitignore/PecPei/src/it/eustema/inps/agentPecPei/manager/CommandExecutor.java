package it.eustema.inps.agentPecPei.manager;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.DTO;
import it.eustema.inps.agentPecPei.dto.InputDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.OutputDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.inps.agentPec.verificaPec.dto.MessaggioPecVerifica;

/**
 * Interfaccia per classi che si occupano di eseguire attivit� definite in oggetti della 
 * classe Command.
 * @author ALEANZI
 *
 */
public interface CommandExecutor {
	
	public void setCommand( Command command );
	public Object executeCommand()  throws BusinessException;
	public Object executeCommandAndLog()  throws BusinessException;
	public Object executeCommandAndLogNew(MessaggioDTO msg) throws BusinessException;
	public Object executeCommandAndLogVerificaNew(AccountPecDTO msg) throws BusinessException;
	public Object executeCommandAndLogVerificaNew(MessaggioPecVerifica msg) throws BusinessException;
}
