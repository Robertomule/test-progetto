package it.eustema.inps.agentPecPei.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.log4j.Logger;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.DBConnectionManager;
import it.inps.agentPec.verificaPec.dto.MessaggioPecVerifica;

/**
 * Implementazione generica interfaccia CommandExecutor.
 * @author ALEANZI
 *
 */
public class CommandExecutorImpl implements CommandExecutor {
	protected static Logger log = Logger.getLogger(CommandExecutorImpl.class);
	protected Command command;
	
	@Override
	public Object executeCommand() throws BusinessException {
		// TODO Auto-generated method stub
		log.debug("Inizio esecuzione COMMAND : [" + command.getClass().getCanonicalName() +"]" );
		Object out = null;
		try{
			out = command.execute(); 
		} catch ( BusinessException e ){
			StringBuilder sb = new StringBuilder("Errore durante esecuzione esecuzione COMMAND : [");
			sb.append( command.getClass().getCanonicalName() );
			sb.append("]");
			sb.append(" Dettaglio Errore: Codice: ");
			sb.append( e.getErrorCode() );
			sb.append(" Descrizione: ");
			sb.append( e.getMessage() );
			log.error( sb.toString() );
			
			if ( e instanceof NotifyMailBusinessException ){
				new SendMailCommand( (NotifyMailBusinessException)e ).execute();
			}
			
			throw e;
		}
		 
		log.debug("Fine esecuzione COMMAND : [" + command.getClass().getCanonicalName() +"]" );
		return out;
	}



	@Override
	public void setCommand(Command command) {
		// TODO Auto-generated method stub
		this.command = command;
	}



	@Override
	public Object executeCommandAndLog() throws BusinessException {
		// TODO Auto-generated method stub
		long startTime = new Date().getTime();
		long endTime;
		Object out = null;
		try{
			out = executeCommand();
			endTime = new Date().getTime();
			log( startTime, endTime );
		} catch ( BusinessException e ){
			endTime = new Date().getTime();
			log( startTime, endTime, e );
			throw e;
		}
		return out;
	}
	
	@Override
	public Object executeCommandAndLogNew(MessaggioDTO msg) throws BusinessException {
		// TODO Auto-generated method stub
		long startTime = new Date().getTime();
		long endTime;
		Object out = null;
		try{
			out = executeCommand();
			endTime = new Date().getTime();
			logNew( startTime, endTime,msg );
		} catch ( BusinessException e ){
			endTime = new Date().getTime();
			logNew( startTime, endTime, e,msg);
			throw e;
		}
		return out;
	}
	
	public Object executeCommandAndLogVerificaNew(AccountPecDTO msg)throws BusinessException {
		// TODO Auto-generated method stub
		long startTime = new Date().getTime();
		long endTime;
		Object out = null;
		try{
			out = executeCommand();
			endTime = new Date().getTime();
			logVerifica( startTime, endTime,msg );
		} catch ( BusinessException e ){
			endTime = new Date().getTime();
			logVerifica( startTime, endTime, e,msg);
			throw e;
		}
		return out;
	}
	
	public Object executeCommandAndLogVerificaNew(MessaggioPecVerifica msg)throws BusinessException {
		// TODO Auto-generated method stub
		long startTime = new Date().getTime();
		long endTime;
		Object out = null;
		try{
			out = executeCommand();
			endTime = new Date().getTime();
			logVerifica( startTime, endTime,msg );
		} catch ( BusinessException e ){
			endTime = new Date().getTime();
			logVerifica( startTime, endTime, e,msg);
			throw e;
		}
		return out;
	}
	
	protected void log( long startTime, long endTime ){
		Logger.getLogger(CommandExecutorImpl.class).debug( "LOG to DB **** Funzione [" +
				command.getName() + "] Tempo esecuzione [" + (endTime-startTime) + "]"
				);
		
		Connection conn;
		try {
			conn = DBConnectionManager.getInstance().getConnection( Constants.DB_PECPEI );
			PreparedStatement pstmt =  conn.prepareStatement("INSERT INTO MONITOR_TRANSAZIONE (xml_request, xml_response, data_richiesta, data_risposta, tempo_transazione, esito, desc_errore, app_utente, operation) values (?,?,?,?,?,?,?,?,?)");
			
			pstmt.setString(1, "");
			pstmt.setString(2, "");
			pstmt.setTimestamp(3, new Timestamp( startTime ));
			pstmt.setTimestamp(4, new Timestamp( endTime ));
			pstmt.setInt(5, (int)(endTime - startTime) );
			pstmt.setShort(6, (short)0);
			pstmt.setString(7, "");
			pstmt.setString(8, "");
			pstmt.setString(9, command.getName() );			
			pstmt.executeUpdate();
			
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	
	
	protected void log( long startTime, long endTime, BusinessException e){
		Logger.getLogger(CommandExecutorImpl.class).debug( "LOG **** Funzione [" +
				command.getName() + "] Tempo esecuzione [" + (endTime-startTime) + "]"
				);
		
		Connection conn;
		try {
			conn = DBConnectionManager.getInstance().getConnection( Constants.DB_PECPEI );
			PreparedStatement pstmt =  conn.prepareStatement("INSERT INTO MONITOR_TRANSAZIONE (xml_request, xml_response, data_richiesta, data_risposta, tempo_transazione, esito, desc_errore, app_utente, operation) values (?,?,?,?,?,?,?,?,?)");
			
			pstmt.setString(1, "");
			pstmt.setString(2, "");
			pstmt.setTimestamp(3, new Timestamp( startTime ));
			pstmt.setTimestamp(4, new Timestamp( endTime ));
			pstmt.setInt(5, (int)(endTime - startTime) );
			pstmt.setShort(6, (short)e.getErrorCode() );
			pstmt.setString(7, e.getMessage() );
			pstmt.setString(8, "");
			pstmt.setString(9, command.getName() );			
			pstmt.executeUpdate();
			
			conn.close();
		} catch (Exception ex) {
			
		}
	}
	
	protected void logNew( long startTime, long endTime,MessaggioDTO msg ){
		Logger.getLogger(CommandExecutorImpl.class).debug( "LOG to DB **** Funzione [" +
				command.getName() + "] Tempo esecuzione [" + (endTime-startTime) + "]"
				);
		
		Connection conn;
		try {
			String commandName = command.getName();
			
			if(commandName.equalsIgnoreCase("InviaPecVerificaCommand"))
				conn = DBConnectionManager.getInstance().getConnection( Constants.DB_VERIFICAPEC );
			else
				conn = DBConnectionManager.getInstance().getConnection( Constants.DB_PECPEI );
			PreparedStatement pstmt =  conn.prepareStatement("INSERT INTO MONITOR_TRANSAZIONE (xml_request, xml_response, data_richiesta, data_risposta, tempo_transazione, esito, desc_errore, app_utente, operation,descrizione,protocollo,n_step,messageId,accountPEC,aoo) "
																+" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			
			
			pstmt.setString(1, "");
			pstmt.setString(2, "");
			pstmt.setTimestamp(3, new Timestamp( startTime ));
			pstmt.setTimestamp(4, new Timestamp( endTime ));
			pstmt.setInt(5, (int)(endTime - startTime) );
			pstmt.setShort(6, (short)0);
			pstmt.setString(7, "");
			pstmt.setString(8, "");
			pstmt.setString(9, commandName);
			
			if(msg.getTipoMessaggio() != null && msg.getTipoMessaggio().equalsIgnoreCase("posta-certificata"))
				pstmt.setString(11, "pec" );
			else if(msg.getTipoMessaggio() != null && msg.getTipoMessaggio().equalsIgnoreCase("messaggio-pei")) 
				pstmt.setString(11, "pei" );
			else
				pstmt.setString(11, "" );

			if(commandName.equalsIgnoreCase("WSPIA Messaggio in Uscita")){
				pstmt.setString(10, "Step di protocollazione del messaggio");
				pstmt.setInt(12, 3);
			}else if(commandName.equalsIgnoreCase("InviaPecCommand")){
				pstmt.setString(10, "Step di invio del messaggio");
				pstmt.setInt(12, 4);
			}else if(commandName.equalsIgnoreCase("InviaPecVerificaCommand")){
				pstmt.setString(10, "Step di invio del messaggio di Verifica Pec");
				pstmt.setInt(12, 4);
			}else{
				pstmt.setString(10, "Step non riconosciuto");
				pstmt.setInt(12, 4);
			}
				
			pstmt.setString(13, msg.getMessageId() );		
			pstmt.setString(14, msg.getAccountPec() );		
			pstmt.setString(15, msg.getCodiceAOO() );		
			
			pstmt.executeUpdate();
			
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}		
		
	}

	
	protected void logNew( long startTime, long endTime, BusinessException e, MessaggioDTO msg){
		Logger.getLogger(CommandExecutorImpl.class).debug( "LOG **** Funzione [" +
				command.getName() + "] Tempo esecuzione [" + (endTime-startTime) + "]"
				);
		
		Connection conn;
		try {
			
			String commandName = command.getName();
			
			if(commandName.equalsIgnoreCase("InviaPecVerificaCommand"))
				conn = DBConnectionManager.getInstance().getConnection( Constants.DB_VERIFICAPEC );
			else
				conn = DBConnectionManager.getInstance().getConnection( Constants.DB_PECPEI );
			PreparedStatement pstmt =  conn.prepareStatement("INSERT INTO MONITOR_TRANSAZIONE (xml_request, xml_response, data_richiesta, data_risposta, tempo_transazione, esito, desc_errore, app_utente, operation,descrizione,protocollo,n_step,messageId,accountPEC,aoo) "
																+" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			pstmt.setString(1, "");
			pstmt.setString(2, "");
			pstmt.setTimestamp(3, new Timestamp( startTime ));
			pstmt.setTimestamp(4, new Timestamp( endTime ));
			pstmt.setInt(5, (int)(endTime - startTime) );
			pstmt.setShort(6, (short)e.getErrorCode() );
			pstmt.setString(7, e.getMessage() );
			pstmt.setString(8, "");
			pstmt.setString(9, commandName);

			if(msg.getTipoMessaggio() != null && msg.getTipoMessaggio().equalsIgnoreCase("posta-certificata"))
				pstmt.setString(11, "pec" );
			else if(msg.getTipoMessaggio() != null && msg.getTipoMessaggio().equalsIgnoreCase("messaggio-pei")) 
				pstmt.setString(11, "pei" );
			else
				pstmt.setString(11, "" );
			
			if(commandName.equalsIgnoreCase("WSPIA Messaggio in Uscita")){
				pstmt.setString(10, "Step di protocollazione del messaggio");
				pstmt.setInt(12, 3);
			}else if(commandName.equalsIgnoreCase("InviaPecCommand")){
				pstmt.setString(10, "Step di invio del messaggio");
				pstmt.setInt(12, 4);
			}else if(commandName.equalsIgnoreCase("InviaPecVerificaCommand")){
				pstmt.setString(10, "Step di invio del messaggio di Verifica Pec");
				pstmt.setInt(12, 4);
			}else{
				pstmt.setString(10, "Step non riconosciuto");
				pstmt.setInt(12, 4);
			}
			
			pstmt.setString(13, msg.getMessageId() );		
			pstmt.setString(14, msg.getAccountPec() );		
			pstmt.setString(15, msg.getCodiceAOO() );	
			
			pstmt.executeUpdate();
			
			conn.close();
		} catch (Exception ex) {
			
		}
	}
	

	protected void logVerifica( long startTime, long endTime,MessaggioPecVerifica msg ){
		Logger.getLogger(CommandExecutorImpl.class).debug( "LOG to DB **** Funzione [" +
				command.getName() + "] Tempo esecuzione [" + (endTime-startTime) + "]"
				);
		
		Connection conn;
		try {
			conn = DBConnectionManager.getInstance().getConnection( Constants.DB_VERIFICAPEC );
			PreparedStatement pstmt =  conn.prepareStatement("INSERT INTO MONITOR_TRANSAZIONE (xml_request, xml_response, data_richiesta, data_risposta, tempo_transazione, esito, desc_errore, app_utente, operation,descrizione,protocollo,n_step,messageId,accountPEC,aoo) "
																+" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			String commandName = command.getName();
			
			pstmt.setString(1, "");
			pstmt.setString(2, "");
			pstmt.setTimestamp(3, new Timestamp( startTime ));
			pstmt.setTimestamp(4, new Timestamp( endTime ));
			pstmt.setInt(5, (int)(endTime - startTime) );
			pstmt.setShort(6, (short)0);
			pstmt.setString(7, "");
			pstmt.setString(8, "");
			pstmt.setString(9, commandName);			
			pstmt.setString(11, "Verifica Pec" );
			
			if(commandName.equalsIgnoreCase("Read Message MAIL")){
				pstmt.setString(10, "Step di lettura caselle");
				pstmt.setInt(12, 3);
			}else if(commandName.equalsIgnoreCase("RiceviMessaggiPECInIngresso")){
				pstmt.setString(10, "Step di Creazione del messaggio in uscita ");
				pstmt.setInt(12, 3);
			}else{
				pstmt.setString(10, "Step non riconosciuto");
				pstmt.setInt(12, 3);
			}
			
			pstmt.setString(13, msg.getMessageId());		
			pstmt.setString(14, msg.getAccountPec());		
			pstmt.setString(15, msg.getCodiceAOO());		
			
			pstmt.executeUpdate();
			
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}		
		
	}
	
	protected void logVerifica( long startTime, long endTime, BusinessException e,MessaggioPecVerifica msg ){
		Logger.getLogger(CommandExecutorImpl.class).debug( "LOG to DB **** Funzione [" +
				command.getName() + "] Tempo esecuzione [" + (endTime-startTime) + "]"
				);
		
		Connection conn;
		try {
			conn = DBConnectionManager.getInstance().getConnection( Constants.DB_VERIFICAPEC );
			PreparedStatement pstmt =  conn.prepareStatement("INSERT INTO MONITOR_TRANSAZIONE (xml_request, xml_response, data_richiesta, data_risposta, tempo_transazione, esito, desc_errore, app_utente, operation,descrizione,protocollo,n_step,messageId,accountPEC,aoo) "
																+" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			String commandName = command.getName();
			
			pstmt.setString(1, "");
			pstmt.setString(2, "");
			pstmt.setTimestamp(3, new Timestamp( startTime ));
			pstmt.setTimestamp(4, new Timestamp( endTime ));
			pstmt.setInt(5, (int)(endTime - startTime) );
			pstmt.setShort(6, (short)e.getErrorCode() );
			pstmt.setString(7, e.getMessage() );
			pstmt.setString(8, "");
			pstmt.setString(9, commandName);		
			pstmt.setString(11, "Verifica Pec" );
			
			if(commandName.equalsIgnoreCase("Read Message MAIL")){
				pstmt.setString(10, "Step di lettura caselle");
				pstmt.setInt(12, 3);
			}else if(commandName.equalsIgnoreCase("RiceviMessaggiPECInIngresso")){
				pstmt.setString(10, "Step di Creazione del messaggio in uscita ");
				pstmt.setInt(12, 3);
			}else{
				pstmt.setString(10, "Step non riconosciuto");
				pstmt.setInt(12, 3);
			}
			
			pstmt.setString(13, msg.getMessageId());		
			pstmt.setString(14, msg.getAccountPec());		
			pstmt.setString(15, msg.getCodiceAOO());				
			
			pstmt.executeUpdate();
			
			conn.close();
		} catch (Exception ex) {
			System.out.println(ex);
		}		
		
	}
	
	protected void logVerifica( long startTime, long endTime,AccountPecDTO msg ){
		Logger.getLogger(CommandExecutorImpl.class).debug( "LOG to DB **** Funzione [" +
				command.getName() + "] Tempo esecuzione [" + (endTime-startTime) + "]"
				);
		
		Connection conn;
		try {
			conn = DBConnectionManager.getInstance().getConnection( Constants.DB_VERIFICAPEC );
			PreparedStatement pstmt =  conn.prepareStatement("INSERT INTO MONITOR_TRANSAZIONE (xml_request, xml_response, data_richiesta, data_risposta, tempo_transazione, esito, desc_errore, app_utente, operation,descrizione,protocollo,n_step,messageId,accountPEC,aoo) "
																+" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			String commandName = command.getName();
			
			pstmt.setString(1, "");
			pstmt.setString(2, "");
			pstmt.setTimestamp(3, new Timestamp( startTime ));
			pstmt.setTimestamp(4, new Timestamp( endTime ));
			pstmt.setInt(5, (int)(endTime - startTime) );
			pstmt.setShort(6, (short)0);
			pstmt.setString(7, "");
			pstmt.setString(8, "");
			pstmt.setString(9, commandName);			
			pstmt.setString(11, "Verifica Pec" );
			
			if(commandName.equalsIgnoreCase("Read Message MAIL")){
				pstmt.setString(10, "Step di lettura caselle");
				pstmt.setInt(12, 3);
			}else if(commandName.equalsIgnoreCase("RiceviMessaggiPECInIngresso")){
				pstmt.setString(10, "Step di Creazione del messaggio in uscita ");
				pstmt.setInt(12, 3);
			}else{
				pstmt.setString(10, "Step non riconosciuto");
				pstmt.setInt(12, 3);
			}
			
			pstmt.setString(13, "");		
			pstmt.setString(14, msg.getUserName());		
			pstmt.setString(15, msg.getCodAOO());		
			
			pstmt.executeUpdate();
			
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}		
		
	}
	
	protected void logVerifica( long startTime, long endTime, BusinessException e,AccountPecDTO msg ){
		Logger.getLogger(CommandExecutorImpl.class).debug( "LOG to DB **** Funzione [" +
				command.getName() + "] Tempo esecuzione [" + (endTime-startTime) + "]"
				);
		
		Connection conn;
		try {
			conn = DBConnectionManager.getInstance().getConnection( Constants.DB_VERIFICAPEC );
			PreparedStatement pstmt =  conn.prepareStatement("INSERT INTO MONITOR_TRANSAZIONE (xml_request, xml_response, data_richiesta, data_risposta, tempo_transazione, esito, desc_errore, app_utente, operation,descrizione,protocollo,n_step,messageId,accountPEC,aoo) "
																+" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			String commandName = command.getName();
			
			pstmt.setString(1, "");
			pstmt.setString(2, "");
			pstmt.setTimestamp(3, new Timestamp( startTime ));
			pstmt.setTimestamp(4, new Timestamp( endTime ));
			pstmt.setInt(5, (int)(endTime - startTime) );
			pstmt.setShort(6, (short)e.getErrorCode() );
			pstmt.setString(7, e.getMessage() );
			pstmt.setString(8, "");
			pstmt.setString(9, commandName);		
			pstmt.setString(11, "Verifica Pec" );
			
			if(commandName.equalsIgnoreCase("Read Message MAIL")){
				pstmt.setString(10, "Step di lettura caselle");
				pstmt.setInt(12, 3);
			}else if(commandName.equalsIgnoreCase("RiceviMessaggiPECInIngresso")){
				pstmt.setString(10, "Step di Creazione del messaggio in uscita ");
				pstmt.setInt(12, 3);
			}else{
				pstmt.setString(10, "Step non riconosciuto");
				pstmt.setInt(12, 3);
			}
			
			pstmt.setString(13, "");		
			pstmt.setString(14, msg.getUserName());		
			pstmt.setString(15, msg.getCodAOO());		
			
			pstmt.executeUpdate();
			
			conn.close();
		} catch (Exception ex) {
			System.out.println(ex);
		}		
		
	}
}
