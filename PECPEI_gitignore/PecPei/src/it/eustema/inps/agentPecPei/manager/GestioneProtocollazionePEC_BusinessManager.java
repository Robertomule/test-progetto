package it.eustema.inps.agentPecPei.manager;
import java.util.List;

import org.apache.log4j.Logger;

import it.eustema.inps.agentPecPei.business.pec.mail.ReadAndElabMessaggiPecDaProtocollare;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.exception.PecBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;

public class GestioneProtocollazionePEC_BusinessManager {

private static Logger log = Logger.getLogger(GestioneProtocollazionePEC_BusinessManager.class);
private int idBatch = -1;

public GestioneProtocollazionePEC_BusinessManager(int batch){
	this.idBatch = batch;
}

public String elabora() throws PecBusinessException {
	
	log.info("GestioneProtocollazionePEC_BusinessManager :: elabora");
	String exitCode = "";
	
	try {
		List<String> codAOOMessaggi = new MessaggiDAO().selectAllAOO(idBatch);
		List<String> codAOOAccounts = new AccountPecDAO().selectAllAOO(idBatch);
		
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, codAOOMessaggi.size() + codAOOAccounts.size() );
		
//		for ( String codAOO: codAOOMessaggi ){
//				executor.setCommand( new InviaMessaggiPECInUscitaCommand( codAOO ) );
//				executor.executeCommand();
//		}
//		for ( String codAOO: codAOOAccounts ){
//			executor.setCommand( new RiceviMessaggiPECInIngressoCommand( codAOO ) );
//			executor.executeCommand();	
//		}
		
		for ( String codAOO: codAOOMessaggi ){
			executor.setCommand( new ReadAndElabMessaggiPecDaProtocollare( codAOO ) );
			executor.executeCommand();
		}
		
		List<Object> out = ((CommandExecutorMultiThread)executor).waitResult();
		
	} catch (Exception e) {
		e.printStackTrace();
		throw new PecBusinessException(e.getMessage());			
	}
	
	log.info("GestioneProtocollazionePEC_BusinessManager :: fine");
		
		
		return exitCode;
	}
}