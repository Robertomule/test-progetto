package it.eustema.inps.agentPecPei.manager;

import it.eustema.inps.agentPecPei.business.pec.InviaMessaggiPECInUscitaCommand;
import it.eustema.inps.agentPecPei.business.pec.RiceviMessaggiPECInIngressoCommand;
import it.eustema.inps.agentPecPei.business.pec.mail.ReadAndElabMessaggiPecDaProtocollare;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dao.AssoAccountsBatchDAO;
import it.eustema.inps.agentPecPei.dao.DestinatariTestDao;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dao.SedeDAO;
import it.eustema.inps.agentPecPei.exception.PecBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;
import it.eustema.inps.utility.ConfigProp;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class GestionePEC_BusinessManager {
	private static Logger log = Logger.getLogger(GestionePEC_BusinessManager.class);
	private int idBatch;
	
	public GestionePEC_BusinessManager(int batch){
		this.idBatch = batch;
	}
	
	public String elabora() throws PecBusinessException {

		System.out.println("GestionePEC_BusinessManager :: elabora() :: batch "+ idBatch);
		log.info("GestionePEC_BusinessManager :: elabora batch "+ idBatch);
		String exitCode = "";

		try{
			//Carico Gli Indirizzi Di Test
			DestinatariTestDao testDao = new DestinatariTestDao();
			ConfigProp.indirizziPerTest = testDao.getDestinatariTest();
			
			//Carico la lista delle aoo abilitate al nuovo protocollo
			SedeDAO sedi = new SedeDAO();
			ConfigProp.listaAooAbilitate = sedi.selectSediNewProto();				
		}catch(Exception e){
			log.error("Errore in caricamento lista destinatari test: "+e.getMessage());
			ConfigProp.indirizziPerTest = new ArrayList<String>(); //Creo una lista vuota
		}
		
		try {
			System.out.println("Mi trovo dentro al try-catch e controllo se il batch e' attivo oppure no.");
			//Verifico prima se il batch � attivo, altrimenti esco
			AssoAccountsBatchDAO batchDao = new AssoAccountsBatchDAO();
			if(!batchDao.isBatchAttivo(idBatch)){
				System.out.println("Il batch con id "+ idBatch + " non attivo oppure non presente. Esco!");
				log.info("Il batch con id "+ idBatch + " non attivo oppure non presente. Esco!");
				return exitCode; 
			}
			
			System.out.println("GestionePEC_BusinessManager :: elabora() :: Batch con id "+ idBatch + " attivo, inizio esecuzione!");
			log.info("Batch con id "+ idBatch + " attivo, inizio esecuzione!");
			
			List<String> codAOOMessaggi = new MessaggiDAO().selectAllAOO(idBatch);
			List<String> codAOOAccounts = new AccountPecDAO().selectAllAOO(idBatch);

			System.out.println("GestionePEC_BusinessManager :: getExecutor :: Entro nel CommandExecutorManager");
			CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, codAOOMessaggi.size() + codAOOAccounts.size() );
			
			for ( String codAOO: codAOOMessaggi ){
					System.out.println("GestionePEC_BusinessManager :: getExecutor :: Scorro la lista di tutti i Messaggi CODAOO");
					System.out.println("Per ogni CODAOO creo un oggetto 'InviaMessaggiPECInUscitaCommand'");
					executor.setCommand( new InviaMessaggiPECInUscitaCommand( codAOO ) );
					executor.executeCommand();
			}
		
			for ( String codAOO: codAOOAccounts ){
				System.out.println("GestionePEC_BusinessManager :: getExecutor :: Scorro la lista di tutti gli Account CODAOO");
				System.out.println("Per ogni CODAOO creo un oggetto 'RiceviMessaggiPECInIngressoCommand'");
				executor.setCommand( new RiceviMessaggiPECInIngressoCommand( codAOO ) );
				executor.executeCommand();	
			}
			
			List<Object> out = ((CommandExecutorMultiThread)executor).waitResult();
			log.info("Termine invio e ricezione messaggi: "+out.get(0));
			for ( String codAOO: codAOOMessaggi ){
				executor.setCommand( new ReadAndElabMessaggiPecDaProtocollare( codAOO ) );
				executor.executeCommand();
			}

		/*	
			for ( String codAOO: codAOOAccounts ){
				executor.setCommand( new RiceviMessaggiPECInIngressoCommand( codAOO ) );
				executor.executeCommand();	
			}
			
		*/
			out = ((CommandExecutorMultiThread)executor).waitResult();
			log.info("Termine protocollazione messaggi: "+out.get(0));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new PecBusinessException(e.getMessage());			
		}
		
		log.info("GestionePEC_BusinessManager :: fine");
		
		
		return exitCode;
	}
}
