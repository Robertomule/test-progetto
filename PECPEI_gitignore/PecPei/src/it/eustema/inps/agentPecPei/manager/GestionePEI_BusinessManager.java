package it.eustema.inps.agentPecPei.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommand;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioUscitaCommand;
import it.eustema.inps.agentPecPei.business.util.ValorizzaSegnaturaCommand;
import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dao.SedeDAO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SedeDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.PeiBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;
import it.eustema.inps.agentPecPei.pei.RichiestaProtocolloMessaggiPEI;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;

public class GestionePEI_BusinessManager {

	int commitPoint = ConfigProp.intervalloDiCommit;
	private static Log log = LogFactory.getLog("it.eustema.inps.agentPecPei.manager.GestionePEI_BusinessManager");
	
	public String elabora() throws PeiBusinessException {

		System.out.println("Sono nel metodo 'elabora' del Gestione_PEI_BusinessManager");

		log.info("elabora START");
		String exitCode = "";
		try {
			System.out.println("Invio / Ricevo PEI");
			inviaRiceviPEI();
		} catch (Exception e) {
			throw new PeiBusinessException(e.getMessage());			
		}
		
		return exitCode;
	}
	
	
	/**
	 * Metodo che implementa la funzionalita' PECPEI_F002 Invio-Ricezione PEI.
	 * Il flusso e' descritto al � 5.1.2 del doc di analisi dell'Agent.
	 * @throws Exception se si verifica un errore bloccante durante l'elaborazione
	 */
	private void inviaRiceviPEI() throws Exception{
					
		log.info("inviaRiceviPEI START");
		MessaggiDAO messaggiDAO = new MessaggiDAO();
		List<MessaggioDTO> listaMessaggiInUscitaPEI = null;
		
		// contenitore formato dai messaggi in uscita fin qui elaborati (come chiavi)
		// e dalla lista dei corrispondenti messaggi in entrata
		Map<MessaggioDTO,List<MessaggioDTO>> msgMap = new ConcurrentHashMap<MessaggioDTO,List<MessaggioDTO>>();			
		
		
		int counter = 0;
		/*
		 *  SELEZIONE ELENCO MESSAGGI PEI DA INVIARE
		 */		
		try{
			System.out.println("Seleziono i messaggi da mandare per le PEI");
			listaMessaggiInUscitaPEI = messaggiDAO.selezionaMessaggiInUscitaPEI();
		}
		catch(DAOException daoEx){
			log.error("inviaRiceviPEI: " + daoEx.getMessage());
			//System.err.println("GestionePEI_BusinessManager :: inviaRiceviPEI: " + daoEx.getMessage());			
			throw daoEx;
		}
		
		for(MessaggioDTO messaggioInUscitaPEI: listaMessaggiInUscitaPEI){
			/*
			 *  ELABORAZIONE SINGOLO MESSAGGIO PEI IN USCITA
			 */
//			System.out.println("messaggio da lavorare : "+messaggioInUscitaPEI.getMessageId());
			counter ++;
			log.debug("inviaRiceviPEI :: counter=" + counter);
			SegnaturaDTO segnaturaMessaggio = new SegnaturaDTO();
			List<MessaggioDTO> listaMessaggiInEntrataPEI = null;
			messaggioInUscitaPEI.setElencoFileAllegati( new AllegatiDAO().selectAllegatiWS( messaggioInUscitaPEI.getMessageId() ) );
			// 	RICHIESTA AL SISTEMA DI PROTOCOLLO			
			try {
				if(messaggioInUscitaPEI.getSegnatura()==null)
					segnaturaMessaggio = richiestaProtocolloMessaggio(messaggioInUscitaPEI);
				else
					segnaturaMessaggio.setStatus(0);
				
				if( !segnaturaMessaggio.isOK() ){
					notificaErroreByMail("Errore durante il processo di protocollazione del messaggio PEI: [" + messaggioInUscitaPEI.getMessageId() + "] Dettaglio Errore Codice: " + segnaturaMessaggio.getCodiceErrore() + " Descrizione: " + segnaturaMessaggio.getDescErrore() );
					System.out.println("Errore durante il processo di protocollazione del messaggio PEI: [" + messaggioInUscitaPEI.getMessageId() + "] Dettaglio Errore Codice: " + segnaturaMessaggio.getCodiceErrore() + " Descrizione: " + segnaturaMessaggio.getDescErrore());
					continue; // errore non bloccante passa elaborazione del msg successivo					
				}
				else{
					if(messaggioInUscitaPEI.getSegnatura()==null){
						CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
						executor.setCommand( new ValorizzaSegnaturaCommand( messaggioInUscitaPEI, segnaturaMessaggio.getSegnatura() ) );
						executor.executeCommand();
					}
					messaggioInUscitaPEI.setStato( Constants.STATO_MSG_DAINVIARE );
					messaggiDAO.aggiornaMsgUscenteInviato(null, messaggioInUscitaPEI);
					// segnatura ottenuta dal WS: impostarla @ TODO vedere come impostare la segnatura ottenuta
					messaggioInUscitaPEI.setStato( Constants.STATO_MSG_INVIATO );
					messaggioInUscitaPEI.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
					
				}
			} catch (Exception e) {
				// errore durante la protocollazione del msg in uscita corrente
				notificaErroreByMail("Errore durante il processo di protocollazione del messaggio PEI: [" + messaggioInUscitaPEI.getMessageId() + "] Dettaglio Errore: " + e.getMessage());
				continue; // errore non bloccante passa elaborazione msg successivo
			}
			
			// CREAZIONE MESSAGGI IN ENTRATA - 1 PER OGNI DESTINATARIO DEL MSG IN USCITA
			try {
				listaMessaggiInEntrataPEI = creaMessaggiInEntrataPEI(messaggioInUscitaPEI, segnaturaMessaggio);
			} catch (Exception e) {
				// errore durante la creazione dei msg in entrata relativi al msg in uscita corrente
				e.printStackTrace();
				log.error("inviaRiceviPEI: " + e.getMessage());
				//System.err.println("GestionePEI_BusinessManager :: inviaRiceviPEI: " + e.getMessage());
				notificaErroreByMail("Errore durante la generazione dei messaggi in entrata relativi a: " + messaggioInUscitaPEI.getMessageId());
				//throw e;
				continue; // errore non bloccante passa elaborazione msg successivo
			}
			
			// 	RICHIESTA AL SISTEMA DI PROTOCOLLO PER I MSG IN ENTRATA
			Map<String,String> messageIdSegnaturaMap;
			try {
				messageIdSegnaturaMap = richiestaProtocolloMessaggi(messaggioInUscitaPEI, listaMessaggiInEntrataPEI);
				// @ TODO impostare le segnature ai messaggi
				
			} catch (Exception e) {
				// errore durante la protocollazione dei msg in entrata
				e.printStackTrace();
				log.error("inviaRiceviPEI: " + e.getMessage());
				//System.err.println("GestionePEI_BusinessManager :: inviaRiceviPEI: " + e.getMessage());
				notificaErroreByMail(e.getMessage());
				continue; // errore non bloccante il msg non e' inviato passa elaborazione del msg in uscita successivo
			}
			
			/*
			log.debug("inviaRiceviPEI :: counter=" + counter);
			// @ TODO GESTIRE I MESSAGGI RIMANENTI DELL'ULTIMO GRUPPO!!!!
			int numTOTMsgUscita = listaMessaggiInUscitaPEI.size();
			BigDecimal bd = new BigDecimal(numTOTMsgUscita / commitPoint);
			
			if(counter < commitPoint){
				msgMap.put(messaggioInUscitaPEI, listaMessaggiInEntrataPEI);
			}else{
				// caso counter=commitPoint il counter ha raggiunto il valore del commitPoint
				// aggiunge alla mappa il msg in uscita con i rispettivi in entrata
				msgMap.put(messaggioInUscitaPEI, listaMessaggiInEntrataPEI);
				// chiama l'aggiornamento del DB														 
				try{					
					int ret = messaggiDAO.aggiornaMsgInviatiRicevuti(msgMap);
				}
				catch(DAOException daoEx){
					//System.err.println("GestionePEI_BusinessManager :: inviaRiceviPEI: " + daoEx.getMessage());
					log.error("inviaRiceviPEI: " + daoEx.getMessage());
//					throw daoEx;
				}
				// reset counter
				counter = 0;
				msgMap = new ConcurrentHashMap<MessaggioDTO,List<MessaggioDTO>>();
			}
		*/	
		} // for sui messaggi in uscita
		/*
		 *  AGGIORNAMENTO DB MESSAGGI PEI INVIATI-RICEVUTI
		 */	
		/*
		try{
			messaggiDAO.aggiornaMsgInviatiRicevuti(msgMap);
		}
		catch(DAOException daoEx){
			System.err.println("GestionePEI_BusinessManager :: inviaRiceviPEI: " + daoEx.getMessage());
			throw daoEx;
		}	
		 */	
		log.info("inviaRiceviPEI END");
		
	} //inviaRiceviPEI
	
	
	/**
	 * 
	 * @param messaggioInUscitaPEI
	 * @return
	 * @throws Exception
	 */
	private List<MessaggioDTO> creaMessaggiInEntrataPEI(MessaggioDTO messaggioInUscitaPEI, SegnaturaDTO segnaturaIdentificatore) throws Exception {
		
		//	System.out.println("GestionePEI_BusinessManager :: creaMessaggiInEntrataPEI START");
		List<MessaggioDTO> listaMessaggiInEntrataPEI = new ArrayList<MessaggioDTO>();
		
		//	String codiceAOOdestinatario = null;
	 	List<DestinatarioDTO> listaDestinatariPer = messaggioInUscitaPEI.getElencoDestinatariPer();
	 	List<DestinatarioDTO> listaDestCC = messaggioInUscitaPEI.getElencoDestinatariCc();
	 	List<DestinatarioDTO> listaTotDestinatari = new ArrayList<DestinatarioDTO>();
	 	listaTotDestinatari.addAll(listaDestinatariPer);
	 	listaTotDestinatari.addAll(listaDestCC);
	 	
	 	MessaggioDTO messaggioInEntrataPEI = new MessaggiDAO().selezionaDettaglioMessaggioInUscitaPEI( messaggioInUscitaPEI.getMessageId() );	 	
	 	MessaggioDTO messaggioInEntrataPEIIns = null;
	 	int i = 0;
	 	for(DestinatarioDTO destinatario: listaTotDestinatari){
	 		messaggioInEntrataPEIIns = new MessaggioDTO();
	 		messaggioInEntrataPEIIns = (MessaggioDTO) SerializationUtils.clone(messaggioInEntrataPEI); 
	 		messaggioInEntrataPEIIns.setCodiceAOO(destinatario.getCodiceAOO());
	 		messaggioInEntrataPEIIns.setMessageId(messaggioInUscitaPEI.getMessageId());
	 		messaggioInEntrataPEIIns.setVerso(Constants.VERSO_MSG_ENTRANTE);
	 		messaggioInEntrataPEIIns.setSegnaturaIdentificatore(segnaturaIdentificatore);
	 		messaggioInEntrataPEIIns.setMessaggioIdentificatore( messaggioInUscitaPEI );
	 		messaggioInEntrataPEIIns.setStato(Constants.STATO_MSG_DA_PROTOCOLLARE);
	 		messaggioInEntrataPEIIns.setAccountPec("PEI"+destinatario.getCodiceAOO());
	 		messaggioInEntrataPEIIns.setSegnaturaMitt(segnaturaIdentificatore.getSegnatura());
	 		messaggioInEntrataPEIIns.setTipoMessaggio("messaggio-pei");
	 		SedeDTO anagraficaSede = new SedeDAO().selezioneSedeFromAOO(destinatario.getCodiceAOO());
	 		messaggioInEntrataPEIIns.setNomeRegione(anagraficaSede.getRegione());
	 		messaggioInEntrataPEIIns.setNomeUfficio(destinatario.getName());
	 		messaggioInEntrataPEIIns.setNomeSede(destinatario.getCodiceAOO()+"__/"+anagraficaSede.getDescrSede());
	 		messaggioInEntrataPEIIns.setElencoFileAllegati( messaggioInUscitaPEI.getElencoFileAllegati() );
	 		messaggioInEntrataPEIIns.setUiDoc(messaggioInUscitaPEI.getUiDoc());
	 		messaggioInEntrataPEIIns.setMessageId(messaggioInUscitaPEI.getMessageId());
	 		// lo stato ricevuto non lo imposto a questo punto ma dopo la protocollazione OK?
	 		// messaggioInEntrataPEI.setStato(Constants.STATO_MSG_RICEVUTO);
//			if(!listaSediDestinatarie.contains(messaggioInEntrataPEIIns.getCodiceAOO())){
//				listaSediDestinatarie.add(destinatario.getCodiceAOO());
			listaMessaggiInEntrataPEI.add(messaggioInEntrataPEIIns);
//				i++;
//			}
	 	}
		return bonificaMessaggiPeiInEntrata(listaMessaggiInEntrataPEI);
	}
	private List<MessaggioDTO> bonificaMessaggiPeiInEntrata(List<MessaggioDTO> messaggiInEntrata){
		List<String> listaSediDestinatarieDirezione = new ArrayList<String>(); 
		List<MessaggioDTO> messaggiInEntrataDef = new ArrayList<MessaggioDTO>();
		for(MessaggioDTO messaggio : messaggiInEntrata){
			if(!listaSediDestinatarieDirezione.contains(messaggio.getCodiceAOO()) && messaggio.getNomeUfficio().equalsIgnoreCase("direzione")){
				listaSediDestinatarieDirezione.add(messaggio.getCodiceAOO());
				messaggiInEntrataDef.add(messaggio);
			}
		}
		for(MessaggioDTO messaggio : messaggiInEntrata){
			if(!listaSediDestinatarieDirezione.contains(messaggio.getCodiceAOO())){
				listaSediDestinatarieDirezione.add(messaggio.getCodiceAOO());
				messaggiInEntrataDef.add(messaggio);
			}
		}
		return messaggiInEntrataDef;
 	}

	/**
	 * 
	 * @param messaggioInUscitaPEI
	 * @return
	 * @throws Exception
	 */
	/*
	private List<MessaggioDTO> generaRicevutaInEntrataPEI(MessaggioDTO messaggioInUscitaPEI, SegnaturaDTO segnaturaIdentificatore, String segnaturaMittente, String codiceAooMittente ) throws Exception {
		
		//System.out.println("GestionePEI_BusinessManager :: creaMessaggiInEntrataPEI START");
		List<MessaggioDTO> listaMessaggiInEntrataPEI = new ArrayList<MessaggioDTO>();
		//String codiceAOOdestinatario = null;
		
	 	List<DestinatarioDTO> listaDestinatariPer = messaggioInUscitaPEI.getElencoDestinatariPer();
	 	List<DestinatarioDTO> listaDestCC = messaggioInUscitaPEI.getElencoDestinatariCc();
	 	List<DestinatarioDTO> listaTotDestinatari = new ArrayList<DestinatarioDTO>();
	 	listaTotDestinatari.addAll(listaDestinatariPer);
	 	listaTotDestinatari.addAll(listaDestCC);
	 	
	 	MessaggioDTO messaggioInEntrataPEI = new MessaggiDAO().selezionaDettaglioPerRicevutaPEI( messaggioInUscitaPEI.getMessageId(), segnaturaMittente, codiceAooMittente);	 	
	 	
	 	for(DestinatarioDTO destinatario: listaTotDestinatari){
	 		messaggioInEntrataPEI.setCodiceAOO(destinatario.getCodiceAOO());
	 		messaggioInEntrataPEI.setMessageId(messaggioInUscitaPEI.getMessageId());
	 		messaggioInEntrataPEI.setVerso(Constants.VERSO_MSG_ENTRANTE);
	 		messaggioInEntrataPEI.setSegnaturaIdentificatore(segnaturaIdentificatore);
	 		messaggioInEntrataPEI.setMessaggioIdentificatore( messaggioInUscitaPEI );
	 		messaggioInEntrataPEI.setStato(Constants.STATO_MSG_DA_PROTOCOLLARE);
	 		messaggioInEntrataPEI.setAccountPec("PEI"+destinatario.getCodiceAOO());
	 		
	 		SedeDTO anagraficaSede = new SedeDAO().selezioneSedeFromAOO(destinatario.getCodiceAOO());
	 		messaggioInEntrataPEI.setNomeRegione(anagraficaSede.getRegione());
	 		messaggioInEntrataPEI.setNomeUfficio(destinatario.getName());
	 		messaggioInEntrataPEI.setNomeSede(destinatario.getCodiceAOO()+"__/"+anagraficaSede.getDescrSede());
			messaggioInEntrataPEI.setUiDoc(messaggioInUscitaPEI.getUiDoc());
			messaggioInEntrataPEI.setMessageId(messaggioInUscitaPEI.getMessageId());
			
	 		// lo stato ricevuto non lo imposto a questo punto ma dopo la protocollazione OK?
	 		listaMessaggiInEntrataPEI.add(messaggioInEntrataPEI);
	 	}
	 		 	
		return listaMessaggiInEntrataPEI;
	}
	*/
	//private MessaggioDTO richiestaProtocolloMessaggio(MessaggioDTO messaggioDaProtocollare){
	public static SegnaturaDTO richiestaProtocolloMessaggio(MessaggioDTO messaggioDaProtocollare) throws BusinessException{
		
		//log.debug("richiestaProtocolloMessaggio: " + messaggioDaProtocollare.getMessageId());		
		SegnaturaDTO segnaturaProtocollo = null;
		//@ TODO chiamare le classi che si interfacciano con il sistema di protocollazione INPS
		try{
			if ( Constants.VERSO_MSG_USCENTE.equalsIgnoreCase( messaggioDaProtocollare.getVerso() )){
				CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
				executor.setCommand( new ProtocollaMessaggioUscitaCommand( messaggioDaProtocollare ) );
				segnaturaProtocollo = (SegnaturaDTO)executor.executeCommandAndLogNew(messaggioDaProtocollare);
//				segnaturaProtocollo = (SegnaturaDTO)executor.executeCommandAndLog();
			} else if ( Constants.VERSO_MSG_ENTRANTE.equalsIgnoreCase( messaggioDaProtocollare.getVerso() )){
				CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
				executor.setCommand( new ProtocollaMessaggioEntrataCommand( messaggioDaProtocollare ) );
				segnaturaProtocollo = (SegnaturaDTO)executor.executeCommandAndLog();
			} else {
				throw new BusinessException(-50001, "Verso del messaggio [" + messaggioDaProtocollare.getVerso() != null ? messaggioDaProtocollare.getVerso() : "NULL" + "] PEI non riconosciuto ");
			}
			
		}
		catch(Exception e){
			// errore durante la protocollazione del msg corrente
			e.printStackTrace();
			log.error("richiestaProtocolloMessaggio: impossibile protocollare il messaggio " + messaggioDaProtocollare +" - "+ e.getMessage());
			//System.err.println("GestionePEI_BusinessManager :: richiestaProtocolloMessaggio: impossibile protocollare il messaggio " + messaggioDaProtocollare +" - "+ e.getMessage());
			if ( e instanceof BusinessException && ((BusinessException)e ).getErrorCode() == -50001 )
				throw (BusinessException)e;
			else 
				throw new BusinessException(-50010, "richiestaProtocolloMessaggio: impossibile protocollare il messaggio " + messaggioDaProtocollare +" - "+ e.getMessage());
		}
		return segnaturaProtocollo;
	}
	
	
	private  Map<String,String> richiestaProtocolloMessaggi(MessaggioDTO messaggioInUscitaPEI, List<MessaggioDTO> messaggiDaProtocollare) throws BusinessException, DAOException{
		
		Map<String,String> msgIdSegnaturaMap = new ConcurrentHashMap<String,String>();
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, 20 );
		MessaggiDAO messDao = new MessaggiDAO();
		int i = 0;
		for(MessaggioDTO messaggioDaProtocollare: messaggiDaProtocollare){						
			i++;
//			try{
				executor.setCommand( new RichiestaProtocolloMessaggiPEI( messaggioInUscitaPEI, messaggioDaProtocollare ) );
				executor.executeCommand();
				
				/*
				String segnaturaPei = messDao.leggiSegnaturaPEI(messaggioDaProtocollare.getMessageId(),  messaggioDaProtocollare.getAccountPec(), messaggioDaProtocollare.getCodiceAOO());
				
				if(segnaturaPei!=null&&!segnaturaPei.equalsIgnoreCase("")){
					segnatura.setSegnatura(segnaturaPei);
					segnatura.setStatus(SegnaturaDTO.OK);
				}else
					segnatura = richiestaProtocolloMessaggio(messaggioDaProtocollare);
				
				if ( segnatura.isOK() ){
					executor.setCommand( new ValorizzaSegnaturaCommand( messaggioDaProtocollare, segnatura.getSegnatura() ) );
					executor.executeCommand();
				//Dopo che la protocollazione ha dato esito positivo, viene impostato lo stato
				// a RI: Messaggio Ricevuto
					messaggioDaProtocollare.setStato(Constants.STATO_MSG_RICEVUTO);
					messaggioDaProtocollare.setPostedDate( new Date() );
					messaggioDaProtocollare.setDeliveredDate( messaggioDaProtocollare.getPostedDate() );
					messaggioDaProtocollare.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
				}else{
					messaggioDaProtocollare.setStato(Constants.STATO_MSG_DA_PROTOCOLLARE);
//					messaggioDaProtocollare.setPostedDate( new Date() );
					messaggioDaProtocollare.setDeliveredDate( messaggioDaProtocollare.getPostedDate() );
//					messaggioDaProtocollare.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
				}	
				
				messDao.aggiornaMsgInviatiRicevuti(messaggioInUscitaPEI, messaggioDaProtocollare, false);
				*/
//			msgIdSegnaturaMap.put(messaggioDaProtocollare.getMessageId(),segnatura.getSegnatura() );	
//			} else {
//			}catch(DAOException e){
//				notificaErroreByMail( "Errore durante il processo di protocollazione in entrata del messaggio: " + messaggioDaProtocollare.getMessageId() );
//				//throw new BusinessException(-70001, "Errore durante il processo di protocollazione in entrata del messaggio: " + messaggioDaProtocollare.getMessageId() );
//			}

		}
		List<Object> out = ((CommandExecutorMultiThread)executor).waitResult();
		
		if(out.size() == messaggiDaProtocollare.size() )
			messDao.aggiornaMsgInviatiRicevuti(messaggioInUscitaPEI, null, true);
		
		return msgIdSegnaturaMap;
	}
	
	
	/**
	 * Invia una email al reponsabile o ai responsabili del processo
	 * @param errorMessage
	 */
	public static void notificaErroreByMail(String errorMessage){
				
		log.debug("notificaErroreByMail");
		try{
			Vector<String> destinatari = new Vector<String>();
			String destPer = ConfigProp.emailDestDettaglioEccezione;
			String[] destinatariPostaPer = destPer.split(",");
			String destinatario;
			if(destinatariPostaPer.length > 0){
				for(int d=0; d<destinatariPostaPer.length; d++){
					if(destinatariPostaPer[d].trim().contains("<"))
						destinatario = destinatariPostaPer[d].trim().substring(destinatariPostaPer[d].trim().indexOf("<"),destinatariPostaPer[d].trim().indexOf(">")).replaceAll("<", "").replaceAll(">", "");
					else
						destinatario = destinatariPostaPer[d].trim();

					destinatari.add(destinatario);
				}
			}
//			Posta posta = new Posta();
//			posta.inviaEmail(ConfigProp.serverPosta,
//							ConfigProp.uidPosta,
//							ConfigProp.pwdPosta,
//							ConfigProp.emailMittDettaglioEccezione,
//							destinatari,
//							null,
//							"Errore: " + errorMessage,
//							errorMessage,
//							false);
		}
		catch(Exception e){}
		
	} //notificaErroreByMail
}
