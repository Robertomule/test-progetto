package it.eustema.inps.agentPecPei.manager;

/**
 * Factory Singleton per oggetti di tipo CommandExecutor
 * @author ALEANZI
 *
 */
public class CommandExecutorManager {
	
	private static CommandExecutorManager instance = null;
	
	private CommandExecutorManager(){}
	
	public static CommandExecutorManager getInstance(){
		if ( instance == null ){
			synchronized ( CommandExecutorManager.class ) {
				if ( instance == null ){
					instance = new CommandExecutorManager();
				}
				
			}
		}
		return instance;
	}
	
	public CommandExecutor getExecutor(){
		return new CommandExecutorImpl();
	}
	
	public CommandExecutor getExecutor( TYPE type, Object ... params ){
		System.out.println("CommandExecutorManager :: getExecutor :: Controllo del tipo di thread (Normal / Multi_Thread)");
		if ( type == TYPE.NORMAL ){
			return new CommandExecutorImpl();
		}
		else if ( type == TYPE.MULTI_THREAD ){
			System.out.println("CommandExecutorManager :: getExecutor :: MULTI_THREAD");
			return new CommandExecutorMultiThreadImpl( (Integer)params[0] );
		}
		else return null;
	}
	
	
	public enum TYPE {
		NORMAL,
		MULTI_THREAD;
	}
	
	
}
