package it.eustema.inps.agentPecPei.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.util.SendMailCommand;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.NoCallableCommandException;
import it.eustema.inps.agentPecPei.exception.NotifyMailBusinessException;
import it.eustema.inps.utility.ConfigProp;

/**
 * Classe Executor che si occupa di eseguire Comandi in modalit� MultiThread. Ogni comando oltre ad essere una classe che implementa 
 * l'interfaccia Command, deve anche implementare l'interfaccia Callable. Ogni comando
 * viene eseguito in un nuovo thread.
 * @author ALEANZI
 *
 */
public class CommandExecutorMultiThreadImpl extends CommandExecutorImpl implements CommandExecutorMultiThread {
	private ExecutorService concurrentEX = null;
	private List<Future<Object>> futureList;
	private List<Command> listaCommand;
	
	private static final int MAX_THREADS = new Integer(ConfigProp.maxThread).intValue();
	
	public CommandExecutorMultiThreadImpl( int numberThreads ){
		this.concurrentEX = Executors.newFixedThreadPool( numberThreads > MAX_THREADS ? MAX_THREADS : numberThreads );
		futureList = new ArrayList<Future<Object>>();
		this.listaCommand = new ArrayList<Command>( numberThreads );
	}
	
	@Override
	public Object executeCommand() throws BusinessException {
		
		log.debug("Inizio esecuzione COMMAND : [" + command.getClass().getCanonicalName() +"]" );
		
		Object out = null;
		if ( command instanceof Callable){
			
		} else {
			throw new NoCallableCommandException(-9999, "La classe [" + command.getClass().getCanonicalName() +"] non � Callable e non pu� essere eseguita come Thread." );
		}
		
		try{
			Future<Object> submit = concurrentEX.submit((Callable)command);
			futureList.add( submit );
			
			
		} catch ( Exception e ){
			StringBuilder sb = new StringBuilder("Errore durante esecuzione esecuzione COMMAND : [");
			sb.append( command.getClass().getCanonicalName() );
			sb.append("]");
			sb.append(" Dettaglio Errore: ");
			sb.append( e.getMessage() );
			log.error( sb.toString() );
			
			if ( e instanceof NotifyMailBusinessException ){
				new SendMailCommand( (NotifyMailBusinessException)e ).execute();
			}
			
			throw new BusinessException( -8888, e.getMessage() );
		}
		 
		log.debug("Fine esecuzione COMMAND : [" + command.getClass().getCanonicalName() +"]" );
		return out;
	}

	@Override
	public Object executeCommandAndLog() throws BusinessException {
		
		long startTime = new Date().getTime();
		long endTime;
		Object out = null;
		try{
			out = executeCommand();
			endTime = new Date().getTime();
			log( startTime, endTime );
		} catch ( BusinessException e ){
			endTime = new Date().getTime();
			log( startTime, endTime, e );
			throw e;
		}
		return out;
	}

	@Override
	public List<Object> waitResult() {
		 /*while (!concurrentEX.isTerminated()) {
			 try {
				concurrentEX.awaitTermination( 10, TimeUnit.SECONDS );
			} catch (InterruptedException e) {}
	     }*/
		 
		 List<Object> out = new ArrayList<Object>();
		 for ( Future<Object> f : futureList ){
			 try {
				out.add( f.get() );
			} catch (InterruptedException e) {
				log.error("Errore nel recupero risultato : " + e.getMessage());
			} catch (ExecutionException e) {
				log.error("Errore nel recupero risultato : " + e.getMessage());
			}
		 }
		
		return out;
	}
	
	@Override
	public void setCommand(Command command) {
		super.setCommand( command );
		this.listaCommand.add( command );
	}

}
