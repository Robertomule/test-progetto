package it.eustema.inps.agentPecPei.manager;

import java.util.List;
/**
 * Interfaccia per classi che si occupano di eseguire attivit� definite in oggetti della 
 * classe Command in modalit� MultiThread.
 * @author ALEANZI
 *
 */

/**
 * Mette il attesa il CommandExecutor finch� non termina l'esecuzione di tutti i Thread.
 * Ritorna la lista di output di ogni singolo comando.
 */
public interface CommandExecutorMultiThread extends CommandExecutor {
	public List<Object> waitResult();
}
