package it.eustema.inps.agentPecPei.manager;

import java.util.List;

import org.apache.log4j.Logger;

import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.exception.PecBusinessException;
import it.eustema.inps.agentPecPei.gestioneDestinatari.CaricaDestinatariCommand;
import it.eustema.inps.agentPecPei.gestioneDestinatari.GestDatiCert;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;

public class GestioneDestinatari_BusinessManager {
	
	private static Logger log = Logger.getLogger(GestioneDestinatari_BusinessManager.class);
	
	public String elabora() throws PecBusinessException{
		
		log.info("GestioneDestinatari_BusinessManager :: elabora");
		System.out.println("Inizio Elaborazione");
		String exitCode = "";
		
		try {
				MessaggiDAO dao = new MessaggiDAO();
				List<GestDatiCert> listDatiCert = dao.selectDatiCert();
				
				if(listDatiCert.size() > 0){
					CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, listDatiCert.size() );
	//				CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
					
					for ( GestDatiCert datiCert: listDatiCert ){
							executor.setCommand( new CaricaDestinatariCommand(datiCert) );
							executor.executeCommandAndLog();
					}
					
					List<Object> out = ((CommandExecutorMultiThread)executor).waitResult();
					
					log.info("Elaborati "+out.size());
					System.out.println("Elaborati "+out.size());
				}else{
					System.out.println("Nessun Messaggio da Elaborare!");
					log.info("Nessun Messaggio da Elaborare!");
				}
			
		} catch (Exception e) {
			e.printStackTrace();		
		}
		
		log.info("GestioneDestinatari_BusinessManager :: Fine Elaborazione.");
		System.out.println("Fine Elaborazione.");
		
		return exitCode;
	}
}
