package it.eustema.inps.agentPecPei.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import it.eustema.inps.agentPecPei.business.pec.InviaMessaggiPECInUscitaCommand;
import it.eustema.inps.agentPecPei.business.pec.InviaMessaggiPECInUscitaCommandTest;
import it.eustema.inps.agentPecPei.business.pec.RiceviMessaggiPECInIngressoCommand;
import it.eustema.inps.agentPecPei.business.pec.RiceviMessaggiPECInIngressoCommandTest;
import it.eustema.inps.agentPecPei.business.pec.mail.ReadAndElabMessaggiPecDaProtocollare;
import it.eustema.inps.agentPecPei.business.pec.mail.ReadAndElabMessaggiPecDaProtocollareTest;
import it.eustema.inps.agentPecPei.dao.AccountPecDAO;
import it.eustema.inps.agentPecPei.dao.DestinatariTestDao;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AccountPecDTO;
import it.eustema.inps.agentPecPei.exception.PecBusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager.TYPE;
import it.eustema.inps.utility.ConfigProp;

public class GestionePEC_BusinessManagerTest {
	private static Logger log = Logger.getLogger(GestionePEC_BusinessManagerTest.class);
	
	
	public String elabora() throws PecBusinessException {
		
		log.info("GestionePEC_BusinessManager :: elabora");
		String exitCode = "";
		
		try{
			//Carico Gli Indirizzi Di Test
			DestinatariTestDao testDao = new DestinatariTestDao();
			ConfigProp.indirizziPerTest = testDao.getDestinatariTest();
		}catch(Exception e){
			log.error("Errore in caricamento lista destinatari test: "+e.getMessage());
			ConfigProp.indirizziPerTest = new ArrayList<String>(); //Creo una lista vuota
		}
		
		//Se ho caricato con successo i destinatari faccio il blocco altrimento non eseguo nulla
		if(ConfigProp.indirizziPerTest.size() > 0)
			try {
				List<String> codAOOMessaggi = new MessaggiDAO().selectAllAOOTest();
				List<String> codAOOAccounts = new AccountPecDAO().selectAllAOOTest();
				
				CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor( TYPE.MULTI_THREAD, codAOOMessaggi.size() + codAOOAccounts .size());
				
				for ( String codAOO: codAOOMessaggi ){
						executor.setCommand( new InviaMessaggiPECInUscitaCommandTest( codAOO ) );
						executor.executeCommand();
				}
							
				for ( String codAOO: codAOOMessaggi ){
						executor.setCommand( new ReadAndElabMessaggiPecDaProtocollareTest( codAOO ) );
						executor.executeCommand();
				}
				
				for ( String codAOO: codAOOAccounts ){
					executor.setCommand( new RiceviMessaggiPECInIngressoCommandTest( codAOO ) );
					executor.executeCommand();	
				}
				
				List<Object> out = ((CommandExecutorMultiThread)executor).waitResult();
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new PecBusinessException(e.getMessage());			
			}
		
		log.info("GestionePEC_BusinessManager :: fine");
		
		
		return exitCode;
	}
}
