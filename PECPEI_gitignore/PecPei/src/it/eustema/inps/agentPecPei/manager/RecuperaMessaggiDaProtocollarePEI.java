package it.eustema.inps.agentPecPei.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioEntrataCommand;
import it.eustema.inps.agentPecPei.business.pec.documentale.ProtocollaMessaggioUscitaCommand;
import it.eustema.inps.agentPecPei.business.util.ValorizzaSegnaturaCommand;
import it.eustema.inps.agentPecPei.dao.AllegatiDAO;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dao.SedeDAO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.dto.SedeDTO;
import it.eustema.inps.agentPecPei.dto.SegnaturaDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.exception.DAOException;
import it.eustema.inps.agentPecPei.exception.PeiBusinessException;
import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.Posta;

public class RecuperaMessaggiDaProtocollarePEI {

	int commitPoint = ConfigProp.intervalloDiCommit;
	private static Log log = LogFactory.getLog("it.eustema.inps.agentPecPei.manager.RecuperaMessaggiDaProtocollarePEI");
	
	public String elabora() throws PeiBusinessException {
				
		log.info("riprovaProtocollazionePEI START");
		String exitCode = "";
		try {
			System.out.println("\nProvo la protocollazione della PEI");
			riprovaProtocollazionePEI();
		} catch (Exception e) {
			throw new PeiBusinessException(e.getMessage());			
		}
		
		return exitCode;
	}
	
	
	/**
	 * Metodo che implementa la funzionalita' PECPEI_F002 Invio-Ricezione PEI.
	 * Il flusso e' descritto al � 5.1.2 del doc di analisi dell'Agent.
	 * @throws Exception se si verifica un errore bloccante durante l'elaborazione
	 */
	private void riprovaProtocollazionePEI() throws Exception{

		System.out.println("Sono nel metodo...");
		log.info("inviaRiceviPEI START");
		MessaggiDAO messaggiDAO = new MessaggiDAO();
		List<MessaggioDTO> listaMessaggiInUscitaPEI = null;

		// contenitore formato dai messaggi in uscita fin qui elaborati (come chiavi)
		// e dalla lista dei corrispondenti messaggi in entrata
		int counter = 0;

		/*
		 *  SELEZIONE ELENCO MESSAGGI PEI DA RIPROTOCOLLARE
		 */		
		try{
			System.out.println("\n$ - Seleziono i messaggi da protocollare");
			listaMessaggiInUscitaPEI = messaggiDAO.selezionaMessaggiDaProtocollarePEI();

		}
		catch(DAOException daoEx){
			System.out.println("Eccezione");
			log.error("inviaRiceviPEI: " + daoEx.getMessage());
			//System.err.println("GestionePEI_BusinessManager :: inviaRiceviPEI: " + daoEx.getMessage());			
			throw daoEx;
		}
		
		for(MessaggioDTO messaggioInUscitaPEI: listaMessaggiInUscitaPEI){			
			/*
			 *  ELABORAZIONE SINGOLO MESSAGGIO PEI IN USCITA
			 */
			counter ++;
			System.out.println("Scorro la lista dei messaggi in uscita con il counter pari a 1");
			log.debug("riprovaProtocollazionePEI :: counter=" + counter);
			SegnaturaDTO segnaturaMessaggio = null;
			messaggioInUscitaPEI.setElencoFileAllegati( new AllegatiDAO().selectAllegatiWS( messaggioInUscitaPEI.getMessageId() ) );
			// 	RICHIESTA AL SISTEMA DI PROTOCOLLO
			System.out.println("Faccio la richiesta al sistema di protocollo...");
			try {
				System.out.println("******SegnaturaMessaggio\n\n");
				segnaturaMessaggio = richiestaProtocolloMessaggio(messaggioInUscitaPEI);

				if( !segnaturaMessaggio.isOK() ){
					System.out.println("Segnatura non e' OK.");
					notificaErroreByMail("Errore durante il processo di riprovaProtocollazionePEI del messaggio PEI: [" + messaggioInUscitaPEI.getMessageId() + "] Dettaglio Errore Codice: " + segnaturaMessaggio.getCodiceErrore() + " Descrizione: " + segnaturaMessaggio.getDescErrore() );
					continue; // errore non bloccante passa elaborazione del msg successivo
				}
				else{
					System.out.println("Segnatura E' OK.");
					// segnatura ottenuta dal WS: impostarla @ TODO vedere come impostare la segnatura ottenuta
					if(messaggioInUscitaPEI.getVerso().equals("E"))
						messaggioInUscitaPEI.setStato( Constants.STATO_RICEVUTO );
					else
						messaggioInUscitaPEI.setStato( Constants.STATO_MSG_INVIATO );
					
					messaggioInUscitaPEI.setSubstato(Constants.SUBSTATO_INVIO_NOTIFICA);
					
					CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
					executor.setCommand( new ValorizzaSegnaturaCommand( messaggioInUscitaPEI, segnaturaMessaggio.getSegnatura() ) );
					executor.executeCommand();
					
				}
			} catch (Exception e) {
				// errore durante la protocollazione del msg in uscita corrente
				notificaErroreByMail("Errore durante il processo di riprovaProtocollazionePEI del messaggio PEI: [" + messaggioInUscitaPEI.getMessageId() + "] Dettaglio Errore: " + e.getMessage());
				continue; // errore non bloccante passa elaborazione msg successivo
			}
			
			// CREAZIONE MESSAGGI IN ENTRATA - 1 PER OGNI DESTINATARIO DEL MSG IN USCITA
			log.debug("riprovaProtocollazionePEI :: counter=" + counter);
			
			// @ TODO GESTIRE I MESSAGGI RIMANENTI DELL'ULTIMO GRUPPO!!!!
			// chiama l'aggiornamento del DB														 
			try{					
//					int ret = messaggiDAO.aggiornaMsgInviatiRicevuti(msgMap);
				int ret = messaggiDAO.aggiornaMsgUscenteInviato(null,messaggioInUscitaPEI);
			}
			catch(DAOException daoEx){
				//System.err.println("GestionePEI_BusinessManager :: inviaRiceviPEI: " + daoEx.getMessage());
				log.error("riprovaProtocollazionePEI: " + daoEx.getMessage());
				throw daoEx;
			}
		} // for sui messaggi da riprotocollare
		
		log.info("riprovaProtocollazionePEI END");
	} //inviaRiceviPEI
	
	
	/**
	 * 
	 * @param messaggioInUscitaPEI
	 * @return
	 * @throws Exception
	 */
	private List<MessaggioDTO> creaMessaggiInEntrataPEI(MessaggioDTO messaggioInUscitaPEI, SegnaturaDTO segnaturaIdentificatore) throws Exception {
		
		//System.out.println("GestionePEI_BusinessManager :: creaMessaggiInEntrataPEI START");
		List<MessaggioDTO> listaMessaggiInEntrataPEI = new ArrayList<MessaggioDTO>();
		//String codiceAOOdestinatario = null;
		
	 	List<DestinatarioDTO> listaDestinatariPer = messaggioInUscitaPEI.getElencoDestinatariPer();
	 	List<DestinatarioDTO> listaDestCC = messaggioInUscitaPEI.getElencoDestinatariCc();
	 	List<DestinatarioDTO> listaTotDestinatari = new ArrayList<DestinatarioDTO>();
	 	listaTotDestinatari.addAll(listaDestinatariPer);
	 	listaTotDestinatari.addAll(listaDestCC);
	 	
	 	MessaggioDTO messaggioInEntrataPEI = new MessaggiDAO().selezionaDettaglioMessaggioInUscitaPEI( messaggioInUscitaPEI.getMessageId() );	 	
	 	
	 	for(DestinatarioDTO destinatario: listaTotDestinatari){
	 		//BeanUtilsBean.getInstance().copyProperties(messaggioInEntrataPEI, messaggioInUscitaPEI);
	 		//codiceAOOdestinatario = destinatarioPer.getCodiceAOO();
	 		messaggioInEntrataPEI.setCodiceAOO(destinatario.getCodiceAOO());
	 		messaggioInEntrataPEI.setMessageId(messaggioInUscitaPEI.getMessageId());
	 		messaggioInEntrataPEI.setVerso(Constants.VERSO_MSG_ENTRANTE);
	 		messaggioInEntrataPEI.setSegnaturaIdentificatore(segnaturaIdentificatore);
	 		messaggioInEntrataPEI.setMessaggioIdentificatore( messaggioInUscitaPEI );
	 		messaggioInEntrataPEI.setStato(Constants.STATO_MSG_DA_PROTOCOLLARE);
	 		messaggioInEntrataPEI.setAccountPec("PEI"+destinatario.getCodiceAOO());
	 		
	 		SedeDTO anagraficaSede = new SedeDAO().selezioneSedeFromAOO(destinatario.getCodiceAOO());

	 		messaggioInEntrataPEI.setNomeRegione(anagraficaSede.getRegione());
	 		messaggioInEntrataPEI.setNomeUfficio(destinatario.getName());
	 		messaggioInEntrataPEI.setNomeSede(anagraficaSede.getDescrSede());
	 		
	 		//messaggioInEntrataPEI.setElencoDestinatariPer( messaggioInUscitaPEI.getElencoDestinatariPer() );
	 		//messaggioInEntrataPEI.setElencoDestinatariCc( messaggioInUscitaPEI.getElencoDestinatariCc() );
	 		messaggioInEntrataPEI.setElencoFileAllegati( messaggioInUscitaPEI.getElencoFileAllegati() );
//	 		String uiDoc = StaticUtil.generateUiDoc(messaggioInEntrataPEI.getCodiceAMM(), messaggioInEntrataPEI.getAutoreCompositore(), destinatario.getCodiceAOO(), messaggioInEntrataPEI.getIpAddress());
//			String messageID = StaticUtil.generateMessageIdPei(uiDoc);
			messaggioInEntrataPEI.setUiDoc(messaggioInUscitaPEI.getUiDoc());
			messaggioInEntrataPEI.setMessageId(messaggioInUscitaPEI.getMessageId());
	 		
	 		// lo stato ricevuto non lo imposto a questo punto ma dopo la protocollazione OK?
	 		//messaggioInEntrataPEI.setStato(Constants.STATO_MSG_RICEVUTO);
	 		
	 		listaMessaggiInEntrataPEI.add(messaggioInEntrataPEI);
	 	}
	 		 	
	 	/*if(listaDestinatariPer != null){
		 	for(DestinatarioDTO destinatarioPer: listaDestinatariPer){
		 		MessaggioDTO messaggioInEntrataPEI = new MessaggioDTO(); 
		 		//codiceAOOdestinatario = destinatarioPer.getCodiceAOO();
		 		messaggioInEntrataPEI.setCodiceAOO(destinatarioPer.getCodiceAOO());
		 		messaggioInEntrataPEI.setMessageId(messaggioInUscitaPEI.getMessageId());
		 		messaggioInEntrataPEI.setVerso(Constants.VERSO_MSG_ENTRANTE);
		 		
		 		listaMessaggiInEntrataPEI.add(messaggioInEntrataPEI);
		 	}
	 	}
	 	if(listaDestCC != null){
		 	for(DestinatarioDTO destinatarioCC: listaDestCC){
		 		MessaggioDTO messaggioInEntrataPEI = new MessaggioDTO();
		 		messaggioInEntrataPEI.setCodiceAOO(destinatarioCC.getCodiceAOO());
		 		
		 		listaMessaggiInEntrataPEI.add(messaggioInEntrataPEI);
		 	}	 	
	 	}
	 	*/		
		
		return listaMessaggiInEntrataPEI;
	}
	
	
	//private MessaggioDTO richiestaProtocolloMessaggio(MessaggioDTO messaggioDaProtocollare){
	private SegnaturaDTO richiestaProtocolloMessaggio(MessaggioDTO messaggioDaProtocollare) throws BusinessException{
		
		//log.debug("richiestaProtocolloMessaggio: " + messaggioDaProtocollare.getMessageId());		
		SegnaturaDTO segnaturaProtocollo = null;
		//@ TODO chiamare le classi che si interfacciano con il sistema di protocollazione INPS
		try{
			System.out.println("Entro nella richiesta protocollo messaggioooo");

			if ( Constants.VERSO_MSG_USCENTE.equalsIgnoreCase( messaggioDaProtocollare.getVerso() )){
				CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
				executor.setCommand( new ProtocollaMessaggioUscitaCommand( messaggioDaProtocollare ) );
				segnaturaProtocollo = (SegnaturaDTO)executor.executeCommandAndLog();

			} else if ( Constants.VERSO_MSG_ENTRANTE.equalsIgnoreCase( messaggioDaProtocollare.getVerso() )){
				System.out.println("Entro nell'else if -> VERSO_MSG_ENTRANTE");
				CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
				executor.setCommand( new ProtocollaMessaggioEntrataCommand( messaggioDaProtocollare ) );
				System.out.println("$ - Executor : " + executor);
				segnaturaProtocollo = (SegnaturaDTO)executor.executeCommandAndLog();
				System.out.println("$ - Segnatura protocollo : " + segnaturaProtocollo);
			} else {
				throw new BusinessException(-50001, "Verso del messaggio [" + messaggioDaProtocollare.getVerso() != null ? messaggioDaProtocollare.getVerso() : "NULL" + "] PEI non riconosciuto ");
			}
			
		}
		catch(Exception e){
			// errore durante la protocollazione del msg corrente
			e.printStackTrace();
			log.error("richiestaProtocolloMessaggio: impossibile protocollare il messaggio " + messaggioDaProtocollare +" - "+ e.getMessage());
			//System.err.println("GestionePEI_BusinessManager :: richiestaProtocolloMessaggio: impossibile protocollare il messaggio " + messaggioDaProtocollare +" - "+ e.getMessage());
			if ( e instanceof BusinessException && ((BusinessException)e ).getErrorCode() == -50001 )
				throw (BusinessException)e;
			else 
				throw new BusinessException(-50010, "richiestaProtocolloMessaggio: impossibile protocollare il messaggio " + messaggioDaProtocollare +" - "+ e.getMessage());
		}
		return segnaturaProtocollo;
	}
	
	
	private  Map<String,String> richiestaProtocolloMessaggi(List<MessaggioDTO> messaggiDaProtocollare) throws BusinessException{
		
		Map<String,String> msgIdSegnaturaMap = new ConcurrentHashMap<String,String>();
		CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		for(MessaggioDTO messaggioDaProtocollare: messaggiDaProtocollare){						

			SegnaturaDTO segnatura = richiestaProtocolloMessaggio(messaggioDaProtocollare);
			
			if ( segnatura.isOK() ){
				executor.setCommand( new ValorizzaSegnaturaCommand( messaggioDaProtocollare, segnatura.getSegnatura() ) );
				executor.executeCommand();
				
				//Dopo che la protocollazione ha dato esito positivo, viene impostato lo stato
				// a RI: Messaggio Ricevuto
				messaggioDaProtocollare.setStato(Constants.STATO_MSG_RICEVUTO);
				messaggioDaProtocollare.setPostedDate( new Date() );
				messaggioDaProtocollare.setDeliveredDate( messaggioDaProtocollare.getPostedDate() );
				
				msgIdSegnaturaMap.put(messaggioDaProtocollare.getMessageId(),segnatura.getSegnatura() );	
			} else {
				notificaErroreByMail( "Errore durante il processo di protocollazione in entrata del messaggio: " + messaggioDaProtocollare.getMessageId() );
				//throw new BusinessException(-70001, "Errore durante il processo di protocollazione in entrata del messaggio: " + messaggioDaProtocollare.getMessageId() );
			}
				
				
				
			

		}		

		return msgIdSegnaturaMap;
	}
	
	
	/**
	 * Invia una email al reponsabile o ai responsabili del processo
	 * @param errorMessage
	 */
	private void notificaErroreByMail(String errorMessage){
				
		log.debug("notificaErroreByMail");
		try{
			Vector<String> destinatari = new Vector<String>();
			String destPer = ConfigProp.emailDestDettaglioEccezione;
			String[] destinatariPostaPer = destPer.split(",");
			String destinatario;
			if(destinatariPostaPer.length > 0){
				for(int d=0; d<destinatariPostaPer.length; d++){
					if(destinatariPostaPer[d].trim().contains("<"))
						destinatario = destinatariPostaPer[d].trim().substring(destinatariPostaPer[d].trim().indexOf("<"),destinatariPostaPer[d].trim().indexOf(">")).replaceAll("<", "").replaceAll(">", "");
					else
						destinatario = destinatariPostaPer[d].trim();

					destinatari.add(destinatario);
				}
			}
			Posta posta = new Posta();
			posta.inviaEmail(ConfigProp.serverPosta,
							ConfigProp.uidPosta,
							ConfigProp.pwdPosta,
							ConfigProp.emailMittDettaglioEccezione,
							destinatari,
							null,
							"Errore: " + errorMessage,
							errorMessage,
							false);
		}
		catch(Exception e){}
		
	} //notificaErroreByMail
}
