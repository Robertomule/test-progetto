package it.eustema.inps.agentPecPei.util;

import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.builder.MessageMessaggioDTOBuilder;
import it.eustema.inps.agentPecPei.business.util.SendMailInteropCommand;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.DestinatarioDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.DBConnectionManager;
import it.eustema.inps.utility.StaticUtil;
import it.eustema.inps.utility.logging.LoggingConfigurator;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeUtility;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;


public class AnalizzaMessaggiUtilsFile implements Command {

	private static Logger log = Logger.getLogger(AnalizzaMessaggiUtilsFile.class);
	private CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
	private MessaggiDAO messaggiDAO = new MessaggiDAO();
//	static BASE64Decoder decoder = new BASE64Decoder();
//	static BASE64Encoder encoder = new BASE64Encoder();
	int depth = -1;
	Connection connPecPei = null;
	// private String codAOO;
	// private String accountPEC;
	// private String nomeFile;
	static {
		System.out.println("AnalizzaMessaggiUtils :: esegue blocco di inizializzazione");
		// blocco eseguito una volta al caricamento della classe
		try {
			// carica i parametri dal file di cfg
			// new ConfigProp(cfgFileName);
			new ConfigProp().init();
			// inizializzazione configurazione del logging
			new LoggingConfigurator().init(ConfigProp.logPathFileName);
		} catch (Exception e) {
			// System.err.println("" + e.getMessage());
		}
	}

	public AnalizzaMessaggiUtilsFile() {
		try {
//			connPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// this.codAOO = codAOO;
		// this.nomeFile = nomeFile;
		// this.accountPEC = accountPEC;
	}

	public static void main(String[] args) {
		// String from =
		// "\"Per conto di: cch39867@pec.carabinieri.it\" <posta-certificata@cert.actalis.it>";
		// System.out.println(from.substring(0,from.lastIndexOf("\"")).replaceAll("<",
		// "").replaceAll(">", "").replaceAll("\"",
		// "").replaceAll("Per conto di:", "").trim());
		try {
			//
			// MessaggioDTO messaggio = new MessaggioDTO();
			// messaggio.setReplyTo("test-codice@inps.it");
			// System.out.println(messaggio.getReplyTo().substring(0, messaggio.getReplyTo().indexOf("@")));
			CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
			executor.setCommand(new AnalizzaMessaggiUtilsFile());
			executor.executeCommand();
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Object execute() throws BusinessException {
		// TODO Auto-generated method stub
		
		boolean gestioneDB = false;
		try {
			BufferedReader br = null;
			BufferedReader brContent = null;
			try {

				String sCurrentLine="";//= "<opec275.20140915152631.10370.04.1.10.3@pec.aruba.it>";
				br = new BufferedReader(new FileReader("C:\\testPdf\\messaggiBonificati\\BodyAnomali.txt"));

				// Per una lettura da file
				if ((sCurrentLine = br.readLine()) != null) {
					sCurrentLine = "<opec228.20141016124117.00357.02.1.211@sicurezzapostale.it>";
//					sCurrentLine = "<5E303A4B.00492C04.ADA87691.31B549F2.posta-certificata@postacert.it.net>";
//					sCurrentLine = "<opec275.20141007105315.09912.05.1.2@postacert.inps.gov.it>";		
						try {
								String nomeFile = "C:\\testPdf\\messaggiBonificati\\"+ sCurrentLine.replaceAll("<", "").replaceAll(">", "")+ "postacert.eml";
								File f = new File(nomeFile);
//								java.sql.Timestamp postedDate = null;
//								String codiceAOO = "";
//								String accountPec = "";
//								String object = "";
								if (true) {
//									ByteArrayInputStream aProtoSemplice = new ByteArrayInputStream(Files);
//									Reader readerSemplice = new InputStreamReader(aProtoSemplice, "ISO-8859-1");
//									System.out.println(object.replaceAll("(\\r\\n|\\n||\\t)", ""));
									FileInputStream io = new FileInputStream(f);
									MimeBodyPart m = new MimeBodyPart(io);
									Multipart multipart = (Multipart) m.getContent();
									MessaggioDTO messaggio = new MessaggioDTO();
									ArrayList<AllegatoDTO> allegati = new ArrayList<AllegatoDTO>();
									ArrayList<DestinatarioDTO> destinatari = new ArrayList<DestinatarioDTO>();

									try {
										String postedDateString = MessageMessaggioDTOBuilder.readHeaders("Date", m.getAllHeaders()) == null ? null : MessageMessaggioDTOBuilder.readHeaders("Date",m.getAllHeaders()).get(0);
										SimpleDateFormat date = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US);
										java.util.Date data = date.parse(postedDateString);
										SimpleDateFormat date1 = new SimpleDateFormat("dd/MM/yyyy hh.mm.ss");
										// System.out.println(date1.format(data));
										messaggio.setPostedDate(date1.parse(date1.format(data)));
									} catch (ParseException e) {
										// TODO Blocco catch generato
										// automaticamente
										e.printStackTrace();
									}
									String subject = MessageMessaggioDTOBuilder.readHeaders("Subject", m.getAllHeaders()) == null ? null : MessageMessaggioDTOBuilder.readHeaders("Subject",m.getAllHeaders()).get(0);
									String messageId = MessageMessaggioDTOBuilder.readHeaders("Message-ID", m.getAllHeaders()) == null ? null : MessageMessaggioDTOBuilder.readHeaders("Message-ID",m.getAllHeaders()).get(0);
									
//									String tipoRicevutaCorretto = MessageMessaggioDTOBuilder.readHeaders("X-TipoRicevuta", m.getAllHeaders()) == null ? null : MessageMessaggioDTOBuilder.readHeaders("X-TipoRicevuta",m.getAllHeaders()).get(0);
//									System.out.println("Tipo Ricevuta Corretto:"+tipoRicevutaCorretto);
									
//									String tipoRicevuta = "";
//									StringTokenizer st = new StringTok0enizer(contenuto,"\n");
//									boolean stampa= false;
//									boolean creaNuovoFile = false;
//									int pdf = 0;
//									PrintWriter writer = new PrintWriter("test.txt", "UTF-8");
//									String newFile = "";
//									StringWriter sw = new StringWriter();
//									while(st.hasMoreElements()){
//										String stringaLetta = st.nextElement().toString();
//										if(stringaLetta.startsWith("begin")){
//											stampa=true;
//											creaNuovoFile = true;
//											pdf++;
//										}
//										if(stringaLetta.startsWith("end")){
//											writer.close();
//											writer.flush();
//											
//											byte dearr[] = decoder.decodeBuffer(new String(IOUtils.toByteArray(new FileInputStream(newFile))));
//											File fNew = new File(newFile);
//											FileOutputStream foNew = new FileOutputStream(fNew);
//											foNew.write(dearr);
//											foNew.flush();
//											foNew.close();
//											stampa=false;
//										}
//										if(stampa){
//											if(creaNuovoFile){
//												newFile = "C:\\testPdf\\messaggiBonificati\\"+stringaLetta.replaceAll("begin","").trim();
//												writer = new PrintWriter(newFile);
//												creaNuovoFile = false;
//											}
//											if(!stringaLetta.startsWith("begin")){
//												sw.append(stringaLetta.trim());
//												writer.println(stringaLetta.replaceAll("(\\r\\n|\\n||\\t)", "").trim());
//											}
//										}
//									}
									
//									List<String> s = MessageMessaggioDTOBuilder.readRowContent("X-TipoRicevuta",st);
//									if(s.size()>0)
//										System.out.println(s.get(0));
									
									String tipoRicevuta = ""; //MessageMessaggioDTOBuilder.readRowContent("X-TipoRicevuta",st).size()==0?"":MessageMessaggioDTOBuilder.readRowContent("X-TipoRicevuta",st).get(0);
//									System.out.println("Tipo Ricevuta :"+tipoRicevuta);
									
									messaggio.setMessageId(messageId);
									// System.out.println(MimeUtility.decodeText(subject));
									// System.out.println(MimeUtility.decodeText(new
									// String(subject.getBytes("UTF-8"),
									// "UTF-8")));
									/*
									brContent = new BufferedReader(new FileReader(nomeFile));
									String sCurrentLineContent = "";
									while((sCurrentLineContent = brContent.readLine())!=null){
										if( sCurrentLineContent != null && sCurrentLineContent.startsWith("X-Riferimento-Message-ID:")){
											sCurrentLineContent = brContent.readLine();
											if( sCurrentLineContent != null &&
											    (sCurrentLineContent.startsWith("X-TipoRicevuta:") ||
												 sCurrentLineContent.startsWith("MIME-Version:") ||
												 sCurrentLineContent.startsWith("Date:") ||
												 sCurrentLineContent.startsWith("Message-ID:") ||
												 sCurrentLineContent.startsWith("X-CrmlId:"))){
//												 System.out.println(messaggio.getMessageId()+" : ok");
											}else if(sCurrentLineContent.startsWith("")){
												sCurrentLineContent = brContent.readLine();
												if(!sCurrentLineContent.startsWith("----") && (tipoRicevuta!=null&&tipoRicevuta.equals("completa"))){
													nomeFile = "C:\\testPdf\\messaggiBonificati\\KO\\"+ sCurrentLine.replaceAll("<", "").replaceAll(">", "")+ "postacert.eml";
													f = new File(nomeFile);
//													fo = new FileOutputStream(f);
//													fo.write(contenuto.getBytes("ISO-8859-1"));
//													fo.flush();
//													fo.close();
//													System.out.println(messaggio.getMessageId()+" : ko");
												}
											}
											
										}
									}
									*/
//									System.out.println("posted date : "	+ messaggio.getPostedDate());
//									System.out.println("message ID : "	+ messaggio.getMessageId());

									new MessageMessaggioDTOBuilder().elabPostaCertEML(messaggio, m,	allegati, true);
									if (messaggio.getBodyHtml() != null	&& messaggio.getPostaCertEMLBody() == null) {
										messaggio.setPostaCertEMLBody(messaggio.getBodyHtml());
									}
									System.out.println("\n messaggio.getSubject(): \n"+messaggio.getSubject()+subject);
									System.out.println("\n messaggio.getBody(): \n"+messaggio.getBody());
									System.out.println("\n messaggio.getBodyHtml(): \n"+messaggio.getBodyHtml());
									System.out.println("\n messaggio.getPostaCertEMLBody(): \n"+messaggio.getPostaCertEMLBody());
									MessageMessaggioDTOBuilder.bonificaAllegati(allegati);
									// BASE64Encoder encoder = new
									// BASE64Encoder();
//									if(tipoRicevuta.equals("")){
										for (AllegatoDTO a : allegati) {
											System.out.println("Nome allegati: "+ a.getFileName()+ " Dimensione : "+ a.getFileSize());
											 f = new File("C:\\testPdf\\messaggiBonificati\\"+a.getFileName());
											 try{
											 FileOutputStream fo1 = new
											 FileOutputStream(f);
											 fo1.write(a.getFileData());
											 fo1.flush();
											 fo1.close();
											 }catch(Exception e){}
											String segnatura = "";
											 if(a.getFileName().equalsIgnoreCase("daticert.xml")){
												 segnatura = new String(a.getFileData());
//												 System.out.println(segnatura);
												 JAXBContext context = JAXBContext.newInstance(it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert.class);
												 it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert datiCertificati = new it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert();
												 datiCertificati = (it.eustema.inps.agentPecPei.business.pec.datiCert.Postacert) context.createUnmarshaller().unmarshal(new ByteArrayInputStream(segnatura.replaceAll("<!DOCTYPE Segnatura SYSTEM \"Segnatura.dtd\">", "").replaceAll("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "").getBytes()));
												 //System.out.println(datiCertificati.getDati().getRicevuta().getTipo());
												 tipoRicevuta = datiCertificati.getDati().getRicevuta().getTipo();
//												 System.out.println("Tipo Ricevuta per il messaggio :"+messaggio.getMessageId()+" � "+tipoRicevuta);
											 }
//									}
										// String destinatarioIterop =
										// segInterop.getIntestazione().getOrigine().getIndirizzoTelematico().getValue();
										// if(destinatarioIterop!=null &&
										// destinatarioIterop.equals(""))
										// destinatarioIterop=
										// segInterop.getIntestazione().getRisposta().getIndirizzoTelematico().getValue();
										//			    						
										// System.out.println("destinatarioIterop: "+destinatarioIterop+" valido : "+SendMailInteropCommand.validateEmail(destinatarioIterop));
										// }
									}
									MessaggiDAO messaggiDao = new MessaggiDAO();
									// try{
//									conPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI);
//									for(AllegatoDTO a : allegati){
//									 		a.setPostedDate(new Timestamp(messaggio.getPostedDate().getTime()));
//									 }
//									 messaggiDao.doInsertAllegati(conPecPei, messaggio.getMessageId(), "7066", allegati);
									
//									String updateMessage = "update messaggi set X_TipoRicevuta = ? where messageId = ? and codiceAOO = ? and accountPec = ? and X_TipoRicevuta = ''";
//									pstmt = connPecPei.prepareStatement(updateMessage);
//									pstmt.setString(1, tipoRicevuta);
//									pstmt.setString(2, messaggio.getMessageId());
//									pstmt.setString(3, codiceAOO);
//									pstmt.setString(4, accountPec);
//									int i = pstmt.executeUpdate();
//									System.out.println("recordi aggiornato : "+i);
									
									// }catch(Exception e){
									// e.printStackTrace();
									// }
									/*
									conPecPei.setAutoCommit(false);
									String updateBody = "update messaggi set PostacertEml_Body = ?, substato=? where messageId = ? and accountPEC = ? and codiceAOO = ?";
									pstmt = conPecPei.prepareStatement(updateBody);
									if(messaggio.getPostaCertEMLBody()!=null)
									 pstmt.setString(1, messaggio.getPostaCertEMLBody());
									else
									 pstmt.setString(1, messaggio.getBody());
												    				
									pstmt.setString(2, "NT");
									pstmt.setString(3, sCurrentLine);
									pstmt.setString(4, accountPec);
									pstmt.setString(5, codiceAOO);
									int r = pstmt.executeUpdate();
									if(r>1){
									  System.out.println("Trovato pi� di un message Id con il valore non corretto "+sCurrentLine);
									  conPecPei.rollback();
									}else{
									  System.out.println("Body Del messaggio Aggiornato "+sCurrentLine);
									  conPecPei.commit();
									}
									conPecPei.setAutoCommit(true);
									*/
								}
							} catch (Exception e) {
								System.err.println(e.getMessage());
							}
						}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null)
						br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception err) {
			err.printStackTrace();
		}
		return null;
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Lettura file RawContent";
	}
}
