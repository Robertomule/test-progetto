package it.eustema.inps.agentPecPei.util;

public enum AgentStatus {

	COMPLETED, STARTED, STOPPED, FAILED, DISABLED;
}
