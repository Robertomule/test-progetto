package it.eustema.inps.agentPecPei.util;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;



public class CrittografiaPasswordPEC {
	
		private static String TRANSFORMATION = "AES/CBC/PKCS5Padding";
		private static String ALGORITHM = "AES";
		private static Cipher _cipher;
		private static SecretKey _password;
		private static IvParameterSpec _IVParamSpec;
		// 16-byte private key
		private static byte[] IV;
		
		/**
		 * Constructor
		 * 
		 * @password Public key
		 */
		public CrittografiaPasswordPEC() {
			try {
				String password = "0123456789abcdef"; // 0123456789abcdef
				// Encode digest
//				MessageDigest digest;
//				digest = MessageDigest.getInstance(DIGEST);
				IV = "fedcba9876543210".getBytes("UTF-8");
				_password = new SecretKeySpec(password.getBytes("UTF-8"), ALGORITHM);
				// Initialize objects
				_cipher = Cipher.getInstance(TRANSFORMATION);
				_IVParamSpec = new IvParameterSpec(IV);
			} catch (Exception e) {
			} 
//			catch (NoSuchPaddingException e) {
//			}
		}

		/**
		 * Encryptor.
		 * 
		 * @text String to be encrypted
		 * @return Base64 encrypted text
		 */
		public String encrypt(String text) throws Exception{
			byte[] encryptedData;
			try {
				_cipher.init(Cipher.ENCRYPT_MODE, _password, _IVParamSpec);
				encryptedData = _cipher.doFinal(text.getBytes("UTF-8"));
			} catch (InvalidKeyException e) {
				return null;
			} catch (InvalidAlgorithmParameterException e) {
				return null;
			} catch (IllegalBlockSizeException e) {
				return null;
			} catch (BadPaddingException e) {
				return null;
			} catch (Exception e){
				return null;
			}
//			ByteBuffer b = ByteBuffer.wrap(encryptedData).order(ByteOrder.BIG_ENDIAN);
			BASE64Encoder encoder = new BASE64Encoder();
			return encoder.encode(encryptedData);
		}
		 
		/**
		 * Decryptor.
		 * 
		 * @text Base64 string to be decrypted
		 * @return decrypted text
		 */
		public String decrypt(String text) {
			BASE64Decoder decoder = new BASE64Decoder();

			try {
				_cipher.init(Cipher.DECRYPT_MODE, _password, _IVParamSpec);
				byte[] decodedValue = decoder.decodeBuffer(text);
				byte[] decryptedVal = _cipher.doFinal(decodedValue);
				return new String(decryptedVal);
			} catch (InvalidKeyException e) {
				return null;
			} catch (InvalidAlgorithmParameterException e) {
				return null;
			} catch (IllegalBlockSizeException e) {
				return null;
			} catch (BadPaddingException e) {
				return null;
			} catch (Exception e) {
				return null;
			}
		}
	
	private static final String ALGORITHMNEW = "3DES"; 
	private static final byte[] keyValue = new byte[] { 'P', '3', 'c', '$', '&', '$', 'P', 'e', 'i'};	
	
	private static Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, ALGORITHM);
		return key;
	}
	
	public static String encryptNew(String valueToEnc) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHMNEW);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encValue = c.doFinal(valueToEnc.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encValue);
		return encryptedValue;
	}

	public static String decryptNew(String encryptedValue) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGORITHMNEW);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	public static void main(String[] args) throws Exception {
	
		CrittografiaPasswordPEC crit = new CrittografiaPasswordPEC();
//		System.out.println("---------------------------------");
//		System.out.println(crit.encrypt("rAQLVphO"));
//		System.out.println("---------------------------------");
//		System.out.println(crit.encrypt("Password01"));
		
//		 password utente1: mtt8MynRPZj/ExIZ7G3dYQ==
		System.out.println("-------utente1----------------");
		System.out.println(crit.decrypt("mtt8MynRPZj/ExIZ7G3dYQ=="));
//		password utente2: 1E+F5qH2bYC1En+7Hwdhig==
		System.out.println("---------utente2------------");
		System.out.println(crit.decrypt("1E+F5qH2bYC1En+7Hwdhig=="));
		
		System.out.println("---------utente3------------");
		System.out.println(crit.decrypt("yttA1dxbDTEHSVpWhkhUKw=="));

		System.out.println("---------utente4------------");
		System.out.println(crit.decrypt("EHCv5Y80O82Q3A7x3NOBmA=="));
			
//         BASE64Encoder encoder = new BASE64Encoder();
//         BASE64Decoder decoder = new BASE64Decoder();
//		 String claveString = "P3c$&$Pei";
//
//		 byte[] claveTripleDES = claveString.getBytes();
//		 DESedeKeySpec deskeySpec = new DESedeKeySpec(claveTripleDES);
//		 SecretKeyFactory secretKeyFactory =   SecretKeyFactory.getInstance("DESede");
//		 SecretKey generateSecret = secretKeyFactory.generateSecret(deskeySpec);
//	     
//	     Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding"); 
//	     cipher.init(Cipher.ENCRYPT_MODE, generateSecret);
//	     byte[] encrypted = cipher.doFinal("hello".getBytes());
//	     String cifrata = encoder.encode(encrypted);
//	     System.out.println(cifrata);
	     
//	     Cipher cipherDec = Cipher.getInstance("DESede/CBC/PKCS5Padding"); 
//	     cipherDec.init(Cipher.DECRYPT_MODE, generateSecret);
//	     byte[] decrypt = cipherDec.doFinal(cifrata.getBytes());
//	     String decifrata = encoder.encode(decrypt);
//	     System.out.println(decifrata);
//		String enc = encryptNew("test");
//		System.out.println(enc);
//		String dec = decryptNew(enc);
//		System.out.println(dec);
//        System.out.println(decoder.decodeBuffer("MHIGCSsGAQQBgjdYA6BlMGMGCisGAQQBgjdYAwGgVTBTAgMCAAECAmYQAgIBAAQQAAAAAAAAAAAAAAAAAAAAAAQQVduORygZ6nWnJctw4gtmkQQgWn0G9LUH19SLKcP8PRht9yiQ0NsAGaRFAyJ7Msxlqvw="));
		
	}
	
}
