package it.eustema.inps.agentPecPei.util;

import it.eustema.inps.utility.ConfigProp;

import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class TripleDES {
	static BASE64Decoder decoder = new BASE64Decoder();
	static BASE64Encoder encoder = new BASE64Encoder();

//	static final String password = "prova1235456";
	static final String codifica = "UTF-8"; 
	
    public static void main(String[] args) throws Exception {
//    	String text = "1213123781098KIOJOPIJHPOIJciao come stai, prova codifica/decoficicaa�����";
    	String text = "WOR2O0ID";
    	boolean abilitaBase64 = true;
//    	byte[] codedtext = "ZhE0+iIPCRmyhfm9+c5EHzAqRFF7zIfgFVBhKbaS9BGb+e0kACqSjuHfa2nTyp6joRvPG+nt/vL9pb1B0r/Zkw==".getBytes();
    	byte[] codedtext = encoder.encode(new TripleDES().encrypt(text)).getBytes();
    	String codetextEncoder = new String(codedtext);
    	System.out.println("codetextEncoder: "+codetextEncoder);
    	byte[] passwordPec;
    	if(abilitaBase64){
    		passwordPec = decoder.decodeBuffer(codetextEncoder);
    	}else{
    		passwordPec = codedtext;
    	}
    	String decodedtext = new TripleDES().decrypt(codetextEncoder);
    	System.out.println("codetextDecoder: "+decodedtext);
    }

    public byte[] encrypt(String message) throws Exception {
    	final byte[] digestOfPassword = ConfigProp.passwordPec.getBytes(codifica);
    	final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
    	final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
    	final IvParameterSpec iv = new IvParameterSpec(ConfigProp.passwordIVPec.getBytes(codifica));
    	final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
    	cipher.init(Cipher.ENCRYPT_MODE, key, iv);
    	final byte[] plainTextBytes = message.getBytes(codifica);
    	final byte[] cipherText = cipher.doFinal(plainTextBytes);
    	return cipherText;
    }

    public String decrypt(String stringMessage) throws Exception {
    	final byte[] digestOfPassword = ConfigProp.passwordPec.getBytes(codifica);//
    	final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
    	byte[] message = decoder.decodeBuffer(stringMessage);
    	final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
    	final IvParameterSpec iv = new IvParameterSpec(ConfigProp.passwordIVPec.getBytes(codifica));
    	final Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
    	decipher.init(Cipher.DECRYPT_MODE, key, iv);
    	final byte[] plainText = decipher.doFinal(message);
    	return new String(plainText,codifica);
    }
}