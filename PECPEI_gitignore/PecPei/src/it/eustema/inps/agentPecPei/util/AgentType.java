package it.eustema.inps.agentPecPei.util;

public enum AgentType {

    PEC(1),
    PEI(2),
    HERMES(3),
    PEC_PEI(4),
    PEC_HERMES(5),
    PEI_HERMES(6),
    PEC_PEI_HERMES(7),
    VERIFICAPEC(8)
    ;

    private int typeId;

    AgentType(int typeId) {
        this.typeId = typeId;
    }

    public int getTypeId() { 
        return typeId;
    }
}
