package it.eustema.inps.agentPecPei.util;

import it.eustema.inps.utility.ConfigProp;

public interface DebugConstants {

	public static final boolean ENABLE_DEBUG_CONSTANTS = false;
	public static final boolean ENABLE_DEBUG_CONSTANTS_DESTINATARI = ConfigProp.destinatariTest;
	public static final String PROTOCOLLO_AOO = "0040";

//	Password Cifrata EHCv5Y80O82Q3A7x3NOBmA==
//	public static final String ACCOUNT_PEC_USER_NAME = "utente04@postacert.inps.gov.it";
//	public static final String ACCOUNT_PEC_USER_PASSWORD = "EHCv5Y80O82Q3A7x3NOBmA==";

//	 Password Cifrata yttA1dxbDTEHSVpWhkhUKw==
	public static final String ACCOUNT_PEC_USER_NAME = "utente03@postacert.inps.gov.it";
	public static final String ACCOUNT_PEC_USER_PASSWORD = "yttA1dxbDTEHSVpWhkhUKw==";
	
}
