package it.eustema.inps.agentPecPei.util;

public interface Constants {

	public static final String AGENT_ENABLED = "1";
	public static final String AGENT_DISABLED = "0";
	
	public static final String VERSO_MSG_USCENTE = "U";
	public static final String VERSO_MSG_ENTRANTE = "E";
	
	public static final String STATO_BOZZA = "BZ";
	public static final String STATO_MSG_INVIATO = "IN";
	public static final String STATO_MSG_DAINVIARE = "DI";
	public static final String STATO_MSG_RICEVUTO = "RI";
	public static final String STATO_MSG_DA_PROTOCOLLARE = "DP";
	public static final String STATO_MSG_DA_PROTOCOLLARE_TEST = "TDP";
	
	public static final String SUBSTATO_INVIO_NOTIFICA = "DN";
	public static final String SUBSTATO_NOTIFICATO = "NT";
	
	public static final String DB_PECPEI = "PECPEI";
	public static final String DB_VERIFICAPEC = "VERIFICAPEC";
	public static final String DB_HERMES = "HERMES";
	public static final String DB_COMMON = "COMMON";
	public static final String DB_PECPEI_PASSW = "PECPEI_PASSW";
	
	//Stato messaggio ricevuto
	public static final String STATO_RICEVUTO = "RI";
	public static final String STATO_INVIATO = "IN";
	public static final String TIPOLOGIADB_XTRASPORTO = "X-Trasporto";
	
	public static final String Posta_Certificata = "posta-certificata";
	
	public static final String AUTORE_COMPOSITORE_PST = "provaPOPexENTI-PP";
	public static final String nonAccettazione = "non-accettazione";
	public static final String erroreConsegna = "errore-consegna";
	public static final String anomaliaMessaggio = "ANOMALIA MESSAGGIO:";
	public static final String avvenutaConsegna = "avvenuta-consegna";
	
}
