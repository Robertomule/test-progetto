package it.eustema.inps.agentPecPei.util;
import it.eustema.inps.agentPecPei.business.Command;
import it.eustema.inps.agentPecPei.business.pec.builder.MessageMessaggioDTOBuilder;
import it.eustema.inps.agentPecPei.dao.MessaggiDAO;
import it.eustema.inps.agentPecPei.dto.AllegatoDTO;
import it.eustema.inps.agentPecPei.dto.MessaggioDTO;
import it.eustema.inps.agentPecPei.exception.BusinessException;
import it.eustema.inps.agentPecPei.manager.CommandExecutor;
import it.eustema.inps.agentPecPei.manager.CommandExecutorManager;
import it.eustema.inps.utility.ConfigProp;
import it.eustema.inps.utility.DBConnectionManager;
import it.eustema.inps.utility.logging.LoggingConfigurator;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;

import org.apache.log4j.Logger;

import sun.misc.BASE64Encoder;
public class CaricamentoPassw implements Command {

		private static Logger log = Logger.getLogger(CaricamentoPassw.class);
		private CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
		private MessaggiDAO messaggiDAO = new MessaggiDAO();
		int depth = -1;
		Connection connPecPei = null;
//		private String codAOO;
//		private String accountPEC;
//		private String nomeFile;
		static {
	        System.out.println("CaricamentoPassw :: esegue blocco di inizializzazione");
			//blocco eseguito una volta al caricamento della classe
	        try {
				// carica i parametri dal file di cfg
				//new ConfigProp(cfgFileName);
				new ConfigProp().init();
				// inizializzazione configurazione del logging
				new LoggingConfigurator().init(ConfigProp.logPathFileName);
			} catch (Exception e) {
				System.err.println("" + e.getMessage());
			}        
	    }

		public CaricamentoPassw( ){
			try {
				connPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI_PASSW);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			this.codAOO = codAOO;
//			this.nomeFile = nomeFile;
//			this.accountPEC = accountPEC;
		}
		
		
		
		public static void main(String[] args){
//			String from = "\"Per conto di: cch39867@pec.carabinieri.it\" <posta-certificata@cert.actalis.it>";
//			System.out.println(from.substring(0,from.lastIndexOf("\"")).replaceAll("<", "").replaceAll(">", "").replaceAll("\"", "").replaceAll("Per conto di:", "").trim());
			try {
				CommandExecutor executor = CommandExecutorManager.getInstance().getExecutor();
				executor.setCommand( new CaricamentoPassw());
				executor.executeCommand();
			} catch (BusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		@Override
		public Object execute() throws BusinessException {
			// TODO Auto-generated method stub
			try {
				BufferedReader br = null;
				BASE64Encoder encoder = new BASE64Encoder();
				try {
					
					String accountLoginName;
					String accountPassword;
					String interlinea;
					
					br = new BufferedReader(new FileReader("C:\\config\\AgentPecPei\\gestioneDomino\\testIOutput.txt"));
					while(br.ready()){
						interlinea = br.readLine();
						accountLoginName = br.readLine();
						accountPassword = br.readLine();
						String updatePassw = "update AccountsPEC set PasswordAccountBatch = ? where AccountLoginName = ? ";
						PreparedStatement pstmt = null;
						try{
							Connection conPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI_PASSW);							
							pstmt = conPecPei.prepareStatement(updatePassw);
							pstmt.setString(1, encoder.encode(new TripleDES().encrypt(accountPassword)));
							pstmt.setString(2, accountLoginName);
							int i = pstmt.executeUpdate();
							if(i>0){
								System.out.println(accountLoginName+"--> aggiornato");
							}else{
								System.out.println(accountLoginName+"--> non aggiornato");
							}
//							System.out.println(accountLoginName + " password aggiornata :"+encoder.encode(new TripleDES().encrypt(accountPassword)));
						}catch(Exception e){
							e.printStackTrace();
						}finally{
							if(connPecPei!=null)
								connPecPei.close();
							if(pstmt!=null)
								pstmt.close();
						}
						
					}
					
		 
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						if (br != null)br.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}

	    } catch (Exception err) {
	            err.printStackTrace();
	    }
			return null;

		}
		
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return "Lettura file RawContent";
		}
		
		public static void estraiPassword(){
			String queryPassw = "select AccountLoginName, AOO, AccountPassword, DtUltimaRicezione FROM AccountsPEC order by DtUltimaRicezione desc";
			PreparedStatement pstmt = null;
			ResultSet rs=null;
			try{
				Connection conPecPei = DBConnectionManager.getInstance().getConnection(Constants.DB_PECPEI_PASSW);
				StringBuilder sb = new StringBuilder("");
				pstmt = conPecPei.prepareStatement(queryPassw);
				rs = pstmt.executeQuery();
				if(rs!=null){
					while(rs.next()){
						sb.append(rs.getString("AccountLoginName")+"\n");
						sb.append(rs.getString("AccountPassword")+"\n");
					}
				}
				FileOutputStream fos = new FileOutputStream("C:\\workspaceAgentPecPei\\PecPei\\passwordAccount.txt");
				BufferedOutputStream bo = new BufferedOutputStream(fos);
				bo.write(sb.toString().getBytes());
				bo.close();
				bo.flush();
			}catch(Exception e){
				e.printStackTrace();
			}
		}

	}


