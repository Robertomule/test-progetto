package it.eustema.inps.agentPecPei;

import it.eustema.inps.agentPecPei.invioNotificheMailInterop.InvioMailInterop_BusinessManager;
import it.eustema.inps.agentPecPei.manager.GestioneDestinatari_BusinessManager;
import it.eustema.inps.agentPecPei.manager.GestioneHermes_BusinessManager;
import it.eustema.inps.agentPecPei.manager.GestionePEC_BusinessManager;
import it.eustema.inps.agentPecPei.manager.GestionePEC_BusinessManagerTest;
import it.eustema.inps.agentPecPei.manager.GestionePEI_BusinessManager;
import it.eustema.inps.agentPecPei.manager.GestioneProtocollazionePEC_BusinessManager;
import it.eustema.inps.agentPecPei.manager.RecuperaMessaggiDaProtocollarePEI;
import it.eustema.inps.agentPecPei.util.AgentType;
import it.inps.agentPec.verificaPec.manager.VerificaCasellePec_Manager;
import it.inps.agentPec.verificaPec.manager.VerificaPec_Manager;

import java.awt.*;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

public class AgentCallable implements Callable<String> {
	private static Logger log = Logger.getLogger(AgentCallable.class);
	
	
	private AgentType agentType;
	private GestionePEC_BusinessManager pecManager;
	private GestionePEI_BusinessManager peiManager;
	private GestioneHermes_BusinessManager hermesMgr;
	private GestioneDestinatari_BusinessManager destinatariMgr;
	private InvioMailInterop_BusinessManager invioMailInterop;
	private VerificaPec_Manager verificaPecManager;
	private RecuperaMessaggiDaProtocollarePEI recuperaMessaggiDaProtocollarePEI;
	private GestioneProtocollazionePEC_BusinessManager recuperaMessaggiDaProtocollarePEC;
	private VerificaCasellePec_Manager verificaCasellePecManager;
	
	private int idBatch;
	
	public AgentCallable(AgentType agentType, int batchId){
		this.agentType = agentType;
		this.idBatch = batchId;
	}
	
	@Override
	public String call() {
		
		// invece di scegliere in base ad uno switch-case eseguire una getInstance dell'oggetto
		// che serve e chiamare il metodo elabora
		System.out.println("AgentRunnable :: call() :: agentType="+agentType);
		log.debug("AgentRunnable :: call :: agentType="+agentType);
		switch(agentType) {
			case PEC:
				try {
					System.out.println("AgentRunnable :: switch() :: Caso PEC");
					GestionePEC_BusinessManager pecManager = new GestionePEC_BusinessManager(idBatch);
					System.out.println("AgentRunnable :: pecManager.elabora() :: Entro nel GestionePEC_BusinessManager");
					pecManager.elabora();
					verificaCasellePecManager = new VerificaCasellePec_Manager(idBatch);
					verificaCasellePecManager.elabora();
					//Da utilzzare per le pec di test
					if(idBatch == 1){
						log.info("Esecuzione del batch "+idBatch+" lavoro le caselle di test");
						GestionePEC_BusinessManagerTest pecManagerTest = new GestionePEC_BusinessManagerTest();
						pecManagerTest.elabora();
					}
				} catch (Exception e) {
					log.error( e.getMessage() );
				}					
				break;
			case PEI:
				System.out.println("AgentRunnable :: switch() :: Caso PEI");
				log.debug("crea un nuovo GestionePEI_BusinessManager");
			try {
					//solo il primo batch lavora le PEI
					if(idBatch == -2){
						System.out.println("Esecuzione del Batch, "+idBatch+" lavoro le Pei");
						log.info("Esecuzione del Batch 1, lavoro le Pei");
						peiManager = new GestionePEI_BusinessManager();
						System.out.println("Entro nel 'GestionePEI_BusinessManager' ed elaboro");
						peiManager.elabora();
						System.out.println("Entro nel 'RecuperaMessaggiDaProtocollarePEI' ed elaboro");
						recuperaMessaggiDaProtocollarePEI = new RecuperaMessaggiDaProtocollarePEI();
						recuperaMessaggiDaProtocollarePEI.elabora();
					}else{
						System.out.println("Esecuzione del Batch "+idBatch+", NON lavoro le Pei");
						log.info("Esecuzione del Batch "+idBatch+", NON lavoro le Pei");
					}
//					destinatariMgr = new GestioneDestinatari_BusinessManager();
//					destinatariMgr.elabora();
//					invioMailInterop = new InvioMailInterop_BusinessManager();
//					invioMailInterop.elabora();
				} catch (Exception e) {
					log.error( e.getMessage() );
				}							
				break;
			case HERMES:	
				log.debug("crea un nuovo GestioneHermes_BusinessManager");
				try {
//					hermesMgr = new GestioneHermes_BusinessManager();
//					hermesMgr.elabora();					
				} catch (Exception e) {
					log.error( e.getMessage() );
				}				
				break;
				
			case VERIFICAPEC:	
				log.debug("crea un nuovo VerificaPec_Manager");
				try {
					verificaPecManager = new VerificaPec_Manager();
					verificaPecManager.elabora();					
				} catch (Exception e) {
					log.error( e.getMessage() );
				}				
				break;				
		}
		
		return "0";
	}
}