package wsProtocollo;
/**
 * SegnaturaAllegata.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class SegnaturaAllegata  implements java.io.Serializable {
    private java.lang.String segnatura;

    private java.lang.Integer idProtocollo;

    private org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes tipoCatena;

    private java.lang.String note;

    public SegnaturaAllegata() {
    }

    public SegnaturaAllegata(
           java.lang.String segnatura,
           java.lang.Integer idProtocollo,
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes tipoCatena,
           java.lang.String note) {
           this.segnatura = segnatura;
           this.idProtocollo = idProtocollo;
           this.tipoCatena = tipoCatena;
           this.note = note;
    }


    /**
     * Gets the segnatura value for this SegnaturaAllegata.
     * 
     * @return segnatura
     */
    public java.lang.String getSegnatura() {
        return segnatura;
    }


    /**
     * Sets the segnatura value for this SegnaturaAllegata.
     * 
     * @param segnatura
     */
    public void setSegnatura(java.lang.String segnatura) {
        this.segnatura = segnatura;
    }


    /**
     * Gets the idProtocollo value for this SegnaturaAllegata.
     * 
     * @return idProtocollo
     */
    public java.lang.Integer getIdProtocollo() {
        return idProtocollo;
    }


    /**
     * Sets the idProtocollo value for this SegnaturaAllegata.
     * 
     * @param idProtocollo
     */
    public void setIdProtocollo(java.lang.Integer idProtocollo) {
        this.idProtocollo = idProtocollo;
    }


    /**
     * Gets the tipoCatena value for this SegnaturaAllegata.
     * 
     * @return tipoCatena
     */
    public org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes getTipoCatena() {
        return tipoCatena;
    }


    /**
     * Sets the tipoCatena value for this SegnaturaAllegata.
     * 
     * @param tipoCatena
     */
    public void setTipoCatena(org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes tipoCatena) {
        this.tipoCatena = tipoCatena;
    }


    /**
     * Gets the note value for this SegnaturaAllegata.
     * 
     * @return note
     */
    public java.lang.String getNote() {
        return note;
    }


    /**
     * Sets the note value for this SegnaturaAllegata.
     * 
     * @param note
     */
    public void setNote(java.lang.String note) {
        this.note = note;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SegnaturaAllegata)) return false;
        SegnaturaAllegata other = (SegnaturaAllegata) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.segnatura==null && other.getSegnatura()==null) || 
             (this.segnatura!=null &&
              this.segnatura.equals(other.getSegnatura()))) &&
            ((this.idProtocollo==null && other.getIdProtocollo()==null) || 
             (this.idProtocollo!=null &&
              this.idProtocollo.equals(other.getIdProtocollo()))) &&
            ((this.tipoCatena==null && other.getTipoCatena()==null) || 
             (this.tipoCatena!=null &&
              this.tipoCatena.equals(other.getTipoCatena()))) &&
            ((this.note==null && other.getNote()==null) || 
             (this.note!=null &&
              this.note.equals(other.getNote())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSegnatura() != null) {
            _hashCode += getSegnatura().hashCode();
        }
        if (getIdProtocollo() != null) {
            _hashCode += getIdProtocollo().hashCode();
        }
        if (getTipoCatena() != null) {
            _hashCode += getTipoCatena().hashCode();
        }
        if (getNote() != null) {
            _hashCode += getNote().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SegnaturaAllegata.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "SegnaturaAllegata"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnatura");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Segnatura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idProtocollo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IdProtocollo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCatena");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TipoCatena"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnRelationShipTypes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("note");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
