package wsProtocollo;
/**
 * AllegatiCatenaDocumentale.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class AllegatiCatenaDocumentale  implements java.io.Serializable {
    private java.lang.Integer idProtocollo;

    private java.lang.String segnatura;

    private org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes relazioneConPadre;

    private AllegatiCatenaDocumentale[] allegati;

    public AllegatiCatenaDocumentale() {
    }

    public AllegatiCatenaDocumentale(
           java.lang.Integer idProtocollo,
           java.lang.String segnatura,
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes relazioneConPadre,
           AllegatiCatenaDocumentale[] allegati) {
           this.idProtocollo = idProtocollo;
           this.segnatura = segnatura;
           this.relazioneConPadre = relazioneConPadre;
           this.allegati = allegati;
    }


    /**
     * Gets the idProtocollo value for this AllegatiCatenaDocumentale.
     * 
     * @return idProtocollo
     */
    public java.lang.Integer getIdProtocollo() {
        return idProtocollo;
    }


    /**
     * Sets the idProtocollo value for this AllegatiCatenaDocumentale.
     * 
     * @param idProtocollo
     */
    public void setIdProtocollo(java.lang.Integer idProtocollo) {
        this.idProtocollo = idProtocollo;
    }


    /**
     * Gets the segnatura value for this AllegatiCatenaDocumentale.
     * 
     * @return segnatura
     */
    public java.lang.String getSegnatura() {
        return segnatura;
    }


    /**
     * Sets the segnatura value for this AllegatiCatenaDocumentale.
     * 
     * @param segnatura
     */
    public void setSegnatura(java.lang.String segnatura) {
        this.segnatura = segnatura;
    }


    /**
     * Gets the relazioneConPadre value for this AllegatiCatenaDocumentale.
     * 
     * @return relazioneConPadre
     */
    public org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes getRelazioneConPadre() {
        return relazioneConPadre;
    }


    /**
     * Sets the relazioneConPadre value for this AllegatiCatenaDocumentale.
     * 
     * @param relazioneConPadre
     */
    public void setRelazioneConPadre(org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRelationShipTypes relazioneConPadre) {
        this.relazioneConPadre = relazioneConPadre;
    }


    /**
     * Gets the allegati value for this AllegatiCatenaDocumentale.
     * 
     * @return allegati
     */
    public AllegatiCatenaDocumentale[] getAllegati() {
        return allegati;
    }


    /**
     * Sets the allegati value for this AllegatiCatenaDocumentale.
     * 
     * @param allegati
     */
    public void setAllegati(AllegatiCatenaDocumentale[] allegati) {
        this.allegati = allegati;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AllegatiCatenaDocumentale)) return false;
        AllegatiCatenaDocumentale other = (AllegatiCatenaDocumentale) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idProtocollo==null && other.getIdProtocollo()==null) || 
             (this.idProtocollo!=null &&
              this.idProtocollo.equals(other.getIdProtocollo()))) &&
            ((this.segnatura==null && other.getSegnatura()==null) || 
             (this.segnatura!=null &&
              this.segnatura.equals(other.getSegnatura()))) &&
            ((this.relazioneConPadre==null && other.getRelazioneConPadre()==null) || 
             (this.relazioneConPadre!=null &&
              this.relazioneConPadre.equals(other.getRelazioneConPadre()))) &&
            ((this.allegati==null && other.getAllegati()==null) || 
             (this.allegati!=null &&
              java.util.Arrays.equals(this.allegati, other.getAllegati())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdProtocollo() != null) {
            _hashCode += getIdProtocollo().hashCode();
        }
        if (getSegnatura() != null) {
            _hashCode += getSegnatura().hashCode();
        }
        if (getRelazioneConPadre() != null) {
            _hashCode += getRelazioneConPadre().hashCode();
        }
        if (getAllegati() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAllegati());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAllegati(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AllegatiCatenaDocumentale.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "AllegatiCatenaDocumentale"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idProtocollo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IdProtocollo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnatura");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Segnatura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relazioneConPadre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RelazioneConPadre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnRelationShipTypes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allegati");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Allegati"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "AllegatiCatenaDocumentale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "AllegatiCatenaDocumentale"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
