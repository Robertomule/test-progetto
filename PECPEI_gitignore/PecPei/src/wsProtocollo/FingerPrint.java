package wsProtocollo;
/**
 * FingerPrint.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class FingerPrint  implements java.io.Serializable {
    private java.lang.String algoritmo;

    private java.lang.String codifica;

    private java.lang.String impronta;

    public FingerPrint() {
    }

    public FingerPrint(
           java.lang.String algoritmo,
           java.lang.String codifica,
           java.lang.String impronta) {
           this.algoritmo = algoritmo;
           this.codifica = codifica;
           this.impronta = impronta;
    }


    /**
     * Gets the algoritmo value for this FingerPrint.
     * 
     * @return algoritmo
     */
    public java.lang.String getAlgoritmo() {
        return algoritmo;
    }


    /**
     * Sets the algoritmo value for this FingerPrint.
     * 
     * @param algoritmo
     */
    public void setAlgoritmo(java.lang.String algoritmo) {
        this.algoritmo = algoritmo;
    }


    /**
     * Gets the codifica value for this FingerPrint.
     * 
     * @return codifica
     */
    public java.lang.String getCodifica() {
        return codifica;
    }


    /**
     * Sets the codifica value for this FingerPrint.
     * 
     * @param codifica
     */
    public void setCodifica(java.lang.String codifica) {
        this.codifica = codifica;
    }


    /**
     * Gets the impronta value for this FingerPrint.
     * 
     * @return impronta
     */
    public java.lang.String getImpronta() {
        return impronta;
    }


    /**
     * Sets the impronta value for this FingerPrint.
     * 
     * @param impronta
     */
    public void setImpronta(java.lang.String impronta) {
        this.impronta = impronta;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FingerPrint)) return false;
        FingerPrint other = (FingerPrint) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.algoritmo==null && other.getAlgoritmo()==null) || 
             (this.algoritmo!=null &&
              this.algoritmo.equals(other.getAlgoritmo()))) &&
            ((this.codifica==null && other.getCodifica()==null) || 
             (this.codifica!=null &&
              this.codifica.equals(other.getCodifica()))) &&
            ((this.impronta==null && other.getImpronta()==null) || 
             (this.impronta!=null &&
              this.impronta.equals(other.getImpronta())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAlgoritmo() != null) {
            _hashCode += getAlgoritmo().hashCode();
        }
        if (getCodifica() != null) {
            _hashCode += getCodifica().hashCode();
        }
        if (getImpronta() != null) {
            _hashCode += getImpronta().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FingerPrint.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "FingerPrint"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("algoritmo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Algoritmo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codifica");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codifica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impronta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Impronta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
