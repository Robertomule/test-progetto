package wsProtocollo;
/**
 * Annullamento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Annullamento  implements java.io.Serializable {
    private Applicazione applicazione;

    private java.util.Calendar dataAnnullamento;

    private java.lang.String motivazione;

    private java.lang.String numeroDirettiva;

    private Utente utente;

    public Annullamento() {
    }

    public Annullamento(
           Applicazione applicazione,
           java.util.Calendar dataAnnullamento,
           java.lang.String motivazione,
           java.lang.String numeroDirettiva,
           Utente utente) {
           this.applicazione = applicazione;
           this.dataAnnullamento = dataAnnullamento;
           this.motivazione = motivazione;
           this.numeroDirettiva = numeroDirettiva;
           this.utente = utente;
    }


    /**
     * Gets the applicazione value for this Annullamento.
     * 
     * @return applicazione
     */
    public Applicazione getApplicazione() {
        return applicazione;
    }


    /**
     * Sets the applicazione value for this Annullamento.
     * 
     * @param applicazione
     */
    public void setApplicazione(Applicazione applicazione) {
        this.applicazione = applicazione;
    }


    /**
     * Gets the dataAnnullamento value for this Annullamento.
     * 
     * @return dataAnnullamento
     */
    public java.util.Calendar getDataAnnullamento() {
        return dataAnnullamento;
    }


    /**
     * Sets the dataAnnullamento value for this Annullamento.
     * 
     * @param dataAnnullamento
     */
    public void setDataAnnullamento(java.util.Calendar dataAnnullamento) {
        this.dataAnnullamento = dataAnnullamento;
    }


    /**
     * Gets the motivazione value for this Annullamento.
     * 
     * @return motivazione
     */
    public java.lang.String getMotivazione() {
        return motivazione;
    }


    /**
     * Sets the motivazione value for this Annullamento.
     * 
     * @param motivazione
     */
    public void setMotivazione(java.lang.String motivazione) {
        this.motivazione = motivazione;
    }


    /**
     * Gets the numeroDirettiva value for this Annullamento.
     * 
     * @return numeroDirettiva
     */
    public java.lang.String getNumeroDirettiva() {
        return numeroDirettiva;
    }


    /**
     * Sets the numeroDirettiva value for this Annullamento.
     * 
     * @param numeroDirettiva
     */
    public void setNumeroDirettiva(java.lang.String numeroDirettiva) {
        this.numeroDirettiva = numeroDirettiva;
    }


    /**
     * Gets the utente value for this Annullamento.
     * 
     * @return utente
     */
    public Utente getUtente() {
        return utente;
    }


    /**
     * Sets the utente value for this Annullamento.
     * 
     * @param utente
     */
    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Annullamento)) return false;
        Annullamento other = (Annullamento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.applicazione==null && other.getApplicazione()==null) || 
             (this.applicazione!=null &&
              this.applicazione.equals(other.getApplicazione()))) &&
            ((this.dataAnnullamento==null && other.getDataAnnullamento()==null) || 
             (this.dataAnnullamento!=null &&
              this.dataAnnullamento.equals(other.getDataAnnullamento()))) &&
            ((this.motivazione==null && other.getMotivazione()==null) || 
             (this.motivazione!=null &&
              this.motivazione.equals(other.getMotivazione()))) &&
            ((this.numeroDirettiva==null && other.getNumeroDirettiva()==null) || 
             (this.numeroDirettiva!=null &&
              this.numeroDirettiva.equals(other.getNumeroDirettiva()))) &&
            ((this.utente==null && other.getUtente()==null) || 
             (this.utente!=null &&
              this.utente.equals(other.getUtente())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApplicazione() != null) {
            _hashCode += getApplicazione().hashCode();
        }
        if (getDataAnnullamento() != null) {
            _hashCode += getDataAnnullamento().hashCode();
        }
        if (getMotivazione() != null) {
            _hashCode += getMotivazione().hashCode();
        }
        if (getNumeroDirettiva() != null) {
            _hashCode += getNumeroDirettiva().hashCode();
        }
        if (getUtente() != null) {
            _hashCode += getUtente().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Annullamento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Annullamento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("applicazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Applicazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Applicazione"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataAnnullamento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DataAnnullamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motivazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Motivazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroDirettiva");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumeroDirettiva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("utente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Utente"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "Utente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
