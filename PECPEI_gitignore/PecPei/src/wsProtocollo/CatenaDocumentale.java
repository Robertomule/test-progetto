package wsProtocollo;
/**
 * CatenaDocumentale.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class CatenaDocumentale  implements java.io.Serializable {
    private SegnaturaAllegata[] segnaturePrincipali;

    private SegnaturaAllegata[] segnatureAllegate;

    public CatenaDocumentale() {
    }

    public CatenaDocumentale(
           SegnaturaAllegata[] segnaturePrincipali,
           SegnaturaAllegata[] segnatureAllegate) {
           this.segnaturePrincipali = segnaturePrincipali;
           this.segnatureAllegate = segnatureAllegate;
    }


    /**
     * Gets the segnaturePrincipali value for this CatenaDocumentale.
     * 
     * @return segnaturePrincipali
     */
    public SegnaturaAllegata[] getSegnaturePrincipali() {
        return segnaturePrincipali;
    }


    /**
     * Sets the segnaturePrincipali value for this CatenaDocumentale.
     * 
     * @param segnaturePrincipali
     */
    public void setSegnaturePrincipali(SegnaturaAllegata[] segnaturePrincipali) {
        this.segnaturePrincipali = segnaturePrincipali;
    }


    /**
     * Gets the segnatureAllegate value for this CatenaDocumentale.
     * 
     * @return segnatureAllegate
     */
    public SegnaturaAllegata[] getSegnatureAllegate() {
        return segnatureAllegate;
    }


    /**
     * Sets the segnatureAllegate value for this CatenaDocumentale.
     * 
     * @param segnatureAllegate
     */
    public void setSegnatureAllegate(SegnaturaAllegata[] segnatureAllegate) {
        this.segnatureAllegate = segnatureAllegate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CatenaDocumentale)) return false;
        CatenaDocumentale other = (CatenaDocumentale) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.segnaturePrincipali==null && other.getSegnaturePrincipali()==null) || 
             (this.segnaturePrincipali!=null &&
              java.util.Arrays.equals(this.segnaturePrincipali, other.getSegnaturePrincipali()))) &&
            ((this.segnatureAllegate==null && other.getSegnatureAllegate()==null) || 
             (this.segnatureAllegate!=null &&
              java.util.Arrays.equals(this.segnatureAllegate, other.getSegnatureAllegate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSegnaturePrincipali() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegnaturePrincipali());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegnaturePrincipali(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSegnatureAllegate() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegnatureAllegate());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegnatureAllegate(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CatenaDocumentale.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "CatenaDocumentale"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnaturePrincipali");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SegnaturePrincipali"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "SegnaturaAllegata"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "SegnaturaAllegata"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segnatureAllegate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SegnatureAllegate"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "SegnaturaAllegata"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "SegnaturaAllegata"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
