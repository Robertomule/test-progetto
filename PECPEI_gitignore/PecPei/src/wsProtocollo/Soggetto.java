package wsProtocollo;
/**
 * Soggetto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Soggetto  implements java.io.Serializable {
    private java.lang.String nominativo;

    private java.lang.String codice;

    private java.lang.String codiceInterno;

    private java.lang.String email;

    private org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoSoggetto tipoSoggetto;

    private org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRuoloNelProcedimento ruoloNelProcedimento;

    public Soggetto() {
    }

    public Soggetto(
           java.lang.String nominativo,
           java.lang.String codice,
           java.lang.String codiceInterno,
           java.lang.String email,
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoSoggetto tipoSoggetto,
           org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRuoloNelProcedimento ruoloNelProcedimento) {
           this.nominativo = nominativo;
           this.codice = codice;
           this.codiceInterno = codiceInterno;
           this.email = email;
           this.tipoSoggetto = tipoSoggetto;
           this.ruoloNelProcedimento = ruoloNelProcedimento;
    }


    /**
     * Gets the nominativo value for this Soggetto.
     * 
     * @return nominativo
     */
    public java.lang.String getNominativo() {
        return nominativo;
    }


    /**
     * Sets the nominativo value for this Soggetto.
     * 
     * @param nominativo
     */
    public void setNominativo(java.lang.String nominativo) {
        this.nominativo = nominativo;
    }


    /**
     * Gets the codice value for this Soggetto.
     * 
     * @return codice
     */
    public java.lang.String getCodice() {
        return codice;
    }


    /**
     * Sets the codice value for this Soggetto.
     * 
     * @param codice
     */
    public void setCodice(java.lang.String codice) {
        this.codice = codice;
    }


    /**
     * Gets the codiceInterno value for this Soggetto.
     * 
     * @return codiceInterno
     */
    public java.lang.String getCodiceInterno() {
        return codiceInterno;
    }


    /**
     * Sets the codiceInterno value for this Soggetto.
     * 
     * @param codiceInterno
     */
    public void setCodiceInterno(java.lang.String codiceInterno) {
        this.codiceInterno = codiceInterno;
    }


    /**
     * Gets the email value for this Soggetto.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this Soggetto.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the tipoSoggetto value for this Soggetto.
     * 
     * @return tipoSoggetto
     */
    public org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoSoggetto getTipoSoggetto() {
        return tipoSoggetto;
    }


    /**
     * Sets the tipoSoggetto value for this Soggetto.
     * 
     * @param tipoSoggetto
     */
    public void setTipoSoggetto(org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnTipoSoggetto tipoSoggetto) {
        this.tipoSoggetto = tipoSoggetto;
    }


    /**
     * Gets the ruoloNelProcedimento value for this Soggetto.
     * 
     * @return ruoloNelProcedimento
     */
    public org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRuoloNelProcedimento getRuoloNelProcedimento() {
        return ruoloNelProcedimento;
    }


    /**
     * Sets the ruoloNelProcedimento value for this Soggetto.
     * 
     * @param ruoloNelProcedimento
     */
    public void setRuoloNelProcedimento(org.datacontract.schemas._2004._07.WSGDA_BLL_Enumerations.EnRuoloNelProcedimento ruoloNelProcedimento) {
        this.ruoloNelProcedimento = ruoloNelProcedimento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Soggetto)) return false;
        Soggetto other = (Soggetto) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nominativo==null && other.getNominativo()==null) || 
             (this.nominativo!=null &&
              this.nominativo.equals(other.getNominativo()))) &&
            ((this.codice==null && other.getCodice()==null) || 
             (this.codice!=null &&
              this.codice.equals(other.getCodice()))) &&
            ((this.codiceInterno==null && other.getCodiceInterno()==null) || 
             (this.codiceInterno!=null &&
              this.codiceInterno.equals(other.getCodiceInterno()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.tipoSoggetto==null && other.getTipoSoggetto()==null) || 
             (this.tipoSoggetto!=null &&
              this.tipoSoggetto.equals(other.getTipoSoggetto()))) &&
            ((this.ruoloNelProcedimento==null && other.getRuoloNelProcedimento()==null) || 
             (this.ruoloNelProcedimento!=null &&
              this.ruoloNelProcedimento.equals(other.getRuoloNelProcedimento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNominativo() != null) {
            _hashCode += getNominativo().hashCode();
        }
        if (getCodice() != null) {
            _hashCode += getCodice().hashCode();
        }
        if (getCodiceInterno() != null) {
            _hashCode += getCodiceInterno().hashCode();
        }
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        if (getTipoSoggetto() != null) {
            _hashCode += getTipoSoggetto().hashCode();
        }
        if (getRuoloNelProcedimento() != null) {
            _hashCode += getRuoloNelProcedimento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Soggetto.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Soggetto"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nominativo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Nominativo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceInterno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceInterno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoSoggetto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TipoSoggetto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnTipoSoggetto"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruoloNelProcedimento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RuoloNelProcedimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WSGDA_BLL.Enumerations", "EnRuoloNelProcedimento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
