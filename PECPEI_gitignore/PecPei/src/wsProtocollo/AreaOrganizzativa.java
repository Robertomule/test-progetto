package wsProtocollo;
/**
 * AreaOrganizzativa.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class AreaOrganizzativa  extends OrganizationalAreaExt  implements java.io.Serializable {
    private java.lang.String accountCode;

    private java.lang.String codice6Cifre;

    private java.lang.String codiceLegale;

    private java.lang.String codiceOA;

    private java.lang.String complessita;

    private java.lang.Boolean disabilitato;

    private java.lang.String EMail;

    private java.lang.String organizzazione;

    private java.lang.String tipoOA;

    private java.util.Calendar validaAl;

    private java.util.Calendar validaDal;

    public AreaOrganizzativa() {
    }

    public AreaOrganizzativa(
           java.lang.String codice,
           java.lang.String descrizione,
           java.lang.String accountCode,
           java.lang.String codice6Cifre,
           java.lang.String codiceLegale,
           java.lang.String codiceOA,
           java.lang.String complessita,
           java.lang.Boolean disabilitato,
           java.lang.String EMail,
           java.lang.String organizzazione,
           java.lang.String tipoOA,
           java.util.Calendar validaAl,
           java.util.Calendar validaDal) {
        super(
            codice,
            descrizione);
        this.accountCode = accountCode;
        this.codice6Cifre = codice6Cifre;
        this.codiceLegale = codiceLegale;
        this.codiceOA = codiceOA;
        this.complessita = complessita;
        this.disabilitato = disabilitato;
        this.EMail = EMail;
        this.organizzazione = organizzazione;
        this.tipoOA = tipoOA;
        this.validaAl = validaAl;
        this.validaDal = validaDal;
    }


    /**
     * Gets the accountCode value for this AreaOrganizzativa.
     * 
     * @return accountCode
     */
    public java.lang.String getAccountCode() {
        return accountCode;
    }


    /**
     * Sets the accountCode value for this AreaOrganizzativa.
     * 
     * @param accountCode
     */
    public void setAccountCode(java.lang.String accountCode) {
        this.accountCode = accountCode;
    }


    /**
     * Gets the codice6Cifre value for this AreaOrganizzativa.
     * 
     * @return codice6Cifre
     */
    public java.lang.String getCodice6Cifre() {
        return codice6Cifre;
    }


    /**
     * Sets the codice6Cifre value for this AreaOrganizzativa.
     * 
     * @param codice6Cifre
     */
    public void setCodice6Cifre(java.lang.String codice6Cifre) {
        this.codice6Cifre = codice6Cifre;
    }


    /**
     * Gets the codiceLegale value for this AreaOrganizzativa.
     * 
     * @return codiceLegale
     */
    public java.lang.String getCodiceLegale() {
        return codiceLegale;
    }


    /**
     * Sets the codiceLegale value for this AreaOrganizzativa.
     * 
     * @param codiceLegale
     */
    public void setCodiceLegale(java.lang.String codiceLegale) {
        this.codiceLegale = codiceLegale;
    }


    /**
     * Gets the codiceOA value for this AreaOrganizzativa.
     * 
     * @return codiceOA
     */
    public java.lang.String getCodiceOA() {
        return codiceOA;
    }


    /**
     * Sets the codiceOA value for this AreaOrganizzativa.
     * 
     * @param codiceOA
     */
    public void setCodiceOA(java.lang.String codiceOA) {
        this.codiceOA = codiceOA;
    }


    /**
     * Gets the complessita value for this AreaOrganizzativa.
     * 
     * @return complessita
     */
    public java.lang.String getComplessita() {
        return complessita;
    }


    /**
     * Sets the complessita value for this AreaOrganizzativa.
     * 
     * @param complessita
     */
    public void setComplessita(java.lang.String complessita) {
        this.complessita = complessita;
    }


    /**
     * Gets the disabilitato value for this AreaOrganizzativa.
     * 
     * @return disabilitato
     */
    public java.lang.Boolean getDisabilitato() {
        return disabilitato;
    }


    /**
     * Sets the disabilitato value for this AreaOrganizzativa.
     * 
     * @param disabilitato
     */
    public void setDisabilitato(java.lang.Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }


    /**
     * Gets the EMail value for this AreaOrganizzativa.
     * 
     * @return EMail
     */
    public java.lang.String getEMail() {
        return EMail;
    }


    /**
     * Sets the EMail value for this AreaOrganizzativa.
     * 
     * @param EMail
     */
    public void setEMail(java.lang.String EMail) {
        this.EMail = EMail;
    }


    /**
     * Gets the organizzazione value for this AreaOrganizzativa.
     * 
     * @return organizzazione
     */
    public java.lang.String getOrganizzazione() {
        return organizzazione;
    }


    /**
     * Sets the organizzazione value for this AreaOrganizzativa.
     * 
     * @param organizzazione
     */
    public void setOrganizzazione(java.lang.String organizzazione) {
        this.organizzazione = organizzazione;
    }


    /**
     * Gets the tipoOA value for this AreaOrganizzativa.
     * 
     * @return tipoOA
     */
    public java.lang.String getTipoOA() {
        return tipoOA;
    }


    /**
     * Sets the tipoOA value for this AreaOrganizzativa.
     * 
     * @param tipoOA
     */
    public void setTipoOA(java.lang.String tipoOA) {
        this.tipoOA = tipoOA;
    }


    /**
     * Gets the validaAl value for this AreaOrganizzativa.
     * 
     * @return validaAl
     */
    public java.util.Calendar getValidaAl() {
        return validaAl;
    }


    /**
     * Sets the validaAl value for this AreaOrganizzativa.
     * 
     * @param validaAl
     */
    public void setValidaAl(java.util.Calendar validaAl) {
        this.validaAl = validaAl;
    }


    /**
     * Gets the validaDal value for this AreaOrganizzativa.
     * 
     * @return validaDal
     */
    public java.util.Calendar getValidaDal() {
        return validaDal;
    }


    /**
     * Sets the validaDal value for this AreaOrganizzativa.
     * 
     * @param validaDal
     */
    public void setValidaDal(java.util.Calendar validaDal) {
        this.validaDal = validaDal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AreaOrganizzativa)) return false;
        AreaOrganizzativa other = (AreaOrganizzativa) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.accountCode==null && other.getAccountCode()==null) || 
             (this.accountCode!=null &&
              this.accountCode.equals(other.getAccountCode()))) &&
            ((this.codice6Cifre==null && other.getCodice6Cifre()==null) || 
             (this.codice6Cifre!=null &&
              this.codice6Cifre.equals(other.getCodice6Cifre()))) &&
            ((this.codiceLegale==null && other.getCodiceLegale()==null) || 
             (this.codiceLegale!=null &&
              this.codiceLegale.equals(other.getCodiceLegale()))) &&
            ((this.codiceOA==null && other.getCodiceOA()==null) || 
             (this.codiceOA!=null &&
              this.codiceOA.equals(other.getCodiceOA()))) &&
            ((this.complessita==null && other.getComplessita()==null) || 
             (this.complessita!=null &&
              this.complessita.equals(other.getComplessita()))) &&
            ((this.disabilitato==null && other.getDisabilitato()==null) || 
             (this.disabilitato!=null &&
              this.disabilitato.equals(other.getDisabilitato()))) &&
            ((this.EMail==null && other.getEMail()==null) || 
             (this.EMail!=null &&
              this.EMail.equals(other.getEMail()))) &&
            ((this.organizzazione==null && other.getOrganizzazione()==null) || 
             (this.organizzazione!=null &&
              this.organizzazione.equals(other.getOrganizzazione()))) &&
            ((this.tipoOA==null && other.getTipoOA()==null) || 
             (this.tipoOA!=null &&
              this.tipoOA.equals(other.getTipoOA()))) &&
            ((this.validaAl==null && other.getValidaAl()==null) || 
             (this.validaAl!=null &&
              this.validaAl.equals(other.getValidaAl()))) &&
            ((this.validaDal==null && other.getValidaDal()==null) || 
             (this.validaDal!=null &&
              this.validaDal.equals(other.getValidaDal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAccountCode() != null) {
            _hashCode += getAccountCode().hashCode();
        }
        if (getCodice6Cifre() != null) {
            _hashCode += getCodice6Cifre().hashCode();
        }
        if (getCodiceLegale() != null) {
            _hashCode += getCodiceLegale().hashCode();
        }
        if (getCodiceOA() != null) {
            _hashCode += getCodiceOA().hashCode();
        }
        if (getComplessita() != null) {
            _hashCode += getComplessita().hashCode();
        }
        if (getDisabilitato() != null) {
            _hashCode += getDisabilitato().hashCode();
        }
        if (getEMail() != null) {
            _hashCode += getEMail().hashCode();
        }
        if (getOrganizzazione() != null) {
            _hashCode += getOrganizzazione().hashCode();
        }
        if (getTipoOA() != null) {
            _hashCode += getTipoOA().hashCode();
        }
        if (getValidaAl() != null) {
            _hashCode += getValidaAl().hashCode();
        }
        if (getValidaDal() != null) {
            _hashCode += getValidaDal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AreaOrganizzativa.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "AreaOrganizzativa"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccountCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice6Cifre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Codice6Cifre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceLegale");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceLegale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceOA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodiceOA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("complessita");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Complessita"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("disabilitato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Disabilitato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organizzazione");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Organizzazione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoOA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TipoOA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validaAl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ValidaAl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validaDal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ValidaDal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
