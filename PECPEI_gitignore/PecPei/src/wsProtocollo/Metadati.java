package wsProtocollo;
/**
 * Metadati.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

public class Metadati  implements java.io.Serializable {
    private java.lang.Integer nrModifiche;

    private OrganizationalAreaExt OArea;

    private ProtocolloEntrata protocolloEntrata;

    private ProtocolloUscita protocolloUscita;

    public Metadati() {
    }

    public Metadati(
           java.lang.Integer nrModifiche,
           OrganizationalAreaExt OArea,
           ProtocolloEntrata protocolloEntrata,
           ProtocolloUscita protocolloUscita) {
           this.nrModifiche = nrModifiche;
           this.OArea = OArea;
           this.protocolloEntrata = protocolloEntrata;
           this.protocolloUscita = protocolloUscita;
    }


    /**
     * Gets the nrModifiche value for this Metadati.
     * 
     * @return nrModifiche
     */
    public java.lang.Integer getNrModifiche() {
        return nrModifiche;
    }


    /**
     * Sets the nrModifiche value for this Metadati.
     * 
     * @param nrModifiche
     */
    public void setNrModifiche(java.lang.Integer nrModifiche) {
        this.nrModifiche = nrModifiche;
    }


    /**
     * Gets the OArea value for this Metadati.
     * 
     * @return OArea
     */
    public OrganizationalAreaExt getOArea() {
        return OArea;
    }


    /**
     * Sets the OArea value for this Metadati.
     * 
     * @param OArea
     */
    public void setOArea(OrganizationalAreaExt OArea) {
        this.OArea = OArea;
    }


    /**
     * Gets the protocolloEntrata value for this Metadati.
     * 
     * @return protocolloEntrata
     */
    public ProtocolloEntrata getProtocolloEntrata() {
        return protocolloEntrata;
    }


    /**
     * Sets the protocolloEntrata value for this Metadati.
     * 
     * @param protocolloEntrata
     */
    public void setProtocolloEntrata(ProtocolloEntrata protocolloEntrata) {
        this.protocolloEntrata = protocolloEntrata;
    }


    /**
     * Gets the protocolloUscita value for this Metadati.
     * 
     * @return protocolloUscita
     */
    public ProtocolloUscita getProtocolloUscita() {
        return protocolloUscita;
    }


    /**
     * Sets the protocolloUscita value for this Metadati.
     * 
     * @param protocolloUscita
     */
    public void setProtocolloUscita(ProtocolloUscita protocolloUscita) {
        this.protocolloUscita = protocolloUscita;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Metadati)) return false;
        Metadati other = (Metadati) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nrModifiche==null && other.getNrModifiche()==null) || 
             (this.nrModifiche!=null &&
              this.nrModifiche.equals(other.getNrModifiche()))) &&
            ((this.OArea==null && other.getOArea()==null) || 
             (this.OArea!=null &&
              this.OArea.equals(other.getOArea()))) &&
            ((this.protocolloEntrata==null && other.getProtocolloEntrata()==null) || 
             (this.protocolloEntrata!=null &&
              this.protocolloEntrata.equals(other.getProtocolloEntrata()))) &&
            ((this.protocolloUscita==null && other.getProtocolloUscita()==null) || 
             (this.protocolloUscita!=null &&
              this.protocolloUscita.equals(other.getProtocolloUscita())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNrModifiche() != null) {
            _hashCode += getNrModifiche().hashCode();
        }
        if (getOArea() != null) {
            _hashCode += getOArea().hashCode();
        }
        if (getProtocolloEntrata() != null) {
            _hashCode += getProtocolloEntrata().hashCode();
        }
        if (getProtocolloUscita() != null) {
            _hashCode += getProtocolloUscita().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Metadati.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("", "Metadati"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nrModifiche");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NrModifiche"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OArea");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "OrganizationalAreaExt"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolloEntrata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProtocolloEntrata"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "ProtocolloEntrata"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("protocolloUscita");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProtocolloUscita"));
        elemField.setXmlType(new javax.xml.namespace.QName("", "ProtocolloUscita"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
