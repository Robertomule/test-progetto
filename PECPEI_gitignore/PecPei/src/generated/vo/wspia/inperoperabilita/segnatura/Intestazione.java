//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.07.31 at 12:10:03 PM CEST 
//


package generated.vo.wspia.inperoperabilita.segnatura;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Identificatore"/>
 *         &lt;element ref="{}Classifica"/>
 *         &lt;element ref="{}Origine"/>
 *         &lt;element ref="{}Destinazione"/>
 *         &lt;element ref="{}PerConoscenza" maxOccurs="unbounded"/>
 *         &lt;element ref="{}Riservato"/>
 *         &lt;element ref="{}Risposta"/>
 *         &lt;element ref="{}Oggetto"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "identificatore",
    "origine",
    "destinazione",
    "perConoscenza",
    "riservato",
    "risposta",
    "oggetto",
    "classifica"
})
@XmlRootElement(name = "Intestazione")
public class Intestazione
    implements Serializable
{

    @XmlElement(name = "Identificatore", required = true)
    protected Identificatore identificatore;
    @XmlElement(name = "Origine", required = true)
    protected Origine origine;
    @XmlElement(name = "Destinazione", required = true)
    protected Destinazione destinazione;
    @XmlElement(name = "PerConoscenza", required = true)
    protected List<PerConoscenza> perConoscenza;
    @XmlElement(name = "Riservato", required = true)
    protected String riservato;
    @XmlElement(name = "Risposta", required = true)
    protected Risposta risposta;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "Classifica", required = true)
    protected Classifica classifica;

    /**
     * Gets the value of the identificatore property.
     * 
     * @return
     *     possible object is
     *     {@link Identificatore }
     *     
     */
    public Identificatore getIdentificatore() {
        return identificatore;
    }

    /**
     * Sets the value of the identificatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Identificatore }
     *     
     */
    public void setIdentificatore(Identificatore value) {
        this.identificatore = value;
    }

  
    public Origine getOrigine() {
        return origine;
    }

    /**
     * Sets the value of the origine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Origine }
     *     
     */
    public void setOrigine(Origine value) {
        this.origine = value;
    }

    /**
     * Gets the value of the destinazione property.
     * 
     * @return
     *     possible object is
     *     {@link Destinazione }
     *     
     */
    public Destinazione getDestinazione() {
        return destinazione;
    }

    /**
     * Sets the value of the destinazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Destinazione }
     *     
     */
    public void setDestinazione(Destinazione value) {
        this.destinazione = value;
    }

    /**
     * Gets the value of the perConoscenza property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the perConoscenza property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPerConoscenza().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PerConoscenza }
     * 
     * 
     */
    public List<PerConoscenza> getPerConoscenza() {
        if (perConoscenza == null) {
            perConoscenza = new ArrayList<PerConoscenza>();
        }
        return this.perConoscenza;
    }

    /**
     * Gets the value of the riservato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiservato() {
        return riservato;
    }

    /**
     * Sets the value of the riservato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiservato(String value) {
        this.riservato = value;
    }

    /**
     * Gets the value of the risposta property.
     * 
     * @return
     *     possible object is
     *     {@link Risposta }
     *     
     */
    public Risposta getRisposta() {
        return risposta;
    }

    /**
     * Sets the value of the risposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Risposta }
     *     
     */
    public void setRisposta(Risposta value) {
        this.risposta = value;
    }

    /**
     * Gets the value of the oggetto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Sets the value of the oggetto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }
    
    /**
     * Gets the value of the classifica property.
     * 
     * @return
     *     possible object is
     *     {@link Classifica }
     *     
     */
    public Classifica getClassifica() {
        return classifica;
    }

    /**
     * Sets the value of the classifica property.
     * 
     * @param value
     *     allowed object is
     *     {@link Classifica }
     *     
     */
    public void setClassifica(Classifica value) {
        this.classifica = value;
    }

    /**
     * Gets the value of the origine property.
     * 
     * @return
     *     possible object is
     *     {@link Origine }
     *     
     */

}
