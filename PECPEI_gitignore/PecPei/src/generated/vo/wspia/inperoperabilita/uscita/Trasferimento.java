//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.06.20 at 12:41:57 PM CEST 
//


package generated.vo.wspia.inperoperabilita.uscita;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}CodAmm"/>
 *         &lt;element ref="{}CodAoo"/>
 *         &lt;element ref="{}Classifica"/>
 *         &lt;element ref="{}ProcessoDestinatario"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codAmm",
    "codAoo",
    "classifica",
    "processoDestinatario"
})
@XmlRootElement(name = "Trasferimento")
public class Trasferimento
    implements Serializable
{

    @XmlElement(name = "CodAmm", required = true)
    protected String codAmm;
    @XmlElement(name = "CodAoo")
    protected short codAoo;
    @XmlElement(name = "Classifica", required = true)
    protected BigDecimal classifica;
    @XmlElement(name = "ProcessoDestinatario", required = true)
    protected ProcessoDestinatario processoDestinatario;

    /**
     * Gets the value of the codAmm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAmm() {
        return codAmm;
    }

    /**
     * Sets the value of the codAmm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAmm(String value) {
        this.codAmm = value;
    }

    /**
     * Gets the value of the codAoo property.
     * 
     */
    public short getCodAoo() {
        return codAoo;
    }

    /**
     * Sets the value of the codAoo property.
     * 
     */
    public void setCodAoo(short value) {
        this.codAoo = value;
    }

    /**
     * Gets the value of the classifica property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getClassifica() {
        return classifica;
    }

    /**
     * Sets the value of the classifica property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setClassifica(BigDecimal value) {
        this.classifica = value;
    }

    /**
     * Gets the value of the processoDestinatario property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessoDestinatario }
     *     
     */
    public ProcessoDestinatario getProcessoDestinatario() {
        return processoDestinatario;
    }

    /**
     * Sets the value of the processoDestinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessoDestinatario }
     *     
     */
    public void setProcessoDestinatario(ProcessoDestinatario value) {
        this.processoDestinatario = value;
    }

}
