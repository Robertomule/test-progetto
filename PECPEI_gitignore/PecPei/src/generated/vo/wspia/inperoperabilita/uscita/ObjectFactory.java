//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.06.20 at 12:41:57 PM CEST 
//


package generated.vo.wspia.inperoperabilita.uscita;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated.vo.wspia.inperoperabilita.uscita package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FlagProtocollo_QNAME = new QName("", "FlagProtocollo");
    private final static QName _CodAoo_QNAME = new QName("", "CodAoo");
    private final static QName _CodAmm_QNAME = new QName("", "CodAmm");
    private final static QName _DataArrivo_QNAME = new QName("", "DataArrivo");
    private final static QName _CategoriaProtocollo_QNAME = new QName("", "CategoriaProtocollo");
    private final static QName _Riservato_QNAME = new QName("", "Riservato");
    private final static QName _Classifica_QNAME = new QName("", "Classifica");
    private final static QName _Sensibile_QNAME = new QName("", "Sensibile");
    private final static QName _Codice_QNAME = new QName("", "Codice");
    private final static QName _Tipo_QNAME = new QName("", "Tipo");
    private final static QName _Oggetto_QNAME = new QName("", "Oggetto");
    private final static QName _EMail_QNAME = new QName("", "EMail");
    private final static QName _Nominativo_QNAME = new QName("", "Nominativo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated.vo.wspia.inperoperabilita.uscita
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Azione }
     * 
     */
    public Azione createAzione() {
        return new Azione();
    }

    /**
     * Create an instance of {@link ListaAllegati }
     * 
     */
    public ListaAllegati createListaAllegati() {
        return new ListaAllegati();
    }

    /**
     * Create an instance of {@link Trasferimento }
     * 
     */
    public Trasferimento createTrasferimento() {
        return new Trasferimento();
    }

    /**
     * Create an instance of {@link Mittente }
     * 
     */
    public Mittente createMittente() {
        return new Mittente();
    }

    /**
     * Create an instance of {@link Destinatario }
     * 
     */
    public Destinatario createDestinatario() {
        return new Destinatario();
    }

    /**
     * Create an instance of {@link ProtocolloMittente }
     * 
     */
    public ProtocolloMittente createProtocolloMittente() {
        return new ProtocolloMittente();
    }

    /**
     * Create an instance of {@link ListaDestinatari }
     * 
     */
    public ListaDestinatari createListaDestinatari() {
        return new ListaDestinatari();
    }

    /**
     * Create an instance of {@link ListaSoggettiAfferenti }
     * 
     */
    public ListaSoggettiAfferenti createListaSoggettiAfferenti() {
        return new ListaSoggettiAfferenti();
    }

    /**
     * Create an instance of {@link Documento }
     * 
     */
    public Documento createDocumento() {
        return new Documento();
    }

    /**
     * Create an instance of {@link SoggettoAfferente }
     * 
     */
    public SoggettoAfferente createSoggettoAfferente() {
        return new SoggettoAfferente();
    }

    /**
     * Create an instance of {@link ProcessoDestinatario }
     * 
     */
    public ProcessoDestinatario createProcessoDestinatario() {
        return new ProcessoDestinatario();
    }

    /**
     * Create an instance of {@link ListaDocumenti }
     * 
     */
    public ListaDocumenti createListaDocumenti() {
        return new ListaDocumenti();
    }

    /**
     * Create an instance of {@link DatiInvio }
     * 
     */
    public DatiInvio createDatiInvio() {
        return new DatiInvio();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FlagProtocollo")
    public JAXBElement<String> createFlagProtocollo(String value) {
        return new JAXBElement<String>(_FlagProtocollo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CodAoo")
    public JAXBElement<Short> createCodAoo(Short value) {
        return new JAXBElement<Short>(_CodAoo_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CodAmm")
    public JAXBElement<String> createCodAmm(String value) {
        return new JAXBElement<String>(_CodAmm_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DataArrivo")
    public JAXBElement<XMLGregorianCalendar> createDataArrivo(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataArrivo_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CategoriaProtocollo")
    public JAXBElement<String> createCategoriaProtocollo(String value) {
        return new JAXBElement<String>(_CategoriaProtocollo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Riservato")
    public JAXBElement<String> createRiservato(String value) {
        return new JAXBElement<String>(_Riservato_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Classifica")
    public JAXBElement<BigDecimal> createClassifica(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Classifica_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Sensibile")
    public JAXBElement<String> createSensibile(String value) {
        return new JAXBElement<String>(_Sensibile_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Codice")
    public JAXBElement<String> createCodice(String value) {
        return new JAXBElement<String>(_Codice_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Tipo")
    public JAXBElement<String> createTipo(String value) {
        return new JAXBElement<String>(_Tipo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Oggetto")
    public JAXBElement<String> createOggetto(String value) {
        return new JAXBElement<String>(_Oggetto_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EMail")
    public JAXBElement<String> createEMail(String value) {
        return new JAXBElement<String>(_EMail_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Nominativo")
    public JAXBElement<String> createNominativo(String value) {
        return new JAXBElement<String>(_Nominativo_QNAME, String.class, null, value);
    }

}
