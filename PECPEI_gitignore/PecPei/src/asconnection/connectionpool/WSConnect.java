package asconnection.connectionpool;


import it.eustema.inps.agentPecPei.util.Constants;
import it.eustema.inps.utility.DBConnectionManager;




public class WSConnect extends ConnectionPool 
{    

/**
 * Metodo WSConnect
 * Parametri:
 *  String nameDataSource
 *  String pagina 
 *
**/
	public WSConnect( String nameDataSource, String pagina )
	{
			try
			{				
			 
			
			/**
			 * Blocco if basato sulla condizione:
			 * myDataSource  diverso da  null
			**/
             
		      super.connection  = DBConnectionManager.getInstance().getConnection( Constants.DB_HERMES );
		      super.connection.setAutoCommit(false);		       		 
			  	         	               		     
                             
			}
			catch( Exception ex )
			{
				ex.printStackTrace();				
			}
		
	}		

}

