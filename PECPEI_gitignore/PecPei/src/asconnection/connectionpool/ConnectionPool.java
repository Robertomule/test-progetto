package asconnection.connectionpool;

import java.io.* ;
import java.sql.* ;
import java.util.*;

import asconnection.utility.*;


 

public abstract class ConnectionPool implements  Serializable{  
      
  public  String TIPOFLOAT = "TIPOFLOAT";	
  private   Statement stmt       =  null;
	
  protected java.sql.Connection connection;
		
  private Vector prestmt  = new Vector();
  private Vector callstmt = new Vector();
		
/**
 * Metodo prepareCall
 * Parametri:
 * String query
 *
**/
  public CallableStatement prepareCall(String query) throws Exception
  {
	CallableStatement cs = null;
	try
	{
  		
		cs = connection.prepareCall(query);
  		
	}
	catch(Exception exc)
	{
		exc.printStackTrace();
		throw exc;
	}
	return cs;
  }		

  /**Restituisce l'indice dell'ultimo PreparedStatement inserito*/
/**
 * Metodo getLastPreStmtIndex
 * Parametri:
 *
**/
  public int getLastPreStmtIndex()
   {
/**
 * Blocco if basato sulla condizione:
 * prestmt  diverso da  null
**/
	if(prestmt != null)
	   return( prestmt.size() - 1);
	return(-1);
   }

  /**Restituisce l'indice dell'ultimo CallableStatement inserito*/
/**
 * Metodo getLastCallStmtIndex
 * Parametri:
 *
**/
  public int getLastCallStmtIndex()
   {
/**
 * Verifico la seguente condizione:
 * callstmt  diverso da  null
**/
	if(callstmt != null)
	   return( callstmt.size() - 1);
	return(-1);
   }    	    
	
/**
 * Metodo prepareStatement
 * Parametri:
 * String str
 *
**/
  public void prepareStatement(String str) throws Exception 
   {		
	try
	  {
	   PreparedStatement psstmt = connection.prepareStatement(str);
	   prestmt.addElement(psstmt);
	  }		
	catch (Exception ex)
		{
		 try
		   {				
			connection.rollback();				
		   }			
		 catch (Exception exc)
			 {throw exc;}
			    
		 finally{throw ex;}	    
		} // end catch (Exception ex)
	} // end preparedStatement

  /**
   * Metodo per inizializzare un 
   * nuovo CallableStatement per 
   * questa ConnectioPool
   */  
/**
 * Metodo callableStmt
 * Parametri:
 *  String procedure 
 *
**/
  public void callableStmt( String procedure ) throws Exception
  {	
   try
	 {
	  CallableStatement clstmt = connection.prepareCall(procedure);
	  callstmt.addElement(clstmt);
	 }		
   catch (Exception ex)
	   {
		try
		  {				
		   connection.rollback();				
		  }  			   		
		catch (Exception exc)
			{throw exc;}
		    			
		finally{throw ex;}    
	   } // end catch (Exception ex)
  }
  
  /**
   * Metodo per eseguire uno dei CallableStatement di 
   * questa ConnectionPool.
   * Prende in ingresso 
   * i seguenti parametri:
   * 1) Elenco parametri ingresso
   * 2) indice del CallableStatement scelto
   * 3) numero di parametri in uscita 
   * 	utilizzati, oltre al
   * 	true/false
   */
/**
 * Metodo executeCallableStmt
 * Parametri:
 *  Vector param
 *  int index
 *  int sizeOutput 
 *
**/
  public Vector executeCallableStmt( Vector param, int index, int sizeOutput ) throws Exception
   {    
	CallableStatement cs = null;
	int sizeParam = param.size();
	Vector result = new Vector();
    
	try 
	  {
	   cs = (CallableStatement)callstmt.elementAt(index);//conn.prepareCall(query);
	        
	   cs.registerOutParameter(1, Types.CHAR);
	        
	   Enumeration myenum = param.elements();
	   int i=2;
	   Object column=null;
/**
 * Il ciclo si sviluppa su questa condizione: myenum.hasMoreElements()
 * Quando non � soddisfatta non entra nel ciclo
**/
	   while (myenum.hasMoreElements())
		   {
			column = myenum.nextElement();
/**
 * La condizione nel blocco if �:
 * column  diverso da  null
**/
			if (column != null)
				cs.setObject(i,column);
			else
			   {
				cs.setNull(i, Types.VARCHAR);
			   }
				i++;
		   } // end while
									
	   int out = 1;
/**
 * Il ciclo si sviluppa su questa condizione:  out <= sizeOutput 
 * Quando non � soddisfatta non entra nel ciclo
**/
	   while( out <= sizeOutput )
			{
			 cs.registerOutParameter(i, Types.CHAR);
			 i++;
			 out++;
			}			

		cs.execute();
		int x = sizeParam + 2;
		int xx = 1;
/**
 * Il ciclo viene impostato su questa condizione:  xx <= sizeOutput
 * Quando non � soddisfatta non entra nel ciclo
**/
		while( xx <= sizeOutput)
			{
			 Object obj = cs.getObject(x);
			 result.add(obj);
			 xx++;
			 x++;
			}
			
		return result;		
	   }      
	  catch (Exception ex) 
		  {
		   ex.printStackTrace();
		   try {
				connection.rollback();
			   }
			
		   catch (Exception exc) 
				{throw exc;}
			    
		   finally{throw ex;}		    
		  }
  }      
   
/**
 * Metodo executePrepareStatement
 * Parametri:
 * int index
 * Vector param
 *
**/
  public int executePrepareStatement(int index,Vector param) throws Exception 
   {						
	int ret=0;
	try
	  {
	   PreparedStatement myprepstmt = (PreparedStatement)prestmt.elementAt(index);
	   Enumeration myenum = param.elements();
	   int i=1;
	   Object column=null;
/**
 * Il ciclo si sviluppa su questa condizione: myenum.hasMoreElements()
 * Quando non � soddisfatta non entra nel ciclo
**/
	   while (myenum.hasMoreElements()) 
		   {
			column = myenum.nextElement();
/**
 * Se la seguente condizione � soddisfatta entro nell'if
 * column  diverso da  null  e   se non � column.toString().trim().equals("null")
**/
			if (column != null && !column.toString().trim().equals("null"))	
			 {			   
			  String type = column.toString();
/**
 * Entro nel blocco if se la seguente condizione � soddisfatta
 * type.equals(this.TIPOFLOAT)
**/
			  if (type.equals(this.TIPOFLOAT)) 
				  myprepstmt.setNull(i, Types.NUMERIC);
			  else	
				 {       	 
				  myprepstmt.setObject(i,column);									     			      
				 } 
			 } 
			else
			   {
				myprepstmt.setNull(i, Types.VARCHAR);			    
			   }
				
			i++;
		   } // end while		   	   	  	
	   ret = myprepstmt.executeUpdate();	   		
	  }				
	 catch (Exception ex) 
		 {	    		        
		  try
			{   		     		      
			 connection.rollback();	
			 throw ex;			
			}
			
		  catch (Exception exc) 
			  {throw exc;}				         		 
		 }
		 	 
	 return(ret);
	} // end executePrepareStatement
	
/**
 * Metodo executeQuery
 * Parametri:
 * String str
 *
**/
  public Vector executeQuery(String str) throws Exception 
   {		
	ResultSet rs=null;
	Vector ret=new Vector(1000, 1000);
	try 
	  {
	   Object column=null;
	   stmt = connection.createStatement();
	   
    
	   rs = stmt.executeQuery(str);
 
	   ResultSetMetaData rsm = rs.getMetaData();
	   
	   int numcol = rsm.getColumnCount();

/**
 * Il ciclo si sviluppa su questa condizione: rs.next()
 * Quando non � soddisfatta non entra nel ciclo
**/
	   while (rs.next()) 
		   {
			Vector row = new Vector();
/**
 * La variabile su cui si basa il ciclo for � numcol 
 * Per controllarne ogni singolo elemento
**/
			for (int i=1;i<=numcol ;i++ ) 
			  {

			   column = rs.getObject(i);
			   row.addElement(column);
			  }
			ret.addElement(row);

		   }
	
	  }     
	 catch (Exception ex) 
		 {ex.printStackTrace();
			throw ex;}		
         
	 finally 
		   {
			try
			  {

			   rs.close();
			   stmt.close();
			   stmt=null;
			  }
			catch (Exception e) 
				{e.printStackTrace();
					throw e;}
		   }

		   		     
	 return ret;
	} // end executeQuery

/**
 * Metodo executeUp
 * Parametri:
 * String str
 *
**/
	public int executeUp(String str) throws Exception 
	   {		
		int rs = 0;		
		try 
		  {
		   Object column=null;
		   stmt = connection.createStatement();
	   
    
		   rs = stmt.executeUpdate(str); 		   
	
		  }     
		 catch (Exception ex) 
			 {ex.printStackTrace();
				throw ex;}		
         
		 finally 
			   {
				try
				  {				  
				   stmt.close();
				   stmt=null;
				  }
				catch (Exception e) 
					{e.printStackTrace();
						throw e;}
			   }		   		    
		 return rs;
		} // end executeQuery
/**
 * Metodo insert
 * Parametri:
 * Record record
 *
**/
  public int insert(Record record) throws Exception
   {		
	int ret=0;
	try
	  {
	   String insert = "insert into " + record.getTable() + " (";
	   String values  = "values( ";

	   Vector externalVect = record.getVectFieldValue();
	   Vector vectFieldValue = null;
	   int size = externalVect.size();
	   boolean existStatement= false;
/**
 * La variabile su cui si basa il ciclo for �  index 
 * Per controllarne ogni singolo elemento
**/
	   for (int index=0; index < size ; index++)
		 {
		  vectFieldValue = (Vector)externalVect.elementAt(index);
/**
 * Blocco if basato sulla condizione:
 * vectFieldValue.elementAt(1)  diverso da  null
**/
		  if (vectFieldValue.elementAt(1) != null)
		   {
/**
 * La condizione nel blocco if �:
 * existStatement
**/
			if(existStatement)
			 {
			  insert+=",";
			  values+=",";
			 }
			insert+=(String)vectFieldValue.elementAt(0);
/**
 * Blocco if basato sulla condizione:
 * vectFieldValue.elementAt(1) instanceof java.lang.String
**/
			if(vectFieldValue.elementAt(1) instanceof java.lang.String)
			   values+="'" + parseString((String)vectFieldValue.elementAt(1)) + "'";
			else
				values+=vectFieldValue.elementAt(1);
				existStatement = true;
		   }
		 } // end for
		insert += ") ";
		values += ")";
		insert += values;
	    
	   
                                        	    	            
/**
 * La condizione nel blocco if �:
 * existStatement
**/
		if (existStatement)
		 {                    		
		  ret = executeUpdate(insert);
		 }
		else
		   {}
		}
		
	catch(Exception exc){throw exc;}
		
	return(ret);
   }//END insert

/**
 * Metodo update
 * Parametri:
 * Record record
 *
**/
  public int update(Record record) throws Exception
   {		
	int ret=0;
	try
	  {              
	   String update = "update " + record.getTable() + " set ";

	   Vector externalVect = record.getVectFieldValue();
	   Vector vectFieldValue = null;
	   
	   int size = externalVect.size();
	   boolean existStatement= false;
	   
/**
 * La variabile su cui si basa il ciclo for �  index 
 * Per controllarne ogni singolo elemento
**/
	   for (int index=0; index < size ; index++)
		 {
		  vectFieldValue = (Vector)externalVect.elementAt(index);

/**
 * La condizione nel blocco if �:
 * existStatement
**/
		  if(existStatement)
		   {
			update+=", ";
		   }
            
/**
 * Se la seguente condizione � soddisfatta entro nell'if
 * vectFieldValue.elementAt(1) uguale a null
**/
		  if(vectFieldValue.elementAt(1)==null)
		   {
			update+=(String)vectFieldValue.elementAt(0) + "=NULL";            
		   }
/**
 * Verifico la seguente condizione:
 * vectFieldValue.elementAt(1) instanceof java.lang.String 
**/
		  else if(vectFieldValue.elementAt(1) instanceof java.lang.String )
			 {
			  update+=(String)vectFieldValue.elementAt(0) + "=" + "'" + parseString((String)vectFieldValue.elementAt(1))+ "'";
			 }
/**
 * La condizione nel blocco if �:
 *  vectFieldValue.elementAt(1) instanceof java.sql.Timestamp 
**/
		  else if( vectFieldValue.elementAt(1) instanceof java.sql.Timestamp )
			 {               
			  update+=(String)vectFieldValue.elementAt(0) + "=" + "'" + ((java.sql.Timestamp)vectFieldValue.elementAt(1)).toString()+ "'";            	
			 }
		  else
			 {
			  update+=(String)vectFieldValue.elementAt(0) + "=" + vectFieldValue.elementAt(1);
			 }

		  existStatement = true;
		 } // end for

/**
 * Blocco if basato sulla condizione:
 * record.getWhere()  diverso da  null  e   se non � (record.getWhere()).equals("")
**/
	   if (record.getWhere() != null && !(record.getWhere()).equals(""))
		{
		 update = update + " where " + record.getWhere();
		}

/**
 * La condizione nel blocco if �:
 * existStatement
**/
	   if(existStatement)
		{         
		 ret = executeUpdate(update);                        
		}
	   else
		  {}
          
	  } // end try		
	 catch(Exception exc)
		 {exc.printStackTrace();
			throw exc;}
 	     		
	 finally{return(ret);}
	}//END update

   /**Ricerca gli apici all'interno di una stringa.
	  Trovato un apice ne aggiunge un altro altrimenti l'insert o l'update falliscono.
	  es.   input -> d'angelo   output -> d''angelo
	 */
  private String parseString(String str) 
   {
	int ch = '\'';
	int index = 0;
	String retString = "";
	String subStringRigth = new String(str);
/**
 * Il ciclo si sviluppa su questa condizione: index!=-1
 * Quando non � soddisfatta non entra nel ciclo
**/
	while(index!=-1)
	 {
	  index = subStringRigth.indexOf(ch);
/**
 * La condizione nel blocco if �:
 * index diverso da -1
**/
	  if(index!=-1)
	   {
		retString += subStringRigth.substring(0, index+1);
		retString += '\'';
	   }
	  else
		  retString += subStringRigth.substring(0);

	  subStringRigth = subStringRigth.substring(index+1);
	 }
   return retString;
  }

/**
 * Metodo delete
 * Parametri:
 * String statement
 *
**/
  public int delete(String statement) throws Exception
   {    
	int ret = 0;
    
	try
	  {     
	   ret = executeUpdate(statement);         
	  }
	   		
	catch(Exception exc)
		 {throw exc;}
	     		
	finally
		   {return(ret);}
   }

 /**Restituisce la connessione costruita mediante l'invocazione del costruttore*/
/**
 * Metodo getConnection
 * Parametri:
 *
**/
  public java.sql.Connection getConnection() 
   {return connection;}

/**
 * Metodo isClosed
 * Parametri:
 *
**/
  public boolean isClosed()
   {
	boolean ret=false;
	try
	  {
/**
 * Blocco if basato sulla condizione:
 *  connection  diverso da  null 
**/
	   if( connection != null )
		{
		 ret = connection.isClosed();
		}
	   else
		  {
		   ret=true;
		  }
	  }
	catch(Exception ex)
		{
		 ex.printStackTrace();
		}
	return ret;
   }

  /**Chiude la connessione*/
/**
 * Metodo close
 * Parametri:
 *
**/
  public void close() throws  Exception
   {
	try
	  {
/**
 * Verifico la seguente condizione:
 * connection  diverso da  null  e   se non � (connection.isClosed())
**/
	   if (connection != null && !(connection.isClosed()))
		{
		 Enumeration enumy = prestmt.elements();
		 PreparedStatement myprepstat=null;
/**
 * Il ciclo viene impostato su questa condizione: enum.hasMoreElements()
 * Quando non � soddisfatta non entra nel ciclo
**/
		 while (enumy.hasMoreElements())
			 {
			  myprepstat = (PreparedStatement)enumy.nextElement();
			  myprepstat.close();
			 }
/**
 * Entro nel blocco if se la seguente condizione � soddisfatta
 * stmt  diverso da  null
**/
		 if (stmt != null)
			{
			 stmt.close();
			 stmt = null;
			}
 
		}
	  }
	catch(Exception ex)
		 {throw ex;}
	}

/**
 * Metodo commit
 * Parametri:
 *
**/
  public void commit() throws  Exception
   {
	try
	  {
/**
 * Verifico la seguente condizione:
 * connection  diverso da  null  e   se non � (connection.isClosed())
**/
	   if (connection != null && !(connection.isClosed()))
		{
		 connection.commit();
		}
	  }
	 catch(Exception ex)
		 {throw ex;}
	}
 
/**
 * Metodo rollback
 * Parametri:
 *
**/
  public void rollback() throws  Exception
   {
	try
	  {
/**
 * Verifico la seguente condizione:
 * connection  diverso da  null  e   se non � (connection.isClosed())
**/
	   if (connection != null && !(connection.isClosed()))
		{
		 connection.rollback();
		}
	  }
	 catch(Exception ex)
		 {		  
		  throw (ex);		
		 }
   }




  private int executeUpdate(String str) throws Exception 
  {
	int ret=0;
	try
	  {
	   stmt = connection.createStatement();
	   ret = stmt.executeUpdate(str);
	   return(ret); 
	  }		
		
	 catch (Exception ex) 
		 {
		  try 
			{
		     	
			 connection.rollback();	
			 stmt.close();
			 stmt=null;	     
			}			   		  	 
		  catch (Exception exc)
			  {
		       
			  }		  	    
		  finally
				 {throw ex;}      
		 }// end catch (Exception ex)     
     
  } // end executeUpdate
     
} // end class
