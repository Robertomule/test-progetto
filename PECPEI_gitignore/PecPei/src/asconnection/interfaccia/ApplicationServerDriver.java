package asconnection.interfaccia;

import it.eustema.inps.utility.ConfigProp;




/**
   * Interfaccia di costanti, che sar� implementata da ConnectionPool.
   * Contenente i driver di alcuni application server utilizzabili.

   * @version 1.00 13/12/2000
   * @author Fabio Bolognesi & Alessandro Erbani
*/

public interface ApplicationServerDriver{	                    
/**
 * WEBSPHERE
 **/    
	 public static final String DBCOMMON = "";
	 public static final String DBHERMES = "";
	 
}
