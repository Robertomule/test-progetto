package asconnection.utility;

import java.util.*;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class RowSet{
       Vector rs = null;
       int numRow = 0;
       int countRow = 0;
/**
 * Metodo RowSet
 * Parametri:
 *
**/
       public RowSet(){

       }
/**
 * Metodo RowSet
 * Parametri:
 * Vector rs
 *
**/
       public RowSet(Vector rs){
/**
 * Blocco if basato sulla condizione:
 * rs  diverso da  null
**/
        if (rs != null){
           countRow=rs.size();
           this.rs=rs;
        }
       }
/**
 * Metodo nextElement
 * Parametri:
 *
**/
       public boolean nextElement(){
/**
 * Entro nel blocco if se la seguente condizione � soddisfatta
 * numRow  minore di  countRow
**/
         if (numRow < countRow){
          numRow++;
          return true;
         }
         return false;

       }
/**
 * Metodo getRow
 * Parametri:
 *
**/
       public Row getRow(){
         Vector v = (Vector)rs.elementAt(numRow-1);
         return (new Row(v));
       }

/**
 * Metodo getSize
 * Parametri:
 *
**/
      public int getSize(){
       return countRow;
      }
    }
