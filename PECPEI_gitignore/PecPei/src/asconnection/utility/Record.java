package asconnection.utility;
import java.io.Serializable;
import java.util.Vector;
/**
   Classe che gestisce che rappresenta un nuovo record da inserire o agguiornare
   in nua tabella

 * @version 1.00 13/12/2000
 * @author Fabio Bolognesi & Giorgio Izzo & Alessandro Erbani
*/

public class Record implements Serializable
{
	private Vector vectFieldValue = new Vector();
    private String where = null;
	private String table = null;
	//modifica per eseguire update con inner join su db mssql 2005 o +
	//del 27 gennaio 2010
	private String from = null;

	// costruttori --
/**
 * Metodo Record
 * Parametri:
 *
**/
	public Record(){
	}

/**
 * Metodo Record
 * Parametri:
 * String strX
 *
**/
	public Record(String strX){
		this.table  = new String(strX);
	}
	
	public Record(String tab,String join)
	{
		this.table = new String(tab);
		this.from = new String(join);
	}

	/**Restituisce la vector*/
/**
 * Metodo getVectFieldValue
 * Parametri:
 *
**/
	public Vector getVectFieldValue(){
		return this.vectFieldValue;
	}
	/**Imposta la vector*/
/**
 * Metodo setVectFieldValue
 * Parametri:
 * Vector vect
 *
**/
	public void setVectFieldValue(Vector vect){
		this.vectFieldValue = new Vector(vect);
	}
	/**Restituisce la Stringa where*/
/**
 * Metodo getWhere
 * Parametri:
 *
**/
	public String getWhere(){
		return this.where;
	}
	/**Imposta la Stringa where*/
/**
 * Metodo setWhere
 * Parametri:
 * String str
 *
**/
	public void setWhere(String str){
		this.where = new String(str);
	}

	/**Restituisce la Stringa from*/
/**
 * Metodo getTable
 * Parametri:
 *
**/
	public String getTable(){
		return this.table;
	}
	/**Imposta la Stringa from*/
/**
 * Metodo setTable
 * Parametri:
 * String str
 *
**/
	public void setTable(String str){
		this.table = new String(str);
	}

	/**aggiunge una nuova coppia (colonna - valore) al vettore*/
/**
 * Metodo add
 * Parametri:
 * String key
 *  Object value
 *
**/
	public void add(String key, Object value){
		Vector vect = new Vector();
		vect.addElement(key);
		vect.addElement(value);
		this.vectFieldValue.addElement(vect);
	}
	/**
	 * @return
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param string
	 */
	public void setFrom(String string) {
		from = string;
	}

}
