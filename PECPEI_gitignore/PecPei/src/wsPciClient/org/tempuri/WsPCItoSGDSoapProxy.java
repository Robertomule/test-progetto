package wsPciClient.org.tempuri;

import it.eustema.inps.utility.ConfigProp;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;

import org.WSProtocolloNew.DNA_BASICHTTP_BindingStub;
import org.apache.axis.AxisFault;
import org.apache.axis.message.SOAPHeaderElement;

public class WsPCItoSGDSoapProxy implements wsPciClient.org.tempuri.WsPCItoSGDSoap {
	private String _endpoint = null;
	private wsPciClient.org.tempuri.WsPCItoSGDSoap wsPCItoSGDSoap = null;

	public WsPCItoSGDSoapProxy() {
		_initWsPCItoSGDSoapProxy();
	}

	public WsPCItoSGDSoapProxy(String endpoint) {
		_endpoint = endpoint;
		_initWsPCItoSGDSoapProxy();
	}

	private void _initWsPCItoSGDSoapProxy() {
		try {
			wsPCItoSGDSoap = (new wsPciClient.org.tempuri.WsPCItoSGDLocator()).getwsPCItoSGDSoap();
			if (wsPCItoSGDSoap != null) {
				if (_endpoint != null)
					((javax.xml.rpc.Stub)wsPCItoSGDSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
				else
					_endpoint = (String)((javax.xml.rpc.Stub)wsPCItoSGDSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
			}

		}
		catch (javax.xml.rpc.ServiceException serviceException) {}
	}

	public String getEndpoint() {
		return _endpoint;
	}

	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (wsPCItoSGDSoap != null)
			((javax.xml.rpc.Stub)wsPCItoSGDSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

	}

	public wsPciClient.org.tempuri.WsPCItoSGDSoap getWsPCItoSGDSoap() {
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap;
	}

	public java.lang.String versione() throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.versione();
	}

	public wsPciClient.org.tempuri.WsOUTRecuperaElencoUffici wsRecuperaElencoUffici(java.lang.String sCasellaMittente, java.lang.String sCodiceAOO, java.lang.String sAccount, java.lang.String sMatricola) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsRecuperaElencoUffici(sCasellaMittente, sCodiceAOO, sAccount, sMatricola);
	}

	public wsPciClient.org.tempuri.WsOUTRecuperaElencoApprovatori wsRecuperaElencoApprovatori(java.lang.String sCasellaMittente) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsRecuperaElencoApprovatori(sCasellaMittente);
	}

	public wsPciClient.org.tempuri.WsOUTVerificaInvio wsVerificaInvio(java.lang.String sAccountAutore, java.lang.String sMatricolaAutore, java.lang.String sMessageID, java.lang.String codiceAOO, java.lang.String sAccountPEC, int iRichiediApprovazione) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsVerificaInvio(sAccountAutore, sMatricolaAutore, sMessageID, codiceAOO, sAccountPEC, iRichiediApprovazione);
	}

	public wsPciClient.org.tempuri.WsOUTwsAnnullaInvioPEC wsAnnullaInvioPEC(java.lang.String sMessageID, java.lang.String sCodiceAOO, java.lang.String sAccountPEC) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsAnnullaInvioPEC(sMessageID, sCodiceAOO, sAccountPEC);
	}

	public wsPciClient.org.tempuri.WsOUTVerificaStatoApprovazione wsVerificaStatoApprovazione(java.lang.String sMessageID, java.lang.String sCodiceAOO, java.lang.String sAccountPEC) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsVerificaStatoApprovazione(sMessageID, sCodiceAOO, sAccountPEC);
	}

	public wsPciClient.org.tempuri.WsOUTComunicaRispostePEC wsComunicaRispostePEC(java.lang.String sMessageIDOriginale, java.lang.String sMessageIDRisposta, java.lang.String sCodiceAOORisposta, java.lang.String sAccountPECRIsposta, java.lang.String sTipo) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsComunicaRispostePEC(sMessageIDOriginale, sMessageIDRisposta, sCodiceAOORisposta, sAccountPECRIsposta, sTipo);
	}

	public wsPciClient.org.tempuri.WsOUTDestinatari wsDestinatariPerIniziale(org.apache.axis.types.UnsignedShort cCarattere) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsDestinatariPerIniziale(cCarattere);
	}

	public wsPciClient.org.tempuri.WsOUTElencoCategorie wsElencoCategorie() throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsElencoCategorie();
	}

	public wsPciClient.org.tempuri.WsOUTDestinatari wsDestinatariPerCategoria(java.lang.String sCategoria) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsDestinatariPerCategoria(sCategoria);
	}

	public wsPciClient.org.tempuri.WsOUTElencoRegione wsElencoRegione() throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsElencoRegione();
	}

	public wsPciClient.org.tempuri.WsOUTElencoProvincia wsElencoProvincia(java.lang.String sRegione) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsElencoProvincia(sRegione);
	}

	public wsPciClient.org.tempuri.WsOUTElencoComune wsElencoComune(java.lang.String sRegione, java.lang.String sProvincia) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsElencoComune(sRegione, sProvincia);
	}

	public wsPciClient.org.tempuri.WsOUTDestinatari wsDestinatariPerZonaGeografica(java.lang.String sRegione, java.lang.String sProvincia, java.lang.String sComune) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsDestinatariPerZonaGeografica(sRegione, sProvincia, sComune);
	}

	public wsPciClient.org.tempuri.WsOUTDestinatari wsDestinatariPerCodiceFiscaleCittadini(java.lang.String sCodiceFiscale) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsDestinatariPerCodiceFiscaleCittadini(sCodiceFiscale);
	}

	public wsPciClient.org.tempuri.WsOUTDestinatari wsElencoAccountINPSPrincipale(java.lang.String sCodiceAOO) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsElencoAccountINPSPrincipale(sCodiceAOO);
	}

	public wsPciClient.org.tempuri.WsOUTDestinatari wsElencoAccountINPS(java.lang.String sCodiceAOO) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsElencoAccountINPS(sCodiceAOO);
	}

	public boolean wsValidaEmail(java.lang.String sEmail) throws java.rmi.RemoteException{
		if (wsPCItoSGDSoap == null)
			_initWsPCItoSGDSoapProxy();
		return wsPCItoSGDSoap.wsValidaEmail(sEmail);
	}

	public static WsPCItoSGDSoapStub getProxyHelper(){

		/*
		TEST:
		Appname: AB00445
		Appkey: AB00445
		URL del servizio: https://ws.svil.inps/Ws.Net/WSProtocollo

		COLLAUDO:
		Appname: AB00445
		Appkey: 0zxehsxknoj2wwcyq9iwa1ievxss2jn2
		URL del servizio: https://ws.ser-collaudo.inps/Ws.Net/WSProtocollo
		 */

		WsPCItoSGDSoapStub prx=null;

		try {

			URL url = new URL(ConfigProp.endPointWsPciToSgd);
			prx = new WsPCItoSGDSoapStub(url,null);

			SOAPHeaderElement header = new SOAPHeaderElement("http://inps.it/", "Identity");
			header.addNamespaceDeclaration("inps", "http://inps.it/");

			SOAPElement nodeName = header.addChildElement("AppName");
			nodeName.addTextNode(ConfigProp.appName);
			SOAPElement nodeKey = header.addChildElement("AppKey");
			nodeKey.addTextNode(ConfigProp.appKey);
			SOAPElement nodeUser = header.addChildElement("UserId");
			nodeUser.addTextNode(ConfigProp.userId);
			SOAPElement nodeIdProvider = header.addChildElement("IdentityProvider");
			nodeIdProvider.addTextNode(ConfigProp.identityProvider);

			//	  		SOAPElement nodeSession = header.addChildElement("SessionId");
			//	  		nodeSession.addTextNode("");
			//	  		SOAPElement nodeSequence = header.addChildElement("SequenceId");
			//	  		nodeSequence.addTextNode("");
			//	  		SOAPElement nodePerr = header.addChildElement("PeerHost");
			//	  		nodePerr.addTextNode("");
			//	  		
			//	  		SOAPElement nodeCorrelation = header.addChildElement("CorrelationId");
			//	  		nodeCorrelation.addTextNode("");

			prx.setHeader(header);
			//proxy._getCall().setTargetEndpointAddress(url);
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		}

		return prx;

	}

	public static WsPCItoSGDSoapStub getProxyHelperNoDataPower(){

		WsPCItoSGDSoapStub prx=null;

		try {

			URL url = new URL(ConfigProp.endPointWsPciToSgd);
			prx = new WsPCItoSGDSoapStub(url,null);

			SOAPHeaderElement header = new SOAPHeaderElement("http://inps.it/", "Identity");
			header.addNamespaceDeclaration("inps", "http://inps.it/");
			SOAPElement nodeName = header.addChildElement("AppName","inps");
			nodeName.addTextNode(ConfigProp.appnamePciToSgd);
			SOAPElement nodeKey = header.addChildElement("AppKey","inps");
			nodeKey.addTextNode(ConfigProp.appkeyPciToSgd);
			SOAPElement nodeUser = header.addChildElement("UserId","inps");
			nodeUser.addTextNode(ConfigProp.userIdSgd);
			SOAPElement nodeIdProvider = header.addChildElement("IdentityProvider","inps");
			nodeIdProvider.addTextNode(ConfigProp.identityProviderSgd);
			SOAPElement nodeSession = header.addChildElement("SessionId");
			nodeSession.addTextNode("2F358F78-5782-4E7B-B645-396C7127A31D");
			SOAPElement nodeSequence = header.addChildElement("SequenceId");
			nodeSequence.addTextNode("0");
			SOAPElement nodePerr = header.addChildElement("PeerHost");
			nodePerr.addTextNode("127.0.0.1");
			SOAPElement nodeCorrelation = header.addChildElement("CorrelationId");
			nodeCorrelation.addTextNode("");
			SOAPElement nodeCoperation = header.addChildElement("OperationContextId");
			nodeCoperation.addTextNode("0");

			prx.setHeader(header);
			//proxy._getCall().setTargetEndpointAddress(url);
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		}

		return prx;

	}
}