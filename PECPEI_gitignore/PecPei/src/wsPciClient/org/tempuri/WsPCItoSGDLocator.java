/**
 * WsPCItoSGDLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

public class WsPCItoSGDLocator extends org.apache.axis.client.Service implements wsPciClient.org.tempuri.WsPCItoSGD {

    public WsPCItoSGDLocator() {
    }


    public WsPCItoSGDLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WsPCItoSGDLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for wsPCItoSGDSoap
    private java.lang.String wsPCItoSGDSoap_address = "http://msws2.svil.inps/WSPCI/wsPCItoSGD.asmx";

    public java.lang.String getwsPCItoSGDSoapAddress() {
        return wsPCItoSGDSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String wsPCItoSGDSoapWSDDServiceName = "wsPCItoSGDSoap";

    public java.lang.String getwsPCItoSGDSoapWSDDServiceName() {
        return wsPCItoSGDSoapWSDDServiceName;
    }

    public void setwsPCItoSGDSoapWSDDServiceName(java.lang.String name) {
        wsPCItoSGDSoapWSDDServiceName = name;
    }

    public wsPciClient.org.tempuri.WsPCItoSGDSoap getwsPCItoSGDSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(wsPCItoSGDSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getwsPCItoSGDSoap(endpoint);
    }

    public wsPciClient.org.tempuri.WsPCItoSGDSoap getwsPCItoSGDSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            wsPciClient.org.tempuri.WsPCItoSGDSoapStub _stub = new wsPciClient.org.tempuri.WsPCItoSGDSoapStub(portAddress, this);
            _stub.setPortName(getwsPCItoSGDSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setwsPCItoSGDSoapEndpointAddress(java.lang.String address) {
        wsPCItoSGDSoap_address = address;
    }


    // Use to get a proxy class for wsPCItoSGDSoap12
    private java.lang.String wsPCItoSGDSoap12_address = "http://msws2.svil.inps/WSPCI/wsPCItoSGD.asmx";

    public java.lang.String getwsPCItoSGDSoap12Address() {
        return wsPCItoSGDSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String wsPCItoSGDSoap12WSDDServiceName = "wsPCItoSGDSoap12";

    public java.lang.String getwsPCItoSGDSoap12WSDDServiceName() {
        return wsPCItoSGDSoap12WSDDServiceName;
    }

    public void setwsPCItoSGDSoap12WSDDServiceName(java.lang.String name) {
        wsPCItoSGDSoap12WSDDServiceName = name;
    }

    public wsPciClient.org.tempuri.WsPCItoSGDSoap getwsPCItoSGDSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(wsPCItoSGDSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getwsPCItoSGDSoap12(endpoint);
    }

    public wsPciClient.org.tempuri.WsPCItoSGDSoap getwsPCItoSGDSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            wsPciClient.org.tempuri.WsPCItoSGDSoap12Stub _stub = new wsPciClient.org.tempuri.WsPCItoSGDSoap12Stub(portAddress, this);
            _stub.setPortName(getwsPCItoSGDSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setwsPCItoSGDSoap12EndpointAddress(java.lang.String address) {
        wsPCItoSGDSoap12_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (wsPciClient.org.tempuri.WsPCItoSGDSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                wsPciClient.org.tempuri.WsPCItoSGDSoapStub _stub = new wsPciClient.org.tempuri.WsPCItoSGDSoapStub(new java.net.URL(wsPCItoSGDSoap_address), this);
                _stub.setPortName(getwsPCItoSGDSoapWSDDServiceName());
                return _stub;
            }
            if (wsPciClient.org.tempuri.WsPCItoSGDSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                wsPciClient.org.tempuri.WsPCItoSGDSoap12Stub _stub = new wsPciClient.org.tempuri.WsPCItoSGDSoap12Stub(new java.net.URL(wsPCItoSGDSoap12_address), this);
                _stub.setPortName(getwsPCItoSGDSoap12WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("wsPCItoSGDSoap".equals(inputPortName)) {
            return getwsPCItoSGDSoap();
        }
        else if ("wsPCItoSGDSoap12".equals(inputPortName)) {
            return getwsPCItoSGDSoap12();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "wsPCItoSGD");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "wsPCItoSGDSoap"));
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "wsPCItoSGDSoap12"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("wsPCItoSGDSoap".equals(portName)) {
            setwsPCItoSGDSoapEndpointAddress(address);
        }
        else 
if ("wsPCItoSGDSoap12".equals(portName)) {
            setwsPCItoSGDSoap12EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
