/**
 * WsPCItoSGD.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

public interface WsPCItoSGD extends javax.xml.rpc.Service {
    public java.lang.String getwsPCItoSGDSoapAddress();

    public wsPciClient.org.tempuri.WsPCItoSGDSoap getwsPCItoSGDSoap() throws javax.xml.rpc.ServiceException;

    public wsPciClient.org.tempuri.WsPCItoSGDSoap getwsPCItoSGDSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getwsPCItoSGDSoap12Address();

    public wsPciClient.org.tempuri.WsPCItoSGDSoap getwsPCItoSGDSoap12() throws javax.xml.rpc.ServiceException;

    public wsPciClient.org.tempuri.WsPCItoSGDSoap getwsPCItoSGDSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
