/**
 * DNA_BASICHTTP_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

import it.eustema.inps.utility.ConfigProp;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;

import org.apache.axis.message.SOAPHeaderElement;

public class DNA_BASICHTTP_BindingStub extends org.apache.axis.client.Stub implements it.inps.soa.WS00541.IStoreAttachmentDocument {
	private java.util.Vector cachedSerClasses = new java.util.Vector();
	private java.util.Vector cachedSerQNames = new java.util.Vector();
	private java.util.Vector cachedSerFactories = new java.util.Vector();
	private java.util.Vector cachedDeserFactories = new java.util.Vector();

	static org.apache.axis.description.OperationDesc [] _operations;

	private static enum DataPowerHeader {
		APP_NAME ("WS00591"),
		APP_KEY ("WS00591"),
		USER_ID ("soapUI"),
		IDENTITY_PROVIDER ("AD");
		//		SESSION_ID ("FD713788-B5AE-49FF-8B2C-F311B9CB0CC4"),
		//		SEQUENCE_ID ("112233"),
		//		CLIENT_IP ("10.170.82.174");

		private final String value;

		DataPowerHeader(String value) {
			this.value = value;
		}

		private String value() { 
			return value; 
		}
	}

	static {
		_operations = new org.apache.axis.description.OperationDesc[25];
		_initOperationDesc1();
		_initOperationDesc2();
		_initOperationDesc3();

	}

	private static void _initOperationDesc1(){
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("EsistonoDocumentiPerSegnatura");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "EsistonoDocumentiPerSegnaturaRequest"), it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "EsistonoDocumentiPerSegnaturaResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "EsistonoDocumentiPerSegnaturaResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[0] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("GetDocumento");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoRequest"), it.inps.soa.DocumentStore.data.GetDocumentoRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.GetDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "GetDocumentoResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[1] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("GetDocumentoPerContainer");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoRequest"), it.inps.soa.DocumentStore.data.GetDocumentoRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.GetDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "GetDocumentoPerContainerResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[2] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ListaDocumentiPerContainer");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiPerContainerRequest"), it.inps.soa.DocumentStore.data.ListaDocumentiPerContainerRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiPerContainerResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[3] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ListaDocumentiCorrelatiPerContainer");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiPerContainerRequest"), it.inps.soa.DocumentStore.data.ListaDocumentiPerContainerRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiCorrelatiPerContainerResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[4] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ListaDocumentiPerSegnatura");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiPerSegnaturaRequest"), it.inps.soa.DocumentStore.data.ListaDocumentiPerSegnaturaRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiPerSegnaturaResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[5] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ListaDocumentiCorrelatiPerSegnatura");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiPerSegnaturaRequest"), it.inps.soa.DocumentStore.data.ListaDocumentiPerSegnaturaRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiCorrelatiPerSegnaturaResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[6] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ArchiviaDocumentoPerSegnatura");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerSegnaturaRequest"), it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerSegnaturaResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[7] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ArchiviaDocumentoPerContainer");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerContainerRequest"), it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerContainerRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerContainerResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[8] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("PrenotaVisualizzazioneDocumento");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneDocumentoRequest"), it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaVisualizzazioneDocumentoResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[9] = oper;

	}

	private static void _initOperationDesc2(){
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("PrenotaVisualizzazioneSintesi");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneSintesiRequest"), it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneSintesiResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaVisualizzazioneSintesiResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[10] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("PrenotaVisualizzazioneRicevuta");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneRicevutaRequest"), it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneRicevutaResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaVisualizzazioneRicevutaResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[11] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("GetAllegato");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetAllegatoRequest"), it.inps.soa.DocumentStore.data.GetAllegatoRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetAllegatoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.GetAllegatoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "GetAllegatoResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[12] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("RecuperaMetadatiAllegato");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "RecuperaMetadatiRequest"), it.inps.soa.DocumentStore.data.RecuperaMetadatiRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "RecuperaMetadatiResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.RecuperaMetadatiResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "RecuperaMetadatiAllegatoResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[13] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ArchiviaAllegato");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoRequest"), it.inps.soa.DocumentStore.data.ArchiviaDocumentoRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaAllegatoResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[14] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("CancellaAllegato");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "CancellaDocumentoRequest"), it.inps.soa.DocumentStore.data.CancellaDocumentoRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "CancellaDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.CancellaDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "CancellaAllegatoResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[15] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("PrenotaVisualizzazioneAllegato");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneDocumentoRequest"), it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaVisualizzazioneAllegatoResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[16] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("PrenotaCreazioneAllegato");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaCreazioneDocumentoRequest"), it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaCreazioneDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaCreazioneAllegatoResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[17] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("PrenotaAggiornamentoAllegato");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaAggiornamentoDocumentoRequest"), it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaAggiornamentoDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaAggiornamentoAllegatoResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[18] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ArchiviaDocumentoPerSegnaturaPregressaNonStandard");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerSegnaturaPregressaRequest"), it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaPregressaRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerSegnaturaPregressaNonStandardResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[19] = oper;


	}

	private static void _initOperationDesc3(){
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ArchiviaDocumentoPerSegnaturaPregressa");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerSegnaturaPregressaRequest"), it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaPregressaRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerSegnaturaPregressaResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[20] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ArchiviaDocumentoPerVersione");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerVersioneRequest"), it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerVersioneRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerVersioneResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[21] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("GetDocumentoPerVersione");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoPerVersioneRequest"), it.inps.soa.DocumentStore.data.GetDocumentoPerVersioneRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.GetDocumentoResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "GetDocumentoPerVersioneResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[22] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("ListaDocumentiVersionati");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiVersionatiRequest"), it.inps.soa.DocumentStore.data.ListaDocumentiVersionatiRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiVersionatiResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[23] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("AggiornaMetadati");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "AggiornaMetadatiRequest"), it.inps.soa.DocumentStore.data.AggiornaMetadatiRequest.class, false, false);
		param.setOmittable(true);
		param.setNillable(true);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "AggiornaMetadatiResponse"));
		oper.setReturnClass(it.inps.soa.DocumentStore.data.AggiornaMetadatiResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "AggiornaMetadatiResult"));
		oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
		oper.setUse(org.apache.axis.constants.Use.LITERAL);
		_operations[24] = oper;

	}

	public DNA_BASICHTTP_BindingStub() throws org.apache.axis.AxisFault {
		this(null);
	}

	public DNA_BASICHTTP_BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
		this(service);
		super.cachedEndpoint = endpointURL;
	}

	public DNA_BASICHTTP_BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
		if (service == null) {
			super.service = new org.apache.axis.client.Service();
		} else {
			super.service = service;
		}
		((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
		java.lang.Class cls;
		javax.xml.namespace.QName qName;
		javax.xml.namespace.QName qName2;
		java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
		java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
		java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
		java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
		java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
		java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
		java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
		java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
		java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
		java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
		qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", ">ArrayOfKeyValueOfstringstring>KeyValueOfstringstring");
		cachedSerQNames.add(qName);
		cls = com.microsoft.schemas._2003._10.Serialization.Arrays.ArrayOfKeyValueOfstringstringKeyValueOfstringstring.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfint");
		cachedSerQNames.add(qName);
		cls = int[].class;
		cachedSerClasses.add(cls);
		qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
		qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int");
		cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
		cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

		qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfKeyValueOfstringstring");
		cachedSerQNames.add(qName);
		cls = com.microsoft.schemas._2003._10.Serialization.Arrays.ArrayOfKeyValueOfstringstringKeyValueOfstringstring[].class;
		cachedSerClasses.add(cls);
		qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", ">ArrayOfKeyValueOfstringstring>KeyValueOfstringstring");
		qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "KeyValueOfstringstring");
		cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
		cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

		qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
		cachedSerQNames.add(qName);
		cls = java.lang.String[].class;
		cachedSerClasses.add(cls);
		qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
		qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string");
		cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
		cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "AggiornaMetadatiRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.AggiornaMetadatiRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "AggiornaMetadatiResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.AggiornaMetadatiResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerContainerRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerContainerRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerSegnaturaPregressaRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaPregressaRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerSegnaturaRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoPerVersioneRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerVersioneRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoProtocollatoRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ArchiviaDocumentoProtocollatoRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ArchiviaDocumentoRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArchiviaDocumentoResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ArrayOfDocumentoProtocollatoExistsResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.DocumentoProtocollatoExistsResponse[].class;
		cachedSerClasses.add(cls);
		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoProtocollatoExistsResponse");
		qName2 = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoProtocollatoExistsResponse");
		cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
		cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "CancellaDocumentoRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.CancellaDocumentoRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "CancellaDocumentoResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.CancellaDocumentoResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentiStore");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.DocumentoStore[].class;
		cachedSerClasses.add(cls);
		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoStore");
		qName2 = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Documento");
		cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
		cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Documento");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.Documento.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoProtocollatoExistsResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.DocumentoProtocollatoExistsResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentoStore");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.DocumentoStore.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentStoreLastState");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.DocumentStoreLastState.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(enumsf);
		cachedDeserFactories.add(enumdf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentStoreRequestBase");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.DocumentStoreRequestBase.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentStoreResponseBase");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.DocumentStoreResponseBase.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentStoreType");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.DocumentStoreType.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(enumsf);
		cachedDeserFactories.add(enumdf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "DocumentType");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.DocumentType.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(enumsf);
		cachedDeserFactories.add(enumdf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "EsistonoDocumentiPerSegnaturaRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "EsistonoDocumentiPerSegnaturaResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "EsitoTypeEnum");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.EsitoTypeEnum.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(enumsf);
		cachedDeserFactories.add(enumdf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetAllegatoRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.GetAllegatoRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetAllegatoResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.GetAllegatoResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoPerVersioneRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.GetDocumentoPerVersioneRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.GetDocumentoRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "GetDocumentoResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.GetDocumentoResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "InternalDocumentType");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.InternalDocumentType.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(enumsf);
		cachedDeserFactories.add(enumdf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiPerContainerRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ListaDocumentiPerContainerRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiPerSegnaturaRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ListaDocumentiPerSegnaturaRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "ListaDocumentiVersionatiRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.ListaDocumentiVersionatiRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "Metadati");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.Metadati.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaAggiornamentoDocumentoRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaAggiornamentoDocumentoResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaCreazioneDocumentoRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaCreazioneDocumentoResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneDocumentoRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneDocumentoResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneRicevutaRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneRicevutaResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneSintesiRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "PrenotaVisualizzazioneSintesiResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "RecuperaMetadatiRequest");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.RecuperaMetadatiRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://soa.inps.it/DocumentStore/data", "RecuperaMetadatiResponse");
		cachedSerQNames.add(qName);
		cls = it.inps.soa.DocumentStore.data.RecuperaMetadatiResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

	}

	protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
		try {
			org.apache.axis.client.Call _call = super._createCall();
			if (super.maintainSessionSet) {
				_call.setMaintainSession(super.maintainSession);
			}
			if (super.cachedUsername != null) {
				_call.setUsername(super.cachedUsername);
			}
			if (super.cachedPassword != null) {
				_call.setPassword(super.cachedPassword);
			}
			if (super.cachedEndpoint != null) {
				_call.setTargetEndpointAddress(super.cachedEndpoint);
			}
			if (super.cachedTimeout != null) {
				_call.setTimeout(super.cachedTimeout);
			}
			if (super.cachedPortName != null) {
				_call.setPortName(super.cachedPortName);
			}
			java.util.Enumeration keys = super.cachedProperties.keys();
			while (keys.hasMoreElements()) {
				java.lang.String key = (java.lang.String) keys.nextElement();
				_call.setProperty(key, super.cachedProperties.get(key));
			}
			// All the type mapping information is registered
			// when the first call is made.
			// The type mapping information is actually registered in
			// the TypeMappingRegistry of the service, which
			// is the reason why registration is only needed for the first call.
			synchronized (this) {
				if (firstCall()) {
					// must set encoding style before registering serializers
					_call.setEncodingStyle(null);
					for (int i = 0; i < cachedSerFactories.size(); ++i) {
						java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
						javax.xml.namespace.QName qName =
							(javax.xml.namespace.QName) cachedSerQNames.get(i);
						java.lang.Object x = cachedSerFactories.get(i);
						if (x instanceof Class) {
							java.lang.Class sf = (java.lang.Class)
							cachedSerFactories.get(i);
							java.lang.Class df = (java.lang.Class)
							cachedDeserFactories.get(i);
							_call.registerTypeMapping(cls, qName, sf, df, false);
						}
						else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
							org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
							cachedSerFactories.get(i);
							org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
							cachedDeserFactories.get(i);
							_call.registerTypeMapping(cls, qName, sf, df, false);
						}
					}
				}
			}

			return _call;
		}
		catch (java.lang.Throwable _t) {
			throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
		}
	}

	public it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaResponse esistonoDocumentiPerSegnatura(it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[0]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/EsistonoDocumentiPerSegnatura");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "EsistonoDocumentiPerSegnatura"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.EsistonoDocumentiPerSegnaturaResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.GetDocumentoResponse getDocumento(it.inps.soa.DocumentStore.data.GetDocumentoRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[1]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/GetDocumento");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "GetDocumento"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.GetDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.GetDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.GetDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.GetDocumentoResponse getDocumentoPerContainer(it.inps.soa.DocumentStore.data.GetDocumentoRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[2]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/GetDocumentoPerContainer");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "GetDocumentoPerContainer"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.GetDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.GetDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.GetDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiPerContainer(it.inps.soa.DocumentStore.data.ListaDocumentiPerContainerRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[3]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ListaDocumentiPerContainer");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiPerContainer"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiCorrelatiPerContainer(it.inps.soa.DocumentStore.data.ListaDocumentiPerContainerRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[4]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ListaDocumentiCorrelatiPerContainer");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiCorrelatiPerContainer"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiPerSegnatura(it.inps.soa.DocumentStore.data.ListaDocumentiPerSegnaturaRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[5]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ListaDocumentiPerSegnatura");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiPerSegnatura"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiCorrelatiPerSegnatura(it.inps.soa.DocumentStore.data.ListaDocumentiPerSegnaturaRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[6]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ListaDocumentiCorrelatiPerSegnatura");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiCorrelatiPerSegnatura"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerSegnatura(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[7]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ArchiviaDocumentoPerSegnatura");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerSegnatura"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerContainer(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerContainerRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[8]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ArchiviaDocumentoPerContainer");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerContainer"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse prenotaVisualizzazioneDocumento(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[9]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/PrenotaVisualizzazioneDocumento");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaVisualizzazioneDocumento"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiResponse prenotaVisualizzazioneSintesi(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[10]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/PrenotaVisualizzazioneSintesi");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaVisualizzazioneSintesi"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneSintesiResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaResponse prenotaVisualizzazioneRicevuta(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[11]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/PrenotaVisualizzazioneRicevuta");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaVisualizzazioneRicevuta"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneRicevutaResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.GetAllegatoResponse getAllegato(it.inps.soa.DocumentStore.data.GetAllegatoRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[12]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/GetAllegato");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "GetAllegato"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.GetAllegatoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.GetAllegatoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.GetAllegatoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.RecuperaMetadatiResponse recuperaMetadatiAllegato(it.inps.soa.DocumentStore.data.RecuperaMetadatiRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[13]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/RecuperaMetadatiAllegato");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "RecuperaMetadatiAllegato"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.RecuperaMetadatiResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.RecuperaMetadatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.RecuperaMetadatiResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaAllegato(it.inps.soa.DocumentStore.data.ArchiviaDocumentoRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}

		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[14]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ArchiviaAllegato");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaAllegato"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.CancellaDocumentoResponse cancellaAllegato(it.inps.soa.DocumentStore.data.CancellaDocumentoRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[15]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/CancellaAllegato");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "CancellaAllegato"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.CancellaDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.CancellaDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.CancellaDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse prenotaVisualizzazioneAllegato(it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[16]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/PrenotaVisualizzazioneAllegato");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaVisualizzazioneAllegato"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.PrenotaVisualizzazioneDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoResponse prenotaCreazioneAllegato(it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[17]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/PrenotaCreazioneAllegato");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaCreazioneAllegato"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.PrenotaCreazioneDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoResponse prenotaAggiornamentoAllegato(it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[18]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/PrenotaAggiornamentoAllegato");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "PrenotaAggiornamentoAllegato"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.PrenotaAggiornamentoDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerSegnaturaPregressaNonStandard(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaPregressaRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[19]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ArchiviaDocumentoPerSegnaturaPregressaNonStandard");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerSegnaturaPregressaNonStandard"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerSegnaturaPregressa(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerSegnaturaPregressaRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[20]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ArchiviaDocumentoPerSegnaturaPregressa");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerSegnaturaPregressa"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse archiviaDocumentoPerVersione(it.inps.soa.DocumentStore.data.ArchiviaDocumentoPerVersioneRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[21]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ArchiviaDocumentoPerVersione");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ArchiviaDocumentoPerVersione"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ArchiviaDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.GetDocumentoResponse getDocumentoPerVersione(it.inps.soa.DocumentStore.data.GetDocumentoPerVersioneRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[22]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/GetDocumentoPerVersione");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "GetDocumentoPerVersione"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.GetDocumentoResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.GetDocumentoResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.GetDocumentoResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.ListaDocumentiResponse listaDocumentiVersionati(it.inps.soa.DocumentStore.data.ListaDocumentiVersionatiRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[23]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/ListaDocumentiVersionati");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "ListaDocumentiVersionati"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.ListaDocumentiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.ListaDocumentiResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public it.inps.soa.DocumentStore.data.AggiornaMetadatiResponse aggiornaMetadati(it.inps.soa.DocumentStore.data.AggiornaMetadatiRequest request) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[24]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("http://soa.inps.it/WS00541/IStoreAttachmentDocument/AggiornaMetadati");
		_call.setEncodingStyle(null);
		_call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
		_call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://soa.inps.it/WS00541", "AggiornaMetadati"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

		if (_resp instanceof java.rmi.RemoteException) {
			throw (java.rmi.RemoteException)_resp;
		}
		else {
			extractAttachments(_call);
			try {
				return (it.inps.soa.DocumentStore.data.AggiornaMetadatiResponse) _resp;
			} catch (java.lang.Exception _exception) {
				return (it.inps.soa.DocumentStore.data.AggiornaMetadatiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, it.inps.soa.DocumentStore.data.AggiornaMetadatiResponse.class);
			}
		}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	private SOAPHeaderElement getProxyHelper(){

		try {
			SOAPHeaderElement header = new SOAPHeaderElement("http://inps.it", "Identity");
			SOAPElement nodeName = header.addChildElement("AppName");
			nodeName.addTextNode(DataPowerHeader.APP_NAME.value());
			SOAPElement nodeKey = header.addChildElement("AppKey");
			nodeKey.addTextNode(DataPowerHeader.APP_KEY.value());
			SOAPElement nodeUser = header.addChildElement("UserId");
			nodeUser.addTextNode(DataPowerHeader.USER_ID.value());
			SOAPElement nodeIdProvider = header.addChildElement("IdentityProvider");
			nodeIdProvider.addTextNode(DataPowerHeader.IDENTITY_PROVIDER.value());
			SOAPElement nodeSession = header.addChildElement("SessionId");
			nodeSession.addTextNode("");
			SOAPElement nodeSequence = header.addChildElement("SequenceId");
			nodeSequence.addTextNode("");
			SOAPElement nodePerr = header.addChildElement("PeerHost");
			nodePerr.addTextNode("");
			SOAPElement nodeCorrelation = header.addChildElement("CorrelationId");
			nodeCorrelation.addTextNode("");
			//this.setHeader(header);

			return header;

		} catch (SOAPException e) {
			e.printStackTrace();
		}

		return null;
	}

}
