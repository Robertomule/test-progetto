/**
 * Ufficio.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

public class Ufficio  implements java.io.Serializable {
    private java.lang.String sDescrizione;

    private java.lang.String sSede;

    public Ufficio() {
    }

    public Ufficio(
           java.lang.String sDescrizione,
           java.lang.String sSede) {
           this.sDescrizione = sDescrizione;
           this.sSede = sSede;
    }


    /**
     * Gets the sDescrizione value for this Ufficio.
     * 
     * @return sDescrizione
     */
    public java.lang.String getSDescrizione() {
        return sDescrizione;
    }


    /**
     * Sets the sDescrizione value for this Ufficio.
     * 
     * @param sDescrizione
     */
    public void setSDescrizione(java.lang.String sDescrizione) {
        this.sDescrizione = sDescrizione;
    }


    /**
     * Gets the sSede value for this Ufficio.
     * 
     * @return sSede
     */
    public java.lang.String getSSede() {
        return sSede;
    }


    /**
     * Sets the sSede value for this Ufficio.
     * 
     * @param sSede
     */
    public void setSSede(java.lang.String sSede) {
        this.sSede = sSede;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Ufficio)) return false;
        Ufficio other = (Ufficio) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sDescrizione==null && other.getSDescrizione()==null) || 
             (this.sDescrizione!=null &&
              this.sDescrizione.equals(other.getSDescrizione()))) &&
            ((this.sSede==null && other.getSSede()==null) || 
             (this.sSede!=null &&
              this.sSede.equals(other.getSSede())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSDescrizione() != null) {
            _hashCode += getSDescrizione().hashCode();
        }
        if (getSSede() != null) {
            _hashCode += getSSede().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Ufficio.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Ufficio"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SDescrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sDescrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSede");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sSede"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
