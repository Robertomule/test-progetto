/**
 * WsOUTElencoProvincia.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

public class WsOUTElencoProvincia  implements java.io.Serializable {
    private int iEsito;

    private java.lang.String sDescrizione;

    private java.lang.String[] aProvince;

    public WsOUTElencoProvincia() {
    }

    public WsOUTElencoProvincia(
           int iEsito,
           java.lang.String sDescrizione,
           java.lang.String[] aProvince) {
           this.iEsito = iEsito;
           this.sDescrizione = sDescrizione;
           this.aProvince = aProvince;
    }


    /**
     * Gets the iEsito value for this WsOUTElencoProvincia.
     * 
     * @return iEsito
     */
    public int getIEsito() {
        return iEsito;
    }


    /**
     * Sets the iEsito value for this WsOUTElencoProvincia.
     * 
     * @param iEsito
     */
    public void setIEsito(int iEsito) {
        this.iEsito = iEsito;
    }


    /**
     * Gets the sDescrizione value for this WsOUTElencoProvincia.
     * 
     * @return sDescrizione
     */
    public java.lang.String getSDescrizione() {
        return sDescrizione;
    }


    /**
     * Sets the sDescrizione value for this WsOUTElencoProvincia.
     * 
     * @param sDescrizione
     */
    public void setSDescrizione(java.lang.String sDescrizione) {
        this.sDescrizione = sDescrizione;
    }


    /**
     * Gets the aProvince value for this WsOUTElencoProvincia.
     * 
     * @return aProvince
     */
    public java.lang.String[] getAProvince() {
        return aProvince;
    }


    /**
     * Sets the aProvince value for this WsOUTElencoProvincia.
     * 
     * @param aProvince
     */
    public void setAProvince(java.lang.String[] aProvince) {
        this.aProvince = aProvince;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsOUTElencoProvincia)) return false;
        WsOUTElencoProvincia other = (WsOUTElencoProvincia) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.iEsito == other.getIEsito() &&
            ((this.sDescrizione==null && other.getSDescrizione()==null) || 
             (this.sDescrizione!=null &&
              this.sDescrizione.equals(other.getSDescrizione()))) &&
            ((this.aProvince==null && other.getAProvince()==null) || 
             (this.aProvince!=null &&
              java.util.Arrays.equals(this.aProvince, other.getAProvince())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getIEsito();
        if (getSDescrizione() != null) {
            _hashCode += getSDescrizione().hashCode();
        }
        if (getAProvince() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAProvince());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAProvince(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsOUTElencoProvincia.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "wsOUTElencoProvincia"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IEsito");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "iEsito"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SDescrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sDescrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AProvince");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "aProvince"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
