/**
 * Approvatore.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

public class Approvatore  implements java.io.Serializable {
    private java.lang.String sMatricola;

    private java.lang.String sEmail;

    private java.lang.String sNominativo;

    private java.lang.String sAccountRete;

    private java.lang.String sRiservato;

    private java.lang.String sSensibile;

    public Approvatore() {
    }

    public Approvatore(
           java.lang.String sMatricola,
           java.lang.String sEmail,
           java.lang.String sNominativo,
           java.lang.String sAccountRete,
           java.lang.String sRiservato,
           java.lang.String sSensibile) {
           this.sMatricola = sMatricola;
           this.sEmail = sEmail;
           this.sNominativo = sNominativo;
           this.sAccountRete = sAccountRete;
           this.sRiservato = sRiservato;
           this.sSensibile = sSensibile;
    }


    /**
     * Gets the sMatricola value for this Approvatore.
     * 
     * @return sMatricola
     */
    public java.lang.String getSMatricola() {
        return sMatricola;
    }


    /**
     * Sets the sMatricola value for this Approvatore.
     * 
     * @param sMatricola
     */
    public void setSMatricola(java.lang.String sMatricola) {
        this.sMatricola = sMatricola;
    }


    /**
     * Gets the sEmail value for this Approvatore.
     * 
     * @return sEmail
     */
    public java.lang.String getSEmail() {
        return sEmail;
    }


    /**
     * Sets the sEmail value for this Approvatore.
     * 
     * @param sEmail
     */
    public void setSEmail(java.lang.String sEmail) {
        this.sEmail = sEmail;
    }


    /**
     * Gets the sNominativo value for this Approvatore.
     * 
     * @return sNominativo
     */
    public java.lang.String getSNominativo() {
        return sNominativo;
    }


    /**
     * Sets the sNominativo value for this Approvatore.
     * 
     * @param sNominativo
     */
    public void setSNominativo(java.lang.String sNominativo) {
        this.sNominativo = sNominativo;
    }


    /**
     * Gets the sAccountRete value for this Approvatore.
     * 
     * @return sAccountRete
     */
    public java.lang.String getSAccountRete() {
        return sAccountRete;
    }


    /**
     * Sets the sAccountRete value for this Approvatore.
     * 
     * @param sAccountRete
     */
    public void setSAccountRete(java.lang.String sAccountRete) {
        this.sAccountRete = sAccountRete;
    }


    /**
     * Gets the sRiservato value for this Approvatore.
     * 
     * @return sRiservato
     */
    public java.lang.String getSRiservato() {
        return sRiservato;
    }


    /**
     * Sets the sRiservato value for this Approvatore.
     * 
     * @param sRiservato
     */
    public void setSRiservato(java.lang.String sRiservato) {
        this.sRiservato = sRiservato;
    }


    /**
     * Gets the sSensibile value for this Approvatore.
     * 
     * @return sSensibile
     */
    public java.lang.String getSSensibile() {
        return sSensibile;
    }


    /**
     * Sets the sSensibile value for this Approvatore.
     * 
     * @param sSensibile
     */
    public void setSSensibile(java.lang.String sSensibile) {
        this.sSensibile = sSensibile;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Approvatore)) return false;
        Approvatore other = (Approvatore) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sMatricola==null && other.getSMatricola()==null) || 
             (this.sMatricola!=null &&
              this.sMatricola.equals(other.getSMatricola()))) &&
            ((this.sEmail==null && other.getSEmail()==null) || 
             (this.sEmail!=null &&
              this.sEmail.equals(other.getSEmail()))) &&
            ((this.sNominativo==null && other.getSNominativo()==null) || 
             (this.sNominativo!=null &&
              this.sNominativo.equals(other.getSNominativo()))) &&
            ((this.sAccountRete==null && other.getSAccountRete()==null) || 
             (this.sAccountRete!=null &&
              this.sAccountRete.equals(other.getSAccountRete()))) &&
            ((this.sRiservato==null && other.getSRiservato()==null) || 
             (this.sRiservato!=null &&
              this.sRiservato.equals(other.getSRiservato()))) &&
            ((this.sSensibile==null && other.getSSensibile()==null) || 
             (this.sSensibile!=null &&
              this.sSensibile.equals(other.getSSensibile())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSMatricola() != null) {
            _hashCode += getSMatricola().hashCode();
        }
        if (getSEmail() != null) {
            _hashCode += getSEmail().hashCode();
        }
        if (getSNominativo() != null) {
            _hashCode += getSNominativo().hashCode();
        }
        if (getSAccountRete() != null) {
            _hashCode += getSAccountRete().hashCode();
        }
        if (getSRiservato() != null) {
            _hashCode += getSRiservato().hashCode();
        }
        if (getSSensibile() != null) {
            _hashCode += getSSensibile().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Approvatore.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Approvatore"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMatricola");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sMatricola"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SNominativo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sNominativo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SAccountRete");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sAccountRete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SRiservato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sRiservato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSensibile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sSensibile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
