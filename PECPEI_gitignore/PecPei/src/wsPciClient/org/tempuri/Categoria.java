/**
 * Categoria.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

public class Categoria  implements java.io.Serializable {
    private java.lang.String sDescrizione;

    private java.lang.String sValore;

    public Categoria() {
    }

    public Categoria(
           java.lang.String sDescrizione,
           java.lang.String sValore) {
           this.sDescrizione = sDescrizione;
           this.sValore = sValore;
    }


    /**
     * Gets the sDescrizione value for this Categoria.
     * 
     * @return sDescrizione
     */
    public java.lang.String getSDescrizione() {
        return sDescrizione;
    }


    /**
     * Sets the sDescrizione value for this Categoria.
     * 
     * @param sDescrizione
     */
    public void setSDescrizione(java.lang.String sDescrizione) {
        this.sDescrizione = sDescrizione;
    }


    /**
     * Gets the sValore value for this Categoria.
     * 
     * @return sValore
     */
    public java.lang.String getSValore() {
        return sValore;
    }


    /**
     * Sets the sValore value for this Categoria.
     * 
     * @param sValore
     */
    public void setSValore(java.lang.String sValore) {
        this.sValore = sValore;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Categoria)) return false;
        Categoria other = (Categoria) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sDescrizione==null && other.getSDescrizione()==null) || 
             (this.sDescrizione!=null &&
              this.sDescrizione.equals(other.getSDescrizione()))) &&
            ((this.sValore==null && other.getSValore()==null) || 
             (this.sValore!=null &&
              this.sValore.equals(other.getSValore())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSDescrizione() != null) {
            _hashCode += getSDescrizione().hashCode();
        }
        if (getSValore() != null) {
            _hashCode += getSValore().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Categoria.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Categoria"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SDescrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sDescrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SValore");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sValore"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
