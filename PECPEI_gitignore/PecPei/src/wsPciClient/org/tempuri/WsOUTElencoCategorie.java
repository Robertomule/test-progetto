/**
 * WsOUTElencoCategorie.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

public class WsOUTElencoCategorie  implements java.io.Serializable {
    private int iEsito;

    private java.lang.String sDescrizione;

    private wsPciClient.org.tempuri.Categoria[] aCategorie;

    public WsOUTElencoCategorie() {
    }

    public WsOUTElencoCategorie(
           int iEsito,
           java.lang.String sDescrizione,
           wsPciClient.org.tempuri.Categoria[] aCategorie) {
           this.iEsito = iEsito;
           this.sDescrizione = sDescrizione;
           this.aCategorie = aCategorie;
    }


    /**
     * Gets the iEsito value for this WsOUTElencoCategorie.
     * 
     * @return iEsito
     */
    public int getIEsito() {
        return iEsito;
    }


    /**
     * Sets the iEsito value for this WsOUTElencoCategorie.
     * 
     * @param iEsito
     */
    public void setIEsito(int iEsito) {
        this.iEsito = iEsito;
    }


    /**
     * Gets the sDescrizione value for this WsOUTElencoCategorie.
     * 
     * @return sDescrizione
     */
    public java.lang.String getSDescrizione() {
        return sDescrizione;
    }


    /**
     * Sets the sDescrizione value for this WsOUTElencoCategorie.
     * 
     * @param sDescrizione
     */
    public void setSDescrizione(java.lang.String sDescrizione) {
        this.sDescrizione = sDescrizione;
    }


    /**
     * Gets the aCategorie value for this WsOUTElencoCategorie.
     * 
     * @return aCategorie
     */
    public wsPciClient.org.tempuri.Categoria[] getACategorie() {
        return aCategorie;
    }


    /**
     * Sets the aCategorie value for this WsOUTElencoCategorie.
     * 
     * @param aCategorie
     */
    public void setACategorie(wsPciClient.org.tempuri.Categoria[] aCategorie) {
        this.aCategorie = aCategorie;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsOUTElencoCategorie)) return false;
        WsOUTElencoCategorie other = (WsOUTElencoCategorie) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.iEsito == other.getIEsito() &&
            ((this.sDescrizione==null && other.getSDescrizione()==null) || 
             (this.sDescrizione!=null &&
              this.sDescrizione.equals(other.getSDescrizione()))) &&
            ((this.aCategorie==null && other.getACategorie()==null) || 
             (this.aCategorie!=null &&
              java.util.Arrays.equals(this.aCategorie, other.getACategorie())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getIEsito();
        if (getSDescrizione() != null) {
            _hashCode += getSDescrizione().hashCode();
        }
        if (getACategorie() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getACategorie());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getACategorie(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsOUTElencoCategorie.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "wsOUTElencoCategorie"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IEsito");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "iEsito"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SDescrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sDescrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACategorie");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "aCategorie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Categoria"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "Categoria"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
