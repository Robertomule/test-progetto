/**
 * WsOUTVerificaInvio.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

public class WsOUTVerificaInvio  implements java.io.Serializable {
    private int iEsito;

    private java.lang.String sDescrizione;

    public WsOUTVerificaInvio() {
    }

    public WsOUTVerificaInvio(
           int iEsito,
           java.lang.String sDescrizione) {
           this.iEsito = iEsito;
           this.sDescrizione = sDescrizione;
    }


    /**
     * Gets the iEsito value for this WsOUTVerificaInvio.
     * 
     * @return iEsito
     */
    public int getIEsito() {
        return iEsito;
    }


    /**
     * Sets the iEsito value for this WsOUTVerificaInvio.
     * 
     * @param iEsito
     */
    public void setIEsito(int iEsito) {
        this.iEsito = iEsito;
    }


    /**
     * Gets the sDescrizione value for this WsOUTVerificaInvio.
     * 
     * @return sDescrizione
     */
    public java.lang.String getSDescrizione() {
        return sDescrizione;
    }


    /**
     * Sets the sDescrizione value for this WsOUTVerificaInvio.
     * 
     * @param sDescrizione
     */
    public void setSDescrizione(java.lang.String sDescrizione) {
        this.sDescrizione = sDescrizione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsOUTVerificaInvio)) return false;
        WsOUTVerificaInvio other = (WsOUTVerificaInvio) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.iEsito == other.getIEsito() &&
            ((this.sDescrizione==null && other.getSDescrizione()==null) || 
             (this.sDescrizione!=null &&
              this.sDescrizione.equals(other.getSDescrizione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getIEsito();
        if (getSDescrizione() != null) {
            _hashCode += getSDescrizione().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsOUTVerificaInvio.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "wsOUTVerificaInvio"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IEsito");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "iEsito"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SDescrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sDescrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
