/**
 * WsPCItoSGDSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package wsPciClient.org.tempuri;

public interface WsPCItoSGDSoap extends java.rmi.Remote {
    public java.lang.String versione() throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTRecuperaElencoUffici wsRecuperaElencoUffici(java.lang.String sCasellaMittente, java.lang.String sCodiceAOO, java.lang.String sAccount, java.lang.String sMatricola) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTRecuperaElencoApprovatori wsRecuperaElencoApprovatori(java.lang.String sCasellaMittente) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTVerificaInvio wsVerificaInvio(java.lang.String sAccountAutore, java.lang.String sMatricolaAutore, java.lang.String sMessageID, java.lang.String codiceAOO, java.lang.String sAccountPEC, int iRichiediApprovazione) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTwsAnnullaInvioPEC wsAnnullaInvioPEC(java.lang.String sMessageID, java.lang.String sCodiceAOO, java.lang.String sAccountPEC) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTVerificaStatoApprovazione wsVerificaStatoApprovazione(java.lang.String sMessageID, java.lang.String sCodiceAOO, java.lang.String sAccountPEC) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTComunicaRispostePEC wsComunicaRispostePEC(java.lang.String sMessageIDOriginale, java.lang.String sMessageIDRisposta, java.lang.String sCodiceAOORisposta, java.lang.String sAccountPECRIsposta, java.lang.String sTipo) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTDestinatari wsDestinatariPerIniziale(org.apache.axis.types.UnsignedShort cCarattere) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTElencoCategorie wsElencoCategorie() throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTDestinatari wsDestinatariPerCategoria(java.lang.String sCategoria) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTElencoRegione wsElencoRegione() throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTElencoProvincia wsElencoProvincia(java.lang.String sRegione) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTElencoComune wsElencoComune(java.lang.String sRegione, java.lang.String sProvincia) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTDestinatari wsDestinatariPerZonaGeografica(java.lang.String sRegione, java.lang.String sProvincia, java.lang.String sComune) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTDestinatari wsDestinatariPerCodiceFiscaleCittadini(java.lang.String sCodiceFiscale) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTDestinatari wsElencoAccountINPSPrincipale(java.lang.String sCodiceAOO) throws java.rmi.RemoteException;
    public wsPciClient.org.tempuri.WsOUTDestinatari wsElencoAccountINPS(java.lang.String sCodiceAOO) throws java.rmi.RemoteException;
    public boolean wsValidaEmail(java.lang.String sEmail) throws java.rmi.RemoteException;
}
