set LIB_DIR=E:\Batch\Produzione\GD500GestioneMessaggistica\GD539PecPei\jar

set JAR_DIR=E:\Batch\Produzione\GD500GestioneMessaggistica\GD539PecPei\

set CLASS_PATH=%LIB_DIR%\cayenne-1.2.1.jar;%LIB_DIR%\jutf7-1.0.0.jar;%LIB_DIR%\commons-io-1.3.1.jar;%LIB_DIR%\commons-logging-1.1.1.jar;%LIB_DIR%\log4j-1.2.14.jar;%LIB_DIR%\osgi-2.0.1.jar;%LIB_DIR%\bcmail-jdk15on-150.jar;%LIB_DIR%\bcpkix-jdk15on-150.jar;%LIB_DIR%\bcprov-jdk15on-150.jar;%LIB_DIR%\java-libpst.0.7.jar;%LIB_DIR%\jaxrpc.jar;%LIB_DIR%\axis.jar;%LIB_DIR%\commons-lang3-3.3.jar;%LIB_DIR%\commons-discovery-0.4.jar;%LIB_DIR%\wsdl4j-1.6.2.jar;%LIB_DIR%\commons-beanutils-1.7.0.jar;%LIB_DIR%\junit-3.8.1.jar;%LIB_DIR%\poi-3.9.jar;%LIB_DIR%\poi-scratchpad-3.9.jar;%LIB_DIR%\sqljdbc4.jar;%LIB_DIR%\activation.jar;%LIB_DIR%\jdom-2.0.1.jar;%LIB_DIR%\dsn-1.6.2.jar;%LIB_DIR%\gimap-1.6.2.jar;%LIB_DIR%\imap-1.6.2.jar;%LIB_DIR%\javax.mail-1.6.2.jar;%LIB_DIR%\javax.mail-api-1.6.2.jar;%LIB_DIR%\logging-mailhandler-1.6.2.jar;%LIB_DIR%\mailapi-1.6.2.jar;%LIB_DIR%\pop3-1.6.2.jar;%LIB_DIR%\smtp-1.6.2.jar;

java -Xms512M -Xms1024M -cp "%JAR_DIR%PecPei.jar;%CLASS_PATH%" "it.eustema.inps.hermes.ScheduleMain" 
